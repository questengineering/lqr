#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/16/2012 10:53:14 AM
// Created by:   C.Madigan
//
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

namespace QuestIntegrity.LifeQuest.Common
{
    public static class ApplicationConstants
    {
        public static string AppCopyright
        {
            get { return string.Format("�2008-{0}", DateTime.Today.Year); }
        }

        public static Color QuestSwoosh
        {
            get { return Color.FromArgb(49, 143, 207); }
        }

        public static Color IconBackground
        {
            get { return Color.FromArgb(0, 121, 194); }
        }

        public static byte[] Key
        {
            get { return new byte[] { 80, 97, 53, 53, 119, 111, 114, 100 }; }
        }

        public static string AppUpdaterExecutable
        {
            get 
            {
                return Application.ProductName.Replace(" ", string.Empty) +"Updater.exe";
            }
        }
        
    }
}