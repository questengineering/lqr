﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: ScreenLayout.cs,v 1.36 2011/02/03 22:24:38 J.Rowe Exp $
//

#endregion

using System;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    ///     Class for performing standard layers for the application.  This is a singleton instance
    ///     that needs to get initialized one at startup.
    /// </summary>
    public class ScreenLayout
    {
        #region Private/Protected Properties

        protected static ScreenLayout _instance;
        private static readonly object SyncRoot = new object();

        private bool _initialized;
        protected DockManager _dockManager;
        protected DocumentManager _documentManager;

        #endregion

        #region Constructor

        protected ScreenLayout()
        { }

        #endregion

        #region Public Properties

        #endregion

        #region Public Methods

        public void Initialize(DockManager dockManager, DocumentManager documentManager)
        {
            if (dockManager != null)
            {
                _dockManager = dockManager;
            }

            if (documentManager != null)
            {
                _documentManager = documentManager;
            }

            _initialized = true;
        }

        #endregion

        #region Private Methods

        protected void Layout_Core()
        {
            
        }

        protected void SizeRootPanels(int leftWidth,
                                      int topHeight,
                                      int rightWidth,
                                      int bottomHeight)
        {
            foreach (DockPanel panel in _dockManager.RootPanels)
            {
                switch (panel.Dock)
                {
                    case DockingStyle.Bottom:
                        panel.Height = bottomHeight;
                        break;
                    case DockingStyle.Left:
                        panel.Width = leftWidth;
                        break;
                    case DockingStyle.Right:
                        panel.Width = rightWidth;
                        break;
                    case DockingStyle.Top:
                        panel.Height = topHeight;
                        break;
                    case DockingStyle.Fill:
                    case DockingStyle.Float:
                        break;
                }
            }
        }

        /// <summary>
        ///     Sets the split for a root panel containing two panels
        /// </summary>
        /// <param name="dockingStyle">The docking style.</param>
        /// <param name="panelIndex">Index of the panel.</param>
        /// <param name="size">The size.</param>
        protected void SetSplit(DockingStyle dockingStyle,
                                int panelIndex,
                                int size)
        {
            //Find the root panel
            DockPanel rootPanel = null;
            foreach (DockPanel panel in _dockManager.RootPanels)
            {
                if (panel.Dock == dockingStyle)
                {
                    rootPanel = panel;

                    break;
                }
            }

            //Find panels under the root
            int i = 0;
            foreach (DockPanel child in _dockManager.Panels)
            {
                if (child.HasAsParent(rootPanel))
                {
                    if (i == panelIndex)
                    {
                        if (rootPanel != null &&
                            rootPanel.DockVertical == DevExpress.Utils.DefaultBoolean.False)
                        {
                            child.Width = size;
                        }
                        else
                        {
                            child.Height = size;
                        }
                        break;
                    }
                    i++;
                }
            }
        }

        protected void EnsureInitialized()
        {
            if (!_initialized)
            {
                throw new InvalidOperationException("Layout must be initialized with a DockManager and DocumentManager.");
            }
        }

        #endregion
    }
}