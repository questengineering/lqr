#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/15/2008 10:27:55 AM
// Created by:   j.rowe
//
#endregion

using System.Reflection;
using System.Runtime.InteropServices;

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    /// Class provides version information for use in the application.
    /// </summary>
    public static class ApplicationVersion
    {
        /// <summary> Date-based version string that license file can compare to. Needs to be updated manually with each official rev. </summary>
        public const string LicenseVersion = "2018.0418";  //
        

        /// <summary>
        /// Gets the version string for the current application
        /// </summary>
        /// <returns>The full version string. E.g., 2.0.1.122</returns>
        public static string FullVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public static string Platform => System.Environment.Is64BitProcess ? "x64" : "x86";

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MemoryStatusEx
        {
            public uint Length;
            public uint MemoryLoad;
            public ulong TotalPhysical;
            public ulong AvailablePhysical;
            public ulong TotalPageFile;
            public ulong AvailablePageFile;
            public ulong TotalVirtual;
            public ulong AvailableVirtual;
            public ulong AvailableExtendedVirtual;

            public MemoryStatusEx()
            {
                Length = (uint)Marshal.SizeOf(typeof(MemoryStatusEx));
            }
        }
    }
}