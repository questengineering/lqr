﻿#region Copyright Quest Integrity, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 2/22/2012 2:43:38 PM
// Created by:   C.Turiano
//

#endregion

namespace QuestIntegrity.LifeQuest.Common
{
    public abstract class LifeQuestApplicationBase
    {
        #region Private Properties

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Public Properties

        public ScreenLayout Layout { get; set; }

        #endregion
    }
}