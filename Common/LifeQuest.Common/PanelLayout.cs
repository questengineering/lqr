#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: PanelLayout.cs,v 1.36 2011/02/03 22:24:38 J.Rowe Exp $
//
#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common
{
    #region Enums

    public class PanelLayout
    {
        public static readonly PanelLayout Default = new PanelLayout("Default");
       
        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public PanelLayout() { }
        protected PanelLayout(string TheValue) { Value = TheValue; }

        public string Value { get; set; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //These are utilized because XMLDeserialized versions of statics(like FeatureType.None) will not equality-compare with instantiated versions at runtime as expected.
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(Object obj)
        {
            if (obj == null) return false; // fails existance
            PanelLayout typedObj = obj as PanelLayout;
            if (typedObj == null) return false; // fails cast.
            return (string.Equals(Value, typedObj.Value));
        }

        protected bool Equals(PanelLayout other)
        {
            return string.Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        public static bool operator ==(PanelLayout A, PanelLayout B)
        {
            if (ReferenceEquals(A, B)) return true; //Means the same instance, or both null.
            if ((object)A == null || (object)B == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(A.Value, B.Value);
        }

        public static bool operator !=(PanelLayout A, PanelLayout B)
        {
            return !(A == B);
        }

        #endregion Equality Comparing
    }

    #endregion
}