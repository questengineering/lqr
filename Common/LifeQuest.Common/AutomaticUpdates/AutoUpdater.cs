#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 01/07/11 8:51:42 AM
// Created by:   j.rowe
//
// File:  $Id: AutoUpdater.cs,v 1.13 2011/03/08 01:10:59 J.Rowe Exp $
#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using log4net;
using Microsoft.Win32;
using QuestIntegrity.Core.Extensions.IO;

namespace QuestIntegrity.LifeQuest.Common.AutomaticUpdates
{
    /// <summary>
    /// Singleton class to manage auto-update operations
    /// </summary>    
    public class AutoUpdater
    {
        #region Enums

        public enum Config
        {
            Customer,
            Internal,
            Development
        }

        public enum ExitCode
        {
            ProcessStartFailed = -1,
            Success = 0,
            UpdateNotRequired = 1,
            LicenseNotValidForVersion = 2,
            UserAborted = 5,
            EvalutionExpired = 7,
            ClientExeNeededUpdate = 10,
            ClientDataNeededUpdate = 11,
            ClientExeAndDataNeededUpdate = 12,
            ExeInvalidName = 103,
            PluginFailed = 104,
            TemporaryFolderFailed = 105,
            Debugger = 998,
            Unknown = 999
        }

        #endregion

        #region Private Properties

        private static AutoUpdater _instance;
        private static readonly object syncRoot = new object();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors

        /// <summary>
        /// AutoUpdater class is a singleton.  Only expose private constructor and
        /// access is achieved through the public "Instance" property
        /// </summary>
        private AutoUpdater()
        {
            MinHoursBetweenChecks = 24;
        }

        #endregion

        #region Public Properties

        public static AutoUpdater Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        _instance = new AutoUpdater();
                    }
                }
                return _instance;
            }
        }

        public double MinHoursBetweenChecks
        {
            get;
            set;
        }

        public ExitCode ProcessExitCode
        {
            get;
            set;
        }

        public Config CurrentUpdateTarget { get; set; }

        /// <summary>
        /// Determines the current configuration based off of the registry key's value.
        /// </summary>
        public Config CurrentConfiguration {
            get
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(""); //TODO: Get app registry path.
                if (key != null)
                {
                    string value = key.GetValue("Configuration").ToString();
                    if (value != string.Empty)
                        return (Config)(Enum.Parse(typeof (Config), value));
                }
                return Config.Customer;
            }
        }

        public string CurrentUpdateSuffix
        {
            get
            {
                switch (CurrentUpdateTarget)
                {
                    case Config.Customer:
                        return "C";
                    case Config.Internal:
                        return "I";
                    case Config.Development:
                    default:
                        return "D";
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Performs the timed update check based on the specified update check interval
        /// </summary>
        /// <returns>Boolean indicating success or failure of update.</returns>
        public bool PerformTimedUpdateCheck()
        {
            // a check to see if LastUpdateCheck throws NullReferenceException
            return false;
        }

        /// <summary>
        /// Checks for an update regardless of the last update time check.
        /// </summary>
        /// <returns>Boolean indicating success or failure of update.</returns>
        public bool CheckForUpdate()
        {
            return RunTrueUpdateClient();
        }

        public void CheckForUpdateWithMessages()
        {
            bool isUpdate = RunTrueUpdateClient(false);
            string message;
            string caption = string.Format("Update Check");

            if (isUpdate)
            {
                switch (Instance.ProcessExitCode)
                {
                    case ExitCode.Success:
                        message = "You have the latest version.";
                        MessageBox.Show(message, caption);
                        break;
                    case ExitCode.UserAborted:
                        //Was handled by the installer, no extra UI needed.
                        break;
                    case ExitCode.LicenseNotValidForVersion:
                        message = "A newer version is available, but your license requires updating.\r\n"
                                   + "Please contact support@questintegrity.com to obtain a new license.";
                        MessageBox.Show(message, caption);
                        break;
                    case ExitCode.UpdateNotRequired:
                        message = string.Format("Your version of {0} is up-to-date.", "LifeQuest Reformer"); //TODO: Re-implement a way to get the product name.
                        MessageBox.Show(message, caption);
                        break;
                    case ExitCode.ClientExeNeededUpdate:
                    case ExitCode.ClientDataNeededUpdate:
                    case ExitCode.ClientExeAndDataNeededUpdate:
                        message = "The auto-update program is out of date.  Please go to\r\n"
                                    + "www.QuestIntegrity.com to download the latest update.";
                        MessageBox.Show(message, caption);
                        break;
                    case ExitCode.Debugger:
                        message = "You cannot run auto-updates while the Debugger is attached.\r\n"
                                    + "Detach the debugger or run outside of Visual Studio to test.";
                        MessageBox.Show(message, caption);
                        break;
                    case ExitCode.EvalutionExpired:
                    case ExitCode.ExeInvalidName:
                    case ExitCode.PluginFailed:
                    case ExitCode.TemporaryFolderFailed:
                    case ExitCode.Unknown:
                    case ExitCode.ProcessStartFailed:
                    default:
                        message = "Unable to check for updates.  Make sure that you are connected to the network.\r\n"
                            + "If this message persists, please contact support@questintegrity.com\r\n"
                            + "or check www.QuestIntegrity.com for updates.";
                        MessageBox.Show(message, caption);
                        break;
                }
            }
            else
            {
                message = "Unable to check for updates.\r\n"
                          + "Please check www.QuestIntegrity.com for available updates\r\n"
                          + "or contact support@questintegrity.com";
                MessageBox.Show(message, caption);
            }
        }

        #endregion

        #region Private Methods

        private bool RunTrueUpdateClient(bool silentUntilUpdate = true)
        {
            //Don't want to run while debugging or in IDE.
            if (Debugger.IsAttached)
            {
                _log.Debug("Cannot run updater from debugger.");
                ProcessExitCode = ExitCode.Debugger;
                return true;
            }

            using (Process updateProcess = new Process())
            {
                bool bReturn;
                try
                {
                    string args = string.Format("/{0} {1} /{2} {3} /{4} {5}",
                        "License", QuestIntegrity.Core.Licensing.LicenseManager.Instance.Version,
                        "Silent", silentUntilUpdate,
                        "Target", CurrentUpdateTarget
                        );
                    string updaterTargetExe = Path.GetFileNameWithoutExtension(ApplicationConstants.AppUpdaterExecutable);
                    updaterTargetExe = string.Format("{0}_{1}.exe", updaterTargetExe, CurrentUpdateSuffix);                    
                    updateProcess.StartInfo.FileName = Path.Combine(PathExtensions.CommonApplicationDataPath, "Updates", ApplicationVersion.Platform, updaterTargetExe);
                    updateProcess.StartInfo.Arguments = args;
                    updateProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;

                    //The Start method returns true on success and false on failure
                    bReturn = updateProcess.Start();
                    updateProcess.WaitForExit();

                    //Set exit code 
                    _log.DebugFormat("{0} Exit Code: {1}", updaterTargetExe, updateProcess.ExitCode);
                    if (Enum.IsDefined(typeof(ExitCode), updateProcess.ExitCode))
                    {
                        ProcessExitCode = (ExitCode)updateProcess.ExitCode;
                    }
                    else
                    {
                        ProcessExitCode = ExitCode.Unknown;
                    }
                }
                catch (Win32Exception e)
                {
                    //An error has occurred
                    bReturn = false;
                    _log.Error(string.Format("Error starting AutoUpdater client .exe at {0}", updateProcess.StartInfo.FileName), e);
                    ProcessExitCode = ExitCode.ProcessStartFailed;
                }
                return bReturn;
            }
        }

        #endregion
    }
}