﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/2/2009 2:11:43 PM
// Created by:   j.rowe
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using System.Xml.Serialization;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.LifeQuest.Common.Engineering
{
    /// <summary> Holds a simple pipe size records, assuming no nominal information and inches as base units. </summary>
    [XmlInclude(typeof(NominalPipeSize))]
    public class PipeSize
    {
        [Obsolete("Do not use this constructor. Only provided for XML Serialization.", true)]
        public PipeSize() { }

        public PipeSize(double innerRadius, double wallThickness)
        {
            InnerRadius = (decimal)innerRadius;
            WallThickness = (decimal)wallThickness;
            CustomName = "Tube";
        }

        public PipeSize(decimal outerDiameter, double wallThickness)
        {
            InnerRadius = outerDiameter / 2 - (decimal)wallThickness;
            WallThickness = (decimal)wallThickness;
            CustomName = "Tube";
        }

        public PipeSize(decimal innerDiameter, decimal wallThickness)
        {
            InnerRadius = innerDiameter / 2;
            WallThickness = wallThickness;
            CustomName = "Tube";
        }

        public decimal InnerRadius;
        public decimal WallThickness;
        public decimal OuterRadius { get { return InnerRadius + WallThickness; } }
        public decimal InnerDiameter { get { return InnerRadius * 2; } }
        public decimal OuterDiameter { get { return (InnerRadius + WallThickness) * 2; } }

        public decimal InnerRadiusInDisplayUnits 
        { get { return (decimal)Length.Convert((double)InnerRadius, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); } 
          set { InnerRadius = (decimal)Length.Convert((double)value, Length.LengthScale.Inches, DisplayUnits.Instance.MeasurementUnits.Scale); } }

        public decimal OuterRadiusInDisplayUnits
        { get { return (decimal)Length.Convert((double)OuterRadius, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); } }

        public decimal WallThicknessInDisplayUnits
        { get { return (decimal)Length.Convert((double)WallThickness, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); } 
          set { WallThickness = (decimal)Length.Convert((double)value, Length.LengthScale.Inches, DisplayUnits.Instance.MeasurementUnits.Scale); } }

        public decimal InnerDiameterInDisplayUnits
        { get { return (decimal)Length.Convert((double)InnerDiameter, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); } }
          
        public decimal OuterDiameterInDisplayUnits
        { get { return (decimal)Length.Convert((double)OuterDiameter, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); } }

        /// <summary> Gets the Inner Circumference in inches </summary>
        public decimal InnerCircumference { get { return Convert.ToDecimal(2.0 * Math.PI * (double)InnerRadius); } }

        /// <summary>  Gets the Outer Circumference in inches  </summary>
        public decimal OuterCircumference { get { return Convert.ToDecimal(2.0 * Math.PI * (double)OuterRadius); } }

        /// <summary> Empty string for non-nominal pipe size. </summary>
        public virtual string NPS { get { return string.Empty; } }

        /// <summary> null for non-nominal pipe size. </summary>
        public virtual decimal? NPS_Value { get { return null;} }

        /// <summary> null for non-nominal pipe size. </summary>
        public virtual int? DN { get { return null; } }

        /// <summary> Empty string for non-nominal pipe size. </summary>
        public virtual string Schedule { get { return string.Empty; } }

        /// <summary> 'X" Tube' for a non-nominal pipe size.  </summary>
        public virtual string TextString { get { return string.Format(@"{0}"" Tube", Math.Floor(OuterDiameter)); } }

        /// <summary> A common name for whatever this represents. </summary>
        public virtual string CustomName { get; set; }

        public static PipeSize CopyFrom(NominalPipeSize otherSize)
        {
            var returnSize = new NominalPipeSize(otherSize.NPS, otherSize.OuterDiameter, otherSize.Schedule, otherSize.WallThickness)
            {
                CustomName = otherSize.CustomName
            };
            return returnSize;
        }

        public static PipeSize CopyFrom(PipeSize otherSize)
        {
            var returnSize = new PipeSize(otherSize.InnerDiameter, otherSize.WallThickness)
            {
                CustomName = otherSize.CustomName
            };
            return returnSize;
        }

        #region Equality Override

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = InnerRadius.GetHashCode();
                hashCode = (hashCode * 397) ^ WallThickness.GetHashCode();
                hashCode = (hashCode * 397) ^ (CustomName != null ? CustomName.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <summary> Checks equality with another object by IR, WT, and CustomName values. </summary>
        public override bool Equals(Object other)
        {
            var typedOther = other as PipeSize;
            if (typedOther == null) return false;

            return InnerRadius == typedOther.InnerRadius && WallThickness == typedOther.WallThickness && string.Equals(CustomName, typedOther.CustomName);
        }

        #endregion Equality Override
    }
}