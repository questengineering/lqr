﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuestIntegrity.LifeQuest.Common.Engineering
{
    /// <summary>
    /// Class for holding and accessing a standard piping size table.  The table
    /// contains sizes for nominal piping for all schedules.  Use accessor methods
    /// for retreiving pre-selected distinct lists and fetching data from the table.
    /// Values from:  Tony's spreadsheet named OPS-FO-105-GLB-0 - FTIS & Hydra Job Package.xlsx (2014)
    /// </summary>
    public class PipeSchedules
    {
        #region Private/Protected Variables

        #endregion Private/Protected Variables

        #region Constructors

        /// <summary>  Private constructor for singleton instantiation. </summary>
        private PipeSchedules()
        {
            FillBasePipeSizeTable();
        }

        #endregion Constructors

        #region Public Properties

        /// <summary> The one and only instance of the pipe schedule table </summary>
        public static PipeSchedules Instance = new PipeSchedules();

        /// <summary> Pipeschedule table in its raw form.  Use other accessor methods to retreive data and lists from this table. </summary>
        public List<NominalPipeSize> PipeSizeTable { get; private set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary> Gets the ID and WT for a given actual OD(within .001) size and schedule  If no matching record is found function returns false. </summary>
        /// <param name="OD">OuterDiameter, e.g., 6.625, 8.625, etc </param>
        /// <param name="schedule">Pipe schedule, e.g., 40, 80, std, 120, etc.</param>
        /// <returns>True if matching value is found, otherwise false.</returns>
        public bool GetNominalPipingSize(decimal OD, string schedule, out decimal ID, out decimal WT)
        {
            NominalPipeSize nps = PipeSizeTable.FirstOrDefault(s => s.Schedule == schedule && Math.Abs(s.OuterDiameter - OD) < .002m);
            ID = nps.InnerDiameter;
            WT = nps.WallThickness;
            return (!nps.Equals(default(NominalPipeSize)));
        }

        /// <summary> Gets the NominalPipingSize record for a given actual OD (within .001) and schedule.  If no matching record is found function returns false. </summary>
        /// <param name="OD">OuterDiameter, e.g., 6.625, 8.625, etc </param>
        /// <param name="schedule">Pipe schedule, e.g., 40, 80, std, 120, etc.</param>
        /// <returns>True if matching value is found, otherwise false.</returns>
        public bool GetNominalPipingSize(decimal OD, string schedule, out NominalPipeSize NPS)
        {
            NPS = PipeSizeTable.FirstOrDefault(s => s.Schedule == schedule && Math.Abs(s.OuterDiameter - OD) < .002m);
            return (!NPS.Equals(default(NominalPipeSize)));
        }

        /// <summary> Finds the corresponding nominal size from the provided OD and WT (within .001). If no match is found, returns false. </summary>
        /// <param name="od">The od.</param>
        /// <param name="wt">The wt.</param>
        /// <param name="NPS">NominalPipingSize record</param>
        /// <returns></returns>
        public bool GetNominalPipingSize(decimal od, decimal wt, out NominalPipeSize NPS)
        {
            NPS = PipeSizeTable.FirstOrDefault(nps =>  Math.Abs(nps.OuterDiameter - od) < .002m && Math.Abs(nps.WallThickness - wt) < .002m);
            return (NPS != null && !NPS.Equals(default(NominalPipeSize)));
        }

        /// <summary> Finds the closest corresponding Nominal Piping Size.  First by OD, then by WT. If nominal is found, will return NominalPipeSize, otherwise it will just be a PipeSize </summary>
        public bool GetClosestNominalPipingSize(decimal od, decimal wt, out PipeSize PS)
        {
            decimal previousThickness = 0m;

            foreach (var pipingSize in PipeSizeTable.Where(p => p.OuterDiameter == PipeSizeTable.Min(ps => (ps.OuterDiameter - od))))
            {
                if (previousThickness < wt && pipingSize.WallThickness >= wt)
                {
                    PS = pipingSize;
                    return true;
                }
                previousThickness = pipingSize.WallThickness;
            }

            //Return just a pipe size with the given information.
            PS = new PipeSize(od, (double)wt);
            return false;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Load the base table.  Add additional sizes and schedules here.
        /// </summary>
        private void FillBasePipeSizeTable()
        {
            PipeSizeTable = new List<NominalPipeSize>();
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "5", 0.065m));
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "10", 0.109m));
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "40", 0.154m));
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "80", 0.218m));
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "160", 0.343m));
            PipeSizeTable.Add(new NominalPipeSize("2", 2, 2.375m, "XXS", 0.436m));

            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "5", 0.083m));
            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "10", 0.120m));
            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "40", 0.203m));
            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "80", 0.276m));
            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "160", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("2-1/2", 2.5m, 2.875m, "XXS", 0.552m));

            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "5", 0.083m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "10", 0.120m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "30", 0.188m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "40 / STD", 0.216m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "80 / XS", 0.300m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "160", 0.438m));
            PipeSizeTable.Add(new NominalPipeSize("3", 3, 3.5m, "XXS", 0.600m));

            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "5", 0.083m));
            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "10", 0.120m));
            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "30", 0.188m));
            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "40 / STD", 0.226m));
            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "80 / XS", 0.318m));
            PipeSizeTable.Add(new NominalPipeSize("3-1/2", 3.5m, 4m, "XXS", 0.636m));

            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "5", 0.083m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "10", 0.120m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "30", 0.188m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "40 / STD", 0.237m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "80 / XS", 0.337m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "120", 0.438m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "160", 0.531m));
            PipeSizeTable.Add(new NominalPipeSize("4", 4, 4.5m, "XXS", 0.674m));

            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "5", 0.109m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "10", 0.134m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "40 / STD", 0.258m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "80 / XS", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "120", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "160", 0.625m));
            PipeSizeTable.Add(new NominalPipeSize("5", 5, 5.563m, "XXS", 0.750m));

            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "5", 0.109m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "10", 0.134m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "40 / STD", 0.280m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "80 / XS", 0.432m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "120", 0.562m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "160", 0.719m));
            PipeSizeTable.Add(new NominalPipeSize("6", 6, 6.625m, "XXS", 0.864m));

            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "5", 0.109m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "10", 0.148m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "20", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "30", 0.277m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "40 / STD", 0.322m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "60", 0.406m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "80 / XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "100", 0.594m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "120", 0.719m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "140", 0.812m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "160", 0.906m));
            PipeSizeTable.Add(new NominalPipeSize("8", 8, 8.625m, "XXS", 0.875m));

            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "5", 0.134m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "10", 0.165m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "20", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "30", 0.307m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "40 / STD", 0.365m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "60 / XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "80", 0.594m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "100", 0.719m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "120", 0.844m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "140", 1.000m));
            PipeSizeTable.Add(new NominalPipeSize("10", 10, 10.75m, "160", 1.125m));

            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "5", 0.165m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "10", 0.180m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "20", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "30", 0.330m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "40", 0.406m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "60", 0.562m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "80", 0.688m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "100", 0.844m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "120 / XXS", 1.000m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "140", 1.125m));
            PipeSizeTable.Add(new NominalPipeSize("12", 12, 12.75m, "160", 1.312m));

            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "5", 0.156m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "10", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "20", 0.312m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "30 / STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "40", 0.438m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "60", 0.594m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "80", 0.750m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "100", 0.938m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "120", 1.094m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "140", 1.250m));
            PipeSizeTable.Add(new NominalPipeSize("14", 14, 14.00m, "160", 1.406m));

            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "5", 0.165m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "10", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "20", 0.312m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "30 / STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "40 / XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "60", 0.656m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "80", 0.844m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "100", 1.031m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "120", 1.219m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "140", 1.438m));
            PipeSizeTable.Add(new NominalPipeSize("16", 16, 16.00m, "160", 1.594m));

            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "5", 0.165m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "10", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "20", 0.312m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "30", 0.438m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "40", 0.562m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "60", 0.750m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "80", 0.938m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "100", 1.156m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "120", 1.375m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "140", 1.562m));
            PipeSizeTable.Add(new NominalPipeSize("18", 18, 18.00m, "160", 1.781m));

            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "5", 0.188m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "10", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "20 / STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "30 / XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "40", 0.594m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "60", 0.812m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "80", 1.031m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "100", 1.281m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "120", 1.500m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "140", 1.750m));
            PipeSizeTable.Add(new NominalPipeSize("20", 20, 20.00m, "160", 1.969m));

            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "5", 0.218m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "10", 0.250m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "20 / STD", 0.375m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "XS", 0.500m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "30", 0.562m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "40", 0.688m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "60", 0.969m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "80", 1.219m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "100", 1.531m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "120", 1.812m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "140", 2.062m));
            PipeSizeTable.Add(new NominalPipeSize("24", 24, 24.00m, "160", 2.344m));

            //used for custom sizes
            PipeSizeTable.Add(new NominalPipeSize("N/A", 0, 0m, "N/A", 0m));
        }

        #endregion Private Methods
    }
}