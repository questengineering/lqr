﻿using System;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions;

namespace QuestIntegrity.LifeQuest.Common.Engineering
{

    /// <summary> Holds information about a specific Nominal Piping Size.  The nominal piping size is unique based on the Nominal Size and the Schedule. </summary>
    public class NominalPipeSize : PipeSize
    {
        #region Constructor
        [Obsolete("Parameterless constructor only available for XML Serialization", true)]
        public NominalPipeSize() { }

        public NominalPipeSize(string nps, decimal od, string schedule, decimal thickness):base(od, (double)thickness)
        {
            NPS = nps;
            NPS_Value = nps.ToDecimal();
            Schedule = schedule;
            CustomName = TextString;
        }

        public NominalPipeSize(string nps, decimal nps_value, decimal od, string schedule, decimal thickness):base(od, (double)thickness)
        {
            NPS = nps;
            NPS_Value = nps_value;
            Schedule = schedule;
            CustomName = TextString;
        }
        #endregion

        #region Fields
        /// <summary> The Nominal Size (e.g. 2-1/2, 4, 8, 12, etc.) </summary>
        public new string NPS { get; set; }

        /// <summary> Decimal representation of the Nominal Size (e.g. 2.5, 4.0, 8.0, 12.0, etc) </summary>
        public new decimal? NPS_Value { get; set; }

        /// <summary> The schedule (e.g., 5, 5s, 40, 80, 80s, XXS, etc.) </summary>
        public new string Schedule { get; set; }

        /// <summary> Gets the text representation of the pipe schedule in format: 4" Sch 40  </summary>
        [XmlIgnore]
        public override string TextString { get { return string.Format("{0}\" Sch {1}", NPS_Value, Schedule); } }

        #endregion
        
    }
}

