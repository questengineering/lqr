﻿#region Copyright Quest Integrity Group, LLC 2014

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//

#endregion

using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using QuestIntegrity.Core.Extensions.Numeric;

namespace QuestIntegrity.LifeQuest.Common.Resources
{
    /// <summary>
    ///     Small class to help manage Most-Recently-Used file lists.  Property to cap the maximum size of the list
    ///     and methods to keep list sorted by mru at the top of the list.  Additionally helper methods
    ///     to convert from ArrayList which is what is serialize-able in Settings.
    /// </summary>
    public class MruList : List<string>
    {
        #region Private Properties

        private const int DefaultMaxSize = 8; //default maximum MruList size to store; display size is controlled outside of this class

        /// <summary> Specify the maximum size of the arraylist </summary>
        private int _maxSize = DefaultMaxSize;

        #endregion

        #region Public Properties

        public int MaxSize
        {
            get
            {
                return _maxSize;
            }
            set
            {
                _maxSize = value.Constrain(1, 20); //sensible values only
                TrimToMaxSize();
            }
        }

        #endregion

        #region Constructors

        public MruList()
        { }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Load the MruList from the specified settings file and property name.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="pathListSettingName"></param>
        public MruList(ApplicationSettingsBase settings,
                       string pathListSettingName)
        {
            object pathList = settings[pathListSettingName];
            if (pathList != null)
            {
                LoadFromArrayList((ArrayList)pathList);
            }
        }

        /// <summary>
        ///     Load the MruList from an ArrayList.
        /// </summary>
        /// <param name="pathList"></param>
        public MruList(ArrayList pathList)
        {
            LoadFromArrayList(pathList);
        }

        /// <summary>
        ///     Move the object to the top of the list.
        /// </summary>
        /// <param name="value"></param>
        public void AddToTop(string value)
        {
            Remove(value); //remove the value from where ever it is in the list
            Insert(0, value); //add it at the top of the list
            if (Count > MaxSize)
            {
                int itemsToRemove = Count - MaxSize;
                RemoveRange(Count - itemsToRemove, itemsToRemove);
            }
        }

        /// <summary> Converts this List to an ArrayList </summary>
        public ArrayList ToArrayList()
        {
            ArrayList array = new ArrayList(this);
            return array;
        }

        #endregion

        #region Private Methods

        private void TrimToMaxSize()
        {
            while (Count > MaxSize && MaxSize > 0)
            {
                RemoveAt(Count - 1);
            }
        }

        private void LoadFromArrayList(ArrayList pathList)
        {
            if (pathList == null) return;

            for (int i = 0; i < pathList.Count && i < MaxSize; i++)
            {
                Add(pathList[i].ToString());
            }
        }

        #endregion
    }
}
