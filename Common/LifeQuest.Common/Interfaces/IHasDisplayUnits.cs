﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
#endregion Copyright Quest Integrity Group, LLC 2012

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    /// <summary>
    /// Interface for any class that uses DisplayUnits such that it might need to change when the DisplayUnits changes
    /// </summary>
    public interface IHasDisplayUnits
    {
        void UpdateDisplayUnits();
    }
}
