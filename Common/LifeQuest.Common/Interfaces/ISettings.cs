﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/26/2008 11:06:36 PM
// Created by:   J.Rowe
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    public interface ISettings
    {
        bool UpgradeSettings { get; set; }

        void Upgrade();
    }
}