﻿#region Copyright Quest Integrity, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 2/20/2012 12:56:13 PM
// Created by:   C.Turiano
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    public interface ILifeQuestBaseView : ILifeQuestBaseForm
    {
    }
}
