#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//
#endregion

using DevExpress.XtraBars.Docking;

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    public interface ILifeQuestBaseForm
    {
        DockingStyle FormDockingStyle { get; set; }
        string TabText { get; set; }
        int InitialFormHeight { get; }
        bool Duplicatable { get; set; }
        
        // Read properties
        string GetFormText();

        // Setting of properties
        void SetVisibility(bool value);

   }
}
