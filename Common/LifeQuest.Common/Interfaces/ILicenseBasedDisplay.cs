#region Copyright Quest Integrity Group, LLC 2012
//
// www.questintegritygroup.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/11/2012 2:12:52 PM
// Created by:   j.rowe
//
// File:  $Id:$
#endregion

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    /// <summary>
    /// Interface to indicate that a form had conditional display based on the License
    /// </summary>    
    public interface ILicenseBasedDisplay
    {
        void SetLicenseBasedDisplay();
    }
}
