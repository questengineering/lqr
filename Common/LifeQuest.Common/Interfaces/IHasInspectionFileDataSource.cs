#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/16/2008 8:20:04 AM
// Created by:   j.rowe
//
#endregion

using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    /// <summary>
    /// Interface for a window that is tied to an inspection file.
    /// </summary>
    public interface IHasInspectionFileDataSource : IHasDataSource
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a reference to the current inspection file.
        /// </summary>
        /// <value>
        /// The current inspection file.
        /// </value>
        InspectionFile CurrentInspectionFile { get; set; }

        #endregion
    }
}
