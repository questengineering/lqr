﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/26/2008 11:06:36 PM
// Created by:   J.Rowe
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    public interface IDataToolTip
    {
        /// <summary>
        /// Gets or sets a value indicating whether Data Tool tips are displayed.
        /// </summary>
        /// <value><c>true</c> if Data Tool tips are displayed; otherwise, <c>false</c>.</value>
        bool DisplayDataTips { get; set; }

        /// <summary>
        /// Hides the data tool tip.
        /// </summary>
        void HideDataTip();

        /// <summary>
        /// Shows the data tool tip.
        /// </summary>
        /// <param name="toolTipText">The tool tip text.</param>
        void ShowDataTip(string toolTipText);
    }
}