﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//
#endregion

using System.Windows.Forms;
using DevExpress.XtraBars.Docking;
using QuestIntegrity.LifeQuest.Common.Forms;

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    public interface IMain
    {
        // Properties
        float DpiX { get; }
        IWin32Window MainForm { get; }
        
        // Form methods
        void SetupForm(LifeQuestBaseForm form, DockingStyle dockingStyle = (DockingStyle)(-1), bool autoHide = false);
        void SetCursor(Cursor cursor);
        void SetEnabled(bool enableForm);
        void SuspendFormDrawing();
        void ResumeFormDrawing(bool doRedraw);
        void RefreshForm();
        void SuspendFormLayout();
        void ResumeFormLayout(bool performLayout);
        void ActivateForm();
        void CloseAllPanels();
        void CloseDockedPanels();
        void CloseTabbedPanels();
        
        // Show messages
        void ShowMessage(string message);
        void ShowMessage(string message, string caption);
        DialogResult ShowMessage(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon);
    }
}
