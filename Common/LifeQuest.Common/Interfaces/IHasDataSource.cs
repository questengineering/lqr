﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: IHasDataSource.cs,v 1.44 2011/11/23 22:42:16 C.Turiano Exp $
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Interfaces
{
    /// <summary>
    /// A class that implements IHasDataSource must implement the UpdateDataSource() method
    /// </summary>
    public interface IHasDataSource
    {
        /// <summary>
        /// Updates this class's data source
        /// </summary>
        void UpdateDataSource();
    }
}