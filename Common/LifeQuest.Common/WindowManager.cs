#region Copyright Quest Integrity Group, LLC 2012

// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: WindowManager.cs,v 1.514 2012/01/26 22:11:29 J.Rowe Exp $

#endregion Copyright Quest Integrity Group, LLC 2010

using System;
using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.LifeQuest.Common.Forms;

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    ///     Main Controller for Panels within the application
    /// </summary>
    public class WindowManager
    {
        #region Private/Protected Variables

        protected static WindowManager _instance;
        protected static readonly object SyncRoot = new object();

        protected readonly List<LifeQuestBaseForm> OpenWindows = new List<LifeQuestBaseForm>();
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion Private/Protected Variables

        #region Constructors

        protected WindowManager()
        {
            
        }

        #endregion Constructors

        #region Public Properties

        public List<LifeQuestBaseView> OpenViewWindows
        {
            get { return OpenWindows.Where(W => W is LifeQuestBaseView).Cast<LifeQuestBaseView>().ToList(); }
        }

        #endregion Public Properties

        #region Public Methods  

        /// <summary> Gets all open forms stored within WindowManager that implement a certain interface, IE: IHasDisplayUnits </summary>
        /// <param name="TheType">An interface or type in the form of: <b>typeof(iFace)</b></param>
        public List<LifeQuestBaseForm> GetFormsByInterface(Type TheType)
        {
            return OpenWindows.Where(F => F.GetType().GetInterfaces().Contains(TheType)).ToList();
        }

        public List<LifeQuestBaseForm> GetFormsByType(Type TheType, bool IncludeSubClasses)
        {
            return IncludeSubClasses 
                ? OpenWindows.Where(F => F.GetType().IsSubclassOf(TheType) || F.GetType() == TheType).ToList() 
                : OpenWindows.Where(F => F.GetType() == TheType).ToList();
        }

        /// <summary> Adds a form to the window manager's list of open forms if not already inside the list, but does not create it. </summary>
        public void AddForm(LifeQuestBaseForm Form)
        {
            if (OpenWindows.Contains(Form)) return;
            OpenWindows.Add(Form);
        }

        public void RemoveForm(LifeQuestBaseForm Form)
        {
            if (!OpenWindows.Contains(Form)) return;
            OpenWindows.Remove(Form);
        }

        /// <summary>
        ///     Create a view of the type requested
        /// </summary>
        public LifeQuestBaseView CreateView(ViewType TheViewType)
        {
            LifeQuestBaseForm[] existingForms = GetFormsByType(TheViewType.ObjectType, false).Where(F => !F.Duplicatable).ToArray();
            if (existingForms.Any())
            {
                ShowForm(existingForms.First());
                return null;
            }
            LifeQuestBaseView view = (LifeQuestBaseView)Activator.CreateInstance(TheViewType.ObjectType);
            OpenWindows.Add(view);
            return view;
        }

        /// <summary>
        ///     Create a form of the type requested, returns null if not duplicatable and already exists.
        /// </summary>
        public LifeQuestBaseForm CreateForm(FormType TheFormType)
        {
            LifeQuestBaseForm[] existingForms = GetFormsByType(TheFormType.ObjectType, false).Where(F => !F.Duplicatable).ToArray();
            if (existingForms.Any())
            {
                ShowForm(existingForms.First());
                return null;
            }
            LifeQuestBaseForm form = (LifeQuestBaseForm)Activator.CreateInstance(TheFormType.ObjectType);
            OpenWindows.Add(form);
            return form;
        }

        #endregion Public Methods

        #region Protected Methods

        protected virtual void ShowForm(LifeQuestBaseForm TheForm)
        {
            throw new NotImplementedException("Implement form-showing in inherited versions of WindowManager.");
        }

        #endregion Protected Methods
    }
}