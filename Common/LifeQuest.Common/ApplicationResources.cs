#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/16/2012 12:29:19 PM
// Created by:   C.Madigan
//
#endregion

using System.Drawing;
using QuestIntegrity.LifeQuest.Common.Abstract;

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    /// TODO: Summary goes here.  Don't leave this default text here
    /// under penalty of death.
    /// </summary>    
    public class ApplicationResources : LifeQuestResources
    {
        #region Dummy Implementations

        /// <summary>
        /// These properties must be overridden in the descendants
        /// </summary>
        public override Icon AppIcon
        {
            get { return null; }
 
        }

        public override Image AboutLogo
        {
            get { return null; }
        }

        #endregion
    }
}
