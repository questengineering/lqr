#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/30/2008 11:19:13 AM
// Created by:   j.rowe
//

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HDF5DotNet;

namespace QuestIntegrity.LifeQuest.Common.IO
{
    /// <summary> Reads .qidh type files. </summary>
    public class HDF5FileDataReader :IDisposable
    {
        #region Private/Protected Variables

        private H5FileId _fileId;
        #endregion

        #region Constructors

        public HDF5FileDataReader(FileInfo TheFile)
        {
            TheHDF5File = TheFile;
            Initialize(TheFile);
        }

        /// <summary> Opens the file and stores its ID.</summary>
        private void Initialize(FileInfo TheFile)
        {
            _fileId = H5F.open(TheFile.FullName, H5F.OpenMode.ACC_RDONLY);
        }
        
        #endregion

        #region Public Properties

        public FileInfo TheHDF5File { get; private set; }
        public float SizeInMB { get { return (float)(TheHDF5File.Length*9.53674e-7); } }

        #endregion

        #region Public Methods

        /// <summary>Gets all points between the start and end index provided, including the start and end index.</summary>
        public void GetPositions(long StartIndex, long EndIndex, out List<float> OutputPosition )
        {
            //Designate a dimensional array that defines which dimensions we're pulling from. Position only has 1.
            long numPoints = EndIndex - StartIndex + 1;
            long[] dims = { numPoints }; //Add 1 to be inclusive of the end index.

            //Set up some references so we know what we're getting.
            float[] positions = new float[dims[0]];
            H5DataSetId positionDataSetId = H5D.open(_fileId, "Axial");
            H5DataSpaceId positionDataSpaceId = H5D.getSpace(positionDataSetId);

            //Allocate 1D memory to read into.
            H5DataSpaceId memSpace = H5S.create_simple(1, dims);

            //Select a hyperslab of 1D position data based off of the dataspace ID.
            H5S.selectHyperslab(positionDataSpaceId, H5S.SelectOperator.SET, new[] { StartIndex }, new[] { numPoints });

            //Read into the memory space we built.
            H5D.read(positionDataSetId, new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT), memSpace, positionDataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<float>(positions));

            OutputPosition = positions.ToList();
        }

        public void GetScalarData<T>(int StartIndex, int EndIndex, int StartCircIndex, int EndCircIndex, string ScalarKey, out List<T> OutputData )where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            //Get some information about the scalars we're about to read.
            H5T.H5Type dataType = HDF5Helper.GetH5DataType(typeof (T));
            H5DataSetId dataSetId= H5D.open(_fileId, ScalarKey);
            H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
            H5DataTypeId dataTypeInFile = H5D.getType(dataSetId);

            //Designate a dimensional array that defines which dimensions we're pulling from. Position only has 1.
            long numPoints = EndIndex - StartIndex + 1;
            long numCircIndices = EndCircIndex - StartCircIndex + 1;
            T[,] dataArray = new T[numPoints, numCircIndices];
            long[] dims = { numPoints, numCircIndices };

            //Allocate 2D memory to read into.
            H5DataSpaceId memSpace = H5S.create_simple(2, dims);

            //Select a hyperslab of 2D scalar data based off of the dataspace ID.
            H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { StartIndex, StartCircIndex }, new[] { numPoints, numCircIndices });

            //Read into the memory space we built.
            H5D.read(dataSetId, new H5DataTypeId(dataType), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<T>(dataArray));
            
            //Unwind the 2D array into a 1D, unfortunately we have to do some manual type-casting.
            T[] unwoundData = HDF5Helper.Unwind2DDataTo1D(dataArray);
            OutputData = unwoundData.ToList();
        }

        #endregion

        #region Private Methods

        
        

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            H5F.close(_fileId);
        }

        #endregion

        public void GetSensorData<T>(int StartIndex, int EndIndex, int Sensor, string ScalarKey, out List<T> OutputData ) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            //Designate a dimensional array that defines which dimensions we're pulling from. Position only has 1.
            long numPoints = EndIndex - StartIndex + 1;
            long[] dims = { numPoints, 1 }; //Add 1 to be inclusive of the end index.

            //Set up some references so we know what we're getting.
            T[,] dataArray = new T[dims[0], dims[1]];
            H5DataSetId dataSetId = H5D.open(_fileId, ScalarKey);
            H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);

            //Allocate 1D memory to read into.
            H5DataSpaceId memSpace = H5S.create_simple(2, dims);

            //Select a hyperslab of 1D position data based off of the dataspace ID.
            H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { StartIndex, Sensor - 1 }, new[] { numPoints, 1 });

            //Read into the memory space we built.
            H5D.read(dataSetId, new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<T>(dataArray));
            
            //Unwind the 2D array into a 1D, unfortunately we have to do some manual type-casting.
            T[] unwoundData = new T[numPoints];
            if (typeof(T) == typeof(float))
                unwoundData = HDF5Helper.Unwind2DDataTo1D(dataArray as float[,]) as T[];
            if (typeof(T) == typeof(Int32))
                unwoundData = HDF5Helper.Unwind2DDataTo1D(dataArray as Int32[,]) as T[];
            OutputData = unwoundData.ToList();
        }
    }
}

