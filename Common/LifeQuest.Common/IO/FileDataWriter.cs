#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/3/2008 7:33:34 AM
// Created by:   J.Rowe
//
#endregion

using System;
using System.IO;
using QuestIntegrity.LifeQuest.Common.Data.Interfaces;

namespace QuestIntegrity.LifeQuest.Common.IO
{
    /// <summary>
    /// Base class for FileData writers.
    /// </summary>
    public abstract class FileDataWriter : IDisposable
    {
        #region Private/Protected Fields

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected string OriginalFileName;
        protected string TempFileName;

        #endregion

        #region Constructors

        protected FileDataWriter()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Indicates if the FileDataReader is capable of Deflating compressed streams.
        /// </summary>
        protected bool CanDeflate;

        /// <summary>
        /// Indicates if the FileDataReader is capable of Decrypting compressed streams.
        /// </summary>
        protected bool CanDecrypt;

        public abstract float SizeInMB
        {
            get;
        }

        #endregion

        #region Public Methods

        public abstract void Write(IFileData fileData);

        public void Close()
        {
            Dispose();
        }

        #endregion

        #region Private Methods

        protected void SwapFiles()
        {
            string newTempFile = string.Empty;
            try
            {
                //If there was not an original file, no need to swap, just move.
                if (!File.Exists(OriginalFileName))
                {
                    File.Move(TempFileName, OriginalFileName);
                }
                else
                {
                    //Otherwise, perform the swap
                    newTempFile = Path.GetTempFileName();  //use to get a unique name, but cannot exist when "move" is run
                    File.Delete(newTempFile);              //remove temp file
                    File.Move(OriginalFileName, newTempFile);
                    File.Move(TempFileName, OriginalFileName);
                }
            }
            catch (Exception)
            {
                if (!File.Exists(OriginalFileName))
                {
                    //failed mid-move, try to recover
                    if (File.Exists(newTempFile))
                    {
                        File.Move(newTempFile, OriginalFileName);
                    }
                    else if (File.Exists(TempFileName))
                    {
                        File.Move(TempFileName, OriginalFileName);
                    }
                }
                Log.Error("Unable to swap temporarily written file.  File changes not made.");
                throw;
            }
            finally
            {
                if (File.Exists(newTempFile))
                {
                    File.Delete(newTempFile);
                }
            }
        }

        #endregion

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}