#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/30/2008 11:19:13 AM
// Created by:   j.rowe
//
#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.IO
{
    [Flags]
    public enum EncryptionCompressionFlag
    {
        /// <summary>
        /// Indicates that there is no encryption or compression
        /// </summary>
        None = 0x1,
        /// <summary>
        /// Indicates that the data is encrypted
        /// </summary>
        Encrypted = 0x2,
        /// <summary>
        /// Indicates that the data is compressed
        /// </summary>
        Compressed = 0x4
    }

 }

