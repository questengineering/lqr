#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/30/2008 11:19:13 AM
// Created by:   j.rowe
//
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using QuestIntegrity.Common.Conversion;
using QuestIntegrity.Common.IO;
using QuestIntegrity.LifeQuest.Common.Data;

#pragma warning disable 618

namespace QuestIntegrity.LifeQuest.Common.IO
{
    /// <summary>
    /// Reads the Peaks File data
    /// Format is:
    /// % ---------- Get File Header Info  ---------------------
    ////% Raw Peak FILE FORMAT: 12-20-11
    ////%
    ////% PreHeader
    ////% 	Date	 		(int32)  - 20090501 - (YYYYMMDD)  Date when this version was created/compiled
    ////% 	Version 		(int32)  - 1003 - Incrementing number with each new version starting at 1003
    ////%
    ////% Header
    ////% 	NumberOfSlices		(int32)  - number of sweeps/slices in the file
    ////% 	NumberOfXDucers		(in32)	 - number of transducers
    ////% 	NumberOfPeaks		(int32)	 - number of Peaks

    ////%Header String Data
    ////% 	ModelNumber         (char)   - null terminated string. "FTIS 4-Inch\0"
    ////% 	SerialNumber		(char)   - null terminated string. "4101\0"
    ////% 	HeaderTermination	(char)   - 4 nulls =>  \0\0\0\0
    ////% 
    ////%1D Array
    ////%Xdcr Location     (int32)  Location of sensors on tool
    ////%
    ////% 3D Peak Data Array
    ////% 	Peaks 		(int16)  - [No Slices, No xdcrs, No Peaks]
    ////% Each 16bit peak is formated as follows:
    ////%Pk1Idx, Amp1Idx - [u8,u8]
    ////%Pk2Idx, Amp2Idx - [u8,u8]
    ////%Pk3Idx, Amp3Idx - [u8,u8]
    ////%Pk4Idx, Amp4Idx - [u8,u8]
    /// </summary>
    public class PeaksFileDataReader : FileDataReader
    {
        #region Public Enums

        public enum PKSExportVersions
        {
            PreVersion3001 = 0,
            Version3001 = 3001 //first header version 12/21/2011
        }

        #endregion

        #region Private/Protected Variables

        private const string rlfDebugPrefix = "RLF READ:";
        private const int PreVersion3001CompileDate = 20110101;
        private RLPHeader _header;
        private Stream _baseStream;
        private EndianBinaryReader _ebinReader;

        #endregion

        #region Constructors

        public PeaksFileDataReader(Stream stream)
        {
            if(stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if(!stream.CanRead)
            {
                throw new ArgumentException("Stream must be readable.");
            }
            Initialize(stream);
        }

        public PeaksFileDataReader(string fileName)
        {
            if(!File.Exists(fileName))
            {
                throw new ArgumentException(string.Format("File {0} does not exist.", fileName));
            }
            
            using(Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                Initialize(stream);
            }
        }

        #endregion

        #region Public Properties

        public RLPHeader Header
        {
            get
            {
                return _header;
            }
        }

        public int NumberOfSlices
        {
            get;
            private set;
        }

        public override float SizeInMB
        {
            get
            {
                float size = 0.0F;
                if(_baseStream != null)
                {
                    size = ((float)_baseStream.Length / 1000000.0F);
                }
                return size;
            }
        }

        public int[] TransducerMap
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        public override IFileData Read()
        {
            _header = ReadHeader();
            return ReadInternal();
        }

        public FileData Read(int startIndex, int valuesToRead)
        {
            _header = ReadHeader();
            return ReadInternal(startIndex, valuesToRead);
        }

        public RLPHeader ReadHeader()
        {
            _ebinReader.Seek(0, SeekOrigin.Begin);
            return ReadHeader(_ebinReader);
        }

        private RLPHeader ReadHeader(EndianBinaryReader binaryReader)
        {
            try
            {
                //read the pre-header
                binaryReader.Seek(0, SeekOrigin.Begin);
                RLFPreHeader preheader = new RLFPreHeader()
                {
                    DateCompiled = binaryReader.ReadInt32()
                };
                LogVariableValue(rlfDebugPrefix, "DateCompiled", "int32", preheader.DateCompiled);
                preheader.Version = binaryReader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "Version", "int32", preheader.Version);

                //Setup the header
                RLPHeader header = new RLPHeader();
                bool isValidVersion = Enum.IsDefined(typeof(PKSExportVersions), preheader.Version);
                if(isValidVersion)
                {
                    header.PreHeader = preheader;
                }
                else
                {
                    header.PreHeader.Version = (int)PKSExportVersions.PreVersion3001;
                    header.PreHeader.DateCompiled = PreVersion3001CompileDate;
                }

                //Create the header                
                switch(header.PreHeader.Version)
                {
                    case 0:
                        return ReadHeaderVersion0(binaryReader, header);
                    case 3001:
                    default:
                        return ReadHeaderVersion3001(binaryReader, header);
                }
            }
            catch(Exception e)
            {
                _log.Error("Error reading file header.", e);
                throw new Exception("File header cannot be read.  Verify the Header structure.", e);
            }
        }

        /// <summary>
        /// Reads the original PKS file with no pre-header
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        private RLPHeader ReadHeaderVersion0(EndianBinaryReader reader, RLPHeader header)
        {
            try
            {
                //Read the header
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                long startPos = reader.BaseStream.Position;
                header.NumberOfSlices = (int)reader.ReadInt32();
                NumberOfSlices = (int)header.NumberOfSlices;
                LogVariableValue(rlfDebugPrefix, "NumberOfSlices", "int32", header.NumberOfSlices);

                header.NumberOfTransducers = (int)reader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "NumberOfXDucers", "int32", header.NumberOfTransducers);

                header.NumberOfPeaks = (int)reader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "NumberOfPeaks", "int32", header.NumberOfPeaks);

                header.ModelNumber = string.Empty;
                header.SerialNumber = string.Empty;

                long endPosition = reader.BaseStream.Position;
                header.EndPosition = endPosition;
                header.DataStartPosition = endPosition;
                return header;
            }
            catch(Exception e)
            {
                _log.Error("Error reading file header.", e);
                throw new Exception("File header cannot be read.  Verify the Header structure.", e);
            }
        }

        /// <summary>
        /// Read the Version 3001 header
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        private RLPHeader ReadHeaderVersion3001(EndianBinaryReader reader, RLPHeader header)
        {
            try
            {
                //Set flags based on the preheader version
                // No flags yet

                //Read the header
                reader.BaseStream.Seek(8, SeekOrigin.Begin);
                long startPos = reader.BaseStream.Position;
                header.NumberOfSlices = (int)reader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "NumberOfSlices", "int32", header.NumberOfSlices);
                NumberOfSlices = (int)header.NumberOfSlices;

                header.NumberOfTransducers = (int)reader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "NumberOfXDucers", "int32", header.NumberOfTransducers);

                header.NumberOfPeaks = (int)reader.ReadInt32();
                LogVariableValue(rlfDebugPrefix, "NumberOfPeaks", "int32", header.NumberOfPeaks);

                header.ModelNumber = reader.ReadStringTerminated(0x0);
                LogVariableValue(rlfDebugPrefix, "ModelNumber", @"char[]", header.ModelNumber);

                header.SerialNumber = reader.ReadStringTerminated(0x0);
                LogVariableValue(rlfDebugPrefix, "SerialNumber", @"char[]", header.SerialNumber);

                //header is terminated by 4 null values -- check this
                bool headerComplete = true;
                const int nullBytes = 3;
                for(int i = 0; i < nullBytes; i++)
                {
                    byte termByte = reader.ReadByte();
                    headerComplete &= (termByte == 0);
                }
                if(!headerComplete)
                {
                    throw new Exception("File header is improperly terminated.  Verify the Header structure");
                }
                else
                {
                    LogMessage(rlfDebugPrefix, "Termination bytes read OK");
                }

                long endPosition = reader.BaseStream.Position;
                header.EndPosition = endPosition;

                header.DataStartPosition = endPosition + (4 * header.NumberOfTransducers);  //4-bytes per transducer. This comes after the header.

                return header;
            }
            catch(Exception e)
            {
                _log.Error("Error reading file header.", e);
                throw new Exception("File header cannot be read.  Verify the Header structure.", e);
            }
        }

        private FileDataPeaks ReadInternal(int startIndex = 0, int numberOfSlices = -1)
        {
            //Make sure that the header has been read
            if(_header.Equals(default(RLPHeader)))
            {
                ReadHeader();
            }
            _ebinReader.BaseStream.Seek(_header.EndPosition, SeekOrigin.Begin);

            //Slices < 0 signifies to read all slices
            if(numberOfSlices < 0)
            {
                numberOfSlices = (int)_header.NumberOfSlices;
            }
            else if(startIndex + numberOfSlices > _header.NumberOfSlices)
            {
                numberOfSlices = (int)_header.NumberOfSlices - startIndex;
            }

            //Read the transducer location array  int32[]
            TransducerMap = ReadInt32Array(_header.NumberOfTransducers);

            //Read the peaks - data is packed as slice->xducer->peak[index, amplitude]
            //First move to the specified starting index
            _ebinReader.BaseStream.Seek(_header.DataStartPosition, SeekOrigin.Begin);
            _ebinReader.BaseStream.Seek(startIndex * 2 * _header.NumberOfTransducers * _header.NumberOfPeaks, SeekOrigin.Current);

            //Read specified number of slices
            List<List<List<Peak>>> data = ReadPeaksData(numberOfSlices);

            FileDataPeaks peaks = new FileDataPeaks()
            {
                Peaks = data
            };

            return peaks;
        }

        /// <summary>
        /// Reads specified number of slices from current file position
        /// </summary>
        /// <param name="numberOfSlices"></param>
        /// <returns></returns>
        private List<List<List<Peak>>> ReadPeaksData(int numberOfSlices)
        {
            List<List<List<Peak>>> data = new List<List<List<Peak>>>(numberOfSlices);
            for(int slice = 0; slice < numberOfSlices; slice++)
            {
                List<List<Peak>> xdcrs = new List<List<Peak>>(_header.NumberOfTransducers);

                //Read the data
                for (int xdcr = 0; xdcr < _header.NumberOfTransducers; xdcr++)
                {
                    List<Peak> peaks = new List<Peak>(_header.NumberOfPeaks);
                    for(int peak = 0; peak < _header.NumberOfPeaks; peak++)
                    {
                        Peak p = new Peak()
                        {
                            Index = (byte)_ebinReader.BaseStream.ReadByte(),
                            Amplitude = (byte)_ebinReader.BaseStream.ReadByte()
                        };
                        peaks.Add(p);
                    }
                    xdcrs.Add(peaks);
                }
                data.Add(xdcrs);
            }
            return data;
        }

        #endregion

        #region Private Methods

        #region Array Reading Routings

        //Reads a contiguous set of decimal values stored as int16's but that
        // are scaled by a factor of 1000.  Zero values are set to NaNs according to flag
        // Array is laid out by transducer.  I.e., all readings for transducer 1, then xducer 2, then xducer3, etc..
        // Therefore, we need to skip around a bit to read chunks at a time.
        protected float[] ReadScaledSingleDataArray(long arrayStartPosition, int offsetSlices, int numSlices, bool setZerosToNaNs)
        {
            int xducers = _header.NumberOfTransducers;
            float[] values = new float[numSlices * xducers];
            for (short xducer = 0; xducer < _header.NumberOfTransducers; xducer++)
            {
                long startPosition = arrayStartPosition + (offsetSlices * sizeof(short)) + (xducer * _header.NumberOfSlices * sizeof(short));
                _ebinReader.Seek(startPosition, SeekOrigin.Begin);
                for(int slice = 0; slice < numSlices; slice++)
                {
                    short value = _ebinReader.ReadInt16();
                    if(value == 0 && setZerosToNaNs)
                    {
                        values[(slice * xducers) + xducer] = float.NaN;
                    }
                    else
                    {
                        values[(slice * xducers) + xducer] = value / 1000.0f;
                    }
                }
            }
            return values;
        }

        //Reads a contiguous set of int16s.
        // Array is laid out by transducer.  I.e., all readings for transducer 1, then xducer 2, then xducer3, etc..
        // Therefore, we need to skip around a bit to read chunks at a time.
        protected float[] ReadInt16DataArrayAsFloat(long arrayStartPosition, int offsetSlices, int numSlices, bool setZerosToNaNs)
        {
            int xducers = _header.NumberOfTransducers;
            float[] values = new float[numSlices * xducers];
            for (short xducer = 0; xducer < _header.NumberOfTransducers; xducer++)
            {
                long startPosition = arrayStartPosition + (offsetSlices * sizeof(short)) + (xducer * _header.NumberOfSlices * sizeof(short));
                _ebinReader.Seek(startPosition, SeekOrigin.Begin);
                for(int slice = 0; slice < numSlices; slice++)
                {
                    ushort value = _ebinReader.ReadUInt16();
                    if(value == 0 && setZerosToNaNs)
                    {
                        values[(slice * xducers) + xducer] = float.NaN;
                    }
                    else
                    {
                        values[(slice * xducers) + xducer] = Convert.ToSingle(value);
                    }
                }
            }
            return values;
        }

        //Reads a contiguous set of int16s.
        // Array is laid out by transducer.  I.e., all readings for transducer 1, then xducer 2, then xducer3, etc..
        // Therefore, we need to skip around a bit to read chunks at a time.
        protected short[] ReadInt16DataArray(long arrayStartPosition, int offsetSlices, int numSlices)
        {
            int xducers = _header.NumberOfTransducers;
            short[] values = new short[numSlices * xducers];
            for (short xducer = 0; xducer < _header.NumberOfTransducers; xducer++)
            {
                long startPosition = arrayStartPosition + (offsetSlices * sizeof(short)) + (xducer * _header.NumberOfSlices * sizeof(short));
                _ebinReader.Seek(startPosition, SeekOrigin.Begin);
                for(int slice = 0; slice < numSlices; slice++)
                {
                    short value = _ebinReader.ReadInt16();
                    values[(slice * xducers) + xducer] = value;
                }
            }
            return values;
        }

        protected short[] ReadInt16Array(int numValues)
        {
            long position = _ebinReader.BaseStream.Position;
            return ReadInt16Array(position, numValues);
        }

        protected short[] ReadInt16Array(long arrayStartPosition, int numValues)
        {
            short[] values = new short[numValues];
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            for(int i = 0; i < numValues; i++)
            {
                values[i] = _ebinReader.ReadInt16();
            }
            return values;
        }

        protected float[] ReadSingleArray(int numValues)
        {
            long position = _ebinReader.BaseStream.Position;
            return ReadSingleArray(position, numValues);
        }

        protected float[] ReadSingleArray(long arrayStartPosition, int numValues)
        {
            float[] values = new float[numValues];
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            for(int i = 0; i < numValues; i++)
            {
                values[i] = _ebinReader.ReadSingle();
            }
            return values;
        }

        protected uint[] ReadUInt32Array(int numValues)
        {
            long position = _ebinReader.BaseStream.Position;
            return ReadUInt32Array(position, numValues);
        }

        protected uint[] ReadUInt32Array(long arrayStartPosition, int numValues)
        {
            uint[] values = new uint[numValues];
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            for(int i = 0; i < numValues; i++)
            {
                values[i] = _ebinReader.ReadUInt32();
            }
            return values;
        }

        protected int[] ReadInt32Array(int numValues)
        {
            long position = _ebinReader.BaseStream.Position;
            return ReadInt32Array(position, numValues);
        }

        protected int[] ReadInt32Array(long arrayStartPosition, int numValues)
        {
            int[] values = new int[numValues];
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            for(int i = 0; i < numValues; i++)
            {
                values[i] = _ebinReader.ReadInt32();
            }
            return values;
        }

        protected byte[] ReadUInt8Array(long arrayStartPosition, int numValues)
        {
            byte[] values = new byte[numValues];
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            for(int i = 0; i < numValues; i++)
            {
                values[i] = _ebinReader.ReadUInt8();
            }
            return values;
        }

        protected string[] ReadStringArray(int numValuesExpected)
        {
            if(numValuesExpected < 1)
                return new string[0];

            return _ebinReader.ReadStringArray(0x0, 0x7);
        }

        protected string[] ReadStringArray()
        {
            return _ebinReader.ReadStringArray(0x0, 0x7);
        }

        protected string[] ReadStringArray(long arrayStartPosition, out long endPosition)
        {
            _ebinReader.Seek(arrayStartPosition, SeekOrigin.Begin);
            string[] values = _ebinReader.ReadStringArray(0x0, 0x7);
            endPosition = _ebinReader.BaseStream.Position;
            return values;
        }

        #endregion

        private void Initialize(Stream stream)
        {
            _baseStream = stream;
            _ebinReader = new EndianBinaryReader(new BigEndianBitConverter(), _baseStream);
            CanDeflate = false;
            CanDecrypt = false;
        }

        public override void Dispose()
        {
            if(_baseStream != null)
                _baseStream.Close();
        }

        #endregion
    }
}

#pragma warning restore 618