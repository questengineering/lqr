﻿// Encoded Streams
// (c) 2004 Richard Grimes 
// You are free to use this code in your own product but Richard Grimes is not 
// liable in anyway for the suitability of this code, nor for any issues resulting
// from running this code
//
// This code contains the Base64Stream class (c) 2004 Richard Grimes richard@richardgrimes.com

using System;
using System.IO;

namespace QuestReliability.IO
{
    // This class provides a stream interface over another stream. When you
    // read bytes through this stream it will read encoded bytes and return 
    // the decoded bytes. If you write to this stream it will encode the bytes
    // and write them to the wrapped stream
    public sealed class Base64Stream : Stream, IDisposable
    {
        // This is the data used to encode each 6-bit value
        static byte[] Base64EncodingTable = 
      {
         0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51,
         0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
         0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
         0x7A, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x2B, 0x2F 
      };

        const int LINE_LENGTH = 76;
        const int BYTE_PACKET_SIZE = 3;
        const int CHAR_PACKET_SIZE = 4;
        const int REFRESH_BUFFER = -1;
        byte[] crlf = new byte[] { 0x0D, 0x0A };
        enum ReadWrite : byte { Unknown, Read, Write };

        // This is the stream which receives the encoded data
        Stream wrappedStream;
        // This is temporary storage for encoded bytes before they are decoded
        byte[] encodedBuffer = new byte[CHAR_PACKET_SIZE];
        // This is temporary storage for the raw bytes before they are encoded
        byte[] decodedBuffer = new byte[BYTE_PACKET_SIZE];
        // This is the number of raw items in decodedBuffer. If it is -1 then there 
        // is no data in the buffer. Its is also used as the index where the next 
        // item is inserted from the binar file.
        int numItemsInDecodedBuffer = -1;
        // This is the current index in the read temporary buffer
        int positionInDecodedBuffer = 0;
        // This determines how the buffers are used, one you have made a choice
        // by calling Read() or Write() you cannot change it
        ReadWrite status = ReadWrite.Unknown;
        // Determine if the stream has been disposed
        bool bDisposed = false;
        // For a write stream the data is written out as strings of this length. If
        // it is zero then the output is written out as a single string.
        int lineLength = 0;
        // This is the number of bytes written/read
        int bytesReadWritten = 0;
        // If the stream is split in lines, this is the count of characters on the 
        // current line
        int curCharCount = 0;

        // ******************Constructors and Destructors************************
        // This stream *must* be based on another stream
        private Base64Stream() { }
        /// <summary>
        /// Initially it is not know whether we want to read from or write to this
        /// stream. Calling Read or Write methods will set the status.
        /// </summary>
        /// <param name="stream"></param>
        public Base64Stream(Stream stream)
        {
            wrappedStream = stream;
        }
        /// <summary>
        /// The user has specified that they want to read (read is true) or write
        /// to the buffer (read is false)
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="read"></param>
        public Base64Stream(Stream stream, bool read)
        {
            wrappedStream = stream;
            if (read) status = ReadWrite.Read;
            else status = ReadWrite.Write;
        }
        /// <summary>
        /// This is a write stream and the data must be written in lines of the
        /// length specified. If lineLen is zero then use the internet default for
        /// the line length.
        /// </summary>
        /// <param name="stream">stream to write to</param>
        /// <param name="lineLen">0 = use default; otherwise, specify line length</param>
        public Base64Stream(Stream stream, int lineLen)
        {
            wrappedStream = stream;
            status = ReadWrite.Write;
            if (lineLen == 0) lineLength = LINE_LENGTH;
            else lineLength = lineLen;
        }
        // Give the user the ability to Dispose the object
        public override void Close()
        {
            if (!bDisposed)
            {
                IDisposable i = this as IDisposable;
                i.Dispose();
            }
        }
        // Just in case the user forgets to dispose the object, make sure that
        // the stream is correctly cleaned up.
        ~Base64Stream()
        {
            Close();
        }

        // ******************Public Methods from IDisposable*********************
        // This will flush any data waiting to be written to the stream and
        // then it will close the stream. We override the base class version because
        // we want to set the bDisposed member.
        void IDisposable.Dispose()
        {
            if (!bDisposed)
            {
                Flush();
                wrappedStream.Close();
                wrappedStream = null;
                bDisposed = true;
            }
        }

        // ******************Public Methods from Stream**************************
        /// <summary>
        /// The stream can be read or write but not both. Reading from or writing to
        /// the stream will set status to indicate whether the stream is readonly or
        /// write only. If the stream has not been read/written then get the info from
        /// the wrapped stream. This may mean that subsequent access to these properties
        /// could give a different result.
        /// </summary>
        public override bool CanRead
        {
            get
            {
                if (status == ReadWrite.Unknown) return wrappedStream.CanRead;
                return status == ReadWrite.Read;
            }
        }
        /// <summary>
        /// The stream can be read or write but not both. Reading from or writing to
        /// the stream will set status to indicate whether the stream is readonly or
        /// write only. If the stream has not been read/written then get the info from
        /// the wrapped stream. This may mean that subsequent access to these properties
        /// could give a different result.
        /// </summary>
        public override bool CanWrite
        {
            get
            {
                if (status == ReadWrite.Unknown) return wrappedStream.CanWrite;
                return status == ReadWrite.Write;
            }
        }
        /// <summary>
        /// Don't allow the user to move the stream pointer because they may not
        /// move it to the right position
        /// </summary>
        public override bool CanSeek
        {
            get { return false; }
        }       
        /// <summary>
        /// The Length property only gives a true answer after all of the data has 
        /// been read or written. If you have not read or written any data then the 
        /// value returned is just an estimate.
        /// </summary>
        public override long Length
        {
            get
            {
                switch (status)
                {
                    case ReadWrite.Read:
                        // Data holds encoded data, so return the size of the decoded data
                        // If the input data has line breaks this size might be too big
                        if (bytesReadWritten == 0) return wrappedStream.Length * 3 / 4;
                        return bytesReadWritten;
                    case ReadWrite.Write:
                        // Data holds decoded data, so return the size of encoded data
                        if (bytesReadWritten == 0) return wrappedStream.Length * 4 / 3;
                        return bytesReadWritten;
                    default:
                        throw new NotSupportedException("Cannot seek in this stream");
                }
            }
        }
        /// <summary>
        /// This stream does not support seeking
        /// </summary>
        public override long Position
        {
            get { throw new NotSupportedException("Cannot seek in this stream"); }
            set { throw new NotSupportedException("Cannot seek in this stream"); }
        }
        /// <summary>
        /// Flush any remaining bytes to the stream. If the temporary buffer has
        /// bytes then by definition it means that we have to pad the output
        /// </summary>
        public override void Flush()
        {
            if (bDisposed)
                throw new ObjectDisposedException("stream has been disposed");
            if (numItemsInDecodedBuffer == REFRESH_BUFFER) return;
            switch (status)
            {
                case ReadWrite.Write:
                    // Flush any bytes to the output buffer
                    FlushEncodedBytes();
                    // Flush the bytes in the buffer to the stream. 
                    numItemsInDecodedBuffer = REFRESH_BUFFER;
                    break;
                case ReadWrite.Read:
                    // Position the stream to the point corresponding to where we have
                    // read items
                    if (wrappedStream.CanSeek)
                    {
                        // Take into account the last item read from the decoded buffer
                        wrappedStream.Position = wrappedStream.Position
                           - (decodedBuffer.Length - positionInDecodedBuffer);
                    }
                    break;
                default:
                    // Should not get here!
                    break;
            }
        }
        /// <summary>
        /// Write a buffer of bytes as Base64 encoded characters. This will first write any
        /// bytes left over from previous writes, then it writes as many bytes from the
        /// buffer as it can. Any bytes left over is saved and will be flushed later.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            if (bDisposed)
                throw new ObjectDisposedException("stream has been disposed");
            if (status == ReadWrite.Read)
                throw new InvalidOperationException("stream is read-only");
            if (status == ReadWrite.Unknown)
            {
                if (!wrappedStream.CanWrite)
                    throw new InvalidOperationException("wrapped stream is read-only");
                status = ReadWrite.Write;
            }
            if (offset + count > buffer.Length)
                throw new ArgumentException("offset and count point to beyond the end of buffer");

            // Iterate over all data, starting with the temp buffer
            int posSrc = offset;
            while (posSrc < offset + count)
            {
                WriteAsEncodedByte(buffer[posSrc++]);
            }
        }
        /// <summary>
        /// Read the specified number of Base64 encoded bytes from the stream and decode them into an 
        /// array of raw bytes.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            if (bDisposed)
                throw new ObjectDisposedException("stream has been disposed");
            if (status == ReadWrite.Write)
                throw new InvalidOperationException("stream is write-only");
            if (status == ReadWrite.Unknown)
            {
                if (!wrappedStream.CanRead)
                    throw new InvalidOperationException("wrapped stream is write-only");
                status = ReadWrite.Read;
            }

            int byteCount = 0;
            while (count > byteCount)
            {
                // Read a single byte from the cached decoded buffer
                int read = ReadDecodedByte();
                if (read == -1) break;
                buffer[offset + byteCount] = (byte)read;
                byteCount++;
                bytesReadWritten++;
            }

            return byteCount;
        }
        /// <summary>
        /// Seeking Not supported
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException("Cannot seek in this stream");
        }        
        /// <summary>
        /// Cannot change the size of the stream
        /// </summary>
        /// <param name="value"></param>
        public override void SetLength(long value)
        {
            throw new NotSupportedException("Cannot seek in this stream");
        }

        // Note that System.Stream provides versions of WriteByte and ReadByte that 
        // are adequate.
        // public override void WriteByte(byte value);
        // public override int ReadByte();

        // ******************Helper Methods**************************************
        // This reads a single byte from the stream and decodes it. The data is
        // read from the stream in small chunks and are stored in encodedBuffer,
        // this data is then decoded into the decodedBuffer. Each call to this
        // method will return the next byte from decodedBuffer until the buffer
        // is empty.
        int ReadDecodedByte()
        {
            // First test to see if the decodedBuffer is empty
            if (numItemsInDecodedBuffer == REFRESH_BUFFER)
            {
                // This is the 24-bit number read from the stream
                uint curPacket = 0;
                // This is the number of bits read
                int nBits = 0;
                // Loop reading until we have read enough bytes to get a 24-bit
                // value. If there are not enough bytes in the stream then 
                // pad with zero. Make sure that we ignore whitespace.
                while (true)
                {
                    int readChars = wrappedStream.Read(encodedBuffer, 0, (24 - nBits) / 6);
                    if (readChars == 0)
                    {
                        // End of File
                        break;
                    }
                    for (int i = 0; i < readChars; i++)
                    {
                        uint nCh = DecodeBase64Char(encodedBuffer[i]);
                        // 0xffff indicates that a whitespace character was passed
                        if (nCh == 0xffff) continue;
                        curPacket <<= 6;
                        curPacket |= nCh;
                        nBits += 6;
                    }
                    if (nBits == 24) break;
                }
                // curPacket has the data to write to the byte buffer left to right
                curPacket <<= 24 - nBits;
                // Note that there may not be exactly three bytes in curPacket
                numItemsInDecodedBuffer = nBits / 8;
                if (numItemsInDecodedBuffer == 0)
                {
                    // End of File, no more data
                    numItemsInDecodedBuffer = REFRESH_BUFFER;
                    return -1;
                }
                // Cache the decoded data
                for (int i = 0; i < numItemsInDecodedBuffer; i++)
                {
                    decodedBuffer[i] = (byte)((curPacket & 0x00ff0000) >> 16);
                    curPacket <<= 8;
                }
                positionInDecodedBuffer = 0;
            }

            // Extract the appropriate byte from the temporary buffer
            int byteVal = (int)decodedBuffer[positionInDecodedBuffer++];
            if (positionInDecodedBuffer >= numItemsInDecodedBuffer)
            {
                // Next time, read chars from the stream
                numItemsInDecodedBuffer = REFRESH_BUFFER;
            }
            return byteVal;
        }
        // This writes a single unencoded byte to the stream as encoded bytes. 
        // Encoding can only occur on groups of 3 bytes so the data is stored in
        // a temporary buffer, when this has been completely filled the buffer
        // is converted to encoded characters and written to the stream. Note
        // That it is important to Flush the stream to write bytes from the buffer
        // to the stream.
        void WriteAsEncodedByte(byte value)
        {
            if (numItemsInDecodedBuffer == REFRESH_BUFFER)
            {
                // The buffer has no entries, so indicate that the value should be 
                // placed in the first location.
                numItemsInDecodedBuffer = 0;
            }
            // Put the data in the temporary buffer
            decodedBuffer[numItemsInDecodedBuffer++] = value;
            if (numItemsInDecodedBuffer >= BYTE_PACKET_SIZE)
            {
                // We have filled the temporary buffer so convert and write to the
                // output stream. Since the buffer is full, we do not need to check
                // whether padding is necessary.
                WriteEncodedPacket(false);
                // This is reset to indicate that the data in the temporary buffer
                // is not longer valid
                numItemsInDecodedBuffer = REFRESH_BUFFER;
            }
        }
        // This converts the data in the temporary buffer to Base64 and writes it to 
        // the stream. If the parameter is true we will check to see if padding is
        // required and apply the approriate number of pad characters
        void WriteEncodedPacket(bool pad)
        {
            // Determine if there is data in the temporary buffer
            if (numItemsInDecodedBuffer > 0)
            {
                // This will eventually hold the 24-bit number
                uint curPacket = 0;
                // Partial packet, add zero bits to the right to create the full 24 bits
                for (int pktIdx = 0; pktIdx < BYTE_PACKET_SIZE; pktIdx++)
                {
                    if (pktIdx < numItemsInDecodedBuffer)
                        curPacket |= decodedBuffer[pktIdx];
                    curPacket <<= 8;
                }
                // Convert to encoded data, store the bytes in a temporary buffer
                byte[] bOutput = new byte[CHAR_PACKET_SIZE];
                int encIdx;
                for (encIdx = 0; encIdx <= numItemsInDecodedBuffer; encIdx++)
                {
                    byte b = (byte)(curPacket >> 26);
                    bOutput[encIdx] = Base64EncodingTable[b];
                    curPacket <<= 6;
                }

                if (pad)
                {
                    // See how many padding characters we need
                    int nPkts = (numItemsInDecodedBuffer == REFRESH_BUFFER) ?
                       0 : CHAR_PACKET_SIZE - numItemsInDecodedBuffer - 1;
                    for (int padIdx = 0; padIdx < nPkts; padIdx++)
                    {
                        bOutput[encIdx + padIdx] = (byte)'=';
                    }
                }

                // If lineLength is not zero it means that the user wants the
                // data split into lines. We then check to see if we have written
                // enough characters on the line and if so, add a newline.
                if (lineLength > 0 && curCharCount + bOutput.Length >= lineLength)
                {
                    // Add the standard RFC822 end line sequence CRLF
                    byte[] temp = new byte[bOutput.Length + 2];
                    int posEndLine = lineLength - curCharCount;
                    Array.Copy(bOutput, 0, temp, 0, posEndLine);
                    Array.Copy(crlf, 0, temp, posEndLine, crlf.Length);
                    Array.Copy(bOutput, posEndLine, temp, posEndLine + crlf.Length, bOutput.Length - posEndLine);
                    curCharCount = 0;
                    bOutput = temp;
                }
                else
                {
                    curCharCount += bOutput.Length;
                }

                // Finally write the data to the stream
                wrappedStream.Write(bOutput, 0, bOutput.Length);
            }
        }
        // If the internal buffer contains data that has not been written to the
        // output buffer, then flush those bytes
        void FlushEncodedBytes()
        {
            // If the temporary buffer has bytes then by definition it means 
            // that we have to pad the output
            if (numItemsInDecodedBuffer != REFRESH_BUFFER)
            {
                WriteEncodedPacket(true);
                numItemsInDecodedBuffer = REFRESH_BUFFER;
            }
        }
        // This does the initial decoding on a character, it returns the 6 bits
        // which should be combined with 18 other bits to make up the 24 bit (three
        // byte) binary data.
        static uint DecodeBase64Char(byte ch)
        {
            if (ch >= 0x41 && ch <= 0x5A)
                return (uint)(ch - 0x41 + 0);	   // 0 range starts at 'A' (0x41)
            if (ch >= 0x61 && ch <= 0x7A)
                return (uint)(ch - 0x61 + 26);	// 26 range starts at 'a' (0x61)
            if (ch >= 0x30 && ch <= 0x39)
                return (uint)(ch - 0x30 + 52);	// 52 range starts at '0' (0x30)
            if (ch == 0x2B) return (uint)62;
            if (ch == 0x2F) return (uint)63;
            // Check for whitespace or padding
            if (ch == 0x09 || ch == 0x20 || ch == 0x0A || ch == 0x0D || ch == 0x3D)
                return (uint)0xffff;
            throw new ArgumentException("invalid character in base64 string");
        }
    }
}