﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using HDF5DotNet;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.IO
{
    public class HDF5Helper
    {
        /// <summary>A cache of what types the scalars are in data files. </summary>
        protected static Dictionary<string, Type> ScalarDataTypes = new Dictionary<string, Type>();
        protected static List<Tuple<string, string, int>> ScalarReadingsPerSlice = new List<Tuple<string, string, int>>();

        protected static object SyncRoot = new object();

        public static T[] Unwind2DDataTo1D<T>(T[,] SomeData)
        {
            //Figure out the byte size of the generic type T (32 bits for float 32, etc.)
            Type dataType = typeof(T);
            int byteSizeOfType = Marshal.SizeOf(dataType);

            //Figure out the 1D length of the array
            int rowCount = SomeData.GetLength(0);
            int colCount = SomeData.GetLength(1);
            int newLength = rowCount * colCount;
            T[] oneD = new T[newLength];

            //Copy the memory over to the new array.
            Buffer.BlockCopy(SomeData, 0, oneD, 0, byteSizeOfType * newLength);
            return oneD;
        }

        public static T[,] Wind1DDataTo2D<T>(T[] inData, int numSensors)
        {
            //Figure out the byte size of the generic type T (32 bits for float 32, etc.)
            Type dataType = typeof(T);
            int byteSizeOfType = Marshal.SizeOf(dataType);

            //Figure out the 2D length of the array
            int rowCount = inData.Length / numSensors;
            int colCount = numSensors;
            T[,] twoD = new T[rowCount, colCount];

            //Copy the memory over to the new array.
            Buffer.BlockCopy(inData, 0, twoD, 0, byteSizeOfType * inData.Length);
            return twoD;
        }

        public static H5T.H5Type GetH5DataType(Type TheType)
        {
            if (TheType == typeof(double))
                return H5T.H5Type.NATIVE_DOUBLE;
            if (TheType == typeof(float))
                return H5T.H5Type.NATIVE_FLOAT;
            if (TheType == typeof(short))
                return H5T.H5Type.NATIVE_SHORT;
            if (TheType == typeof(int))
                return H5T.H5Type.NATIVE_INT;
            if (TheType == typeof(byte))
                return H5T.H5Type.NATIVE_UCHAR;
            throw new InvalidCastException("Could not convert " + TheType + " to H5 data type.");
        }

        /// <summary> Converts a H5DataTypeId to its Net equivalent (IE: int) </summary>
        public static Type GetNetTypeFromH5Type(H5DataTypeId TheType)
        {
            Type returnType = null;
            H5T.H5TClass theClass = H5T.getClass(TheType);
            var bytes = H5T.getSize(TheType);
            if (theClass == H5T.H5TClass.FLOAT && bytes == 4) returnType = typeof(float);
            if (theClass == H5T.H5TClass.FLOAT && bytes == 8) returnType = typeof(double);
            if (theClass == H5T.H5TClass.INTEGER && bytes == 2) returnType = typeof(short);
            if (theClass == H5T.H5TClass.INTEGER && bytes == 4) returnType = typeof(int);
            if (theClass == H5T.H5TClass.INTEGER && bytes == 8) returnType = typeof(long);
            if (theClass == H5T.H5TClass.INTEGER && bytes == 1) returnType = typeof(byte);
            if (returnType == null) throw new InvalidCastException("Could not convert " + TheType + " to Net data type.");
            return returnType;
            
        }

        /// <summary> Reads 1D or 2D Scalar Data between the indices assuming the data is in the base file and not a subgroup. </summary>
        public static IScalarArray ReadScalarData<T>(string dataFileFullName, Scalar scalar, int lowAxialIndex, int highAxialIndex, int lowSensorIndex, int highSensorIndex, int sliceStride = 1, string groupName = null)
            where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            lock (SyncRoot)
            {
                //Get information about the source data.
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5DataSetId dataSetId;

                //If the data was grouped somehow, open the group before attempting to read.
                if (!string.IsNullOrWhiteSpace(groupName))
                {
                    H5GroupId groupId = H5G.open(fileId, groupName);
                    dataSetId = H5D.open(groupId, scalar.ToString());
                }
                else { dataSetId = H5D.open(fileId, scalar.ToString()); }
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                if (highAxialIndex > dataExtents[0] - 1)
                {
                    throw new Exception(string.Format("Slice {0} is above max of {1}", highAxialIndex, dataExtents[0]));
                }

                long numPoints = (int)Math.Ceiling((highAxialIndex - lowAxialIndex + 1) / (double)sliceStride);
                long numCircIndices = highSensorIndex - lowSensorIndex + 1;

                //Allocate 2D memory to read into.
                long[] dims = { numPoints, numCircIndices };
                H5DataSpaceId memSpace = H5S.create_simple(2, dims);
                var dataArray = new T[numPoints, numCircIndices];

                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectStridedHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { lowAxialIndex, lowSensorIndex }, new long[] {sliceStride, 1}, new[] { numPoints, numCircIndices });

                //Read into the array.
                H5T.H5Type dataType = GetH5DataType(typeof(T));
                H5D.read(dataSetId, new H5DataTypeId(dataType), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<T>(dataArray));
                var data = new ScalarArray<T>(scalar.ToString(), Unwind2DDataTo1D(dataArray));
                data.SetDimensions((int)numPoints, (int)numCircIndices);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
                return data;
            }
        }

        /// <summary> Reads 1D or 2D Data between the indices assuming the data is in the base file and not a subgroup. </summary>
        public static void WriteData<T>(string dataFileFullName, Scalar scalar, int lowAxialIndex, int highAxialIndex, int lowSensorIndex, int highSensorIndex, ScalarArray<T> scalarData, string groupName = null)
            where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            lock (SyncRoot)
            {
                //Get information about the source data.
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5DataSetId dataSetId;

                //If the data was grouped somehow, open the group before attempting to read.
                if (!string.IsNullOrWhiteSpace(groupName))
                {
                    H5GroupId groupId = H5G.open(fileId, groupName);
                    dataSetId = H5D.open(groupId, scalar.ToString());
                }
                else { dataSetId = H5D.open(fileId, scalar.ToString()); }
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);

                //Figure out how much data we're going to write.
                long numPoints = highAxialIndex - lowAxialIndex + 1;
                long numCircIndices = highSensorIndex - lowSensorIndex + 1;
                long[] dims = { numPoints, numCircIndices };
                
                //Convert the scalar array into a 2D data array.
                T[,] dataArray = new T[numPoints, numCircIndices];
                for (int i = 0; i < numPoints; i++)
                {
                    for (int j = 0; j < numCircIndices; j++)
                    {
                        dataArray[i, j] = scalarData[(i * numCircIndices + j).ToInt32()];
                    }
                }
                
                //Allocate 2D memory to read into.
                H5DataSpaceId memSpace = H5S.create_simple(2, dims);
                
                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { lowAxialIndex, lowSensorIndex }, new[] { numPoints, numCircIndices });

                //Read into the array.
                H5T.H5Type dataType = GetH5DataType(typeof(T));
                H5D.write(dataSetId, new H5DataTypeId(dataType), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<T>(dataArray));
                
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
            }
        }

        public static void Write1DData(string dataFileFullName, Scalar s, int startSlice, int endSlice, List<float> data, string groupName = null)
        {
            lock (SyncRoot)
            {
                //Get information about the source data.
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5DataSetId dataSetId;

                //If the data was grouped somehow, open the group before attempting to read.
                if (!string.IsNullOrWhiteSpace(groupName))
                {
                    H5GroupId groupId = H5G.open(fileId, groupName);
                    dataSetId = H5D.open(groupId, s.ToString());
                }
                else { dataSetId = H5D.open(fileId, s.ToString()); }
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);

                //Figure out how much data we're going to write.
                long numPoints = endSlice - startSlice + 1;
                long[] dims = { numPoints, 1 };

                //Allocate 2D memory to read into.
                H5DataSpaceId memSpace = H5S.create_simple(2, dims);

                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { startSlice, endSlice }, new[] { numPoints, 1 });

                //Read into the array.
                H5T.H5Type dataType = GetH5DataType(typeof(float));
                H5D.write(dataSetId, new H5DataTypeId(dataType), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<float>(data.ToArray()));

                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
            }
        }

        public static Type GetScalarDataType(string dataFileFullName, Scalar scalar)
        {
            //If the type is already cached, just return it.
            lock (SyncRoot)
            {
                if (ScalarDataTypes.ContainsKey(scalar.ToString())) return ScalarDataTypes[scalar.ToString()];
            
                //Otherwise go read the data type from the file's metadata.
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5DataSetId dataSetId = H5D.open(fileId, scalar.ToString());
                Type dataType = GetNetTypeFromH5Type(H5D.getType(dataSetId));

                //Make sure to cache the result.
                ScalarDataTypes.Add(scalar.ToString(), dataType);
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
                return dataType;
            }
        }

        /// <summary> Gets how many sensors are in each slice of data for the given scalar. </summary>
        public static int GetDataSensorsPerSlice(string dataFileFullName, Scalar scalar)
        {
            lock (SyncRoot)
            {
                //If the file-specific type's sensor count is already cached, just return it.
                var cachedValue = ScalarReadingsPerSlice.FirstOrDefault(T => T.Item1 == dataFileFullName && T.Item2 == scalar.ToString());
                if (cachedValue != null) return cachedValue.Item3;

                //Otherwise go read the data type from the file's metadata.
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5DataSetId dataSetId = H5D.open(fileId, scalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                int numSensorsInSlice = dataExtents.Length > 1 ? dataExtents[1].ToInt32() : 1;

                //Make sure to cache the result.
                ScalarReadingsPerSlice.Add(new Tuple<string, string, int> (dataFileFullName, scalar.ToString(), numSensorsInSlice));
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
                return numSensorsInSlice;
            }
        }

        /// <summary> Gets the extents of data. minimum of Rank 2 (even if 1D data, the second dimension will be 1.) </summary>
        public static long[] GetDataDimensions(string dataFileFullName, Scalar scalar)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5DataSetId dataSetId = H5D.open(fileId, scalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                long[] returnDataExtents = { dataExtents[0], dataExtents.Length > 1 ? dataExtents[1] : 1 };
                
                H5D.close(dataSetId);
                H5F.close(fileId);
                H5.Close();
                return returnDataExtents;
            }
        }

    }
}
