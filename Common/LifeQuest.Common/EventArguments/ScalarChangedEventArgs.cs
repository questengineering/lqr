#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/16/2012 10:53:14 AM
// Created by:   C.Madigan
//
#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.EventArguments
{
    public delegate void ScalarChangedEventHandler(object sender, ScalarChangedEventArgs e);

    /// <summary>
    /// Event arguments to indicate the selected scalar has changed
    /// </summary>
    public class ScalarChangedEventArgs : EventArgs
    {
        private readonly string _scalarKey;

        public ScalarChangedEventArgs(string scalarKey)
        {
            _scalarKey = scalarKey;
        }

        /// <summary>
        /// Gets the scalar key.
        /// </summary>
        /// <value>The scalar key.</value>
        public string ScalarKey
        {
            get
            {
                return _scalarKey;
            }
        }
    }
}