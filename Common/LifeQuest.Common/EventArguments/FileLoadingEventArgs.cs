#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/16/2008 8:20:04 AM
// Created by:   j.rowe
//
#endregion

using System.ComponentModel;

namespace QuestIntegrity.LifeQuest.Common.EventArguments
{
    public delegate void FileLoadingEventHandler(object sender, FileLoadingEventArgs e);

    public class FileLoadingEventArgs : CancelEventArgs
    {
        private readonly string _fileName = "";

        // Constructor.
        public FileLoadingEventArgs(string fileName, bool cancel)
            : base(cancel)
        {
            _fileName = fileName;
        }

        public FileLoadingEventArgs(string fileName)
        {
            base.Cancel = false;
            _fileName = fileName;
        }

        // Properties.
        public string FileName
        {
            get
            {
                return _fileName;
            }
        }
    }
}