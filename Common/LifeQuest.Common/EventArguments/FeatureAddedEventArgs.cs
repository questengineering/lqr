#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/5/2008 9:37:40 AM
// Created by:   D.Revelle
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using QuestIntegrity.LifeQuest.Common.Data.Features;

namespace QuestIntegrity.LifeQuest.Common.EventArguments
{
     public delegate void FeatureAddedEventHandler(object sender, FeatureAddedEventArgs e);
    
    /// <summary>
    /// Event arguments to indicate that a feature has been added to the project (e.g., RSF, Features, Markers, etc).
    /// </summary>
    public class FeatureAddedEventArgs : EventArgs 
    {
        private readonly FeatureInfo _feature;

        #region Constructor

        public FeatureAddedEventArgs(FeatureInfo feature)
        {
            _feature = feature;
        }

        #endregion
        
        #region Properties.
        
        public FeatureInfo Feature
        {
            get
            {
                return _feature;
            }
        }

        #endregion
    }
}