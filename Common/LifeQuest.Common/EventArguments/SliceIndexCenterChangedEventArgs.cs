#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/8/2008 9:41:37 AM
// Created by:   D.Revelle
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.EventArguments
{
    public delegate void SliceIndexCenterChangedEventHandler(object sender,
                                                             SliceIndexCenterChangedEventArgs e);

    /// <summary>
    ///     Event arguments to indicate that the position along the pipeline has changed.
    /// </summary>
    public class SliceIndexCenterChangedEventArgs : EventArgs
    {
        private readonly int _sliceIndex;

        //Constructor.
        public SliceIndexCenterChangedEventArgs(int sliceIndex)
        {
            _sliceIndex = sliceIndex;
        }

        //Properties.
        public int SliceIndex
        {
            get
            {
                return _sliceIndex;
            }
        }
    }
}