﻿
using System;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    public class Scalar
    {
        //Example scalar, Axial Position.
        public static readonly Scalar AxialPosition = new Scalar("Axial Position");

        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public Scalar() {}
        protected Scalar(string displayName) { DisplayName = displayName; }

        public override string ToString() => DisplayName;
        
        public string DisplayName { get; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //These are utilized because XMLDeserialized versions of statics(like FeatureType.None) will not equality-compare with instantiated versions at runtime as expected.
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(object obj)
        {
            Scalar typedObj = obj as Scalar;
            if (typedObj == null) return false; // fails cast.
            return (string.Equals(DisplayName, typedObj.DisplayName));
        }

        protected bool Equals(Scalar other)
        {
            return string.Equals(DisplayName, other.DisplayName);
        }

        public override int GetHashCode()
        {
            return DisplayName?.GetHashCode() ?? 0;
        }

        public static bool operator ==(Scalar a, Scalar b)
        {
            if (ReferenceEquals(a, b)) return true; //Means the same instance, or both null.
            if ((object)a == null || (object)b == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(a.DisplayName, b.DisplayName);
        }

        public static bool operator !=(Scalar a, Scalar b)
        {
            return !(a == b);
        }

        #endregion Equality Comparing
    }
}
