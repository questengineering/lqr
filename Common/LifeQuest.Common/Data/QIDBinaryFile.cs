#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/30/2008 11:19:13 AM
// Created by:   j.rowe
//
#endregion

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace QuestIntegrity.LifeQuest.Data
{
    /// <summary>
    /// TODO: Summary goes here.
    /// </summary>
    public class QIDBinaryFile
    {
        #region Enums
        #endregion

        #region Constants
        #endregion

        #region Private/Protected Variables
        #endregion

        #region Constructors
        #endregion

        #region Public Properties
        public void Write()
        {
            PipeDataPreHeader pre = new PipeDataPreHeader();
            PipeDataHeader header = new PipeDataHeader();
            pre.FileTypeMarker = FileTypeMarker.QIDGeneration1;
            pre.VersionMajor = 1;
            pre.VersionMinor = 0;
            pre.VersionBuild = 0;
            header.EncryptionCompressionFlag = EncryptionCompressionFlag.Encrypted;
            header.ZMin = 0;
            header.ZMax = 125;
            header.ChecksumSHA1 = new byte[16];

            using (FileStream filestream = new FileStream(@"c:\temp\out.qid", FileMode.Create, FileAccess.ReadWrite))
            {
                using (BinaryWriter writer = new BinaryWriter(filestream))
                {
                    //write the header information
                    pre.ToBinaryWriter(writer);
                    header.ToBinaryWriter(writer);  //write this eventhough it isn't full.  We will overwrite later once offsets are known.                                       

                    //encrypt the main data package
                    long positionEncryptionStart = writer.BaseStream.Position;
                    byte[] key = CryptographyHelper.GeneratePasswordHashKey("123", 8);
                    byte[] IV = CryptographyHelper.GeneratePasswordHashKey("123", 8);
                    DES algorithm = DES.Create();                   
                    using (CryptoStream cryptoStream = new CryptoStream(writer.BaseStream, algorithm.CreateEncryptor(key, IV), CryptoStreamMode.Write))
                    {
                        string s = "thl;ajdf;lk askdjf;j a;ksdfjadjj; asdf asd f adsf as df adf as df asdf asdf a sdf adf a dsfadflk;aj a df adf adf";
                        byte[] b = ASCIIEncoding.UTF8.GetBytes(s);

                        cryptoStream.Write(b, 0, b.Length);
                        //header.ToBinaryWriter(writer);
                       // header.ToBinaryWriter(writer);
                       // header.ToBinaryWriter(writer);                    
                    }

                    //Calc checksum of the encrypted part of the file
                    writer.Seek(positionEncryptionStart, SeekOrigin.Begin);
                    header.ChecksumSHA1 = GetChecksum(writer.BaseStream);

                    //Seek to past the pre-header and write the header now that we have the data
                    writer.Seek(PipeDataPreHeaderSize+1, SeekOrigin.Begin);
                    header.ToBinaryWriter(writer);
                }
            }
        }

        public void Read()
        {
            PipeDataPreHeader pre = new PipeDataPreHeader();
            PipeDataHeader header = new PipeDataHeader();

            using (FileStream filestream = new FileStream(@"c:\temp\out.qid", FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(filestream))
                {
                    //Check Pre-header
                    pre = PipeDataPreHeader.FromBinaryReader(reader);
                    if (Enum.IsDefined(typeof(FileTypeMarker), pre.FileTypeMarker))
                    {
                        header = PipeDataHeader.FromBinaryReader(reader);
                        if ((header.EncryptionCompressionFlag & EncryptionCompressionFlag.Encrypted) == EncryptionCompressionFlag.Encrypted)
                        {
                            long encryptedStartPosition = reader.BaseStream.Position;
                            if(CompareByteArrays(header.ChecksumSHA1, GetChecksum(reader.BaseStream)))
                            {
                                //decrypt


                            }
                        }
                    }
                }
            }
        }

        private byte[] GetChecksum(Stream stream)
        {
            SHA1Managed sha1 = new SHA1Managed();
            return sha1.ComputeHash(stream);
        }

        public static bool CompareByteArrays(byte[] data1, byte[] data2)
        {
            // If both are null, they're equal
            if (data1 == null && data2 == null)
            {
                return true;
            }
            // If either but not both are null, they're not equal
            if (data1 == null || data2 == null)
            {
                return false;
            }
            if (data1.Length != data2.Length)
            {
                return false;
            }
            for (int i = 0; i < data1.Length; i++)
            {
                if (data1[i] != data2[i])
                {
                    return false;
                }
            }
            return true;
        }

        // header
        //   fileTypeMarker    (int)
        //   versionMajor      (int)
        //   versionMinor      (int)
        //   versionBuild      (int)
        //   encryptionCompFlag (int)
        //   zmin              (double)
        //   zmax              (double)
        //   checksumSHA1      (byte[20])
        //   numScalarArrays (int)
        //   scalarArrayName[]  (string[numScalarArrays])
        //   pipeDataOffset (int)
        //   2DcellOffset (int)
        //   3DcellOffset (int)
        //   scalarOffset[] (int[numScalarArrays])

        //>>Encrypt  
        ///* Points */
        //   arrayLength  (int)
        //   id (int), 3dx (double), 3dy (double), 2dx (double), z (double)
        //   id,3dx,3dy,2dx,z
        //   id,3dx,3dy,2dx,z
        //   id,3dx,3dy,2dx,z

        ///* 2D Connectivity */
        //   arrayLength (int)
        //   numPts (int), 
        //   pt[1]
        //   pt[2]
        //   pt[n-1]
        //   pt[n]

        ///* 3D Connectivity */
        //   arrayLength  (int)
        //   numPts (int), 
        //   pt[1]
        //   pt[2]
        //   pt[n-1]
        //   pt[n]

        ///* Scalar 1 */
        //   scalarIndex   (int)
        //   scalar1length (int)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)

        //   scalarIndex   (int)
        //   scalar2length (int)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)

        //   scalarIndex   (int)
        //   scalar3length (int)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)
        //   value (double)

        // <<End encrypt

        #endregion

        #region Public Methods
        //public Write()
        //{


        //}


        #endregion

    }
}
