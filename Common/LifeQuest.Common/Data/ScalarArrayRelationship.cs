﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/3/2008 10:31:49 AM
// Created by:   J.Rowe
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    /// Stores basic parent-child relationship information for ScalarArrays.  Useful for combo-boxes,
    ///  where knowing what child ScalarArrays exist for a given parent.
    ///  Typcially Parent is s 2D Array, Child is a 1D array.
    /// </summary>
    public class ScalarArrayRelationship
    {
        public string ParentKey { get; set; }
        public string Key { get; set; }
        public string DisplayName { get; set; }

        public ScalarArrayRelationship(string parentKey, string key, string displayName)
        {
            ParentKey = parentKey;
            Key = key;
            DisplayName = displayName;
        }
    }
}