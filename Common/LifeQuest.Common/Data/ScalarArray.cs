#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/3/2008 8:51:12 AM
// Created by:   J.Rowe
//

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;
using QuestIntegrity.LifeQuest.Common.Properties;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    ///     Wrapper for named array of scalar values.  Provides
    ///     basic statistics as well as a key and localized property for the list.
    ///     Class is generic with restrictions.
    ///     NOTE: min/max comparison are done after converting to doubles since T cannot do
    ///     comparisons of &lt;, &gt; etc.  Not sure how this will work for DateTime, for example.
    ///     Intention is numeric data:  int, float, single, double
    /// </summary>
    public class ScalarArray<T> : List<T>,
                                  IScalarArray where T : struct, IComparable<T>, IEquatable<T>, IConvertible
    {
        #region Private/Protected Variables

        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string _key;
        private T _minimum;
        private T _maximum;
        private int _realCount;
        private int _minimumID;
        private int _maximumID;
        private double _mean;
        private T _median;
        private double _variance;
        private double _stddev;
        private readonly uint[] _counts = new uint[100];
        private bool _statsUpToDate;
        private string _displayName = string.Empty;
        private readonly List<long> _indexMap = new List<long>();
        private int _jIndexCount = 1;

        #endregion

        #region Constructors

        private ScalarArray()
        {
            Persist = false; //Default to data not being saved to file
            InitializeProcessingOptions();
        }

        private ScalarArray(int capacity)
            : base(capacity)
        {
            InitializeProcessingOptions();
        }

        private ScalarArray(IEnumerable<T> values)
            : base(values)
        {
            UpdateStatistics();
            InitializeProcessingOptions();
        }

        public ScalarArray(string key,
                           int capacity)
            : base(capacity)
        {
            Key = key;
            DisplayName = GetDisplayName();
            InitializeProcessingOptions();
        }

        public ScalarArray(string key,
                           IEnumerable<T> values)
            : this(values)
        {
            Key = key;
            DisplayName = GetDisplayName();
        }

        public ScalarArray(string key,
                           T[] values)
            : this(values)
        {
            Key = key;
            DisplayName = GetDisplayName();
        }

        public ScalarArray(Scalar type,
                           IEnumerable<T> values)
            : this(values)
        {
            Key = type.Value;
            DisplayName = type.ToString();
        }

        public ScalarArray(string key,
                           string displayName)
            : this()
        {
            Key = key;
            DisplayName = displayName;
        }

        public ScalarArray(string key)
            : this()
        {
            Key = key;
            DisplayName = GetDisplayName();
        }

        public ScalarArray(Scalar type)
            : this()
        {
            Key = type.Value;
            DisplayName = type.ToString();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the <see cref="T" /> for the specified slice and sensor numbers.
        /// </summary>
        /// <value></value>
        public T this[int slice,
                      int sensor]
        {
            get
            {
                return this[slice * _jIndexCount + sensor];
            }
            set
            {
                this[slice * _jIndexCount + sensor] = value;
            }
        }

        /// <summary>
        ///     Gets or sets the processing options.
        /// </summary>
        /// <value>The processing options.</value>
        public ProcessingOptions ProcessingOptions { get; set; }

        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
                DisplayName = GetDisplayName();
            }
        }

        public string ParentKey { get; set; }

        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
            }
        }

        public TypeCode DataTypeCode
        {
            get
            {
                return Type.GetTypeCode(typeof(T));
            }
        }

        public Type DataType
        {
            get
            {
                return typeof(T);
            }
        }

        /// <summary>
        ///     Set to TRUE to force 1D stats to be saved with file data
        /// </summary>
        public bool Persist { get; set; }

        /// <summary>
        ///     Indicates if this scalar array has a mapping array of local index to previous global index
        /// </summary>
        public bool HasIndexMap { get; set; }

        public List<long> IndexMap
        {
            get
            {
                return _indexMap;
            }
        }

        /// <summary>
        ///     Count without NaNs and Infinities
        /// </summary>
        public int RealCount
        {
            get
            {
                UpdateStatistics();
                return _realCount;
            }
        }

        public T Minimum
        {
            get
            {
                UpdateStatistics();
                return _minimum;
            }
        }

        public T Maximum
        {
            get
            {
                UpdateStatistics();
                return _maximum;
            }
        }

        /// <summary>
        ///     Index of the minimum value occurrence
        /// </summary>
        public int IndexOfMinimum
        {
            get
            {
                UpdateStatistics();
                return _minimumID;
            }
        }

        /// <summary>
        ///     Index of the maximum value occurrence
        /// </summary>
        public int IndexOfMaximum
        {
            get
            {
                UpdateStatistics();
                return _maximumID;
            }
        }

        public double Mean
        {
            get
            {
                UpdateStatistics();
                return _mean;
            }
        }

        public T Median
        {
            get
            {
                UpdateStatistics();
                return _median;
            }
        }

        public double Variance
        {
            get
            {
                UpdateStatistics();
                return _variance;
            }
        }

        public double StandardDeviation
        {
            get
            {
                UpdateStatistics();
                return _stddev;
            }
        }

        public uint[] Counts
        {
            get
            {
                UpdateStatisticsWithCounts();
                return _counts;
            }
        }

        #endregion

        #region Public Methods

        public void SetDimensions(int iIndexCount,
                                  int jIndexCount)
        {
            _jIndexCount = jIndexCount;
        }

        /// <summary>
        ///     Creates a clone of the ScalarArray initializing as the same type
        /// </summary>
        /// <returns></returns>
        public IScalarArray Create()
        {
            IScalarArray newArray = new ScalarArray<T>(Key)
                                    {
                                        ProcessingOptions = ProcessingOptions
                                    };
            return newArray;
        }

        public T[] CreateArray()
        {
            return new T[Count];
        }

        /// <summary>
        ///     Convert the base list into an array of type T
        /// </summary>
        /// <returns>An array of type T.</returns>
        public new T[] ToArray()
        {
            return this.ToArray<T>();
        }

        public void AddRange(IEnumerable scalars)
        {
            if (scalars != null)
            {
                base.AddRange(scalars as IEnumerable<T>);
            }
        }

        public void InsertRange(int index,
                                IEnumerable scalars)
        {
            if (scalars != null)
            {
                base.InsertRange(index, scalars as IEnumerable<T>);
            }
        }

        /// <summary>
        ///     Returns a byte array of the value at a particular index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public byte[] GetBytes(int index)
        {
            switch (DataTypeCode)
            {
                case TypeCode.Single:
                    return BitConverter.GetBytes(Convert.ToSingle(this[index]));
                case TypeCode.Double:
                    return BitConverter.GetBytes(Convert.ToDouble(this[index]));
                case TypeCode.Int32:
                    return BitConverter.GetBytes(Convert.ToInt32(this[index]));
                case TypeCode.Int16:
                    return BitConverter.GetBytes(Convert.ToInt16(this[index]));
                case TypeCode.UInt32:
                    return BitConverter.GetBytes(Convert.ToUInt32(this[index]));
                case TypeCode.UInt16:
                    return BitConverter.GetBytes(Convert.ToUInt16(this[index]));
                case TypeCode.Decimal:
                    return BitConverter.GetBytes(Convert.ToDouble(this[index]));
                case TypeCode.Boolean:
                    return BitConverter.GetBytes(Convert.ToBoolean(this[index]));
                case TypeCode.Int64:
                    return BitConverter.GetBytes(Convert.ToInt64(this[index]));
                case TypeCode.UInt64:
                    return BitConverter.GetBytes(Convert.ToUInt64(this[index]));
                default:
                    return new byte[0];
            }
        }

        /// <summary>
        ///     Gets the value at location index
        /// </summary>
        /// <param name="index"></param>
        /// <returns>The value at location index.</returns>
        public T GetValueAt(int index)
        {
            return this[index];
        }

        /// <summary>
        ///     Sets the statistics for this data set.  Used when stats are already calculated and persisted
        ///     to disk so that recalculation is not needed.
        /// </summary>
        /// <param name="stats">The ScalarArray to copy data from</param>
        public void SetStatistics(ScalarArray<T> stats)
        {
            _minimumID = stats.IndexOfMinimum;
            _maximumID = stats.IndexOfMaximum;
            _minimum = stats.Minimum;
            _maximum = stats.Maximum;
            _mean = stats.Mean;
            _median = stats.Median;
            _realCount = stats.RealCount;
            _stddev = stats.StandardDeviation;
            _variance = stats.Variance;
            for (int i = 0; i < stats.Counts.Length; i++)
            {
                _counts[i] = stats.Counts[i];
            }
            _statsUpToDate = true;
#if DEBUG
            _log.DebugFormat("Set stats for ScalarArray {0} using supplied values.", Key);
#endif
        }

        /// <summary>
        ///     Sets the statistics for this data set.  Used when stats are already calculated and persisted
        ///     to disk so that recalculation is not needed.
        /// </summary>
        /// <param name="stats">The Descriptive Stats structure</param>
        public void SetStatistics(DescriptiveStatisticData<T> stats)
        {
            _minimumID = stats.MinID;
            _maximumID = stats.MaxID;
            _minimum = stats.Minimum;
            _maximum = stats.Maximum;
            _mean = stats.Mean;
            _median = stats.Median;
            _realCount = stats.RealCount;
            _stddev = stats.StandardDeviation;
            _variance = stats.Variance;

            for (int i = 0; i < Count; i++)
            {
                _counts[i] = stats.Counts[i];
            }

            _statsUpToDate = true;
#if DEBUG
            _log.DebugFormat("Set stats for ScalarArray {0} using supplied values.", Key);
#endif
        }

        public void UpdateStatisticsWithCounts(bool forceRecalc)
        {
            if (forceRecalc)
            {
                _statsUpToDate = false;
            }
            UpdateStatisticsWithCounts();
        }

        public void UpdateStatisticsWithCounts()
        {
            //exit if:
            //  stats are already calculated and
            //  _counts have been calculated or
            //  the type T is not of type short
            if ((_statsUpToDate && (_counts[0] + _counts[1] + _counts[2] + _counts[3] + _counts[4] + _counts[5] + _counts[6]) > 0) ||
                typeof(T) != typeof(short))
            {
                return;
            }

            double total = 0.0f;
            int realValueCount = 0;
            double mean = 0.0d;
            double s = 0.0d;
            bool hasStartingValues = false;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            List<T> newValues = new List<T>(Count);

            if (Count > 0)
            {
                _median = this[Count / 2]; //set the median; this will get overwritten with the true median later. But to handle case of ALL NaN data, we set now.
            }

            int valueCount = Count;
            for (int index = 0; index < valueCount; index++)
            {
                short val = Convert.ToInt16(this[index]);
                if (val > 0 &&
                    val < 100)
                {
                    _counts[val - 1]++; //does histogram of Grade
                }

                T value = this[index];

                double doubleValue = Convert.ToDouble(this[index]);

                //set starting values
                if (!hasStartingValues)
                {
                    _minimum = this[index]; //set a non-NaN starting value
                    _maximum = this[index]; //set a non-NaN starting value
                    hasStartingValues = true;
                }

                //Min and max
                if (value.CompareTo(_minimum) < 0)
                {
                    _minimum = value;
                    _minimumID = index;
                }
                if (value.CompareTo(_maximum) > 0)
                {
                    _maximum = value;
                    _maximumID = index;
                }

                //Variance calculation
                //Variance algorithm from: http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance; type III On-line algorithm
                double delta = doubleValue - mean;
                mean = mean + delta / (realValueCount + 1);
                s = s + delta * (doubleValue - mean);

                //totals for mean
                total += doubleValue;

                newValues.Add(value);
                realValueCount++;
            }
            _mean = total / (realValueCount <= 1 ? 1 : realValueCount); //Pulled outside of the loop to help with speed and debugging (DJR 4/20/10)
            _realCount = realValueCount;
            _variance = s / realValueCount;
            _stddev = Math.Sqrt(_variance);

            //calculate the median value (ignoring NaNs)
            if (realValueCount > 0)
            {
                newValues.Sort();
                _median = newValues[newValues.Count / 2];
                newValues.Clear();
            }
            _statsUpToDate = true;
        }

        /// <summary>
        ///     Calculate the Min, Max, and Average for the list.
        ///     NaN and Infinity values are ignored.
        /// </summary>
        public void UpdateStatistics(bool forceRecalc)
        {
            if (forceRecalc)
            {
                _statsUpToDate = false;
            }
            UpdateStatistics();
        }

        /// <summary>
        ///     Updates the min, max, mean, median, index-of-min, index-of-max of the ScalarArray.
        ///     Does not calculate Mode
        /// </summary>
        public void UpdateStatistics()
        {
            //exit if stats are already calculated
            if (_statsUpToDate)
            {
                return;
            }

            if (typeof(T) == typeof(short))
            {
                UpdateStatisticsWithCounts();
                return;
            }

            double total = 0.0f;
            int realValueCount = 0;
            double mean = 0.0d;
            double s = 0.0d;
            bool hasStartingValues = false;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            List<T> newValues = new List<T>(Count);

            if (Count > 0)
            {
                _median = this[Count / 2]; //set the median; this will get overwritten with the true median later. But to handle case of ALL NaN data, we set now.
            }

            int valueCount = Count;
            for (int index = 0; index < valueCount; index++)
            {
                T value = this[index];
                if (!IsNaN(value) &&
                    !IsInfinity(value)) //skip NaNs and Infinity values
                {
                    double doubleValue = Convert.ToDouble(this[index]);

                    //set starting values
                    if (!hasStartingValues)
                    {
                        _minimum = this[index]; //set a non-NaN starting value
                        _maximum = this[index]; //set a non-NaN starting value
                        hasStartingValues = true;
                    }

                    //Min and max
                    if (value.CompareTo(_minimum) < 0)
                    {
                        _minimum = value;
                        _minimumID = index;
                    }

                    if (value.CompareTo(_maximum) > 0)
                    {
                        _maximum = value;
                        _maximumID = index;
                    }

                    //Variance calculation
                    //Variance algorithm from: http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance; type III On-line algorithm
                    double delta = doubleValue - mean;
                    mean = mean + delta / (realValueCount + 1);
                    s = s + delta * (doubleValue - mean);

                    //totals for mean
                    total += doubleValue;

                    newValues.Add(value);
                    realValueCount++;
                }
            }
            _mean = total / (realValueCount <= 1 ? 1 : realValueCount); //Pulled outside of the loop to help with speed and debugging (DJR 4/20/10)
            _realCount = realValueCount;
            _variance = s / realValueCount;
            _stddev = Math.Sqrt(_variance);

            //calculate the median value (ignoring NaNs)
            if (realValueCount > 0)
            {
                newValues.Sort();
                _median = newValues[newValues.Count / 2];
                newValues.Clear();
            }

            _statsUpToDate = true;
        }

        public void Calc1DStats(StatisticalMeasures statsToSave, int numIndiciesPer1DValue, Dictionary<string, IScalarArray> existing1DScalars)
        {
            int num1DValues = Count / numIndiciesPer1DValue;
            List<DescriptiveStatistics<T>> axialStats = new List<DescriptiveStatistics<T>>(num1DValues);
            axialStats.Fill(null);
            
            //Calculate the 1D stats for the scalar            
            ParallelOptions options = new ParallelOptions { MaxDegreeOfParallelism = ProcessingOptions.MaxNumberOfThreads };
            Parallel.For(0, num1DValues, options, axialIndex =>
            {
                //Get all the radial readings at the axial position and calculate stats
                int rangeStart = axialIndex * numIndiciesPer1DValue;
                DescriptiveStatistics<T> axialSliceStats = new DescriptiveStatistics<T>(GetRange(rangeStart, numIndiciesPer1DValue));
                axialSliceStats.ClearData(); //release the underlying data to save memory.  We don't need this again since stats are already calucated

                //Save the stats into a local List
                lock (axialStats) { axialStats[axialIndex] = axialSliceStats; }
            });
            
            //Copy the stats from the local List<> to the 1D Arrays key'd by Scalar2D + Measure (e.g., "ThicknessWall_Minimum")
            //Basically, we need to transpose an array of the struct into a series of 1D arrays
            Parallel.ForEach(Enum.GetValues(typeof(StatisticalMeasures)).Cast<StatisticalMeasures>().ToList().Where(M => (statsToSave & M) == M), options, measure =>
            {
                
                //Create a new 1D ScalarArray<> if needed
                string key = string.Format("{0}_{1}", Key, measure); //(e.g., "ThicknessWall_Minimum") 

                //create the measure array
                IScalarArray newMeasureArray;
                if (measure == StatisticalMeasures.Count || measure == StatisticalMeasures.RealCount)
                    newMeasureArray = new ScalarArray<int>(key);
                else
                    newMeasureArray = Create();
                
                newMeasureArray.ParentKey = Key;
                newMeasureArray.ProcessingOptions = ProcessingOptions;

                lock (existing1DScalars)
                {
                    if (existing1DScalars.ContainsKey(key))
                    {
                        existing1DScalars[key].Clear();
                        existing1DScalars[key] = newMeasureArray;
                    }
                    else existing1DScalars.Add(key, newMeasureArray);
                }

                //exit to avoid looping if there is no data
                if (axialStats.Count == 0) return;

                //Populate the value
                switch (measure)
                {
                    case StatisticalMeasures.Minimum:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Minimum); }
                        break;
                    case StatisticalMeasures.Maximum:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Maximum); }
                        break;
                    case StatisticalMeasures.First:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].First); }
                        break;
                    case StatisticalMeasures.Last:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Last); }
                        break;
                    case StatisticalMeasures.Mean:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(ConvertToT(axialStats[axialIndex].Mean)); }
                        break;
                    case StatisticalMeasures.Median:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Median); }
                        break;
                    case StatisticalMeasures.Mode:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Mode); }
                        break;
                    case StatisticalMeasures.Variance:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Variance); }
                        break;
                    case StatisticalMeasures.StandardDeviation:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].StandardDeviation); }
                        break;
                    case StatisticalMeasures.RealCount:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].RealCount); }
                        break;
                    case StatisticalMeasures.Count:
                        for (int axialIndex = 0; axialIndex < num1DValues; axialIndex++)
                        { newMeasureArray.Add(axialStats[axialIndex].Count); }
                        break;
                }
            });
        }

        public ScalarArray<float> CastToFloat()
        {
            ScalarArray<float> castedScalar = new ScalarArray<float>
                                              {
                                                  Key = Key,
                                                  ParentKey = ParentKey,
                                                  DisplayName = DisplayName,
                                                  Persist = Persist
                                              };

            foreach (T value in this)
            {
                castedScalar.Add(Convert.ToSingle(value));
            }
            castedScalar.UpdateStatistics();
            return castedScalar;
        }

        /// <summary>
        ///     Parses a scalar key like "ThicknessWall_Minimum" into "Minimum"
        /// </summary>
        /// <param name="scalarName">The full scalar name.</param>
        /// <returns>The sub-scalar name if it can be parsed, otherwise the scalarName</returns>
        public static string ParseSubScalarName(string scalarName)
        {
            string subScalarName = scalarName;
            string[] parts = scalarName.Split(new[]
                                              {
                                                      '_'
                                              });
            if (parts.Length == 2)
            {
                subScalarName = parts[1];
            }
            return subScalarName;
        }

        #endregion

        #region Private Methods

        /// <summary> Creates processing options based off of the program options. </summary>
        private void InitializeProcessingOptions()
        {
            ProcessingOptions = new ProcessingOptions ( true, Environment.ProcessorCount, Settings.Default.MaximumMemoryMB , true);
        }

        private string GetDisplayName()
        {
            string lookup = string.Format("Scalar_{0}", Key);
            string displayName = Resources.ScalarArrayStrings.ResourceManager.GetString(lookup, Resources.ScalarArrayStrings.Culture);
            if (displayName == null)
            {
                //Try to parse to the sub-piece.  E.g., Scalar_ThicknessWall_Minimum => Scalar_Minimum.
                // Just a convenience to not store all the combinations in the resource file.
                string[] parts = lookup.Split(new[]
                                              {
                                                      '_'
                                              });
                if (parts.Length == 1)
                {
                    displayName = Key;
                }
                else if (parts.Length > 2)
                {
                    lookup = string.Format("Scalar_{0}", parts[2]);
                    displayName = Resources.ScalarArrayStrings.ResourceManager.GetString(lookup, Resources.ScalarArrayStrings.Culture);
                    if (displayName == null)
                    {
                        displayName = Key;
                    }
                    else
                    {
                        if (parts.Length > 3)
                        {
                            displayName += " " + parts[3];
                        }
                    }
                }
                else
                {
                    displayName = Key;
                }
            }
            return displayName;
        }

        /// <summary>
        ///     Generic DataProperty checking for NaN values
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsNaN(T value)
        {
            bool isNaN = false;
            if (typeof(T) == typeof(Single))
            {
                isNaN = float.IsNaN(Convert.ToSingle(value));
            }
            else if (typeof(T) == typeof(Double))
            {
                isNaN = double.IsNaN(Convert.ToDouble(value));
            }

            return isNaN;
        }

        /// <summary>
        ///     Generic DataProperty checking for NegativeInfinity values
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsInfinity(T value)
        {
            bool isInf = false;
            if (typeof(T) == typeof(Single))
            {
                isInf = float.IsInfinity(Convert.ToSingle(value));
            }
            else if (typeof(T) == typeof(Double))
            {
                isInf = double.IsInfinity(Convert.ToSingle(value));
            }
            return isInf;
        }

        private static T ConvertToT(IConvertible value)
        {
            //icky syntax, but need to call this to convert to the generic type (which implements IConvertible)
            return (T)Convert.ChangeType(value, typeof(T));
        }

        #endregion
    }
}