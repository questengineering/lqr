#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/17/2008 9:02:59 AM
// Created by:   j.rowe
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics;
using QuestIntegrity.Graphics.ColorScaleBar;
using QuestIntegrity.Graphics.Layout;
using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    public class GridBuilder
    {
        #region Private/Protected Variables

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //NOTE: vtk uses image textures, which as of 5.4 is limited to powers of 2 sizes. Don't think this applies anymore, but is a good number.
        public static readonly int TargetPointsPerGrid = (int)Math.Pow(2, 16); //2^17 = 131,072 points
        /// <summary> Keeps some vtk methods from causing multi-threaded issues by using a locking object </summary>
        private static readonly object SyncRoot = new object();

        #endregion

        #region Public Properties

        public ProcessingOptions ProcessingOptions { get; set; }
        
        #endregion

        #region Public Methods
        
        /// <summary> Creates an empty 3D tube grid. </summary>
        public static vtkStructuredGrid CreateEmptyTubeGrid(List<float> AxialPositions, int NumberOfRadialPoints, double Diameter, double WallThickness, bool ShowThickness)
        {
            Stopwatch timer = Stopwatch.StartNew();
            
            //3D Grid
            double[] ptTube = new double[3]; //x,y,z triplet for tube point
            int numberOfZs = ShowThickness ? 2 : 1;
            int[] dims = { AxialPositions.Count, NumberOfRadialPoints, numberOfZs }; //x,y,z dimensions

            vtkPoints tubePoints = new vtkPoints();
            tubePoints.SetDataTypeToDouble();
            tubePoints.SetNumberOfPoints(dims[0]*(dims[1] + 1)*dims[2]); //note: number of y points is +1 to wrap back to itself.

            // Fill order is Y-X-Z, so we will loop in that order as well to make managing the array IDs easier.
            for (int z = 0; z < dims[2]; z++)
            {
                double radius = Diameter / 2 + z * WallThickness;
                //Parallel loops don't work here. VTK makes weird grids, even if you object lock.
                for (int y = 0; y < dims[1]; y++)
                {
                    int pointId =  y * dims[0] + z * dims[0] * (dims[1] + 1); //this z's progress, plus any other z's progress added together.
                    double theta = 360.0d * (y / (double)dims[1]) * Math.PI / 180.0d;
                    ptTube[1] = radius * Math.Cos(theta);
                    ptTube[2] = radius * Math.Sin(theta);
                    for (int x = 0; x < dims[0]; x++)
                    {
                        ptTube[0] = AxialPositions[x];
                        tubePoints.SetPoint(pointId, ptTube[0], ptTube[1], ptTube[2]);
                        
                        //Add extra point for the the tube to wrap.  The point itself is the same as the first, but the index is +dims[0]*dims[1]
                        if (y == 0)
                            tubePoints.SetPoint(pointId + dims[0] * dims[1], ptTube[0], ptTube[1], ptTube[2]);
                        pointId++;
                    }
                }
            }

            //3D Tube Grid
            vtkStructuredGrid returnGrid = new vtkStructuredGrid();
            returnGrid.SetDimensions(dims[0], dims[1] + 1, dims[2]); //Note: y-dimension is +1 to wrap
            returnGrid.SetPoints(tubePoints);
            returnGrid.Update();

            Log.DebugFormat("Empty Grid created. Num dimensions: [{0}, {1}, {2}].  Elapsed time (ms)={3}", dims[0], dims[1], dims[2], timer.ElapsedMilliseconds);
            Log.DebugFormat("Grid Points Count: {0}", returnGrid.GetNumberOfPoints());
            return returnGrid;
        }

        
        #endregion

        #region Public Events

        
        #endregion

        #region Static Public Methods

        public static void AddScalarsToGrid(vtkStructuredGrid Grid, List<float> Scalars, int NumberOfZsToRepeat)
        {
            //Convert Scalars to a vtkFloatArray.
            vtkFloatArray formattedScalars = new vtkFloatArray();
            int numXs = Grid.GetDimensions()[0];
            int numYs = Grid.GetDimensions()[1];
            int numberOfValues = numXs * NumberOfZsToRepeat * numYs;
            formattedScalars.SetNumberOfValues(numberOfValues);

            for (int z = 0; z < NumberOfZsToRepeat; z++)
            {
                for (int i = 0; i < Scalars.Count; i++)
                {
                    formattedScalars.SetValue(i + Scalars.Count * z, Scalars[i]);
                }
            }

            formattedScalars.Modified();
            //Add the proper scalar structure to the grid.
            Grid.GetPointData().SetScalars(formattedScalars);
            Grid.Modified();

            if (Grid.GetPoints().GetNumberOfPoints() % formattedScalars.GetNumberOfTuples() != 0)
            {
                throw new Exception(string.Format("Grid points don't match Scalar points. GridPoints: {0}, ScalarPoints: {1}", Grid.GetPoints().GetNumberOfPoints(), formattedScalars.GetNumberOfTuples()));
            }
        }

        public static void AddScalarsToGrid(vtkStructuredGrid Grid, float[,] RepeatedData, int NumberOfZsToRepeat, string ScalarName)
        {
            //Convert Scalars to a vtkFloatArray.
            vtkFloatArray formattedScalars = new vtkFloatArray();
            formattedScalars.SetNumberOfValues(RepeatedData.GetLength(0) * RepeatedData.GetLength(1) * NumberOfZsToRepeat);
            int rows = RepeatedData.GetLength(0), cols = RepeatedData.GetLength(1), totalValues = rows * cols;

            for (int z = 0; z < NumberOfZsToRepeat; z++)
            {
                for (int y = 0; y < cols; y++)
                {
                    int startIdx = y * rows;
                    for (int x = 0; x < rows; x++)
                    {
                        formattedScalars.SetValue(startIdx + x + totalValues * z, RepeatedData[x,y]);
                    }
                }
            }

            formattedScalars.Modified();
            formattedScalars.SetName(ScalarName);
            //Add the proper scalar structure to the grid.
            Grid.GetPointData().SetScalars(formattedScalars);
            Grid.Modified();

            if (Grid.GetPoints().GetNumberOfPoints() % formattedScalars.GetNumberOfTuples() != 0)
            {
                Debug.Print("NumberofGridPoints: {0}, ScalarPoints: {1}", Grid.GetPoints().GetNumberOfPoints(), formattedScalars.GetNumberOfTuples());
                throw new Exception("Grid points don't match Scalar points. 3D view will not appear");
            }

        }

        /// <summary> Create ImageGrid sections for a section of data.  Images will only be created for scalars passed, and will recycle any existingSections possible. </summary>
        public static List<GridSection> CreateGridSections(InspectionFile inspection, Range<int> sliceRange , List<Scalar> scalars, List<GridSection> existingSections = null)
        {
            List<GridSection> gridSections = new List<GridSection>();
            int numSensors = inspection.NumRadialReadings;
            int numSlices = sliceRange.Max - sliceRange.Min + 1;
            int maxSlicesPerSection = (int)Math.Ceiling((double)TargetPointsPerGrid / numSensors);
            int numSections = (int)Math.Ceiling(numSlices / (double)maxSlicesPerSection);
            
            
            ParallelOptions options = new ParallelOptions { MaxDegreeOfParallelism = inspection.ProcessingOptions.MaxNumberOfThreads };
            Parallel.For(0, numSections, options, sectionNum =>
            //for (int sectionNum = 0; sectionNum < numSections; sectionNum++) //non-parallel version, slower.
            {
                //Get the color scale bars to map through from the inspection file's parent. Make a new copy for each thread so they're thread safe.
                Dictionary<Scalar, ColorScaleBar> colorMaps = scalars.ToDictionary(s => s, s => ColorScaleBar.GetNewCopy(inspection.Parent.ColorScale, s.ToString()));
                int padXAfter = sectionNum == numSections - 1 ? 0 : 1; //slices to add after each middle grid section (used to prevent gap in rendering)
                int sectionStartIdx = sliceRange.Min + maxSlicesPerSection * sectionNum;
                int sectionEndIdx = Math.Min(sectionStartIdx + maxSlicesPerSection + padXAfter, sliceRange.Max);
                Range<int> sectionRange = new Range<int>(sectionStartIdx, sectionEndIdx);
                GridSection thisSection = null;
                //If we can recycle a grid, let's do it. Otherwise, build it. Checks if there is a similar-sized grid with all of the requested scalars available.
                if (existingSections != null) thisSection = existingSections.FirstOrDefault(grid => grid.IsRecyclable(sectionStartIdx, sectionEndIdx, scalars.ConvertAll(s => s.ToString())));
                if (thisSection == null) thisSection = CreateGridSection(inspection, colorMaps, sectionRange, numSensors);
                lock (gridSections) {gridSections.Add(thisSection);}
            });
            return gridSections;
         }

        /// <summary> Threadsafe method (can be done in parallel using the same colormap objects) that creates a single grid section from a raw array of 2D or 1D Data. </summary>
        public static vtkImageActor CreateImageFromScalarArray(ScalarArray<float> data, ColorScaleBar colorMap, int numSensors  )
        {
            //Create the 2D Image data.
            vtkImageData grid2D;
            int numSlices = data.Count / numSensors;
            lock (SyncRoot)
            {
                grid2D = new vtkImageData();
                grid2D.SetDimensions(numSlices, numSensors, 1);
                grid2D.SetSpacing(1, 1, 1);
                grid2D.SetScalarTypeToUnsignedChar();
                grid2D.SetNumberOfScalarComponents(3);
            }
            
            //Create an RGB array to hold color data
            vtkUnsignedCharArray array;
            lock (SyncRoot)
            {
                array = new vtkUnsignedCharArray();
                array.SetNumberOfComponents(3);
                array.SetName(data.Key);
                array.SetNumberOfTuples(data.Count);
            }
            
            //Map the values to colors
            IntPtr[] colors = colorMap.MapValues(data.ToArray());

            //Place mapped values in the proper location.
            for (int sensor = 0; sensor < numSensors; sensor++) // Y
            {
                for (int slice = 0; slice < numSlices; slice++) // X
                {
                    int vtkId = slice + (sensor * numSlices);
                    int dataId = slice * numSensors + sensor;
                    array.InsertTupleValue(vtkId, colors[dataId]);
                }
            }

            lock (SyncRoot)
            {
                grid2D.GetPointData().AddArray(array);
                grid2D.GetPointData().SetActiveScalars(data.Key);
            }

            vtkImageActor newActor = new vtkImageActor();
            newActor.SetInput(grid2D);

            return newActor;
        }

        /// <summary> Threadsafe method (can be done in parallel using the same colormap objects) that creates a single grid section. </summary>
        private static GridSection CreateGridSection(InspectionFile insp, Dictionary<Scalar, ColorScaleBar> colorMaps, Range<int> axialRange, int numSensors)
        {
            GridSection newsection = new GridSection { StartingSliceIdx = axialRange.Min, EndingSliceIdx = axialRange.Max };
            int slicesInSection = axialRange.Max - axialRange.Min + 1;

            //Create the 2D Image data.
            vtkImageData grid2D;
            lock (SyncRoot)
            {
                grid2D = new vtkImageData();
                grid2D.SetDimensions(slicesInSection, numSensors, 1);
                grid2D.SetSpacing(1, 1, 1);
                grid2D.SetScalarTypeToUnsignedChar();
                grid2D.SetNumberOfScalarComponents(3);
            }
            foreach (Scalar scalar in colorMaps.Keys)
            {
                //Create an RGB array to hold color data
                vtkUnsignedCharArray array;
                lock (SyncRoot)
                {
                    array = new vtkUnsignedCharArray();
                    array.SetNumberOfComponents(3);
                    array.SetName(scalar.ToString());
                    array.SetNumberOfTuples(slicesInSection * numSensors);
                }
                //Read the scalar data that will get mapped
                ScalarArray<float> scalarData = insp.ReadScalarData<float>(scalar, axialRange.Min, axialRange.Max, 0, numSensors - 1);
                IntPtr[] colors = colorMaps[scalar].MapValues(scalarData.ToArray());

                //Place mapped values in the proper location.
                for (int sensor = 0; sensor < numSensors; sensor++) // Y
                {
                    for (int slice = 0; slice < slicesInSection; slice++) // X
                    {
                        int vtkId = slice + (sensor * slicesInSection);
                        int dataId = slice * numSensors + sensor;
                        array.InsertTupleValue(vtkId, colors[dataId]);
                    }
                }

                lock (SyncRoot)
                {
                    grid2D.GetPointData().AddArray(array);
                    grid2D.GetPointData().SetActiveScalars(scalar.ToString());
                    newsection.Grids[scalar.ToString()] = grid2D;
                }
            }

            return newsection;
        }

        /// <summary> Creates an outline of the given range in 2D and 3D. If sensorRange has a higher min than max, then it will assume the item is wrapped. </summary>
        public static vtkActor CreateOutline(Range<double> axialRange, Range<int> sensorRange, Color OutlineColor, bool TwoD, double OuterDiameterFor3D = 1, int numRadialReadingsFor3D = 0)
        {
            const int xPadding = 2;
            double startxPos = axialRange.Min - xPadding, endxPos = axialRange.Max + xPadding;
           
            const int yPadding = 2;
            int startyIdx = sensorRange.Min - yPadding;
            int endyIdx = sensorRange.Max + yPadding;
            bool wraps = startyIdx > endyIdx;
            
            vtkPoints points = new vtkPoints();
            vtkCellArray cells = new vtkCellArray();
            vtkPolyLine outline = new vtkPolyLine();
            points.SetDataTypeToDouble();

            if (TwoD)
            {
                vtkPolyLine optionalOutline = null;
                if (!wraps) // A single cell can represent it
                {
                    points.InsertNextPoint(startxPos, startyIdx, 1);
                    points.InsertNextPoint(startxPos, endyIdx, 1);
                    points.InsertNextPoint(endxPos, endyIdx, 1);
                    points.InsertNextPoint(endxPos, startyIdx, 1);
                    points.InsertNextPoint(startxPos, startyIdx, 1);
                    outline.GetPointIds().SetNumberOfIds(points.GetNumberOfPoints());
                    for (int i = 0; i < points.GetNumberOfPoints(); i++) { outline.GetPointIds().SetId(i, i); }
                }
                else //We will need 2 cells to represent this beast.
                {
                    points.InsertNextPoint(startxPos, endyIdx, 1);
                    points.InsertNextPoint(startxPos, double.MaxValue, 1); //max value chosen as some number of sensors that is above the top of the inspection no matter what.
                    points.InsertNextPoint(endxPos, double.MaxValue, 1);
                    points.InsertNextPoint(endxPos, endyIdx, 1);
                    points.InsertNextPoint(startxPos, endyIdx, 1);
                    int numberOfPointsAfter1 = points.GetNumberOfPoints().ToInt32(); //required cast for x64
                    outline.GetPointIds().SetNumberOfIds(numberOfPointsAfter1);
                    for (int i = 0; i < numberOfPointsAfter1; i++) { outline.GetPointIds().SetId(i, i); }

                    points.InsertNextPoint(startxPos, -yPadding, 1);
                    points.InsertNextPoint(startxPos, startyIdx, 1);
                    points.InsertNextPoint(endxPos, startyIdx, 1);
                    points.InsertNextPoint(endxPos, -yPadding, 1);
                    points.InsertNextPoint(startxPos, -yPadding, 1);
                    optionalOutline = new vtkPolyLine();
                    optionalOutline.GetPointIds().SetNumberOfIds(numberOfPointsAfter1); //assumes first and second cell have the same amount of points, which should be safe.
                    for (int i = numberOfPointsAfter1; i < points.GetNumberOfPoints(); i++) { optionalOutline.GetPointIds().SetId(i - numberOfPointsAfter1, i); }
                }

                cells.InsertNextCell(outline);
                if (optionalOutline != null) cells.InsertNextCell(optionalOutline);
                cells.SetNumberOfCells(2);
            }
            else
            {
                int maxY = endyIdx;
                int minY = startyIdx;

                List<int> yOrder = new List<int>();
                if (!wraps)
                    for (int i = minY; i < maxY; i++) yOrder.Add(i);
                else
                    for (int i = maxY; i < maxY + numRadialReadingsFor3D - (maxY - minY); i++) yOrder.Add(i);

                double radius = (OuterDiameterFor3D / 2) * 1.05; //Add a bit of padding so it shows outside of the OD by a little bit.
                //add points counting up for Y at first, and down for Y at second to give a solid line.
                foreach (int yIdx in yOrder)
                {
                    double theta = 180 - 360d * (yIdx / (double)numRadialReadingsFor3D); //Assumes the data starts at the bottom of the tube (180) and writes initially towards the +Z direction (-angle).
                    double yValue = radius * Math.Cos(theta * Math.PI / 180d);
                    double zValue = radius * Math.Sin(theta * Math.PI / 180d);
                    points.InsertNextPoint(startxPos, yValue, zValue);
                }
                for (int i = yOrder.Count - 1; i >= 0; i--)
                {
                    double theta = 180 - 360d * (yOrder[i] / (double)numRadialReadingsFor3D);
                    double yValue = radius * Math.Cos(theta * Math.PI / 180d);
                    double zValue = radius * Math.Sin(theta * Math.PI / 180d);
                    points.InsertNextPoint(endxPos, yValue, zValue);
                }

                outline.GetPointIds().SetNumberOfIds(points.GetNumberOfPoints() + 1);
                for (int i = 0; i < points.GetNumberOfPoints(); i++) { outline.GetPointIds().SetId(i, i); }
                outline.GetPointIds().SetId(points.GetNumberOfPoints(), 0); //add the first point to the end so the line wraps back around.
                cells.InsertNextCell(outline);
            }

            vtkPolyData polyData = new vtkPolyData();
            polyData.SetPoints(points);
            polyData.SetLines(cells);

            vtkPolyDataMapper dataMapper = vtkPolyDataMapper.New();
            dataMapper.SetInput(polyData);

            vtkActor actor = new vtkActor();
            actor.SetMapper(dataMapper);
            actor.GetProperty().SetColor(OutlineColor);
            actor.GetProperty().SetLineWidth(1);

            return actor;
        }
        #endregion Static Public Methods

    }
}