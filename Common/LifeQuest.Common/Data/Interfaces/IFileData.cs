#region Copyright Quest Integrity Group, LLC 2012
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/19/2008 10:42:59 AM
// Created by:   D.Revelle
//
// Filename: $Id: IFileData.cs,v 1.1 2012/01/03 18:59:12 J.Rowe Exp $
#endregion

namespace QuestIntegrity.LifeQuest.Common.Data.Interfaces
{
    /// <summary>
    /// Basic interface for file data
    /// </summary>
    public interface IFileData
    {
        #region Public Properties

        int NumRadialReadings
        {
            get;
        }

        int NumAxialSections
        {
            get;
        }

        float SizeInMB
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        void Clear();

        #endregion
    }
}