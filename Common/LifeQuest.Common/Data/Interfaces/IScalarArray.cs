﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/3/2008 8:51:12 AM
// Created by:   J.Rowe
//
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    /// Interface of the non type-specific methods of ScalarArray(of T). For use
    /// in collections holding multiple ScalarArrays of different type T.
    /// </summary>
    public interface IScalarArray : IList
    {
        #region Properties

        string Key
        {
            get;
            set;
        }

        string ParentKey
        {
            get;
            set;
        }

        string DisplayName
        {
            get;
            set;
        }

        TypeCode DataTypeCode
        {
            get;
        }

        Type DataType
        {
            get;
        }

        bool Persist
        {
            get;
            set;
        }

        int IndexOfMinimum
        {
            get;
        }

        int IndexOfMaximum
        {
            get;
        }

        int RealCount
        {
            get;
        }

        double Mean
        {
            get;
        }

        double StandardDeviation
        {
            get;
        }

        double Variance
        {
            get;
        }

        uint[] Counts
        {
            get;
        }

        ProcessingOptions ProcessingOptions
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a clone of the ScalarArray initializing as the same type
        /// </summary>
        /// <returns></returns>
        IScalarArray Create();
        void AddRange(IEnumerable scalars);
        void InsertRange(int index, IEnumerable collection);
        void RemoveRange(int index, int count);
        void UpdateStatisticsWithCounts(bool forceRecalc);
        void UpdateStatisticsWithCounts();
        void UpdateStatistics(bool forceRecalc);
        void UpdateStatistics();
        void Calc1DStats(StatisticalMeasures statsToSave, int numIndiciesPerValue, Dictionary<string, IScalarArray> existing1DScalars);
        byte[] GetBytes(int index);
        void SetDimensions(int iIndexCount, int jIndexCount);

        #endregion
    }
}