﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Extensions.Progress;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.LifeQuest.Common.Data.DataCache;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;
using QuestIntegrity.LifeQuest.Common.IO;

namespace QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo
{
    public static class InspectionFileDataExt
    {
        /// <summary> Reads 1D Data, such as position, for a subset of data. Optionally loads data straight out of the file and skips filling/readingFrom the cache (useful for single point requests in the UI)</summary>
        public static ScalarArray<T> Read1DData<T>(this InspectionFile insp, Scalar TheScalar, int startAxialIndex, int endAxialIndex, bool cacheData = true) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            return ReadScalarData<T>(insp, TheScalar, startAxialIndex, endAxialIndex, 0, 0, cacheData);
        }

        /// <summary> Reads Scalar Data for a subset of data. Optionally loads data straight out of the file and skips filling/readingFrom the cache (useful for single point requests in the UI) </summary>
        public static ScalarArray<T> ReadScalarData<T>(this InspectionFile insp, Scalar TheScalar, int startAxialIndex, int endAxialIndex, int startCircIndex, int endCircIndex, bool cacheData = true) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            //Grab the scalar data from the data file directly and skip loading an entire file data's worth if requested to not use the caching system.
            if (!cacheData)
            {
                return (ScalarArray<T>)HDF5Helper.ReadScalarData<T>(insp.DataFileFullName, TheScalar, startAxialIndex, endAxialIndex, startCircIndex, endCircIndex);
            }

            int numSlicesRequested = (endAxialIndex - startAxialIndex + 1); //Add 1 because it's inclusive.
            int numReadingsRequested = (endCircIndex - startCircIndex + 1);
            int totalValues = numSlicesRequested * numReadingsRequested; 
            T[] tmpArray = new T[totalValues];
            
            int numReadings = HDF5Helper.GetDataSensorsPerSlice(insp.DataFileFullName, TheScalar);
            List<FileDataInfo> overlappingFiles = insp.FileDataInfos.Where(F => F.Overlaps(startAxialIndex, endAxialIndex)).ToList();
            Parallel.ForEach(overlappingFiles, F =>
            {
                //Figure out the local low/high axial index to get.
                int localStart = Math.Max(startAxialIndex - F.LowIndex, 0);
                int localEnd = Math.Min(endAxialIndex - F.LowIndex, F.NumSections - 1);
                
                using (new FileDataLocker(F, new [] {TheScalar}, 0))
                {
                    ScalarArray<T> fileScalars = F.FileData.GetScalars2D<T>(TheScalar.ToString());
                    for (int fileSlice = localStart; fileSlice <= localEnd; fileSlice++ )
                    {
                        int returnArraySliceIdx = F.LowIndex + fileSlice - startAxialIndex;
                        for (int sensor = startCircIndex; sensor <= endCircIndex; sensor++)
                        {
                            int returnArrayGlobalIdx = returnArraySliceIdx * numReadingsRequested + sensor - startCircIndex;
                            tmpArray[returnArrayGlobalIdx] = fileScalars[fileSlice * numReadings + sensor];
                        }
                    }
                }
            });
            var array = new ScalarArray<T>(TheScalar.ToString(), tmpArray);
            array.SetDimensions(numSlicesRequested, numReadingsRequested);
            return array;
        }

        /// <summary> Reads Scalar Data for a subset of data including a stride (every x values, including first value). Optionally loads data straight out of the file and skips filling/readingFrom the cache (useful for single point requests in the UI) </summary>
        public static ScalarArray<T> ReadScalarData<T>(this InspectionFile insp, Scalar TheScalar, int startAxialIndex, int endAxialIndex, int startCircIndex, int endCircIndex, int sliceStride, bool cacheData = true) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            //Grab the scalar data from the data file directly and skip loading an entire file data's worth if requested to not use the caching system.
            if (!cacheData)
            {
                 return (ScalarArray<T>)HDF5Helper.ReadScalarData<T>(insp.DataFileFullName, TheScalar, startAxialIndex, endAxialIndex, startCircIndex, endCircIndex, sliceStride);
            }

            //Figure out what slices are going to be read in.
            int numTotalReadings = HDF5Helper.GetDataSensorsPerSlice(insp.DataFileFullName, TheScalar);
            int numReadingsRequested = endCircIndex - startCircIndex + 1;
            List<int> slicesToRead = new List<int>();
            for (int i = 0; i < endAxialIndex - startAxialIndex + 1; i++)
            {
                if (i % sliceStride == 0)
                    slicesToRead.Add(startAxialIndex + i);
            }

            int totalValues = (slicesToRead.Count) * (numReadingsRequested); //Add 1 because it's inclusive.
            T[] tmpArray = new T[totalValues];
            
            Parallel.ForEach(slicesToRead, (slice, State, arrayIdx)  =>
            {
                //Figure out which FileInfo this slice resides in.
                FileDataInfo fileDataInfo = insp.FileDataInfos.FirstOrDefault(F => slice.Between(F.LowIndex, F.HighIndex));
                if (fileDataInfo == null) return; //shouldn't ever happen, but just in case.

                //Cherry pick the slices we want from the cache
                int localSlice = startAxialIndex - fileDataInfo.LowIndex;
                
                using (new FileDataLocker(fileDataInfo, new[] { TheScalar }, 0))
                {
                    IScalarArray fileScalars = fileDataInfo.FileData.ExtractScalars2D(TheScalar.ToString(), localSlice, localSlice, 0, numTotalReadings);
                    for (int sensorIdx = startCircIndex; sensorIdx <= endCircIndex; sensorIdx++ )
                        tmpArray[arrayIdx * numTotalReadings + sensorIdx] = (T)Convert.ChangeType(fileScalars[sensorIdx], typeof(T));
                }
            });
            var array = new ScalarArray<T>(TheScalar.ToString(), tmpArray);
            array.SetDimensions(slicesToRead.Count, numReadingsRequested);
            return array;
        }

        /// <summary> Gets statistical data for all measures passed in utilizing all sensors over an axial distance. </summary>
        public static Dictionary<string, ScalarArray<float>> GetScalarStats(this InspectionFile insp, Scalar TheScalar, StatisticalMeasures stats, int startAxialIndex, int endAxialIndex)
        {
            return insp.GetScalarStats(TheScalar, stats, startAxialIndex, endAxialIndex, 0, insp.NumRadialReadings - 1);
        }

        /// <summary> Gets statistical data for all measures passed in utilizing all sensors inclusively between those specified. </summary>
        public static Dictionary<string, ScalarArray<float>> GetScalarStats(this InspectionFile insp, Scalar theScalar, StatisticalMeasures stats, int startAxialIndex, int endAxialIndex, int startCircIndex, int endCircIndex, bool useCache = true)
        {
            //Read the 2D scalar data
            ScalarArray<float> rawData = insp.ReadScalarData<float>(theScalar, startAxialIndex, endAxialIndex, startCircIndex, endCircIndex,  useCache);
            
            //Calc the 1D Stats
            var rawResults = new Dictionary<string, IScalarArray>();
            rawData.Calc1DStats(stats, endCircIndex - startCircIndex + 1, rawResults);
            
            //Convert the stats into a dictionary
            return rawResults.ToDictionary(result => result.Key, result => new ScalarArray<float>(result.Key, from ValueType v in result.Value select v.ToSingle()));
        }

        /// <summary> Clears out all FileDataInfos and cleans the cache, fully resetting everything related to the inspection before re-registering it. </summary>
        public static void UpdateFileDataInfos(this InspectionFile insp, IProgress<ProgressStatus> p)
        {
            //Clean out the old data completely.
            FileCacheManager.Instance.DeRegister(insp);
            insp.FileDataInfos.ForEach(F => F.UnloadData(true));
            insp.FileDataInfos.Clear();

            //Create the new file data infos in a parallel loop
            int slicesPerFileData = 2000;
            int numFileDataInfos = (int)Math.Ceiling(insp.MaximumIndex / (double)slicesPerFileData);
            for (int fileNum = 0; fileNum < numFileDataInfos; fileNum++)
            {
                FileDataInfo newFileDataInfo = new FileDataInfo { LowIndex = fileNum * slicesPerFileData };
                newFileDataInfo.HighIndex = Math.Min(newFileDataInfo.LowIndex + slicesPerFileData - 1, insp.MaximumIndex); //don't go over the number of total slices in the inspection.
                newFileDataInfo.Name = string.Format("FDI-{0}", fileNum);
                newFileDataInfo.NumRadialReadings = insp.NumRadialReadings;
                newFileDataInfo.Parent = insp;
                insp.FileDataInfos.Add(newFileDataInfo);
                if (p != null) p.Report("Updated: " + newFileDataInfo.Name, (int)Math.Round(fileNum / (double)numFileDataInfos * 100));
            }

            //Register the refreshed inspection in the cache
            FileCacheManager.Instance.Register(insp);
            if (p != null) p.Report("Finished updating FDIs.", 100);
        }

        public static int[] GetSliceAndSensorFromGlobalID(this InspectionFile Insp, long globalId)
        {
            long sensor = 0, slice = 0;
            if (Insp.NumRadialReadings != 0)
                slice = Math.DivRem(globalId, Insp.NumRadialReadings, out sensor);
            return new[] { (int)slice, (int)sensor }; //sensor and slice will be ints, globalID will be long.
        }

        /// <summary> Calculates the global ID from given information. If it's out of bounds, returns -1; </summary>
        public static long GetGlobalID(this InspectionFile Insp, int slice, int sensorIdx)
        {
            if (slice < Insp.MinimumIndex || slice > Insp.MaximumIndex || sensorIdx > Insp.NumRadialReadings - 1 || sensorIdx < 0)
                return -1;
            return Insp.NumRadialReadings * (long)slice + sensorIdx;
        }

        /// <summary> Multithreaded method that reads chunks of 2D data, scales it, and then writes it back into the datafile. Does not touch any objects that might reference the data or scales associated with it. </summary>
        public static void ScaleAllData(this InspectionFile Insp, Scalar scalar, double scaleFactor, IProgress<ProgressStatus> pt)
        {
            //Split up the data into 10000slice chunks for parallel processing.
            const int numPointsPerChunk = 10000;
            int numDataChunks = (int)Math.Ceiling(Insp.MaximumIndex / (double)numPointsPerChunk);

            double progress = 0;
            Parallel.For(0, numDataChunks, chunkNum =>
            {
                int startIndex = chunkNum * numPointsPerChunk;
                int endIndex = Math.Min(startIndex + numPointsPerChunk, Insp.MaximumIndex); //Don't go past the end.
                
                //Read this data chunk.
                ScalarArray<float> data = Insp.ReadScalarData<float>(scalar, startIndex, endIndex, 0, Insp.NumRadialReadings - 1);

                //Scale every data point.
                for (int i = 0; i < data.Count(); i++) { data[i] *= (float)scaleFactor; }

                HDF5Helper.WriteData(Insp.DataFileFullName, scalar, startIndex, endIndex, 0, Insp.NumRadialReadings - 1, data);
                progress += 1d / numDataChunks * 100;
                pt.Report(String.Format("Scaled {0} slices.", endIndex - startIndex + 1), (int)Math.Round(progress));
            });
                
            
        }

        /// <summary> Multithreaded method that reads chunks of 2D data, scales it, and then writes it back into the datafile. Does not touch any objects that might reference the data or scales associated with it. </summary>
        public static void ScaleData(this InspectionFile Insp, Scalar scalar, double scaleFactor, Range<int> slices, IProgress<ProgressStatus> pt)
        {
            //Split up the data into 10000slice chunks for parallel processing.
            const int numPointsPerChunk = 10000;
            int numDataChunks = (int)Math.Ceiling((slices.Max - slices.Min) / (double)numPointsPerChunk);

            double progress = 0;
            Parallel.For(0, numDataChunks, chunkNum =>
            {
                int startIndex = slices.Min + chunkNum * numPointsPerChunk;
                int endIndex = Math.Min(startIndex + numPointsPerChunk, Insp.MaximumIndex); //Don't go past the end.

                //Read this data chunk.
                ScalarArray<float> data = Insp.ReadScalarData<float>(scalar, startIndex, endIndex, 0, Insp.NumRadialReadings - 1);

                //Scale every data point.
                for (int i = 0; i < data.Count(); i++) { data[i] *= (float)scaleFactor; }

                HDF5Helper.WriteData(Insp.DataFileFullName, scalar, startIndex, endIndex, 0, Insp.NumRadialReadings - 1, data);
                progress += 1d / numDataChunks * 100;
                pt.Report(String.Format("Scaled {0} slices.", endIndex - startIndex + 1), (int)Math.Round(progress));
            });
        }
    }
}
