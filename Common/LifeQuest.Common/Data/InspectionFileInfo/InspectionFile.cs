#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/18/2008 3:17:32 PM
// Created by:   d.revelle
//
// CVS ID Tag - do not change
// Filename: $Id: InspectionFileInfo.cs,v 1.420 2012/01/19 03:09:10 C.Turiano Exp $
//

#endregion Copyright Quest Integrity Group, LLC 2012

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Data.DataCache;
using QuestIntegrity.LifeQuest.Common.Properties;

namespace QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo
{
    /// <summary>
    ///     Class for containing QIG File Information
    ///     QIG File Info is information about a particular inspection.
    ///     There is one QIG File per inspection.
    /// </summary>
    [Serializable]
    public class InspectionFile : IEqualityComparer<InspectionFile>
    {
        #region Private/Protected Variables

        private const UnitSystem.UnitSystems DefaultUnitSystem = UnitSystem.UnitSystems.Lbs_in_psi;
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string _dataFileName;

        #endregion Private/Protected Variables

        #region Constructors

        [Obsolete("Do not use paramaterless constructore. Only exists for XML Serialization.", true)]
        protected InspectionFile() { }

        public InspectionFile(string dataFileFullName)
        {
            //Set Default properties
            Units = new UnitSystem(DefaultUnitSystem); //Set a default unit system
            DataFileName = Path.GetFileName(dataFileFullName);
            FileDataInfos = new List<FileDataInfo>();
            ProcessingOptions = new ProcessingOptions ( true, Environment.ProcessorCount, Settings.Default.MaximumMemoryMB, true );
            GUID = Guid.NewGuid();
        }

        #endregion Constructors

        #region Public Properties

        public virtual string DataFileName
        {
            get
            {
                if (new FileInfo(Path.Combine(DirectoryPath, _dataFileName)).Exists) return _dataFileName;
                string secondPath = Path.Combine(DirectoryPath, Path.GetFileNameWithoutExtension(Parent.FileName) + ".qidh");
                if (new FileInfo(secondPath).Exists) return secondPath;
                Log.Info("Cannot find data file in project directory even though its address was looked up: " + _dataFileName);
                return _dataFileName;
            }
            set { _dataFileName = value; }
        }
        public string DataFileFullName { get { return Path.Combine(DirectoryPath, DataFileName); } }
        public const string DefaultExtension = "qig";
        public Guid GUID { get; set; }
        public DateTime DateInspected { get; set; }
        
        public List<FileDataInfo> FileDataInfos { get; set; }
        
            /// <summary>  Gets or sets the processing options. </summary>
        [XmlIgnore]
        public ProcessingOptions ProcessingOptions { get; set; }

        /// <summary>   Project currently using this inspection file.  Set on loading of the InspectionFile. </summary>
        [XmlIgnore] 
        public ProjectInfo Parent { get; protected set; }

        /// <summary> The inspection name without the file extension. </summary>
        public string Name{get; set;}

        /// <summary> Path to the parent project file </summary>
        public string DirectoryPath{get{return Parent != null ? Parent.DirectoryPath : null;}}

        /// <summary>   Unit System in which all the data are stored. </summary>
        public UnitSystem Units { get; set; }

        /// <summary>  Indicates if data in the InspectionFile or child DataFiles have changed (i.e., their IsDirty flag is set to true). </summary>
        [XmlIgnore]
        public bool IsDirty { get; set; }

        [XmlIgnore] 
        public GridBuilder GridBuilder = new GridBuilder();

        public int MinimumIndex { get; set; }

        public int MaximumIndex { get; set; }

        public long GlobalIDMinimum { get { return 0; } }
        public long GlobalIDMaximum { get { return MaximumIndex * NumRadialReadings; } }
        
        public int NumRadialReadings { get; set; }
        
        #endregion Public Properties

        #region Public Methods
        
        
        /// <summary>
        ///     Given a starting and ending index, returns a list of the indices between those locations.  Takes
        ///     into account the clockwise/anti-clockwise direction, and whether the end points should be included
        ///     or not.
        /// </summary>
       public List<int> GetRadialIndicies(int MinCircIndex, int MaxCircIndex, bool IncludeTopEdge, bool IncludeBottomEdge)
        {
            //***********************************************************************************
            //* NOTE: this is very finicky.  Please do not edit w/o purpose and make sure all
            //*       cases are covered.
            //* JTR 5/27/2010
            //***********************************************************************************

            //Determine the radial indices that are included in the set
            bool wraps = MinCircIndex >= MaxCircIndex;
            int min = MinCircIndex;
            int max = MaxCircIndex;
            //Swap top and bottom inclusion flags
            if (wraps) //If it's possible to be non-clockwise, that logic should be here.
            {
                bool temp = IncludeTopEdge;
                IncludeTopEdge = IncludeBottomEdge;
                IncludeBottomEdge = temp;
            }

            int numRadialReadings = NumRadialReadings;

            //Store list of radial indices in an int array for looping
            List<int> radialIndices = new List<int>(numRadialReadings);
            for (int i = 0; i <= numRadialReadings; i++)
            {
                if (wraps)
                {
                    if ((IncludeBottomEdge && i >= min) || (IncludeTopEdge && i <= max))
                        radialIndices.Add(i);
                    else if ((IncludeBottomEdge && i >= min) || (!IncludeTopEdge && i < max))
                        radialIndices.Add(i);
                    else if ((!IncludeBottomEdge && i > min) || (IncludeTopEdge && i <= max))
                        radialIndices.Add(i);
                    else if ((!IncludeBottomEdge && i > min) || (!IncludeTopEdge && i < max))
                        radialIndices.Add(i);
                }
                else
                {
                    if ((IncludeTopEdge && i >= min) && (IncludeBottomEdge && i <= max))
                        radialIndices.Add(i);
                    else if ((IncludeTopEdge && i >= min) && (!IncludeBottomEdge && i < max))
                        radialIndices.Add(i);
                    else if ((!IncludeTopEdge && i > min) && (IncludeBottomEdge && i <= max))
                        radialIndices.Add(i);
                    else if ((!IncludeTopEdge && i > min) && (!IncludeBottomEdge && i < max))
                        radialIndices.Add(i);
                }
            }

            //Correct for index numRadialReadings + 1 as the final value.  This is the same as "0" and should
            // be corrected if 0 doesn't already exist
            if (radialIndices[radialIndices.Count - 1] == numRadialReadings)
            {
                if (radialIndices[0] != 0)
                    radialIndices[radialIndices.Count - 1] = 0;
                else
                    radialIndices.RemoveAt(radialIndices.Count - 1);
            }

            radialIndices.TrimExcess();
            return radialIndices;
        }

        /// <summary> Determines whether the specified GlobalID is valid. </summary>
        public bool IsValidGlobalID(long GlobalID)
        {
            return (GlobalID >= 0 && GlobalID <= GlobalIDMaximum);
        }

        /// <summary>
        ///      Returns a global slice index based on a GlobalID point. If global ID is outside of range, it will return -1 unless the constrainToValidIndex flag is set to true.
        /// </summary>
        public int GlobalSliceIndex(long GlobalPointID, bool ConstrainToValidIndex = false)
        {
            int globalSliceIndex;
            if (ConstrainToValidIndex && (GlobalPointID < GlobalIDMinimum))
                globalSliceIndex = MinimumIndex;
            else if (ConstrainToValidIndex && (GlobalPointID > GlobalIDMaximum))
                globalSliceIndex = MaximumIndex;
            else
                globalSliceIndex = (int)Math.Floor(GlobalPointID / (decimal)NumRadialReadings);
            
            return globalSliceIndex;
        }

        #endregion Public Methods

   

        #region IEqualityComparer<InspectionFileInfo> Members

        public bool Equals(InspectionFile X, InspectionFile Y)
        {
            if (X == null && Y == null) return true;
            
            if (X != null && X.Name == Y.Name)
                return true;
            
            return false;
        }

        public int GetHashCode(InspectionFile Obj)
        {
            return Obj.Name.ToLower().GetHashCode();
        }

        #endregion IEqualityComparer<InspectionFileInfo> Members

        
    }
}