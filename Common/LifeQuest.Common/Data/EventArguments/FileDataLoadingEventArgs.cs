﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//

#endregion

using System.ComponentModel;

namespace QuestIntegrity.LifeQuest.Common.Data.EventArguments
{
    public delegate void FileDataLoadingEventHandler(object sender,
                                                     FileDataLoadingEventArgs e);

    /// <summary>
    ///     TODO: Summary goes here.
    /// </summary>
    public class FileDataLoadingEventArgs : CancelEventArgs
    {
        #region Private Properties

        private readonly FileDataInfo _fileDataInfo;
        private readonly int _lockPriority;

        #endregion

        #region Constructor

        public FileDataLoadingEventArgs(FileDataInfo fileDataInfo,
                                        int lockPriority = 0,
                                        bool cancel = false)
            : base(cancel)
        {
            _fileDataInfo = fileDataInfo;
            _lockPriority = lockPriority;
        }

        #endregion

        #region Public Properties

        public FileDataInfo LoadedFileDataInfo
        {
            get
            {
                return _fileDataInfo;
            }
        }

        /// <summary>
        ///     Tells the file cache manager that loading or unloading of this file should be counted against
        ///     the lock count for the file in the cache.
        /// </summary>
        public int LockPriority
        {
            get
            {
                return _lockPriority;
            }
        }

        #endregion
    }
}