﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//

#endregion

namespace QuestIntegrity.LifeQuest.Common.Data.EventArguments
{
    public delegate void FileDataUnloadedEventHandler(object sender,
                                                      FileDataUnloadedEventArgs e);

    /// <summary>
    ///     Information about the FileDataInfo that has been loaded and whether a "lock" that is held should be removed.
    /// </summary>
    public class FileDataUnloadedEventArgs : System.EventArgs
    {
        #region Private Properties

        private readonly FileDataInfo _fileDataInfo;
        private readonly bool _unlockFile;

        #endregion

        #region Constructors

        public FileDataUnloadedEventArgs(FileDataInfo fileDataInfo)
        {
            _fileDataInfo = fileDataInfo;
            _unlockFile = false;
        }

        public FileDataUnloadedEventArgs(FileDataInfo fileDataInfo,
                                         bool forceUnlockFile)
        {
            _fileDataInfo = fileDataInfo;
            _unlockFile = forceUnlockFile;
        }

        #endregion

        #region Public Properties

        public FileDataInfo LoadedFileDataInfo
        {
            get
            {
                return _fileDataInfo;
            }
        }

        public bool UnlockFile
        {
            get
            {
                return _unlockFile;
            }
        }

        #endregion
    }
}