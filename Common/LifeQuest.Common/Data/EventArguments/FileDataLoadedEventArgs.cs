﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//

#endregion

namespace QuestIntegrity.LifeQuest.Common.Data.EventArguments
{
    public delegate void FileDataLoadedEventHandler(object sender,
                                                    FileDataLoadedEventArgs e);

    /// <summary>
    ///     Information about the FileDataInfo that has been loaded and whether a "lock" is requested for that file.
    /// </summary>
    public class FileDataLoadedEventArgs : System.EventArgs
    {
        #region Private Properties

        private readonly FileDataInfo _fileDataInfo;
        private readonly int _lockPriority;

        #endregion

        #region Constructor

        public FileDataLoadedEventArgs(FileDataInfo fileDataInfo,
                                       int lockPriority = 0)
        {
            _fileDataInfo = fileDataInfo;
            _lockPriority = lockPriority;
        }

        #endregion

        #region Properties

        public FileDataInfo LoadedFileDataInfo
        {
            get
            {
                return _fileDataInfo;
            }
        }

        public int LockPriority
        {
            get
            {
                return _lockPriority;
            }
        }

        #endregion
    }
}