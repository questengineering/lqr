#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 13-Oct-10 8:32:25 AM
// Created by:   J.Rowe
//
// File:  $Id: LicenseType.cs,v 1.1 2010/10/13 15:11:29 J.Rowe Exp $

#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    ///     License types
    /// </summary>
    [Flags]
    public enum LicenseType
    {
        Default = 0,
        Viewer = 1,
        Analyst = 2
    }
}