﻿#region Copyright Quest Integrity Group, LLC 2012

// www.QuestIntegrity.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: WindowManager.cs,v 1.528 2012/08/06 19:36:15 C.Turiano Exp $

#endregion Copyright Quest Integrity Group, LLC 2012

using System.ComponentModel;
using QuestIntegrity.LifeQuest.Common.Data;

namespace QuestIntegrity.LifeQuest.Data
{
    [TypeConverter(typeof(LocalizedEnumConverter))]
    public enum ProjectStatus
    {
        Unprocessed_Data = 1,
        Feature_Picking_Complete = 2,
        Analysis_Complete = 3,
        Analysis_Reviewed = 4,
        Assessment_Complete = 5,
        Assessment_Reviewed = 6
    }
}