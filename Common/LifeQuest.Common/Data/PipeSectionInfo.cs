#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/24/2008 11:45:32 AM
// Created by:   d.revelle
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion Copyright Quest Integrity Group, LLC 2012

using System;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Classes;
using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;
using QuestIntegrity.LifeQuest.Common.Engineering;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary> Contains Location and information about nominal pipe sizes along the pipeline </summary>
    [Serializable]
    public class PipeSectionInfo
    {
        private InspectionFile _parentInspection;
        /// <summary> The way that this pipe section links itself to its parent even after XML Serialization. Inspection names are assumed unique. </summary>
        public Guid ParentInspectionGuid;
        #region Constructors

        [Obsolete("Do not use paramaterless constructor, it only exists for XML Serialization", true)]
        public PipeSectionInfo() { }

        public PipeSectionInfo(InspectionFile parentInspectionFile)
        {
            ID = Guid.NewGuid();
            ParentInspectionGuid = parentInspectionFile.GUID;
        }

        #endregion Constructors

        #region Public Properties

        /// <summary> Gets or sets the a property value with the specified property name. </summary>
        [XmlIgnore]
        public object this[string PropertyName]
        {
            get
            {
                return this.GetPropertyValue(PropertyName);
            }
            set
            {
                this.SetPropertyValue(PropertyName, value);
            }
        }

        public PipeSize Size { get; set; }
        public Guid ID { get; set; }

        public virtual string Name { get; set; }

        /// <summary> The base-typed parent inspection file. </summary>
        [XmlIgnore]
        public virtual InspectionFile Inspection 
        { 
            get { return _parentInspection ?? (_parentInspection = GetParentInspection()); }
        }
        #endregion Public Properties

        #region Public Methods

        public override string ToString()
        {
            return Name;
        }

        protected virtual InspectionFile GetParentInspection()
        {
            throw new Exception("Pipe section could not find its parent inspection.");
        }
        #endregion Public Methods

    }
    
}