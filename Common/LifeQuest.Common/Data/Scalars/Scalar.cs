﻿
using System;

namespace QuestIntegrity.LifeQuest.Common.Data.Scalars
{
    public class Scalar
    {
        public static readonly Scalar AxialPosition = new Scalar("AxialPosition");

        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public Scalar() {}
        protected Scalar(string TheValue) { Value = TheValue;}

        public override string ToString()
        {
            string returnValue = ScalarDisplayNames.ResourceManager.GetString(Value);
            return returnValue ?? @"Scalar Name Not In Resource File";
        }

        public string Value { get; set; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //These are utilized because XMLDeserialized versions of statics(like FeatureType.None) will not equality-compare with instantiated versions at runtime as expected.
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(System.Object obj)
        {
            if (obj == null) return false; // fails existance
            Scalar typedObj = obj as Scalar;
            if (typedObj == null) return false; // fails cast.
            return (string.Equals(Value, typedObj.Value));
        }

        protected bool Equals(Scalar other)
        {
            return string.Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        public static bool operator ==(Scalar A, Scalar B)
        {
            if (ReferenceEquals(A, B)) return true; //Means the same instance, or both null.
            if ((object)A == null || (object)B == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(A.Value, B.Value);
        }

        public static bool operator !=(Scalar A, Scalar B)
        {
            return !(A == B);
        }

        #endregion Equality Comparing
    }
}
