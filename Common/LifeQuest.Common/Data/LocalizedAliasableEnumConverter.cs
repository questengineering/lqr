#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/3/2008 10:31:49 AM
// Created by:   J.Rowe
//
#endregion

#region File ColumnHeader
//
// Code taken from CodeProject.com
// http://www.codeproject.com/KB/cs/LocalizingEnums.aspx
//
// COPYRIGHT:   Copyright 2007 
//              Infralution
//
#endregion

using System;
using QuestIntegrity.Core.Extensions.Enum;
using QuestIntegrity.Core.Extensions.Resources;
using QuestIntegrity.LifeQuest.Common.Data.Resources;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    /// Defines a type converter for enum types defined in this project
    /// </summary>
    class LocalizedAliasableEnumConverter : AliasableResourceEnumConverter
    {
        /// <summary>
        /// Create a new instance of the converter using translations from the given resource manager
        /// </summary>
        /// <param name="type"></param>
        public LocalizedAliasableEnumConverter(Type type)
            : base(type, DataStrings.ResourceManager, ResourceLookup.Instance)
        {
        }
    }
}
