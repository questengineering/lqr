﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.questintegritygroup.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
#endregion

using System;
using QuestIntegrity.Core.Extensions.Enum;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    /// Defines a type converter for enum types defined in this project
    /// </summary>
    public class LocalizedEnumConverter : ResourceEnumConverter
    {
        /// <summary>
        /// Create a new instance of the converter using translations from the given resource manager
        /// </summary>
        /// <param name="type"></param>
        public LocalizedEnumConverter(Type type)
            : base(type, Resources.DataStrings.ResourceManager)
        {
        }
    }
}