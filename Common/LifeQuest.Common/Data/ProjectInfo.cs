#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/18/2008 11:14:29 AM
// Created by:   d.revelle
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Strings;
using QuestIntegrity.Core.Units;
using QuestIntegrity.Graphics.ColorScaleBar;
using QuestIntegrity.LifeQuest.Common.Data.Resources;
using QuestIntegrity.LifeQuest.Data;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    ///     Class for containing LifeQuest Project information
    ///     There is one set of Project information for a particular pipeline
    ///     A Project file can point to multiple inspection files.
    /// </summary>
    [Serializable]
    public class ProjectInfo
    {
        #region Private/Protected Variables

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors

        public ProjectInfo()
        {
            TrackingUpdates = false;
            ProjectName = DataStrings.NewProject;
            ApplicationVersionOnCreation = Application.ProductVersion;
            ApplicationLicenseVersionOnCreation = ApplicationVersion.LicenseVersion;
            Location = "";
            AnalystName = "";
            EngineerName = "";
            ProjectDate = "";
            ProjectName = "";
            ProjectStatus = ProjectStatus.Unprocessed_Data;
            DateCreated = DateTime.Now;
        }

        #endregion

        #region Public Properties

        [XmlIgnore]
        public bool TrackingUpdates { get; set; }

        public string ProjectName { get; set; }

        public ProjectStatus ProjectStatus { get; set; } 

        public string Description { get; set; }

        public string DirectoryPath { get; set; }
        
        public string FileName { get; set; }

        public string FilePath
        {
            get
            {
                return Path.Combine(DirectoryPath.ReplaceNull(""), FileName.ReplaceNull(""));
            }
        }

        public string Location { get; set; }

        public DateTime DateCreated { get; set; }

        public string AnalystName { get; set; }

        public string EngineerName { get; set; }

        public string ProjectDate { get; set; }

        public string ProjectOffice { get; set; }

        public string ApplicationVersionOnCreation { get; set; }

        public string ApplicationLicenseVersionOnCreation { get; set; }

        public DisplayUnits DisplayUnits
        {
            get
            {
                return DisplayUnits.Instance;
            }
            set
            {
                //this line helps overcome XmlSerializer's inability to properly deserialize a singleton
                DisplayUnits.Instance.Merge(ref value);
            }
        }

        public ColorScaleBar ColorScale { get; set; }

        #endregion Public Properties
        
    }
}