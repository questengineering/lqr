﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/3/2008 10:31:49 AM
// Created by:   J.Rowe
//

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Strings;
using QuestIntegrity.LifeQuest.Common.Data.EventArguments;
using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.Data.DataCache
{
    /// <summary>
    ///     Class that keeps tabs on what data files have been loaded and how they must be preserved
    ///     inside of the cache.  It is a "passive" cache in that it will try to prevent the unloading
    ///     of files if the cache MaximumMB size has not been reached, but it will do nothing to prevent or cancel
    ///     the actual loading of files.  To manage the cache size, it will Trim() the cache after a file
    ///     has been loaded by unloading the lowest priority file without a Hard Lock set.
    /// </summary>
    public class FileCacheManager
    {
        #region Private Members

        private List<InspectionFile> _registeredInspectionFiles = new List<InspectionFile>(); //holds reference to and InspectionFileInfo's registered with FileCacheManager
        private CachedFileSet _cachedFiles = new CachedFileSet(); //holds reference to any FileDataInfo's registered with FileCacheManager as well as FileLockEntry info
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
#if X64
        private const int DefaultCacheSize = 1500;
#else
        private const int DefaultCacheSize = 1000;
#endif
        private const float BaseMemoryFootPrint = 300; //rough estimate of basic forms open.  Could be adjusted to get a better number

        #endregion

        #region Public Properties

        /// <summary> The one-and-only instance of the FileCache </summary>
        public static readonly FileCacheManager Instance = new FileCacheManager();

        /// <summary>
        ///     Maximum number of megabytes of the cache.  Note: this is a "soft" maximum, meaning
        ///     that the cache will only unload files when requested if the number of MB in the cache is over the MaximumMB amount.
        ///     NOTE: you are still able to Load files regardless of this value.
        /// </summary>
        [XmlIgnore]
        public int MaximumMB { get; set; }

        /// <summary>
        ///     Maximum number of megabytes in the cache.  Note: this is a "soft" maximum, meaning
        ///     that the cache will only unload files when requested over this amount.  You are still
        ///     able to Load files.
        /// </summary>
        [XmlIgnore]
        public float CachedMB
        {
            get
            {
                return _cachedFiles.CachedMB;
            }
        }

        #endregion

        #region Constructor

        private FileCacheManager()
        {
            MaximumMB = DefaultCacheSize;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Registers an inspection file with the FileCacheManager.  This will associate event
        ///     handlers with the loading/unloading events of the FileData objects.
        /// </summary>
        /// <param name="inspectionFile"></param>
        public void Register(InspectionFile inspectionFile)
        {
            if (!_registeredInspectionFiles.Contains(inspectionFile))
            {
                _registeredInspectionFiles.Add(inspectionFile);

                //Register loading/unloading events
                foreach (FileDataInfo file in inspectionFile.FileDataInfos)
                {
                    file.FileDataLoading += File_FileDataLoading;
                    file.FileDataLoaded += File_FileDataLoaded;
                    file.FileDataUnLoading += File_FileDataUnLoading;
                    file.FileDataUnLoaded += File_FileDataUnLoaded;
                }
            }
        }

        /// <summary>
        ///     DeRegisters an inspection file with the FileCacheManager.  This will disassociate event
        ///     handlers with the loading/unloading events of the FileData objects.
        /// </summary>
        /// <param name="inspectionFile"></param>
        public void DeRegister(InspectionFile inspectionFile)
        {
            if (_registeredInspectionFiles.Contains(inspectionFile))
            {
                _registeredInspectionFiles.Remove(inspectionFile);

                //DeRegister loading/unloading events
                foreach (FileDataInfo file in inspectionFile.FileDataInfos)
                {
                    file.FileDataLoading -= File_FileDataLoading;
                    file.FileDataLoaded -= File_FileDataLoaded;
                    file.FileDataUnLoading -= File_FileDataUnLoading;
                    file.FileDataUnLoaded -= File_FileDataUnLoaded;

                    //Remove any associated files that may be in the cache
                    ForceRemoveFileFromCache(file);
                }
            }
        }

        /// <summary>
        ///     DeRegisters all InspectionFiles associated with FileCacheManager.  This will disassociate event
        ///     handlers with the loading/unloading events of the FileData objects.
        /// </summary>
        public void DeRegisterAll()
        {
            foreach (InspectionFile inspectionFile in _registeredInspectionFiles)
            {
                //DeRegister loading/unloading events
                foreach (FileDataInfo file in inspectionFile.FileDataInfos)
                {
                    //Un-attach events
                    file.FileDataLoading -= File_FileDataLoading;
                    file.FileDataLoaded -= File_FileDataLoaded;
                    file.FileDataUnLoading -= File_FileDataUnLoading;
                    file.FileDataUnLoaded -= File_FileDataUnLoaded;
                }
            }

            //Explicitly clear lists
            _registeredInspectionFiles = new List<InspectionFile>();
            lock (_cachedFiles)
            {
                _cachedFiles.Clear();
                _cachedFiles = new CachedFileSet();
            }
            _cachedFiles.CachedMB = 0;
        }

        /// <summary>
        ///     Marks file as "locked" and will prevent from unloading
        /// </summary>
        public void LockFile(FileDataInfo file)
        {
            lock (_cachedFiles)
            {
                _cachedFiles.AddHardLock(file);
            }
        }

        /// <summary>
        ///     Marks file as "unlocked" and will allow it to be unloaded
        /// </summary>
        public void UnlockFile(FileDataInfo file)
        {
            if (_cachedFiles.ContainsKey(file))
            {
                _cachedFiles[file].HardLockCount = 0;
            }
        }

        /// <summary>
        ///     Prints the Cache content to console
        /// </summary>
        public override string ToString()
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("===========================================================================");
            summary.AppendLine("FileCacheManager Contents");
            summary.AppendLine("===========================================================================");
            summary.AppendFormat("Max MB:      {0}" + StringExtensions.CRLF, MaximumMB);
            summary.AppendFormat("Cached MB:   {0}" + StringExtensions.CRLF, CachedMB);
            summary.AppendFormat("Files:       {0}" + StringExtensions.CRLF, _cachedFiles.Count);
            summary.AppendFormat("Registered Inspection Files:" + StringExtensions.CRLF);
            foreach (InspectionFile inspection in _registeredInspectionFiles)
            {
                summary.AppendFormat("   {0}" + StringExtensions.CRLF, inspection.Name);
            }

            summary.AppendLine("===========================================================================");
            summary.AppendLine("Cache contents:");
            summary.AppendLine("    Name, MB, Age (ms), Last Access (ms), Hard, Soft, Loaded");
            foreach (KeyValuePair<FileDataInfo, LockEntry> file in _cachedFiles.OrderByDescending(f => f.Value.TimeSinceLastAccess))
            {
                try
                {
                    summary.AppendFormat("   {0}, {1:N1} MB, {2:N3}, {3:N3}, {4}, {5}" + StringExtensions.CRLF, file.Key.Name, file.Key.MemorySizeInMB, file.Value.Age.TotalSeconds, file.Value.TimeSinceLastAccess.TotalSeconds, file.Value.HardLockCount, file.Value.SoftLockPriority);
                }
                catch (ArgumentOutOfRangeException)
                {
                    //continue, multithreading issue
                }
            }
            summary.AppendLine("===========================================================================");

            return summary.ToString();
        }

        #endregion

        #region Private Methods and Event Handlers

        /// <summary>
        ///     Checks the cache and let's the calling routing know if the file already exists
        ///     in the cache by setting the e.Cancel property to "true".  It is up to the
        ///     calling routing to do (or not do) something with this information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void File_FileDataLoading(object sender, FileDataLoadingEventArgs e)
        {
            if (CacheContainsFileAndScalars(e.LoadedFileDataInfo, e.ScalarsLoading))
            {
                //Let the sender know that the file already exists in the cache.  Nothing
                // necessarily needs to be done with this information by the sender.
                e.Cancel = true;
            }
        }

        /// <summary>
        ///     Handles the FileDataLoaded event of the registered file.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///     The <see cref="FileDataLoadedEventArgs" /> instance containing the event data.
        /// </param>
        private void File_FileDataLoaded(object sender,
                                         FileDataLoadedEventArgs e)
        {
            AddFileToCache(e.LoadedFileDataInfo, e.LockPriority);
            //Print();
        }

        /// <summary>
        ///     Handles the cache logic when a file is about to be unloaded.  Based on the
        ///     cache state and options, can return e.Cancel value of true signaling
        ///     the calling routing to cancel loading.  Note that the cache itself does not actually
        ///     doing any loading/unloading of the file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void File_FileDataUnLoading(object sender,
                                            FileDataLoadingEventArgs e)
        {
            //Make sure file is in cache; if so, get the priority of
            // remaining item after this lock is removed.
            int hardLockCount;
            lock (_cachedFiles)
            {
                if (!_cachedFiles.ContainsKey(e.LoadedFileDataInfo))
                {
                    Log.DebugFormat("File {0} not found in cache.", e.LoadedFileDataInfo.Name);
                    return;
                }

                if (e.LockPriority == 0)
                {
                    _cachedFiles.RemoveHardLock(e.LoadedFileDataInfo);
                }
                else
                {
                    _cachedFiles.SetSoftLock(e.LoadedFileDataInfo, 0); //Remove the soft lock
                }
                hardLockCount = _cachedFiles[e.LoadedFileDataInfo].HardLockCount;
            }

            //Determine if file unloading should be canceled
            if (hardLockCount > 0) //Need to cancel if multiple locks are held regardless of whether cache is "Full"
            {
                //Cancel the unload event and to keep the file in memory                
#if LOCK_TRACING
                _log.DebugFormat("Cancelling Unload request for {1}.  Num hard locks: {0}.", hardLockCount, e.LoadedFileDataInfo.Name);
#endif
                e.Cancel = true;
            }
            else if (!IsCacheFull()) //Cache is not full, so cancel
            {
                //Cancel the unload event and to keep the file in memory
#if LOCK_TRACING
                _log.DebugFormat("Cancelling Unload request for {1}.  Cache Full={0}.", isCacheFull(), e.LoadedFileDataInfo.Name);
#endif
                e.Cancel = true;
            }
            else //Don't cancel since the cache is full.
            {
#if LOCK_TRACING
                _log.DebugFormat("Not Cancelling Unload request for {2}.  Cache Full={0}. Soft lock priority: {1}.",
                        isCacheFull(), _cachedFiles[e.LoadedFileDataInfo].SoftLockPriority, e.LoadedFileDataInfo.Name);
#endif
                _cachedFiles.CachedMB -= e.LoadedFileDataInfo.MemorySizeInMB; //need to decrement here before all the data is removed and the file shrinks!
#if DEBUG
                Log.DebugFormat("Cache full. Unloading: {0}.", e.LoadedFileDataInfo.Name);
#endif
            }
        }

        /// <summary>
        ///     Handles the FileDataUnLoaded event of the registered file.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///     The <see cref="FileDataUnloadedEventArgs" /> instance containing the event data.
        /// </param>
        private void File_FileDataUnLoaded(object sender,
                                           FileDataUnloadedEventArgs e)
        {
            //Data has been unloaded, so need to remove from cache regardless
            lock (_cachedFiles)
            {
                _cachedFiles.Remove(e.LoadedFileDataInfo);

#if LOCK_TRACING
                _log.Debug(this.ToString());
#endif
            }
        }

        /// <summary>
        ///     Determines whether the current Cached # MB's exceed the Maximum specified # MBs
        /// </summary>
        /// <returns>
        ///     <c>true</c> if [is cache full]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsCacheFull()
        {
            float usedMemory = BaseMemoryFootPrint + _cachedFiles.CachedMB;
            return (usedMemory > MaximumMB);
        }

        /// <summary>
        ///     Adds a file to the cached and marks as locked or not depending on parameter.
        /// </summary>
        private void AddFileToCache(FileDataInfo file,
                                    int lockPriority)
        {
            lock (_cachedFiles)
            {
                //Only adjust cached size if new
                if (!_cachedFiles.ContainsKey(file))
                {
                    _cachedFiles.CachedMB += file.MemorySizeInMB;
                }

                //Set the lock counts/priority
                if (lockPriority == 0)
                {
                    _cachedFiles.AddHardLock(file);
                }
                else
                {
                    _cachedFiles.SetSoftLock(file, lockPriority);
                }
                Trim(file);
            }
        }

        /// <summary>
        ///     Removes a file from the file cache unless it is locked.  Returns "true" if file was removed
        ///     successfully or "false" if it could not be removed
        /// </summary>
        /// <returns></returns>
        private void RemoveFileFromCache(FileDataInfo file,
                                         bool force)
        {
            lock (_cachedFiles)
            {
                if (_cachedFiles.ContainsKey(file))
                {
                    if (_cachedFiles[file].HardLockCount == 0 || force)
                    {
                        _cachedFiles.CachedMB -= file.MemorySizeInMB;
                        _cachedFiles.Remove(file);
                    }
                }
            }
        }

        /// <summary>
        ///     Removes a file from the file cache unless it is locked.  Returns "true" if file was removed
        ///     successfully or "false" if it could not be removed
        /// </summary>
        /// <returns></returns>
        private void ForceRemoveFileFromCache(FileDataInfo file)
        {
            RemoveFileFromCache(file, true);
        }

        /// <summary>
        ///     Searches for file in CachedFiles list
        /// </summary>
        /// <param name="file"></param>
        /// <param name="scalars">The scalars that are attempting to be loaded.</param>
        /// <returns></returns>
        private bool CacheContainsFileAndScalars(FileDataInfo file, Scalar[] scalars)
        {
            //Check if there is any requested scalar not contained within the cached 2D or 1D filedata already.
            bool cacheHasItAlready = scalars.All(s => file.FileData.ScalarsKeys2D.Contains(s.ToString()) || file.FileData.ScalarsKeys1D.Contains(s.ToString()));
            if (!_cachedFiles.ContainsKey(file))
                cacheHasItAlready = false;
            return cacheHasItAlready;
        }

        /// <summary>
        ///     Searches for file in CachedFiles list and returns whether it is locked or not.
        ///     A "locked" file is one that has a > 0 hard lock count
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool IsFileLocked(FileDataInfo file)
        {
            lock (_cachedFiles)
            {
                if (_cachedFiles.ContainsKey(file))
                {
                    return (_cachedFiles[file].HardLockCount > 0);
                }
                return false;
            }
        }

        /// <summary>
        ///     Determines if unlocked files exist and need to be removed from the cache.
        /// </summary>
        private void Trim(FileDataInfo excludeThisfile)
        {
            //Just exit if not full
            if (!IsCacheFull())
            {
                return;
            }

            //Get the cached list in reverse order
            List<FileDataInfo> orderedList;
            lock (_cachedFiles)
            {
                //Query files based by cache age...this makes more sense than
                // using the "priority" flag.
                orderedList = _cachedFiles.Where(cv => cv.Value.HardLockCount <= 0).OrderByDescending(cv => cv.Value.TimeSinceLastAccess).Select(cv => cv.Key).ToList();
            }

            //Trim until below cache level
            foreach (FileDataInfo file in orderedList)
            {
                int priority = _cachedFiles[file].SoftLockPriority;
                if (Monitor.TryEnter(file))
                {
                    try
                    {
                        //in case the list is somehow out of synch--this will remove the file from the list directly.
                        Log.DebugFormat("Removing file from buffer: {0}", file.Name);
                        file.UnloadData(priority);
                    }
                    finally
                    {
                        Monitor.Exit(file);
                    }
                }
                else
                {
                    Log.DebugFormat("Cannot lock file: {0}. Skipping trimming from buffer.", file.Name);
                }

                //Break out once the cache is below target size.
                if (!IsCacheFull())
                {
                    //Update the cache size first to make sure it is not too far out of synch.
                    _cachedFiles.SynchCacheReportedSize();
                    if (!IsCacheFull())
                    {
                        break;
                    }
                }
            }
        }

        #endregion

        #region Private Classes

        /// <summary>
        ///     Holds the lock information for a file.
        /// </summary>
        private class LockEntry
        {
            private DateTime _lastAddTime = DateTime.Now;
            private readonly DateTime _createTime = DateTime.Now;
            private int _hardLockCount;
            private int _softLockPriority;

            public LockEntry(int hardLockCount,
                             int softLockPriority = 0)
            {
                _hardLockCount = hardLockCount;
                _softLockPriority = softLockPriority;
            }

            /// <summary>
            ///     Gets or sets the "hard lock" count.  A hard-lock is a lock that must be
            ///     kept on the file.  Cache will use this value to determine if a file can
            ///     be unloaded.
            /// </summary>
            /// <value>The hard lock count.</value>
            public int HardLockCount
            {
                get
                {
                    return _hardLockCount;
                }
                set
                {
                    _hardLockCount = value;
                    _lastAddTime = DateTime.Now;
                }
            }

            /// <summary>
            ///     Gets or sets the soft lock priority.  "soft lock priority" is an integer
            ///     indicating the relative importance/usefulness of the file in the cache.
            ///     It will not prevent unloading, but can be used for determing which
            ///     file to unload first if no hard lock is held on a set of files in the cache.
            /// </summary>
            /// <value>The soft lock priority.</value>
            public int SoftLockPriority
            {
                get
                {
                    return _softLockPriority;
                }
                set
                {
                    _softLockPriority = value;
                    _lastAddTime = DateTime.Now;
                }
            }

            /// <summary>
            ///     Records the elapsed time since the record was added to the cache
            /// </summary>
            /// <value>Timestamp of the last time this file was accessed.</value>
            public TimeSpan Age
            {
                get
                {
                    return DateTime.Now.Subtract(_createTime);
                }
            }

            /// <summary>
            ///     Records the elapsed time since the entry last had a hard or soft lock added
            /// </summary>
            /// <value>Timestamp of the last time this file was accessed.</value>
            public TimeSpan TimeSinceLastAccess
            {
                get
                {
                    return DateTime.Now.Subtract(_lastAddTime);
                }
            }
        }

        /// <summary>
        ///     Holds references to each cached file along with the lock information containing
        ///     the number of Hard Locks and Soft Lock Priority.  The CachedFileSet
        ///     will contain only one of each file.
        /// </summary>
        private class CachedFileSet : Dictionary<FileDataInfo, LockEntry>
        {
            /// <summary>
            ///     Gets or sets the cached MB.
            /// </summary>
            /// <value>The cached MB.</value>
            public float CachedMB { get; set; }

            /// <summary>
            ///     Adds the hard lock to the specified file.  If the file does not exist in
            ///     the cache, it will be added.  If it already exists, the hard lock count
            ///     will be incremented.
            /// </summary>
            /// <param name="file">The file.</param>
            public void AddHardLock(FileDataInfo file)
            {
                if (ContainsKey(file))
                {
                    this[file].HardLockCount++;

                    //NOTE: this shouldn't be a necessary check, but for some reason, hard locks are going below zero.
                    //  See the corresponding check in RemoveHardLock()
                    if (this[file].HardLockCount < 1)
                    {
                        this[file].HardLockCount = 1;
                    }
                }
                else
                {
                    Add(file, new LockEntry(1));
                }
            }

            /// <summary>
            ///     Removes a hard lock count for the specified file.
            /// </summary>
            /// <param name="file"></param>
            public void RemoveHardLock(FileDataInfo file)
            {
                if (ContainsKey(file))
                {
                    this[file].HardLockCount--;
                    if (this[file].HardLockCount < 0)
                    {
                        this[file].HardLockCount = 0;
                    }
                }
            }

            /// <summary>
            ///     Sets a soft lock on the file with the corresponding priority.
            ///     If a soft lock priority already exists, it will be updated to the specified
            ///     value.
            /// </summary>
            /// <param name="file">The file.</param>
            /// <param name="priority">The priority.</param>
            public void SetSoftLock(FileDataInfo file,
                                    int priority)
            {
                if (ContainsKey(file))
                {
                    this[file].SoftLockPriority = priority;
                }
                else
                {
                    Add(file, new LockEntry(0, priority));
                }
            }

            /// <summary>
            ///     Synchs up the CachedMB value with the memory size of the loaded files.  Because bookkeeping
            ///     can get out of whack, and scalars can be added/removed, this is necessary from time-to-time.
            /// </summary>
            public void SynchCacheReportedSize()
            {
                lock (this)
                {
                    CachedMB = Keys.Sum(size => size.MemorySizeInMB);
                }
            }
        }

        #endregion
    }
}
