#region Copyright Quest Integrity Group, LLC 2012
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/19/2008 10:42:59 AM
// Created by:   D.Revelle
//
// Filename: $Id: FileData.cs,v 1.92 2012/01/03 18:59:08 J.Rowe Exp $
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.LifeQuest.Common.Data.Interfaces;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.Data.DataCache
{
    /// <summary>
    /// Model of the data in a qig file
    /// </summary>
    public class FileData : IFileData
    {
        #region Private/Protected Variables

        private bool _1DStatsCalculated;
        private Dictionary<string, IScalarArray> _scalars2D = new Dictionary<string, IScalarArray>();
        private Dictionary<string, IScalarArray> _scalars1D = new Dictionary<string, IScalarArray>();

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors

        public FileData()
        {
            Initialize();
        }

        #endregion

        #region Public Properties

        public virtual int NumRadialReadings { get; set; }

        public virtual int NumAxialSections  { get; set; }

        /// <summary> Returns the number of 2D scalars. </summary>
        public int NumScalars2D
        {
            get
            {
                return _scalars2D.Count;
            }
        }

        /// <summary>
        /// Returns the number of 1D scalars.
        /// </summary>
        public int NumScalars1D
        {
            get
            {
                return _scalars1D.Count;
            }
        }

        /// <summary>
        /// Returns a list of the 2D Scalar keys.
        /// </summary>
        public List<string> ScalarsKeys2D
        {
            get
            {
                return _scalars2D.Keys.ToList<string>();
            }
        }

        /// <summary>
        /// Returns a list of the 1D Scalar keys.
        /// </summary>
        public List<string> ScalarsKeys1D
        {
            get
            {
                return _scalars1D.Keys.ToList();
            }
        }

        /// <summary>
        /// Returns a list of both the 2D and the 1D scalar keys
        /// </summary>
        public List<string> ScalarKeysAll
        {
            get
            {
                List<string> keys = ScalarsKeys2D;
                keys.AddRange(ScalarsKeys1D);
                return keys;
            }
        }

        public float SizeInMB { get; set; }

        #endregion

        #region Public Methods

        // Functions to make accessing Scalar dictionary more intuitive
        private void AddScalars1D(IScalarArray scalarArray)
        {
            string scalarKey = scalarArray.Key;
            if (_scalars1D.ContainsKey(scalarKey)) { _scalars1D[scalarKey] = scalarArray; }
            else { _scalars1D.Add(scalarKey, scalarArray); }
        }

        public void AddScalars2D(IScalarArray scalarArray)
        {
            string scalarKey = scalarArray.Key;
            AddScalars2D(scalarKey, scalarArray);
        }

        public void AddScalars2D(string scalarKey, IScalarArray scalarArray)
        {            
            if (_scalars2D.ContainsKey(scalarKey)) { _scalars2D[scalarKey] = scalarArray; }
            else {_scalars2D.Add(scalarKey, scalarArray); }           
        }

        /// <summary>
        /// Removes a 2D scalar and optionally all of its children
        /// </summary>
        /// <param name="scalarKey"></param>
        /// <param name="removeChildren"></param>
        public void RemoveScalar2D(string scalarKey, bool removeChildren)
        {
            if (_scalars2D.ContainsKey(scalarKey))
            {
                _scalars2D[scalarKey].Clear();
                if (removeChildren)
                {
                    //Make a list of child scalars to remove
                    List<string> removeList = (from scalar1D in _scalars1D where scalar1D.Value.ParentKey == scalarKey select scalar1D.Key).ToList();

                    //Clear and remove the scalars
                    foreach (string scalar1DKey in removeList)
                    {
                        _scalars1D[scalar1DKey].Clear();
                        _scalars1D.Remove(scalar1DKey);
                    }
                }
                _scalars2D.Remove(scalarKey);
            }
        }

        public void AddScalarsAny(string scalarKey, IScalarArray scalarArray)
        {
            if (scalarArray.Key.Contains("_"))
            {
                scalarArray.Key = scalarKey;        //scalarArray.Key is used by AddScalars1D, so make sure it is correct
                string[] keys = scalarKey.Split('_');
                scalarArray.ParentKey = keys[0];
                AddScalars1D(scalarArray);
            }
            else { AddScalars2D(scalarKey, scalarArray); }
        }

        /// <summary> /// Returns the ScalarArray that matches the given scalarKey (1D or 2D) /// </summary>
        /// <param name="scalarKey">If a 1D scalar is wanted the scalarKey should equal the full scalarSubItemKey</param>
        /// <returns>ScalarArray matching the given scalarKey</returns>
        public IScalarArray GetScalarsAny(string scalarKey)
        {
            IScalarArray scalars = GetScalars2D(scalarKey);
            if (scalars == null) { scalars = GetScalars1D(scalarKey); }
            return scalars;
        }

        public IScalarArray GetScalars2D(Scalar scalarType)
        {
            return GetScalars2D(scalarType.ToString());
        }

        public IScalarArray GetScalars2D(string scalarKey)
        {
            IScalarArray scalars = null;
            if (_scalars2D.ContainsKey(scalarKey))
            {
                scalars = _scalars2D[scalarKey];
            }
            return scalars;
        }

        public IScalarArray ExtractScalars2D(Scalar scalarType, int lowAxialIndex, int hiAxialIndex, int lowCircumferentialIndex, int hiCircumferentialIndex)
        {
            return ExtractScalars2D(scalarType.ToString(), lowAxialIndex, hiAxialIndex, lowCircumferentialIndex, hiCircumferentialIndex);
        }

        public IScalarArray ExtractScalars2D(string scalarKey, int lowAxialIndex, int hiAxialIndex, int lowCircumferentialIndex, int hiCircumferentialIndex)
        {
            IScalarArray scalarsFull = null;
            IScalarArray scalars = null;
            if (_scalars2D.ContainsKey(scalarKey))
            {
                scalarsFull = _scalars2D[scalarKey];
                scalars = new ScalarArray<float>(scalarKey);        //TODO: This float should actually depend on the type of _scalars2D
                for (int i = lowAxialIndex; i <= hiAxialIndex; i++)
                {
                    for (int j = lowCircumferentialIndex; j <= hiCircumferentialIndex; j++)
                    {
                        scalars.Add(scalarsFull[i * NumRadialReadings + j]);
                    }
                }
                scalars.SetDimensions(hiAxialIndex - lowAxialIndex + 1, hiCircumferentialIndex - lowCircumferentialIndex + 1);
            }
            return scalars;
        }

        public ScalarArray<T> GetScalars2D<T>(string scalarKey) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            ScalarArray<T> scalars;
            if (!_scalars2D.ContainsKey(scalarKey)) return null;
            
            if (_scalars2D[scalarKey].DataType != typeof(T))
            {
                scalars = new ScalarArray<T>(scalarKey);
                scalars.AddRange(from ValueType v in _scalars2D[scalarKey] select (T) Convert.ChangeType(v, typeof (T)));
            }
            else 
                scalars = _scalars2D[scalarKey] as ScalarArray<T>;
            return scalars;
        }

        public ScalarArray<T> GetScalars2D<T>(Scalar scalarType) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            return GetScalars2D<T>(scalarType.ToString());
        }

        public IScalarArray GetScalars1D(string scalarSubItemKey)
        {
            IScalarArray scalars = null;
            if (_scalars1D.ContainsKey(scalarSubItemKey))
            {
                scalars = _scalars1D[scalarSubItemKey];
            }
            return scalars;
        }

        public IScalarArray GetScalars1D(string scalarKey, StatisticalMeasures measure)
        {
            string key = string.Format("{0}_{1}", scalarKey, measure);
            return GetScalars1D(key);
        }

        public ScalarArray<T> GetScalars1D<T>(string scalarSubItemKey) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            ScalarArray<T> scalars = null;
            if (_scalars1D.ContainsKey(scalarSubItemKey))
            {
                scalars = _scalars1D[scalarSubItemKey] as ScalarArray<T>;
                if (scalars == null)
                {
                    scalars = _scalars1D[scalarSubItemKey].Cast<T>() as ScalarArray<T>;
                }
            }
            return scalars;
        }

        public ScalarArray<T> GetScalars1D<T>(string scalarTwoDKey, StatisticalMeasures measure) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            string key = string.Format("{0}_{1}", scalarTwoDKey, measure);
            return GetScalars1D<T>(key);
        }

        public IScalarArray ExtractScalars1DAtPosition(int sensorNumber, string scalarKey)
        {
            IScalarArray scalars = GetScalars2D(scalarKey);
            return GetDataAtRadialIndex(sensorNumber - 1, scalars);
        }

        public ScalarArray<T> ExtractScalars1DAtPosition<T>(int sensorNumber, string scalarKey) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            if (sensorNumber > NumRadialReadings)
            {
                return new ScalarArray<T>(scalarKey);
            }
            ScalarArray<T> scalars = GetScalars2D(scalarKey) as ScalarArray<T>;
            return GetDataAtRadialIndex(sensorNumber - 1, scalars);
        }

        /// <summary>
        /// Returns the index into Scalars2D that corresponds to the given axial and circumferential location
        /// </summary>
        /// <param name="axialIndex"></param>
        /// <param name="circIndex"></param>
        /// <returns></returns>
        public int Scalar2DIndex(int axialIndex, int circIndex)
        {
            return axialIndex * NumRadialReadings + circIndex;
        }

        /// <summary>
        /// Returns an int array with [axial, circ] index of the given scalar2DIndex
        /// Inverse of Scalar2DIndex
        /// </summary>
        /// <param name="scalar2DIndex"></param>
        /// <returns>[axialIndex, circIndex]</returns>
        public int[] Scalar2DAxialCircIndex(int scalar2DIndex)
        {
            int[] returnIndices = new int[2];
            returnIndices[0] = Math.DivRem(scalar2DIndex, NumRadialReadings, out returnIndices[1]);
            return returnIndices;
        }

        public virtual void SetProcessingOptions(ProcessingOptions options)
        {
            foreach (var scalar in ScalarsKeys1D)
            {
                GetScalarsAny(scalar).ProcessingOptions = options;
            }

            foreach (var scalar in ScalarsKeys2D)
            {
                GetScalarsAny(scalar).ProcessingOptions = options;
            }
        }


        public virtual void Clear()
        {
            foreach (var item in _scalars1D)
            {
                //Clear values in 1D scalar arrays
                if (item.Value != null)
                {
                    item.Value.Clear();
                }
            }
            foreach (var item in _scalars2D)
            {
                if (item.Value != null)
                {
                    //Clear values in 2D scalar arrays
                    item.Value.Clear();
                }
            }

            //Clear dictionaries themselves
            _scalars2D.Clear();
            _scalars1D.Clear();
            
            //ReInitialize
            Initialize();
        }

        #endregion

        #region Private Methods

        private void Initialize()
        {
            _scalars2D = new Dictionary<string, IScalarArray>();
            _scalars1D = new Dictionary<string, IScalarArray>();
        }

        private IScalarArray GetDataAtRadialIndex(int radialIndex, IScalarArray scalars)
        {
            IScalarArray data = scalars.Create();
            data.SetDimensions(NumAxialSections, 1);
            for (int index = radialIndex; index < NumAxialSections * NumRadialReadings; index += NumRadialReadings)
            {
                data.Add(scalars[index]);
            }
            return data;
        }

        private ScalarArray<T> GetDataAtRadialIndex<T>(int radialIndex, ScalarArray<T> scalars) where T : struct, IComparable<T>, IEquatable<T>, IConvertible
        {
            ScalarArray<T> data = new ScalarArray<T>(scalars.Key);
            data.SetDimensions(NumAxialSections, 1);
            for (int index = radialIndex; index < NumAxialSections * NumRadialReadings; index += NumRadialReadings)
            {
                data.Add(scalars[index]);
            }
            return data;
        }

        public void Calc1DStats(bool forceRecalc)
        {
            if (forceRecalc)
            {
                _1DStatsCalculated = false;
            }
            Calc1DStats();
        }

        public void Calc1DStats()
        {
            if (!_1DStatsCalculated) //TODO: need a check here or way to handle if new Scalars are created (e.g., RSF, MAOPDesign, etc)
            {
                //Calculate stats for each 2D ScalarArray
                StatisticalMeasures statsToSave;
                foreach (KeyValuePair<string, IScalarArray> scalar in _scalars2D)
                {
                    IScalarArray scalar2D = GetScalars2D(scalar.Key);
                    
                    statsToSave = StatisticalMeasures.Minimum |
                                StatisticalMeasures.Maximum |
                                StatisticalMeasures.Mean |
                                StatisticalMeasures.Median |
                                StatisticalMeasures.Mode |
                                StatisticalMeasures.StandardDeviation |
                                StatisticalMeasures.Variance |
                                StatisticalMeasures.RealCount |
                                StatisticalMeasures.Count;
                    string key = scalar2D.Key + "_SensorValue";
                    IScalarArray sensor = new ScalarArray<float>(key)
                    {
                        ParentKey = scalar2D.Key
                    };

                    if (_scalars1D.ContainsKey(key))
                    {
                        _scalars1D[key] = sensor;
                    }
                    else
                    {
                        _scalars1D.Add(key, sensor);
                    }
                    //Calc the 1D stats for the 2D scalars
                    (scalar.Value as IScalarArray).Calc1DStats(statsToSave, NumRadialReadings, _scalars1D);
                    
                }
                _1DStatsCalculated = true;
            }
        }

        #endregion
    }
}