﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//

#endregion

using System.ComponentModel;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.Data.DataCache
{
    public delegate void FileDataLoadingEventHandler(object sender,
                                                     FileDataLoadingEventArgs e);

    public class FileDataLoadingEventArgs : CancelEventArgs
    {
        #region Private Properties

        private readonly FileDataInfo _fileDataInfo;
        private readonly int _lockPriority;
        private readonly Scalar[] _scalarsLoading; 

        #endregion

        #region Constructor

        public FileDataLoadingEventArgs(FileDataInfo fileDataInfo, 
                                        Scalar[] scalarsLoading = null,
                                        int lockPriority = 0,
                                        bool cancel = false )
            : base(cancel)
        {
            _fileDataInfo = fileDataInfo;
            _lockPriority = lockPriority;
            _scalarsLoading = scalarsLoading;
        }

        #endregion

        #region Public Properties

        public Scalar[] ScalarsLoading
        {
            get
            {
                return _scalarsLoading;  
            }
        } 

        public FileDataInfo LoadedFileDataInfo
        {
            get
            {
                return _fileDataInfo;
            }
        }

        /// <summary>
        ///     Tells the file cache manager that loading or unloading of this file should be counted against
        ///     the lock count for the file in the cache.
        /// </summary>
        public int LockPriority
        {
            get
            {
                return _lockPriority;
            }
        }

        #endregion
    }
}