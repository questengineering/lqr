#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/1/2008 2:16:46 PM
// Created by:   D.Revelle
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.LifeQuest.Common.Data.EventArguments;
using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;
using QuestIntegrity.LifeQuest.Common.IO;

namespace QuestIntegrity.LifeQuest.Common.Data.DataCache
{
    /// <summary> cacheable information for a subset of data (IE: slice 200-500) </summary>
    public class FileDataInfo : ICloneable
    {
        #region Private Fields

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Events

        public event EventHandler<FileDataLoadingEventArgs> FileDataLoading;
        public event EventHandler<FileDataLoadedEventArgs> FileDataLoaded;
        public event EventHandler<FileDataLoadingEventArgs> FileDataUnLoading;
        public event EventHandler<FileDataUnloadedEventArgs> FileDataUnLoaded;

        #endregion
        
        #region Public Properties

        [XmlIgnore]
        public InspectionFile Parent { get; set; }

        public string Name { get; set; }

        /// <summary>
        ///     Returns a rough estimate of the memory size used by this File.  Takes into account
        ///     whether the data is loaded or not and whether grids exists.  It does not
        ///     calculate other vtk memory (such as Actors, Features, etc).
        /// </summary>
        [XmlIgnore]
        public float MemorySizeInMB
        {
            get
            {
                //Do a rough calculation the number of bytes needed to hold
                // the data.
                //NOTE: this is fairly rough and does not account for various overheads in all cases.
                float bytes = 0;
                
                //Loaded Scalars
                bytes = FileData.ScalarsKeys1D.Aggregate(bytes, (Current, Scalar1D) => Current + NumSections*sizeof (double));
                bytes = FileData.ScalarsKeys2D.Aggregate(bytes, (Current, Scalar2D) => Current + NumPoints*sizeof (float));
                    
                //other overhead---like points in nevron charts, etc.
                bytes += NumPoints * 16; //locations
                return bytes / 1048576.0f; //convert to MB
            }
        }

        [XmlIgnore]
        public FileData FileData { get; set; }

        /// <summary> Lowest index in slice-based arrays (Location, GlobalPosition) </summary>
        public int LowIndex { get; set; }

        /// <summary> Highest index in slice-based arrays (Location, GlobalPosition) </summary>
        public int HighIndex { get; set; }

        /// <summary> Number of slices stored in FileData </summary>
        [XmlIgnore]
        public int NumSections
        {
            get
            {
                return HighIndex - LowIndex + 1;
            }
        }

        /// <summary>
        ///     Number of radial readings, or sensors, in the file
        /// </summary>
        public int NumRadialReadings { get; set; }

        /// <summary> Total number of points (slices * radial readings) </summary>
        public int NumPoints { get { return NumSections * NumRadialReadings; } }

        [XmlIgnore]
        public long GlobalIDMaximum
        {
            get
            {
                return LowIndex * NumRadialReadings + NumPoints - 1;
            }
        }

        public void ClearDirtyData() { FileData.Clear();  }

        #endregion

       
        #region Public Methods

        /// <summary> Loads data with maximum priority </summary>
        public void LoadData(Scalar[] scalarsToLoad)
        {
            LoadData(0, scalarsToLoad);
        }

        /// <summary> Loads the FileData scalars and PeakData </summary>
        /// <param name="lockPriority">The lock priority. </param>
        /// <param name="scalarsToLoad"> The scalars to load from file. only loads scalars if not already loaded. </param>
        /// <exception cref="System.Exception"></exception>
        public void LoadData(int lockPriority, Scalar[] scalarsToLoad)
        {
            try
            {
                Monitor.Enter(this); //don't want another method, such as "Unload()" to be run on this object while data is being loaded
                
                //Only load new scalars, unless dirty.
                if (FileData == null) FileData = new FileData();
                Scalar[] unloadedScalars = scalarsToLoad.Where(S => !FileData.ScalarKeysAll.Contains(S.ToString())).ToArray();
                
                FileDataLoadingEventArgs args = new FileDataLoadingEventArgs(this, unloadedScalars);
                OnFileDataLoading(args);
                if (args.Cancel) { return; }

                try
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    ReadHDF5Data(unloadedScalars);
                    FileData.ScalarKeysAll.AddRange(unloadedScalars.Select(S => S.ToString())); //add all the scalars that have been loaded for this file data.
                    OnFileDataLoaded(new FileDataLoadedEventArgs(this));
                    Log.InfoFormat("Loaded {0} new scalars ({4}) between {1} and {2} in {3} ms.", unloadedScalars.Length, LowIndex, HighIndex, sw.ElapsedMilliseconds, string.Join(",", unloadedScalars.Select(s => s.ToString())));
                }
                catch (Exception e)
                {
                    Log.Error(string.Format("File data loading exception.  Unable to load between slices {0} and {1}.", LowIndex, HighIndex), e);
                    throw new Exception(string.Format("File data loading exception.  Unable to load between slices {0} and {1}", LowIndex, HighIndex), e);
                }
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        private void ReadHDF5Data(IEnumerable<Scalar> scalarsToLoad)
        {
            Parallel.ForEach(scalarsToLoad, scalar =>
            {
                IScalarArray scalarData;
                Type dataType = HDF5Helper.GetScalarDataType(Parent.DataFileFullName, scalar);
                int numReadingsPerSlice = HDF5Helper.GetDataSensorsPerSlice(Parent.DataFileFullName, scalar);
                if (dataType == typeof(float))
                    scalarData = HDF5Helper.ReadScalarData<float>(Parent.DataFileFullName, scalar, LowIndex, HighIndex, 0, numReadingsPerSlice - 1);
                else if (dataType == typeof(double))
                    scalarData = HDF5Helper.ReadScalarData<double>(Parent.DataFileFullName, scalar, LowIndex, HighIndex, 0, numReadingsPerSlice - 1);
                else if (dataType == typeof(int))
                    scalarData = HDF5Helper.ReadScalarData<int>(Parent.DataFileFullName, scalar, LowIndex, HighIndex, 0, numReadingsPerSlice - 1);
                else //(dataType == typeof(short))
                    scalarData = HDF5Helper.ReadScalarData<short>(Parent.DataFileFullName, scalar, LowIndex, HighIndex, 0, numReadingsPerSlice - 1);
                FileData.AddScalarsAny(scalar.ToString(), scalarData);
            });
        }

        public void SaveData()
        {
            try
            {
                Monitor.Enter(this); //Don't want another method, such as LoadData() to run on this file while data is being saved
                //if (FileData != null && FileData.IsDirty)
                //{
                //    System.Threading.Tasks.Task qid = System.Threading.Tasks.Task.Factory.StartNew(SaveQIDDataFile);
                //    System.Threading.Tasks.Task.WaitAll(qid);
                //}
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        
        //private void SaveQIDDataFile()
        //{
        //    //Write out the .qid data
        //    QIDFileDataWriter writer;
        //    Stopwatch stopWatch = new Stopwatch();
        //    stopWatch.Start();
        //    string filePathTemp = FilePath + ".tmp"; //write to a temporary file, then replace to avoid file corruption
        //    Log.DebugFormat("Saving FileData: {0}. Using: {1}. Level: {2}", Name, EncryptionCompressionFlag, CompressionLevel);
        //    using (writer = new QIDFileDataWriter(filePathTemp))
        //    {
        //        writer.EncryptionCompressionFlag = EncryptionCompressionFlag;
        //        writer.CompressionLevel = CompressionLevel;
        //        writer.Write(FileData);
        //        Log.InfoFormat("Saved FileData: {0}. Elapsed time (ms): {1}.", Name, stopWatch.ElapsedMilliseconds);
        //        writer.Close();
        //        FileData.IsDirty = false;
        //        _isDirty = false;
        //    }
        //    File.Copy(filePathTemp, FilePath, true); //overwrite permanent file with new temp file
        //    File.Delete(filePathTemp);
        //}

        public void UnloadData(bool Force = false)
        {
            UnloadData(0,Force);
        }

        /// <summary>
        ///     Unloads the data.
        /// </summary>
        public void UnloadData(int LockPriority, bool Force = false)
        {
            try
            {
                Monitor.Enter(this); //don't want another method, such as "LoadData()" to be run on this object while data is being unloaded
                FileDataLoadingEventArgs args = new FileDataLoadingEventArgs(this);    
                OnFileDataUnLoading(args);
                if (args.Cancel & !Force)
                {
                    //Log.DebugFormat("Unload requested for {0}, but was cancelled.", Name);
                }
                else if (FileData != null)
                {
                    SaveData();
                    FileData.Clear();
                    FileData = null;
                    Log.InfoFormat("Unloaded FileData: {0}.", Name);
                    OnFileDataUnLoaded(new FileDataUnloadedEventArgs(this));
                }
                else
                {
                    //Log.DebugFormat("Unload requested for {0}, but no FileData is loaded.", Name);
                }
                    
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        /// <summary> Determine if the current file info range overlaps the min and max axial position </summary>
        public bool Overlaps(int MinAxialIndex, int MaxAxialIndex)
        {
            return (LowIndex <= MaxAxialIndex && HighIndex >= MinAxialIndex);
        }

        #endregion

        #region IClonable Interface

        public object Clone() { return this.DeepCopy(); }

        #endregion

        #region Event Throwers

        private void OnFileDataLoading(FileDataLoadingEventArgs e)
        {
            if (FileDataLoading != null) { FileDataLoading(this, e); }
        }

        private void OnFileDataLoaded(FileDataLoadedEventArgs e)
        {
            if (FileDataLoaded != null) { FileDataLoaded(this, e); }
        }

        private void OnFileDataUnLoading(FileDataLoadingEventArgs e)
        {
            if (FileDataUnLoading != null) { FileDataUnLoading(this, e); }
        }

        private void OnFileDataUnLoaded(FileDataUnloadedEventArgs e)
        {
            if (FileDataUnLoaded != null) { FileDataUnLoaded(this, e); }
        }

        #endregion Event Throwers
        
    }
}