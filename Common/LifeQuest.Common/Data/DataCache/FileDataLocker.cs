#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 01-Oct-10 10:40:51 AM
// Created by:   J.Rowe
//
// File:  $Id: FileDataLocker.cs,v 1.4 2012/01/10 22:19:52 J.Rowe Exp $

#endregion

using System;
using QuestIntegrity.LifeQuest.Common.Data.DataCache;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    /// <summary>
    ///     Class to wrap FileDataInfo file loading / unloading with a "using()" statement.
    ///     This class should be used when loading and unloading files for discrete actions.
    ///     It will make sure the load() and unload() calls are paired and the cache
    ///     is managed appropriately.
    /// </summary>
    public class FileDataLocker : IDisposable
    {
        #region Private Properties

        private readonly FileDataInfo _file;
        private readonly int _lockPriority;
        
        #endregion

        #region Constructors

        /// <summary>  Initializes a new instance of the <see cref="FileDataLocker" /> class. Locks one specified file. </summary>
        public FileDataLocker(FileDataInfo file, Scalar[] scalarsToLoad, int lockPriority)
        {
            _file = file;
            _lockPriority = lockPriority;
            file.LoadData(_lockPriority, scalarsToLoad);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _file.UnloadData(_lockPriority);
        }

        #endregion
    }
}