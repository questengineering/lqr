#region Copyright Quest Integrity Group, LLC 2012
//
// www.questintegritygroup.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/17/2012 12:20:32 PM
// Created by:   j.rowe
//
// File:  $Id:$
#endregion

using System.ComponentModel;

namespace QuestIntegrity.LifeQuest.Common.Data
{
    [TypeConverter(typeof(LocalizedEnumConverter))]
    public enum RSFThicknessSource
    {
        ThicknessFromNominal,
        ThicknessFromMedian,
        RadiusFromNominal,
        RadiusFromMedian
    }
}
