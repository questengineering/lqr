﻿
namespace QuestIntegrity.LifeQuest.Common.Data.Features
{
    /// <summary>
    /// Base class defintion for FeatureTypes, expand by inheriting FeatureType into a separate class and adding static readonly CustomFeatureTypes and returning names from a CustomFeatureTypeDisplayNames resource file.
    /// </summary>
    public class FeatureType: System.Object
    {
        public static readonly FeatureType None = new FeatureType("None");

        public FeatureType() {} //Needed for XML serialization.

        protected FeatureType(string TheValue)
        {
            Value = TheValue;
        }

        public override string ToString()
        {
            return Value == null ? @"Feature Not In Resource File" : FeatureTypeDisplayNames.ResourceManager.GetString(Value);
        }

        public string Value { get; set; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //These are utilized because XMLDeserialized versions of statics(like FeatureType.None) will not equality-compare with instantiated versions at runtime as expected.
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(System.Object obj)
        {
            if (obj == null) return false; // fails existance
            FeatureType typedObj = obj as FeatureType;
            if (typedObj == null) return false; // fails cast.
            return (string.Equals(Value, typedObj.Value));
        }

        protected bool Equals(FeatureType other)
        {
            return string.Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        public static bool operator ==(FeatureType A, FeatureType B)
        {
            if (ReferenceEquals(A, B)) return true; //Means the same instance, or both null.
            if ((object)A == null || (object)B == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(A.Value, B.Value);
        }

        public static bool operator !=(FeatureType A, FeatureType B)
        {
            return !(A == B);
        }

        #endregion Equality Comparing

    }

    
}
