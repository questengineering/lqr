#region Copyright Quest Integrity Group, LLC 2010

//
// www.QuestIntegrity.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/1/2008 8:59:20 AM
// Created by:   d.revelle
//
// ID: $Id: FeatureInfo.cs,v 1.301 2012/05/09 22:37:10 J.Rowe Exp $
//

#endregion Copyright Quest Integrity Group, LLC 2010

using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Classes;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.LifeQuest.Common.Annotations;
using QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo;

namespace QuestIntegrity.LifeQuest.Common.Data.Features
{
    /// <summary> Class for holding location and detail information about individual piping features </summary>
    [Serializable]
    public class FeatureInfo : IComparable<FeatureInfo>, INotifyPropertyChanged
    {
        #region Private/Protected Variables

        protected SerializableDictionary<string, DescriptiveStatisticData<float>> ScalarStatistics = new SerializableDictionary<string, DescriptiveStatisticData<float>>();
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [XmlIgnore]
        public virtual InspectionFile ParentInspection {get; protected set;}
        public string ParentInspectionName { get; set; }
        public Guid ParentInspectionGuid { get; set; }
        private bool _isLoaded;
        private bool _isLocked;
        private DateTime _dateCreated;
        private string _createdBy;
        private string _label;
        private int _sliceIndexStart;
        private int _sliceIndexEnd;
        private int _radialIndexStart;
        private int _radialIndexEnd;

        #endregion Private/Protected Variables

        #region Constructors

        public FeatureInfo()
        {
            GUID = Guid.NewGuid();
            DateCreated = DateTime.Today;
            CreatedBy = Environment.UserName;
        }

        #endregion Constructors

                
        #region Public Methods
        
        /// <summary>
        ///     Gets or sets the a property value with the specified property name.
        /// </summary>
        [XmlIgnore]
        public object this[string PropertyName]
        {
            get
            {
                return this.GetPropertyValue(PropertyName);
            }
            set
            {
                this.SetPropertyValue(PropertyName, value);
            }
        }

        protected virtual InspectionFile GetParentInspection()
        {
            throw new NotImplementedException("Inherited FeatureInfos must be able to find their parents.");
        }

        #endregion Public Methods

        public override string ToString()
        {
            return Label.ToString(string.Empty);
        }

        public int CompareTo(FeatureInfo That)
        {
            if (SliceIndexStart > That.SliceIndexStart) return -1;
            if (SliceIndexStart == That.SliceIndexStart) return 0;
            return 1;
        }

        #region Public Properties
        
        public Guid GUID { get; set; }

        public string Label
        {
            get { return _label; }
            set
            {
                if (value == _label) return;
                _label = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Global Index of the pipe segment of the start of this feature
        /// </summary>
        public int SliceIndexStart
        {
            get { return _sliceIndexStart; }
            set
            {
                if (value == _sliceIndexStart) return;
                _sliceIndexStart = value;
                OnPropertyChanged();
                OnPropertyChanged("LengthInSlices");
                OnPropertyChanged("StartPosition");
                OnPropertyChanged("PositionInDisplayUnits");
            }
        }

        /// <summary>
        ///     Global Index of the pipe segment of the end of this feature
        /// </summary>
        public virtual int SliceIndexEnd
        {
            get { return _sliceIndexEnd; }
            set
            {
                if (value == _sliceIndexEnd) return;
                _sliceIndexEnd = value;
                OnPropertyChanged();
                OnPropertyChanged("LengthInSlices");
            }
        }

        /// <summary>
        ///     Zero-based index of the radial location (a.k.a sensor number) of the start of this feature. NOT the vtk index.
        /// </summary>
        public virtual int RadialIndexStart
        {
            get { return _radialIndexStart; }
            set
            {
                if (value == _radialIndexStart) return;
                _radialIndexStart = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Index of the radial location (a.k.a sensor number) of the start of this feature
        /// </summary>
        public virtual int RadialIndexEnd
        {
            get { return _radialIndexEnd; }
            set
            {
                if (value == _radialIndexEnd) return;
                _radialIndexEnd = value;
                OnPropertyChanged();
            }
        }

        public int LengthInSlices
        {
            get
            {
                return SliceIndexEnd - SliceIndexStart + 1; //NOTE: length is edge-to-edge, not center to center, so add 1
            }
        }

        public string Notes { get; set; }

        /// <summary> User that created the feature </summary>
        public string CreatedBy
        {
            get { return _createdBy; }
            set
            {
                if (value == _createdBy) return;
                _createdBy = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     DateTime that the feature was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set
            {
                if (value.Equals(_dateCreated)) return;
                _dateCreated = value;
                OnPropertyChanged();
            }
        }

        public bool IsLocked
        {
            get { return _isLocked; }
            set
            {
                if (value.Equals(_isLocked)) return;
                _isLocked = value;
                OnPropertyChanged();
            }
        }

        public bool IsLoaded
        {
            get { return _isLoaded; }
            set
            {
                if (value.Equals(_isLoaded)) return;
                _isLoaded = value;
                OnPropertyChanged();
            }
        }

        /// <summary> Position of the beginning of the feature. </summary>
        public double StartPosition
        {
            get { return GetPosition(SliceIndexStart); }
        }

        /// <summary> Global position of the beginning of the feature in Display Units. </summary>
        public double PositionInDisplayUnits
        {
            get { return StartPosition * DisplayUnitsAxialFactor; }
        }

        protected virtual float GetPosition(int SliceIndex)
        {
            throw new NotImplementedException("Base feature class unable to obtain position values, and must be implemented in inherited classes.");
        }

        protected virtual float DisplayUnitsAxialFactor
        {
            get { throw new NotImplementedException("Base Feature class unable to determine the display units factor, and must be implemented in inherited classes."); }
        }

        #endregion Public Properties

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}