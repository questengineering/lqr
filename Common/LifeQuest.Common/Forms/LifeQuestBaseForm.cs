﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//
#endregion

using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    /// <summary>
    /// This is a base form for inheritance by LifeQuest forms such as
    /// LifeQuestBaseView, etc
    /// Add GUI elements and logic common to all forms to this class
    /// </summary>
    public partial class LifeQuestBaseForm : UserControl,
                                             ILifeQuestBaseForm,
                                             ILicenseBasedDisplay
    {
        #region Private/Protected Properties

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors

        public LifeQuestBaseForm()
        {
            FormDockingStyle = DockingStyle.Left;
            AddToDockPanel = true;
            InitializeComponent();
            SetTopLevel(false);
            InitialFormHeight = Height;
            InitialFormWidth = Width;
        }

        #endregion

        #region Public Events

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets/Sets the default docking position for the form
        /// </summary>
        [Description("Gets/Sets the default docking position for this form")]
        public DockingStyle FormDockingStyle { get; set; }

        /// <summary>
        /// Gets/Sets whether this form is added to an existing panel added to its own panel
        /// </summary>
        [Description("Gets/Sets whether this form is added to an existing panel added to its own panel")]
        public bool AddToDockPanel { get; set; }

        /// <summary>
        /// Gets/Sets whether this form is duplicated or the existing form is shown
        /// </summary>
        [Description("Gets/Sets whether this form is duplicated or the existing form is shown")]
        public bool Duplicatable { get; set; }

        /// <summary>
        /// Gets/Sets whether this form should be show in its own window
        /// </summary>
        [Description("Gets/Sets whether this form should be show in its own window")]
        public bool IsStandAlone { get; set; }

        /// <summary>
        /// Gets/Sets whether this form should be auto-hidden by default
        /// </summary>
        [Description("Gets/Sets whether this form should be auto-hidden by default")]
        public bool AutoHide { get; set; }

        /// <summary>
        /// Gets the default docking position of this form
        /// </summary>
        public System.Drawing.Point DefaultDockingStyle { get; set; }

        [Browsable(true), Description("The text that is displayed on the docking panel header when the form is docked")]
        public string TabText { get; set; }

        [Browsable(true), Description("The initial width to display the form")]
        public int InitialFormWidth { get; set; }

        [Browsable(true), Description("The initial height to display the form")]
        public int InitialFormHeight { get; set; }

        [Browsable(true)]
        [Description("Whether this form is in viewer only mode.")]
        public bool IsViewerOnlyMode
        {
            get { return QuestIntegrity.Core.Licensing.LicenseManager.Instance.IsViewerOnly; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sets conditional display based on the Licensing.
        /// Override this in derived classes to implement form-specific functionality.
        /// </summary>
        public virtual void SetLicenseBasedDisplay()
        {
            return;
        }

        public string GetFormText()
        {
            return Text;
        }

        public string GetTabText()
        {
            return TabText;
        }

        public void SetVisibility(bool value)
        {
            Visible = value;
        }

        

        #endregion

        #region Private/Protected Methods

        

        #endregion
    }
}