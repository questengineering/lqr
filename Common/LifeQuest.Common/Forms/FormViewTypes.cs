﻿#region Copyright Quest Integrity, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 2/24/2012 11:50:18 AM
// Created by:   C.Turiano
//
#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    /// <summary>
    /// Base class defintion for view types, expand by inheriting ViewType into a separate class and adding static readonly CustomViewTypes. Example in source.
    /// </summary>
    public class ViewType
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static readonly ViewType None = new ViewType("None", null);

        protected ViewType(string theValue, Type theViewType)
        {
            Value = theValue;
            ObjectType = theViewType;
        }

        public override string ToString()
        {
            return Value;
        }
    
        public string Value { get; private set; }
        public Type ObjectType { get; private set; }
    }

    //Sample code for implementation of ViewType, FormType is similar. Inherit ViewType and make new static items that can be type-safely specified elsewhere in code.

    //public sealed class ReformerViewType : ViewType
    //{
    //    public static readonly ReformerViewType ReformerXyPlot = new ReformerViewType("ReformerXyPlot", typeof(ReformerXyPlot));
    //    public static readonly ReformerViewType ReformerSingle3DView = new ReformerViewType("ReformerSingle3DView", typeof(ReformerSingle3DView));

    //    private ReformerViewType(string value, Type theViewType)
    //        : base(value, theViewType)
    //    {
    //    }
    //}

    //This can then be utilized to create forms by referencing their type as below
    // ReformerBaseForm someForm = (ReformerBaseForm)Activator.CreateInstance(theFormType.ObjectType, theFormType);

    
    public class FormType
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static readonly FormType None = new FormType("None", null);

        protected FormType(string theValue, Type theFormType)
        {
            Value = theValue;
            ObjectType = theFormType;
        }

        public override string ToString()
        {
            return Value;
        }
    
        public string Value { get; private set; }
        public Type ObjectType { get; private set; }
    }
}
