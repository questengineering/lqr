﻿using System.Reflection;
using System.Windows.Forms;
using log4net;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    /// <summary> Provides the base look and feel for a form that needs the proper outline color and no control box. </summary>
    public partial class QIGBackColorForm : Form
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public QIGBackColorForm()
        {
            InitializeComponent();
        }
    }
}
