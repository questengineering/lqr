#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//

#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using QuestIntegrity.Core.Licensing;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    public partial class Main : Form, IMain
    {
        #region Private Properties
        protected bool LockWindows;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Contructors

        protected Main()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties

        public float DpiX
        {
            get
            {
                return CreateGraphics().DpiX;
            }
        }

        public IWin32Window MainForm
        {
            get { return this; }
        }

        #endregion

        #region Public Methods

        /// <summary> Set the cursor </summary>
        public void SetCursor(Cursor cursor) { Cursor.Current = cursor; }

        /// <summary>
        ///     Enable or disable the main form
        /// </summary>
        /// <param name="enableForm">Whether or not to enable the form</param>
        public void SetEnabled(bool enableForm)
        {
            Enabled = enableForm;
        }

        /// <summary>
        ///     Suspends the forms drawing
        /// </summary>
        public void SuspendFormDrawing()
        {
            SuspendFormLayout();
        }

        /// <summary>
        ///     Suspend layout
        /// </summary>
        public void SuspendFormLayout()
        {
            SuspendLayout();
        }

        /// <summary>
        ///     Resume layout
        /// </summary>
        /// <param name="performLayout"></param>
        public void ResumeFormLayout(bool performLayout)
        {
            ResumeLayout(performLayout);
        }

        /// <summary>
        ///     Resumes the forms drawing
        /// </summary>
        /// <param name="doRedraw"></param>
        public void ResumeFormDrawing(bool doRedraw)
        {
            ResumeFormLayout(true);
        }

        /// <summary>
        ///     Refresh the form
        /// </summary>
        public void RefreshForm()
        {
            Refresh();
        }

        public void ActivateForm()
        {
            Activate();
        }

        /// <summary>
        ///     Display a message with particular caption
        /// </summary>
        /// <param name="message">Message to show</param>
        /// <param name="caption">Caption to display</param>
        public void ShowMessage(string message,
                                string caption)
        {
            MessageBox.Show(this, message, caption);
        }

        /// <summary>
        ///     Show a message
        /// </summary>
        /// <param name="message">Message to show</param>
        public void ShowMessage(string message)
        {
            MessageBox.Show(this, message); 
        }

        /// <summary> Show a message, checking for thread safety. </summary>
        public DialogResult ShowMessage(string text,
                                        string caption,
                                        MessageBoxButtons buttons,
                                        MessageBoxIcon icon)
        {
            if (!InvokeRequired) return MessageBox.Show(this, text, caption, buttons, MessageBoxIcon.Exclamation);
            //Otherwise have to get on the right thread.
            DialogResult answer = DialogResult.OK;
            Invoke(new Action(() => answer = ShowMessage(text, caption, buttons, icon)));
            return answer;
        }

        /// <summary> Closes all the panels </summary>
        public void CloseAllPanels()
        {
            CloseDockedPanels();
            CloseTabbedPanels();
            CloseFloatingPanels();
        }

        /// <summary>
        ///     Closes all the dockManager's docked panels
        /// </summary>
        /// <remarks>This is called after the user has clicked "Close Project" by the WindowManager.CloseAllDocuments()</remarks>
        public void CloseDockedPanels()
        {
            List<DockPanel> panelsToClose = dockManager.Panels.Cast<DockPanel>().ToList();
            panelsToClose.ForEach(P => P.Close());
        }

        /// <summary> Closes all the documentManager's tabbed panels </summary>
        /// <remarks>This is called after the user has clicked "Close Project" by the WindowManager.CloseAllDocuments()</remarks>
        public void CloseTabbedPanels()
        {
            //Clear the document manager
            if (documentManager.View == null) return;
            documentManager.View.Documents.ToList().ForEach(D => D.Dispose());
            documentManager.View.Documents.Clear();
        }

        /// <summary> Closes all the documentManager's floating panels </summary>
        /// <remarks>This is called after the user has clicked "Close Project" by the WindowManager.CloseAllDocuments()</remarks>
        public void CloseFloatingPanels()
        {
            if (documentManager.ViewCollection == null) return;
            foreach (DevExpress.XtraBars.Docking2010.Views.BaseView bv in documentManager.ViewCollection)
            {
                bv.Controller.CloseAll();
            }
        }

        /// <summary> Closes a form and it's associated DockPanel, accounting for if that DockPanel has child DockPanels </summary>
        /// <param name="form">The LifeQuestBaseForm to close.</param>
        public void ClosePanel(ILifeQuestBaseForm form)
        {
            //try to find the form already in a DockPanel
            // loop through all the DockPanels
            foreach (DockPanel dp in dockManager.Panels)
            {
                if (dp.ControlContainer != null)
                {
                    // loop through all the controls in that DockPanel
                    foreach (Control c in dp.ControlContainer.Controls)
                    {
                        // We found the form
                        if (c == form)
                        {
                            // don't need to close the form as DockPanel_PanelClosing handles that
                            // Just need to check if this DockPanel has child DockPanels

                            // check if this DockPanel has child panels
                            if (dp.Count == 0)
                            {
                                dp.Close();
                            }
                            else
                            {
                                for (int i = 0; i < dp.Count; i++)
                                {
                                    dp.ParentPanel.DockAsTab(dp[i]);
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     If a form can only be shown once, this attempts to find an existing version of the form
        ///     and show it.  If it can't find such a form, is calls SetupForm(form).
        /// </summary>
        public void ShowForm(LifeQuestBaseForm TheForm)
        {
            if (TheForm.Duplicatable) return;

            // try to find the form already in a DockPanel
            foreach (DockPanel dp in from dp in dockManager.Panels.Cast<DockPanel>() where dp.ControlContainer != null select dp)
            {
                foreach (Control c in from Control c in dp.ControlContainer.Controls where c == TheForm select c)
                {
                    // If the form is on the left, we need to change tabs, otherwise, just focus it
                    if (dp.ParentPanel != null && TheForm.FormDockingStyle == DockingStyle.Left)
                    {
                        dp.ParentPanel.ActiveChildIndex = dp.Index;
                        ((LifeQuestBaseForm)c).SetVisibility(true);
                    }
                    else
                    {
                        c.Focus();
                    }
                    return;
                }
            }

            // try to find the form already in the documentManager(tabbed view) and show it
            foreach (Document d in from d in documentManager.View.Documents where d.Control == TheForm select d)
            {
                documentManager.View.Controller.Activate(d);
                return;
            }
        }

        /// <summary>
        ///     Put the view onto a dock panel and set common properties
        /// </summary>
        /// <param name="form">The form that we are about to show</param>
        /// <param name="dockingStyle">Optional parameter to override the docking style specified on the form</param>
        /// <param name="autoHide"></param>
        public void SetupForm(LifeQuestBaseForm form,
                              DockingStyle dockingStyle = (DockingStyle)(-1),
                              bool autoHide = false)
        {
            if (form == null)
                return;
            
            DockPanel dp = null;
            DockingStyle dockStyle = form.FormDockingStyle;
            if ((int)dockingStyle > 0)
            {
                dockStyle = dockingStyle;
            }

            bool alreadyDocked = false;
            try
            {
                // Look for a dock panel with the same docking style that already exists
                foreach (DockPanel rp in dockManager.RootPanels)
                {
                    if (rp.Dock == dockStyle)
                    {
                        switch (rp.Dock)
                        {
                            case DockingStyle.Bottom:
                            case DockingStyle.Left:
                                dp = rp.AddPanel();
                                dp.ControlContainer.Controls.Add(form);
                                dp.ParentPanel.ActiveChildIndex = dp.Index;
                                dp.ParentPanel.Tabbed = true;
                                alreadyDocked = true;
                                break;
                            case DockingStyle.Right:
                                dp = rp.AddPanel();
                                dp.ControlContainer.Controls.Add(form);
                                dp.ParentPanel.ActiveChildIndex = dp.Index;
                                alreadyDocked = true;
                                break;
                            case DockingStyle.Top:
                                dp = dockManager.AddPanel(form.FormDockingStyle);
                                dp.ControlContainer.Controls.Add(form);
                                alreadyDocked = true;
                                break;
                        }
                        break;
                    }
                }

                // if the new panel hasn't already been docked, do so now according to the dock style
                if (!alreadyDocked)
                {
                    switch (form.FormDockingStyle)
                    {
                        case DockingStyle.Top:
                        case DockingStyle.Fill:
                            //Put the new document as a tab in an existing tab group or as a new tab group. The tab will be added to the 'active' tab group if not creating its own.
                            //There is one empty document group by default in a Tabbed view, and creating a new one uses that first slot that is supposedly not empty.
                            //This means that this will allow 2 document groups to be created. Counter-intuitive, but a DevExpress intricacy.
                            //The forms are added directly into the tabbed view. If placed onto an explicitly created dockpanel, they will not work right (flash controls)

                            TabbedView view = documentManager.View as TabbedView;
                            if (view != null)
                            {
                                ITabbedViewController controller = view.Controller;

                                view.AddDocument(form);
                                Document doc = (Document)view.Documents[documentManager.View.Documents.Count - 1];
                                doc.Caption = form.TabText;
                                doc.Disposed += (Sender, Args) => OnPanelClosed(form);
                                if (view.DocumentGroups.Count <= 1)
                                {
                                    controller.CreateNewDocumentGroup(doc, Orientation.Horizontal, documentManager.View.Documents.Count - 1);

                                    int height = documentManager.View.Bounds.Height;
                                    if (view.DocumentGroups.Count == 1)
                                        view.DocumentGroups[0].GroupLength = height;
                                    else
                                    {
                                        view.DocumentGroups[0].GroupLength = (int)((2.0 / 3.0) * height);
                                        view.DocumentGroups[1].GroupLength = (int)((1.0 / 3.0) * height);
                                    }
                                }
                            }

                            break;
                        case DockingStyle.Right:
                        case DockingStyle.Left:
                        case DockingStyle.Float:
                            dp = dockManager.AddPanel(form.FormDockingStyle);
                            dp.ControlContainer.Controls.Add(form);
                            dp.FloatSize = new Size(700, 500);
                            break;
                        case DockingStyle.Bottom:
                        default:
                            dp = dockManager.AddPanel(form.FormDockingStyle);
                            dp.ControlContainer.Controls.Add(form);
                            break;
                    }
                }

                if (dp != null)
                {
                    //dp.Layout += DockPanel_Layout;
                    dp.TabText = form.TabText;
                    dp.Text = form.TabText;
                    dp.Visibility = (autoHide) ? DockVisibility.AutoHide : DockVisibility.Visible;
                    dp.ClosedPanel += (Sender, Args) => OnPanelClosed(form);

                    // Hide the pin button if we are in viewer only mode - see case 3039
                    if (LicenseManager.Instance.IsViewerOnly)
                    {
                        dp.Options.ShowAutoHideButton = false;
                        // This removes the pin from the tabbed dock panel on the left
                        if (dp.ParentPanel != null)
                        {
                            dp.ParentPanel.Options.ShowAutoHideButton = false;
                        }
                    }
                }

                form.SetVisibility(true);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Main::SetupForm() has thrown an exception: {0}", e), e);
            }
        }

        protected virtual void OnPanelClosed(LifeQuestBaseForm Form)
        {
            //Do nothing in the base implementation.
        }

        #endregion

        
        /// <summary>
        ///     Set up the menus based on project state
        /// </summary>
        /// <param name="State">ProjectState enum that indicates the current project state.</param>
        protected void SetMenusForFileLoadedState(ProjectState State)
        {
            switch (State)
            {
                case ProjectState.NoProject:
                    SetProjectMenus(false);
                    SetInspectionLoadedMenus(false);
                    break;
                case ProjectState.ProjectLoaded:
                    SetProjectMenus(true);
                    SetInspectionLoadedMenus(false);
                    break;
                case ProjectState.SingleInspectionFileLoaded:
                case ProjectState.MultipleInspectionFilesLoaded:
                default:
                    SetProjectMenus(true);
                    SetInspectionLoadedMenus(true);
                    break;
            }
        }

        /// <summary>
        ///     Set up the project-required menus
        /// </summary>
        /// <param name="projectLoaded">Boolean indicating whether or not a project has been loaded.</param>
        protected virtual void SetProjectMenus(bool projectLoaded)
        {
            
        }

        /// <summary>
        ///     Set up the inspection-required menus
        /// </summary>
        /// <param name="inspectionFileLoaded">Boolean indicating whether or not an inspection is loaded.</param>
        protected virtual void SetInspectionLoadedMenus(bool inspectionFileLoaded)
        {
            
        }

        /// <summary>
        ///     Sets the menus based on which type of license the user has.
        /// </summary>
        private void SetMenusBasedOnLicense()
        {
            
        }

        /// <summary>
        ///     Event Handler to handle the dockManager's StartDocking event.
        ///     This handler allows or disallows the docking of all its DockPanels
        /// </summary>
        private void DockManager_StartDocking(object sender,
                                              DockPanelCancelEventArgs e)
        {
            if (e != null)
            {
                e.Cancel = LockWindows;
            }
        }

        /// <summary>
        ///     Event Handler that fires when a Panel is docking from the tabbedView.
        ///     Allows or disallows a panel to dock or float based on whether the menuWindowLock is checked.
        /// </summary>
        /// <param name="sender">Main form.</param>
        /// <param name="e">Cancel args</param>
        private void TabbedView_BeginDocking(object sender,
                                             DevExpress.XtraBars.Docking2010.Views.DocumentCancelEventArgs e)
        {
            if (e != null)
            {
                e.Cancel = LockWindows;
            }
        }

        /// <summary>
        ///     Event Handler that fires when a Panel is floating from the tabbedView.
        ///     Allows or disallows a panel to dock or float based on whether the menuWindowLock is checked.
        /// </summary>
        /// <param name="sender">Main form.</param>
        /// <param name="e">Cancel args</param>
        private void TabbedView_BeginFloating(object sender,
                                              DevExpress.XtraBars.Docking2010.Views.DocumentCancelEventArgs e)
        {
            if (e != null)
            {
                e.Cancel = LockWindows;
            }
        }

        
        private void fileExit_ItemClick(object sender,
                                        ItemClickEventArgs e)
        {
            Close();
        }

        
    }

    
}