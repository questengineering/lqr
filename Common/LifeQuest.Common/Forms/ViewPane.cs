﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: ViewPane.cs,v 1.120 2011/02/25 00:04:58 J.Rowe Exp $
//
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Kitware.VTK;
using QuestIntegrity.Collections.Generic;
using QuestIntegrity.Graphics;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.EventArguments;
using QuestIntegrity.LifeQuest.Common.Forms.Controls;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using QuestIntegrity.Maths;
using QuestIntegrity.Windows.Forms;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    public partial class ViewPane : LifeQuestBaseView, IGridDisplay, IClipboardCopy, IFileCopy, IDataToolTip
    {
        #region Private/Protected Variables

        protected bool _displayDataTips;
        protected bool _formClosing;
        protected bool _formLoading = true;
        private bool _dataLoaded;
        private string _selectedScalarName;
        private List<ScalarArrayRelationship> _currentViewableScalars;
        private PipeGeometries _pipeGeometry;

        protected readonly InteractionMode _supportedInteractionModes = InteractionMode.AdjustColorBar |
                                                                        InteractionMode.DrawFeature |
                                                                        InteractionMode.ZoomProportional |
                                                                        InteractionMode.ZoomStretch |
                                                                        InteractionMode.ZoomRubberband |
                                                                        InteractionMode.ShowDataTips;

        protected InteractionMode _currentInteractionMode = InteractionMode.ZoomStretch;

        protected GraphicsWindow _graphicsWindow;

        // Display properties
        private DisplayProperties _viewDisplayProperties;
        private ColorScaleOptions _ctfForm;

        #endregion

        #region Constructors

        public ViewPane(ViewType type) : base(type)
        {
            InitializeComponent();
            AddToDockPanel = false;

            _viewDisplayProperties = new DisplayProperties();

            _ctfForm = new ColorScaleOptions();
            _ctfForm.RenderingOptionsChanged += LUTColorForm_RenderingOptions;

            //Setup data view bar
            ControlToolTips.SetToolTip(_dataViewBar.ActionButton1, "Reset view to starting orientation");

            ActionRequested += WindowManager.Instance.ViewPane_ActionRequested;
        }

        #endregion

        #region Public Properties

        #region Data / View Boundaries

        public float DataXMin
        {
            get
            {
                return _graphicsWindow.DataXMin;
            }
        }

        public float DataXMax
        {
            get
            {
                return _graphicsWindow.DataXMax;
            }
        }

        public float DataYMin
        {
            get
            {
                return _graphicsWindow.DataYMin;
            }
        }

        public float DataYMax
        {
            get
            {
                return _graphicsWindow.DataYMax;
            }
        }

        public float DataZMin
        {
            get
            {
                return _graphicsWindow.DataZMin;
            }
        }

        public float DataZMax
        {
            get
            {
                return _graphicsWindow.DataZMax;
            }
        }

        public double[] DataCenter
        {
            get
            {
                return _graphicsWindow.DataCenter;
            }
        }

        public float ViewPositionMin
        {
            get
            {
                return _graphicsWindow.ViewPositionMin;
            }
        }

        public float ViewPositionMax
        {
            get
            {
                return _graphicsWindow.ViewPositionMax;
            }
        }

        #endregion

        public ColorScaleBar CSB
        {
            get
            {
                return _graphicsWindow.GraphicsWindowScalarBar;
            }
            set
            {
                _graphicsWindow.GraphicsWindowScalarBar.CopyFrom(value);
                _graphicsWindow.GraphicsWindowScalarBar.Build();
            }
        }

        public SerializableDictionary<MinMaxMode, double[]> ColorScaleMinMaxRange
        {
            get
            {
                return _graphicsWindow.GraphicsWindowScalarBar.ColorScaleMinMaxRanges;
            }
            set
            {
                _graphicsWindow.GraphicsWindowScalarBar.ColorScaleMinMaxRanges = value;
                _graphicsWindow.GraphicsWindowScalarBar.Build();
            }
        }

        public bool DataLoaded
        {
            get
            {
                return _dataLoaded;
            }
        }

        public DisplayProperties ViewDisplayProperties
        {
            get
            {
                return _viewDisplayProperties;
            }
            set
            {
                _viewDisplayProperties = value;
                _graphicsWindow.ViewDisplayProperties = _viewDisplayProperties;
            }
        }

        public string SelectedScalarKey
        {
            get
            {
                ScalarArrayRelationship item = (ScalarArrayRelationship)_dataViewBar.ScalarComboBox.SelectedItem;
                return item.Key;
            }
            set
            {
                SetScalarComboBoxSelection(value);
            }
        }

        public ScalarType SelectedScalarType
        {
            get
            {
                return (ScalarType)Enum.Parse(typeof(ScalarType), SelectedScalarKey, true);
            }
            set
            {
                SetScalarComboBoxSelection(value.ToString());       //Want the Key, not the display value
            }
        }

        public PipeGeometries PipeGeometry
        {
            get
            {
                return _pipeGeometry;
            }
            set
            {
                _pipeGeometry = value;

                if(_pipeGeometry == PipeGeometries.ThreeDTube || _pipeGeometry == PipeGeometries.ThreeDShell)
                {
                    _graphicsWindow.GraphicsWindowAxes.AxisType = Axes.AxisDisplayType.ThreeD;
                }
                else
                {
                    _graphicsWindow.GraphicsWindowAxes.AxisType = Axes.AxisDisplayType.TwoD;
                }
            }
        }

        public vtkDataSet Grid
        {
            get
            {
                return _graphicsWindow.GraphicsWindowPipe.Grid;
            }
        }

        public vtkDataSet SelectionGrid
        {
            get
            {
                return _graphicsWindow.SelectionGrid;
            }
        }

        public InteractionMode CurrentInteractionMode
        {
            get
            {
                return _currentInteractionMode;
            }
            set
            {
                if(_currentInteractionMode != value)  // JTR: 12/29/09 - preventing extraneous changes and redraws
                {
                    _currentInteractionMode = value;
                    _graphicsWindow.CurrentInteractionMode = value;
                    ChangeInteractionMode();
                }
            }
        }

        public virtual InteractionMode SupportedInteractionModes
        {
            get
            {
                return _supportedInteractionModes;
            }
        }

        public List<ScalarArrayRelationship> CurrentViewableScalars
        {
            get
            {
                return _currentViewableScalars;
            }

            set
            {
                int originalIndex = _dataViewBar.ScalarComboBox.SelectedIndex;
                _currentViewableScalars = value;

                // Reset the drop down for viewable scalars.
                _dataViewBar.ScalarComboBox.Items.Clear();
                _dataViewBar.ScalarComboBox.BeginUpdate();
                _dataViewBar.ScalarComboBox.ValueMember = "Key";
                _dataViewBar.ScalarComboBox.DisplayMember = "DisplayName";
                foreach(ScalarArrayRelationship kvp in _currentViewableScalars)
                {
                    if(QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly)  //don't add certain scalars
                    {
                        if(!(kvp.Key == ScalarType.RadiusDecentered.ToString()) &&
                            !(kvp.Key == ScalarType.QualityFactor.ToString()))
                        {
                            _dataViewBar.ScalarComboBox.Items.Add(kvp);
                        }
                    }
                    else
                    {
                        _dataViewBar.ScalarComboBox.Items.Add(kvp);
                    }
                }
                _dataViewBar.ScalarComboBox.EndUpdate();
                if(originalIndex >= 0 && originalIndex < _dataViewBar.ScalarComboBox.Items.Count)
                {
                    _dataViewBar.ScalarComboBox.SelectedIndex = originalIndex;
                }
                else if(_dataViewBar.ScalarComboBox.Items.Count > 0)
                {
                    _dataViewBar.ScalarComboBox.SelectedIndex = 0;
                }
            }
        }

        public CheckBox DisplayOptions1
        {
            get
            {
                return _dataViewBar.DisplayOptions1;
            }
        }

        public CheckBox DisplayOptions2
        {
            get
            {
                return _dataViewBar.DisplayOptions2;
            }
        }

        public ColorInterpolationMode Interpolation
        {
            get
            {
                return _graphicsWindow.Interpolation;
            }
            set
            {
                _graphicsWindow.Interpolation = value;
            }
        }

        /// <summary>
        /// Indicate whether data tool tips should be shown or not
        /// </summary>
        public bool DisplayDataTips
        {
            get
            {
                return _displayDataTips;
            }
            set
            {
                _displayDataTips = value;
                HideDataTip();
            }
        }

        /// <summary>
        /// Length scale of the base data
        /// </summary>
        public Units.LengthScale DataLengthScale
        {
            get
            {
                return _graphicsWindow.DataLengthScale;
            }
            set
            {
                _graphicsWindow.DataLengthScale = value;
            }
        }

        #endregion

        #region Public Methods

        public virtual void ShowDataTip(string toolTip)
        {
            if(toolTip != string.Empty)
            {
                dataTip.Show(toolTip, this);
            }
        }

        public void HideDataTip()
        {
            dataTip.Hide(this);
        }

        public virtual void SetScalar(ScalarArray<float> scalars)
        {
            _graphicsWindow.SetScalarData(scalars.ToArray(), scalars.DisplayName);

            _graphicsWindow.GraphicsWindowScalarBar.ColorScaleMinMaxRanges[MinMaxMode.CurrentlyViewable][0] = scalars.Minimum;
            _graphicsWindow.GraphicsWindowScalarBar.ColorScaleMinMaxRanges[MinMaxMode.CurrentlyViewable][1] = scalars.Maximum;
            _graphicsWindow.GraphicsWindowScalarBar.Build();

            SetScalarComboBoxSelection(scalars.Key);
        }

        public virtual void SetGrid(vtkDataSet grid)
        {
            _dataLoaded = true;
            _graphicsWindow.SetGrid(grid);
        }

        public void SetGrid(vtkDataSet grid, ScalarArray<float> scalars)
        {

            _dataLoaded = true;
            //_selectedScalarKey = scalars.Key;     Using SelectedScalarKey property directly
            _selectedScalarName = scalars.DisplayName;
            _graphicsWindow.SetGrid(grid, scalars.ToArray(), scalars.DisplayName);

            _graphicsWindow.GraphicsWindowScalarBar.Build();

            SetScalarComboBoxSelection(scalars.Key);
        }

        public void Draw()
        {
            Draw(RenderLayers.Foreground | RenderLayers.Annotations | RenderLayers.Background);
        }

        public void Draw(RenderLayers layers)
        {
            _log.Debug("Redraw called.");
            _graphicsWindow.Draw(layers);
            if(!_graphicsWindow.InvokeRequired)
            {
                _graphicsWindow.Refresh();
            }
        }

        public void CenterCamera()
        {
            _graphicsWindow.CenterCamera();
        }

        public void SetCamera(vtkCamera camera)
        {
            throw new NotImplementedException("ViewPane::SetCamera(vtkCamera camera) is not implemented.");
        }

        public void SetCursor(vtkCursor cursor)
        {
            _graphicsWindow.RenderWindow.SetCurrentCursor((int)cursor);
        }

        public void SetViewExtents(double viewExtentPositionMin, double viewExtentPositionMax)
        {
            // Return if the Window Size is 0 or if there is no data.
            if(viewExtentPositionMin == viewExtentPositionMax)
            {
                return;
            }
            _graphicsWindow.SetViewExtents(viewExtentPositionMin, viewExtentPositionMax);
            _log.DebugFormat("View extents set ({0},{1})", viewExtentPositionMin, viewExtentPositionMax);
        }

        public void SetViewExtents(double viewExtentPositionMin, double viewExtentPositionMax, double circumferentialStart, double sweepAngle)
        {
            // Return if the Window Size is 0 or if there is no data.
            if(viewExtentPositionMin == viewExtentPositionMax)
            {
                return;
            }
            //_graphicsWindow.SetViewExtents(viewExtentPositionMin, viewExtentPositionMax);
            _graphicsWindow.SetViewExtents(viewExtentPositionMin, viewExtentPositionMax, circumferentialStart, sweepAngle);
            _log.DebugFormat("View extents set ({0},{1})", viewExtentPositionMin, viewExtentPositionMax);
        }

        public void SetViewExtents()
        {
            _graphicsWindow.SetViewExtents();
        }

        public void AddFeature(Guid uniqueID, string featureLabel, vtkDataSet featureGrid, vtkPolyData outlineGrid, bool isSelected)
        {
            _graphicsWindow.AddFeature(uniqueID, featureLabel, featureGrid, outlineGrid, isSelected);
        }

        public void DeselectAllFeatures()
        {
            _graphicsWindow.DeselectAllFeatures();
        }

        public void ClearFeatureList()
        {
            _graphicsWindow.ClearFeatureList();
        }

        /// <summary>
        /// Adds a Tmin point to be rendered in the foreground
        /// </summary>
        /// <param name="xyz"></param>
        public void AddTmmPoint(GlyphPoint pt)
        {
            _graphicsWindow.AddTmmPt(pt);
        }

        public void ClearTMinList()
        {
            _graphicsWindow.ClearTMinPoints();
        }

        public void AddArrowCaption(string caption)
        {
            _graphicsWindow.AddArrowCaption(caption);
        }

        public void AddArrowCaption(string caption, double width, double height, double[] attachmentPostion, CaptionPosition captionPositon)
        {
            _graphicsWindow.AddArrowCaption(caption, width, height, attachmentPostion, captionPositon);
        }

        public void ClearArrowCaptionList()
        {
            _graphicsWindow.ClearArrowCaptionList();
        }

        public void AddSelection(CartesianBounds<double> bounds)
        {
            _graphicsWindow.AddSelection(bounds);
        }

        public void AddSelection(vtkDataSet selectionGrid)
        {
            _graphicsWindow.AddSelection(selectionGrid);
        }

        public void ClearSelection()
        {
            _graphicsWindow.ClearSelection();
        }

        #endregion

        #region Private Methods

        private void ViewPaneDisplayFeatures_CheckedChanged(object sender, EventArgs e)
        {
            _graphicsWindow.DisplayFeatures = DisplayOptions1.Checked;
            Draw();
        }

        private void SetScalarComboBoxSelection(string key)
        {
            ComboBox scalarList = _dataViewBar.ScalarComboBox;
            if(((ScalarArrayRelationship)scalarList.SelectedItem).Key != key)
            {
                for(int i = 0; i < scalarList.Items.Count; i++)
                {
                    ScalarArrayRelationship value = (ScalarArrayRelationship)scalarList.Items[i];
                    if(value.Key == key)
                    {
                        scalarList.SelectedIndex = i;
                        break;
                    }
                }
                UpdateCurrentScalar();
            }
        }

        protected virtual void UpdateCurrentScalar()
        {
            // Raise an even that the scalar type to be displayed has been changed.
            ScalarArrayRelationship item = (ScalarArrayRelationship)_dataViewBar.ScalarComboBox.SelectedItem;
            OnScalarChanged(new ScalarChangedEventArgs(item.Key));
        }

        protected virtual void ChangeInteractionMode()
        {
            // Set the scalarbar enabled property
            if((_currentInteractionMode & InteractionMode.AdjustColorBar) == InteractionMode.AdjustColorBar)
            {
                _graphicsWindow.SetScalarBarEditMode(true);
            }
            else
            {
                _graphicsWindow.SetScalarBarEditMode(false);
            }

        }

        private void ViewPane_Shown(object sender, EventArgs e)
        {
            _formLoading = false;
            SetViewExtents(ViewPositionMin, ViewPositionMax);
            Draw();

            // Set default tooltips for controls
            ControlToolTips.SetToolTip(_dataViewBar.Options, "Change color scale and display options");
            ControlToolTips.SetToolTip(_dataViewBar.ScalarComboBox, "Choose data to display");
        }

        protected virtual void DataViewBar_Load(object sender, EventArgs e)
        {
            // TODO Why have this method if nothing happens in it and its not overridden anywhere?

            // throw new NotImplementedException("ViewPane::DataViewBar_Load(object sender, EventArgs e) not implemented.  Inheriting class must implement.");
        }

        #region Menu Click Handlers

        private void menuFeatureDelete_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureInvert_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeLeft_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeRight_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeUp_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeDown_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowWider_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowNarrower_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowShorter_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowLonger_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureSetLongSeamLocation_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeaturePickMode_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGirthWeld_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureFlange_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBend_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBendStart_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBendEnd_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWeldCirc_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureTakeOff_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureTap_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureValve_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureAppurtenance_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWeld_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExtraMetal_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureInternalWallLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWallLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureDent_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureDentWithMetalLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureOvality_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureLamination_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUnknown_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD1_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD2_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD3_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD4_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        #endregion

        #endregion

        #region Events

        //protected void OnViewExtentsChanging(ViewExtentsChangedEventArgs e)
        //{
        //    if(ViewExtentsChanging != null)
        //        ViewExtentsChanging(this, e);
        //}

        //protected void OnViewExtentsChanged(ViewExtentsChangedEventArgs e)
        //{
        //    if(ViewExtentsChanged != null)
        //        ViewExtentsChanged(this, e);
        //}

        //protected void OnCameraChanged(CameraChangedEventArgs e)
        //{
        //    if(CameraChanged != null)
        //        CameraChanged(this, e);
        //}

        //protected void OnScalarChanged(ScalarChangedEventArgs e)
        //{
        //    if(ScalarChanged != null)
        //        ScalarChanged(this, e);
        //}

        //protected void OnFeatureAreaPicked(GridSelectionEventArgs e)
        //{
        //    if(FeatureAreaPicked != null)
        //        FeatureAreaPicked(this, e);
        //}

        //protected void OnFeatureSelected(FeatureSelectedEventArgs e)
        //{
        //    if(FeatureSelected != null)
        //        FeatureSelected(this, e);
        //}

        //protected void OnColorScaleChanged(ColorScaleOptionsEventArgs e)
        //{
        //    if(ColorScaleChanged != null)
        //        ColorScaleChanged(this, e);
        //}

        //protected void OnActionRequested(ActionEventArgs e)
        //{
        //    if(ActionRequested != null)
        //        ActionRequested(this, e);
        //}

        protected void _graphicsWindow_FeatureAreaPicked(object sender, GridSelectionEventArgs e)
        {
            OnFeatureAreaPicked(e);
        }

        protected void _graphicsWindow_FeatureSelected(object sender, FeatureSelectedEventArgs e)
        {
            OnFeatureSelected(e);
        }

        protected void _graphicsWindow_CameraExtentsChanged(object sender, ViewExtentsChangedEventArgs e)
        {
            OnViewExtentsChanged(e);
        }

        public void LUTColorForm_RenderingOptions(object sender, ColorScaleOptionsEventArgs e)
        {
            OnColorScaleChanged(e);
        }

        protected virtual void DataViewBarOptions_Click(object sender, EventArgs e)
        {
            OnViewableScalarRangeChanged(new ViewExtentsChangedEventArgs(ViewPositionMin, ViewPositionMax));
            _ctfForm.SetCSB(_graphicsWindow.GraphicsWindowScalarBar);
            _ctfForm.ShowDialog();
        }

        protected virtual void ViewPane_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formClosing = true;
        }

        #endregion

        #region IClipboardCopy Members

        public void CopySelectedToClipboard()
        {
            if(_graphicsWindow != null)
            {
                _graphicsWindow.CopyToClipboard();
            }
        }

        public void SelectAll()
        {
            // null op.
        }

        public void SelectNone()
        {
            // null op.
        }

        #endregion

        #region IFileCopy Members

        public void CopySelectedToFile(string fileName)
        {
            if(_graphicsWindow != null)
            {
                _graphicsWindow.CopyToFile(fileName);
            }
        }

        #endregion
    }
}