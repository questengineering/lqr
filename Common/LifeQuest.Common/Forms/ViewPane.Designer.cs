﻿using QuestIntegrity.Common;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    partial class ViewPane
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                _graphicsWindow.Dispose();

                if(disposing && (components != null))
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._dataViewBar = new QuestIntegrity.LifeQuest.Common.Forms.Controls.DataViewBar();
            this.dataTip = new System.Windows.Forms.ToolTip(this.components);
            this.ControlToolTips = new System.Windows.Forms.ToolTip(this.components);
            this.menuFeature = new DevExpress.XtraBars.BarSubItem();
            this.menuFeatureResizeFeatures = new DevExpress.XtraBars.BarSubItem();
            this.menuFeaturePickMode = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureGirthWeld = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureFlange = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBend = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBendStart = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBendEnd = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWeldCirc = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureTakeOff = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureTap = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureValve = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureAppurtenance = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWeld = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExtraMetal = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureInternalWallLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWallLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureDent = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureDentWithMetalLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureOvality = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureLamination = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUnknown = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD1 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD2 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD3 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD4 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureDelete = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureInvert = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeLeft = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeRight = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeUp = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeDown = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowWider = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowNarrower = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowShorter = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowLonger = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureSetLongSeamLocation = new DevExpress.XtraBars.BarButtonItem();
            this.SuspendLayout();
            // 
            // _dataViewBar
            // 
            this._dataViewBar.Dock = System.Windows.Forms.DockStyle.Top;
            this._dataViewBar.Location = new System.Drawing.Point(0, 0);
            this._dataViewBar.Name = "_dataViewBar";
            this._dataViewBar.ShowScalarCombo = true;
            this._dataViewBar.Size = new System.Drawing.Size(736, 31);
            this._dataViewBar.TabIndex = 0;
            this._dataViewBar.DisplayOptions1.Visible = true;
            this._dataViewBar.DisplayOptions2.Visible = false;
            this._dataViewBar.DisplayOptions1.Text = "Features";
            this._dataViewBar.DisplayOptions1.Checked = true;
            this._dataViewBar.DisplayOptions1.CheckedChanged += ViewPaneDisplayFeatures_CheckedChanged;
            this._dataViewBar.Options.Click += DataViewBarOptions_Click;
            //
            // barManager
            //
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[]
            {
                this.menuFeature,
                this.menuFeatureResizeFeatures,
                this.menuFeaturePickMode,
                this.menuFeatureGirthWeld,
                this.menuFeatureFlange,
                this.menuFeatureBend,
                this.menuFeatureBendStart,
                this.menuFeatureBendEnd,
                this.menuFeatureExternalWeldCirc,
                this.menuFeatureTakeOff,
                this.menuFeatureTap,
                this.menuFeatureValve,
                this.menuFeatureAppurtenance,
                this.menuFeatureExternalWeld,
                this.menuFeatureExtraMetal,
                this.menuFeatureInternalWallLoss,
                this.menuFeatureExternalWallLoss,
                this.menuFeatureDent,
                this.menuFeatureDentWithMetalLoss,
                this.menuFeatureOvality,
                this.menuFeatureLamination,
                this.menuFeatureUnknown,
                this.menuFeatureUD1,
                this.menuFeatureUD2,
                this.menuFeatureUD3,
                this.menuFeatureUD4,
                this.menuFeatureDelete,
                this.menuFeatureInvert,
                this.menuFeatureNudgeLeft,
                this.menuFeatureNudgeRight,
                this.menuFeatureNudgeUp,
                this.menuFeatureNudgeDown,
                this.menuFeatureGrowWider,
                this.menuFeatureGrowNarrower,
                this.menuFeatureGrowShorter,
                this.menuFeatureGrowLonger,
                this.menuFeatureSetLongSeamLocation
            });
            // 
            // barMenu
            //
            this.barMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]
            {
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeature)
            });
            // 
            // barTools
            //
            this.barTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]
            {
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeaturePickMode),
 
            });
            // 
            // menuFeature
            // 
            this.menuFeature.Caption = "&Features";
            this.menuFeature.Id = 34;
            this.menuFeature.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]
            {
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeaturePickMode),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureResizeFeatures),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGirthWeld, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureFlange),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBend),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBendStart),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBendEnd),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWeldCirc),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureTakeOff, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureTap),                
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureValve),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureAppurtenance),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWeld),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExtraMetal),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureInternalWallLoss, true),                
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWallLoss),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDent),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDentWithMetalLoss),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureOvality),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureLamination),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUnknown, true),                
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD1, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD2),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD3),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD4)
            });
            this.menuFeature.MergeOrder = 2;
            this.menuFeature.MergeType = DevExpress.XtraBars.BarMenuMerge.MergeItems;
            this.menuFeature.Name = "menuFeature";
            // 
            // menuFeatureResizeFeatures
            // 
            this.menuFeatureResizeFeatures.Caption = "&Resize Features";
            this.menuFeatureResizeFeatures.Id = 0;
            this.menuFeatureResizeFeatures.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]
            {
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDelete),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureInvert),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeLeft, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeRight),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeUp),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeDown),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowWider, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowNarrower),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowShorter),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowLonger),
                new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureSetLongSeamLocation, true)
            });
            this.menuFeatureResizeFeatures.Name = "menuFeatureResizeFeatures";
            // 
            // menuFeatureDelete
            // 
            this.menuFeatureDelete.Caption = "&Delete";
            this.menuFeatureDelete.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.datadelete;
            this.menuFeatureDelete.Id = 1;
            this.menuFeatureDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.menuFeatureDelete.Name = "menuFeatureDelete";
            this.menuFeatureDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureDelete_ItemClick);
            // 
            // menuFeatureInvert
            // 
            this.menuFeatureInvert.Caption = "&Invert";
            this.menuFeatureInvert.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.Invert;
            this.menuFeatureInvert.Id = 2;
            this.menuFeatureInvert.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.I));
            this.menuFeatureInvert.Name = "menuFeatureInvert";
            this.menuFeatureInvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureInvert_ItemClick);
            // 
            // menuFeatureNudgeLeft
            // 
            this.menuFeatureNudgeLeft.Caption = "Nudge &Left";
            this.menuFeatureNudgeLeft.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons._112_LeftArrowShort_Green_16x16_72;
            this.menuFeatureNudgeLeft.Id = 3;
            this.menuFeatureNudgeLeft.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Left));
            this.menuFeatureNudgeLeft.Name = "menuFeatureNudgeLeft";
            this.menuFeatureNudgeLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeLeft_ItemClick);
            // 
            // menuFeatureNudgeRight
            // 
            this.menuFeatureNudgeRight.Caption = "Nudge &Right";
            this.menuFeatureNudgeRight.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons._112_RightArrowShort_Green_16x16_72;
            this.menuFeatureNudgeRight.Id = 3;
            this.menuFeatureNudgeRight.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Right));
            this.menuFeatureNudgeRight.Name = "menuFeatureNudgeRight";
            this.menuFeatureNudgeRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeRight_ItemClick);
            // 
            // menuFeatureNudgeUp
            // 
            this.menuFeatureNudgeUp.Caption = "Nudge &Up";
            this.menuFeatureNudgeUp.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons._112_UpArrowShort_Green_16x16_72;
            this.menuFeatureNudgeUp.Id = 3;
            this.menuFeatureNudgeUp.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Up));
            this.menuFeatureNudgeUp.Name = "menuFeatureNudgeUp";
            this.menuFeatureNudgeUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeUp_ItemClick);
            // 
            // menuFeatureNudgeDown
            // 
            this.menuFeatureNudgeDown.Caption = "Nudge &Down";
            this.menuFeatureNudgeDown.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons._112_DownArrowShort_Green_16x16_72;
            this.menuFeatureNudgeDown.Id = 4;
            this.menuFeatureNudgeDown.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Down));
            this.menuFeatureNudgeDown.Name = "menuFeatureNudgeDown";
            this.menuFeatureNudgeDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeDown_ItemClick);
            // 
            // menuFeatureGrowWider
            // 
            this.menuFeatureGrowWider.Caption = "Grow &Wider";
            this.menuFeatureGrowWider.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DataContainer_MoveLastHS;
            this.menuFeatureGrowWider.Id = 5;
            this.menuFeatureGrowWider.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Up));
            this.menuFeatureGrowWider.Name = "menuFeatureGrowWider";
            this.menuFeatureGrowWider.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowWider_ItemClick);
            // 
            // menuFeatureGrowNarrower
            // 
            this.menuFeatureGrowNarrower.Caption = "Grow &Narrower";
            this.menuFeatureGrowNarrower.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DataContainer_MoveFirstHS;
            this.menuFeatureGrowNarrower.Id = 6;
            this.menuFeatureGrowNarrower.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Down));
            this.menuFeatureGrowNarrower.Name = "menuFeatureGrowNarrower";
            this.menuFeatureGrowNarrower.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowNarrower_ItemClick);
            // 
            // menuFeatureGrowShorter
            // 
            this.menuFeatureGrowShorter.Caption = "Grow &Shorter";
            this.menuFeatureGrowShorter.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DataContainer_MovePreviousHS;
            this.menuFeatureGrowShorter.Id = 7;
            this.menuFeatureGrowShorter.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Left));
            this.menuFeatureGrowShorter.Name = "menuFeatureGrowShorter";
            this.menuFeatureGrowShorter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowShorter_ItemClick);
            // 
            // menuFeatureGrowLonger
            // 
            this.menuFeatureGrowLonger.Caption = "Grow Lon&ger";
            this.menuFeatureGrowLonger.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DataContainer_MoveNextHS;
            this.menuFeatureGrowLonger.Id = 8;
            this.menuFeatureGrowLonger.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Right));
            this.menuFeatureGrowLonger.Name = "menuFeatureGrowLonger";
            this.menuFeatureGrowLonger.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowLonger_ItemClick);
            // 
            // menuFeatureSetLongSeamLocation
            // 
            this.menuFeatureSetLongSeamLocation.Caption = "Set Long Seam Lo&cation";
            this.menuFeatureSetLongSeamLocation.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DataContainer_MoveNextHS;
            this.menuFeatureSetLongSeamLocation.Id = 9;
            this.menuFeatureSetLongSeamLocation.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.W));
            this.menuFeatureSetLongSeamLocation.Name = "menuFeatureSetLongSeamLocation";
            this.menuFeatureSetLongSeamLocation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureSetLongSeamLocation_ItemClick);
            // 
            // menuFeaturePickMode
            // 
            this.menuFeaturePickMode.Caption = "Feature Pick &Mode";
            this.menuFeaturePickMode.Glyph = global::QuestIntegrity.LifeQuest.Common.Resources.MenuIcons.DrawFeature;
            this.menuFeaturePickMode.Id = 10;
            this.menuFeaturePickMode.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.M));
            this.menuFeaturePickMode.Name = "menuFeaturePickMode";
            this.menuFeaturePickMode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeaturePickMode_ItemClick);
            // 
            // menuFeatureGirthWeld
            // 
            this.menuFeatureGirthWeld.Caption = "&Girth Weld";
            this.menuFeatureGirthWeld.Id = 11;
            this.menuFeatureGirthWeld.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.G));
            this.menuFeatureGirthWeld.Name = "menuFeatureGirthWeld";
            this.menuFeatureGirthWeld.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGirthWeld_CheckChanged);
            // 
            // menuFeatureFlange
            // 
            this.menuFeatureFlange.Caption = "&Flange";
            this.menuFeatureFlange.Id = 12;
            this.menuFeatureFlange.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F));
            this.menuFeatureFlange.Name = "menuFeatureFlange";
            this.menuFeatureFlange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureFlange_CheckChanged);
            // 
            // menuFeatureBend
            // 
            this.menuFeatureBend.Caption = "&Bend";
            this.menuFeatureBend.Id = 13;
            this.menuFeatureBend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.B));
            this.menuFeatureBend.Name = "menuFeatureBend";
            this.menuFeatureBend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureBend_CheckChanged);
            // 
            // menuFeatureBendEnd
            // 
            this.menuFeatureBendEnd.Caption = "Bend &End";
            this.menuFeatureBendEnd.Id = 14;
            this.menuFeatureBendEnd.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.E));
            this.menuFeatureBendEnd.Name = "menuFeatureBendEnd";
            this.menuFeatureBendEnd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureBendEnd_CheckChanged);
            // 
            // menuFeatureBendStart
            // 
            this.menuFeatureBendStart.Caption = "Bend &Start";
            this.menuFeatureBendStart.Id = 15;
            this.menuFeatureBendStart.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.S));
            this.menuFeatureBendStart.Name = "menuFeatureBendStart";
            this.menuFeatureBendStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureBendStart_CheckChanged);
            // 
            // menuFeatureExternalWeldCirc
            // 
            this.menuFeatureExternalWeldCirc.Caption = "External Weld (&Circ)";
            this.menuFeatureExternalWeldCirc.Id = 16;
            this.menuFeatureExternalWeldCirc.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.C));
            this.menuFeatureExternalWeldCirc.Name = "menuFeatureExternalWeldCirc";
            this.menuFeatureExternalWeldCirc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureExternalWeldCirc_CheckChanged);
            // 
            // menuFeatureTakeOff
            // 
            this.menuFeatureTakeOff.Caption = "&Take Off";
            this.menuFeatureTakeOff.Id = 17;
            this.menuFeatureTakeOff.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.T));
            this.menuFeatureTakeOff.Name = "menuFeatureTakeOff";
            this.menuFeatureTakeOff.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureTakeOff_CheckChanged);
            // 
            // menuFeatureTap
            // 
            this.menuFeatureTap.Caption = "Ta&p";
            this.menuFeatureTap.Id = 18;
            this.menuFeatureTap.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.P));
            this.menuFeatureTap.Name = "menuFeatureTap";
            this.menuFeatureTap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureTap_CheckChanged);
            // 
            // menuFeatureValve
            // 
            this.menuFeatureValve.Caption = "&Valve";
            this.menuFeatureValve.Id = 19;
            this.menuFeatureValve.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.V));
            this.menuFeatureValve.Name = "menuFeatureValve";
            this.menuFeatureValve.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureValve_CheckChanged);
            // 
            // menuFeatureAppurtenance
            // 
            this.menuFeatureAppurtenance.Caption = "&Appurtenance";
            this.menuFeatureAppurtenance.Id = 20;
            this.menuFeatureAppurtenance.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.A));
            this.menuFeatureAppurtenance.Name = "menuFeatureAppurtenance";
            this.menuFeatureAppurtenance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureAppurtenance_CheckChanged);
            // 
            // menuFeatureExternalWeld
            // 
            this.menuFeatureExternalWeld.Caption = "&External Weld";
            this.menuFeatureExternalWeld.Id = 21;
            this.menuFeatureExternalWeld.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.E));
            this.menuFeatureExternalWeld.Name = "menuFeatureExternalWeld";
            this.menuFeatureExternalWeld.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureExternalWeld_CheckChanged);
            // 
            // menuFeatureExtraMetal
            // 
            this.menuFeatureExtraMetal.Caption = "E&xtra Metal";
            this.menuFeatureExtraMetal.Id = 22;
            this.menuFeatureExtraMetal.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.X));
            this.menuFeatureExtraMetal.Name = "menuFeatureExtraMetal";
            this.menuFeatureExtraMetal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureExtraMetal_CheckChanged);
            // 
            // menuFeatureInternalWallLoss
            // 
            this.menuFeatureInternalWallLoss.Caption = "&Internal Wall Loss";
            this.menuFeatureInternalWallLoss.Id = 23;
            this.menuFeatureInternalWallLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.I));
            this.menuFeatureInternalWallLoss.Name = "menuFeatureInternalWallLoss";
            this.menuFeatureInternalWallLoss.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureInternalWallLoss_CheckChanged);
            // 
            // menuFeatureExternalWallLoss
            // 
            this.menuFeatureExternalWallLoss.Caption = "External &Wall Loss";
            this.menuFeatureExternalWallLoss.Id = 24;
            this.menuFeatureExternalWallLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.W));
            this.menuFeatureExternalWallLoss.Name = "menuFeatureExternalWallLoss";
            this.menuFeatureExternalWallLoss.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureExternalWallLoss_CheckChanged);
            // 
            // menuFeatureDent
            // 
            this.menuFeatureDent.Caption = "&Dent";
            this.menuFeatureDent.Id = 25;
            this.menuFeatureDent.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.D));
            this.menuFeatureDent.Name = "menuFeatureDent";
            this.menuFeatureDent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureDent_CheckChanged);
            // 
            // menuFeatureDentWithMetalLoss
            // 
            this.menuFeatureDentWithMetalLoss.Caption = "Dent with &Metal Loss";
            this.menuFeatureDentWithMetalLoss.Id = 26;
            this.menuFeatureDentWithMetalLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.M));
            this.menuFeatureDentWithMetalLoss.Name = "menuFeatureDentWithMetalLoss";
            this.menuFeatureDentWithMetalLoss.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureDentWithMetalLoss_CheckChanged);
            // 
            // menuFeatureOvality
            // 
            this.menuFeatureOvality.Caption = "&Ovality";
            this.menuFeatureOvality.Id = 27;
            this.menuFeatureOvality.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.O));
            this.menuFeatureOvality.Name = "menuFeatureOvality";
            this.menuFeatureOvality.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureOvality_CheckChanged);
            // 
            // menuFeatureLamination
            // 
            this.menuFeatureLamination.Caption = "&Lamination";
            this.menuFeatureLamination.Id = 28;
            this.menuFeatureLamination.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.L));
            this.menuFeatureLamination.Name = "menuFeatureLamination";
            this.menuFeatureLamination.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureLamination_CheckChanged);
            // 
            // menuFeatureUnknown
            // 
            this.menuFeatureUnknown.Caption = "&Unknown";
            this.menuFeatureUnknown.Id = 29;
            this.menuFeatureUnknown.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.U));
            this.menuFeatureUnknown.Name = "menuFeatureUnknown";
            this.menuFeatureUnknown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureUnknown_CheckChanged);
            // 
            // menuFeatureUD1
            // 
            this.menuFeatureUD1.Caption = "User Defined &1";
            this.menuFeatureUD1.Id = 30;
            this.menuFeatureUD1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.D1));
            this.menuFeatureUD1.Name = "menuFeatureUD1";
            this.menuFeatureUD1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureUD1_CheckChanged);
            // 
            // menuFeatureUD2
            // 
            this.menuFeatureUD2.Caption = "User Defined &2";
            this.menuFeatureUD2.Id = 31;
            this.menuFeatureUD2.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.D2));
            this.menuFeatureUD2.Name = "menuFeatureUD2";
            this.menuFeatureUD2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureUD2_CheckChanged);
            // 
            // menuFeatureUD3
            // 
            this.menuFeatureUD3.Caption = "User Defined &3";
            this.menuFeatureUD3.Id = 32;
            this.menuFeatureUD3.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.D3));
            this.menuFeatureUD3.Name = "menuFeatureUD3";
            this.menuFeatureUD3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureUD3_CheckChanged);
            // 
            // menuFeatureUD4
            // 
            this.menuFeatureUD4.Caption = "User Defined &4";
            this.menuFeatureUD4.Id = 33;
            this.menuFeatureUD4.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.D4));
            this.menuFeatureUD4.Name = "menuFeatureUD4";
            this.menuFeatureUD4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureUD4_CheckChanged);
            // 
            // dataTip
            // 
            this.dataTip.AutoPopDelay = 5000;
            this.dataTip.InitialDelay = 0;
            this.dataTip.ReshowDelay = 100;
            this.dataTip.UseAnimation = false;
            this.dataTip.UseFading = false;
            // 
            // ViewPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 394);
            this.Controls.Add(this._dataViewBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "ViewPane";
            this.TabText = "ViewPane";
            this.Text = "ViewPane";
            this.Shown += new System.EventHandler(this.ViewPane_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewPane_FormClosing);
            this.ResumeLayout(false);
        }

        #endregion

        protected QuestIntegrity.LifeQuest.Common.Forms.Controls.DataViewBar _dataViewBar;
        protected System.Windows.Forms.ToolTip dataTip;
        protected System.Windows.Forms.ToolTip ControlToolTips;
        protected DevExpress.XtraBars.BarSubItem menuFeature;
        protected DevExpress.XtraBars.BarSubItem menuFeatureResizeFeatures;
        protected DevExpress.XtraBars.BarCheckItem menuFeaturePickMode;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureGirthWeld;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureFlange;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBend;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBendStart;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBendEnd;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWeldCirc;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureTakeOff;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureTap;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureValve;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureAppurtenance;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWeld;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExtraMetal;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureInternalWallLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWallLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureDent;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureDentWithMetalLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureOvality;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureLamination;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUnknown;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD1;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD2;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD3;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD4;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureDelete;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureInvert;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeLeft;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeRight;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeUp;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeDown;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowWider;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowNarrower;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowShorter;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowLonger;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureSetLongSeamLocation;
    }
}