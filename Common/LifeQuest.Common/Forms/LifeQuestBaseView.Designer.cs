﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 12/12/2011 15:20:04 PM
// Created by:   j.rowe
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    partial class LifeQuestBaseView : LifeQuestBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataToolTip = new System.Windows.Forms.ToolTip();
            this.ControlToolTip = new System.Windows.Forms.ToolTip();
            this.SuspendLayout();
            // 
            // dataToolTip
            // 
            this.dataToolTip.AutoPopDelay = 5000;
            this.dataToolTip.InitialDelay = 0;
            this.dataToolTip.ReshowDelay = 100;
            this.dataToolTip.UseAnimation = false;
            this.dataToolTip.UseFading = false;
            // 
            // LifeQuestBaseView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LifeQuestBaseView";
            this.Text = "LifeQuestBaseView";
            this.Disposed += this.LifeQuestBaseView_FormClosing;
            this.ResumeLayout(false);
        }

        #endregion

        protected System.Windows.Forms.ToolTip dataToolTip;
        protected System.Windows.Forms.ToolTip ControlToolTip;
    }
}