﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 12/12/2011 15:20:04 PM
// Created by:   j.rowe
//
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using QuestIntegrity.Engineering;
using QuestIntegrity.Extensions;
using QuestIntegrity.Graphics;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using QuestIntegrity.LifeQuest.Common.Properties;
using QuestIntegrity.Units;
using QuestIntegrity.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Columns;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    public partial class FeatureDetails : FeatureForm,
                                            IClipboardPaste,
                                            IUndo,
                                            IHasDataSource,
                                            IHasDisplayUnits
    {
        #region Private Properties

        private const string _enterQueryString = "Enter query string";
        private ErrorProvider _inputError;
        private readonly DataTable _featureStats = new DataTable("FeatureStats");
        private DataGridViewColumn _columnLastClicked;

        #endregion Private Properties

        #region Constructors

        public FeatureDetails()
        {
            InitializeComponent();
        }

        public FeatureDetails(FormType type)
            : base(type)
        {
            InitializeComponent();

            //Flags for tracking enter/exit of controls
            jointNominalSize.Tag = false;
            material.Tag = false;

            // Add feature columns
            LoadTableFormat(Path.Combine(DataManager.Instance.CurrentProject.DirectoryPath, DataManager.Instance.CurrentProject.FeatureDetailFormatFilename));

            DefaultFeatureType = FeatureType.GirthWeld;

            //Bind the feature type lookup
            featureType.Properties.DataSource = new List<KeyValuePair<Enum, string>>();
            featureType.Properties.ValueMember = "Key";
            featureType.Properties.DisplayMember = "Value";

            //Fill drop down lists
            reviewFlag.Properties.DataSource = ResourceEnumConverter.GetValues(typeof(FeatureReviewFlag));
            reviewFlag.Properties.ValueMember = "Key";
            reviewFlag.Properties.DisplayMember = "Value";

            featureLocation.Properties.DataSource = ResourceEnumConverter.GetValues(typeof(AnalysisInformation.InternalExternal));
            featureLocation.Properties.ValueMember = "Key";
            featureLocation.Properties.DisplayMember = "Value";

            longWeldType.Properties.DataSource = ResourceEnumConverter.GetValues(typeof(LongitudinalWeldType));
            longWeldType.Properties.ValueMember = "Key";
            longWeldType.Properties.DisplayMember = "Value";

            ConfigureErrorProviders();
            SetLabelsFromResources();

            // Set the default split sizes
            MainSplit.SplitterDistance = Settings.Default.FeatureDetail_MainSplitterDistance;
            BottomHalfSplit.SplitterDistance = Settings.Default.FeatureDetail_BottomSplitterDistance;

            // Align conditional fields
            longWeldType.Location = featureLocation.Location;
            longWeldTypeLabel.Top = labelLocation.Top;
            longWeldTypeLabel.Left = labelLocation.Right - longWeldTypeLabel.Width;

            SetupFeatureFilterMenu();
            SetupStatsView();

            UpdateDataGridColumns();
            CurrentInspectionFile = DataManager.Instance.CurrentProject.CurrentInspectionFile;  // use public property cuz it calls other methods
            UpdateDisplayUnits();

            // Set filter toolbar
            ToggleAdvancedFilter(Settings.Default.ShowAdvancedFeatureFilter);

            SetLicenseBasedDisplay();

            featureView.Click += featureGrid_Click;
            //TODO: CAM add back in
            //featureView.DataError += featureGrid_DataError;
            //featureView.UserAddedRow += featureGrid_UserAddedRow;
            //featureView.UserDeletedRow += featureGrid_UserDeletedRow;
            //featureView.UserDeletingRow += featureGrid_UserDeletingRow;
            featureView.KeyDown += featureGrid_KeyDown;
        }

        #endregion Constructors

        #region Public Properties

        public override double Position
        {
            get
            {
                return position.Text.ToDouble();
            }
            set
            {
                position.Text = value.ToString();
            }
        }

        public override float Length
        {
            get
            {
                return length.Text.ToSingle();
            }
            set
            {
                length.Text = value.ToString();
            }
        }

        public override float CircumferentialLocation
        {
            get
            {
                return circumferentialLocation.Text.ToSingle();
            }
            set
            {
                circumferentialLocation.Text = value.ToString();
            }
        }

        public override float SweepAngle
        {
            get
            {
                return circumferentialExtent.Text.ToSingle();
            }
            set
            {
                circumferentialExtent.Text = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the default FeatureType when
        /// new records are added.
        /// </summary>
        /// <value>The default type of the feature.</value>
        public FeatureType DefaultFeatureType
        {
            get;
            set;
        }

        #endregion Public Properties

        #region Public Methods
        /// <summary>
        /// Changes state when form is set to "ViewerOnlyMode"
        /// </summary>
        public override void SetLicenseBasedDisplay()
        {
            bool viewerOnlyMode = QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;

            //Buttons on menu bar
            copyGridData.Visible = QuestIntegrity.Licensing.LicenseManager.Instance.HasCoreLicense() || QuestIntegrity.Licensing.LicenseManager.Instance.IsSuperUser;
            renumberFeatures.Visible = !viewerOnlyMode;
            recalculateCurrentFeature.Visible = !viewerOnlyMode;
            toolStripSeparator2.Visible = !viewerOnlyMode;
            processDataButton.Visible = !viewerOnlyMode;
            setIMUInfo.Visible = !viewerOnlyMode;

            //controls
            positionIndex.Visible = !viewerOnlyMode;
            lengthIndex.Visible = !viewerOnlyMode;
            circumfrentialLocationIndex.Visible = !viewerOnlyMode;
            circumferentialExtentIndicies.Visible = !viewerOnlyMode;

            nudPercentage.Visible = !viewerOnlyMode;
            labelPercentage.Visible = !viewerOnlyMode;

            chkPositionReferenceIsFixed.Visible = !viewerOnlyMode;
            chkIsIMUReferencePoint.Visible = !viewerOnlyMode;

            capturePosition.Visible = !viewerOnlyMode;
            material.Properties.ReadOnly = viewerOnlyMode;
            jointNominalSize.Properties.ReadOnly = viewerOnlyMode;

            viewLog.Visible = !viewerOnlyMode;

            //Tracking fields
            labelInternalComment.Visible = !viewerOnlyMode;
            labelReview.Visible = !viewerOnlyMode;
            labelReviewDate.Visible = !viewerOnlyMode;
            internalComment.Visible = !viewerOnlyMode;
            reviewDate.Visible = !viewerOnlyMode;
            viewLog.Visible = !viewerOnlyMode;
            reviewFlag.Visible = !viewerOnlyMode;

            notes.Visible = true;
            if (viewerOnlyMode)
            {
                int height = classificationGroup.Height - jointNominalSize.Bottom - 11;
                notes.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top;
                notes.Top = reviewFlag.Top;
                notes.Height = height;

                labelNotes.Top = reviewFlag.Top;
                labelNotes.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            }
            else
            {
                int top = internalComment.Bottom + 3;
                int height = classificationGroup.Height - internalComment.Bottom - 8;

                notes.Top = top;
                notes.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
                notes.Height = height;

                labelNotes.Top = top;
                labelNotes.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            }

            //context menu
            menuInvertFeature.Visible = !viewerOnlyMode;
            menuRecalculate.Visible = !viewerOnlyMode;
            menuInsertMaterialChange.Visible = !viewerOnlyMode;
            menuRemoveMaterialChange.Visible = !viewerOnlyMode;
            menuAssignPipeSize.Visible = !viewerOnlyMode;
            menuInsertPipeSizeChange.Visible = !viewerOnlyMode;
            menuRemovePipeSizeChange.Visible = !viewerOnlyMode;
        }


        /// <summary>
        /// Reassigns the datasource to the Binding source for the grid.
        /// NOTE: by setting the DataSource property, the NotifyPropertyChanged events
        /// will be correctly wired to the grid.  Therefore, it makes sense to call this
        /// if adding items to the list from outside of this form over using the "RefreshDataBindings" method.
        /// </summary>
        public override void UpdateDataSource()
        {
            base.UpdateDataSource();
            SetFeatureDataBindings();

            featureGrid.DataSource = _dataFeatures;

            UpdateLookupLists();

            if (!featureGrid.InvokeRequired)
            {
                _dataFeatures.ResetBindings(false);
                UpdateDataGridColumns();
            }
        }

        public override void UpdateDataGridColumns()
        {
            if (featureGrid.InvokeRequired)
                return;

            featureView.Columns.Clear();
            _columnSorts.Clear();

            //DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn newColumn = null;
            GridColumn newColumn = null;
            //TODO:CAM add back in
            foreach (ColumnInformation column in _columnInfo)
            {
                if (column.Type == ColumnInformation.ColumnType.CheckBox)
                {
                    //newColumn = new DataGridViewCheckBoxColumn();
                }
                else
                {
                    //newColumn = new DataGridViewTextBoxColumn();
                }
                //newColumn.DataPropertyName = column.DataProperty;
                newColumn.Name = column.DataProperty;
                newColumn.Caption = column.ColumnHeader;
                if (column.ToolTip != null && column.ToolTip != string.Empty)
                {
                    //newColumn.ToolTipText = column.ToolTip;
                }
                //newColumn.DefaultCellStyle.Alignment = FeatureSpreadsheetFormat.GetAlignmentByDataPropertyType(column.DataProperty);
                //newColumn.DefaultCellStyle.Format = column.Format;
                //newColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
                //newColumn.ReadOnly = column.ReadOnly;
                
                if (column.Width > 0)
                {
                    //newColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    newColumn.Width = column.Width;
                }

                featureView.Columns.Add(newColumn);

                _columnSorts.Add(ListSortDirection.Ascending);
            }
        }

        /// <summary>
        /// Updates the lookup lists.
        /// </summary>
        public void UpdateLookupLists()
        {
            if (CurrentInspectionFile.Parent != null)
            {
                featureType.Properties.DataSource = CurrentInspectionFile.Parent.GetFeatureTypeValues();
                LookUpListHelper ll = new LookUpListHelper(CurrentInspectionFile);
                jointNominalSize.Properties.DataSource = ll.GetUniquePipingList();
                material.Properties.DataSource = ll.GetUniqueMaterialList();
                SetContextMenuForFeature();
            }
        }

        public void SetLongSeamLocationAtCrosshairs()
        {
            if (CurrentFeature != null && FeatureTypeGroupings.IsJointDelimiter(CurrentFeature.Type))
            {
                long globalID = WindowManager.Instance.CurrentGlobalID;
                float degrees = CurrentInspectionFile.RadialDegreesCorrectedOfGlobalID(globalID);
                CurrentFeature.LongitudinalSeamWeldLocation = degrees;
            }
        }

        public void UpdateDisplayUnits()
        {
            if (CurrentUnitSystem != null)      // Avoid problems during program startup
            {
                // Update the format strings in the binding definitions using the defaults from the DisplayUnits object.
                positionReference.UpdateFormatStringForBoundControl("Text", DisplayUnits.Instance.AxialDistanceUnits.FormatString);
                position.UpdateFormatStringForBoundControl("Text", DisplayUnits.Instance.AxialDistanceUnits.FormatString);
                length.UpdateFormatStringForBoundControl("Text", DisplayUnits.Instance.SizingUnits.FormatString);
                depth.UpdateFormatStringForBoundControl("Text", DisplayUnits.Instance.MeasurementUnits.FormatString);

                // JTR: 2/5/10 - need to think through implementation of this with static classes, etc...
                //       below is pattern from other forms, but may not be appropriate here.
                // update the format string of the grid columns
                // pipingSectionsGrid.UpdateFormatStringForColumn("xxxxx", DisplayUnits.Instance.AxialDistanceUnits.FormatString);

                _dataFeatures.ResetBindings(false);
            }
            // Update the units labels
            UpdateUnitsLabels();
        }

        public void UpdateFeatureDataListIfCurrent(FeatureInfo feature)
        {
            if (CurrentFeature == null || CurrentFeature != feature)
                return;

            SetFeatureStatsToCalculatedData();
            UpdateListView();
        }

        #endregion Public Methods

        #region Public Events

        public event EventHandler<ActionEventArgs> ActionRequested;
        public event EventHandler<PipingAssignmentEventArgs> AssignPipeInfoAtFeatureClick;
        public event EventHandler<MaterialAssignmentEventArgs> AssignMaterialInfoAtFeatureClick;
        public event EventHandler<FeatureInfoEventArgs> FeatureCreated;
        public event EventHandler<FeatureInfoEventArgs> FeatureUpdated;
        public event EventHandler<EventArgs<bool, double>> GotoMaterialClick;  // T,U => isNew, position
        public event EventHandler<EventArgs<bool, double>> GotoPipeInfoClick;  // T,U => isNew, position
        public event EventHandler<EventArgs> IMUReferenceDetailRequested;
        public event EventHandler<FeatureInfoEventArgs> JointSelected;
        public event EventHandler<EventArgs<FeatureInfo>> RemovePipeInfoAtFeatureClick;
        public event EventHandler<EventArgs<FeatureInfo>> RemoveMaterialInfoAtFeatureClick;
        public event EventHandler<FeatureInfoEventArgs> ViewBendRequested;

        #endregion

        #region Private Methods

        protected override void _dataFeatures_CurrentChanged(object sender, EventArgs e)
        {
            SetContextMenuForFeature();

            if (CurrentFeature != null)
            {
                viewLog.Visible = CurrentFeature.ChangeLog.Count > 0;
                SetFeatureStatsToCalculatedData();
                UpdateListView();
            }
        }

        protected override void _dataFeatures_CurrentItemChanged(object sender, EventArgs e)
        {
            //Could be called from mt process
            if (InvokeRequired)
                return;

            if (CurrentFeature == null)
                return;

            //Do the updates
            UpdateFeatureBindingsForType();
            positionReference.Enabled = (chkPositionReferenceIsFixed.Checked);
        }

        /// <summary>
        /// Conditionally sets the display based on feature type.
        /// </summary>
        private void UpdateFeatureBindingsForType()
        {
            Binding binding;
            switch (CurrentFeature.Type)
            {
                case FeatureType.Blister:
                case FeatureType.Dent:
                case FeatureType.DentMetalLoss:
                case FeatureType.GeometricAnomaly:
                    labelFlawDepth.Text = "Dent Depth:";
                    labelFlawDepth.Enabled = true;
                    depth.Enabled = true;

                    depth.DataBindings.Clear();
                    binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "DentDepthInDisplayUnits", true);
                    binding.FormatString = DisplayUnits.Instance.MeasurementUnits.FormatString;
                    depth.ReadOnly = false;
                    depth.DataBindings.Add(binding);

                    depthPercent.DataBindings.Clear();
                    binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "DentDepthPercent", true);
                    binding.FormatString = "0.0";
                    depthPercent.ReadOnly = true;
                    depthPercent.DataBindings.Add(binding);

                    length.Enabled = true;
                    circumferentialExtent.Enabled = true;
                    break;
                case FeatureType.MetalLoss:
                case FeatureType.Grinding:
                    labelFlawDepth.Text = "Depth by Nominal:";
                    labelFlawDepth.Enabled = true;
                    depth.Enabled = true;

                    // Change bindings
                    depth.DataBindings.Clear();
                    binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "DepthInDisplayUnits", true);
                    binding.FormatString = DisplayUnits.Instance.MeasurementUnits.FormatString;
                    depth.ReadOnly = false;
                    depth.DataBindings.Add(binding);

                    depthPercent.DataBindings.Clear();
                    binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "DepthPercent", true);
                    binding.FormatString = "0.0";
                    depthPercent.ReadOnly = true;
                    depthPercent.DataBindings.Add(binding);

                    length.Enabled = true;
                    circumferentialExtent.Enabled = true;
                    break;
                case FeatureType.Flange:
                case FeatureType.Bend:
                case FeatureType.BendStart:
                case FeatureType.BendEnd:
                case FeatureType.GirthWeld:
                case FeatureType.DataEnd:
                case FeatureType.DataStart:
                case FeatureType.PipeTransition:
                    labelFlawDepth.Text = "Depth:";
                    // Change bindings
                    depth.DataBindings.Clear();
                    depthPercent.DataBindings.Clear();
                    depth.Text = "";
                    depthPercent.Text = "";
                    labelFlawDepth.Enabled = false;
                    depth.Enabled = false;
                    length.Enabled = false;
                    circumferentialExtent.Enabled = false;
                    length.Text = "0.0";
                    circumferentialExtent.Text = "360.0";
                    break;
                case FeatureType.AGM:
                case FeatureType.AnomalyGirthWeld:
                case FeatureType.AnomalyMetallurgical:
                case FeatureType.AnomalySeamWeld:
                case FeatureType.AnomolyManufacturing:
                case FeatureType.Appurtenance:
                case FeatureType.Buckle:
                case FeatureType.ExternalWeld:
                case FeatureType.ExtraMetal:
                case FeatureType.Lamination:
                case FeatureType.Launcher:
                case FeatureType.Miscellaneous:
                case FeatureType.Ovality:
                case FeatureType.TakeOff:
                case FeatureType.Tap:
                case FeatureType.Unknown:
                case FeatureType.Valve:
                case FeatureType.Wrinkle:
                case FeatureType.UserDefinedType1:
                case FeatureType.UserDefinedType2:
                case FeatureType.UserDefinedType3:
                case FeatureType.UserDefinedType4:
                case FeatureType.UserDefinedType5:
                case FeatureType.UserDefinedType6:
                case FeatureType.UserDefinedType7:
                case FeatureType.UserDefinedType8:
                case FeatureType.UserDefinedType9:
                case FeatureType.UserDefinedType10:
                case FeatureType.UserDefinedType11:
                case FeatureType.UserDefinedType12:
                default:
                    // Change bindings
                    labelFlawDepth.Text = "Depth:";
                    depth.DataBindings.Clear();
                    depthPercent.DataBindings.Clear();
                    depth.Text = "";
                    depthPercent.Text = "";
                    labelFlawDepth.Enabled = false;
                    depth.Enabled = false;
                    length.Enabled = true;
                    circumferentialExtent.Enabled = true;
                    break;
            }

            // Long Seam weld conditional display
            if (FeatureTypeGroupings.IsJointDelimiter(CurrentFeature))
            {
                labelLocation.Visible = false;
                featureLocation.Visible = false;
                featureLocation.EditValue = AnalysisInformation.InternalExternal.NotApplicable;
                longWeldTypeLabel.Visible = true;
                longWeldType.Visible = true;
                jointNominalSize.Enabled = true;
                material.Enabled = true;

                capturePosition.Enabled = true;
                circumferentialLocation.DataBindings.Clear();
                binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "LongitudinalSeamWeldLocation", true);
                binding.FormatString = "0";
                circumferentialLocation.DataBindings.Add(binding);
                toolTips.SetToolTip(circumferentialLocation, "Location of Long Seam Weld, if applicable.");
            }
            else
            {
                labelLocation.Visible = true;
                featureLocation.Visible = true;
                longWeldTypeLabel.Visible = false;
                longWeldType.Visible = false;
                longWeldType.EditValue = LongitudinalWeldType.NA;
                jointNominalSize.Enabled = false;
                material.Enabled = false;

                capturePosition.Enabled = false;
                circumferentialLocation.DataBindings.Clear();
                binding = new System.Windows.Forms.Binding("Text", _dataFeatures, "CircumferentialStartLocation", true);
                binding.FormatString = "0";
                circumferentialLocation.DataBindings.Add(binding);
                toolTips.SetToolTip(circumferentialLocation, "Circumferential starting location of the feature.");
            }
        }

        private void material_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag != null && e.Button.Tag.ToString() == "Add")
            {
                OnGotoNewMaterialClick();
            }
        }

        private void jointNominalSize_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag != null && e.Button.Tag.ToString() == "Add")
            {
                OnGotoNewPipeInfoClick();
            }
        }

        private void material_Modified(object sender, EventArgs e)
        {
            Control ctrl = sender as Control;
            ctrl.Tag = true;
        }

        private void jointNominalSize_Modified(object sender, EventArgs e)
        {
            Control ctrl = sender as Control;
            ctrl.Tag = true;
        }

        private void material_EditValueChanged(object sender, EventArgs e)
        {
            //Make sure this was user input
            Control ctrl = sender as Control;
            if (ctrl != null && (bool)ctrl.Tag)
            {
                ctrl.Tag = false;
                UniqueMaterial key = material.EditValue as UniqueMaterial;
                OnAssignMaterialInfoAtFeatureClick(key, CurrentFeature);
            }
        }

        private void jointNominalSize_EditValueChanged(object sender, EventArgs e)
        {
            //Make sure it is user input
            Control ctrl = sender as Control;
            if (ctrl != null && (bool)ctrl.Tag)
            {
                ctrl.Tag = false;
                List<FeatureInfo> features = new List<FeatureInfo>();
                features.Add(CurrentFeature);
                UniquePiping key = jointNominalSize.EditValue as UniquePiping;
                OnAssignPipeInfoAtFeatureClick(new PipingAssignmentEventArgs(key, features, true));
            }
        }

        private void capturePosition_Click(object sender, EventArgs e)
        {
            SetLongSeamLocationAtCrosshairs();
        }

        private void SetLabelsFromResources()
        {
            // Labels
            labelID.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelID;
            labelLabel.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelLabel;
            labelLocation.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelLocation;
            labelType.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelType;
            labelMaterial.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelMaterial;
            labelPercentage.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelPercentage;
            labelNotes.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelNotes;
            // TODO:  labelReferencePosition.Text
            labelAbsolutePosition.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelPosition;
            labelCircularPosition.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelCircularPosition;
            labelLength.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelLength;
            labelCircularExtent.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelCircularExtent;
            labelFlawDepth.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelFlawDepth;

            // Group headings
            classificationGroup.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_classificationGroup;
            locationAndSizeGroup.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_locationAndSizeGroup;
            featureInformationGroup.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_featureInformationGroup;

            // Toolbar string
            sortAZ.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_sortAZ;
            sortZA.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_sortZA;
            showFullButton.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_showFullButton;
            processDataButton.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_filterButton;
            addNewFeature.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_addNewFeature;
        }

        /// <summary>
        /// Set all Unit labels using the display units
        /// </summary>
        private void UpdateUnitsLabels()
        {
            positionReferenceUnits.Text = DisplayUnits.Instance.AxialDistanceUnits.Symbol;
            positionUnits.Text = DisplayUnits.Instance.AxialDistanceUnits.Symbol;
            lengthUnits.Text = DisplayUnits.Instance.MeasurementUnits.Symbol;
            depthUnits.Text = DisplayUnits.Instance.MeasurementUnits.Symbol;
            labelCircularExtentsUnits.Text = "Deg.";
            labelCircularPositionUnits.Text = "Deg.";
            labelPercent.Text = QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_percentLabel;
        }

        private void viewLog_Click(object sender, EventArgs e)
        {
            using (LogView log = new LogView()
            {
                Text = CurrentFeature.LabelID + " Change Log",
                LogEntries = CurrentFeature.ChangeLog.Select(f => f.ToString()),
                StartPosition = FormStartPosition.CenterParent
            })
            {
                log.ShowDialog();
            }
            System.Diagnostics.Debug.WriteLine(viewLog.Location);
        }

        /// <summary>
        /// Turns the "enter" key into an accept button similar to tab.
        /// </summary>
        /// <param name="e"></param>
        private void Input_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Control nextControl = sender as Control;
                do
                {
                    nextControl = GetNextControl(nextControl, true);
                } while (!nextControl.TabStop);
                nextControl.Focus();
            }
            else
            {
                e.Handled = false;
            }
        }

        private void ConfigureErrorProviders()
        {
            _inputError = new ErrorProvider(this)
            {
                BlinkStyle = ErrorBlinkStyle.NeverBlink
            };
        }

        private void SetupStatsView()
        {
            // Setup columns in "DataTable" that will be bound
            _featureStats.Columns.Clear();
            _featureStats.Columns.Add("Item", typeof(string));
            _featureStats.Columns[_featureStats.Columns.Count - 1].Caption = QuestIntegrity.LifeQuest.Common.Resources.Strings.Item;
            _featureStats.Columns.Add("Value", typeof(Single));
            _featureStats.Columns[_featureStats.Columns.Count - 1].Caption = QuestIntegrity.LifeQuest.Common.Resources.Strings.Value;
            _featureStats.Columns.Add("Units", typeof(string));
            _featureStats.Columns[_featureStats.Columns.Count - 1].Caption = QuestIntegrity.LifeQuest.Common.Resources.Strings.Units;

            //Add Columns to ListView and configure sizing
            listFeatureStats.View = View.Details;
            listFeatureStats.Columns.Clear();
            for (int colIndex = 0; colIndex < _featureStats.Columns.Count; colIndex++)
            {
                listFeatureStats.Columns.Add(_featureStats.Columns[colIndex].Caption);
            }

            for (int colIndex = 0; colIndex < _featureStats.Columns.Count; colIndex++)
            {
                listFeatureStats.Columns[colIndex].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        /// <summary>
        /// Copies data from the stats data table object to the listview
        /// </summary>
        private void UpdateListView()
        {
            if (listFeatureStats.InvokeRequired)  //need to protect agains background threads doing something like a load that triggers 
                return;

            listFeatureStats.BeginUpdate();
            listFeatureStats.Items.Clear();
            if (_featureStats.Columns.Count > 0)
            {
                for (int rowIndex = 0; rowIndex < _featureStats.Rows.Count; rowIndex++)
                {
                    ListViewItem newRow = new ListViewItem(_featureStats.Rows[rowIndex][0].ToString());

                    for (int colIndex = 1; colIndex < _featureStats.Columns.Count; colIndex++)
                    {
                        if (_featureStats.Columns[colIndex].DataType == typeof(Single) || _featureStats.Columns[colIndex].DataType == typeof(Double))
                        {
                            newRow.SubItems.Add(String.Format("{0:G4}", _featureStats.Rows[rowIndex][colIndex].ToDouble()));
                        }
                        else
                        {
                            newRow.SubItems.Add(_featureStats.Rows[rowIndex][colIndex].ToString());
                        }
                    }

                    listFeatureStats.Items.Add(newRow);
                }

                if (listFeatureStats.Columns.Count > 0)
                {
                    for (int colIndex = 0; colIndex < _featureStats.Columns.Count; colIndex++)
                    {
                        listFeatureStats.Columns[colIndex].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    }
                }
            }
            listFeatureStats.EndUpdate();
        }

        private void SetContextMenuForFeature()
        {
            //Start this as not enabled
            menuViewBend.Enabled = false;

            if (featureView.SelectedRowsCount <= 1)
            {
                if (CurrentFeature != null && FeatureTypeGroupings.IsJointDelimiter(CurrentFeature.Type))
                {
                    menuAssignPipeSize.Enabled = true;
                    menuInsertMaterialChange.Enabled = true;
                    menuInsertPipeSizeChange.Enabled = true;
                    menuRemoveMaterialChange.Enabled = CurrentFeature.IsMaterialChange;
                    menuRemovePipeSizeChange.Enabled = CurrentFeature.IsPipeSizeChange;
                    menuSelectJoint.Enabled = true;
                }
                else
                {
                    menuAssignPipeSize.Enabled = false;
                    menuInsertMaterialChange.Enabled = false;
                    menuInsertPipeSizeChange.Enabled = false;
                    menuRemoveMaterialChange.Enabled = false;
                    menuRemovePipeSizeChange.Enabled = false;
                    menuSelectJoint.Enabled = false;
                }

                //Bend View
                if (CurrentFeature != null && CurrentInspectionFile != null & CurrentInspectionFile.HasIMUData
                    && FeatureTypeGroupings.Instance.Bends.Contains(CurrentFeature.Type))
                {
                    menuViewBend.Enabled = true;
                }
            }
            else
            {
                menuInsertMaterialChange.Enabled = false;
                menuInsertPipeSizeChange.Enabled = false;
                menuRemoveMaterialChange.Enabled = false;
                menuRemovePipeSizeChange.Enabled = false;
                menuAssignPipeSize.Enabled = true;
                menuSelectJoint.Enabled = true;
            }

            //Hide in viewer-only mode options
            menuAssignPipeSize.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuInsertMaterialChange.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuInsertPipeSizeChange.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuRemoveMaterialChange.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuRemovePipeSizeChange.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuRecalculate.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuSelectJoint.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuSeparator1.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
            menuInvertFeature.Visible = !QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly;
        }

        /// <summary>
        /// Since BindingSource is late-bound, need to set these bindings after the Datasource has been set.
        /// </summary>
        private void SetFeatureDataBindings()
        {
            chkPositionReferenceIsFixed.DataBindings.Clear();
            chkIsIMUReferencePoint.DataBindings.Clear();
            circumferentialExtent.DataBindings.Clear();
            circumferentialExtentIndicies.DataBindings.Clear();
            circumferentialLocation.DataBindings.Clear();
            circumfrentialLocationIndex.DataBindings.Clear();
            depth.DataBindings.Clear();
            depthPercent.DataBindings.Clear();
            featureLocation.DataBindings.Clear();
            longWeldType.DataBindings.Clear();
            featureType.DataBindings.Clear();
            id.DataBindings.Clear();
            length.DataBindings.Clear();
            lengthIndex.DataBindings.Clear();
            material.DataBindings.Clear();
            notes.DataBindings.Clear();
            nudPercentage.DataBindings.Clear();
            position.DataBindings.Clear();
            positionIndex.DataBindings.Clear();
            positionReference.DataBindings.Clear();
            userLabel.DataBindings.Clear();
            jointNominalSize.DataBindings.Clear();
            reviewFlag.DataBindings.Clear();
            reviewDate.DataBindings.Clear();
            internalComment.DataBindings.Clear();

            chkPositionReferenceIsFixed.DataBindings.Add(new Binding("Checked", _dataFeatures, "StationPositionIsFixed", true));
            chkIsIMUReferencePoint.DataBindings.Add(new Binding("Checked", _dataFeatures, "IsIMUTiePoint", true));
            circumferentialExtent.DataBindings.Add(new Binding("Text", _dataFeatures, "SweepAngle", true, DataSourceUpdateMode.OnValidation, null, "N1"));
            circumferentialExtentIndicies.DataBindings.Add(new Binding("Text", _dataFeatures, "SweepAngleNumIndicies", true));
            circumferentialLocation.DataBindings.Add(new Binding("Text", _dataFeatures, "CircumferentialStartLocation", true, DataSourceUpdateMode.OnValidation, null, "N1"));
            circumfrentialLocationIndex.DataBindings.Add(new Binding("Text", _dataFeatures, "RadialIndexStart", true));
            depth.DataBindings.Add(new Binding("Text", _dataFeatures, "DepthInDisplayUnits", true));
            depthPercent.DataBindings.Add(new Binding("Text", _dataFeatures, "DepthPercent", true, DataSourceUpdateMode.OnValidation, null, "N1"));
            featureLocation.DataBindings.Add(new Binding("EditValue", _dataFeatures, "LocationInWall", true));
            longWeldType.DataBindings.Add(new Binding("EditValue", _dataFeatures, "LongitudinalWeldType", true));
            featureType.DataBindings.Add(new Binding("EditValue", _dataFeatures, "Type", true, DataSourceUpdateMode.OnValidation));
            id.DataBindings.Add(new Binding("Text", _dataFeatures, "ID", true));
            length.DataBindings.Add(new Binding("Text", _dataFeatures, "LengthInDisplayUnits", true, DataSourceUpdateMode.OnValidation));
            lengthIndex.DataBindings.Add(new Binding("Text", _dataFeatures, "LengthInSlices", true));
            material.DataBindings.Add(new Binding("EditValue", _dataFeatures, "MaterialInfoKey", true));
            jointNominalSize.DataBindings.Add(new Binding("EditValue", _dataFeatures, "PipeInfoKey", true));
            notes.DataBindings.Add(new Binding("Text", _dataFeatures, "Notes", true));
            nudPercentage.DataBindings.Add(new Binding("Value", _dataFeatures, "PercentConfidence", true));
            position.DataBindings.Add(new Binding("Text", _dataFeatures, "PositionInDisplayUnits", true, DataSourceUpdateMode.OnValidation));
            positionIndex.DataBindings.Add(new Binding("Text", _dataFeatures, "SliceIndexStart", true));
            positionReference.DataBindings.Add(new Binding("Text", _dataFeatures, "StationPositionInDisplayUnits", true));
            userLabel.DataBindings.Add(new Binding("Text", _dataFeatures, "LabelID", true));
            reviewDate.DataBindings.Add(new Binding("Text", _dataFeatures, "DateLastChange", true, DataSourceUpdateMode.OnPropertyChanged, "", "s"));
            reviewFlag.DataBindings.Add(new Binding("EditValue", _dataFeatures, "ReviewFlag", true));
            internalComment.DataBindings.Add(new Binding("Text", _dataFeatures, "InternalComment", false, DataSourceUpdateMode.OnValidation));
        }

        /// <summary>
        /// Stores the feature data in a datatable.
        /// TODO P2:  get rid of   this was an idea that didn't work.  Should just map into the list directly.
        /// </summary>
        private void SetFeatureStatsToCalculatedData()
        {
            if (CurrentFeature == null)
                return;

            FeatureType featureType = CurrentFeature.Type;

            // Add Rows to Data Table
            _featureStats.Clear();

            if (FeatureTypeGroupings.IsJointDelimiter(featureType))
            {
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.JointLength, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.JointLength, DisplayUnits.Instance.AxialDistanceUnits.Scale), DisplayUnits.Instance.AxialDistanceUnits.Symbol);
            }
            else
            {
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.PipelineFeatures_labelLength, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.Length, DisplayUnits.Instance.MeasurementUnits.Scale), DisplayUnits.Instance.MeasurementUnits.Symbol);
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.Width, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.Width, DisplayUnits.Instance.MeasurementUnits.Scale), DisplayUnits.Instance.MeasurementUnits.Symbol);
            }

            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.ThicknessNominal, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.ThicknessNominal, DisplayUnits.Instance.MeasurementUnits.Scale), DisplayUnits.Instance.MeasurementUnits.Symbol);

            if (FeatureTypeGroupings.IsDent(featureType) || featureType == FeatureType.Blister)
            {
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.NominalDepth, CurrentFeature.DentDepthFromNominal, DisplayUnits.Instance.MeasurementUnits.Symbol);
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MedianDepth, CurrentFeature.DentDepthFromLocalMedian, DisplayUnits.Instance.MeasurementUnits.Symbol);
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.DiameterOutside, CurrentFeature.DiameterOutsideInDisplayUnits, DisplayUnits.Instance.MeasurementUnits.Symbol);
            }
            else if (featureType == FeatureType.MetalLoss)
            {
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.NominalDepth, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.DepthFromNominal, DisplayUnits.Instance.MeasurementUnits.Scale), DisplayUnits.Instance.MeasurementUnits.Symbol);
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.NominalDepth, CurrentFeature.DepthPercent, "%");
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MedianDepth, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.DepthFromJointMedian, DisplayUnits.Instance.MeasurementUnits.Scale), DisplayUnits.Instance.MeasurementUnits.Symbol);
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MedianDepth + "(Percent From Median)", CurrentFeature.DepthPercentFromJointMedian, "%");
                _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MedianDepth + "(Percent From Nominal)", CurrentFeature.DepthFromJointMedianPercentFromNominal, "%");
            }
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.ThicknessMinimumMeasured, CurrentFeature.ThicknessMinimumMeasuredInDisplayUnits, DisplayUnits.Instance.MeasurementUnits.Symbol);
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.SortMethod_ThicknessMedianMeasured, CurrentFeature.ThicknessMedianMeasuredInDisplayUnits, DisplayUnits.Instance.MeasurementUnits.Symbol);

            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.DistanceToUpstreamGirthWeld, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.USWDistance, DisplayUnits.Instance.AxialDistanceUnits.Scale), DisplayUnits.Instance.AxialDistanceUnits.Symbol);
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.DistanceToDownstreamGirthWeld, CurrentUnitSystem.LengthUnitSystem.Convert(CurrentFeature.DSWDistance, DisplayUnits.Instance.AxialDistanceUnits.Scale), DisplayUnits.Instance.AxialDistanceUnits.Symbol);
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.Psafe_B31GModified, CurrentFeature.B31GModified_PressureSafeMaximum, "");
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.ERF_B31GModified, CurrentFeature.ERF_B31GModified, "");
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.ScalarType_RSF, CurrentFeature.RSF, "");
            _featureStats.Rows.Add("RSF Binned", CurrentFeature.RSFBinned, "");
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MAOP_579, CurrentUnitSystem.StressUnitSystem.Convert(CurrentFeature.MAOP579, DisplayUnits.Instance.StressUnits.Scale), DisplayUnits.Instance.StressUnits.Symbol);
            _featureStats.Rows.Add("MAOPr Binned", CurrentUnitSystem.StressUnitSystem.Convert(CurrentFeature.MAOP579Binned, DisplayUnits.Instance.StressUnits.Scale), DisplayUnits.Instance.StressUnits.Symbol);
            _featureStats.Rows.Add(QuestIntegrity.LifeQuest.Common.Resources.Strings.MAOP_Design, CurrentUnitSystem.StressUnitSystem.Convert(CurrentFeature.MAOPDesign, DisplayUnits.Instance.StressUnits.Scale), DisplayUnits.Instance.StressUnits.Symbol);
        }

        private void featureGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Context == DataGridViewDataErrorContexts.Display)
            {
                if (e.Exception is IndexOutOfRangeException)
                {
                    e.ThrowException = false;
                    e.Cancel = true;
                }
            }
        }

        private void NumericTextBox_Validating(object sender, CancelEventArgs e)
        {
            double value;
            TextBox input = sender as TextBox;
            if (input != null)
            {
                if (!double.TryParse(input.Text, out value))
                {
                    _inputError.SetIconAlignment(input, ErrorIconAlignment.MiddleRight);
                    _inputError.SetIconPadding(input, -18);
                    _inputError.SetError(input, "Please enter a numeric value.");
                    e.Cancel = true;
                }
                else
                {
                    _inputError.SetError(input, "");
                }
            }
        }

        private void featureGrid_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            CurrentInspectionFile.IsDirty = true;
        }

        private void featureGrid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            bool doDelete = true;
            if (featureView.SelectedRowsCount > 1 && _firstDelete)
            {
                _firstDelete = false;
                if (DialogResult.No == MessageBox.Show(this, string.Format("You are about to delete {0} features.\r\nDo you want to continue?", featureView.SelectedRowsCount), "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    doDelete = false;
                }
            }

            //Set flag to not delete start or end
            FeatureInfo featureToDelete = e.Row.DataBoundItem as FeatureInfo;
            if (featureToDelete.Type == FeatureType.DataStart || featureToDelete.Type == FeatureType.DataEnd)
            {
                //HACK: Problems with Start and End - Allowing users to delete these features so they can be recreated correctly.  TODO: Figure out underlying problem
                //doDelete = false;
            }

            if (!doDelete)
            {
                e.Cancel = true;
            }
        }

        private void deleteSelectedFeature_Click(object sender, EventArgs e)
        {
            bool doDelete = true;
            if (featureView.SelectedRowsCount > 1)
            {
                if (DialogResult.No == MessageBox.Show(this, string.Format("You are about to delete {0} features.\r\nDo you want to continue?", featureView.SelectedRowsCount), "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    doDelete = false;
                }
            }

            if (doDelete)
            {
                //_dataFeatures.SuspendBinding();
                
                // Create an empty list.
                //TODO: CAM fix next line
                ArrayList rows = null;// GetGridSelectedRows();
                
                foreach (DataGridViewRow row in rows)  // ro snapshot so this is OK to delete in loop
                {
                    FeatureInfo feature = row.DataBoundItem as FeatureInfo;
                    if (feature != null)
                    {
                        if (true)       //HACK: Problems with Start and End - Allowing users to delete these features so they can be recreated correctly.  TODO: Figure out underlying problem
                        //if (feature.Type != FeatureType.DataStart && feature.Type != FeatureType.DataEnd)
                        {
                            _dataFeatures.Remove(feature);
                        }
                    }
                }
                //_dataFeatures.ResumeBinding();
                OnFeatureDeleted(null);
            }
        }

        //TODO: CAM add back in
        //protected override void featureGrid_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        //{
        //    CurrentInspectionFile.IsDirty = true;
        //    if (featureView.SelectedRowsCount <= 1)
        //    {
        //        OnFeatureDeleted(null);
        //    }
        //}

        private void filterButton_Click(object sender, EventArgs e)
        {
            base.OnFilterDataSelected(new FeatureInfoEventArgs(CurrentFeature));
        }

        private void showFullButton_Click(object sender, EventArgs e)
        {
            OnShowSpreadsheetSelected(new EventArgs());
        }

        private void copyGridDataThickness_Click(object sender, EventArgs e)
        {
            if (CurrentFeature != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                CurrentFeature.CopyFeatureScalarDataGridToClipboard(ScalarType.ThicknessWall);
                Cursor.Current = Cursors.Default;
            }
        }

        private void copyGridDataRadius_Click(object sender, EventArgs e)
        {
            if (CurrentFeature != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                CurrentFeature.CopyFeatureScalarDataGridToClipboard(ScalarType.RadiusInside);
                Cursor.Current = Cursors.Default;
            }
        }

        private void chkPositionReferenceIsFixed_CheckedChanged(object sender, EventArgs e)
        {
            positionReference.Enabled = (chkPositionReferenceIsFixed.Checked);
        }

        private void recalculateCurrentFeature_Click(object sender, EventArgs e)
        {
            RecalculateFeature();
        }

        /// <summary>
        /// Recalculates all engineering and independent feature properties.
        /// Does not recalculate relative properties such as distances to welds, etc.
        /// </summary>
        private void RecalculateFeature()
        {
            CurrentFeature.CalculateFeatureIndicesUsingPosition();
            CurrentFeature.CalculateInspectionDataInformation();    // Not putting this into CalculateAndSetDepth becuase that is called from RecalculateAllFeatures and we don't want to bog down that loop by doing a full set of data loads
            CurrentFeature.CalculateFeatureEngineeringInfo();
            OnCalculateWeldDistancesRequested(new CancelEventArgs());
            OnFeatureUpdated(new FeatureInfoEventArgs(CurrentFeature));
            SetFeatureStatsToCalculatedData();
            UpdateListView();
        }

        private void addNewFeature_Click(object sender, EventArgs e)
        {
            FeatureInfo newFeature = new FeatureInfo(0, 0, 0, 2, 15)  // just some random starting values
            {
                ParentInspectionFile = CurrentInspectionFile
            };
            OnFeatureCreated(new FeatureInfoEventArgs(newFeature));
        }

        private void SortColumn(int columnIndex, ListSortDirection direction)
        {
            if (_dataFeatures.SupportsSorting)
            {
                if (columnIndex.Between(0, featureView.Columns.Count - 1))
                {
                    // Sort based on the specifed column
                    FeatureInfo selectedFeature = CurrentFeature;
                    //DataGridViewColumn sortColumn = featureView.Columns[columnIndex];
                    //featureGrid.Sort(sortColumn, direction);
                    //sortColumn.HeaderCell.SortGlyphDirection =
                    //        (direction == ListSortDirection.Ascending) ?
                    //        SortOrder.Ascending : SortOrder.Descending;

                    // Reposition the selected rows
                    SetCurrentFeature(selectedFeature);
                }
            }
        }

        private void renumberFeatures_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            DialogResult result;
            if (!CheckFeatureSortOrder())
            {
                result = MessageBox.Show(String.Format("Features are not currently sorted by position.{0}Do you want to continue?",
                                                        StringExtensions.CRLF),
                                         "Features Not Sorted",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Warning);
            }
            else
            {
                result = DialogResult.Yes;
            }

            if (result == DialogResult.Yes)
            {
                OnCalculateFeatureIDsRequested();
                OnCalculateWeldDistancesRequested(new CancelEventArgs());    // Also need to recalculate weld distances because they rely on FeatureIDs
            }
            Cursor.Current = Cursors.Default;
        }

        private bool CheckFeatureSortOrder()
        {
            double lastPosition = double.MinValue;
            
            int i = 0;
            do
            {
                DataRow row = featureView. GetDataRow(i);
                double currPosition = row.GetPropertyValue("Position").ToDouble();

                if (currPosition < lastPosition)
                {
                    return false;
                }
                
                lastPosition = currPosition;

            } while (i < featureView.RowCount);


            //foreach (DataGridViewRow row in featureView.Rows)
            //{
            //    if (row.DataBoundItem is FeatureInfo && !row.IsNewRow)
            //    {
            //        double currPosition = (row.DataBoundItem as FeatureInfo).Position;
            //        if (currPosition < lastPosition)
            //        {
            //            return false;
            //        }
            //        lastPosition = currPosition;
            //    }
            //}
            return true;
        }

        private void MainSplit_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Properties.Settings.Default.FeatureDetail_MainSplitterDistance = MainSplit.SplitterDistance;
        }

        private void BottomHalfSplit_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Properties.Settings.Default.FeatureDetail_BottomSplitterDistance = BottomHalfSplit.SplitterDistance;
        }

        private void OnFeatureCreated(FeatureInfoEventArgs e)
        {
            if (FeatureCreated != null)
                FeatureCreated(this, e);
        }

        private void OnViewBendClicked(FeatureInfoEventArgs e)
        {
            if (ViewBendRequested != null)
                ViewBendRequested(this, e);
        }

        private void OnJointSelected(FeatureInfoEventArgs e)
        {
            if (JointSelected != null)
                JointSelected(this, e);
        }

        private void OnFeatureUpdated(FeatureInfoEventArgs e)
        {
            if (FeatureUpdated != null)
                FeatureUpdated(this, e);
        }

        private void OnIMUReferenceInformationRequested()
        {
            if (IMUReferenceDetailRequested != null)
                IMUReferenceDetailRequested(this, new EventArgs());
        }

        private void OnActionRequested(ActionEventArgs e)
        {
            if (ActionRequested != null)
                ActionRequested(this, e);
        }

        private void featureGrid_Click(object sender, EventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs)e;
            if (args.Button == MouseButtons.Right)
            {
                //DataGridView.HitTestInfo gridHitTest = featureGrid.HitTest(args.Location.X, args.Location.Y);
                //// Use HitTest to determine if a row or rowheader is double-clicked.  This is more predictable
                //// than using the other events.
                //if (gridHitTest != null && CurrentFeature != null
                //        && (gridHitTest.Type == DataGridViewHitTestType.Cell || gridHitTest.Type == DataGridViewHitTestType.RowHeader)
                //        && gridHitTest.RowIndex >= 0)
                if (CurrentFeature != null)
                {
                    // Select the cell if it wasn't already selected
                    //TODO: CAM Might need next line in again
                    //SetCurrentFeature((FeatureInfo)featureGrid.Rows[gridHitTest.RowIndex].DataBoundItem);

                    // Dispplay context menu for GW Rows only
                    UpdatePipingSizeMenuItems();
                    UpdateMaterialMenuItems();
                    featureContextMenu.Show(Control.MousePosition);
                }
            }
        }

        protected override void featureView_DoubleClick(object sender, EventArgs e)
        {
            base.featureView_DoubleClick(sender, e);

            if (CurrentFeature != null)
            {
                SetFeatureStatsToCalculatedData();
                UpdateListView();
            }
        }

        //protected override void featureGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    //if (e.Button == MouseButtons.Left)
        //    //{
        //    //    base.featureGrid_ColumnHeaderMouseClick(sender, e);
        //    //}
        //    //else 
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        _columnLastClicked = (sender as DataGridView).Columns[e.ColumnIndex];
        //        columnMenuStrip.Show(Control.MousePosition);
        //    }
        //}

        private void featureGrid_KeyDown(object sender, KeyEventArgs e)
        {
            // Copy
            if (e.Control && e.KeyCode == Keys.C)
            {
                CopySelectedToClipboard();
            }

            // Select All
            if (e.Control && e.KeyCode == Keys.A)
            {
                SelectAll();
            }

            // Paste
            if (!QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly && e.Control && e.KeyCode == Keys.V)
            {
                PasteFromClipboard();
            }

            // Delete
            if (e.KeyCode == Keys.Delete)
            {
                _firstDelete = true;  // track deleting state for multi-row deleting cancelling
            }
        }

        private void PipelineFeatures_Load(object sender, EventArgs e)
        {
            ToggleAdvancedFilter(Settings.Default.ShowAdvancedFeatureFilter);
        }

        private void filterUseAdvanced_Click(object sender, EventArgs e)
        {
            ToggleAdvancedFilter(filterUseAdvanced.Checked);
        }

        private void applyAdvancedFilter_Click(object sender, EventArgs e)
        {
            string filterString = advancedFilterText.Text;
            if (filterString != null && filterString.Length > 2)  // no serious parsing; will let the filter method handle
            {
                OnApplyFilterRequested(sender, new FilteredFeatureTypesEventArgs(filterString));
            }
        }

        private void closeAdvancedFilter_Click(object sender, EventArgs e)
        {
            ResetQueryString();
            DeselectFilters();
            OnApplyFilterRequested(sender, new FilteredFeatureTypesEventArgs(true));
        }

        private void advancedFilterText_Enter(object sender, EventArgs e)
        {
            if (advancedFilterText.Text == _enterQueryString)
            {
                advancedFilterText.Text = "";
                advancedFilterText.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
            }
        }

        private void ResetQueryString()
        {
            advancedFilterText.Text = _enterQueryString;
            advancedFilterText.ForeColor = Color.FromKnownColor(KnownColor.GrayText);
        }

        private void ToggleAdvancedFilter(bool show)
        {
            queryToolStrip.Visible = show;
            filterUseAdvanced.Checked = show;
            Settings.Default.ShowAdvancedFeatureFilter = show;
            ResetQueryString();
        }

        private void PipelineFeatures_FormClosing(object sender, EventArgs e)
        {
            // save filter state
            Settings.Default.ShowAdvancedFeatureFilter = queryToolStrip.Visible;
        }

        /// <summary>
        /// Updates a dynamic list of drop-down menu items showing piping
        /// sizes taht already exist in the Piping Size form.
        /// </summary>
        private void UpdateMaterialMenuItems()
        {
            // Clear items
            menuInsertMaterialChange.DropDownItems.Clear();

            // Rebuild the items

            // New
            ToolStripMenuItem newItem = new ToolStripMenuItem() { Text = "New Material Type..." };
            newItem.Click += menuNewMaterial_Click;
            menuInsertMaterialChange.DropDownItems.Add(newItem);

            // Separator
            menuInsertMaterialChange.DropDownItems.Add(new ToolStripSeparator());

            // Dynamic items
            LookUpListHelper ll = new LookUpListHelper(CurrentInspectionFile);
            foreach (UniqueMaterial item in ll.GetUniqueMaterialList())
            {
                newItem = new ToolStripMenuItem();
                newItem.Text = item.ToString();
                newItem.Tag = item;
                newItem.CheckOnClick = true;
                newItem.Click += menuDynamicMaterial_Click;
                menuInsertMaterialChange.DropDownItems.Add(newItem);
            }
        }

        /// <summary>
        /// Updates a dynmaic list of drop-down menu items showing materials
        /// that already exist in the Materials form
        /// </summary>
        private void UpdatePipingSizeMenuItems()
        {
            // Clear items
            menuInsertPipeSizeChange.DropDownItems.Clear();
            menuAssignPipeSize.DropDownItems.Clear();

            // Rebuild menu

            // New
            ToolStripMenuItem insertMenuItem = new ToolStripMenuItem();
            ToolStripMenuItem applyMenItem = new ToolStripMenuItem();
            insertMenuItem.Text = "New Piping Size...";
            insertMenuItem.Click += menuNewPipingSize_Click;
            menuInsertPipeSizeChange.DropDownItems.Add(insertMenuItem);

            // Separator
            menuInsertPipeSizeChange.DropDownItems.Add(new ToolStripSeparator());

            // Dynamic items
            LookUpListHelper ll = new LookUpListHelper(CurrentInspectionFile);
            foreach (UniquePiping item in ll.GetUniquePipingList())
            {
                //Insert Menu
                insertMenuItem = new ToolStripMenuItem();
                insertMenuItem.Text = item.ToString();
                insertMenuItem.Tag = item;
                insertMenuItem.Click += menuDynamicInsertPipingSize_Click;
                menuInsertPipeSizeChange.DropDownItems.Add(insertMenuItem);

                //Apply menu
                applyMenItem = new ToolStripMenuItem();
                applyMenItem.Text = item.ToString();
                applyMenItem.Tag = item;
                applyMenItem.Click += menuDynamicApplyPipingSize_Click;
                menuAssignPipeSize.DropDownItems.Add(applyMenItem);
            }
        }


        #region Menu Click Handlers

        private void menuJumpToFeature_Click(object sender, EventArgs e)
        {
            if (featureView.SelectedRowsCount > 0)
            {
                OnFeatureSelected(new FeatureInfoEventArgs(CurrentFeature));
            }
        }

        private void menuInvertFeature_Click(object sender, EventArgs e)
        {
            if (featureView.SelectedRowsCount > 0)
            {
                OnActionRequested(new ActionEventArgs(ActionShortcut.InvertFeature));
            }
        }

        private void menuNewPipingSize_Click(object sender, EventArgs e)
        {
            OnGotoNewPipeInfoClick();
        }

        private void menuDynamicInsertPipingSize_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem selectedMenu = sender as ToolStripDropDownItem;
            OnAssignPipeInfoAtFeatureClick(new PipingAssignmentEventArgs(selectedMenu.Tag as UniquePiping, new List<FeatureInfo>() { CurrentFeature }, true));
        }

        private void menuDynamicApplyPipingSize_Click(object sender, EventArgs e)
        {
            //Get a list of all the selected features
            List<FeatureInfo> features = new List<FeatureInfo>();
            
            // Create an empty list. 
            ArrayList rows = GetGridSelectedRows(featureView);

            foreach (DataGridViewRow row in rows)
            {
                FeatureInfo selectedFeature = row.DataBoundItem as FeatureInfo;
                if (selectedFeature != null)
                {
                    if (FeatureTypeGroupings.JointDelimiter.Contains(selectedFeature.Type))
                    {
                        features.Add(selectedFeature);
                    }
                }
            }

            //Fire the event
            if (features.Count > 0)
            {
                ToolStripDropDownItem selectedMenu = sender as ToolStripDropDownItem;
                OnAssignPipeInfoAtFeatureClick(new PipingAssignmentEventArgs(selectedMenu.Tag as UniquePiping, features, false));
            }
        }

        private void menuNewMaterial_Click(object sender, EventArgs e)
        {
            OnGotoNewMaterialClick();
        }

        private void menuDynamicMaterial_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem selectedMenu = sender as ToolStripDropDownItem;
            OnAssignMaterialInfoAtFeatureClick(selectedMenu.Tag as UniqueMaterial, CurrentFeature);
        }

        private void menuRemoveMaterialChange_Click(object sender, EventArgs e)
        {
            OnRemoveMaterialInfoAtFeatureClick(CurrentFeature);
        }

        private void menuRemovePipeSizeChange_Click(object sender, EventArgs e)
        {
            OnRemovePipeInfoAtFeatureClick(CurrentFeature);
        }

        private void menuSelectJoint_Click(object sender, EventArgs e)
        {
            OnJointSelected(new FeatureInfoEventArgs(CurrentFeature));
        }

        private void menuViewBend_Click(object sender, EventArgs e)
        {
            OnViewBendClicked(new FeatureInfoEventArgs(CurrentFeature));
        }

        private void menuColumns_Click(object sender, EventArgs e)
        {
            WindowManager.Instance.CreateForm(FormType.FeatureSpreadsheetFormat);
        }

        private void setIMUInfo_Click(object sender, EventArgs e)
        {
            OnIMUReferenceInformationRequested();
        }

        private void OnRemovePipeInfoAtFeatureClick(FeatureInfo feature)
        {
            if (RemovePipeInfoAtFeatureClick != null)
            {
                if (feature != null)
                {
                    RemovePipeInfoAtFeatureClick(this, new EventArgs<FeatureInfo>(feature));
                }
            }
        }

        private void OnAssignPipeInfoAtFeatureClick(PipingAssignmentEventArgs args)
        {
            if (AssignPipeInfoAtFeatureClick != null)
            {
                if (args.Features != null && args.Features.Count > 0)
                {
                    AssignPipeInfoAtFeatureClick(this, args);
                }
            }
        }

        private void OnRemoveMaterialInfoAtFeatureClick(FeatureInfo feature)
        {
            if (RemoveMaterialInfoAtFeatureClick != null)
            {
                if (feature != null)
                {
                    RemoveMaterialInfoAtFeatureClick(this, new EventArgs<FeatureInfo>(feature));
                }
            }
        }

        private void OnAssignMaterialInfoAtFeatureClick(UniqueMaterial key, FeatureInfo feature)
        {
            if (AssignMaterialInfoAtFeatureClick != null)
            {
                if (feature != null)
                {
                    AssignMaterialInfoAtFeatureClick(this, new MaterialAssignmentEventArgs(key, new List<FeatureInfo>() { feature }, true));
                }
            }
        }

        private void OnGotoNewPipeInfoClick()
        {
            if (GotoPipeInfoClick != null)
            {
                GotoPipeInfoClick(this, new EventArgs<bool, double>(true, CurrentFeature.Position));
            }
        }

        private void OnGotoNewMaterialClick()
        {
            if (GotoMaterialClick != null)
            {
                GotoMaterialClick(this, new EventArgs<bool, double>(true, CurrentFeature.Position));
            }
        }

        private void menuRecalculate_Click(object sender, EventArgs e)
        {
            RecalculateFeature();
        }

        private void menuGotoPipeSection_Click(object sender, EventArgs e)
        {
            if (GotoPipeInfoClick != null)
            {
                GotoPipeInfoClick(this, new EventArgs<bool, double>(false, CurrentFeature.PositionMid));
            }
        }

        private void menuGotoMaterial_Click(object sender, EventArgs e)
        {
            if (GotoMaterialClick != null)
            {
                GotoMaterialClick(this, new EventArgs<bool, double>(false, CurrentFeature.PositionMid));
            }
        }

        private void menuFeatureDelete_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureInvert_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeLeft_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeRight_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeUp_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureNudgeDown_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowWider_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowNarrower_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowShorter_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGrowLonger_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureSetLongSeamLocation_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeaturePickMode_ItemClick(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureGirthWeld_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureFlange_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBend_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBendStart_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureBendEnd_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWeldCirc_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureTakeOff_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureTap_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureValve_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureAppurtenance_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWeld_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExtraMetal_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureInternalWallLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureExternalWallLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureDent_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureDentWithMetalLoss_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureOvality_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureLamination_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUnknown_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD1_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD2_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD3_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        private void menuFeatureUD4_CheckChanged(object sender, ItemClickEventArgs e)
        { }

        #endregion

        #endregion Private Methods

        #region Filtering Methods

        private void filter_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem checkedItem = sender as ToolStripMenuItem;
            List<FeatureType> filter = new List<FeatureType>();

            // Set the filter
            if (checkedItem.Name == "filterAll")
            {
                DeselectFilters();
            }
            else
            {
                filterAll.Checked = false;

                if (filterGirthWelds.Checked == true)
                {
                    filter.AddRange(FeatureTypeGroupings.JointDelimiter);
                }
                if (filterFeatures.Checked == true)
                {
                    filter.AddRange(FeatureTypeGroupings.Instance.Construction);
                }
                if (filterAGMs.Checked == true)
                {
                    filter.Add(FeatureType.AGM);
                }
                if (filterBends.Checked == true)
                {
                    filter.AddRange(FeatureTypeGroupings.Instance.Bends);
                }
                if (filterAnomaliesOther.Checked == true)
                {
                    filter.AddRange(FeatureTypeGroupings.Instance.NonMLAndDentAnomaly);
                }
                if (filterAnomaliesDents.Checked == true)
                {
                    filter.Add(FeatureType.Dent);
                    filter.Add(FeatureType.DentMetalLoss);
                    filter.Add(FeatureType.GeometricAnomaly);
                }
                if (filterAnomaliesMetalLoss.Checked == true)
                {
                    filter.Add(FeatureType.DentMetalLoss);
                    filter.Add(FeatureType.MetalLoss);
                }
            }

            OnApplyFilterRequested(sender, new FilteredFeatureTypesEventArgs(filter));
        }

        private void DeselectFilters()
        {
            filterGirthWelds.Checked = false;
            filterFeatures.Checked = false;
            filterBends.Checked = false;
            filterAGMs.Checked = false;
            filterFeatures.Checked = false;
            filterAnomaliesOther.Checked = false;
            filterAnomaliesDents.Checked = false;
            filterAnomaliesMetalLoss.Checked = false;
        }

        private void SetupFeatureFilterMenu()
        {
            filterGirthWelds.Tag = FeatureTypeGroupings.JointDelimiter;
            filterAGMs.Tag = new FeatureType[] { FeatureType.AGM };
            filterBends.Tag = FeatureTypeGroupings.Instance.Bends;
            filterFeatures.Tag = FeatureTypeGroupings.Instance.Construction;
            filterAnomaliesOther.Tag = FeatureTypeGroupings.Instance.NonMLAndDentAnomaly;
            filterAnomaliesDents.Tag = new FeatureType[] { FeatureType.Dent | FeatureType.DentMetalLoss | FeatureType.GeometricAnomaly };
            filterAnomaliesMetalLoss.Tag = new FeatureType[] { FeatureType.MetalLoss | FeatureType.DentMetalLoss };
        }

        private void PipelineFeatures_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.I)
            {
                CurrentFeature.Invert();
                e.Handled = true;
            }
        }

        #endregion Filtering Methods

        #region IClipboardPaste Members

        public void PasteFromClipboard()
        {
            // Just in case someone wires another event to this
            if (QuestIntegrity.Licensing.LicenseManager.Instance.IsViewerOnly)
                return;

            //Only paste if focus is on grid
            if (featureGrid.ContainsFocus)
            {
                PasteFeatureDateToGrid();
            }
            else
            {
                if (ActiveControl is TextBox)
                {
                    (ActiveControl as TextBox).Paste();
                }
            }
        }

        private void PasteFeatureDateToGrid()
        {
            //Paste data
            List<string> lines;
            if (Clipboard.GetText().Contains(Environment.NewLine) || Clipboard.GetText().Contains('\t'))
            {
                lines = Clipboard.GetText().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList<string>();

                bool supress = false;
                for (int i = 0; i < lines.Count; i++)
                {
                    string[] values = lines[i].Split('\t');
                    const int numFields = 9;
                    if (values.Length != numFields)
                    {
                        if (supress)
                        {
                            continue;
                        }
                        else
                        {
                            DialogResult answer;
                            answer = MessageBox.Show(string.Format("Row {0} does not contain {1} fields.\r\n Expected Fields are: {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}", i + 1, numFields,
                                "ID", "LabelID", "Type", "PositionInDisplayUnits", "LengthInDisplayUnits", "CircumferentialStartLocation (deg)", "SweepAngle (deg)", "LocationInWall", "Notes") +
                                "\r\n\r\nPress 'Yes' to continue to be warned; 'No' to supress further warnings; or 'Cancel' to stop rows from being pasted",
                                "Invalid Paste Data", MessageBoxButtons.YesNoCancel);
                            if (answer == DialogResult.Cancel)
                            {
                                break;
                            }
                            if (answer == DialogResult.No)
                            {
                                supress = true;
                            }
                            continue;
                        }
                    }

                    // Create a new feature and copy info
                    FeatureInfo feature = new FeatureInfo()
                    {
                        ParentInspectionFile = CurrentInspectionFile,
                        CurrentDisplayUnits = DisplayUnits.Instance,
                        ID = values[0].ToInt32(),
                        LabelID = values[1]
                    };

                    try
                    {
                        feature.Type = (FeatureType)Enum.Parse(typeof(FeatureType), values[2], true);
                    }
                    catch (ArgumentException)
                    {
                        feature.Type = FeatureType.Unknown;
                    }

                    feature.PositionInDisplayUnits = values[3].ToDouble();
                    feature.LengthInDisplayUnits = values[4].ToSingle();

                    // use degrees instead of o'clock
                    feature.CircumferentialStartLocation = values[5].ToSingle();
                    feature.SweepAngle = values[6].ToSingle();

                    try
                    {
                        feature.LocationInWall = (AnalysisInformation.InternalExternal)Enum.Parse(typeof(AnalysisInformation.InternalExternal), values[7], true);
                    }
                    catch (ArgumentException)
                    {
                        feature.LocationInWall = AnalysisInformation.InternalExternal.NotApplicable;
                    }

                    feature.Notes = values[8];

                    CurrentInspectionFile.FeatureList.Add(feature);
                }
                UpdateDataSource();
            }
        }

        #endregion IClipboardPaste Members

        #region IUndo Members

        public int UndoStackSize
        {
            get
            {
                return int.MaxValue;
            }
            set
            {
            }
        }

        public void Undo()
        {
            if (CurrentInspectionFile != null && CurrentInspectionFile.FeatureDeleteUndoStack.Count > 0)
            {
                FeatureInfo featureToRestore = CurrentInspectionFile.FeatureDeleteUndoStack.Pop();
                OnFeatureCreated(new FeatureInfoEventArgs(featureToRestore));
                SortColumn(_lastSortedColumnIndex, _columnSorts[_lastSortedColumnIndex]);
            }
        }

        #endregion
    }
}