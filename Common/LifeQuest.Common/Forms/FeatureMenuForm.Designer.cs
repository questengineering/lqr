﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/24/2012 10:27:55 AM
// Created by:   c.turiano
//
#endregion

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    partial class FeatureMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuFeature = new DevExpress.XtraBars.BarSubItem();
            this.toolFeaturePickMode = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureResizeFeatures = new DevExpress.XtraBars.BarSubItem();
            this.menuFeatureDelete = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureInvert = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeLeft = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeRight = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeUp = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureNudgeDown = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowWider = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowNarrower = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowShorter = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGrowLonger = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureSetLongSeamLocation = new DevExpress.XtraBars.BarButtonItem();
            this.menuFeatureGirthWeld = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureFlange = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBend = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBendStart = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureBendEnd = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWeldCirc = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureTakeOff = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureTap = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureValve = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureAppurtenance = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWeld = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExtraMetal = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureInternalWallLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureExternalWallLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureDent = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureDentWithMetalLoss = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureOvality = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureLamination = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUnknown = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD1 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD2 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD3 = new DevExpress.XtraBars.BarCheckItem();
            this.menuFeatureUD4 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // viewBar
            // 
            this.viewBar.Location = new System.Drawing.Point(0, 29);
            this.viewBar.ShowDisplayOptions1 = true;
            this.viewBar.ShowDisplayOptions2 = true;
            this.viewBar.ShowOptionsButton = true;
            this.viewBar.ShowScalarComboBox = true;
            this.viewBar.Size = new System.Drawing.Size(337, 30);
            // 
            // barManager
            // 
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.menuFeature,
            this.menuFeatureResizeFeatures,
            this.toolFeaturePickMode,
            this.menuFeatureGirthWeld,
            this.menuFeatureFlange,
            this.menuFeatureBend,
            this.menuFeatureBendStart,
            this.menuFeatureBendEnd,
            this.menuFeatureExternalWeldCirc,
            this.menuFeatureTakeOff,
            this.menuFeatureTap,
            this.menuFeatureValve,
            this.menuFeatureAppurtenance,
            this.menuFeatureExternalWeld,
            this.menuFeatureExtraMetal,
            this.menuFeatureInternalWallLoss,
            this.menuFeatureExternalWallLoss,
            this.menuFeatureDent,
            this.menuFeatureDentWithMetalLoss,
            this.menuFeatureOvality,
            this.menuFeatureLamination,
            this.menuFeatureUnknown,
            this.menuFeatureUD1,
            this.menuFeatureUD2,
            this.menuFeatureUD3,
            this.menuFeatureUD4,
            this.menuFeatureDelete,
            this.menuFeatureInvert,
            this.menuFeatureNudgeLeft,
            this.menuFeatureNudgeRight,
            this.menuFeatureNudgeUp,
            this.menuFeatureNudgeDown,
            this.menuFeatureGrowWider,
            this.menuFeatureGrowNarrower,
            this.menuFeatureGrowShorter,
            this.menuFeatureGrowLonger,
            this.menuFeatureSetLongSeamLocation,
            this.barButtonItem1,
            this.barButtonItem2});
            this.barManager.MaxItemId = 2202;
            // 
            // mainMenuBar
            // 
            this.mainMenuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeature)});
            this.mainMenuBar.Visible = true;
            // 
            // barTools
            // 
            this.barTools.DockCol = 1;
            this.barTools.DockRow = 0;
            // 
            // menuFeature
            // 
            this.menuFeature.Caption = "Fe&atures";
            this.menuFeature.Id = 2100;
            this.menuFeature.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.toolFeaturePickMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureResizeFeatures),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGirthWeld, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureFlange),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBend),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBendStart),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureBendEnd),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWeldCirc),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureTakeOff, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureTap),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureValve),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureAppurtenance),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWeld),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExtraMetal),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureInternalWallLoss, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureExternalWallLoss),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDent),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDentWithMetalLoss),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureOvality),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureLamination),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUnknown, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD2),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD3),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureUD4)});
            this.menuFeature.MergeOrder = 4;
            this.menuFeature.Name = "menuFeature";
            // 
            // toolFeaturePickMode
            // 
            this.toolFeaturePickMode.Caption = "Feature Pick &Mode";
            this.toolFeaturePickMode.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DrawFeature;
            this.toolFeaturePickMode.Id = 2110;
            this.toolFeaturePickMode.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.M));
            this.toolFeaturePickMode.MergeOrder = 7;
            this.toolFeaturePickMode.Name = "toolFeaturePickMode";
            this.toolFeaturePickMode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toolFeaturePickMode_ItemClick);
            // 
            // menuFeatureResizeFeatures
            // 
            this.menuFeatureResizeFeatures.Caption = "&Resize Features";
            this.menuFeatureResizeFeatures.Id = 2101;
            this.menuFeatureResizeFeatures.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureInvert),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeLeft, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeRight),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeUp),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureNudgeDown),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowWider, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowNarrower),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowShorter),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureGrowLonger),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuFeatureSetLongSeamLocation, true)});
            this.menuFeatureResizeFeatures.Name = "menuFeatureResizeFeatures";
            // 
            // menuFeatureDelete
            // 
            this.menuFeatureDelete.Caption = "&Delete";
            this.menuFeatureDelete.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.datadelete;
            this.menuFeatureDelete.Id = 2111;
            this.menuFeatureDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.menuFeatureDelete.Name = "menuFeatureDelete";
            this.menuFeatureDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureDelete_ItemClick);
            // 
            // menuFeatureInvert
            // 
            this.menuFeatureInvert.Caption = "&Invert";
            this.menuFeatureInvert.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.Invert;
            this.menuFeatureInvert.Id = 2112;
            this.menuFeatureInvert.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I);
            this.menuFeatureInvert.Name = "menuFeatureInvert";
            this.menuFeatureInvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureInvert_ItemClick);
            // 
            // menuFeatureNudgeLeft
            // 
            this.menuFeatureNudgeLeft.Caption = "Nudge &Left";
            this.menuFeatureNudgeLeft.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons._112_LeftArrowShort_Green_16x16_72;
            this.menuFeatureNudgeLeft.Id = 2113;
            this.menuFeatureNudgeLeft.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Left);
            this.menuFeatureNudgeLeft.Name = "menuFeatureNudgeLeft";
            this.menuFeatureNudgeLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeLeft_ItemClick);
            // 
            // menuFeatureNudgeRight
            // 
            this.menuFeatureNudgeRight.Caption = "Nudge &Right";
            this.menuFeatureNudgeRight.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons._112_RightArrowShort_Green_16x16_72;
            this.menuFeatureNudgeRight.Id = 2113;
            this.menuFeatureNudgeRight.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Right);
            this.menuFeatureNudgeRight.Name = "menuFeatureNudgeRight";
            this.menuFeatureNudgeRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeRight_ItemClick);
            // 
            // menuFeatureNudgeUp
            // 
            this.menuFeatureNudgeUp.Caption = "Nudge &Up";
            this.menuFeatureNudgeUp.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons._112_UpArrowShort_Green_16x16_72;
            this.menuFeatureNudgeUp.Id = 2114;
            this.menuFeatureNudgeUp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Up);
            this.menuFeatureNudgeUp.Name = "menuFeatureNudgeUp";
            this.menuFeatureNudgeUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeUp_ItemClick);
            // 
            // menuFeatureNudgeDown
            // 
            this.menuFeatureNudgeDown.Caption = "Nudge &Down";
            this.menuFeatureNudgeDown.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons._112_DownArrowShort_Green_16x16_72;
            this.menuFeatureNudgeDown.Id = 2115;
            this.menuFeatureNudgeDown.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Down);
            this.menuFeatureNudgeDown.Name = "menuFeatureNudgeDown";
            this.menuFeatureNudgeDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureNudgeDown_ItemClick);
            // 
            // menuFeatureGrowWider
            // 
            this.menuFeatureGrowWider.Caption = "Grow &Wider";
            this.menuFeatureGrowWider.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DataContainer_MoveLastHS;
            this.menuFeatureGrowWider.Id = 2155;
            this.menuFeatureGrowWider.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                | System.Windows.Forms.Keys.Up));
            this.menuFeatureGrowWider.Name = "menuFeatureGrowWider";
            this.menuFeatureGrowWider.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowWider_ItemClick);
            // 
            // menuFeatureGrowNarrower
            // 
            this.menuFeatureGrowNarrower.Caption = "Grow &Narrower";
            this.menuFeatureGrowNarrower.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DataContainer_MoveFirstHS;
            this.menuFeatureGrowNarrower.Id = 2116;
            this.menuFeatureGrowNarrower.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                | System.Windows.Forms.Keys.Down));
            this.menuFeatureGrowNarrower.Name = "menuFeatureGrowNarrower";
            this.menuFeatureGrowNarrower.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowNarrower_ItemClick);
            // 
            // menuFeatureGrowShorter
            // 
            this.menuFeatureGrowShorter.Caption = "Grow &Shorter";
            this.menuFeatureGrowShorter.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DataContainer_MovePreviousHS;
            this.menuFeatureGrowShorter.Id = 2117;
            this.menuFeatureGrowShorter.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                | System.Windows.Forms.Keys.Left));
            this.menuFeatureGrowShorter.Name = "menuFeatureGrowShorter";
            this.menuFeatureGrowShorter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowShorter_ItemClick);
            // 
            // menuFeatureGrowLonger
            // 
            this.menuFeatureGrowLonger.Caption = "Grow Lon&ger";
            this.menuFeatureGrowLonger.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DataContainer_MoveNextHS;
            this.menuFeatureGrowLonger.Id = 2118;
            this.menuFeatureGrowLonger.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                | System.Windows.Forms.Keys.Right));
            this.menuFeatureGrowLonger.Name = "menuFeatureGrowLonger";
            this.menuFeatureGrowLonger.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureGrowLonger_ItemClick);
            // 
            // menuFeatureSetLongSeamLocation
            // 
            this.menuFeatureSetLongSeamLocation.Caption = "Set Long Seam Lo&cation";
            this.menuFeatureSetLongSeamLocation.Glyph = global::QuestIntegrity.LifeQuest.Common.MenuIcons.DataContainer_MoveNextHS;
            this.menuFeatureSetLongSeamLocation.Id = 2119;
            this.menuFeatureSetLongSeamLocation.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W);
            this.menuFeatureSetLongSeamLocation.Name = "menuFeatureSetLongSeamLocation";
            this.menuFeatureSetLongSeamLocation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menuFeatureSetLongSeamLocation_ItemClick);
            // 
            // menuFeatureGirthWeld
            // 
            this.menuFeatureGirthWeld.Caption = "&Girth Weld";
            this.menuFeatureGirthWeld.Id = 2121;
            this.menuFeatureGirthWeld.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) | System.Windows.Forms.Keys.G));
            this.menuFeatureGirthWeld.Name = "menuFeatureGirthWeld";
            this.menuFeatureGirthWeld.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureFlange
            // 
            this.menuFeatureFlange.Caption = "&Flange";
            this.menuFeatureFlange.Id = 2122;
            this.menuFeatureFlange.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) | System.Windows.Forms.Keys.F));
            this.menuFeatureFlange.Name = "menuFeatureFlange";
            this.menuFeatureFlange.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureBend
            // 
            this.menuFeatureBend.Caption = "&Bend";
            this.menuFeatureBend.Id = 2123;
            this.menuFeatureBend.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.B));
            this.menuFeatureBend.Name = "menuFeatureBend";
            this.menuFeatureBend.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureBendStart
            // 
            this.menuFeatureBendStart.Caption = "Bend &Start";
            this.menuFeatureBendStart.Id = 2125;
            this.menuFeatureBendStart.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.S));
            this.menuFeatureBendStart.Name = "menuFeatureBendStart";
            this.menuFeatureBendStart.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureBendEnd
            // 
            this.menuFeatureBendEnd.Caption = "Bend &End";
            this.menuFeatureBendEnd.Id = 2124;
            this.menuFeatureBendEnd.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.E));
            this.menuFeatureBendEnd.Name = "menuFeatureBendEnd";
            this.menuFeatureBendEnd.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureExternalWeldCirc
            // 
            this.menuFeatureExternalWeldCirc.Caption = "External Weld (&Circ)";
            this.menuFeatureExternalWeldCirc.Id = 2126;
            this.menuFeatureExternalWeldCirc.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.C));
            this.menuFeatureExternalWeldCirc.Name = "menuFeatureExternalWeldCirc";
            this.menuFeatureExternalWeldCirc.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureTakeOff
            // 
            this.menuFeatureTakeOff.Caption = "&Take Off";
            this.menuFeatureTakeOff.Id = 2127;
            this.menuFeatureTakeOff.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.T));
            this.menuFeatureTakeOff.Name = "menuFeatureTakeOff";
            this.menuFeatureTakeOff.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureTap
            // 
            this.menuFeatureTap.Caption = "Ta&p";
            this.menuFeatureTap.Id = 2128;
            this.menuFeatureTap.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.P));
            this.menuFeatureTap.Name = "menuFeatureTap";
            this.menuFeatureTap.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureValve
            // 
            this.menuFeatureValve.Caption = "&Valve";
            this.menuFeatureValve.Id = 2129;
            this.menuFeatureValve.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.V));
            this.menuFeatureValve.Name = "menuFeatureValve";
            this.menuFeatureValve.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureAppurtenance
            // 
            this.menuFeatureAppurtenance.Caption = "&Appurtenance";
            this.menuFeatureAppurtenance.Id = 2120;
            this.menuFeatureAppurtenance.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.A));
            this.menuFeatureAppurtenance.Name = "menuFeatureAppurtenance";
            this.menuFeatureAppurtenance.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureExternalWeld
            // 
            this.menuFeatureExternalWeld.Caption = "&External Weld";
            this.menuFeatureExternalWeld.Id = 2130;
            this.menuFeatureExternalWeld.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.E));
            this.menuFeatureExternalWeld.Name = "menuFeatureExternalWeld";
            this.menuFeatureExternalWeld.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureExtraMetal
            // 
            this.menuFeatureExtraMetal.Caption = "E&xtra Metal";
            this.menuFeatureExtraMetal.Id = 2132;
            this.menuFeatureExtraMetal.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.X));
            this.menuFeatureExtraMetal.Name = "menuFeatureExtraMetal";
            this.menuFeatureExtraMetal.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureInternalWallLoss
            // 
            this.menuFeatureInternalWallLoss.Caption = "&Internal Wall Loss";
            this.menuFeatureInternalWallLoss.Id = 2133;
            this.menuFeatureInternalWallLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.I));
            this.menuFeatureInternalWallLoss.Name = "menuFeatureInternalWallLoss";
            this.menuFeatureInternalWallLoss.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureExternalWallLoss
            // 
            this.menuFeatureExternalWallLoss.Caption = "External &Wall Loss";
            this.menuFeatureExternalWallLoss.Id = 2134;
            this.menuFeatureExternalWallLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.W));
            this.menuFeatureExternalWallLoss.Name = "menuFeatureExternalWallLoss";
            this.menuFeatureExternalWallLoss.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureDent
            // 
            this.menuFeatureDent.Caption = "&Dent";
            this.menuFeatureDent.Id = 2135;
            this.menuFeatureDent.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.D));
            this.menuFeatureDent.Name = "menuFeatureDent";
            this.menuFeatureDent.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureDentWithMetalLoss
            // 
            this.menuFeatureDentWithMetalLoss.Caption = "Dent with &Metal Loss";
            this.menuFeatureDentWithMetalLoss.Id = 2136;
            this.menuFeatureDentWithMetalLoss.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.M));
            this.menuFeatureDentWithMetalLoss.Name = "menuFeatureDentWithMetalLoss";
            this.menuFeatureDentWithMetalLoss.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureOvality
            // 
            this.menuFeatureOvality.Caption = "&Ovality";
            this.menuFeatureOvality.Id = 2137;
            this.menuFeatureOvality.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.O));
            this.menuFeatureOvality.Name = "menuFeatureOvality";
            this.menuFeatureOvality.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureLamination
            // 
            this.menuFeatureLamination.Caption = "&Lamination";
            this.menuFeatureLamination.Id = 2138;
            this.menuFeatureLamination.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.L));
            this.menuFeatureLamination.Name = "menuFeatureLamination";
            this.menuFeatureLamination.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureUnknown
            // 
            this.menuFeatureUnknown.Caption = "&Unknown";
            this.menuFeatureUnknown.Id = 2139;
            this.menuFeatureUnknown.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.U));
            this.menuFeatureUnknown.Name = "menuFeatureUnknown";
            this.menuFeatureUnknown.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureUD1
            // 
            this.menuFeatureUD1.Caption = "User Defined &1";
            this.menuFeatureUD1.Id = 2140;
            this.menuFeatureUD1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.D1));
            this.menuFeatureUD1.Name = "menuFeatureUD1";
            this.menuFeatureUD1.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureUD2
            // 
            this.menuFeatureUD2.Caption = "User Defined &2";
            this.menuFeatureUD2.Id = 2141;
            this.menuFeatureUD2.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.D2));
            this.menuFeatureUD2.Name = "menuFeatureUD2";
            this.menuFeatureUD2.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureUD3
            // 
            this.menuFeatureUD3.Caption = "User Defined &3";
            this.menuFeatureUD3.Id = 2142;
            this.menuFeatureUD3.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.D3));
            this.menuFeatureUD3.Name = "menuFeatureUD3";
            this.menuFeatureUD3.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // menuFeatureUD4
            // 
            this.menuFeatureUD4.Caption = "User Defined &4";
            this.menuFeatureUD4.Id = 2143;
            this.menuFeatureUD4.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                | System.Windows.Forms.Keys.D4));
            this.menuFeatureUD4.Name = "menuFeatureUD4";
            this.menuFeatureUD4.ItemClick += menuFeaturePickType_ItemClick;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 2200;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Btn";
            this.barButtonItem2.Id = 2201;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // FeatureMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 100);
            this.Name = "FeatureMenuForm";
            this.Text = "FeatureMenuForm";
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraBars.BarSubItem menuFeature;
        protected DevExpress.XtraBars.BarSubItem menuFeatureResizeFeatures;
        protected DevExpress.XtraBars.BarCheckItem toolFeaturePickMode;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureGirthWeld;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureFlange;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBend;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBendStart;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureBendEnd;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWeldCirc;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureTakeOff;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureTap;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureValve;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureAppurtenance;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWeld;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExtraMetal;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureInternalWallLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureExternalWallLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureDent;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureDentWithMetalLoss;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureOvality;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureLamination;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUnknown;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD1;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD2;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD3;
        protected DevExpress.XtraBars.BarCheckItem menuFeatureUD4;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureDelete;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureInvert;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeLeft;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeRight;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeUp;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureNudgeDown;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowWider;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowNarrower;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowShorter;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureGrowLonger;
        protected DevExpress.XtraBars.BarButtonItem menuFeatureSetLongSeamLocation;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
    }
}