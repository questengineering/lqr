﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//
#endregion

using System;

namespace QuestIntegrity.LifeQuest.Common.Forms
{
    public partial class LifeQuestBaseView 
    {
        #region Private/Protected Properties

        
        protected bool _displayDataTips;
        protected bool _formClosing;
        protected bool _formLoading = true;
        protected bool _dataLoaded;
        
        #endregion

        #region Constructors

        public LifeQuestBaseView()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Properties

        public virtual bool DataLoaded
        {
            get
            {
                return _dataLoaded;
            }
        }

        #region IGraphicsViewDisplay Properties

        #endregion

        #region IDataToolTip Properties

        /// <summary>
        /// Indicate whether data tool tips should be shown or not
        /// </summary>
        public virtual bool DisplayDataTips
        {
            get
            {
                return _displayDataTips;
            }
            set
            {
                _displayDataTips = value;
                HideDataTip();
            }
        }

        #endregion

        #endregion

        #region Public Methods

        public virtual void Draw()
        {
            Refresh();
        }

        #endregion

        #region Public Events

        #endregion

        #region Private/Protected Methods

        #region Event Handlers

        protected virtual void LifeQuestBaseView_FormClosing(object sender, EventArgs e)
        {
            _formClosing = true;
        }

        #endregion

        #endregion

        #region IDataToolTip Members

        public virtual void ShowDataTip(string toolTip)
        {
            if (toolTip != string.Empty)
            {
                dataToolTip.Show(toolTip, this);
            }
            else
            {
                HideDataTip();
            }
        }

        public virtual void HideDataTip()
        {
            dataToolTip.Hide(this);
        }

        #endregion

        #region IClipboardCopy Members

        public virtual void CopySelectedToClipboard()
        {
            throw new NotImplementedException("LifeQuestBaseView::CopySelectedToFile(string filename) is not implemented: {0}.");
        }

        public virtual void SelectAll()
        {
            throw new NotImplementedException("LifeQuestBaseView::CopySelectedToFile(string filename) is not implemented: {0}.");
        }

        public virtual void SelectNone()
        {
            throw new NotImplementedException("LifeQuestBaseView::CopySelectedToFile(string filename) is not implemented: {0}.");
        }

        #endregion

        #region IFileCopy Members

        public virtual void CopySelectedToFile(string fileName)
        {
            throw new NotImplementedException(string.Format("LifeQuestBaseView::CopySelectedToFile(string filename) is not implemented: {0}.", fileName));
        }

        #endregion
    }
}