#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/20/2013 7:39:48 AM
// Created by:   D.Revelle, Gutted by J.Foster

#endregion Copyright Quest Integrity Group, LLC 2010

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.IO;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.Data.DataCache;
using QuestIntegrity.LifeQuest.Common.Properties;

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    ///     DataManager class for controlling file loading and passing to graphics
    /// </summary>
    public class DataManager
    {
        #region Private/Protected Variables

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion Private/Protected Variables

        #region Private Enums

        protected enum ProjectVersions { Version1 = 1 }

        #endregion

        #region Constructors

        protected DataManager()
        {
            const int minMB = 600;
            const int maxMB = 32000; //put a max cap of 32 GB
            
            int startingMemory = Settings.Default.MaximumMemoryMB.Constrain(minMB, maxMB);
            Settings.Default.MaximumMemoryMB = startingMemory;
            //FileCacheManager.Instance.MaximumMB = startingMemory;
        }

        #endregion Constructors

        #region Public Properties

        public ProjectInfo CurrentProject { get; protected set; }

        /// <summary>
        ///     Gets an ordered lists of paths to try finding Xml configuration files for the project
        /// </summary>
        [XmlIgnore]
        public List<string> TryPaths
        {
            get
            {
                List<string> tryPaths = new List<string>();
                if (CurrentProject != null)
                {
                    tryPaths.Add(CurrentProject.DirectoryPath);
                }
                tryPaths.Add(PathExtensions.CommonApplicationDataPath);
                tryPaths.Add(PathExtensions.ApplicationPath);
                return tryPaths;
            }
        }

        #endregion Public Properties

        #region Public Events

        public event EventHandler<EventArgs> ProjectClosed;

        private void OnProjectClosed()
        {
            if (ProjectClosed != null) ProjectClosed(this, EventArgs.Empty);
        }

        #endregion Public Events

        #region Public Methods

        public bool CloseCurrentProject()
        {
            if (CurrentProject != null)
            {
                Log.DebugFormat("Closing Project: {0}.", CurrentProject.FilePath);
                OnProjectClosed();
                CurrentProject = null;
            }
            
            FileCacheManager.Instance.DeRegisterAll();
            Log.Info("Closed Project.");
            GC.Collect();
            return true;
        }

        public virtual void SaveProject()
        {
            throw new NotImplementedException("Base version of SaveProject not implemented.");
        }

        public virtual void NewProject(string fileName)
        {
            CurrentProject = new ProjectInfo
                              {
                                  FileName = Path.GetFileName(fileName),
                                  DirectoryPath = Path.GetDirectoryName(fileName),
                                  Description = "",
                              };
            Log.InfoFormat("New Project: {0}.", fileName);
        }

        /// <summary>
        ///     Gets a list of paths combining the standard locations of files and a base filename
        /// </summary>
        /// <param name="BaseFilename">The filename.</param>
        /// <returns></returns>
        public IList<string> GetTryPaths(string BaseFilename)
        {
            return TryPaths.Select(ThePath => Path.Combine(ThePath, BaseFilename)).ToList();
        }

        #endregion Public Methods

        #region Private Methods

        #endregion Private Methods

        #region Data Reading



        #endregion

        
    }
}