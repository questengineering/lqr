#region Copyright Quest Integrity Group, LLC 2012
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 10:06:31 AM
// Created by:   j.rowe
//
// Filename: $Id: FileTypes.cs,v 1.5 2012/01/03 18:59:11 J.Rowe Exp $
#endregion


namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    /// Enumeration of LifeQuest file types
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// .prjX file type
        /// </summary>
        ProjectFile,

        /// <summary>
        /// .qig file type
        /// </summary>
        InspectionFile,

        /// <summary>
        /// .qid file type
        /// </summary>
        DataFile,
    }

}
