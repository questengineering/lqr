#region Copyright Quest Integrity Group, LLC 2011
//
// www.questintegritygroup.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 11/29/2011 2:45:58 PM
// Created by:   J.Rowe
//
// File:  $Id: ProjectState.cs,v 1.1 2011/11/30 00:52:36 J.Rowe Exp $
#endregion

namespace QuestIntegrity.LifeQuest.Common
{
    /// <summary>
    /// Enum for indicating state of project.
    /// </summary>    
    public enum ProjectState
    {
        NoProject,
        ProjectLoaded,
        SingleInspectionFileLoaded,
        MultipleInspectionFilesLoaded
    }
}