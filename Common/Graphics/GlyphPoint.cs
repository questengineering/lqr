#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 3/25/2010 5:09:58 PM
// Created by:   J.Rowe
//
#endregion

using System.Drawing;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics
{
    /// <summary>
    /// Simple structure to hold data to render a Glyph (point + glyph symbol)
    /// </summary>
    public class GlyphPoint
    {
        #region Enums
        /// <summary>
        /// Glyph symbols used in rendering
        /// </summary>
        public enum Symbol
        {
            None = 0,
            Vertext = 1,
            Dash = 2,
            Cross = 3,
            ThickCross = 4,
            Triangle = 5,
            Square = 6,
            Circle = 7,
            Diamond = 8,
            Arrow = 9,
            ThickArrow = 10,
            HookedArrow = 11,
            EdgeArrow = 12
        }
        #endregion

        #region Constructors
        public GlyphPoint(CartesianPoint point)
        {
            X = point.X;
            Y = point.Y;
            Z = point.Z;
            DrawSymbol = DefaultSymbol;
            DrawColor = DefaultColor;
        }
        public GlyphPoint(CartesianPoint point, string glyphType, Symbol drawSymbol, Color drawColor)
            : this(point)
        {
            this.GlyphType = glyphType;
            DrawSymbol = drawSymbol;
            DrawColor = drawColor;
        }
        public GlyphPoint(double x, double y, double z, string glyphType, Symbol drawSymbol, Color drawColor)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.GlyphType = glyphType;
            DrawSymbol = drawSymbol;
            DrawColor = drawColor;
        }
        #endregion

        #region Public Properties
        public static Symbol DefaultSymbol = Symbol.Triangle;
        public static Color DefaultColor = Color.Black;
        public string GlyphType;
        public double X;
        public double Y;
        public double Z;
        /// <summary>
        /// The color used for rendering the glyph
        /// </summary>
        public Color DrawColor;
        /// <summary>
        /// The symbol to be used in rendering the glyph
        /// </summary>
        public Symbol DrawSymbol
        {
            get;
            set;
        }

        public override int GetHashCode()
        {
            int hash = this.X.GetHashCode();
            hash ^= this.Y.GetHashCode();
            hash ^= this.Z.GetHashCode();
            hash ^= this.GlyphType.GetHashCode();
            hash ^= this.DrawSymbol.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != typeof(GlyphPoint))
                return false;

            return Equals((GlyphPoint)obj);
        }

        public bool Equals(GlyphPoint ptOther)
        {
            if (ptOther.X == X && ptOther.Y == Y && ptOther.Z == Z && ptOther.GlyphType == GlyphType && ptOther.DrawSymbol == DrawSymbol)
            {
                return true;
            }
            return false;
        }

        public static bool operator ==(GlyphPoint point1, GlyphPoint point2)
        {
            return point1.Equals(point2);
        }

        public static bool operator !=(GlyphPoint point1, GlyphPoint point2)
        {
            return !point1.Equals(point2);
        }

        #endregion

    }
}
