﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: StripChartAxes.cs,v 1.6 2010/09/09 00:01:57 J.Rowe Exp $
//
#endregion

using System;
using System.Drawing;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics
{

    /// <summary>
    /// Provides interface for creating axes around the Strip Chart view.  Will create
    /// 4 axes - top/bottom/left/right, and label based on the number of Strips,
    /// SlicesPerStrip, and the Offset.
    /// 
    /// Axes are rendered on the global coordinate system and should scale with normal camera
    /// scaling accordingly.
    /// </summary>
    public class StripChartAxes : IDisposable
    {
        #region Private Properties

        private vtkAxisActor2D _xBottomAxis;
        private vtkAxisActor2D _xTopAxis;
        private vtkAxisActor2D _yLeftAxis;
        private vtkAxisActor2D _yRightAxis;
        private int _yAxisOffset; //correction to place y axis in the middle of the strip
        private CartesianBounds<double> _startingBounds;
        private const string LabelFormat = "%8.0f";
        private const double DesiredFontSize = 9.0d;

        #endregion

        #region Constructors

        public StripChartAxes()
        {
            _xBottomAxis = new vtkAxisActor2D();
            _xTopAxis = new vtkAxisActor2D();
            _yLeftAxis = new vtkAxisActor2D();
            _yRightAxis = new vtkAxisActor2D();

            _xBottomAxis.SetTitle("Position Index");

            _xBottomAxis.SetLabelFormat(LabelFormat);
            _xTopAxis.SetLabelFormat(LabelFormat);
            _yLeftAxis.SetLabelFormat(LabelFormat);
            _yRightAxis.SetLabelFormat(LabelFormat);

            //_XBottomAxis.SetTickOffset(-5);
            //_XTopAxis.SetTickOffset(-5);
            //_YLeftAxis.SetTickOffset(-5);
            //_YRightAxis.SetTickOffset(-5);

            _yLeftAxis.AxisVisibilityOff();  //just show the tick marks
            _yRightAxis.AxisVisibilityOff();  //just show the tick marks

            SetFonts();
            AxisColor = Color.Black;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The number of major tick marks (and labels) for the X-Axis
        /// </summary>
        public int NumMajorTicks
        {
            get;
            set;
        }

        /// <summary>
        /// Toggles visibility of the axes
        /// </summary>
        public bool Visible
        {
            get
            {
                return _xBottomAxis.GetVisibility().ToBool();
            }
            set
            {
                _xBottomAxis.SetVisibility(value.ToInt());
                _xTopAxis.SetVisibility(value.ToInt());
                _yLeftAxis.SetVisibility(value.ToInt());
                _yRightAxis.SetVisibility(value.ToInt());
            }
        }

        /// <summary>
        /// Gets/sets the color of the Axis, Tickmarks, and fonts for each axis
        /// </summary>
        public Color AxisColor
        {
            get
            {
                return _xBottomAxis.GetProperty().GetColor().ToColor();
            }
            set
            {
                Color color = value;
                _xBottomAxis.GetProperty().SetColor(color);
                _xTopAxis.GetProperty().SetColor(color);
                _yLeftAxis.GetProperty().SetColor(color);
                _yRightAxis.GetProperty().SetColor(color);
                _xBottomAxis.GetTitleTextProperty().SetColor(color);
                _xTopAxis.GetTitleTextProperty().SetColor(color);
                _yLeftAxis.GetTitleTextProperty().SetColor(color);
                _yRightAxis.GetTitleTextProperty().SetColor(color);
                _xBottomAxis.GetLabelTextProperty().SetColor(color);
                _xTopAxis.GetLabelTextProperty().SetColor(color);
                _yLeftAxis.GetLabelTextProperty().SetColor(color);
                _yRightAxis.GetLabelTextProperty().SetColor(color);
            }
        }

        #endregion

        #region Public Methods

        public void ScaleFontToScreen(int viewPortWidth)
        {
            //VTk's font sizing on the axis is completely lame.  It seems to under- or over-size
            // the font as the screen size change.  This uses an empirically determined
            // adjustment
            double newFontSize = 0.0087 * (viewPortWidth) + 5;  //empirically derived slope of how it adjusts font size to screen size
            double factor = DesiredFontSize / newFontSize;      //set factor relative to the desired size in px

            _xBottomAxis.SetFontFactor(factor);
            _xTopAxis.SetFontFactor(factor);
            _yLeftAxis.SetFontFactor(factor);
            _yRightAxis.SetFontFactor(factor);
        }

        public void SetBounds(CartesianBounds<double> bounds)
        {
            _startingBounds = bounds;
        }

        public void SetPosition(CartesianBounds<double> bounds, int numStrips, int numSlicesPerStrip, int numReadingsPerSlice, int startingIndex)
        {
            //General setup of the Axes
            _yAxisOffset = (numReadingsPerSlice / 2);

            _startingBounds = bounds;

            //put all the axes coords in the World coordinate system
            _xBottomAxis.GetPositionCoordinate().SetCoordinateSystemToWorld();
            _xBottomAxis.GetPosition2Coordinate().SetCoordinateSystemToWorld();
            _xTopAxis.GetPositionCoordinate().SetCoordinateSystemToWorld();
            _xTopAxis.GetPosition2Coordinate().SetCoordinateSystemToWorld();
            _yLeftAxis.GetPositionCoordinate().SetCoordinateSystemToWorld();
            _yLeftAxis.GetPosition2Coordinate().SetCoordinateSystemToWorld();
            _yRightAxis.GetPositionCoordinate().SetCoordinateSystemToWorld();
            _yRightAxis.GetPosition2Coordinate().SetCoordinateSystemToWorld();

            //Set the End points of the axes
            SetAxesPosition(1.0);

            //Set ranges based on the actual axial indices being plotted
            _xBottomAxis.SetRange(startingIndex, startingIndex + numSlicesPerStrip - 1);
            _xTopAxis.SetRange(startingIndex + (numSlicesPerStrip * numStrips) - 1, startingIndex + (numSlicesPerStrip * (numStrips - 1)));
            _yLeftAxis.SetRange(startingIndex + (numSlicesPerStrip * (numStrips - 1)), startingIndex);
            _yRightAxis.SetRange(startingIndex + numSlicesPerStrip - 1, startingIndex + (numSlicesPerStrip * numStrips) - 1);

            //prevent from rounding - note, if there are >25 labels, then it seems to reduce the number
            // automatically...as noted below, this should probably be handled at some point.
            _xBottomAxis.AdjustLabelsOff();
            _xTopAxis.AdjustLabelsOff();
            _yLeftAxis.AdjustLabelsOff();
            _yRightAxis.AdjustLabelsOff();

            //Set to a specific number of labels corresponding to the number of rows
            _xBottomAxis.SetNumberOfLabels(NumMajorTicks);
            _xTopAxis.SetNumberOfLabels(NumMajorTicks);

            _yLeftAxis.SetNumberOfLabels(numStrips);
            _yRightAxis.SetNumberOfLabels(numStrips);
        }

        public void SetScale(double yScale)
        {
            //Reposition end points based on scale
            SetAxesPosition(yScale);
        }

        public void AddToRenderer(vtkRenderer renderer)
        {
            renderer.AddActor2D(_xBottomAxis);
            renderer.AddActor2D(_xTopAxis);
            renderer.AddActor2D(_yLeftAxis);
            renderer.AddActor2D(_yRightAxis);
        }

        #endregion

        #region Private Methods

        private void SetAxesPosition(double yScale)
        {
            if (_startingBounds == null)
                return;

            //Note: need to rescale the X-Axis as specified.  This would be set, for example,
            // if the global coordinate space was being anisotropic-ly scaled
            // as per a locked-zoom mode.         
            _xBottomAxis.SetPosition(_startingBounds.XMin, _startingBounds.YMin * yScale);
            _xBottomAxis.SetPosition2(_startingBounds.XMax, _startingBounds.YMin * yScale);

            _xTopAxis.SetPosition(_startingBounds.XMax, _startingBounds.YMax * yScale);
            _xTopAxis.SetPosition2(_startingBounds.XMin, _startingBounds.YMax * yScale);

            _yLeftAxis.SetPosition(_startingBounds.XMin, (_startingBounds.YMax - _yAxisOffset) * yScale);
            _yLeftAxis.SetPosition2(_startingBounds.XMin, (_startingBounds.YMin + _yAxisOffset) * yScale);

            _yRightAxis.SetPosition(_startingBounds.XMax, (_startingBounds.YMin + _yAxisOffset) * yScale);
            _yRightAxis.SetPosition2(_startingBounds.XMax, (_startingBounds.YMax - _yAxisOffset) * yScale);
        }

        private void SetFonts()
        {

            vtkTextProperty[] props = { _xBottomAxis.GetTitleTextProperty(),
                                        _xTopAxis.GetTitleTextProperty(),
                                        _yLeftAxis.GetTitleTextProperty(),
                                        _yRightAxis.GetTitleTextProperty(),
                                        _xBottomAxis.GetLabelTextProperty(),
                                        _xTopAxis.GetLabelTextProperty(),
                                        _yLeftAxis.GetLabelTextProperty(),
                                        _yRightAxis.GetLabelTextProperty()                                                                           
                                      };

            foreach (vtkTextProperty prop in props)
            {
                prop.BoldOff();
                prop.ItalicOff();
                prop.ShadowOff();
                prop.SetFontFamilyToArial();
                prop.SetJustificationToLeft();
            }
        }

        #endregion

        #region IDispose Pattern Implementation

        private bool _disposed = false;

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // Managed Resources
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_xBottomAxis != null)
                        _xBottomAxis.Dispose();
                    if (_xTopAxis != null)
                        _xTopAxis.Dispose();
                    if (_yLeftAxis != null)
                        _yLeftAxis.Dispose();
                    if (_yRightAxis != null)
                        _yRightAxis.Dispose();
                }

                //Cleanup Unmanaged resources


                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}
