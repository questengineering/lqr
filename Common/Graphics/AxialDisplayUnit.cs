#region Copyright Quest Reliability, LLC 2008
//
// www.questreliability.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/6/2008 10:06:40 AM
// Created by:   j.rowe
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace QuestReliability.Graphics
{
    /// <summary>
    /// Indicates if axial units should be displayted in Degrees or OClock
    /// </summary>
    public enum CircumferentialDisplayUnit
    {
        Degrees,
        OClock,
        None
    }
}
