#region Copyright Quest Reliability, LLC 2008
//
// www.questreliability.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/26/2008 9:17:17 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Kitware.VTK;

namespace QuestReliability.Graphics
{
    /// <summary>
    /// TODO: Summary goes here.
    /// </summary>

    public class ScalarBar : IDisposable
    {
        #region Enums
        #endregion

        #region Constants
        #endregion

        #region Private/Protected Variables
        
        private vtkScalarBarActor   _scalarBarActor;
        private vtkScalarBarWidget  _scalarBarWidget;
        private vtkTextProperty     _prop;

        private bool _disposed = false;

        #endregion

        #region Constructors

        public ScalarBar(vtkRenderer ren)
        {
            _prop = new vtkTextProperty();
            _scalarBarActor = new vtkScalarBarActor();
            _scalarBarWidget = new vtkScalarBarWidget();
          
            _prop.SetFontFamilyToArial();
            _prop.SetFontSize(10);
            _prop.SetColor(0,0,0);
            _prop.SetJustificationToCentered();

            _scalarBarActor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport();
            _scalarBarActor.SetPosition(.45, .045);
            _scalarBarActor.SetOrientationToHorizontal();
            _scalarBarActor.SetWidth(.5);
            _scalarBarActor.SetHeight(0.1);
            _scalarBarActor.SetLabelFormat("%g");
            _scalarBarActor.SetLabelTextProperty(_prop);
            _scalarBarActor.SetTextPositionToSucceedScalarBar();
            _scalarBarActor.SetTitleTextProperty(_prop);

            _scalarBarWidget.SetInteractor(ren.GetRenderWindow().GetInteractor());
            _scalarBarWidget.SetCurrentRenderer(ren);
            _scalarBarWidget.SetScalarBarActor(_scalarBarActor);

            double [] pos = _scalarBarActor.GetPosition();
            _scalarBarWidget.GetScalarBarRepresentation().SetPosition(ref pos[0]);
           
            double [] pos2 = _scalarBarActor.GetPosition2();
            _scalarBarWidget.GetScalarBarRepresentation().SetPosition2(ref pos2[0]);
          
            _scalarBarWidget.EnabledOff();
        }
      
        public ScalarBar(string title, vtkRenderer ren) : this(ren)
        {
            _scalarBarActor.SetTitle(title); 
        }
      
        #endregion

        #region Public Properties

        public String Title
        {
            get { return _scalarBarActor.GetTitle(); }
            set { _scalarBarActor.SetTitle(value); }
        }
        public double Height
        {
            get { return _scalarBarActor.GetHeight(); }
            set { _scalarBarActor.SetHeight(value); }
        }
        public double Width
        {
            get { return _scalarBarActor.GetWidth(); }
            set { _scalarBarActor.SetWidth(value); }
        }
        public double [] Position
        {
            get { return _scalarBarActor.GetPosition(); }
            set { _scalarBarActor.SetPosition(ref value[0]); }
        }

        #endregion

        #region Public Methods
        
        public void Draw(vtkLookupTable lut, vtkRenderer ren)
        {
            ren.RemoveActor2D(_scalarBarActor);
            
            _scalarBarActor.SetLookupTable(lut);
            _scalarBarActor.Modified();

            ren.AddActor2D(_scalarBarActor);
            ren.Render();
        }

        public void SetOrientationToHorizontal()
        {
            _scalarBarActor.SetOrientationToHorizontal();
        }
        public void SetOrientationToVertical()
        {
            _scalarBarActor.SetOrientationToVertical();
        }

        public void EditingOn()
        {
            _scalarBarWidget.EnabledOn();
        }
        public void EditingOff()
        {
            _scalarBarWidget.EnabledOff();
        }

        public void VisibilityOn()
        {
            _scalarBarActor.VisibilityOn();
  
        }
        public void VisibilityOff()
        {
            _scalarBarActor.VisibilityOff();
        }

        #endregion

        #region Private Methods

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // Managed Resources
                if (disposing)
                {
                    _prop.Dispose();
                    _scalarBarActor.Dispose();
                    _scalarBarWidget.Dispose();
                }

                //Set Flag
                _disposed = true;
            }
        }

        #endregion

    }
}
 