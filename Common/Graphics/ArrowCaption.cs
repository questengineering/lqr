#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/8/2008 10:53:08 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using Kitware.VTK;

namespace QuestIntegrity.Graphics
{

    #region Enums

    public enum CaptionPosition
    {
        BottomRight = 0,
        BottomLeft = 1,
        TopRight = 2,
        TopLeft = 3
    };

    #endregion

    /// <summary>
    /// ArrowCaption
    /// </summary>
    public class ArrowCaption : IDisposable
    {
        #region Private/Protected Variables

        private double[] _captionColor = new double[3] { 0.0, 0.0, 0.0 };
        private CaptionPosition _location = CaptionPosition.TopLeft;

        private vtkTextProperty _tprop;
        private vtkCaptionActor2D _captionActor;
        private vtkConeSource _arrow;

        private bool _disposed;

        #endregion

        #region Constructors

        public ArrowCaption(string caption)
        {
            _tprop = new vtkTextProperty();
            _tprop.SetFontFamilyToArial();
            _tprop.SetFontSize(10);
            _tprop.BoldOn();
            _tprop.ShadowOn();
            _tprop.SetColor(_captionColor[0], _captionColor[1], _captionColor[2]);

            _arrow = new vtkConeSource();
            _arrow.SetResolution(10);

            _captionActor = new vtkCaptionActor2D();
            _captionActor.SetCaptionTextProperty(_tprop);
            _captionActor.SetCaption(caption);
            _captionActor.GetProperty().SetColor(_captionColor[0], _captionColor[1], _captionColor[2]);
            _captionActor.SetLeaderGlyph(_arrow.GetOutput());
            _captionActor.SetMaximumLeaderGlyphSize(1);
            _captionActor.SetLeaderGlyphSize(0.01);
            _captionActor.ThreeDimensionalLeaderOn();
            _captionActor.BorderOn();

            _captionActor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport();
            _captionActor.GetPositionCoordinate().SetReferenceCoordinate(null);
        }

        public ArrowCaption(string caption, double width, double height, double[] attachmentPostion, CaptionPosition captionPositon) : this(caption)
        {
            _captionActor.SetWidth(width);
            _captionActor.SetHeight(height);
            _captionActor.SetAttachmentPoint(attachmentPostion[0], attachmentPostion[1], attachmentPostion[2]);

            _location = captionPositon;

            if(_location == CaptionPosition.TopLeft)
            {
                _captionActor.GetPositionCoordinate().SetValue(0.9, 0.1);
            }
            else if(_location == CaptionPosition.TopRight)
            {
                _captionActor.GetPositionCoordinate().SetValue(0.9, 0.9);
            }
            else if(_location == CaptionPosition.BottomLeft)
            {
                _captionActor.GetPositionCoordinate().SetValue(0.1, 0.1);
            }
            else if(_location == CaptionPosition.BottomRight)
            {
                _captionActor.GetPositionCoordinate().SetValue(0.1, 0.9);
            }
        }

        #endregion

        #region Public Properties

        public string Caption
        {
            get
            {
                return _captionActor.GetCaption();
            }
            set
            {
                _captionActor.SetCaption(value);
            }
        }

        public double[] CaptionColor
        {
            get { return _captionColor; }
            set
            {
                _captionColor = value;
                _tprop.SetColor(_captionColor[0], _captionColor[1], _captionColor[2]);
                _captionActor.GetProperty().SetColor(_captionColor[0], _captionColor[1], _captionColor[2]);
            }
        }

        public double Height
        {
            get
            {
                return _captionActor.GetHeight();
            }
            set
            {
                _captionActor.SetHeight(value);
            }
        }

        public double Width
        {
            get
            {
                return _captionActor.GetWidth();
            }
            set
            {
                _captionActor.SetWidth(value);
            }
        }

        public CaptionPosition Position
        {
            get { return _location; }
            set
            {
                _location = value;

                if(value == CaptionPosition.TopLeft)
                {
                    _captionActor.GetPositionCoordinate().SetValue(0.9, 0.1);
                }
                else if(value == CaptionPosition.TopRight)
                {
                    _captionActor.GetPositionCoordinate().SetValue(0.9, 0.9);
                }
                else if(value == CaptionPosition.BottomLeft)
                {
                    _captionActor.GetPositionCoordinate().SetValue(0.1, 0.1);
                }
                else if(value == CaptionPosition.BottomRight)
                {
                    _captionActor.GetPositionCoordinate().SetValue(0.1, 0.9);
                }
            }
        }

        public double[] AttachmentPosition
        {
            get
            { return _captionActor.GetAttachmentPoint(); }
            set
            {
                _captionActor.SetAttachmentPoint(value[0], value[1], value[2]);
            }
        }

        #endregion

        #region Public Methods

        public void Draw(vtkRenderer ren)
        {
            ren.RemoveActor2D(_captionActor);

            ren.AddActor2D(_captionActor);
            ren.Render();
        }

        public void BorderOn()
        {
            _captionActor.BorderOn();
        }

        public void BorderOff()
        {
            _captionActor.BorderOff();
        }

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if(!this._disposed)
            {
                // Managed Resources
                if(disposing)
                {
                    _arrow.Dispose();
                    _captionActor.Dispose();
                    _tprop.Dispose();
                }

                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}