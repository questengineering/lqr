﻿using System;
using System.Drawing;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics
{
    public class ViewSectionIndicator : IDisposable
    {
        #region Private Fields
        private CartesianPoint _position;
        private CartesianPoint _scale;
        private double _height;
        private int[] _directionVector;
        public TextPositions TextPosition;
        private readonly vtkLineSource _top = new vtkLineSource(), _middle = new vtkLineSource(), _bottom = new vtkLineSource();
        private readonly vtkConeSource _topArrow = new vtkConeSource(), _bottomArrow = new vtkConeSource();
        private vtkPropAssembly _indicator;

        private readonly vtkActor _topActor = new vtkActor(), _middleActor = new vtkActor(), _bottomActor = new vtkActor(), _topArrowActor = new vtkActor(), _bottomArrowActor = new vtkActor();
        private readonly vtkPolyDataMapper _topMapper = vtkPolyDataMapper.New(), _middleMapper = vtkPolyDataMapper.New(), _bottomMapper = vtkPolyDataMapper.New(), _topArrowMapper = vtkPolyDataMapper.New(), _bottomArrowMapper = vtkPolyDataMapper.New();

        //Sizing for the arrowheads - measurements in Pixels. Will be scaled to world units
        private const double ExtenderLength = 8, ArrowHeight = 7, ArrowRadius = 4, ArrowOffsetX = ArrowHeight / 2;
        private readonly MarkupText3D _text = new MarkupText3D();

        #endregion

        #region Constructor
        public ViewSectionIndicator()
        {
            BuildIndicator();

            Direction = new [] { 1, 0, 0 };
            UnitsPerPixel = new CartesianPoint(1, 1, 1);
            ShowArrows = true;
        }

        /// <summary>
        /// Sets the position of the indicator
        /// </summary>
        /// <param name="position"></param>
        /// <param name="height"></param>
        public void SetPosition(CartesianPoint position, double height)
        {
            _position = position;
            _position.Z = 0.1; //offset to put above grid
            _height = height * _scale.Y;
            Redraw();
        }

        /// <summary>
        /// Scales the height from the origin position
        /// </summary>
        /// <param name="scale"></param>
        public void SetScale(CartesianPoint scale)
        {
            _scale = scale;
            Redraw();
        }

        public void AddToRenderer(vtkRenderer renderer)
        {
            renderer.AddActor(_indicator);
            _text.SetCamera(renderer.GetActiveCamera());
        }

        public void RemoveFromRenderer(vtkRenderer renderer)
        {
            renderer.RemoveActor(_indicator);    
        }

        public void SetTextPosition(TextPositions pos)
        {
            TextPosition = pos;
            if (pos == TextPositions.Bottom)
                _text.Position = new Point3D { X = _position.X, Y = _position.Y + 1, Z = _position.Z };
            else if (pos == TextPositions.Top)
                _text.Position = new Point3D { X = _position.X, Y = _position.Y + _height - 1, Z = _position.Z };
            else if (pos == TextPositions.Middle)
                _text.Position = new Point3D { X = _position.X, Y = _position.Y + _height / 2d, Z = _position.Z };
        }

        public void Redraw()
        {

            double extender = ExtenderLength * UnitsPerPixel.X;
            double arrowOffsetX = ArrowOffsetX * UnitsPerPixel.X;
            double yOffset = 1;// UnitsPerPixel.Y * _yOffset * _scale.Y;

            _topActor.GetProperty().SetLineWidth(1.0f);
            _middleActor.GetProperty().SetLineWidth(2.0f);
            _bottomActor.GetProperty().SetLineWidth(1.0f);

            _top.SetPoint1(_position.X, _position.Y + _height + yOffset, _position.Z);
            _top.SetPoint2(_position.X + (_directionVector[0] * extender), _position.Y + _height + yOffset, _position.Z);

            _middle.SetPoint1(_position.X, _position.Y + _height + yOffset, _position.Z);
            _middle.SetPoint2(_position.X, _position.Y - yOffset, _position.Z);

            _bottom.SetPoint1(_position.X, _position.Y - yOffset, _position.Z);
            _bottom.SetPoint2(_position.X + (_directionVector[0] * extender), _position.Y - yOffset, _position.Z);

            _topArrow.SetRadius(ArrowRadius * UnitsPerPixel.Y);
            _topArrow.SetHeight(ArrowHeight * UnitsPerPixel.Y);
            _topArrow.SetCenter(_position.X + (_directionVector[0] * (extender + arrowOffsetX)), _position.Y + _height + yOffset, _position.Z);
            _topArrow.SetDirection(Direction[0], Direction[1], Direction[2]);

            _bottomArrow.SetRadius(ArrowRadius * UnitsPerPixel.Y);
            _bottomArrow.SetHeight(ArrowHeight * UnitsPerPixel.X);
            _bottomArrow.SetCenter(_position.X + (_directionVector[0] * (extender + arrowOffsetX)), _position.Y - yOffset, _position.Z);
            _bottomArrow.SetDirection(Direction[0], Direction[1], Direction[2]);

            _topArrowActor.SetVisibility(ShowArrows.ToInt());
            _bottomArrowActor.SetVisibility(ShowArrows.ToInt());

            SetTextPosition(TextPosition);

            //Make the text take up 18 pixels of X and Y equivalent
            var scaleMult = Math.Min(Scale.X, Scale.Y);
            _text.Actor.SetScale(scaleMult * TextSize, scaleMult * TextSize, Scale.Z); //looks weird. I don't like it.
            
        }

        #endregion

        #region Public Properties

        public int TextSize = 16;
        
        /// <summary>
        /// Set the WorldUnits per pixel used for scaling the arrow heads
        /// </summary>
        public CartesianPoint UnitsPerPixel { get; set; }

        /// <summary>
        /// Sets the scale used for the indicator.  Setting this property will not trigger
        /// a Redraw(). This method most be called explicitly or is called from SetPosition()
        /// </summary>
        public CartesianPoint Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _scale = value;
            }
        }

        /// <summary>
        /// The number of major tick marks (and labels) for the X-Axis
        /// </summary>
        public bool ShowArrows
        {
            get
            {
                return _topArrowActor.GetVisibility().ToBool();
            }
            set
            {
                _topArrowActor.SetVisibility(value.ToInt());
                _bottomArrowActor.SetVisibility(value.ToInt());
            }
        }

        /// <summary> Arrow direction vector.  E.g., {0,0,1} </summary>
        public int[] Direction
        {
            get
            {
                return _directionVector;
            }
            set
            {
                _directionVector = value;
                _topArrow.SetDirection(_directionVector[0], _directionVector[1], _directionVector[2]);
                _bottomArrow.SetDirection(_directionVector[0], _directionVector[1], _directionVector[2]);
            }
        }

        /// <summary>
        /// Gets/sets the color of the Axis, Tickmarks, and fonts for each arrow
        /// </summary>
        public Color Color
        {
            get
            {
                return _topActor.GetProperty().GetColor().ToColor();
            }
            set
            {
                _topActor.GetProperty().SetColor(value.R / 255f, value.G / 255f, value.B / 255f);
                _middleActor.GetProperty().SetColor(value.R / 255f, value.G / 255f, value.B / 255f);
                _bottomActor.GetProperty().SetColor(value.R / 255f, value.G / 255f, value.B / 255f);
                _topArrowActor.GetProperty().SetColor(value.R / 255f, value.G / 255f, value.B / 255f);
                _bottomArrowActor.GetProperty().SetColor(value.R / 255f, value.G / 255f, value.B / 255f);
            }
        }

        /// <summary>
        /// Sets the visibility of the arrow
        /// </summary>
        public bool Visible
        {
            get
            {
                return _indicator.GetVisibility().ToBool();
            }
            set
            {
                _indicator.SetVisibility(value.ToInt());
            }
        }

        public int SliceIndex { get; set; }

        public string Label
        {
            get { return _text.Text; }
            set { _text.Text = value; }
        }

        #endregion

        #region Private Methods
        private void BuildIndicator()
        {
            _topMapper.SetInputConnection(_top.GetOutputPort());
            _topActor.SetMapper(_topMapper);

            _middleMapper.SetInputConnection(_middle.GetOutputPort());
            _middleActor.SetMapper(_middleMapper);

            _bottomMapper.SetInputConnection(_bottom.GetOutputPort());
            _bottomActor.SetMapper(_bottomMapper);

            _topArrow.SetResolution(1); // per docs, this will create a single triangle
            _topArrow.CappingOff();
            _topArrowMapper.SetInputConnection(_topArrow.GetOutputPort());
            _topArrowActor.SetMapper(_topArrowMapper);

            _bottomArrow.SetResolution(1); // per docs, this will create a single triangle
            _bottomArrow.CappingOff();
            _bottomArrowMapper.SetInputConnection(_bottomArrow.GetOutputPort());
            _bottomArrowActor.SetMapper(_bottomArrowMapper);

            _indicator = new vtkPropAssembly();
            _indicator.AddPart(_topArrowActor);
            _indicator.AddPart(_topActor);
            _indicator.AddPart(_middleActor);
            _indicator.AddPart(_bottomActor);
            _indicator.AddPart(_bottomArrowActor);
            _indicator.AddPart(_text.Actor);

        }

        #endregion

        public enum TextPositions
        {
            Top, Bottom, Middle
        }

        #region IDispose Pattern Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (_disposed) return;

            // Managed Resources
            if (disposing)
            {
                // Dispose managed resources.
                if (_top != null) _top.Dispose();
                if (_middle != null) _middle.Dispose();
                if (_bottom != null) _bottom.Dispose();
                if (_indicator != null) _indicator.Dispose();
                if (_topArrow != null) _topArrow.Dispose();
                if (_bottomArrow != null) _bottomArrow.Dispose();
                if (_topActor != null) _topActor.Dispose();
                if (_middleActor != null) _middleActor.Dispose();
                if (_bottomActor != null) _bottomActor.Dispose();
                if (_topArrowActor != null) _topArrowActor.Dispose();
                if (_bottomArrowActor != null) _bottomArrowActor.Dispose();
                if (_topMapper != null) _topMapper.Dispose();
                if (_middleMapper != null) _middleMapper.Dispose();
                if (_bottomMapper != null) _bottomMapper.Dispose();
                if (_topArrowMapper != null) _topArrowMapper.Dispose();
                if (_bottomArrowMapper != null) _bottomArrowMapper.Dispose();
            }

            //Set Flag
            _disposed = true;
        }
        #endregion



    }
}
