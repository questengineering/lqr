﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("QuestIntegrity.Graphics")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("QuestIntegrity.LifeQuest.Graphics")]
[assembly: AssemblyCompany("Quest Integrity Group, LLC")]
[assembly: AssemblyCopyright("Copyright © 2008-2012")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("829c479b-cf02-4c4f-9f90-250ac1a92c55")]


//This allows the pre-compiled xml serializer to access internal methods.  Need to build the
//serializer using xgen and the same .snk used for building the .dll.
//  use: "sn.exe -Tp" to get the public key.
[assembly: InternalsVisibleTo("QuestIntegrity.Graphics.XmlSerializers, PublicKey=0024000004800000940000000602000000240000525341310004000001000100cd3e7d4769d4dfccd88081842ba56eaff1797e5427d291b10080e281ea5d9a354cc54e0fc044f7427841ca6179ec5e1d3da594f922277320d1c522804e80158a92ad634b11419ad0b7adadd6ee246c6745cad1370eaa308bb19f34cdfcb4ae831012460b45a3fb2302a12e26f4d1f2ed69324e7f9c0e074bbbcfcc5c875210de")]
