﻿namespace QuestIntegrity.Graphics.Controls
{
    /// <summary>
    ///     Display window geared towards 2D display of data
    /// </summary>
    public sealed class TwoDGraphicsDisplayWindow : GraphicsDisplayWindow
    {
        #region Constructors

        public TwoDGraphicsDisplayWindow()
        {
            if (DesignMode) { return; }

            //Bind the standard handlers
            RegisterInteractorEventHandlers();
        }

        #endregion

       
    }
}