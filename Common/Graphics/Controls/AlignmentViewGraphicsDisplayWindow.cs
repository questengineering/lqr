#region Copyright Quest Reliability, LLC 2008
//
// www.questreliability.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/7/2010 10:11:18 AM
// Created by:   D.Revelle
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace QuestReliability.Graphics
{
    /// <summary>
    /// Display window for showing XYZ mapping of pipe.  
    /// Use Viewing angle for various alignement sheet views
    /// </summary>

    public class AlignmentViewGraphicsDisplayWindow : GraphicsDisplayWindow
    {
        #region Enums
        #endregion

        #region Constants
        #endregion

        #region Private/Protected Variables
        #endregion

        #region Constructors
        #endregion

        #region Public Properties
        #endregion

        #region Public Methods


        #endregion

        #region Private Methods
        #endregion

    }
}
