﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.EventArgs;
using QuestIntegrity.Graphics.EventArgs.PickPositions;
using QuestIntegrity.Graphics.Interactors;
using QuestIntegrity.Graphics.Layout;

namespace QuestIntegrity.Graphics.Controls
{
    /// <summary> Display window geared towards 2D display of image data, basically a strip chart with only one strip. </summary>
    public sealed class ImageGraphicsDisplayWindow : GraphicsDisplayWindow
    {
        #region Constructors
       public ImageGraphicsDisplayWindow()
        {
            InitializeComponent();
            
            //register the appropriate interactor for this display as well as display-type-specific event handlers
            base.Interactor = new HorzTwoDInteractor(this);
            RegisterInteractorEventHandlers();

            //Setup camera
            Camera.SetViewUp(0, 1, 0);
            Camera.ParallelProjectionOn();
            DisplayDataTips = true;
        }

        #endregion

        #region Public Properties

        public SingleStripGridLayout GridLayout { get; set; }
        public CartesianPoint GridScale = new CartesianPoint(1, 1, 1);
        public bool AllowUnconstrainedZoom = true;
        public bool AllowUnconstrainedPan = true;
        public bool AllowHorizontalPan = true;
        public bool AllowStretchZoom = true;
        
        public new HorzTwoDInteractor Interactor { get { return (base.Interactor as HorzTwoDInteractor); } }
        
        /// <summary> Calculated XY-Image based data extents allowing partial visibles. X is slices, Y is sensors, can extend outside of inspection in case there's white space visible </summary>
        public CartesianBounds<double> GetXyExtents()
        { 
            var rawBounds = Renderer.GetCurrentWorldExtents();
            var scaledBounds = new CartesianBounds<double>
            {
                XMin = (rawBounds.XMin) / GridScale.X,
                XMax = (rawBounds.XMax) / GridScale.X,
                YMin = (rawBounds.YMin) / GridScale.Y,
                YMax = (rawBounds.YMax) / GridScale.Y
            };
            return scaledBounds;
        }

        #endregion
         
        #region Public Methods

        /// <summary> scales the actors in the Y direction to fill the current view based off of the start and end indices. Does not automatically render. </summary>
        public void ScaleActorsToFillWindow(double xSpace, double yWidth)
        {
            if (GridLayout.GridStrip == null) return;
            //Make the left and right sides of the camera line up properly.
            int[] size = Renderer.GetSize();
            double yUnits = (size[1]) * xSpace / (size[0]); // Y units available at 1:1 scale
            double yScale = yUnits / yWidth;
            
            //Now the actors need to be scaled along the Y Axis to fill the view vertically.
            foreach (var thing in GridLayout.GridStrip.ImageActors.Values)
            { thing.SetScale(1, yScale, 1); }
            GridScale = new CartesianPoint(1, yScale, 1);
        }

        /// <summary> Sets the current scalar. NOTE: this does not actually create any scalars. It assumes that the point data() scalar exists in the grid. </summary>
        /// <param name="scalarArrayName">Name of the scalar array. Must be an array pre-existing in the actors. Will throw exception if array is not available. </param>
        public void SetCurrentScalar(string scalarArrayName)
        {
            foreach (vtkImageActor actor in GridLayout.GridStrip.ImageActors.Values)
            {
                vtkAbstractArray scalars = actor.GetInput().GetPointData().GetScalars(scalarArrayName);
                if (scalars == null) throw new Exception(string.Format("No scalars named {0} available to be set in ImageGraphicsDisplayWindow.", scalarArrayName));
                actor.GetInput().GetPointData().SetActiveScalars(scalarArrayName);
            }
        }
        
        //Sets the camera to the given image-XY coordinates. They will be scaled from image-xy values to the current grid scale automatically.
        public void SetCameraExtents(double xMin, double xMax, double yMin, double yMax)
        {
            double xSpace = xMax - xMin;
            double ySpace = yMax - yMin;
            int[] renSize = Renderer.GetSize();
            double yUnits = (renSize[1]) * xSpace / (renSize[0]); // Y units available at 1:1 scale

            ScaleActorsToFillWindow(xSpace, ySpace);
            //ScaleActorsToFillWindow(xSpace, 80.857335247595955);
            //Position to the center of the data set
            CartesianPoint center = new CartesianPoint
            {
                X = (xMin + xSpace / 2) * GridScale.X,
                Y = (yMin + ySpace / 2) * GridScale.Y
            };
           
            Camera.SetFocalPoint(center.X, center.Y, 0);
            Camera.SetPosition(center.X, center.Y, 1);
            //Scale is 1/2 viewport to fill completely
            Camera.SetParallelScale(yUnits / 2);
        }

        /// <summary> Resets the camera to the center of the data extents and fills the window with all sensors, and the axial data range. </summary>
        public void ResetCamera()
        {
            var xyImageExtents = GetXyExtents(); //cache a local version so it doesn't recalc a bunch.
            double xSpace = xyImageExtents.XMax - xyImageExtents.XMin;
            double numY = xyImageExtents.YMax - xyImageExtents.YMin;
            int[] renSize = Renderer.GetSize();
            double yUnits = (renSize[1]) * xSpace / (renSize[0]); // Y units available at 1:1 scale
            ScaleActorsToFillWindow(xSpace, numY);
            
            //Position to the center of the data set
            CartesianPoint center = new CartesianPoint
            {
                X = (xyImageExtents.XMin + xSpace / 2) * GridScale.X,
                Y = (xyImageExtents.YMin + (numY / 2d)) * GridScale.Y
            };
            Camera.SetFocalPoint(center.X, center.Y, 0); 
            Camera.SetPosition(center.X, center.Y, 1);

            //Scale is 1/2 viewport to fill completely
            Camera.SetParallelScale(yUnits/ 2); 
        }

        public double[] GetCursorLocation()
        {
            double[] descaledPoint = { Interactor.CrosshairWorldScaledPoint[0] / GridScale.X, Interactor.CrosshairWorldScaledPoint[1] / GridScale.Y, 0 };
            return descaledPoint;
        }

        /// <summary> Sets the cursor to the given raw position. Will automatically apply x/y scaling internally so pass raw slice/sensor data in. </summary>
        public void SetCursor(double x, double y, bool render)
        {
            double[] scaledPoint = { x * GridScale.X, y * GridScale.Y, 0 };
            Interactor.SetCrosshairs(scaledPoint, render);    
        }

        /// <summary> Redraws the cursor at its last given point and re-renders the interactor</summary>
        public void RedrawCursor()
        {
            Interactor.SetCrosshairs(Interactor.CrosshairWorldScaledPoint, true);
        }

        public void SetCursorText(string leftText, string rightText)
        {
            Interactor.SetCrosshairText(leftText, rightText, true);
        }

        public void ShowCursorText(bool show)
        {
            Interactor.ShowCursorText(show);
        }

        /// <summary> Adds an unscaled, or 1:1 image actor to the view. Applies the current scale automatically. </summary>
        public void AddImageActor(vtkProp3D outline)
        {
            var curPos = outline.GetPosition();
            var curScale = outline.GetScale();
            outline.SetPosition(curPos[0] * GridScale.X, curPos[1] * GridScale.Y, -.1);
            outline.SetScale(GridScale.X, GridScale.Y, GridScale.Z);
            Renderer.AddActor(outline);
        }

        #endregion

        #region Private Methods

        protected internal override void RegisterInteractorEventHandlers()
        {
            base.RegisterInteractorEventHandlers();
            Interactor.ImageViewExtentsChanged += OnImageViewExtentsChanged;
            Interactor.LeftMouseUp += OnLeftMouseUp;
            Interactor.LeftMouseDown += OnLeftMouseDown;
            Interactor.LeftMouseDragging += OnLeftMouseMoving;
            Interactor.KeyReleased += OnKeyUp;
            Interactor.KeyPressed += OnKeyDown;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (KeyDown != null) KeyDown(this, e);
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (KeyUp != null) KeyUp(this, e);
        }

        private void OnImageViewExtentsChanged(object sender, ImageViewExtentsChangedEventArgs viewExtentsChangedEventArgs)
        {
            var viewBounds = new CartesianBounds<double>(GetXyExtents());
            if (ViewExtentsChanged != null) ViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(viewBounds));
        }

        private void OnLeftMouseUp(object sender, WorldPositionEventArgs args)
        {
            var sliceAndSensor = UnscaleWorldPoint(args.WorldPoint);
            var pickArgs = new ImagePositionEventArgs(new PointF(sliceAndSensor[0], sliceAndSensor[1]));
            if (LeftMouseUp != null)
                LeftMouseUp(this, pickArgs);
            if (Interactor.RubberBanding && RubberBandSelected != null)
            {
                var rubberBounds = Interactor.GetRawRubberBandBounds();
                int[] point1 = UnscaleWorldPoint(rubberBounds.Point1.ToArray());
                int[] point2 = UnscaleWorldPoint(rubberBounds.Point2.ToArray());
                RubberBandSelected(this, new CartesianBounds<int>(point1[0], point2[0], point1[1], point2[1], 0,0));
            }
        }

        private void OnLeftMouseDown(object sender, WorldPositionEventArgs args)
        {
            if (LeftMouseDown == null) return;
            var sliceAndSensor = UnscaleWorldPoint(args.WorldPoint);
            var pickArgs = new ImagePositionEventArgs(new PointF(sliceAndSensor[0], sliceAndSensor[1]));
            LeftMouseDown(this, pickArgs);
        }

        private void OnLeftMouseMoving(object sender, WorldPositionEventArgs args)
        {
            if (CursorMoving == null || Interactor.RubberBanding) return;
            var sliceAndSensor = UnscaleWorldPoint(args.WorldPoint);
            var pickArgs = new ImagePositionEventArgs(new PointF(sliceAndSensor[0], sliceAndSensor[1]));
            CursorMoving(this, pickArgs);
        }

        /// <summary> Removes X and Y scaling from the given world coordinate. </summary>
        private int[] UnscaleWorldPoint(double[] worldPoint)
        {
            int sliceIdx = (int)Math.Round(worldPoint[0] / GridScale.X);
            int sensorIdx = (int)Math.Round(worldPoint[1] / GridScale.Y);
            return new [] { sliceIdx, sensorIdx };
        }

        /// <summary> Removes X and Y scaling from the given world coordinate. </summary>
        private int[] UnscaleWorldPoint(PointF worldPoint)
        {
            return UnscaleWorldPoint(new double[] { worldPoint.X, worldPoint.Y });
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            Name = "ImageGraphicsDisplayWindow";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        #region Events

        /// <summary> Thrown when the cursor is still moving (button held down and dragging). Will not trigger when rubber band selecting. </summary>
        public event EventHandler<ImagePositionEventArgs> CursorMoving;
        /// <summary> Thrown when the left mouse button button is released. </summary>
        public event EventHandler<ImagePositionEventArgs> LeftMouseUp;
        /// <summary> Thrown when the left mouse button is initially clicked down. </summary>
        public event EventHandler<ImagePositionEventArgs> LeftMouseDown;
        /// <summary> Thrown when a key is released. </summary>
        public new event EventHandler<KeyEventArgs> KeyDown;
        /// <summary> Thrown when a key is released. </summary>
        public new event EventHandler<KeyEventArgs> KeyUp;
        /// <summary> Thrown when a non-zoom(ctrl-key held) rubber band is selected </summary>
        public event EventHandler<CartesianBounds<int>> RubberBandSelected; 

        public event EventHandler<ImageViewExtentsChangedEventArgs> ViewExtentsChanged;
        
        #endregion Events
        
    }
}