﻿using System.Drawing;
using System.Windows.Input;

namespace QuestIntegrity.Graphics.Controls
{
    /// <summary> Helper class that contains a picked point and modifier keys and mouse stuff as needed to be digested at an upper level. </summary>
    public class PickedPoint
    {
        public PickedPoint(int x, int y, MouseButton button = MouseButton.Left, Key modKeys = Key.None)
        {
            Point = new Point(x, y);
            MouseButton = button;
            ModifierKeys = modKeys;
        }

        public Point Point;
        public MouseButton MouseButton;
        public Key ModifierKeys;
    }
}
