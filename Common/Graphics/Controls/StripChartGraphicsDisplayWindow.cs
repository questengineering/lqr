﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.EventArgs.PickPositions;
using QuestIntegrity.Graphics.Interactors;
using QuestIntegrity.Graphics.Layout;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

namespace QuestIntegrity.Graphics.Controls
{
    /// <summary> Display window geared towards a large amount of 2D data </summary>
    public sealed class StripChartGraphicsDisplayWindow : GraphicsDisplayWindow
    {
        /// <summary> Represents a list of indicators currently on screen, and their purpose. Contains typeName and Indicator class.</summary>
        private readonly List<Tuple<string, ViewSectionIndicator>> _indicators = new List<Tuple<string, ViewSectionIndicator>>();
       
        #region Constructors

        public StripChartGraphicsDisplayWindow()
        {
            if (DesignMode) { return; }

            //register the appropriate interactor for this display
            Interactor = new StripChartInteractor(this) { CurrentZoomMode = ZoomMode.Stretch };
            Camera.ParallelProjectionOn();
            DisplayDataTips = true;
            
            RegisterInteractorEventHandlers();
        }

        #endregion

        #region Public Properties

        /// <summary> Holds parameters/instructions specific to laying out the strip chart display. Null by default, and must be set by form controlling graphics window for app-specific logic. </summary>
        public StripGridLayout StripLayout { get; set; }

        /// <summary> The strip chart interactor that controls picking, movement, panning, and zoomin. </summary>
        public new StripChartInteractor Interactor { get; set; }
        #endregion

        #region Public Methods

        /// <summary> Removes all rows from the renderer and clears out the internal lists.</summary>
        public void RemoveStripsFromRenderer()
        {
            StripLayout.GridStrips.ForEach(gs => gs.ImageActors.Values.ToList().ForEach(a => Renderer.RemoveActor(a)));
        }

        /// <summary> Layouts the strips and adds strips to the renderer </summary>
        public void AddStripsToRenderer()
        {
            StripLayout.LayoutStrips();
            StripLayout.GridStrips.ForEach(gs => gs.ImageActors.Values.ToList().ForEach(a =>
            {
                a.SetInterpolate(0); //Always flat, never blended.
                Renderer.AddActor(a);
            }));
        }

        public void ScaleAllActors(CartesianPoint relativeScale)
        {
            //Scale the grid sections
            ScaleGridSections(relativeScale);
            //Scale the indicators
            foreach (ViewSectionIndicator I in _indicators.Select(f => f.Item2).ToList())
            {
                I.Scale = StripLayout.Scale;
                var position = GetWorldPositionOfSliceAndSensor(I.SliceIndex, 0, true);
                I.SetPosition(position, StripLayout.NumReadingsPerSlice - 1);
                I.Redraw();
            }
        }

        private void ScaleGridSections(CartesianPoint scale)
        {
            StripLayout.ScaleActors(scale);
        }

        public void ResetCamera(bool keepCurrentView = false)
        {
            if (StripLayout.GridStrips.Count == 0) return; //If there's nothing on screen, don't bother.

            //Figure out the full world coordinates that would display all data. 
            double unScaledStartWorldX = 0;
            double unScaledStartWorldY = 0;
            double unScaledEndWorldX = StripLayout.NumSlicesPerRow;
            double unScaledEndWorldY = StripLayout.NumRows * StripLayout.NumReadingsPerSlice + (StripLayout.NumRows - 1) *  StripLayout.RowsSpacing - 1;

            //Replace view start/ends if 'Keeping Current View', like when resizing a window.
            var previousScale = StripLayout.Scale;
            if (keepCurrentView)
            {
                var botLeft = Renderer.ScreenToWorldCoordinate(0, 0);
                var topRight = Renderer.ScreenToWorldCoordinate(RenderWindow.GetSize()[0], RenderWindow.GetSize()[1]);
                unScaledStartWorldX = botLeft[0] / previousScale.X;
                unScaledStartWorldY = botLeft[1] / previousScale.Y;
                unScaledEndWorldX = topRight[0] / previousScale.X;
                unScaledEndWorldY = topRight[1] / previousScale.Y;
            }

            StripLayout.ResetScale();
            
            //Determine parallel scale needed for height to fill window. This is the only camera adjustment, the rest is done by scaling actors.
            double yLength = unScaledEndWorldY - unScaledStartWorldY;
            Camera.SetParallelScale(yLength / 2);
            Renderer.ResetCameraClippingRange();

            //Figure out the scale needed to squish the width into the window exactly.
            double xLength = unScaledEndWorldX - unScaledStartWorldX;
            int[] screenSize = RenderWindow.GetSize();
            double widthOnScreen = Math.Ceiling(screenSize[0] / (double)screenSize[1] * yLength); 
            double xScaleToFitStripOnScreen = widthOnScreen / xLength;
            StripLayout.ScaleActors(new CartesianPoint(xScaleToFitStripOnScreen, 1));
            
            //Determine the center position since actors are now scaled
            double centerX = unScaledStartWorldX * xScaleToFitStripOnScreen + xLength * xScaleToFitStripOnScreen / 2d;
            double centerY = unScaledStartWorldY + yLength / 2;
            
            Camera.SetFocalPoint(centerX, centerY, 0.0);
            Camera.SetPosition(centerX, centerY, 1);
            //Renderer.ResetCamera();
            Render();
        }

        #endregion

        #region Private Methods

        protected internal override void RegisterInteractorEventHandlers()
        {
            Interactor.LeftMouseDown += Interactor_LeftMouseDown;
            Interactor.RightMouseDown += Interactor_RightMouseDown;
            Interactor.LeftMouseDragging += Interactor_LeftMouseDragged;
            Interactor.KeyPressed += OnKeyPressed;
        }

        private void Interactor_LeftMouseDown(object sender, WorldPositionEventArgs e)
        {
            var pickedPoint = GetPickedPoint(e);
            GetCurrentModifierKeys(ref pickedPoint);
            pickedPoint.MouseButton = MouseButton.Left;
            OnPointPicked(pickedPoint);
        }

        private void GetCurrentModifierKeys(ref PickedPoint pickedPoint)
        {
            if (ModifierKeys == Keys.Control)
                pickedPoint.ModifierKeys = Key.LeftCtrl;
            if (ModifierKeys == Keys.Shift)
                pickedPoint.ModifierKeys = Key.LeftShift;
            if (ModifierKeys == Keys.Alt)
                pickedPoint.ModifierKeys = Key.LeftAlt;
        }

        private void Interactor_RightMouseDown(object sender, WorldPositionEventArgs e)
        {
            var pickedPoint = GetPickedPoint(e);
            GetCurrentModifierKeys(ref pickedPoint);
            pickedPoint.MouseButton = MouseButton.Right;
            OnPointPicked(pickedPoint);
        }

        private void Interactor_LeftMouseDragged(object sender, WorldPositionEventArgs e)
        {
            OnPointDragged(GetPickedPoint(e));
        }

        public CartesianPoint GetWorldPositionOfSliceAndSensor(int sliceIndex, int sensor, bool scaled)
        {
            CartesianPoint returnValue = new CartesianPoint(-1, -1);
            if (StripLayout.GridStrips.Count == 0 || sliceIndex > StripLayout.EndingIndex || sliceIndex < StripLayout.StartingIndex) return returnValue;

            GridStrip containingStrip = StripLayout.GridStrips.First(s => sliceIndex.Between(s.StartingSliceIdx, s.EndingSliceIdx));
            CartesianPoint scaledStripPos = containingStrip.StartingPosition;
            double scaledX = (int)Math.Round(scaledStripPos.X) + (sliceIndex - containingStrip.StartingSliceIdx) * StripLayout.Scale.X;
            double scaledY = (int)Math.Round(scaledStripPos.Y) + sensor * StripLayout.Scale.Y;
            return scaled ?
                new CartesianPoint(scaledX, scaledY) :
                new CartesianPoint(scaledX / StripLayout.Scale.X, scaledY / StripLayout.Scale.Y);
        }

        private PickedPoint GetPickedPoint(WorldPositionEventArgs e)
        {
            //Check if it's within start bounds.
            PickedPoint deadPoint = new PickedPoint(-1, -1);
            if (StripLayout.GridStrips.Count == 0) return deadPoint;
            int firstRowEndIdx = (StripLayout.GridStrips.First(g => g.RowIndex == 0).EndingSliceIdx);
            var sliceAndSensor = e.WorldPoint;
            if (sliceAndSensor.X < 0 || sliceAndSensor.Y < 0) return deadPoint;
            //Check if it's within end bounds.
            double maxY = StripLayout.Scale.Y * (StripLayout.NumRows * StripLayout.NumReadingsPerSlice + (StripLayout.NumRows - 1) * StripLayout.RowsSpacing) - 1;
            double maxX = StripLayout.Scale.X * firstRowEndIdx; //The first row should always be the max, or equivalent to it, by design.
            if (sliceAndSensor.X > maxX || sliceAndSensor.Y > maxY) return deadPoint;

            //Calculate the globalID by figuring out how many slices it is along the X Axis from the row's beginning, and then add it to the row's start index.
            int stripSliceIdx = (int)Math.Round(sliceAndSensor.X / StripLayout.Scale.X);
            double unscaledYPickedPoint = sliceAndSensor.Y / StripLayout.Scale.Y;
            int yPointsPerRow = StripLayout.NumReadingsPerSlice + StripLayout.RowsSpacing;
            int sensorNum;
            int rowNum = Math.DivRem((int)Math.Round(unscaledYPickedPoint), yPointsPerRow, out sensorNum);
            if (sensorNum > StripLayout.NumReadingsPerSlice - 1) return deadPoint; // this is in the white space.

            int sliceNum = StripLayout.GridStrips.OrderBy(s => s.StartingSliceIdx).ToList()[rowNum].StartingSliceIdx + stripSliceIdx;

            return new PickedPoint(sliceNum, sensorNum);
        }

        public void SetDataExtents(int startSlice, int endSlice, int? slicesPerStrip)
        {
            StripLayout.StartingIndex = startSlice;
            StripLayout.EndingIndex = endSlice;
            if (slicesPerStrip != null) StripLayout.NumSlicesPerRow = slicesPerStrip.ToInt32();
        }

        /// <summary> Shows the data tip with the specified text at the crosshair location </summary>
        public override void ShowDataTip(string toolTipText)
        {
            DataTipText = toolTipText;
            ShowDataTip();
        }

        /// <summary>
        ///     Shows the data tip at the crosshair location using the existing text.
        /// </summary>
        public override void ShowDataTip()
        {
            if (Interactor == null) return;

            Point pt = new Point
            {
                X = Interactor.CrosshairScreenPoint[0],
                Y = Height - Interactor.CrosshairScreenPoint[1]
            };

            //If chosen not to display tips, or the point is out of the render window, don't show it.
            if (!DisplayDataTips || (pt.X < 0 || pt.X > Width || pt.Y < 0 || pt.Y > Height))
                DataTip.Hide(this);
            else
                DataTip.Show(DataTipText, this, pt.X + 5, pt.Y + 5);
        }

        #region Public Events

        public event EventHandler<PickedPoint> PointPicked;

        private void OnPointPicked(PickedPoint e)
        {
            PointPicked?.Invoke(this, e);
        }

        public event EventHandler<PickedPoint> PointDragged;

        private void OnPointDragged(PickedPoint e)
        {
            PointDragged?.Invoke(this, e);
        }

        #endregion

        #endregion

        protected override void OnKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                OnPointPicked(new PickedPoint(-1, -1));
            base.OnKeyPressed(sender, e);
        }

        /// <summary> Places a new indicator at the designated slice and renders. Removes the existing indicators of the same type. </summary>
        public ViewSectionIndicator DrawIndicator(int sliceIndex, string indType, Color indColor, string label, ViewSectionIndicator.TextPositions textPosition, bool render)
        {
            RemoveIndicator(indType);
            bool showArrows = true;
            int xDir = 0;
            if (indType.ToLower().Contains("start")) xDir = 1;
            if (indType.ToLower().Contains("end")) xDir = -1;
            if (xDir == 0) showArrows = false;
            ViewSectionIndicator newIndicator = new ViewSectionIndicator
            {
                Color = indColor,
                Direction = new[] { xDir, 0, 0 },
                SliceIndex = sliceIndex,
                Scale = StripLayout.Scale,
                ShowArrows =  showArrows,
                Label = label,
                TextPosition = textPosition
            };
            var position = GetWorldPositionOfSliceAndSensor(sliceIndex, 0, true);
            newIndicator.SetPosition(position, StripLayout.NumReadingsPerSlice - 1);
            newIndicator.AddToRenderer(Renderer);
            newIndicator.Redraw();
            _indicators.Add(new Tuple<string, ViewSectionIndicator>(indType, newIndicator));
            if (render)
            {
                Renderer.ResetCameraClippingRange();
                Render();
            }
            return newIndicator;
        }

        /// <summary> Redraws the indicators that exist on the plot in the correct location. Does not render automatically. </summary>
        public void ReDrawIndicators(bool render)
        {
            foreach (var indPair in _indicators.ToList()) // ToList() is used to create a copy, as the original will be modified during iteration
            {
                //Cache these values. Accessing the indicator vtk object's unsafe properties after it is removed will cause a crash.
                ViewSectionIndicator ind = indPair.Item2;
                Color indColor = ind.Color;
                int indSlice = ind.SliceIndex;
                RemoveIndicator(indPair.Item1);
                DrawIndicator(indSlice, indPair.Item1, indColor, ind.Label, ind.TextPosition, false);
            }
            if (render) Render();
        }

        /// <summary> Creates a task that is waiting for the user to pick a valid globalID. Once finished, the task returns the valid global ID. 
        /// More than one can be active at once, but I would not recommend it. </summary>
        public Task<PickedPoint> PickPointSelectionTask()
        {
            return Task.Factory.StartNew(() =>
            {
                bool pointPicked = false;
                PickedPoint finalPoint = new PickedPoint(-1, -1);
                EventHandler<PickedPoint> pickEventHandler = ((sender, args) =>
                {
                    pointPicked = true;
                    finalPoint = args;
                });
                
                PointPicked += pickEventHandler;
                
                //Hold in this loop while picking occurs, but exit when the flag shows it is done.
                while (!pointPicked) { Thread.Sleep(10); }
                PointPicked -= pickEventHandler;
                return finalPoint;
            });
        }

        /// <summary> Removes an indicator, if it exists.  </summary>
        public void RemoveIndicator(string indType)
        {
            if (_indicators.All(f => f.Item1 != indType)) return;

            var indicators = _indicators.Where(f => f.Item1 == indType).ToList();
            indicators.ForEach(I =>
            {
                _indicators.Remove(I);
                I.Item2.RemoveFromRenderer(Renderer);
                I.Item2.Dispose();
            });
        }

        /// <summary> Removes all indicators in the view.  </summary>
        public void RemoveAllIndicators()
        {
            var indicators = _indicators.Distinct().ToList();
            indicators.ForEach(I =>
            {
                _indicators.Remove(I);
                I.Item2.RemoveFromRenderer(Renderer);
                I.Item2.Dispose();
            });
        }

        public void CancelPicking()
        {
            OnPointPicked(new PickedPoint(-1, -1));
        }
    }
}