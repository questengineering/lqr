﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Graphics.Interactors;

namespace QuestIntegrity.Graphics.Controls
{
    public class GraphicsDisplayWindow : UserControl
    {
        #region Private and Protected Fields

        /// <summary> Required designer variable. </summary>
        private IContainer components;

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected ToolTip DataTip;
        protected string DataTipText = string.Empty;
        protected bool DisplayDataTips;
        static bool _designMode { get { return (LicenseManager.UsageMode == LicenseUsageMode.Designtime); } }
        
        #endregion

        #region Constructors and Initialization

        protected GraphicsDisplayWindow()
        {
            InitializeComponent();
            if (!_designMode) InitializeRenderer();
        }

        private void InitializeComponent()
        {
            components = new Container();
            DataTip = new ToolTip(components)
            {
                AutomaticDelay = 0,
                AutoPopDelay = 0,
                InitialDelay = 0,
                ReshowDelay = 0,
                ShowAlways = true,
                UseAnimation = false,
                UseFading = false
            };
            SuspendLayout();

            // GraphicsDisplayWindow
            AutoScaleMode = AutoScaleMode.None;
            AutoSize = true;
            Controls.Add(RenderWindowControl);
            Cursor = Cursors.Hand;
            Name = "GraphicsDisplayWindow";
            Size = new Size(490, 330);
            ResumeLayout(false);
            PerformLayout();
        }

        private void InitializeRenderer()
        {
            RenderWindowControl = new RenderWindowControl{Dock = DockStyle.Fill};
            RenderWindowControl.CreateControl(); //NOTE: need to call this to fire OnHandleCreated event which will cause the control to create the rendering window, etc.
            if (Renderer != null)
            {
                Renderer.InteractiveOff();
                Renderer.SetBackground(Color.White);
            }
        }

        #endregion

        #region Public Properties

        public RenderWindowControl RenderWindowControl { get; private set; }
        public vtkRenderWindow RenderWindow { get { return RenderWindowControl.RenderWindow; } }
        public virtual vtkRenderer Renderer { get { return RenderWindow.GetRenderers().GetFirstRenderer(); } }
        public virtual vtkCamera Camera { get { return Renderer.GetActiveCamera(); } }
        public virtual BaseInteractor Interactor { get; protected set; }

        /// <summary>  Gets or sets the current interaction mode. </summary>
        public ZoomMode CurrentZoomMode
        {
            get { return Interactor.CurrentZoomMode; }
            set { Interactor.CurrentZoomMode = value; }
        }

        
        #endregion

        #region Public Events

        /// <summary> Registers the standard event handlers. Intended to be called after the interactor has been assigned by the base class. </summary>
        protected internal virtual void RegisterInteractorEventHandlers()
        {
            Interactor.DeregisterAllUserEvents();
            Interactor.KeyPressed += OnKeyPressed;
            Interactor.KeyReleased += OnKeyReleased;
        }

        protected virtual void OnKeyReleased(object sender, KeyEventArgs e)
        {
            OnKeyUp(e);
        }

        protected virtual void OnKeyPressed(object sender, KeyEventArgs e)
        {
            OnKeyDown(e);
        }

        #endregion

        #region Public Methods

        /// <summary> Causes the Renderwindow to render everything. </summary>
        public void Render()
        {
            if (Renderer != null)
                Renderer.ResetCameraClippingRange();
            RenderWindow.Render();
        }

        /// <summary> Shows the data tip with the specified text at the crosshair location </summary>
        public virtual void ShowDataTip(string toolTipText)
        {
            DataTipText = toolTipText;
            ShowDataTip();
        }

        /// <summary>   Shows the data tip at the crosshair location using the existing text. </summary>
        public virtual void ShowDataTip()
        {
            HorzTwoDInteractor twoDInteractor = Interactor as HorzTwoDInteractor;
            if (twoDInteractor == null) return;

            Point pt = new Point
            {
                X = twoDInteractor.CrosshairScreenPoint[0],
                Y = Height - twoDInteractor.CrosshairScreenPoint[1]
            };

            //If chosen not to display tips, or the point is out of the render window, don't show it.
            if (!DisplayDataTips || (pt.X < 0 || pt.X > Width || pt.Y < 0 || pt.Y > Height))
                DataTip.Hide(this);
            else
                DataTip.Show(DataTipText, this, pt.X + 5, pt.Y + 5);
        }
        
        public void HideDataTip()
        {
            DataTip.Hide(this);
        }

        #endregion

        #region IDisposable Implementation

        protected override void OnHandleCreated(System.EventArgs e)
        {
            if (!Controls.Contains(RenderWindowControl) && !Disposing && !IsDisposed)
                Controls.Add(RenderWindowControl);
        }

        protected override void OnHandleDestroyed(System.EventArgs e)
        {
            if (Controls.Contains(RenderWindowControl) && !Disposing && !IsDisposed)
                Controls.Remove(RenderWindowControl);
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                //Dispose of VTK Objects, Protect against the render having been already cleaned up by the control
                if (RenderWindowControl != null &&
                    RenderWindowControl.RenderWindow != null &&
                    RenderWindowControl.RenderWindow.GetRenderers().GetNumberOfItems() > 0 && 
                    Renderer != null)
                {
                    Renderer.RemoveAllViewProps();
                }

                if (disposing && (components != null))
                    components.Dispose();
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion
    }
}