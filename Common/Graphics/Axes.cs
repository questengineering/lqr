#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/26/2008 9:17:37 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.Drawing;
using System.Runtime.InteropServices;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.Graphics
{
    /// <summary>
    ///     Is used to display view axes on VTK windows
    /// </summary>
    public class Axes : IDisposable
    {
        public enum AxisDisplayType
        {
            /// <summary>
            ///     Uses Two-D style axis that draws in image plane and for use in 2D/flat grids
            /// </summary>
            TwoD = 0,

            /// <summary>
            ///     Uses Three-D style axis that draws and rotates in world space along X axis
            /// </summary>
            ThreeD = 1
        }

        #region Private/Protected Variables

        private bool _disposed;
        private bool _showLabels = true;
        private AxisDisplayType _axisType;
        private Color _axisColor;
        private TextProperty _axisText;
        private Length.LengthScale _baseLengthScale = Length.LengthScale.Inches;
        private double _xMinDisplay;
        private double _xMaxDisplay;

        // Axes text property.
        private readonly vtkTextProperty _tprop;

        // Axes actors and assemblies.
        private readonly vtkAxisActor _axes3D;
        private readonly vtkCubeAxesActor2D _cubeAxes2D;

        #endregion

        #region Constructors

        public Axes()
        {
            // Create the bounding box axes.
            _axes3D = new vtkAxisActor();
            _cubeAxes2D = new vtkCubeAxesActor2D();
            _cubeAxes2D.GetAxisLabelTextProperty().SetOpacity(1);

            // Create a text property for the axes.
            _tprop = new vtkTextProperty();

            Color = Color.Black;
            TextProperties = new TextProperty
                             {
                                 FontSize = 10
                             };

            UseCalculatedXRange = true;
            NumberMajorSegments = 10;
            AxisType = AxisDisplayType.ThreeD;
            BaseLengthScale = Length.LengthScale.Inches;
        }

        public Axes(double[] bounds)
            : this()
        {
            SetAxesBounds(bounds);
        }

        public Axes(double xMin,
                    double xMax,
                    double yMin,
                    double yMax,
                    double zMin,
                    double zMax,
                    vtkRenderer ren)
            : this()
        {
            SetAxesBounds(xMin, xMax, yMin, yMax, zMin, zMax);
        }

        ~Axes()
        {
            Dispose(false);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether the Axis should use a calculated X-Axis scale.
        ///     If True, it will use the Display units and ViewExtents to calculate the scale.
        ///     If False, it will use the range specified by "SetXAxisRange()" method.
        /// </summary>
        /// <value>
        ///     <c>true</c> if [use calculated X range]; otherwise, <c>false</c>.
        /// </value>
        public bool UseCalculatedXRange { get; set; }

        public Color Color
        {
            get
            {
                return _axisColor;
            }
            set
            {
                if (_axisColor != value)
                {
                    _axisColor = value;
                }
            }
        }

        public TextProperty TextProperties
        {
            get
            {
                return _axisText;
            }
            set
            {
                if (_axisText != value)
                {
                    _axisText = value;
                }
            }
        }

        /// <summary>
        ///     Indicates what type of axis to use
        /// </summary>
        public AxisDisplayType AxisType
        {
            get
            {
                return _axisType;
            }
            set
            {
                _axisType = value;
            }
        }

        /// <summary>
        ///     Number of major segments to use
        /// </summary>
        public int NumberMajorSegments { get; set; }

        /// <summary>
        ///     Length scale of the original data
        /// </summary>
        public Length.LengthScale BaseLengthScale
        {
            get
            {
                return _baseLengthScale;
            }
            set
            {
                _baseLengthScale = value;
            }
        }

        /// <summary>
        ///     Toggles visibility of Labels
        /// </summary>
        public bool ShowLabels
        {
            get
            {
                return _showLabels;
            }
            set
            {
                _showLabels = value;
                _axes3D.SetLabelVisibility(value.ToInt());
                _cubeAxes2D.GetAxisLabelTextProperty().SetOpacity(value.ToInt());
            }
        }

        public bool Visible
        {
            get
            {
                if (AxisType == AxisDisplayType.ThreeD)
                {
                    return _axes3D.GetVisibility().ToBool();
                }
                return _cubeAxes2D.GetVisibility().ToBool();
            }
            set
            {
                if (AxisType == AxisDisplayType.ThreeD)
                {
                    _cubeAxes2D.VisibilityOff();
                    _axes3D.SetVisibility(value.ToInt());
                }
                else
                {
                    _axes3D.VisibilityOff();
                    _cubeAxes2D.SetVisibility(value.ToInt());
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Sets the X-axis bounds based on Index units.
        /// </summary>
        /// <param name="xMin">The x min in index units</param>
        /// <param name="xMax">The x max in index units</param>
        public void Set2DAxesBounds(int xMin,
                                    int xMax)
        {
            double[] bounds = _cubeAxes2D.GetBounds();
            _cubeAxes2D.SetBounds(xMin, xMax, bounds[2], bounds[3], bounds[4], bounds[5]);
            FormatAxes();
        }

        /// <summary>
        ///     Set the Bounds of the axis.  With either an array of XYZ min and max, individual values or a vtkDataSet
        /// </summary>
        /// <param name="setToBound"></param>
        public void SetAxesBounds(vtkDataSet setToBound)
        {
            double[] bounds = setToBound.GetBounds();
            SetAxesBounds(bounds);
            FormatAxes();
        }

        public void SetAxesBounds(double[] bounds)
        {
            SetAxesBounds(bounds[0], bounds[1], bounds[2], bounds[2], bounds[5], bounds[5]);
        }

        public void SetAxesBounds(double xMin,
                                  double xMax,
                                  double yMin,
                                  double yMax,
                                  double zMin,
                                  double zMax)
        {
            //ick...for some reason they didn't expose a non-pointer version of setting bounds
            int size = Marshal.SizeOf(typeof(double)) * 6;
            IntPtr ptrBounds = Marshal.AllocHGlobal(size);
            try
            {
                double[] bounds = new[]
                                  {
                                          xMin, xMax, yMin, yMax, zMin, zMax
                                  };
                Marshal.Copy(bounds, 0, ptrBounds, bounds.Length);
                _axes3D.SetBounds(ptrBounds);
                _axes3D.SetPoint1(xMin, yMin, zMin);
                _axes3D.SetPoint2(xMax, yMin, zMin);

                _cubeAxes2D.SetInput(null); //SetInput to null so that SetBounds command takes effect
                _cubeAxes2D.SetBounds(xMin, xMax, yMin, yMax, zMin, zMax);
                FormatAxes();
            }
            finally
            {
                Marshal.FreeHGlobal(ptrBounds);
            }
        }

        public void SetAxisLabels(double minLabelNumber,
                                  double maxLabelNumber,
                                  double textScale = 1)
        {
            double increment = (maxLabelNumber - minLabelNumber) / (NumberMajorSegments);
            double label = minLabelNumber;
            string format = DisplayUnits.Instance.AxialDistanceUnits.FormatString;
            vtkStringArray strings = new vtkStringArray();

            for (int i = 0; i < NumberMajorSegments + 1; i++)
            {
                strings.InsertNextValue(label.ToString(format));
                label += increment;
            }

            _axes3D.SetLabels(strings);
            _axes3D.SetLabelScale(textScale);
            _axes3D.SetMajorTickSize(textScale * 2); //Needs the 2X multiplier to be more visible.
        }

        /// <summary>
        ///     Sets the minimum and maximum positions to be displayed.
        /// </summary>
        /// <param name="xMinDisplay">The minimum axial position to be displayed.</param>
        /// <param name="xMaxDisplay">The maximum axial position to be displayed.</param>
        public void SetXDisplayRange(double xMinDisplay,
                                     double xMaxDisplay)
        {
            _xMinDisplay = xMinDisplay;
            _xMaxDisplay = xMaxDisplay;
        }

        public void Draw(vtkRenderer ren)
        {
            ren.RemoveActor(_axes3D);
            ren.RemoveActor(_cubeAxes2D);

            FormatAxes();

            _axes3D.SetCamera(ren.GetActiveCamera());
            _axes3D.BuildAxis(ren, true);
            ren.AddActor(_axes3D);

            _cubeAxes2D.SetCamera(ren.GetActiveCamera());
            ren.AddActor(_cubeAxes2D);
        }

        public void Clear(vtkRenderer ren)
        {
            ren.RemoveActor(_axes3D);
            ren.RemoveActor(_cubeAxes2D);
        }

        #endregion

        #region Private Methods

        private void FormatAxes()
        {
            double xMin = 0;
            double xMax = 0;
            double yMin = 0;
            double yMax = 0;
            double zMin = 0;
            double zMax = 0;

            _cubeAxes2D.GetBounds(ref xMin, ref xMax, ref yMin, ref yMax, ref zMin, ref zMax);

            _tprop.SetJustificationToCentered();
            _tprop.SetColor(TextProperties.Color);
            _tprop.SetFontSize(TextProperties.FontSize);
            _tprop.SetBold(TextProperties.Bold.ToInt());
            _tprop.SetItalic(TextProperties.Italics.ToInt());
            _tprop.SetShadow(TextProperties.Shadow.ToInt());
            _tprop.SetOpacity(TextProperties.Opacity);

            if (AxisType == AxisDisplayType.ThreeD)
            {
                //Configure Axes
                _axes3D.SetTickLocationToInside();
                _axes3D.GetProperty().SetColor(Color);
                _axes3D.SetTitle("");
                _axes3D.SetUseBounds(false);
                _axes3D.SetAxisTypeToX();
                _axes3D.SetAxisPositionToMinMin();
                _axes3D.SetDeltaMajor((xMax - xMin) / (NumberMajorSegments));
                _axes3D.SetDeltaMinor((xMax - xMin) / ((NumberMajorSegments) * 10));
                _axes3D.SetMajorStart(xMin);
                _axes3D.SetMinorStart(xMin);
                _axes3D.LabelVisibilityOn();
                _axes3D.TickVisibilityOn();
                _axes3D.MinorTicksVisibleOn();
                _axes3D.AxisVisibilityOn();
                //Set default values for the labels for now. These will be replaced later, but need to be put in as placeholders.
                SetAxisLabels(0, 1);
                _axes3D.SetLabelScale(.5);
                _axes3D.SetScale(1);
                _cubeAxes2D.VisibilityOff();
                _axes3D.SetVisibility(Visible.ToInt());
            }
            else
            {
                //Cube Axes 2D setup
                double xMinDisplay, xMaxDisplay;

                //display in converted units.
                double yMinDisplay = Length.Convert(yMin, DisplayUnits.Instance.MeasurementUnits.Scale, BaseLengthScale);
                double yMaxDisplay = Length.Convert(yMax, DisplayUnits.Instance.MeasurementUnits.Scale, BaseLengthScale);

                //override the range used for displaying the data
                if (UseCalculatedXRange)
                {
                    xMinDisplay = Length.Convert(xMin, DisplayUnits.Instance.AxialDistanceUnits.Scale, BaseLengthScale);
                    xMaxDisplay = Length.Convert(xMax, DisplayUnits.Instance.AxialDistanceUnits.Scale, BaseLengthScale);
                }
                else
                {
                    xMinDisplay = _xMinDisplay;
                    xMaxDisplay = _xMaxDisplay;
                }

                _cubeAxes2D.SetRanges(xMinDisplay, xMaxDisplay, yMinDisplay, yMaxDisplay, zMin, zMax);
                _cubeAxes2D.UseRangesOn();
                _cubeAxes2D.SetAxisTitleTextProperty(_tprop);
                _cubeAxes2D.SetAxisLabelTextProperty(_tprop);
                _cubeAxes2D.SetNumberOfLabels(NumberMajorSegments + 1);
                _cubeAxes2D.SetXLabel("");
                _cubeAxes2D.SetLabelFormat(GetLabelFormat(xMinDisplay, xMaxDisplay));
                _cubeAxes2D.GetXAxisActor2D().GetLabelTextProperty().SetFontSize(12);
                _cubeAxes2D.GetXAxisActor2D().GetTitleTextProperty().SetFontSize(12);
                _cubeAxes2D.GetProperty().SetColor(Color);
                _cubeAxes2D.SetCornerOffset(0);
                _cubeAxes2D.XAxisVisibilityOn();
                _cubeAxes2D.YAxisVisibilityOff();
                _cubeAxes2D.ZAxisVisibilityOff();
                _cubeAxes2D.ScalingOff();
                _axes3D.VisibilityOff();
                _cubeAxes2D.SetVisibility(Visible.ToInt());
            }
        }

        /// <summary>
        ///     Returns a label format customized for the data range given
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        private string GetLabelFormat(double minValue,
                                      double maxValue)
        {
            double deltaX = maxValue - minValue;
            string labelFormat;

            if (deltaX > 1000)
            {
                labelFormat = maxValue > 1e6 ? "%8.4g" : "%10.0f";
            }
            else if (deltaX > 10)
            {
                labelFormat = "%10.1f";
            }
            else if (deltaX > 1)
            {
                labelFormat = "%10.2f";
            }
            else if (deltaX > .1)
            {
                labelFormat = "%10.4f";
            }
            else
            {
                labelFormat = "%10.6f";
            }
            return labelFormat;
        }

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // Managed Resources
                if (disposing)
                {
                    // Dispose managed resources.
                    _axes3D.Dispose();
                    _cubeAxes2D.Dispose();
                    _tprop.Dispose();
                }

                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}