#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/26/2008 9:17:37 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.Collections.Generic;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.Graphics
{
    /// <summary>
    ///     Renders circular axis around the 3D pipe
    /// </summary>
    public class ClockAxes : IDisposable
    {
        #region Constants

        private const char Degree = '\x00B0'; //degree symbol (ASCII 176) or (hex B0)

        #endregion

        #region Private/Protected Variables

        private bool _disposed;
        private double _innerRadius;
        private double _outterRadius;
        private int _axialIndex;

        // Axes actors and assemblies.
        private vtkDiskSource _clock;
        private vtkPolyDataMapper _clockMapper;
        private vtkActor _clockActor;

        private vtkDiskSource _clockPlane;
        private vtkPolyDataMapper _clockPlaneMapper;
        private vtkActor _clockPlaneActor;

        private readonly List<vtkVectorText> _labelTexts = new List<vtkVectorText>();
        private readonly List<vtkPolyDataMapper> _labelMappers = new List<vtkPolyDataMapper>();
        private readonly List<vtkFollower> _labelFollowers = new List<vtkFollower>();

        private readonly List<vtkLineSource> _tickLines = new List<vtkLineSource>();
        private readonly List<vtkPolyDataMapper> _tickMappers = new List<vtkPolyDataMapper>();
        private readonly List<vtkActor> _tickActors = new List<vtkActor>();

        private vtkAssembly _clockAssembly;
        private bool _visible;

        private readonly vtkRenderer _ren;

        #endregion

        #region Constructors

        public ClockAxes(vtkRenderer ren)
        {
            _ren = ren;
            _clockAssembly = new vtkAssembly();

            DisplayUnits = DisplayUnits.Instance;
            DisplayUnits.CircumferentialUnits = CircumferentialLocation.CircumferentialDisplayUnit.Degrees;
            ShowMajorTicks = true;
            NumMajorTicks = 8;
            ShowMinorTicks = false;
            NumMinorTicks = 24;
            _axialIndex = 0;
            _visible = true;
        }

        #endregion

        #region Public Properties

        public double InnerRadius
        {
            get
            {
                return _innerRadius;
            }
        }

        public double OutterRadius
        {
            get
            {
                return _outterRadius;
            }
        }

        public bool ShowMajorTicks { get; set; }

        /// <summary>
        ///     Number of Major Ticks to display.  This will also set the locations of the labels
        /// </summary>
        public int NumMajorTicks { get; set; }

        public bool ShowMinorTicks { get; set; }

        /// <summary>
        ///     This should be a multiple of NumMajorTicks.  At each MajorTick,
        ///     the minor tick will be skipped
        /// </summary>
        public int NumMinorTicks { get; set; }

        /// <summary>
        ///     Unit systems and scales to use for Display.
        /// </summary>
        public DisplayUnits DisplayUnits { get; set; }

        /// <summary>
        ///     Toggle visibility of the Clock Axes
        /// </summary>
        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;
                SetVisibility(value);
            }
        }

        /// <summary>
        ///     Toggle visibility of the labels
        /// </summary>
        public bool LabelsVisible
        {
            get
            {
                return _labelFollowers[0].GetVisibility().ToBool();
            }
            set
            {
                foreach (vtkFollower label in _labelFollowers)
                {
                    label.SetVisibility(value.ToInt());
                }
            }
        }

        public double LabelScale { get; set; }

        #endregion

        #region Public Methods

        public void SetAxialPosition(int xIndex)
        {
            _axialIndex = xIndex;

            _clockActor.SetPosition(xIndex, 0, 0);
            _clockPlaneActor.SetPosition(xIndex, 0, 0);

            foreach (vtkLineSource tick in _tickLines)
            {
                double[] pt1 = tick.GetPoint1();
                double[] pt2 = tick.GetPoint2();
                pt1[0] = xIndex;
                pt2[0] = xIndex;
                tick.SetPoint1(pt1[0], pt1[1], pt1[2]);
                tick.SetPoint2(pt2[0], pt2[1], pt2[2]);
            }

            foreach (vtkFollower label in _labelFollowers)
            {
                double[] p1 = label.GetPosition();
                p1[0] = xIndex;
                label.SetPosition(p1[0], p1[1], p1[2]);
            }
        }

        public void SetAxesBounds(double radius,
                                  int xIndexCenter,
                                  double yCenter,
                                  double zCenter)
        {
            _innerRadius = 1.1 * radius;
            _outterRadius = 1.05 * _innerRadius;

            _axialIndex = xIndexCenter;
        }

        public void SetAxesBounds(double[] bounds,
                                  double[] center)
        {
            double radius = Math.Max(Math.Abs(bounds[1] - bounds[0]), Math.Abs(bounds[3] - bounds[2])) / 2.0;
            SetAxesBounds(radius, Math.Floor(center[0]).ToInt32(), center[1], center[2]);
        }

        public void SetAxesBounds(double[] bounds)
        {
            double xCenter = Math.Abs(bounds[1] - bounds[0]) / 2.0 + bounds[0];
            double yCenter = Math.Abs(bounds[3] - bounds[2]) / 2.0 + bounds[2];
            double zCenter = Math.Abs(bounds[5] - bounds[4]) / 2.0 + bounds[4];

            double radius = Math.Max(Math.Abs(bounds[3] - bounds[2]), Math.Abs(bounds[5] - bounds[4])) / 2.0;

            SetAxesBounds(radius, Math.Floor(xCenter).ToInt32(), yCenter, zCenter);
        }

        public void Draw()
        {
            _ren.RemoveActor(_clockAssembly);

            foreach (vtkFollower fol in _labelFollowers)
            {
                _ren.RemoveActor(fol);
            }

            _clockAssembly = new vtkAssembly();
            BuildDial();
            BuildTicks();
            BuildLabels();

            for (int i = 0; i < NumMajorTicks; i++)
            {
                _ren.AddActor(_labelFollowers[i]);
                _labelFollowers[i].SetVisibility(Visible.ToInt());
            }

            _clockAssembly.SetVisibility(Visible.ToInt());
            _ren.AddActor(_clockAssembly);
        }

        #endregion

        #region Private Methods

        private void SetVisibility(bool visible)
        {
            int isVisible = visible.ToInt();

            _clockActor.SetVisibility(isVisible);
            _clockPlaneActor.SetVisibility(isVisible);

            foreach (vtkFollower label in _labelFollowers)
            {
                label.SetVisibility(isVisible);
            }

            foreach (vtkActor tick in _tickActors)
            {
                tick.SetVisibility(isVisible);
            }
        }

        private void BuildDial()
        {
            //outer ring
            _clock = new vtkDiskSource();
            _clockMapper = vtkPolyDataMapper.New();
            _clockActor = new vtkActor();

            _clock.SetInnerRadius(_innerRadius);
            _clock.SetOuterRadius(_outterRadius);
            _clockActor.SetPosition(_axialIndex, 0, 0);
            _clockActor.SetOrientation(0, 90, 0);

            _clock.SetCircumferentialResolution(360);
            _clock.SetRadialResolution(100);

            _clockMapper.SetInputConnection(_clock.GetOutputPort());
            _clockActor.SetMapper(_clockMapper);
            _clockActor.GetProperty().SetColor(0, 0, 0);

            _clockAssembly.AddPart(_clockActor);

            //inner plane
            _clockPlane = new vtkDiskSource();
            _clockPlaneMapper = vtkPolyDataMapper.New();
            _clockPlaneActor = new vtkActor();

            _clockPlane.SetOuterRadius(_innerRadius);
            _clockPlane.SetInnerRadius(0);
            _clockPlaneActor.SetPosition(_axialIndex, 0, 0);
            _clockPlaneActor.SetOrientation(0, 90, 0);
            _clockPlane.SetCircumferentialResolution(360);
            _clockPlane.SetRadialResolution(100);
            _clockPlaneMapper.SetInputConnection(_clockPlane.GetOutputPort());
            _clockPlaneActor.SetMapper(_clockPlaneMapper);
            _clockPlaneActor.GetProperty().SetColor(0.75, 0.75, 0.75);
            _clockPlaneActor.GetProperty().SetOpacity(0.5);

            _clockAssembly.AddPart(_clockPlaneActor);
        }

        private void BuildTicks()
        {
            _tickLines.Clear();
            _tickMappers.Clear();
            _tickActors.Clear();

            //Validate ticks and set count
            if (NumMinorTicks < NumMajorTicks)
            {
                NumMinorTicks = NumMajorTicks;
            }

            if (NumMinorTicks % NumMajorTicks != 0)
            {
                int multiplier = (int)Math.Truncate(NumMinorTicks / (float)NumMajorTicks);
                NumMinorTicks = multiplier * NumMajorTicks;
            }

            int majorTickFrequecy = NumMinorTicks / NumMajorTicks;
            for (int i = 0; i < NumMinorTicks; i++)
            {
                double theta = 360d * (i / (double)NumMinorTicks);

                double tickLength;
                if (i % majorTickFrequecy == 0)
                {
                    tickLength = 2.5 * (_outterRadius - _innerRadius);
                }
                else
                {
                    tickLength = _outterRadius - _innerRadius;
                }
                _tickLines.Insert(i, new vtkLineSource());
                _tickLines[i].SetPoint1(_axialIndex, (_innerRadius - tickLength) * Math.Cos(theta * Math.PI / 180), (_innerRadius - tickLength) * Math.Sin(theta * Math.PI / 180));
                _tickLines[i].SetPoint2(_axialIndex, _innerRadius * Math.Cos(theta * Math.PI / 180), _innerRadius * Math.Sin(theta * Math.PI / 180));

                _tickMappers.Insert(i, vtkPolyDataMapper.New());
                _tickActors.Insert(i, new vtkActor());
                _tickActors[i].GetProperty().SetColor(0, 0, 0);

                _tickMappers[i].SetInput(_tickLines[i].GetOutput());
                _tickActors[i].SetMapper(_tickMappers[i]);

                _clockAssembly.AddPart(_tickActors[i]);
            }
        }

        private void BuildLabels()
        {
            _labelTexts.Clear();
            _labelMappers.Clear();
            _labelFollowers.Clear();

            for (int i = 0; i < NumMajorTicks; i++)
            {
                int degrees = (int)(360d * i / NumMajorTicks);
                vtkVectorText label = new vtkVectorText();

                string labelText;
                if (DisplayUnits.CircumferentialUnits == CircumferentialLocation.CircumferentialDisplayUnit.Degrees)
                {
                    labelText = string.Format("{0}{1}", degrees, Degree);
                }
                else
                {
                    bool useZero = DisplayUnits.CircumferentialUnits == CircumferentialLocation.CircumferentialDisplayUnit.OClockZero;
                    labelText = CircumferentialLocation.DegreesToOClock(degrees, useZero);
                }

                label.SetText(labelText);
                _labelTexts.Insert(i, label);
                _labelMappers.Insert(i, vtkPolyDataMapper.New());
                _labelFollowers.Insert(i, new vtkFollower());

                _labelMappers[i].SetInputConnection(_labelTexts[i].GetOutputPort());
                _labelFollowers[i].SetMapper(_labelMappers[i]);
                _labelFollowers[i].GetProperty().SetColor(0, 0, 0);
                _labelFollowers[i].SetCamera(_ren.GetActiveCamera());
                _labelFollowers[i].SetScale(LabelScale);

                ////Align by offsetting for size
                //double height = _labelTexts[i].GetHeight();
                //double width = _labelTexts[i].GetWidth();
                double x = _axialIndex;
                double y = _outterRadius * Math.Cos(degrees * Math.PI / 180);
                double z = _outterRadius * Math.Sin(degrees * Math.PI / 180);
                //double zShift = 0;

                ////Adjust position to anchor correctly for quadrant 
                //if (degrees.Between(0, 30))
                //{
                //    zShift = z - (width / 2);
                //}
                //else if (degrees.Between(30, 60))
                //{}
                //else if (degrees.Between(60, 120))
                //{}
                //else if (degrees.Between(120, 150))
                //{}
                //else if (degrees.Between(150, 210))
                //{
                //    zShift = z - (width / 2);
                //}
                //else if (degrees.Between(210, 240))
                //{
                //    zShift = z - width;
                //}
                //else if (degrees.Between(240, 300))
                //{
                //    zShift = z - width;
                //}
                //else if (degrees.Between(300, 330))
                //{
                //    zShift = z - width;
                //}
                //else if (degrees.Between(330, 360))
                //{
                //    zShift = z - (width / 2);
                //}

                _labelFollowers[i].SetPosition(x, y, z);
            }
        }

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // Managed Resources
                if (disposing)
                {
                    // Dispose managed resources.
                    if (!_clock.IsNull())
                    {
                        _clock.Dispose();
                    }

                    if (!_clockMapper.IsNull())
                    {
                        _clockMapper.Dispose();
                    }

                    if (!_clockActor.IsNull())
                    {
                        _clockActor.Dispose();
                    }

                    if (!_clockPlane.IsNull())
                    {
                        _clockPlane.Dispose();
                    }

                    if (!_clockPlaneMapper.IsNull())
                    {
                        _clockPlaneMapper.Dispose();
                    }

                    if (!_clockPlaneActor.IsNull())
                    {
                        _clockPlaneActor.Dispose();
                    }

                    for (int i = 0; i < _labelFollowers.Count; i++)
                    {
                        _labelFollowers[i].Dispose();
                        _labelMappers[i].Dispose();
                        _labelTexts[i].Dispose();
                    }

                    for (int i = 0; i < _tickActors.Count; i++)
                    {
                        _tickActors[i].Dispose();
                        _tickLines[i].Dispose();
                        _tickLines[i].Dispose();
                    }
                    _clockAssembly.Dispose();
                }

                //Unmanaged resources

                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}