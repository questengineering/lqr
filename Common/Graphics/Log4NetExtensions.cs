﻿﻿#region Copyright QuestReliability, LLC 2008
//
// www.questreliability.com
// +1 (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: Log4NetExtensions.cs,v 1.3 2008/11/25 19:38:04 J.Rowe Exp $
//
#endregion

using System;
using log4net;
using Kitware.VTK;
using System.Text;

namespace QuestReliability.Extensions
{
    public static class VTKLoggingExtensions
    {
        public static void LogGridInfo(this ILog logger, vtkDataSet grid)
        {
            if (logger.IsDebugEnabled)
            {
                double[] bounds = new double[6];
                bounds = grid.GetBounds();
                string message = string.Format("Grid set: Points:{0}; Cells:{1}; Xmin:{2}; Xmax:{3}; Ymin:{4}; Ymax:{5}; Zmin:{6}; Zmax:{7}",
                                    grid.GetNumberOfPoints(), grid.GetNumberOfCells(), bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]);
                logger.Debug(message);
            }
        }

    }
}
