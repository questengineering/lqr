﻿using System.Drawing;
using Kitware.VTK;

namespace QuestIntegrity.Graphics
{
    public class MarkupText3D
    {
        readonly vtkVectorText _vectorText = new vtkVectorText();
        readonly vtkPolyDataMapper _textMapper = vtkPolyDataMapper.New();
        public readonly vtkFollower Actor = new vtkFollower();
        
        /// <summary> Sets the raw position of the text in 3D space. </summary>
        public Point3D Position
        {
            get
            {
                var pos = Actor.GetPosition();
                return new Point3D {X = pos[0], Y = pos[1], Z = pos[2] };
            }
            set
            {
                Actor.SetPosition(value.X, value.Y, value.Z);
            }
        }

        /// <summary> Creates the text. Don't forget to set the text to show, and the camera to follow. </summary>
        public MarkupText3D()
        {
            _textMapper.SetInputConnection(_vectorText.GetOutputPort());
            Actor.SetMapper(_textMapper);
            Actor.GetProperty().SetColor(Color.Black);
        }

        public void SetCamera(vtkCamera cam)
        {
            Actor.SetCamera(cam);
        }

        public string Text
        {
            get { return _vectorText.GetText(); }
            set { _vectorText.SetText(value);}
        }
    }


    public struct Point3D
    {
        public double X {get;set;}
        public double Y {get;set;}
        public double Z {get;set;}
    }
}
