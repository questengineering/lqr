﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

using System;
using System.Drawing;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    public class ColorEntry : IComparable
    {
        #region Private / Protected Variables

        #endregion

        #region Public Properties

        public double ScalarValue { get; set; }
        public Color ColorValue { get; set; }

        public int R { get { return ColorValue.R; } set { ColorValue = Color.FromArgb(value, ColorValue.G, ColorValue.B); } }
        public int G { get { return ColorValue.G; } set { ColorValue = Color.FromArgb(ColorValue.R, value, ColorValue.B); } }
        public int B { get { return ColorValue.B; } set { ColorValue = Color.FromArgb(ColorValue.R, ColorValue.G, value); } }

        #endregion

        #region Constructors

        public ColorEntry()
        {
            ScalarValue = 0;
            ColorValue = Color.White;
        }

        public ColorEntry(double scalarValue, int r, int g, int b)
            : this()
        {
            ScalarValue = scalarValue;
            ColorValue = Color.FromArgb(r, g, b);
        }

        public ColorEntry(double scalarValue, Color c)
            : this()
        {
            ScalarValue = scalarValue;
            ColorValue = c;
        }

        #endregion

        #region Public Methods

        #endregion

        #region IComparable

        public int CompareTo(object obj)
        {
            double value = ((ColorEntry)obj).ScalarValue;

            if (ScalarValue < value) return -1;
            if (ScalarValue == value) return 0;
            return 1;
        }

        #endregion

    }
}
