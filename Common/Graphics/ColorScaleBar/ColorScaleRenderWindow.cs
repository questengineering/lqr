﻿using System.Windows.Forms;
using Kitware.VTK;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    /// <summary> A simple VTK Window that holds a ColorScaleBar and fills the view with it. </summary>
    public partial class ColorScaleRenderWindow : UserControl
    {
        public readonly RenderWindowControl RenderWindowControl = new RenderWindowControl();
        public vtkRenderer Renderer { get { return RenderWindow.GetRenderers().GetFirstRenderer(); } }
        public vtkRenderWindow RenderWindow { get { return RenderWindowControl.RenderWindow; } }
        private ColorScaleBar _currentCSB;
        public ColorScaleBar CurrentCSB { get { return _currentCSB; } }

        public ColorScaleRenderWindow()
        {
            InitializeComponent();
            if (DesignMode) return;
            SetupRendererWindow();
        }

        public ColorScaleRenderWindow(ColorScaleBar csb): this()
        {
            SetColorScaleBar(csb);
        }

        private void SetupRendererWindow()
        {
            RenderWindowControl.CreateControl();
            RenderWindowControl.Dock = DockStyle.Fill;
            Controls.Add(RenderWindowControl);
            Renderer.SetBackground(1, 1, 1);
        }

        /// <summary> Assigns the CSB to the renderer. Make sure to set it as horz/vert before passing it in. </summary>
        public void SetColorScaleBar(ColorScaleBar csb)
        {
            //If a form is undocked it destroys and recreates the renderwindow. Since the only part of the renderer that needs to be adjusted is this background color
            //we might as well set it so it doesn't have to be removed/added on handle destruction like others.
            if (RenderWindow == null) SetupRendererWindow();
            Renderer.SetBackground(1, 1, 1); //Left here on purpose. 
            
            if (_currentCSB != null) _currentCSB.Clear(Renderer);
            if (csb == null) return;
            _currentCSB = csb;
            _currentCSB.Title = _currentCSB.ActiveScalarName;
            _currentCSB.Position = new[] { .07d, 0d };
            _currentCSB.Height = .98;
            _currentCSB.Width = .85;
            _currentCSB.Build();
            _currentCSB.Draw(Renderer);
             RenderWindow.Render();
        }

        protected override void OnHandleCreated(System.EventArgs e)
        {
            SetColorScaleBar(_currentCSB);
            base.OnHandleCreated(e);
        }
    }
}
