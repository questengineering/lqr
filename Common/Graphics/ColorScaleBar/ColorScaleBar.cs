#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// Modified from original ColorScaleBar in 9/13 by J.Foster
//
#endregion

using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using Kitware.VTK;
using log4net;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    /// <summary>
    /// Class that encapsulates the VTK ScalarBarWidget and Actor as well as the various
    /// color scales we use.
    /// </summary>

    public class ColorScaleBar : IDisposable
    {
        #region Private/Protected Variables
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly vtkScalarBarActor _scalarBarActor;
        private readonly vtkScalarBarWidget _scalarBarWidget;
        private vtkTextProperty _prop;
        private readonly ColorScale _colorScale;
        private readonly vtkColorTransferFunction _colorTransferFunction;
        /// <summary> The color transfer function shown in display Units. Should not be used for lookup unless display units are desired. </summary>
        private readonly vtkColorTransferFunction _displayColorTransferFunction;
        private readonly vtkColorTransferFunction _nanColorTransferFunction;
        private readonly vtkLookupTable _colorLookup;
        private const double Epsilon = .0001;
        private UnitSystem _units = new UnitSystem(UnitSystem.UnitSystems.Lbs_in_psi);
        private Length.LengthScale _baseUnitLengthScale = Length.LengthScale.Inches;

        private const string LabelFormatMillimeters = "%10.1f";
        private const string LabelFormatInches = "%10.3f";

        private double[] _position = {0d,0d};

        private bool _disposed;

        private static readonly IntPtr NaNColorPtr = Marshal.AllocHGlobal(3);

        #endregion

        public enum ColorScaleBarOrientation
        {
            Horizontal, Vertical
        }

        #region Constructors

        static ColorScaleBar()
        {
            byte[] color = { 255, 255, 255 };
            Marshal.Copy(color, 0, NaNColorPtr, 3);
        }

        public ColorScaleBar() 
        {
            MinMaxRanges = new SortableSearchableList<ScalarRange>(TypeDescriptor.GetProperties(typeof(ScalarRange))["Min"]);
            _prop = new vtkTextProperty();
            _scalarBarActor = new vtkScalarBarActor();
            _scalarBarWidget = new vtkScalarBarWidget();
            _colorScale = new ColorScale();
            _colorTransferFunction = new vtkColorTransferFunction();
            _displayColorTransferFunction = new vtkColorTransferFunction();
            _nanColorTransferFunction = new vtkColorTransferFunction();
            _colorLookup = new vtkLookupTable();

            _prop.SetFontFamilyToArial();
            _prop.SetFontSize(10);
            _prop.SetColor(0, 0, 0);
            _prop.SetJustificationToCentered();

            _scalarBarActor.SetPosition(.85, .05);
            _scalarBarActor.SetWidth(.05);
            _scalarBarActor.SetHeight(0.9);
            _scalarBarActor.SetLabelFormat(LabelFormatInches);
            _scalarBarActor.SetLabelTextProperty(_prop);
            _scalarBarActor.SetTextPositionToSucceedScalarBar();
            _scalarBarActor.SetTitleTextProperty(_prop);

            _scalarBarWidget.EnabledOff();
            _scalarBarWidget.SetScalarBarActor(_scalarBarActor);
            _scalarBarWidget.CreateDefaultRepresentation();
            _scalarBarWidget.GetScalarBarRepresentation().SetShowBorderToOn();
            _scalarBarWidget.GetScalarBarRepresentation().GetBorderProperty().SetColor(0.4, 0.4, 0.4);

            double[] pos = _scalarBarActor.GetPosition();
            double[] pos2 = _scalarBarActor.GetPosition2();
            _scalarBarWidget.GetScalarBarRepresentation().SetPosition(pos[0], pos[1]);
            _scalarBarWidget.GetScalarBarRepresentation().SetPosition2(pos2[0], pos2[1]);

            _scalarBarActor.SetOrientationToVertical(); 

            // Set the label format
            switch (DisplayUnits.Instance.MeasurementUnits.Scale)
            {
                case Length.LengthScale.Millimeters:
                    _scalarBarActor.SetLabelFormat(LabelFormatMillimeters);
                    break;
                default:
                    _scalarBarActor.SetLabelFormat(LabelFormatInches);
                    break;
            }
        }

        #endregion

        #region Public Properties

        public UnitSystem Units
        {
            get
            {
                return _units;
            }
            set
            {
                _units = value;
                if (_baseUnitLengthScale == Length.LengthScale.Unknown)
                {
                    _baseUnitLengthScale = _units.LengthUnitScale;
                }
            }
        }

        public string Title
        {
            get
            {
                return _scalarBarActor.GetTitle();
            }
            set
            {
                _scalarBarActor.SetTitle(value);
            }
        }

        /// <summary> The height of the Color Scale Bar in view units (0-1) </summary>
        public double Height
        {
            get
            {
                return _scalarBarActor.GetHeight();
            }
            set
            {
                _scalarBarActor.SetHeight(value.Constrain(0, 1));
            }
        }

        /// <summary> The width of the Color Scale Bar in view units (0-1) </summary>
        public double Width
        {
            get
            {
                return _scalarBarActor.GetWidth();
            }
            set
            {
                _scalarBarActor.SetWidth(value.Constrain(0, 1));
            }
        }

        public double[] Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;

                _scalarBarActor.SetPosition(_position[0], _position[1]);

                double[] pos = _scalarBarActor.GetPosition();
                double[] pos2 = _scalarBarActor.GetPosition2();

                _scalarBarWidget.GetScalarBarRepresentation().SetPosition(pos[0], pos[1]);
                _scalarBarWidget.GetScalarBarRepresentation().SetPosition2(pos2[0], pos2[1]);
            }
        }

       

        public ColorScaleBarOrientation Orientation
        {
            get
            {
                return _scalarBarActor.GetOrientation() == 0 ? ColorScaleBarOrientation.Horizontal : ColorScaleBarOrientation.Vertical;
            }
            set
            {
                if (value == ColorScaleBarOrientation.Horizontal)
                {
                    _scalarBarActor.SetOrientationToHorizontal();
                }
                else
                {
                    _scalarBarActor.SetOrientationToVertical();
                }
            }
        }

        /// <summary> A dictionary that allows the color scale bar to know how different scalars should be presented. Looked up by scalar name. Requires rebuild. </summary>
        public SerializableDictionary<string, ColorMap> ScalarColorMaps { get; set; }
            
        [XmlIgnore]
        public vtkTextProperty TextProp
        {
            get
            {
                return _prop;
            }
            set
            {
                _prop = value;
                _scalarBarActor.SetTitleTextProperty(value);
                _scalarBarActor.SetLabelTextProperty(value);
            }
        }

        public ColorMap CurrentColorMap
        {
            get { return _colorScale.CurrentColorMap; }
        }

        public SortableSearchableList<ScalarRange> MinMaxRanges { get; set; }
        /// <summary> Determines the active scalar. Requires rebuild. </summary>
        public string ActiveScalarName { get; set; }

        [XmlIgnore] // Read-only property, if you want to reset the CTF, use the ColorTable.
        public vtkColorTransferFunction NaNColorTransferFunction
        {
            get
            {
                BuildCtf(); 
                BuildNaNctf(); 
                return _nanColorTransferFunction;
                //return _colorTransferFunction;
            }
        }


        [XmlIgnore] // Read-only property, if you want to reset the CTF, use the ColorTable.
        public vtkColorTransferFunction DisplayColorTransferFunction
        {
            get
            {
                BuildCtf();
                return _displayColorTransferFunction;
            }
        }

        [XmlIgnore] // Read-only property, if you want to reset the CTF, use the ColorTable.
        public vtkColorTransferFunction ColorTransferFunction
        {
            get
            {
                BuildCtf(); 
                return _colorTransferFunction;
            }
        }

        public ColorTableEntries ColorTable
        {
            get
            {
                return _colorScale.Table;
            }
            set
            {
                _colorScale.Table = value;
            }
        }

        // This property is not serializable because of it's type.
        [XmlIgnore]
        public Color NaNColor
        {
            get
            {
                return _colorScale.NaNColor;
            }
            set
            {
                _colorScale.NaNColor = value;
                byte[] color = { value.R, value.G, value.B };
                Marshal.Copy(color, 0, NaNColorPtr, 3);
            }
        }

        // This is the serializable version of NaNColor.
        public string NaNColorHtml
        {
            get
            {
                return ColorTranslator.ToHtml(_colorScale.NaNColor);
            }
            set
            {
                _colorScale.NaNColor = ColorTranslator.FromHtml(value);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Maps a value through the CTF.  It will replace NaNs directly
        /// allowing for use of a simple LookupTable instead of the more computationally intensive
        /// piece-wise ColorTransferFunction.  this makes a big difference in performance with setting
        /// up image grid scalars.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public byte[] MapValue(double value)
        {
            if (double.IsNaN(value))
            {
                return new[] { NaNColor.R, NaNColor.G, NaNColor.B };
            }
            byte[] colorByteArray = new byte[3];
            IntPtr ptr = _colorLookup.MapValue(value);
            Marshal.Copy(ptr, colorByteArray, 0, 3);
            return colorByteArray;
        }

        /// <summary>
        /// Maps a value through the CTF.  It will replace NaNs directly
        /// allowing for use of a simple LookupTable instead of the more computationally intensive
        /// piece-wise ColorTransferFunction.  this makes a big difference in performance with setting
        /// up image grid scalars.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="colorPtr"></param>
        /// <returns></returns>
        public void MapValue(double value, out IntPtr colorPtr)
        {
            colorPtr = double.IsNaN(value) ? NaNColorPtr : _colorLookup.MapValue(value);
        }

        public IntPtr[] MapValues(float[] scalarData)
        {
            IntPtr[] pointerValues = new IntPtr[scalarData.Length];

            for (int i = 0; i < scalarData.Length; i++)
            {
                if (scalarData[i].IsNaN()) pointerValues[i] = NaNColorPtr;
                else MapValue(scalarData[i], out pointerValues[i]);
            }

            return pointerValues;
        }

        public IntPtr[] MapValues(float[,] scalarData)
        {
            IntPtr[] pointerValues = new IntPtr[scalarData.GetLength(0) * scalarData.GetLength(1)];

            for (int x = 0; x < scalarData.GetLength(0); x++)
            {
                for (int y = 0; y < scalarData.GetLength(1); y++ )
                {
                    int point1D = x * scalarData.GetLength(1) + y;
                    if (scalarData[x, y].IsNaN()) pointerValues[point1D] = NaNColorPtr;
                    else MapValue(scalarData[x, y], out pointerValues[point1D]);
                }
            }

            return pointerValues;
        }

        public void SetScaleToLinear()
        {
            _colorTransferFunction.SetScaleToLinear();
        }

        public void SetScaleToLog10()
        {
            _colorTransferFunction.SetScaleToLog10();
        }

        public void Draw(vtkRenderer ren)
        {
            ren.RemoveActor2D(_scalarBarActor);

            _scalarBarWidget.SetInteractor(ren.GetRenderWindow().GetInteractor());
            _scalarBarWidget.SetCurrentRenderer(ren);

            _scalarBarActor.SetLookupTable(DisplayColorTransferFunction);

            _scalarBarActor.Modified();

            ren.AddActor2D(_scalarBarActor);
        }

        /// <summary>
        /// Removes actor from the renderer so that it can be disposed of.
        /// </summary>
        /// <param name="ren"></param>
        public void Clear(vtkRenderer ren)
        {
            ren.RemoveActor2D(_scalarBarActor);
        }

        public void EditingOn()
        {
            _scalarBarWidget.EnabledOn();
        }

        public void EditingOff()
        {
            _scalarBarWidget.EnabledOff();
        }

        public void VisibilityOn()
        {
            _scalarBarActor.VisibilityOn();

        }

        public void VisibilityOff()
        {
            _scalarBarActor.VisibilityOff();
        }

        /// <summary>
        /// Copies values from provided parameter to this instance. Performs a deep copy.
        /// (JTR: 2/5/10 don't think this really does a deep copy...this whole CSB class is a little sketchy)
        /// </summary>
        /// <param name="csb">ColorScaleBar to copy</param>
        public void CopyFrom(ColorScaleBar csb)
        {

            ColorTable.CopyFrom(csb.ColorTable);

            Units = csb.Units;
            MinMaxRanges = new SortableSearchableList<ScalarRange>();
            foreach (var range in csb.MinMaxRanges)
            {
                MinMaxRanges.Add(new ScalarRange { Min = range.Min, Max = range.Max, ScalarName = range.ScalarName });
            }
            Title = csb.Title;
            TextProp.ShallowCopy(csb.TextProp);
            _scalarBarActor.SetLabelFormat(csb._scalarBarActor.GetLabelFormat());
            Height = csb.Height;
            Width = csb.Width;
            Position = csb.Position;
            Orientation = csb.Orientation;
            NaNColor = csb.NaNColor;

            ScalarColorMaps = csb.ScalarColorMaps;
            ActiveScalarName = csb.ActiveScalarName;
        }

        public double[] GetRange(string scalarName)
        {
            double[] returnValues = new double[2];
            ScalarRange activeRange = MinMaxRanges.Find(r => r.ScalarName == ActiveScalarName);
            if (activeRange != null) 
            {
                returnValues[0] = activeRange.Min;
                returnValues[1] = activeRange.Max;
            }
            return returnValues;
        }

        /// <summary> Replaces the old range with the new one. </summary>
        public void SetRange(ScalarRange theNewRange)
        {
            ScalarRange theOldRange = MinMaxRanges.Find(r => r.ScalarName == theNewRange.ScalarName);
            MinMaxRanges.Remove(theOldRange);
            //Make sure the min value is actually the minimum, otherwise it will not map colors properly.
            MinMaxRanges.Add(new ScalarRange {Min = Math.Min(theNewRange.Min, theNewRange.Max), Max = Math.Max(theNewRange.Min, theNewRange.Max), ScalarName = theNewRange.ScalarName});
        }

        public void Build()
        {
            try
            {
                if (ActiveScalarName == null && ScalarColorMaps.Count > 0) 
                    ActiveScalarName = ScalarColorMaps.First().Key;
                _colorScale.Range = GetRange(ActiveScalarName);
                _colorScale.CurrentColorMap = ScalarColorMaps[ActiveScalarName];
                _colorScale.Build();
                BuildCtf();
                BuildNaNctf();
                BuildLookupTable();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(@"Error building colormap: " + ex.Message);
                throw;
            }
        }

        public void Render()
        {
            _scalarBarWidget.Render();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Builds a simple VTK lookup table of the color values.  This can be used for linear
        ///  mapping and is more efficient than the piecewise color transfer function.
        ///  MapValues() uses this lookup table.
        /// </summary>
        private void BuildLookupTable()
        {
            double[] currentRange = GetRange(ActiveScalarName);
            double currentMin = currentRange[0];
            double currentMax = currentRange[1];
            _colorLookup.SetNumberOfTableValues(_colorScale.Table.Count);
            _colorLookup.SetTableRange(currentMin, currentMax);
            int i = 0;
            foreach (double[] rgb in _colorScale.Table.Select(e => e.ColorValue.ToFloatRGB()))
            {
                _colorLookup.SetTableValue(i, rgb[0], rgb[1], rgb[2], 1);
                i++;
            }

        }

        private void BuildCtf()
        {
            _colorTransferFunction.RemoveAllPoints();
            _colorTransferFunction.SetColorSpaceToHSV();

            _displayColorTransferFunction.RemoveAllPoints();
            _displayColorTransferFunction.SetColorSpaceToHSV();

            foreach (ColorEntry e in _colorScale.Table)
            {
                double[] rgb = e.ColorValue.ToFloatRGB();
                //Convert label that is displayed for view purposes
                double scaledValue = Length.Convert(e.ScalarValue, DisplayUnits.Instance.MeasurementUnits.Scale, _units.LengthUnitScale);
                _colorTransferFunction.AddRGBPoint(e.ScalarValue, rgb[0], rgb[1], rgb[2]);
                _displayColorTransferFunction.AddRGBPoint(scaledValue, rgb[0], rgb[1], rgb[2]);
            }

            _colorTransferFunction.Build();
            _displayColorTransferFunction.Build();
        }

        private void BuildNaNctf()
        {
            //The NaN Color transfer function is used for rendering the actual data the NaN color for data that is outside of the Global Min and Max range of the data
            //  by using a small "epsilon" increment.

            _nanColorTransferFunction.RemoveAllPoints();
            _nanColorTransferFunction.SetColorSpaceToHSV();

            foreach (ColorEntry e in _colorScale.Table)
            {
                double[] rgb = e.ColorValue.ToFloatRGB();
                //Note: do not convert to display units because the underlying data hasn't changed (unlike CTF which is used for the legend)
                _nanColorTransferFunction.AddRGBPoint(e.ScalarValue, rgb[0], rgb[1], rgb[2]);
            }

            double[] currentRange = GetRange(ActiveScalarName);
            double currentMin = currentRange[0];
            double currentMax = currentRange[1];

            double[] minColor = _colorTransferFunction.GetColor(currentMin);
            double[] maxColor = _colorTransferFunction.GetColor(currentMax);

            double[] naN = _colorScale.NaNColor.ToFloatRGB();
            double rNaN = naN[0];
            double gNaN = naN[1];
            double bNaN = naN[2];

            //Insert new points in the CTR for the values beyone the global mins and max
            _nanColorTransferFunction.AddRGBPoint(currentMin - Epsilon, rNaN, gNaN, bNaN);
            _nanColorTransferFunction.AddRGBPoint(currentMin, minColor[0], minColor[1], minColor[2]);
            _nanColorTransferFunction.AddRGBPoint(currentMax, maxColor[0], maxColor[1], maxColor[2]);
            _nanColorTransferFunction.AddRGBPoint(currentMax + Epsilon, rNaN, gNaN, bNaN);

            _nanColorTransferFunction.Build();
        }

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (_disposed) return;
            // Managed Resources
            if (disposing)
            {
                _nanColorTransferFunction.Dispose();
                _colorTransferFunction.Dispose();
                _displayColorTransferFunction.Dispose();

                _prop.Dispose();
                _scalarBarActor.Dispose();
                _scalarBarWidget.Dispose();

                _colorLookup.Dispose();
                _colorScale.Dispose();
            }

            //Set Flag                
            _disposed = true;
        }

        #endregion

        /// <summary> Initializes which scalars should exist in the color scale bar based off of their name. Clears all existing and creates new ones. </summary>
        public virtual void InitializeScalars(SerializableDictionary<string, ColorMap> colorMapTypes)
        {
            MinMaxRanges.Clear();
            ScalarColorMaps = colorMapTypes;
            foreach (var entry in colorMapTypes)
            {
                MinMaxRanges.Add(new ScalarRange { ScalarName = entry.Key});
            }
        }

        /// <summary> Creates a copy of the passed in color scale and sets the active scalar on the new copy. </summary>
        public static ColorScaleBar GetNewCopy(ColorScaleBar colorScale, string activeScalarName)
        {
            ColorScaleBar newColorScale = new ColorScaleBar();
            newColorScale.CopyFrom(colorScale);
            newColorScale.ActiveScalarName = activeScalarName;
            newColorScale.NaNColor = Color.Gray;
            newColorScale.Build();
            return newColorScale;
        }

        
    }
}