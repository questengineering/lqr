﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

using System.Collections.Generic;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    /// <summary> Wrapper class that is a List(of ColorEntry). </summary>
    public class ColorTableEntries : List<ColorEntry>
    {       
        
        #region Public Methods

        public void CopyFrom(ColorTableEntries c)
        {
            Clear();
            AddRange(c);
        }

        #endregion
    }
}
