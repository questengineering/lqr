﻿

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    public interface IHasColorScaleBar
    {
        void AttachColorBarChangedEvent();
        void DetachColorBarChangedEvent();
        void HandleColorBarChangedEvent(object sender, ColorScaleChangedEventArgs args);
    }
}
