﻿namespace QuestIntegrity.Graphics.ColorScaleBar
{
    public class ColorScaleChangedEventArgs
    {
        public ScalarRange TheScalarRange;

        public ColorScaleChangedEventArgs(ScalarRange theRange)
        {
            TheScalarRange = theRange;
        }
    }
}
