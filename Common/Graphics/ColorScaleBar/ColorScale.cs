﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/26/2008 9:17:37 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//
#endregion

using System;
using System.Drawing;
using QuestIntegrity.Core.Extensions;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    #region Enums

    public enum ColorMap
    {
        JetRedBlue,
        JetBlueRed,
        RainbowRedBlue,
        RainbowBlueRed,
        RainbowPurpleRed,
        RainbowRedPurple,
        Grayscale,
        Custom,
        TwoColor,
        ThreeColor,
    }

    public enum ColorSpace
    {
        Rgb,
        Hsv,
        Grayscale,
        Custom
    }

    #endregion

    public class ColorScale : IDisposable
    {
        #region Private/Protected Variables

        public const int NumberOfColors = 64;  
        private bool _disposed;
        public static Tuple<Color, Color> TwoColors = new Tuple<Color, Color>(Color.Red, Color.SteelBlue);
        public static Tuple<Color, Color, Color> ThreeColors = new Tuple<Color, Color, Color>(Color.Red, Color.Yellow, Color.Green);

        #endregion

        #region Constructors

        public ColorScale()
        {
            Title = string.Empty;
            CurrentColorMap = ColorMap.JetBlueRed;
            Table = new ColorTableEntries();
            NaNColor = Color.White;
        }

        #endregion

        #region Public Properties

        public string Title { get; set; }

        public ColorTableEntries Table { get; set; }

        public Color NaNColor { get; set; }

        public ColorMap CurrentColorMap { get; set; }

        public double[] Range { get; set; }

        #endregion

        #region Public Methods

        public void AddTableValue(double v, Color c)
        {
            Table.Add(new ColorEntry(v, c));
        }

        public void Build()
        {
            double currentMin = Range[0];
            double currentMax = Range[1];

            double value;

            double h, s, v;
            Color c = new Color();

            if (CurrentColorMap == ColorMap.RainbowRedBlue)
            {
                s = 1.0;
                v = 1.0;
                Table.Clear();

                for (int i = 0; i < NumberOfColors; i++)
                {
                    value = currentMin + i * (currentMax - currentMin) / ((double)NumberOfColors - 1);
                    h = ((double)(i) * 240) / (NumberOfColors - 1);

                    Table.Add(new ColorEntry(value, c.FromHSV(h, s, v)));
                }
            }

            if (CurrentColorMap == ColorMap.JetRedBlue || CurrentColorMap == ColorMap.JetBlueRed)
            {
                //JTR:  JET color scale implemented based on https://dev.scicraft.org/svn/scicraft/trunk/Library/Util/colormap.py, which 
                //      was based on algorithm  taken from jet.m at http://octave.sf.net
                //Should be identical to the QTT "Jet" scale used in MatLab
                int i = 0;
                Table.Clear();
                int j = (CurrentColorMap == ColorMap.JetBlueRed ? 0 : NumberOfColors - 1);
                int increment = (CurrentColorMap == ColorMap.JetBlueRed ? 1 : -1);

                while (i < NumberOfColors)
                {
                    value = currentMin + i * (currentMax - currentMin) / ((double)NumberOfColors - 1);
                    double x = (double)j / (NumberOfColors - 1);

                    //R value
                    double r;
                    if ((x >= 3.0 / 8.0) && (x < 5.0 / 8.0))
                    {
                        r = (4.0 * x - 3.0 / 2.0);
                    }
                    else if ((x >= 5.0 / 8.0) && (x < 7.0 / 8.0))
                    {
                        r = 1;
                    }
                    else if (x >= 7.0 / 8.0)
                    {
                        r = -4.0 * x + 9.0 / 2.0;
                    }
                    else
                    {
                        r = 0;
                    }

                    //G value
                    double g;
                    if ((x >= 1.0 / 8.0) && (x < 3.0 / 8.0))
                    {
                        g = (4.0 * x - 1.0 / 2.0);
                    }
                    else if ((x >= 3.0 / 8.0) && (x < 5.0 / 8.0))
                    {
                        g = 1.0;
                    }
                    else if ((x >= 5.0 / 8.0) && (x < 7.0 / 8.0))
                    {
                        g = -4.0 * x + 7.0 / 2.0;
                    }
                    else
                    {
                        g = 0.0;
                    }

                    //B value
                    double b;
                    if ((x < 1.0 / 8.0))
                    {
                        b = (4.0 * x + 1.0 / 2.0);
                    }
                    else if ((x >= 1.0 / 8.0) && (x < 3.0 / 8.0))
                    {
                        b = 1.0;
                    }
                    else if ((x >= 3.0 / 8.0) && (x < 5.0 / 8.0))
                    {
                        b = -4.0 * x + 5.0 / 2.0;
                    }
                    else
                    {
                        b = 0.0;
                    }

                    Table.Add(new ColorEntry(value, (int)(r * 255.0), (int)(g * 255.0), (int)(b * 255.0)));
                    j = j + increment;
                    i++;
                }
            }

            if (CurrentColorMap == ColorMap.RainbowRedPurple)
            {
                s = 1.0;
                v = 1.0;
                Table.Clear();

                for (int i = 0; i < NumberOfColors; i++)
                {
                    value = currentMin + i * (currentMax - currentMin) / ((double)NumberOfColors - 1);
                    h = ((double)(i) * 310) / (NumberOfColors - 1);

                    Table.Add(new ColorEntry(value, c.FromHSV(h, s, v)));
                }
            }
            else if (CurrentColorMap == ColorMap.RainbowBlueRed)
            {
                s = 1.0;
                v = 1.0;
                Table.Clear();

                for (int i = 0; i < NumberOfColors; i++)
                {
                    value = currentMin + i * (currentMax - currentMin) / ((double)NumberOfColors - 1);
                    h = 240 - ((double)i) * 240 / (NumberOfColors - 1);

                    Table.Add(new ColorEntry(value, c.FromHSV(h, s, v)));
                }
            }
            else if (CurrentColorMap == ColorMap.RainbowPurpleRed)
            {
                s = 1.0;
                v = 1.0;
                Table.Clear();

                for (int i = 0; i < NumberOfColors; i++)
                {
                    value = currentMin + i * (currentMax - currentMin) / ((double)NumberOfColors - 1);
                    h = 310 - ((double)i) * 310 / (NumberOfColors - 1);

                    Table.Add(new ColorEntry(value, c.FromHSV(h, s, v)));
                }
            }
            else if (CurrentColorMap == ColorMap.Grayscale)
            {
                Table.Clear();

                Table.Add(new ColorEntry(currentMin, Color.Black));
                Table.Add(new ColorEntry(currentMax, Color.White));
            }
            else if (CurrentColorMap == ColorMap.TwoColor)
            {
                Table.Clear();
                double range = currentMax - currentMin;
                Color cOne = TwoColors.Item1;
                Color cTwo = TwoColors.Item2;
                double rChangePerColor = (cTwo.R - cOne.R) / (double)NumberOfColors;
                double gChangePerColor = (cTwo.G - cOne.G) / (double)NumberOfColors;
                double bChangePerColor = (cTwo.B - cOne.B) / (double)NumberOfColors;
                for (int i = 0; i < NumberOfColors; i++)
                {
                    int r = (int)Math.Round(cOne.R + rChangePerColor * i);
                    int g = (int)Math.Round(cOne.G + gChangePerColor * i);
                    int b = (int)Math.Round(cOne.B + bChangePerColor * i);
                    Table.Add(new ColorEntry(currentMin + range / (NumberOfColors - 1) * i, r,g,b));
                }
            }
            else if (CurrentColorMap == ColorMap.ThreeColor)
            {
                Table.Clear();
                double range = currentMax - currentMin;
                Color cOne = ThreeColors.Item1;
                Color cTwo = ThreeColors.Item2;
                Color cThree = ThreeColors.Item3;
                double rChangePerColorLower = (cTwo.R - cOne.R) / (double)NumberOfColors / 2;
                double gChangePerColorLower = (cTwo.G - cOne.G) / (double)NumberOfColors / 2;
                double bChangePerColorLower = (cTwo.B - cOne.B) / (double)NumberOfColors / 2;
                double rChangePerColorUpper = (cThree.R - cTwo.R) / (double)NumberOfColors / 2;
                double gChangePerColorUpper = (cThree.G - cTwo.G) / (double)NumberOfColors / 2;
                double bChangePerColorUpper = (cThree.B - cTwo.B) / (double)NumberOfColors / 2;
                for (int i = 0; i < NumberOfColors; i++)
                {
                    int r, g, b;
                    if (i <= NumberOfColors / 2)
                    {
                        r = (int)Math.Round(cOne.R + rChangePerColorLower * i);
                        g = (int)Math.Round(cOne.G + gChangePerColorLower * i);
                        b = (int)Math.Round(cOne.B + bChangePerColorLower * i);
                    }
                    else
                    {
                        r = (int)Math.Round(cTwo.R + rChangePerColorUpper * i);
                        g = (int)Math.Round(cTwo.G + gChangePerColorUpper * i);
                        b = (int)Math.Round(cTwo.B + bChangePerColorUpper * i);
                    }
                    
                    Table.Add(new ColorEntry(currentMin + range / (NumberOfColors - 1) * i, r, g, b));
                }
            }

        }

        #endregion

        

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // Managed Resources
                if (disposing)
                {
                }

                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}