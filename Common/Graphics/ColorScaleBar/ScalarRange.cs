﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.Graphics.ColorScaleBar
{
    public class ScalarRange : IComparable, INotifyPropertyChanged
    {
        public string ScalarName { get; set; }
        private readonly UnitSystem _units = new UnitSystem(UnitSystem.UnitSystems.Lbs_in_psi); //arbitrary base unit the same as ColorScaleBar
        private double _max;
        private double _min;

        #region Display Unit Versions

        public double Max
        {
            get { return _max; }
            set
            {
                if (value.Equals(_max)) return;
                _max = value;
                OnPropertyChanged();
                OnPropertyChanged("MaxInDisplayUnits");
            }
        }

        public double Min
        {
            get { return _min; }
            set
            {
                if (value.Equals(_min)) return;
                _min = value;
                OnPropertyChanged();
                OnPropertyChanged("MinInDisplayUnits");
            }
        }

        [XmlIgnore]
        public double MaxInDisplayUnits
        {
            get { return _units.LengthUnitSystem.Convert(Max, DisplayUnits.Instance.MeasurementUnits.Scale); }
            set { Max = DisplayUnits.Instance.MeasurementUnits.Convert(value, _units.LengthUnitScale); }
        }

        [XmlIgnore]
        public double MinInDisplayUnits
        {
            get { return _units.LengthUnitSystem.Convert(Min, DisplayUnits.Instance.MeasurementUnits.Scale); }
            set { Min = DisplayUnits.Instance.MeasurementUnits.Convert(value, _units.LengthUnitScale); }
        }

        #endregion DisplayUnit Versions

        public int CompareTo(object obj)
        {
            if (obj is ScalarRange && ScalarName == ((ScalarRange)obj).ScalarName)return 0;
            return -1;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
