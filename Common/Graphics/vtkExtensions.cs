#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/7/2008 2:44:21 PM
// Created by:   J.Rowe
//
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics
{

    public static class VtkExtensions
    {

        /// <summary>
        /// Sets the visibility of the vtkProp
        /// </summary>
        /// <param name="prop">The prop.</param>
        /// <param name="isVisible">if set to <c>true</c> the vtkProp is visible.</param>
        public static void SetVisibility(this vtkProp prop, bool isVisible)
        {
            prop.SetVisibility(isVisible.ToInt());
        }

        public static double GetDistanceFromCamera(this vtkActor actor, vtkCamera camera)
        {
            var pos1 = actor.GetPosition();
            var pos2 = camera.GetPosition();
            var distX = pos2[0] - pos1[0];
            var distY = pos2[1] - pos1[1];
            var distZ = pos2[2] - pos1[2];

            var distance = Math.Sqrt(distX * distX + distY * distY + distZ * distZ);
            return distance;
        }

        /// <summary>
        /// Return the height of the text in World coordinates
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static double GetHeight(this vtkVectorText text)
        {
            text.Update();
            double[] bounds = text.GetOutput().GetBounds();
            return Math.Abs(bounds[3] - bounds[2]);
        }

        /// <summary>
        /// Return the width of the text in World coordinates
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static double GetWidth(this vtkVectorText text)
        {
            text.Update();
            double[] bounds = text.GetOutput().GetBounds();
            return Math.Abs(bounds[1] - bounds[0]);
        }

        /// <summary>
        /// Helper function to set color using .Net framework Color class.
        /// </summary>
        /// <param name="prop">vtkProperty object.  e.g., something.GetProperty()</param>
        /// <param name="color">Color to set</param>
        public static void SetEdgeColor(this vtkProperty prop, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            prop.SetEdgeColor(colorArray[0], colorArray[1], colorArray[2]);
        }
        /// <summary>
        /// Helper function to set color using .Net framework Color class.
        /// </summary>
        /// <param name="prop">vtkProperty object.  e.g., something.GetProperty()</param>
        /// <param name="color">Color to set</param>
        public static void SetColor(this vtkProperty prop, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            prop.SetColor(colorArray[0], colorArray[1], colorArray[2]);
        }
        /// <summary>
        /// Helper function to set color using .Net framework Color class.
        /// </summary>
        /// <param name="prop">vtkProperty object.  e.g., something.GetProperty()</param>
        /// <param name="color">Color to set</param>
        public static void SetColor(this vtkProperty2D prop, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            prop.SetColor(colorArray[0], colorArray[1], colorArray[2]);
        }
        /// <summary>
        /// Helper function to set color using .Net framework Color class.
        /// </summary>
        /// <param name="prop">vtkProperty object.  e.g., something.GetProperty()</param>
        /// <param name="color">Color to set</param>
        public static void SetColor(this vtkTextProperty prop, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            prop.SetColor(colorArray[0], colorArray[1], colorArray[2]);
        }
        /// <summary> Helper function to set color using .Net framework Color class. </summary>
        public static void SetColor(this vtkGlyphSource2D glyph, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            glyph.SetColor(colorArray[0], colorArray[1], colorArray[2]);
        }

        /// <summary> Helper function to set background color using .Net framework Color class. </summary>
        public static void SetBackground(this vtkViewport viewPort, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            viewPort.SetBackground(colorArray[0], colorArray[1], colorArray[2]);
        }

        /// <summary> Helper function to set color using .Net framework Color class. </summary>
        public static void SetTableValue(this vtkLookupTable lut, int id, Color color)
        {
            double[] colorArray = color.ToFloatRGB();
            lut.SetTableValue(id, colorArray[0], colorArray[1], colorArray[2], 0);
        }
        
        /// <summary> Converts screen coordinates to world coordiates.  Uses the focaldepth as the z value. Scale parameter if the there is a scale factor </summary>
        public static double[] ScreenToWorldCoordinate(this vtkRenderer renderer, double x, double y, double z, double yScaleFactor)
        {
            double[] worldPt = new double[4];
            double[] worldPtXyz = new double[3];
            double[] viewFocus = new double[4];
            if (renderer != null)
            {
                double focalDepth;
                if (z == 0)
                {
                    viewFocus = renderer.GetActiveCamera().GetFocalPoint();
                    renderer.SetWorldPoint(viewFocus[0], viewFocus[1], viewFocus[2], 0);
                    renderer.WorldToDisplay();
                    viewFocus = renderer.GetDisplayPoint();
                    focalDepth = viewFocus[2];
                }
                else
                {
                    focalDepth = z;
                }

                renderer.SetDisplayPoint(x, y, focalDepth);
                renderer.DisplayToWorld();
                worldPt = renderer.GetWorldPoint();

                Array.Copy(worldPt, worldPtXyz, 3); //reduce to x, y, z components
                worldPtXyz[1] = worldPtXyz[1] / yScaleFactor;
            }
            return worldPtXyz;
        }
        /// <summary>
        /// Converts screen coordinates to world coordiates.  Uses the focaldepth as the z value.
        /// </summary>
        /// <param name="renderer"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static double[] ScreenToWorldCoordinate(this vtkRenderer renderer, double x, double y)
        {
            return renderer.ScreenToWorldCoordinate(x, y, 0d, 1.0D);
        }

        public static int[] WorldToScreenCoordinate(this vtkRenderer renderer, double x, double y, double z)
        {
            double[] localPt = new double[3];
            int[] localScreenPt = new int[3];
            double[] worldPtXyz = { x, y, z };
            if (renderer != null)
            {
                renderer.SetWorldPoint(worldPtXyz[0], worldPtXyz[1], worldPtXyz[2], 0);
                renderer.WorldToDisplay();
                localPt = renderer.GetDisplayPoint();
            }
            localScreenPt[0] = (int)Math.Round(localPt[0]);
            localScreenPt[1] = (int)Math.Round(localPt[1]);
            localScreenPt[2] = (int)localPt[2];
            return localScreenPt;
        }

        /// <summary> Returns screen point of 1, 2, or 3D world point. Replaces missing values with 0 if array isn't 3 long. </summary>
        public static int[] WorldToScreenCoordinate(this vtkRenderer renderer, double[] worldPoint)
        {
            if (worldPoint.Length > 2)
                return WorldToScreenCoordinate(renderer, worldPoint[0], worldPoint[1], worldPoint[2]);
            if (worldPoint.Length == 2)
                return WorldToScreenCoordinate(renderer, worldPoint[0], worldPoint[1], 0);
            if (worldPoint.Length == 1)
                return WorldToScreenCoordinate(renderer, worldPoint[0], 0, 0);
            return new[] { 0, 0, 0 };
        }

        /// <summary> Returns the normalized viewport coordinates of a 3D coordinate (x,y,z) on the 2D plane (0-1x, 0-1y) </summary>
        public static double[] WorldToNormalizedViewPortCoordinate(this vtkRenderer renderer, double[] worldPoint)
        {
            //If the array isn't long enough, replace missing values with 0.
            return WorldToNormalizedViewPortCoordinate(renderer, worldPoint[0], worldPoint.Length > 1 ? worldPoint[1] : 0, worldPoint.Length > 2 ? worldPoint[2] : 0);
        }

        /// <summary> Returns the normalized viewport coordinates on the 2D plane (0-1, 0-1) </summary>
        public static double[] WorldToNormalizedViewPortCoordinate(this vtkRenderer renderer, double worldX = 0, double worldY = 0, double worldZ = 0)
        {
            var startScreenCoords = renderer.WorldToScreenCoordinate(worldX, worldY, worldZ);
            int[] windowSize = renderer.GetSize();
            return new[] { startScreenCoords[0] / (double)windowSize[0], startScreenCoords[1] / (double)windowSize[1] };
        }
        
        /// <summary> The raw world extents of the currently viewable area in the renderer. </summary>
        public static CartesianBounds<double> GetCurrentWorldExtents(this vtkRenderer renderer)
        {
            int[] screenSize = renderer.GetRenderWindow().GetSize();
            double[] minPt = renderer.ScreenToWorldCoordinate(0, 0);
            //double[] maxPt = renderer.ScreenToWorldCoordinate(screenSize[0] - 1, screenSize[1] - 1);
            double[] maxPt = renderer.ScreenToWorldCoordinate(screenSize[0], screenSize[1]);
            return new CartesianBounds<double>(minPt[0], maxPt[0], minPt[1], maxPt[1], minPt[2], maxPt[2]);
        }

        /// <summary> Calculates the number of world units per screen pixel. </summary>
        public static CartesianPoint WorldUnitsPerPixel(this vtkRenderer renderer)
        {
            double[] pt1 = renderer.ScreenToWorldCoordinate(1, 1, 0d, 1.0D);
            double[] pt2 = renderer.ScreenToWorldCoordinate(2, 2, 0d, 1.0D);

            double x = Math.Abs(pt1[0] - pt2[0]);
            double y = Math.Abs(pt1[1] - pt2[1]);
            return new CartesianPoint(x, y);
        }

        /// <summary> Gets the C# typed keys including any modifier keys (ctrl, shift, alt) that vtk can handle. Keys values obtained from: http://msdn.microsoft.com/en-us/library/system.windows.forms.keys(v=vs.110).aspx</summary>
        public static Keys GetKeyTyped(this vtkRenderWindowInteractor interactor)
        {
            string lastPressedKey = interactor.GetKeySym();

            //Numbers are a special case, and need to be setup as "D1" from "1"
            int intEquiv;
            if (int.TryParse(lastPressedKey, out intEquiv))
            {
                lastPressedKey = "D" + intEquiv;
            }

            //Try to parse the key by name. Works for basic letters and some others, but if not we'll have to do a manual lookup.
            Keys lastKey = Keys.None;

            switch (lastPressedKey)
            {
                case "Control_L": //Unfortunately VTK cannot differentiate between control L and control R, both come out as control L.
                    lastKey = Keys.ControlKey; break;
                case "Shift_L": //Unfortunately VTK cannot differentiate between shift L and shift R, both come out as Shift L.
                    lastKey = Keys.ShiftKey; break;
                case "Alt_L": //Unfortunately VTK cannot differentiate between alt L and alt R, both come out as alt L.
                    lastKey = Keys.Menu; break;
                case "Caps_Lock":
                    lastKey = Keys.CapsLock; break;
                case "Scroll_Lock":
                    lastKey = Keys.Scroll; break;
                case "quoteleft":
                    lastKey = Keys.Oemtilde; break;
                case "quoteright":
                case "quotedbl":
                    lastKey = Keys.OemQuotes; break;
                case "BackSpace":
                    lastKey = Keys.Back; break;
                case "plus":
                case "equal":
                    lastKey = Keys.Oemplus; break;
                case "underscore":
                case "minus":
                    lastKey = Keys.OemMinus; break;
                case "bracketright":
                case "parenright":
                    lastKey = Keys.OemCloseBrackets; break;
                case "parenleft":
                case "bracketleft":
                    lastKey = Keys.OemOpenBrackets; break;
                case "semicolon":
                case "colon":
                    lastKey = Keys.OemSemicolon; break;
                case "slash":
                case "question": //same key, different shift modifier.
                    lastKey = Keys.OemQuestion; break;
                case "backslash":
                    lastKey = Keys.OemBackslash; break;
                case "period":
                case "greater":
                    lastKey = Keys.OemPeriod; break;
                case "comma":
                case "less":
                    lastKey = Keys.Oemcomma; break;
                case "Win_L":
                    lastKey = Keys.LWin; break;
                case "App":
                    lastKey = Keys.ProcessKey; break;
                case "asterisk":
                    lastKey = Keys.Multiply; break;
                case "Next":
                    lastKey = Keys.PageDown; break;
                case "Prior":
                    lastKey = Keys.PageUp; break;
            }

            bool keyFound = lastKey != Keys.None;
            if (!keyFound)
            {
                //Works for F keys
                Enum.TryParse(lastPressedKey, true, out lastKey);
            }

            //Add modifier keys (Alt but not altKey)
            if (interactor.GetShiftKey() != 0)
                lastKey |= Keys.Shift;
            if (interactor.GetControlKey() != 0)
                lastKey |= Keys.Control;
            if (interactor.GetAltKey() != 0)
                lastKey |= Keys.Alt;

            return lastKey;
        }

        /// <summary> Creates text actors for each string contained in the list so it can be placed in a render window, like for Features or something. By default the actors use World Coordinate system. </summary>
        public static List<vtkTextActor> CreateTextActors(this List<String> strings)
        {
            List<vtkTextActor> returnList = new List<vtkTextActor>();

            //Set up the list of feature actors using the same values stored in the displayProperties.
            strings.ForEach(featureName =>
            {
                vtkTextActor textActor = new vtkTextActor();
                textActor.SetInput(featureName);
                textActor.GetTextProperty().SetItalic(0);
                textActor.GetTextProperty().SetBold(0);
                textActor.GetTextProperty().SetFontSize(12);
                textActor.GetTextProperty().SetShadow(0);
                textActor.GetTextProperty().SetColor(Color.Black);
                textActor.GetTextProperty().SetVerticalJustificationToCentered();
                textActor.PickableOff();
                textActor.GetPositionCoordinate().SetCoordinateSystemToWorld();
                returnList.Add(textActor);
            });

            return returnList;
        }
    }
}

