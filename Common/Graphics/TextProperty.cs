#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

using System;
using System.ComponentModel;
using System.Drawing;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;

namespace QuestIntegrity.Graphics
{

    /// <summary>
    /// Class to encapsulate vtk text properties
    /// </summary>
    [Serializable]
    public class TextProperty : INotifyPropertyChanged
    {

        #region Private fields
        private double _opacity;
        private int _shadowOffsetX;
        private int _shadowOffsetY;
        private Color _color;
        private int _fontSize;
        private bool _bold;
        private bool _italics;
        private bool _shadow;
        #endregion

        #region Constructor

        public TextProperty()
        {
            Color = Color.Black;
            FontSize = 12;
            Bold = false;
            Italics = false;
            Shadow = false;
            _opacity = 1.0d;
            _shadowOffsetX = 0;
            _shadowOffsetY = 0;
        }

        #endregion

        #region Public Properties
        public string ParentPropertyName
        {
            get;
            set;
        }

        // [XmlIgnoreAttribute()]
        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    NotifyPropertyChanged(GetFullName("Color"));
                }
            }
        }

        public int FontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                if (_fontSize != value)
                {
                    _fontSize = value;
                    NotifyPropertyChanged(GetFullName("FontSize"));
                }
            }
        }

        public bool Bold
        {
            get
            {
                return _bold;
            }
            set
            {
                if (_bold != value)
                {
                    _bold = value;
                    NotifyPropertyChanged(GetFullName("Bold"));
                }
            }
        }

        public bool Italics
        {
            get
            {
                return _italics;
            }
            set
            {
                if (_italics != value)
                {
                    _italics = value;
                    NotifyPropertyChanged(GetFullName("Italics"));
                }
            }
        }

        public bool Shadow
        {
            get
            {
                return _shadow;
            }
            set
            {
                if (_shadow != value)
                {
                    _shadow = value;
                    NotifyPropertyChanged(GetFullName("Shadow"));
                }
            }
        }

        public double Opacity
        {
            get
            {
                return _opacity;
            }
            set
            {
                double opacity = value.Constrain(0d, 1d);
                if (_opacity != opacity)
                {
                    _opacity = opacity;
                    NotifyPropertyChanged(GetFullName("Opacity"));
                }
            }
        }

        private string GetFullName(string p)
        {
            string propertyName = "";
            if (ParentPropertyName != null)
            {
                propertyName = ParentPropertyName + ".";
            }
            propertyName += p;
            return propertyName;
        }

        #endregion

        #region Public Methods
        public void SetShadowOffset(int offsetX, int offsetY)
        {
            _shadowOffsetX = offsetX;
            _shadowOffsetY = offsetY;
        }
        public void AssignToVtkTextProperty(vtkTextProperty textProperty)
        {
            textProperty.SetBold(Bold.ToInt());
            textProperty.SetItalic(Italics.ToInt());
            textProperty.SetColor(this.Color);
            textProperty.SetShadow(Shadow.ToInt());
            textProperty.SetOpacity(_opacity);
            textProperty.SetShadowOffset(_shadowOffsetX, _shadowOffsetY);
        }
        #endregion

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion
    }
}
