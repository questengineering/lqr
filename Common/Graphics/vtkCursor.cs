#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 8/6/2008 1:00:21 PM
// Created by:   j.rowe
//
#endregion

namespace Kitware.VTK
{
    public enum VtkCursor
    {
        VtkCursorDefault = 0,
        VtkCursorArrow = 1,
        VtkCursorSizene = 2,
        VtkCursorSizenw = 3,
        VtkCursorSizesw = 4,
        VtkCursorSizese = 5,
        VtkCursorSizens = 6, 
        VtkCursorSizewe = 7,
        VtkCursorSizeall = 8,
        VtkCursorHand = 9,
        VtkCursorCrosshair = 10
    }

}
