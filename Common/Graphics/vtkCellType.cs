﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/3/2008 10:31:49 AM
// Created by:   J.Rowe
//
#endregion

namespace Kitware.VTK
{
    public enum VtkCellType : int
    {
        VtkEmptyCell = 0,
        VtkVertex =  1, 
        VtkPolyVertex =  2,
        VtkLine =  3,
        VtkPolyLine =  4,
        VtkTriangle =  5,
        VtkTriangleStrip =  6,
        VtkPolygon =  7,
        VtkPixel =  8,
        VtkQuad =  9,
        VtkTetra =  10, 
        VtkVoxel =  11,
        VtkHexahedron = 12,
        VtkWedge =  13,
        VtkPyramid =  14, 
        VtkParametricCurve =  51,
        VtkParametricSurface =  52 
    }
}