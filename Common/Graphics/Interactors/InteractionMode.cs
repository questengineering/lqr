using System;
using QuestIntegrity.Core.Extensions.Strings;

namespace QuestIntegrity.Graphics
{
    /// <summary> Static members that determine the type of Interaction being performed. Class-based so it can be added to outside of the base library. </summary>
    public class InteractionMode
    {
        /// <summary> Zooming adjusts the scale in 1 dimension, changing the aspect ratio  </summary>
        public static InteractionMode None = new InteractionMode("None");
        
        protected InteractionMode(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name.IsNullOrEmpty() ? @"Unknown" : "InteractionMode: " + Name;
        }

        public string Name { get; set; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(Object obj)
        {
            if (obj == null) return false; // fails existance
            ZoomMode typedObj = obj as ZoomMode;
            return typedObj != null && (string.Equals(Name, typedObj.Name));
        }

        protected bool Equals(ZoomMode other)
        {
            return string.Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(InteractionMode a, InteractionMode b)
        {
            if (ReferenceEquals(a, b)) return true; //Means the same instance, or both null.
            if ((object)a == null || (object)b == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(a.Name, b.Name);
        }

        public static bool operator !=(InteractionMode a, InteractionMode b)
        {
            return !(a == b);
        }

        #endregion Equality Comparing

    }

    /// <summary> Static members that determine the type of zooming being performed. Class-based so it can be added to outside of the base library. </summary>
    public class ZoomMode
    {
        /// <summary> Zooming adjusts the scale in 1 dimension, changing the aspect ratio  </summary>
        public static ZoomMode Stretch = new ZoomMode("Stretch");
        public static ZoomMode RubberBand = new ZoomMode("Rubber Band");
        public static ZoomMode Proportional = new ZoomMode("Proportional");

        protected ZoomMode(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name.IsNullOrEmpty() ? @"Unknown" : "ZoomMode: " + Name;
        }

        public string Name { get; set; }

        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(Object obj)
        {
            if (obj == null) return false; // fails existance
            ZoomMode typedObj = obj as ZoomMode;
            return typedObj != null && (string.Equals(Name, typedObj.Name));
        }

        protected bool Equals(ZoomMode other)
        {
            return string.Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(ZoomMode a, ZoomMode b)
        {
            if (ReferenceEquals(a, b)) return true; //Means the same instance, or both null.
            if ((object)a == null || (object)b == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(a.Name, b.Name);
        }

        public static bool operator !=(ZoomMode a, ZoomMode b)
        {
            return !(a == b);
        }

        #endregion Equality Comparing

    }
}