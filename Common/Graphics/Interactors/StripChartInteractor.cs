﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.Controls;
using QuestIntegrity.Graphics.EventArgs.PickPositions;

namespace QuestIntegrity.Graphics.Interactors
{
    public class StripChartInteractor : BaseInteractor, IDisposable
    {
       
        #region Private and Protected fields

        private const Keys ProportionalZoomKey = Keys.Shift;

        private const double DefaultZoomPercent = 15.0;
        //Crosshair cursor window position, not in global coords.
        private double[] _crossHairWorldPoint = new double[3];
        private bool _crosshairMoving;
        private vtkCursor2D _crossHairCursor;
        private vtkActor2D _cursorActor;
        private bool _panning;
        
        #endregion

        #region Public Properties

        protected new StripChartGraphicsDisplayWindow GraphicsWindow
        {
            get { return base.GraphicsWindow as StripChartGraphicsDisplayWindow; }
            set { base.GraphicsWindow = value; }
        }

        /// <summary> Gets the last crosshair world position </summary>
        public double[] CrosshairWorldPoint
        {
            get { return _crossHairWorldPoint; }
        }

        public int[] CrosshairScreenPoint
        {
            get { return Renderer.WorldToScreenCoordinate(_crossHairWorldPoint[0], _crossHairWorldPoint[1], _crossHairWorldPoint[1]); }
        }

        #endregion

        #region Constructor

        public StripChartInteractor(StripChartGraphicsDisplayWindow graphicsWindow) : base(graphicsWindow)
        {
            GraphicsWindow = graphicsWindow;
            RegisterInteractionEvents();
            InitializeCrosshairs();
        }

        #endregion

        #region Public Methods
        
        /// <summary> Pans the view Vertically. </summary>
        public void PanVertically(double worldPointsToPan)
        {
            //Move the camera up or down the Y direction
            double[] position = GraphicsWindow.Camera.GetPosition();
            position[1] += worldPointsToPan;
            GraphicsWindow.Camera.SetPosition(position[0], position[1], position[2]);

            position = GraphicsWindow.Camera.GetFocalPoint();
            position[1] += worldPointsToPan;
            GraphicsWindow.Camera.SetFocalPoint(position[0], position[1], position[2]);

            //Set crosshairs and re-render
            SetCrosshairs(CrosshairWorldPoint[0], CrosshairWorldPoint[1]);
            GraphicsWindow.Render();
        }

        /// <summary> Zooms out the default zoom percent. </summary>
        public void ZoomOut()
        {
            Zoom(-DefaultZoomPercent);
            SetCrosshairs(CrosshairWorldPoint[0], CrosshairWorldPoint[1]);
        }

        /// <summary> Zooms in the default zoom percent. </summary>
        public void ZoomIn()
        {
            Zoom(DefaultZoomPercent);
            SetCrosshairs(_crossHairWorldPoint[0], _crossHairWorldPoint[1]);
        }

        /// <summary>  Zooms the specified percent. </summary>
        public void Zoom(double percent)
        {
            //Zoom according to the current interaction mode. Proportional is a simple dolly effect. ZoomStretch locks an axis and requires actor scaling.         
            
            if (CurrentZoomMode == ZoomMode.Proportional)
            {
                double newScale = GraphicsWindow.Camera.GetParallelScale() / (1 + (percent / 100));
                GraphicsWindow.Camera.SetParallelScale(newScale);
            }
            else if (CurrentZoomMode == ZoomMode.Stretch)
            {
                //Scale the grid sections to be that much taller/shorter.
                double scaleDelta = 1 + percent / 100;
                GraphicsWindow.ScaleAllActors(new CartesianPoint(1, scaleDelta));
                
                //Move the camera to stay at the same world point.
                double[] oldPosition = GraphicsWindow.Camera.GetPosition();
                double newY = oldPosition[1] * scaleDelta;
                GraphicsWindow.Camera.SetPosition(oldPosition[0], newY, 1);
                GraphicsWindow.Camera.SetFocalPoint(oldPosition[0], newY, 0);

                _crossHairWorldPoint[1] *= scaleDelta;
                SetCrosshairs(CrosshairWorldPoint[0], CrosshairWorldPoint[1]);
            }

            //Log.DebugFormat(@"Parallel Scale = {3:N2}; Actor Scale = ({0:N2}, {1:N2}, {2:N2})", GraphicsWindow.DisplayScale.X, GraphicsWindow.DisplayScale.Y, GraphicsWindow.DisplayScale.Z, GraphicsWindow.Camera.GetParallelScale());
            GraphicsWindow.HideDataTip();
            GraphicsWindow.Render();
        }

        #endregion

        #region Public Events

        public new event EventHandler<WorldPositionEventArgs> LeftMouseDown;
        public new event EventHandler<WorldPositionEventArgs> LeftMouseDragging;

        public new event EventHandler<WorldPositionEventArgs> RightMouseDown;

        #endregion

        #region Private and Protected Methods

        protected new void RegisterInteractionEvents()
        {
            //common events
            base.RegisterInteractionEvents();

            //register 2D-specific events
            //Define and start with an Image style interactor, but override the events for our custom handling
            Style = new vtkInteractorStyleImage();
            Style.SetInteractor(Interactor);
            Interactor.SetInteractorStyle(Style);
            Interactor.GetInteractorStyle().EnabledOff(); //turn it off so the internal events do not fire.  We are overloading all.

            Style.LeftButtonPressEvt += InteractorStyle_LeftButtonPressEvt;
            Style.LeftButtonReleaseEvt += InteractorStyle_LeftButtonReleaseEvt;

            Style.RightButtonPressEvt += InteractorStyle_RightButtonPressEvt;
            Style.RightButtonReleaseEvt += InteractorStyle_RightButtonReleaseEvt;

            Style.MouseMoveEvt += InteractorStyle_MouseMoveEvt;

            Style.MouseWheelBackwardEvt += InteractorStyle_MouseWheelBackwardEvt;
            Style.MouseWheelForwardEvt += InteractorStyle_MouseWheelForwardEvt;

        }

        private void InteractorStyle_MouseWheelForwardEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;
            
            Zoom(DefaultZoomPercent);
        }

        private void InteractorStyle_MouseWheelBackwardEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;

            Zoom(-DefaultZoomPercent);
        }

        private void InteractorStyle_LeftButtonPressEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            StartCrosshairs();
            if (LeftMouseDown != null) LeftMouseDown(this, new WorldPositionEventArgs(new PointF((float)_crossHairWorldPoint[0], (float)_crossHairWorldPoint[1])));
        }

        private void DragCursorPoint()
        {
            if (LeftMouseDragging != null) LeftMouseDragging(this, new WorldPositionEventArgs(new PointF((float)_crossHairWorldPoint[0], (float)_crossHairWorldPoint[1])));
        }

        private void InteractorStyle_LeftButtonReleaseEvt(vtkObject sender,vtkObjectEventArgs e)
        {
            //Set mouse position in world coordinates.
            _crossHairWorldPoint = Renderer.ScreenToWorldCoordinate(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1]);
            
            GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorDefault);
            EndCrosshairs();
        }

        private void InteractorStyle_RightButtonPressEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            RightMouseDown?.Invoke(this, new WorldPositionEventArgs(new PointF((float)_crossHairWorldPoint[0], (float)_crossHairWorldPoint[1])));
            StartPan();
        }

        private void InteractorStyle_RightButtonReleaseEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            if (_panning) EndPan();
        }

        private void InteractorStyle_MouseMoveEvt(vtkObject sender, vtkObjectEventArgs e)
        {
            // Set mouse position
            var previousCrossHairPoint = CrosshairWorldPoint;
            _crossHairWorldPoint = Renderer.ScreenToWorldCoordinate(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1]);
            
            //Don't re-fire if it hasn't really moved
            if (previousCrossHairPoint[0] == _crossHairWorldPoint[0] && previousCrossHairPoint[0] == _crossHairWorldPoint[1]) return;
            if (_panning)
                MousePan();
            else if (_crosshairMoving)
            {
                RedrawCrosshairs();
                DragCursorPoint();
            }
        }
        
        private void InitializeCrosshairs()
        {
            //Render the cursor using a pipeline:  Cursor2d -> Mapper2d -> Actor2d
            //Define the cursor
            int crossHairPixelsX = Screen.GetWorkingArea(GraphicsWindow).Width;
            int crossHairPixelsY = Screen.GetWorkingArea(GraphicsWindow).Height;
            _crossHairCursor = new vtkCursor2D();
            _crossHairCursor.PointOn();
            _crossHairCursor.AxesOn();
            _crossHairCursor.SetRadius(4);
            _crossHairCursor.SetFocalPoint(0, 0, 0);
            _crossHairCursor.SetModelBounds(-crossHairPixelsX, crossHairPixelsX, -crossHairPixelsY, crossHairPixelsY, 0, 0);
            
            //Set the Mapper
            vtkPolyDataMapper2D cursorMapper = new vtkPolyDataMapper2D();
            cursorMapper.SetInputConnection(_crossHairCursor.GetOutputPort());

            //Set the actor
            _cursorActor = new vtkActor2D();
            _cursorActor.SetMapper(cursorMapper);
            _cursorActor.GetProperty().SetColor(Color.Black);
            _cursorActor.PickableOff();
            _cursorActor.SetVisibility(1);
            GraphicsWindow.Renderer.AddActor2D(_cursorActor);
        }

        private void SetCrosshairs(int screenX, int screenY)
        {
            //Set and draw the crosshairs at the specified screen position
            _crossHairCursor.SetFocalPoint(screenX, screenY, 0.0);
            Interactor.Render();
        }

        /// <summary> Sets the crosshairs at the specified world coordinates and redraws them. </summary>
        public void SetCrosshairs(double worldX, double worldY)
        {
            int[] screenCoords = Renderer.WorldToScreenCoordinate(worldX, worldY, 0);
            SetCrosshairs(screenCoords[0], screenCoords[1]);
        }

        private void RedrawCrosshairs()
        {
            //Redraw the crosshairs at the event position.  Also calculate this position in true world coordinates.
            SetCrosshairs(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1]);
        }

        private void StartPan()
        {
            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;
            if (CurrentZoomMode == ZoomMode.Stretch)
                GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorSizens);
            else
                GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorSizeall);

            _panning = true;
        }

        private void EndPan()
        {
            GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorDefault);
            RedrawCrosshairs();
            _panning = false;
        }

        private void MousePan()
        {
            if (Renderer == null) return;

            //NOTE:  code converted from vtkInteractorStyleTrackBallCamera::Pan()
            double[] motionVector = new double[3];

            // Calculate the focal depth since we'll be using it a lot
            double[] viewFocus = GraphicsWindow.Camera.GetFocalPoint();
            Renderer.SetWorldPoint(viewFocus[0], viewFocus[1], viewFocus[2], 0);
            Renderer.WorldToDisplay();
            viewFocus = Renderer.GetDisplayPoint();
            double focalDepth = viewFocus[2];

            Renderer.SetDisplayPoint(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1], focalDepth);
            Renderer.DisplayToWorld();
            double[] newPickPoint = Renderer.GetWorldPoint();
            newPickPoint[2] = 0;

            // Has to recalculate old mouse point since the viewport has moved,
            // so can't move it outside the loop
            Renderer.SetDisplayPoint(Interactor.GetLastEventPosition()[0], Interactor.GetLastEventPosition()[1], focalDepth);
            Renderer.DisplayToWorld();
            double[] oldPickPoint = Renderer.GetWorldPoint();
            oldPickPoint[2] = 0;

            // Camera motion is reversed
            if (CurrentZoomMode == ZoomMode.Stretch)
            {
                motionVector[0] = oldPickPoint[0] - newPickPoint[0];
                motionVector[1] = oldPickPoint[1] - newPickPoint[1];
                motionVector[2] = oldPickPoint[2] - newPickPoint[2];

                // override and constrain the axis for strip chart panning
                const int axisToWipe = 0;
                motionVector[axisToWipe] = 0;
            }
            else
            {
                motionVector[0] = oldPickPoint[0] - newPickPoint[0];
                motionVector[1] = oldPickPoint[1] - newPickPoint[1];
                motionVector[2] = oldPickPoint[2] - newPickPoint[2];
            }

            viewFocus = GraphicsWindow.Camera.GetFocalPoint();
            double[] viewPoint = GraphicsWindow.Camera.GetPosition();
            GraphicsWindow.Camera.SetFocalPoint(motionVector[0] + viewFocus[0], motionVector[1] + viewFocus[1], motionVector[2] + viewFocus[2]);

            GraphicsWindow.Camera.SetPosition(motionVector[0] + viewPoint[0], motionVector[1] + viewPoint[1], motionVector[2] + viewPoint[2]);

            //track the crosshairs
            RedrawCrosshairs();
            Interactor.Render();

            GraphicsWindow.ShowDataTip();
        }

        private void StartCrosshairs()
        {
            CursorVisible = false;
            _crosshairMoving = true;
            RedrawCrosshairs();
        }

        private void EndCrosshairs()
        {
            CursorVisible = true;
            _crosshairMoving = false;
        }

        #endregion

        #region IDispose Pattern Implementation

        private bool _disposed;
        
        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (_disposed) return;
            // Managed Resources
            if (disposing)
            {
                // Dispose managed resources.
                if (_cursorActor != null) _cursorActor.Dispose();
                if (_crossHairCursor != null) _crossHairCursor.Dispose();
            }

            //Set Flag
            _disposed = true;
        }

        #endregion
    }
}