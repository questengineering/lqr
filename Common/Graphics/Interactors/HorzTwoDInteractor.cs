﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Extensions.Strings;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.Controls;
using QuestIntegrity.Graphics.EventArgs;
using QuestIntegrity.Graphics.EventArgs.PickPositions;

namespace QuestIntegrity.Graphics.Interactors
{
    public class HorzTwoDInteractor : BaseInteractor, IDisposable
    {
        #region Private and Protected fields

        private const int MinZoomBoxSize = 5; //size in pixels of smallest rubberband zoom box allowed

        private const Keys ProportionalZoomKey = Keys.Shift;
        private const double DefaultZoomPercent = 5.0;

        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is pressed </summary>
        private readonly double[] _leftMouseDownPosition = new double[2];
        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is released </summary>
        private readonly double[] _leftMouseUpPosition = new double[2];
        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is pressed, moved, or released </summary>
        private readonly double[] _leftMouseCurrentPosition = new double[2];
        /// <summary> Flag for if the left mouse button is depressed </summary>
        bool _leftMouseDown;
        
        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is pressed </summary>
        private readonly double[] _rightMouseDownPosition = new double[2];
        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is released </summary>
        private readonly double[] _rightMouseUpPosition = new double[2];
        /// <summary> Mouse World Coordinate Updated every time the mouseLeft is pressed, moved, or released </summary>
        private readonly double[] _rightMouseCurrentPosition = new double[2];
        /// <summary> Flag for if the right mouse button is depressed </summary>
        bool _rightMouseDown;

        /// <summary> start point of the rubber band in World Coordinates  </summary>
        private readonly double[] _rubberBandStartWorlPoint = new double[2];
        /// <summary> end point of the rubber band in World Coordinates  </summary>
        private readonly double[] _rubberBandDraggingWorlPoint = new double[2];
        /// <summary> end point of the rubber band in World Coordinates  </summary>
        private readonly double[] _rubberBandEndWorlPoint = new double[2];
        public bool RubberBanding;

        /// <summary> Determines how far the cursor has to move in rounded point units to be considered 'moved' </summary>
        public double CursorTolerance = .0001;

        /// <summary> Determines the minimum amount of X data that is visible on screen at once from Zoom operations. If it will go below MinXZoom, the zoom is cancelled. </summary>
        public double? MinXZoom = null;

        private Point _panStart;
        private Point _panEnd;
        private bool _panning;

        // The pixel array for the rubber band
        private readonly vtkUnsignedCharArray _pixelArray = new vtkUnsignedCharArray();

        /// <summary> Gets the last crosshair world, scaled position. </summary>
        public double[] CrosshairWorldScaledPoint = new double[2];
        /// <summary> Gets the calculated screen point based off of the crosshair's scaled world point. </summary>
        public int[] CrosshairScreenPoint { get { return Renderer.WorldToScreenCoordinate(CrosshairWorldScaledPoint); } }
        private bool _movingCrosshair;
        private vtkCursor2D _crossHairCursor;
        private vtkActor2D _cursorActor;
        private vtkTextActor _xRightLocationText;
        private vtkTextActor _xLeftLocationText;
        
        #endregion

        #region Public Properties

        /// <summary> Returns the raw start and end coordinates for the most recent rubber band. </summary>
        public CartesianBounds<double> GetRawRubberBandBounds()
        {
            return new CartesianBounds<double>(_rubberBandStartWorlPoint[0], _rubberBandEndWorlPoint[0], _rubberBandStartWorlPoint[1], _rubberBandEndWorlPoint[1], 0, 0);
        }

        protected new ImageGraphicsDisplayWindow GraphicsWindow
        {
            get { return base.GraphicsWindow as ImageGraphicsDisplayWindow; }
            set { base.GraphicsWindow = value; }
        }

        #endregion

        #region Constructor and Initialization

        public HorzTwoDInteractor(ImageGraphicsDisplayWindow graphicsWindow): base(graphicsWindow)
        {
            RegisterInteractionEvents();
            InitializeCrosshairs();
        }

        private void InitializeCrosshairs()
        {
            //Render the cursor using a pipeline:  Cursor2d -> Mapper2d -> Actor2d
            //Define the cursor
            int crossHairPixelsX = Screen.GetWorkingArea(GraphicsWindow).Width;
            int crossHairPixelsY = Screen.GetWorkingArea(GraphicsWindow).Height;
            _crossHairCursor = new vtkCursor2D();
            _crossHairCursor.AllOff();
            _crossHairCursor.AxesOn();
            _crossHairCursor.SetRadius(4);
            _crossHairCursor.SetFocalPoint(0, 0, 0);
            _crossHairCursor.SetModelBounds(-crossHairPixelsX, crossHairPixelsX, -crossHairPixelsY, crossHairPixelsY, 0, 0);

            //Set the Mapper
            vtkPolyDataMapper2D cursorMapper = new vtkPolyDataMapper2D();
            cursorMapper.SetInputConnection(_crossHairCursor.GetOutputPort());

            //Set the actor
            _cursorActor = new vtkActor2D();
            _cursorActor.SetMapper(cursorMapper);
            _cursorActor.GetProperty().SetColor(Color.Black);
            _cursorActor.PickableOff();
            GraphicsWindow.Renderer.AddActor2D(_cursorActor);

            //X right and left location text       
            _xRightLocationText = new vtkTextActor();
            _xRightLocationText.SetDisplayPosition(0, 0);
            _xRightLocationText.SetTextScaleModeToNone();
            _xRightLocationText.PickableOff();
            _xRightLocationText.SetInput("");
            _xRightLocationText.GetTextProperty().SetColor(0, 0, 0);
            _xRightLocationText.GetTextProperty().SetJustificationToLeft();

            _xLeftLocationText = new vtkTextActor();
            _xLeftLocationText.SetDisplayPosition(0, 0);
            _xLeftLocationText.SetTextScaleModeToNone();
            _xLeftLocationText.PickableOff();
            _xLeftLocationText.SetInput("");
            _xLeftLocationText.GetTextProperty().SetColor(0, 0, 0);
            _xLeftLocationText.GetTextProperty().SetJustificationToRight();

            GraphicsWindow.Renderer.AddActor2D(_xRightLocationText);
            GraphicsWindow.Renderer.AddActor2D(_xLeftLocationText);
        }

        protected new void RegisterInteractionEvents()
        {
            //common events need to be disabled becuase the 2D view's insteractor style is setup differently.
            DeregisterAllUserEvents();

            //register 2D-specific events
            //Define and start with an Image style interactor, but override the events for our custom handling
            Style = new vtkInteractorStyleImage();
            Style.SetInteractor(Interactor);
            Interactor.SetInteractorStyle(Style);
            Interactor.GetInteractorStyle().EnabledOff(); //turn it off so the internal events do not fire.  We are overloading all.

            Style.LeftButtonPressEvt += OnLeftMouseDown;
            Style.LeftButtonReleaseEvt += OnLeftMouseUp;

            Style.RightButtonPressEvt += OnRightMouseDown;
            Style.RightButtonReleaseEvt += OnRightMouseUp;

            Style.MouseMoveEvt += OnMouseMoving;

            Style.MouseWheelBackwardEvt += OnMouseWheelDown;
            Style.MouseWheelForwardEvt += OnMouseWheelUp;

            Style.KeyPressEvt += OnKeyDown;
            Style.KeyReleaseEvt += OnKeyUp;
        }

        public event ViewExtentsChangedEventHandler ImageViewExtentsChanged;
        #endregion Constructor and Initialization

        #region Public Methods

        public void SetCrosshairs(double[] scaledWorldPoint, string xAxisText, string yAxisText, bool render)
        {
            SetCrosshairs(scaledWorldPoint, false); //render the interactor only at the last step, not twice.
            SetCrosshairText(xAxisText, yAxisText, render); 
        }

        public void SetCrosshairs(double[] scaledWorldPoint, bool render)
        {
            Array.Copy(scaledWorldPoint, CrosshairWorldScaledPoint, 2);
             
            //Set and draw the crosshairs
            int[] screenCoords = CrosshairScreenPoint;
            _crossHairCursor.SetFocalPoint(screenCoords[0], screenCoords[1], 0.0);
            SetCrosshairTextLocation();
            if (render) Interactor.Render();
        }

        #region Panning and Zooming 

        public void UndoPanZoom()
        {
            PanZoom(LastZoomCenter, 1.0 / LastZoomRatio);
        }

        /// <summary> Pans and zooms to new window defined by pt1 and pt2 </summary>
        public void PanZoom(CartesianPoint worldPt1, CartesianPoint worldPt2, int[] startScreenPt, int[] endScreenPt)
        {
            //A little validation to make sure area is not too small
            int deltaX = Math.Abs(endScreenPt[0] - startScreenPt[0]);
            int deltaY = Math.Abs(endScreenPt[0] - startScreenPt[0]);
            if (deltaX < MinZoomBoxSize && deltaY < MinZoomBoxSize) return; 

            //Determine the zoom ratio, zooming proportionally to the smallest screen dimension
            int[] screenSize = GraphicsWindow.RenderWindow.GetSize();
            double xR = (double)screenSize[0] / Math.Abs(endScreenPt[0] - startScreenPt[0]);
            double yR = (double)screenSize[1] / Math.Abs(startScreenPt[1] - endScreenPt[1]);
            double zoomRatio = Math.Min(xR, yR);
            PanZoom(worldPt1, worldPt2, zoomRatio);
        }

        /// <summary>
        ///     Pans and zooms to new window defined by pt1 and pt2
        /// </summary>
        /// <param name="worldZoomBoxPt1">The zoom boxes's first point.</param>
        /// <param name="worldZoomBoxPt2">The zoom boxes's second point.</param>
        /// <param name="zoomRatio">The zoom ratio.  &gt;1 is zoom in. &lt;1 is zoom out</param>
        public void PanZoom(CartesianPoint worldZoomBoxPt1, CartesianPoint worldZoomBoxPt2, double zoomRatio)
        {
            CartesianPoint ptCenter = new CartesianPoint
                                      {
                                          X = (worldZoomBoxPt1.X + worldZoomBoxPt2.X) / 2.0d,
                                          Y = (worldZoomBoxPt1.Y + worldZoomBoxPt2.Y) / 2.0d
                                      };
            PanZoom(ptCenter, zoomRatio);
        }

        /// <summary>
        ///     Pans and zooms to new window defined by pt1 and pt2
        /// </summary>
        /// <param name="cameraCenter"></param>
        /// <param name="zoomRatio">The zoom ratio.  &gt;1 is zoom in. &lt;1 is zoom out</param>
        public void PanZoom(CartesianPoint cameraCenter, double zoomRatio)
        {
            // Get the center in world coordinates
            vtkCamera camera = GraphicsWindow.Camera;

            //Store the old coordinate so that we can "undo" the zoom
            var gScale = GraphicsWindow.GridScale;
            LastZoomExtents = new CartesianBounds<double>(Renderer.GetCurrentWorldExtents())
                               {
                                   YMin = LastZoomExtents.YMin / gScale.Y,
                                   YMax = LastZoomExtents.YMax / gScale.Y,
                               };
            LastZoomCenter = new CartesianPoint(camera.GetPosition());
            LastZoomRatio = zoomRatio;

            // Re-center the Camera and Focal point
            camera.SetPosition(cameraCenter.X, cameraCenter.Y, 1);
            camera.SetFocalPoint(cameraCenter.X, cameraCenter.Y, 0);

            // Zoom in/out            
            camera.Zoom(zoomRatio);

            //Move the crosshairs            
            RedrawCrosshairs();
            GraphicsWindow.Render();

            // Fire the view extents changed event
            ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(GraphicsWindow.GetXyExtents()));
        }

        /// <summary> Pans the view horizontally but does not render. </summary>
        /// <param name="incrementToPan">The increment to pan.</param>
        public void PanHorizontally(double incrementToPan)
        {
            //Move the camera up or down the X direction
            double[] position = GraphicsWindow.Camera.GetPosition();
            position[0] += incrementToPan;
            GraphicsWindow.Camera.SetPosition(position[0], position[1], position[2]);

            position = GraphicsWindow.Camera.GetFocalPoint();
            position[0] += incrementToPan;
            GraphicsWindow.Camera.SetFocalPoint(position[0], position[1], position[2]);

            //Set crosshairs and re-render
            RedrawCrosshairs();
            ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(GraphicsWindow.GetXyExtents()));
        }

        /// <summary> Zooms out the default zoom percent. </summary>
        public void ZoomOut()
        {
            bool zoomed = Zoom(-DefaultZoomPercent);
            if (zoomed) ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(GraphicsWindow.GetXyExtents()));
        }

        /// <summary> Zooms in the default zoom percent. </summary>
        public void ZoomIn()
        {
            bool zoomed = Zoom(DefaultZoomPercent);
            if (zoomed) ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(GraphicsWindow.GetXyExtents()));
        }

        /// <summary>  Zooms the specified percent but does not render. </summary>
        public bool Zoom(double percent)
        {
            var windowImageExtents = GraphicsWindow.GetXyExtents();
            double currentXVisible = (windowImageExtents.XMax - windowImageExtents.XMin);
            if (MinXZoom != null && currentXVisible / (1 + (percent / 100)) < MinXZoom) return false;

            if (CurrentZoomMode == ZoomMode.Proportional)
            {
                double newScale = GraphicsWindow.Camera.GetParallelScale() / (1 + (percent / 100));
                GraphicsWindow.Camera.SetParallelScale(newScale);
            }
            else if (CurrentZoomMode == ZoomMode.Stretch)
            {
                double scaleDelta = 1 / (1 + (percent / 100));
                double newScale = GraphicsWindow.Camera.GetParallelScale() * scaleDelta;
                double[] originalYExtents = { windowImageExtents.YMin, windowImageExtents.YMax };
                GraphicsWindow.Camera.SetParallelScale(newScale);
                var newExtents = GraphicsWindow.GetXyExtents();
                GraphicsWindow.ScaleActorsToFillWindow(newExtents.XMax - newExtents.XMin, originalYExtents[1] - originalYExtents[0]);
                //scale the pan axis dimension
                double[] curFocal = GraphicsWindow.Camera.GetFocalPoint();
                double[] curPosition = GraphicsWindow.Camera.GetPosition();
                //Zoom in/out first by adjusting the parallel scale
                GraphicsWindow.Camera.SetFocalPoint(curFocal[0], curFocal[1] * scaleDelta, curFocal[2]);
                GraphicsWindow.Camera.SetPosition(curPosition[0], curPosition[1] * scaleDelta, curPosition[2]);
                //shift the cursor as the screen position's Y Value will change if it's stretched or compressed.
                var worldpoint = CrosshairWorldScaledPoint;
                SetCrosshairs(new[] { worldpoint[0], worldpoint[1] * scaleDelta, 0 }, false);
            }

            RedrawCrosshairs();
            GraphicsWindow.ShowDataTip();
            return true;
        }

        #endregion Panning and Zooming

        /// <summary> Sets the crosshair text and optionally renders. </summary>
        public void SetCrosshairText(string xLeftText, string xRightText, bool render)
        {
            _xLeftLocationText.SetInput(xLeftText);
            _xRightLocationText.SetInput(xRightText);
            if (render) Interactor.Render();
        }

        public void ShowCursorText(bool show, bool render = true)
        {
            _xLeftLocationText.SetVisibility(show);
            _xRightLocationText.SetVisibility(show);
            if (render) Interactor.Render();
        }
        #endregion

        #region Private and Protected Methods

        /// <summary> indicates whether the Crosshairs cursor and text is visible. </summary>
        private void ToggleCrosshairsCursor(bool showCursor)
        {
            _cursorActor.SetVisibility(showCursor);
            if (!_xRightLocationText.GetInput().IsNullOrEmpty())
                _xRightLocationText.SetVisibility(showCursor);
            if (!_xLeftLocationText.GetInput().IsNullOrEmpty())
                _xLeftLocationText.SetVisibility(showCursor);
        }

        /// <summary> Triggered when the left mouse button is depressed on a point in screen coordinates. </summary>
        public new event EventHandler<WorldPositionEventArgs> LeftMouseDown;
        /// <summary> Triggered when the left mouse button is depressed and moving. </summary>
        public new event EventHandler<WorldPositionEventArgs> LeftMouseDragging;
        /// <summary> Triggered when the left mouse button is released. </summary>
        public new event EventHandler<WorldPositionEventArgs> LeftMouseUp;
        /// <summary> Triggered when the right mouse button is depressed on a point in screen coordinates. </summary>
        public new event EventHandler<WorldPositionEventArgs> RightMouseDown;
        /// <summary> Triggered when the right mouse button is depressed and moving. </summary>
        public new event EventHandler<WorldPositionEventArgs> RightMouseDragging;

        protected override void OnMouseWheelUp(vtkObject sender, vtkObjectEventArgs e)
        {
            if (!GraphicsWindow.AllowStretchZoom && !GraphicsWindow.AllowUnconstrainedZoom) return; //don't zoom if not allowed to.
            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey && GraphicsWindow.AllowUnconstrainedZoom
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;
            ZoomIn();
        }

        protected override void OnMouseWheelDown(vtkObject sender, vtkObjectEventArgs e)
        {
            if (!GraphicsWindow.AllowStretchZoom && !GraphicsWindow.AllowUnconstrainedZoom) return; //don't zoom if not allowed to.
            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey && GraphicsWindow.AllowUnconstrainedZoom
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;
            ZoomOut();
        }

        /// <summary> Left Mouse Down! </summary>
        protected override void OnLeftMouseDown(vtkObject sender, vtkObjectEventArgs e)
        {
            _leftMouseDown = true;
            var screenPosition = Interactor.GetEventPosition();
            var worldPosition = Renderer.ScreenToWorldCoordinate(screenPosition[0], screenPosition[1]);
            var roundedPosition = SnapWorldPointToNearestWholeUnscaledPoint(worldPosition, false);
            if (roundedPosition[0].IsWithinToleranceOf(_leftMouseCurrentPosition[0], CursorTolerance) && roundedPosition[1].IsWithinToleranceOf(_leftMouseCurrentPosition[1], CursorTolerance))
                return;
            _leftMouseDownPosition[0] = _leftMouseCurrentPosition[0] = roundedPosition[0];
            _leftMouseDownPosition[1] = _leftMouseCurrentPosition[1] = roundedPosition[1];
            
            // If shift or control key is down, do rubber band select
            if (Interactor.GetShiftKey() != 0 || Interactor.GetControlKey() != 0)
            {
                StartRubberBand(roundedPosition);
            }
            else
            {
                roundedPosition.CopyTo(CrosshairWorldScaledPoint, 0);
                StartCrosshairs();
                if (LeftMouseDown != null)
                    LeftMouseDown(this, new WorldPositionEventArgs(new PointF((float)CrosshairWorldScaledPoint[0], (float)CrosshairWorldScaledPoint[1])));
            }
        }

        protected override void OnRightMouseDown(vtkObject sender, vtkObjectEventArgs e)
        {
            var screenPosition = Interactor.GetEventPosition();
            var worldPosition = Renderer.ScreenToWorldCoordinate(screenPosition[0], screenPosition[1]);
            var roundedPosition = SnapWorldPointToNearestWholeUnscaledPoint(worldPosition, false);
            _rightMouseDownPosition[0] = roundedPosition[0];
            _rightMouseDownPosition[1] = roundedPosition[1];

            CurrentZoomMode = (Control.ModifierKeys & Keys.Shift) == ProportionalZoomKey && GraphicsWindow.AllowUnconstrainedPan
                ? ZoomMode.Proportional
                : ZoomMode.Stretch;
            StartPan();
            _panStart = new Point(screenPosition[0], screenPosition[1]);
            _rightMouseDown = true;
            if (RightMouseDown != null)
                RightMouseDown(this, new WorldPositionEventArgs(new PointF((float)_rightMouseDownPosition[0], (float)_rightMouseDownPosition[1])));
        }

        protected override void OnRightMouseUp(vtkObject sender, vtkObjectEventArgs e)
        {
            var screenPosition = Interactor.GetEventPosition();
            var worldPosition = Renderer.ScreenToWorldCoordinate(screenPosition[0], screenPosition[1]);
            var roundedPosition = SnapWorldPointToNearestWholeUnscaledPoint(worldPosition, false);
            _rightMouseUpPosition[0] = roundedPosition[0];
            _rightMouseUpPosition[1] = roundedPosition[1];

            //Handle panning and associated crosshair movement
            if (!_panning) return;
            EndPan();
            _panEnd = new Point(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1]);
            bool hasPanned = (Math.Abs(_panEnd.X - _panStart.X) > 1 || Math.Abs(_panEnd.Y - _panStart.Y) > 1);
            if (hasPanned) 
                ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(GraphicsWindow.GetXyExtents()));
            _rightMouseDown = false;
        }

        protected override void OnLeftMouseUp(vtkObject sender, vtkObjectEventArgs e)
        {
            //Set mouse position
            var screenPosition = Interactor.GetEventPosition();
            var worldPosition = Renderer.ScreenToWorldCoordinate(screenPosition[0], screenPosition[1]);
            var roundedPosition = SnapWorldPointToNearestWholeUnscaledPoint(worldPosition, false);
            _leftMouseUpPosition[0] = _leftMouseCurrentPosition[0] = roundedPosition[0];
            _leftMouseUpPosition[1] = _leftMouseCurrentPosition[1] = roundedPosition[1];
            
            if (_movingCrosshair)
                EndCrosshairs();
            if (RubberBanding)
            {
                EndRubberBand(roundedPosition);
                if (Interactor.GetControlKey().ToBool())
                {
                    RubberBandZoom();
                    RubberBanding = false; //rubber banding handled internally.
                }
            }

            LeftMouseUp(this, new WorldPositionEventArgs(new PointF((float)_leftMouseUpPosition[0], (float)_leftMouseUpPosition[1])));
            _leftMouseDown = false;
            RubberBanding = false; //Set this after the LeftMouseUpEvent so the state can be properly read.    
            _movingCrosshair = false;        
        }
        
        protected override void OnMouseMoving(vtkObject sender, vtkObjectEventArgs e)
        {
            var screenPosition = Interactor.GetEventPosition();
            var worldPosition = Renderer.ScreenToWorldCoordinate(screenPosition[0], screenPosition[1]);
            var roundedPoint = SnapWorldPointToNearestWholeUnscaledPoint(worldPosition, false);
            
            // Set appropriate mouse button's position
            double[] prevPos;
            if (_leftMouseDown)
            {
                prevPos = _leftMouseCurrentPosition.DeepCopy();
                if (roundedPoint[0].IsWithinToleranceOf(_leftMouseCurrentPosition[0], CursorTolerance) && roundedPoint[1].IsWithinToleranceOf(_leftMouseCurrentPosition[1], CursorTolerance))
                    return;
                _leftMouseCurrentPosition[0] = roundedPoint[0];
                _leftMouseCurrentPosition[1] = roundedPoint[1];
                if (_movingCrosshair)
                {
                    roundedPoint.CopyTo(CrosshairWorldScaledPoint, 0);
                    RedrawCrosshairs();
                    Interactor.Render();
                }
                if (LeftMouseDragging != null)
                    LeftMouseDragging(this, new WorldPositionEventArgs(new PointF((float)_leftMouseCurrentPosition[0], (float)_leftMouseCurrentPosition[1])));
            }
            else if (_rightMouseDown)
            {
                prevPos = _rightMouseCurrentPosition.DeepCopy();
                _rightMouseCurrentPosition[0] = roundedPoint[0];
                _rightMouseCurrentPosition[1] = roundedPoint[1];
                if (_movingCrosshair)
                {
                    roundedPoint.CopyTo(CrosshairWorldScaledPoint, 0);
                    RedrawCrosshairs();
                    Interactor.Render();
                }
                if (RightMouseDragging != null)
                    RightMouseDragging(this, new WorldPositionEventArgs(new PointF((float)_rightMouseCurrentPosition[0], (float)_rightMouseCurrentPosition[1])));
            }
            else return;
            if (prevPos[0] == worldPosition[0] && prevPos[1] == worldPosition[1]) return;
                
            if (_panning) MousePan();

            if (RubberBanding)
            {
                _rubberBandDraggingWorlPoint[0] = roundedPoint[0];
                _rubberBandDraggingWorlPoint[1] = roundedPoint[1];
                RedrawRubberBand();
            }
            
        }

        private void RedrawRubberBand()
        {
            //NOTE: follows logic of vtkInteractorStyleRubberBand2D in C++ source code.  modified for c#
            int[] size = GraphicsWindow.RenderWindow.GetSize();

            vtkUnsignedCharArray tmpPixelArray = new vtkUnsignedCharArray();
            tmpPixelArray.DeepCopy(_pixelArray);

            var startScreenCoords = Renderer.WorldToScreenCoordinate(_rubberBandStartWorlPoint[0], _rubberBandStartWorlPoint[1], 0);
            var endScreenCoords = Renderer.WorldToScreenCoordinate(_rubberBandDraggingWorlPoint[0], _rubberBandDraggingWorlPoint[1], 0);

            int[] min = new int[2];
            int[] max = new int[2];

            min[0] = Math.Min(startScreenCoords[0], endScreenCoords[0]).Constrain(0, size[0] - 1);
            min[1] = Math.Min(startScreenCoords[1], endScreenCoords[1]).Constrain(0, size[1] - 1);

            max[0] = Math.Max(startScreenCoords[0], endScreenCoords[0]).Constrain(0, size[0] - 1);
            max[1] = Math.Max(startScreenCoords[1], endScreenCoords[1]).Constrain(0, size[1] - 1);

            int i;
            for (i = min[0]; i <= max[0]; i++)
            {
                tmpPixelArray.SetValue(4 * (min[1] * size[0] + i), (byte)(255 ^ tmpPixelArray.GetValue(4 * (min[1] * size[0] + i))));
                tmpPixelArray.SetValue(4 * (min[1] * size[0] + i), (byte)(255 ^ tmpPixelArray.GetValue(4 * (min[1] * size[0] + i))));
                tmpPixelArray.SetValue(4 * (min[1] * size[0] + i) + 1, (byte)(255 ^ tmpPixelArray.GetValue(4 * (min[1] * size[0] + i) + 1)));
                tmpPixelArray.SetValue(4 * (min[1] * size[0] + i) + 2, (byte)(255 ^ tmpPixelArray.GetValue(4 * (min[1] * size[0] + i) + 2)));
                tmpPixelArray.SetValue(4 * (max[1] * size[0] + i), (byte)(255 ^ tmpPixelArray.GetValue(4 * (max[1] * size[0] + i))));
                tmpPixelArray.SetValue(4 * (max[1] * size[0] + i) + 1, (byte)(255 ^ tmpPixelArray.GetValue(4 * (max[1] * size[0] + i) + 1)));
                tmpPixelArray.SetValue(4 * (max[1] * size[0] + i) + 2, (byte)(255 ^ tmpPixelArray.GetValue(4 * (max[1] * size[0] + i) + 2)));
            }
            for (i = min[1] + 1; i < max[1]; i++)
            {
                tmpPixelArray.SetValue(4 * (i * size[0] + min[0]), (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + min[0]))));
                tmpPixelArray.SetValue(4 * (i * size[0] + min[0]) + 1, (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + min[0]) + 1)));
                tmpPixelArray.SetValue(4 * (i * size[0] + min[0]) + 2, (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + min[0]) + 2)));
                tmpPixelArray.SetValue(4 * (i * size[0] + max[0]), (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + max[0]))));
                tmpPixelArray.SetValue(4 * (i * size[0] + max[0]) + 1, (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + max[0]) + 1)));
                tmpPixelArray.SetValue(4 * (i * size[0] + max[0]) + 2, (byte)(255 ^ tmpPixelArray.GetValue(4 * (i * size[0] + max[0]) + 2)));
            }

            GraphicsWindow.RenderWindow.SetRGBACharPixelData(0, 0, size[0] - 1, size[1] - 1, tmpPixelArray, 0, 0);
            GraphicsWindow.RenderWindow.Frame();

            tmpPixelArray.Dispose();
        }

        private void RubberBandZoom()
        {
            var screenPick1 = Renderer.WorldToScreenCoordinate(_rubberBandStartWorlPoint[0], _rubberBandStartWorlPoint[1], 0);
            var screenPick2 = Renderer.WorldToScreenCoordinate(_rubberBandEndWorlPoint[0], _rubberBandEndWorlPoint[1], 0);
            CartesianPoint worldPick1 = new CartesianPoint(_rubberBandStartWorlPoint[0], _rubberBandStartWorlPoint[1], 0);
            CartesianPoint worldPick2 = new CartesianPoint(_rubberBandEndWorlPoint[0], _rubberBandEndWorlPoint[1], 0);
            CartesianPoint screenPoint1 = new CartesianPoint(screenPick1[0], screenPick1[1], 0);
            CartesianPoint screenPoint2 = new CartesianPoint(screenPick2[0], screenPick2[1], 0);

            if (screenPoint1.DistanceFrom(screenPoint2) > 2)
                PanZoom(worldPick1, worldPick2, screenPick1, screenPick2);
            else
                UndoPanZoom();
        }

        public void MoveCrosshair(Direction direction, int points = 1)
        {
            double[] pt = new double[3];
            CrosshairWorldScaledPoint.CopyTo(pt, 0);
            var gridScale = GraphicsWindow.GridScale;
            switch (direction)
            {
                case Direction.Up:
                    pt[1] += points * gridScale.Y;
                    break;
                case Direction.Down:
                    pt[1] -= points * gridScale.Y;
                    break;
                case Direction.Left:
                    pt[0] -= points * gridScale.X;
                    break;
                case Direction.Right:
                    pt[0] += points * gridScale.X;
                    break;
            }
            
            SetCrosshairs(pt, true);
        }

        /// <summary> redraws the crosshair at whatever world position is stored in local variables, usually done while snapping. Does not throw any events while doing so. </summary>
        private void RedrawCrosshairs()
        {
            _crossHairCursor.SetFocalPoint(CrosshairScreenPoint[0], CrosshairScreenPoint[1], 0.0);
            SetCrosshairTextLocation();
        }

        /// <summary> When moving the cursor around the 2DImage data, this will snap the current picked point to the closest value that is a whole number (slice/sensor) </summary>
        private double[] SnapWorldPointToNearestWholeUnscaledPoint(double[] worldPoint, bool render)
        {
            var gScale = GraphicsWindow.GridScale;
            double[] roundedPoint = new double[2];
            roundedPoint[0] = (int)Math.Round(worldPoint[0] / gScale.X) * gScale.X;
            roundedPoint[1] = (int)Math.Round(worldPoint[1] / gScale.Y) * gScale.Y;
            if (render)
                Interactor.Render();
            return roundedPoint;
        }

        private void SetCrosshairTextLocation()
        {
            _xLeftLocationText.SetDisplayPosition(CrosshairScreenPoint[0] - 5, CrosshairScreenPoint[1] + 2);
            _xRightLocationText.SetDisplayPosition(CrosshairScreenPoint[0] + 5, CrosshairScreenPoint[1] + 2);
        }

        private void StartPan()
        {
            if (CurrentZoomMode == ZoomMode.Stretch) GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorSizewe);
            else GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorSizeall);
            _panning = true;
        }

        private void EndPan()
        {
            _panning = false;
            GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorDefault);
            ImageViewExtentsChanged(this, new ImageViewExtentsChangedEventArgs(Renderer.GetCurrentWorldExtents()));
            RedrawCrosshairs();
        }

        private void MousePan()
        {
            if (Renderer == null) return;

            //NOTE:  code converted from vtkInteractorStyleTrackBallCamera::Pan()
            double[] motionVector = new double[3];

            // Calculate the focal depth since we'll be using it a lot
            double[] viewFocus = GraphicsWindow.Camera.GetFocalPoint();
            Renderer.SetWorldPoint(viewFocus[0], viewFocus[1], viewFocus[2], 0);
            Renderer.WorldToDisplay();
            viewFocus = Renderer.GetDisplayPoint();
            double focalDepth = viewFocus[2];

            Renderer.SetDisplayPoint(Interactor.GetEventPosition()[0], Interactor.GetEventPosition()[1], focalDepth);
            Renderer.DisplayToWorld();
            double[] newPickPoint = Renderer.GetWorldPoint();
            newPickPoint[2] = 0;

            // Has to recalculate old mouse point since the viewport has moved, so can't move it outside the loop
            Renderer.SetDisplayPoint(Interactor.GetLastEventPosition()[0], Interactor.GetLastEventPosition()[1], focalDepth);
            Renderer.DisplayToWorld();
            double[] oldPickPoint = Renderer.GetWorldPoint();
            oldPickPoint[2] = 0;

            // Camera motion is reversed
            if ((CurrentZoomMode == ZoomMode.Stretch))
            {
                motionVector[0] = oldPickPoint[0] - newPickPoint[0];
                motionVector[1] = 0;
                motionVector[2] = oldPickPoint[2] - newPickPoint[2];
            }
            else
            {
                motionVector[0] = oldPickPoint[0] - newPickPoint[0];
                motionVector[1] = oldPickPoint[1] - newPickPoint[1];
                motionVector[2] = oldPickPoint[2] - newPickPoint[2];
            }

            viewFocus = GraphicsWindow.Camera.GetFocalPoint();
            double[] viewPoint = GraphicsWindow.Camera.GetPosition();
            GraphicsWindow.Camera.SetFocalPoint(motionVector[0] + viewFocus[0], motionVector[1] + viewFocus[1], motionVector[2] + viewFocus[2]);
            GraphicsWindow.Camera.SetPosition(motionVector[0] + viewPoint[0], motionVector[1] + viewPoint[1], motionVector[2] + viewPoint[2]);

            //track the crosshairs
            RedrawCrosshairs();
            Interactor.Render();
            GraphicsWindow.HideDataTip();
        }

        private void StartCrosshairs()
        {
            CursorVisible = false;
            ToggleCrosshairsCursor(true);
            var roundedPoint = SnapWorldPointToNearestWholeUnscaledPoint(CrosshairWorldScaledPoint, false);
            CrosshairWorldScaledPoint[0] = roundedPoint[0];
            CrosshairWorldScaledPoint[1] = roundedPoint[1];
            _movingCrosshair = true;
            RedrawCrosshairs();
            Interactor.Render();
        }

        private void EndCrosshairs()
        {
            CursorVisible = true;
        }

        private void StartRubberBand(double[] roundedWorldPosition)
        {
            RubberBanding = true;
            GraphicsWindow.RenderWindow.SetCurrentCursor((int)VtkCursor.VtkCursorCrosshair);
            _pixelArray.Initialize();
            _pixelArray.SetNumberOfComponents(4);
            int[] size = GraphicsWindow.RenderWindow.GetSize();
            _pixelArray.SetNumberOfTuples(size[0] * size[1]);
            GraphicsWindow.RenderWindow.GetRGBACharPixelData(0, 0, size[0] - 1, size[1] - 1, 1, _pixelArray);

            _rubberBandStartWorlPoint[0] = roundedWorldPosition[0];
            _rubberBandStartWorlPoint[1] = roundedWorldPosition[1];
        }

        private void EndRubberBand(double[] roundedWorldPosition)
        {
            // Clear the rubber band
            int[] size = GraphicsWindow.RenderWindow.GetSize();
            GraphicsWindow.RenderWindow.SetRGBACharPixelData(0, 0, size[0] - 1, size[1] - 1, _pixelArray, 0, 0);
            GraphicsWindow.RenderWindow.Frame();

            _rubberBandEndWorlPoint[0] = roundedWorldPosition[0];
            _rubberBandEndWorlPoint[1] = roundedWorldPosition[1];
        }

        #endregion

        #region IDispose Pattern Implementation

        private bool _disposed;
        
        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (_disposed) return;
            // Managed Resources
            if (disposing)
            {
                // Dispose managed resources.
                if (_pixelArray != null) _pixelArray.Dispose();
                if (_cursorActor != null) _cursorActor.Dispose();
                if (_xLeftLocationText != null) _xLeftLocationText.Dispose();
                if (_xRightLocationText != null) _xRightLocationText.Dispose();
                if (_crossHairCursor != null) _crossHairCursor.Dispose();
            }

            //Set Flag
            _disposed = true;
        }

        #endregion

        
    }
    
}