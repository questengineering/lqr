﻿//#region Copyright Quest Integrity Group, LLC 2012

////
//// www.QuestIntegrity.com
//// +1-303-415-1475
////
//// All rights are reserved. Reproduction or transmission in whole or in part, in
//// any form or by any means, electronic, mechanical or otherwise, is prohibited
//// without the prior written consent of the copyright owner.
////
//// Filename: $Id: ThreeDInteractor.cs,v 1.2 2011/11/23 21:21:35 C.Turiano Exp $
////

//#endregion

//using System;
//using System.Drawing;
//using Kitware.VTK;
//using QuestIntegrity.Extensions;
//using QuestIntegrity.Graphics.Controls;
//using QuestIntegrity.Graphics.EventArgs;

//namespace QuestIntegrity.Graphics
//{
//    public class ThreeDInteractor : Interactor,
//                                    IDisposable
//    {
//        #region Enumerations

//        #endregion

//        #region Private and Protected fields

//        private const double _zFeatureOffset = 0.1d; //offset of Features in the Z direction. Needs to be the same as in GridBuilder
//        private const int _minZoomBoxSize = 5; //size in pixels of smallest rubberband zoom box allowed

//        protected InteractionMode _currentZoomMode = InteractionMode.ZoomStretch;
//        private const double _defaultZoomPercent = 15.0;
//        private int[] _startPosition = new int[2];
//        private int[] _endPosition = new int[2];

//        // The pixel array for the rubber band
//        private readonly vtkUnsignedCharArray _pixelArray = new vtkUnsignedCharArray();

//        //Pickers
//        private vtkPropPicker _picker = new vtkPropPicker();

//        //Crosshair cursor
//        private int[] _crossHairScreenPosition = new int[2];
//        private double[] _crossHairGlobalPosition = new double[3];
//        private double[] _crossHairMappedPosition = new double[3];
//        private vtkCursor2D _crossHairCursor;
//        private vtkActor2D _cursorActor;
//        private Color _crossHairColor = Color.DarkBlue;
//        private TextProperty _crossHairTextProperty = new TextProperty();
//        private vtkTextActor _xAxisLocationText;
//        private vtkTextActor _yAxisLocationText;
//        private bool _isSnapping = true;
//        private int _pickedPointID = -1;
//        private long _pickedGlobalID1 = -1;
//        private long _pickedGlobalID2 = -1;

//        //Measurement cursors
//        private vtkCursor2D _measureCursor1;
//        private vtkCursor2D _measureCursor2;
//        private vtkActor2D _measureActor1;
//        private vtkActor2D _measureActor2;
//        private int[] _measureScreenPt1 = new int[2];
//        private int[] _measureScreenPt2 = new int[2];
//        private long _measureGlobalID1 = -1;
//        private long _measureGlobalID2 = -1;
//        private double[] _measureWorldPt1 = new double[3];
//        private double[] _measureWorldPt2 = new double[3];
//        private vtkTextActor _measureLocationText;
//        private const int _distanceCursorWidth = 7; //amount to add in each direction to size cursor (1/2 the desired height and width)

//        //used for adjusting camera position
//        private PanAxis _panningAxis;
//        private PanAxis _constrainedAxis;

//        private vtkRenderer Renderer
//        {
//            get
//            {
//                return GraphicsWindow.Renderer;
//            }
//        }

//        /// <summary>
//        ///     Gets or sets a value indicating whether the Crosshairs cursor is visible.
//        /// </summary>
//        /// <value>
//        ///     <c>true</c> if [show crosshairs cursor]; otherwise, <c>false</c>.
//        /// </value>
//        private bool ShowCrosshairsCursor
//        {
//            get
//            {
//                return _cursorActor.GetVisibility().ToBool();
//            }
//            set
//            {
//                _cursorActor.SetVisibility(value);
//                _yAxisLocationText.SetVisibility(value);
//                _xAxisLocationText.SetVisibility(value);
//            }
//        }

//        /// <summary>
//        ///     Gets or sets a value indicating whether the measurement cursors are visible
//        /// </summary>
//        /// <value>
//        ///     <c>true</c> if [show measurement cursor]; otherwise, <c>false</c>.
//        /// </value>
//        private bool ShowMeasurementCursors
//        {
//            get
//            {
//                return _measureActor1.GetVisibility().ToBool();
//            }
//            set
//            {
//                _measureActor1.SetVisibility(value);
//                _measureActor2.SetVisibility(value);
//                _measureLocationText.SetVisibility(value);
//            }
//        }

//        #endregion

//        #region Public Properties

//        /// <summary>
//        ///     Gets or sets the color of the crosshair.
//        /// </summary>
//        /// <value>The color of the crosshair.</value>
//        public Color CrosshairColor
//        {
//            get
//            {
//                return _crossHairColor;
//            }
//            set
//            {
//                _crossHairColor = value;
//                if (_cursorActor != null)
//                {
//                    _cursorActor.GetProperty().SetColor(CrosshairColor);
//                }
//            }
//        }

//        /// <summary>
//        ///     Gets or sets the color of the crosshair.
//        /// </summary>
//        /// <value>The color of the crosshair.</value>
//        public TextProperty CrosshairTextProperties
//        {
//            get
//            {
//                return _crossHairTextProperty;
//            }
//            set
//            {
//                _crossHairTextProperty = value;
//                if (_xAxisLocationText != null &&
//                    _yAxisLocationText != null)
//                {
//                    _crossHairTextProperty.AssignToVtkTextProperty(_xAxisLocationText.GetTextProperty());
//                    _crossHairTextProperty.AssignToVtkTextProperty(_yAxisLocationText.GetTextProperty());
//                }
//            }
//        }

//        #endregion

//        #region Constructor

//        public ThreeDInteractor(GraphicsDisplayWindow graphicsWindow)
//            : base(graphicsWindow)
//        {
//            SetUpLighting();
//        }

//        #endregion

//        #region Public Methods

//        #endregion

//        #region Public Events

//        public event EventHandler<PickPositionEventArgs> CenterPicked;

//        #endregion

//        #region Private and Protected Methods

//        protected new void RegisterInteractionEvents()
//        {
//            //common events
//            base.RegisterInteractionEvents();

//            //register 3D-specific events

//            //Use the default trackball interactor
//            Style = new vtkInteractorStyleTrackballCamera();
//            _interactor.SetInteractorStyle(Style);
//        }

//        private void SetUpLighting()
//        {
//            Renderer.SetAutomaticLightCreation(0);
//            Renderer.GetLights().RemoveAllItems();

//            // Add light kit which gives better illumination in 3D
//            vtkLightKit kit = new vtkLightKit();
//            kit.SetKeyLightAngle(50, 10);
//            kit.SetKeyLightWarmth(0.60);
//            kit.SetKeyLightIntensity(0.75);

//            kit.SetFillLightAngle(-75, -10);
//            kit.SetFillLightWarmth(0.40);
//            kit.SetKeyToFillRatio(3.0);

//            kit.SetBackLightAngle(0, 110);
//            kit.SetBackLightWarmth(0.50);
//            kit.SetKeyToBackRatio(3.5);

//            kit.SetHeadLightWarmth(0.50);
//            kit.SetKeyToHeadRatio(3.0);

//            kit.AddLightsToRenderer(Renderer);
//        }

//        #endregion

//        #region IDispose Pattern Implementation

//        private bool _disposed;

//        void IDisposable.Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }

//        private void Dispose(bool disposing)
//        {
//            // Check to see if Dispose has already been called.
//            if (!_disposed)
//            {
//                // Managed Resources
//                if (disposing)
//                {
//                    // Dispose managed resources.
//                    if (_pixelArray != null)
//                    {
//                        _pixelArray.Dispose();
//                    }
//                    if (_cursorActor != null)
//                    {
//                        _cursorActor.Dispose();
//                    }
//                    if (_yAxisLocationText != null)
//                    {
//                        _yAxisLocationText.Dispose();
//                    }
//                    if (_xAxisLocationText != null)
//                    {
//                        _xAxisLocationText.Dispose();
//                    }
//                    if (_crossHairCursor != null)
//                    {
//                        _crossHairCursor.Dispose();
//                    }
//                }

//                //Cleaup Unmanaged resources

//                //Set Flag
//                _disposed = true;
//            }
//        }

//        #endregion
//    }
//}