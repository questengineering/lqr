﻿#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: Interactor.cs,v 1.11 2011/02/02 05:33:43 J.Rowe Exp $
//

#endregion

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Input;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.Controls;
using QuestIntegrity.Graphics.EventArgs.PickPositions;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

namespace QuestIntegrity.Graphics.Interactors
{
    /// <summary> Base class that provides events for mouse and keyboard actions. </summary>
    public class BaseInteractor
    {
        #region Private and Protected Fields

        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected GraphicsDisplayWindow GraphicsWindow;
        protected vtkRenderer Renderer;
        protected vtkRenderWindow RenderWindow;
        protected vtkRenderWindowInteractor Interactor;
        protected vtkInteractorStyle Style;
        protected CartesianBounds<double> LastZoomExtents = new CartesianBounds<double>();
        protected CartesianPoint LastZoomCenter;
        protected double LastZoomRatio = 1.0;
        private bool _cursorVisible = true; //tracking variables to control cursor showing/hiding
        protected bool MouseLeft; //tracking variables to control cursor showing/hiding
        public InteractionMode CurrentInteractionMode { get; set; }
        public virtual ZoomMode CurrentZoomMode { get; set; }

        #endregion

        #region Constructors

        protected BaseInteractor(GraphicsDisplayWindow graphicsWindow)
        {
            GraphicsWindow = graphicsWindow;
            RenderWindow = GraphicsWindow.RenderWindow;
            Renderer = GraphicsWindow.Renderer;
            Interactor = GraphicsWindow.RenderWindow.GetInteractor();
        }

        #endregion

        #region Public Properties

        /// <summary> Gets or sets a value indicating whether the standard Cursor is visible </summary>
        public bool CursorVisible
        {
            get { return _cursorVisible; }
            set
            {
                if (value) Interactor.ShowCursor();
                else Interactor.HideCursor();
                _cursorVisible = value;
            }
        }

        #endregion

        #region Public Events and Handlers

        /// <summary> Triggered when the left mouse button is depressed on a point in screen coordinates. </summary>
        public event EventHandler<ScreenPositionEventArgs> LeftMouseDown;
        /// <summary> Triggered when the left mouse button is depressed and moving. </summary>
        public event EventHandler<ScreenPositionEventArgs> LeftMouseDragging;
        /// <summary> Triggered when the left mouse button is released. </summary>
        public event EventHandler<ScreenPositionEventArgs> LeftMouseUp;
        /// <summary> Triggered when the right mouse button is depressed on a point in screen coordinates. </summary>
        public event EventHandler<ScreenPositionEventArgs> RightMouseDown;
        /// <summary> Triggered when the right mouse button is depressed and moving. </summary>
        public event EventHandler<ScreenPositionEventArgs> RightMouseDragging;
        /// <summary> Triggered when the right mouse button is released. </summary>
        public event EventHandler<ScreenPositionEventArgs> RightMouseUp;
        public event EventHandler<MouseWheelEventArgs> MouseWheelUp;
        public event EventHandler<MouseWheelEventArgs> MouseWheelDown;

        public event EventHandler<System.EventArgs> MouseLeave;
        public event EventHandler<KeyEventArgs> KeyReleased;
        public event EventHandler<KeyEventArgs> KeyPressed;

        protected virtual void RegisterInteractionEvents()
        {
            Interactor.KeyReleaseEvt += OnKeyUp;
            Interactor.KeyPressEvt += OnKeyDown;
            Interactor.MouseMoveEvt += OnMouseMoving;
            Interactor.LeaveEvt += OnMouseLeave;
            Interactor.LeftButtonPressEvt += OnLeftMouseDown;
            Interactor.LeftButtonReleaseEvt += OnLeftMouseUp;
            Interactor.RightButtonPressEvt += OnRightMouseDown;
            Interactor.RightButtonReleaseEvt += OnRightMouseUp;
            Interactor.MouseWheelBackwardEvt += OnMouseWheelDown;
            Interactor.MouseWheelForwardEvt += OnMouseWheelUp;
        }

        public void DeregisterAllUserEvents()
        {
            LeftMouseDown = null;
            LeftMouseDragging = null;
            LeftMouseUp = null;
            RightMouseDown = null;
            RightMouseDragging = null;
            RightMouseUp = null;
            MouseLeave = null;
            KeyReleased = null;
            KeyPressed = null;
        }

        protected virtual void OnKeyDown(vtkObject sender, vtkObjectEventArgs e)
        {
            Keys key = Interactor.GetKeyTyped();
            KeyEventArgs args = new KeyEventArgs(key);
            if (KeyPressed != null) KeyPressed(this, args);
        }

        protected virtual void OnKeyUp(vtkObject sender, vtkObjectEventArgs e)
        {
            Keys key = Interactor.GetKeyTyped();
            KeyEventArgs args = new KeyEventArgs(key);
            if (KeyReleased != null) KeyReleased(this, args);
        }

        protected virtual void OnLeftMouseDown(vtkObject sender, vtkObjectEventArgs e)
        {
            int[] screenPoint = Interactor.GetEventPosition();
            if (LeftMouseDown != null) LeftMouseDown(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
        }
        
        protected virtual void OnLeftMouseUp(vtkObject sender, vtkObjectEventArgs e)
        {
            int[] screenPoint = Interactor.GetEventPosition();
            if (LeftMouseUp != null) LeftMouseUp(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
        }

        protected virtual void OnRightMouseDown(vtkObject sender, vtkObjectEventArgs e)
        {
            int[] screenPoint = Interactor.GetEventPosition();
            if (RightMouseDown != null) RightMouseDown(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
        }

        protected virtual void OnRightMouseUp(vtkObject sender, vtkObjectEventArgs e)
        {
            int[] screenPoint = Interactor.GetEventPosition();
            if (RightMouseUp != null) RightMouseUp(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
        }

        protected virtual void OnMouseMoving(vtkObject sender, vtkObjectEventArgs e)
        {
            int[] screenPoint = Interactor.GetEventPosition();
            if (Mouse.LeftButton == MouseButtonState.Pressed && LeftMouseDragging != null)
                LeftMouseDragging(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
            if (Mouse.RightButton == MouseButtonState.Pressed && RightMouseDragging != null)
                RightMouseDragging(this, new ScreenPositionEventArgs(new Point(screenPoint[0], screenPoint[1])));
        }

        protected virtual void OnMouseLeave(vtkObject sender, vtkObjectEventArgs e)
        {
            if (MouseLeave != null) MouseLeave(this, null);
        }

        protected virtual void OnMouseWheelUp(vtkObject sender, vtkObjectEventArgs e)
        {
            int timestamp = new TimeSpan(DateTime.Now.Ticks).Milliseconds;
            if (MouseWheelUp != null) MouseWheelUp(this, new MouseWheelEventArgs(Mouse.PrimaryDevice, timestamp, 1));
        }

        protected virtual void OnMouseWheelDown(vtkObject sender, vtkObjectEventArgs e)
        {
            int timestamp = new TimeSpan(DateTime.Now.Ticks).Milliseconds;
            if (MouseWheelDown != null) MouseWheelDown(this, new MouseWheelEventArgs(Mouse.PrimaryDevice, timestamp, -1));
        }
        
        #endregion Event Handling

        #region Enums

        public enum Direction
        {
            Up,
            Down,
            Left,
            Right
        }

        #endregion Enums
    }
}