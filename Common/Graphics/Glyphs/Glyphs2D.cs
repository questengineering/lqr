﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Kitware.VTK;

namespace QuestIntegrity.Graphics.Glyphs
{
    public class Glyphs2D
    {
        private readonly vtkPolyData _glyphDataSet = new vtkPolyData();
        private readonly vtkGlyph3D _glypher = new vtkGlyph3D();
        private readonly vtkPolyDataMapper _glyphMapper = vtkPolyDataMapper.New();
        public readonly vtkActor GlyphActor = new vtkActor();
        /// <summary> Where on the Z axis these points will be drawn. </summary>
        public double ZOffset = -.1;

        public Glyphs2D()
        {
            SetupGlyphs();
        }

        private void SetupGlyphs()
        {
            _glypher.OrientOff();
            _glypher.SetIndexModeToScalar();
            _glypher.ScalingOn();
            _glypher.SetScaleModeToDataScalingOff();
            _glypher.SetRange(0, Enum.GetValues(typeof(GlyphPoint.Symbol)).Length - 1);
            _glypher.SetColorModeToColorByScalar();
            _glypher.SetInputArrayToProcess(1, 0, 0, (int)vtkDataObject.FieldAssociations.FIELD_ASSOCIATION_POINTS, "symbols");
            _glypher.SetInputArrayToProcess(3, 0, 0, (int)vtkDataObject.FieldAssociations.FIELD_ASSOCIATION_POINTS, "colors");
            _glypher.SetInput(_glyphDataSet);

            //Set lookup of shapes using enums
            for (int i = 0; i < Enum.GetValues(typeof(GlyphPoint.Symbol)).Length; i++)
            {
                vtkGlyphSource2D source = new vtkGlyphSource2D();
                switch (i)
                {
                    case 0: source.SetGlyphTypeToNone(); break;
                    case 1: source.SetGlyphTypeToVertex(); break;
                    case 2: source.SetGlyphTypeToDash(); break;
                    case 3: source.SetGlyphTypeToCross(); break;
                    case 4: source.SetGlyphTypeToThickCross(); break;
                    case 5: source.SetGlyphTypeToTriangle(); break;
                    case 6: source.SetGlyphTypeToSquare(); break;
                    case 7: source.SetGlyphTypeToCircle(); break;
                    case 8: source.SetGlyphTypeToDiamond(); break;
                    case 9: source.SetGlyphTypeToArrow(); break;
                    case 10: source.SetGlyphTypeToThickArrow(); break;
                    case 11: source.SetGlyphTypeToHookedArrow(); break;
                    case 12: source.SetGlyphTypeToEdgeArrow(); break;
                }
                source.FilledOff();
                _glypher.SetSource(i, source.GetOutput());
            }

            _glyphMapper.SetInputConnection(_glypher.GetOutputPort());
            _glyphMapper.SetColorModeToDefault();
            _glyphMapper.SetScalarModeToUsePointFieldData();
            _glyphMapper.SelectColorArray("colors");

            GlyphActor.SetMapper(_glyphMapper);
            GlyphActor.GetProperty().SetLineWidth(2.0f);
            GlyphActor.VisibilityOn();
            GlyphActor.PickableOff();

        }

        /// <summary> Reset the Actor with the points specified. Assumes points are already scaled to world coordinates. </summary>
        public void SetPoints(List<GlyphPoint> points, double pointScale, double lineWidth)
        {
            vtkPoints pts = new vtkPoints();
            vtkIntArray symbols = new vtkIntArray(); // holds the symbol mapping
            symbols.SetName("symbols");
            vtkUnsignedCharArray colors = new vtkUnsignedCharArray(); // holds the colors (direct color bytes, not a mapping)
            colors.SetNumberOfComponents(3);
            colors.SetName("colors");

            int size = Marshal.SizeOf(typeof(byte)) * 3;
            IntPtr ptrColor = Marshal.AllocHGlobal(size);

            try
            {
                // Copy points into the vtk structures
                foreach (GlyphPoint pt in points)
                {
                    pts.InsertNextPoint(pt.X, pt.Y, pt.Z);
                    symbols.InsertNextValue((int)pt.DrawSymbol);
                    byte[] color = { pt.DrawColor.R, pt.DrawColor.G, pt.DrawColor.B };
                    Marshal.Copy(color, 0, ptrColor, color.Length);
                    colors.InsertNextTupleValue(ptrColor);
                }
            }
            finally { Marshal.FreeHGlobal(ptrColor); }

            GlyphActor.GetProperty().SetLineWidth((float)lineWidth); // can't actually vary by type the way we are doing this, so just use feature line width
            _glyphDataSet.SetPoints(pts);
            _glyphDataSet.GetPointData().AddArray(symbols);
            _glyphDataSet.GetPointData().AddArray(colors);
            _glyphDataSet.GetPointData().SetScalars(symbols);

            // Setup the glypher
            _glypher.SetInput(_glyphDataSet);
            _glypher.SetScaleFactor(pointScale);
            _glypher.Update();
        }
    }
}
