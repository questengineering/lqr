#region Copyright Quest Integrity Group, LLC 2010
//
// www.QuestIntegrity.com
// (303) 415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//
#endregion

using System;

namespace QuestReliability.Graphics
{
    /// <summary>
    /// Event arguments to indicate the picked position
    /// </summary>
    public class PickPositionEventArgs : EventArgs
    {
        public enum WorldCoordinateSystem
        {
            UnitSystem,
            Indicies
        }

        public enum PickEventType
        {
            Positioning,
            Selecting
        }

        private readonly int[] _screenPoint;
        private readonly double[] _worldPoint;
        private readonly long _pointID = 0;
        private WorldCoordinateSystem _worldSystem = WorldCoordinateSystem.UnitSystem;
        private readonly PickEventType _pickType = PickEventType.Positioning;

        //Constructor.
        public PickPositionEventArgs(int[] screenPoint, double[] worldPoint)
        {
            _worldPoint = worldPoint;
            _screenPoint = screenPoint;
        }
        public PickPositionEventArgs(int[] screenPoint, double[] worldPoint, long pointID)
            : this(screenPoint, worldPoint)
        {
            _pointID = pointID;
        }
        public PickPositionEventArgs(int[] screenPoint, double[] worldPoint, long pointID, PickEventType pickType)
            : this(screenPoint, worldPoint, pointID)
        {
            _pickType = pickType;
        }
        public PickPositionEventArgs(int[] screenPoint, double[] worldPoint, long pointID, WorldCoordinateSystem worldSystem)
            : this(screenPoint, worldPoint, pointID)
        {
            _worldSystem = worldSystem;
        }

        /// <summary>
        /// Position of point in World coordinates
        /// </summary>
        public double[] WorldPoint
        {
            get
            {
                return _worldPoint;
            }
        }

        /// <summary>
        /// Position of mouse in Screen coordinates
        /// </summary>
        public int[] ScreenPoint
        {
            get
            {
                return _screenPoint;
            }
        }
        /// <summary>
        /// Global PointID of the Picked Point
        /// </summary>
        public long PointID
        {
            get
            {
                return _pointID;
            }
        }

        /// <summary>
        /// Whether the world coordinate contain coordinate in Units or as Indicies
        /// </summary>
        public WorldCoordinateSystem WorldSystem
        {
            get { return _worldSystem; }
            set { _worldSystem = value; }
        }

        /// <summary>
        /// More information on the type of pick
        /// </summary>
        public PickEventType PickType
        {
            get { return _pickType; }
        }

    }


}

