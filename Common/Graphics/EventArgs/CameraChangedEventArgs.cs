#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/24/2008 9:48:29 AM
// Created by:   j.rowe
//
#endregion

using Kitware.VTK;

namespace QuestIntegrity.Graphics.EventArgs
{
    public delegate void CameraChangedEventHandler(object sender, CameraChangedEventArgs e);
    
    /// <summary>
    /// Event arguments to indicate the camera has changed
    /// </summary>
    public class CameraChangedEventArgs : System.EventArgs
    {
        #region Private Properties

        private readonly vtkCamera _camera;
        private readonly bool _isZooming;
        private readonly bool _isPanning;

        #endregion

        #region Constructors

        public CameraChangedEventArgs(vtkCamera camera)
        {
            _isZooming = false;
            _isPanning = false;
            _camera = camera;
        }

        public CameraChangedEventArgs(vtkCamera camera, bool isPanning, bool isZooming)
        {
            _isZooming = isZooming;
            _isPanning = isPanning;
            _camera = camera;
        }

        #endregion

        #region Public Properties.

        public vtkCamera Camera
        {
            get
            {
                return _camera;
            }
        }

        public bool IsZooming
        {
            get
            {
                return _isZooming;
            }
        }

        public bool IsPanning
        {
            get
            {
                return _isPanning;
            }
        }

        #endregion
    }
}