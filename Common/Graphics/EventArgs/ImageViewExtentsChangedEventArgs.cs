using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics.EventArgs
{
    public delegate void ViewExtentsChangedEventHandler(object sender, ImageViewExtentsChangedEventArgs e);

    /// <summary> Event arguments to indicate the view extents of an image window have changed </summary>
    public class ImageViewExtentsChangedEventArgs : System.EventArgs
    {
        /// <summary> The image-based (1:1) view extents, double to allow partial image points to show. </summary>
        public readonly CartesianBounds<double> ImageViewExtents = new CartesianBounds<double>();
        
        public ImageViewExtentsChangedEventArgs(CartesianBounds<double> viewExtents)
        {
            ImageViewExtents = viewExtents;
        }

        public ImageViewExtentsChangedEventArgs(double xMin, double xMax, double yMin, double yMax)
        {
            ImageViewExtents = new CartesianBounds<double>(xMin, xMax, yMin, yMax, 0, 0);
        }

    }
}