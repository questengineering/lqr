﻿using System.Drawing;

namespace QuestIntegrity.Graphics.EventArgs.PickPositions
{
    /// <summary> Event args for handling world values converted to image coordinates in image windows. </summary>
    public class ImagePositionEventArgs
    {
        public ImagePositionEventArgs(PointF point)
        {
            Point = point;
        }

        /// <summary> X/Y world point scaled by the display window's X/Y scale. </summary>
        public PointF Point { get; private set; }
    }
}
