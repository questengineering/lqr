﻿using System.Drawing;

namespace QuestIntegrity.Graphics.EventArgs.PickPositions
{
    /// <summary> Event arguments to indicate the picked position in unscaled world coordinates with top left being 0,0 </summary>
    public class WorldPositionEventArgs : System.EventArgs
    {
        public readonly PointF WorldPoint;

        public WorldPositionEventArgs(PointF worldPoint)
        {
            WorldPoint = worldPoint;
        }
    }
}
