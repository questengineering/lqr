using System.Drawing;

namespace QuestIntegrity.Graphics.EventArgs.PickPositions
{
    /// <summary> Event arguments to indicate the picked position in screen coordinates with top left being 0,0 </summary>
    public class ScreenPositionEventArgs : System.EventArgs
    {
        public readonly Point ScreenPoint;

        public ScreenPositionEventArgs(Point screenPoint)
        {
            ScreenPoint = screenPoint;   
        }
    }
}