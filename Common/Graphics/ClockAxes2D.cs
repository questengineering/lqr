#region Copyright Quest Integrity Group, LLC 2012

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 6/26/2008 9:17:37 AM
// Created by:   s.macumber
//
// CVS ID Tag - do not change
// $ID:$
//

#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using Kitware.VTK;
using QuestIntegrity.Core.Units;

namespace QuestIntegrity.Graphics
{
    /// <summary>
    ///     Clock axis optimized for the Cross-sectional 2D view
    /// </summary>
    public class ClockAxes2D : IDisposable
    {
        #region Constants

        private const char Degree = '\x00B0'; //degree symbol (ASCII 176) or (hex B0)

        #endregion

        #region Private/Protected Variables

        private bool _disposed;
        private double _innerRadius;
        private double _outerRadius;
        private bool _visible = true;
        private bool _labelsVisible = true;
        private bool _clockVisible = true;

        // Axes actors and assemblies.
        private readonly vtkDiskSource _clock;
        private readonly vtkPolyDataMapper _clockMapper;
        private readonly vtkActor _clockActor;

        private readonly vtkTextActor _text0 = new vtkTextActor();
        private readonly vtkTextActor _text90 = new vtkTextActor();
        private readonly vtkTextActor _text180 = new vtkTextActor();
        private readonly vtkTextActor _text270 = new vtkTextActor();
        private readonly List<vtkTextActor> _labels = new List<vtkTextActor>();

        #endregion

        #region Constructors

        public ClockAxes2D(vtkRenderer ren)
        {
            CrosshairAngleDisplayUnit = CircumferentialLocation.CircumferentialDisplayUnit.Degrees;

            _labels.Add(_text0);
            _labels.Add(_text90);
            _labels.Add(_text180);
            _labels.Add(_text270);

            AxisColor = Color.Black;
            LabelColor = Color.Black;
            FontSize = 12;
            _clock = new vtkDiskSource();
            _clockMapper = vtkPolyDataMapper.New();
            _clockActor = new vtkActor();

            _clock.SetCircumferentialResolution(500);
            _clock.SetRadialResolution(100);

            _clockMapper.SetInputConnection(_clock.GetOutputPort());
            _clockActor.SetMapper(_clockMapper);
            _clockActor.GetProperty().SetColor(AxisColor);

            _text0.SetInput("0");
            _text0.GetTextProperty().SetJustificationToLeft();
            _text0.GetTextProperty().SetVerticalJustificationToBottom();

            _text90.SetInput("90");
            _text90.GetTextProperty().SetJustificationToLeft();
            _text90.GetTextProperty().SetVerticalJustificationToCentered();

            _text180.SetInput("180");
            _text180.GetTextProperty().SetJustificationToLeft();
            _text180.GetTextProperty().SetVerticalJustificationToTop();

            _text270.SetInput("270");
            _text270.GetTextProperty().SetJustificationToRight();
            _text270.GetTextProperty().SetVerticalJustificationToCentered();

#if HIDE_RADIAL_POSITION_DISPLAY
            _text0.SetInput("");
            _text90.SetInput("");
            _text180.SetInput("");
            _text270.SetInput("");
#endif

            foreach (vtkTextActor l in _labels)
            {
                l.GetTextProperty().SetFontSize(FontSize);
                l.SetTextScaleModeToNone();
                l.GetTextProperty().SetColor(LabelColor);
            }

            //Add all the actors
            //ren.AddActor(_clockActor);
            ren.AddActor2D(_text0);
            ren.AddActor2D(_text90);
            ren.AddActor2D(_text180);
            ren.AddActor2D(_text270);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Color of the labels
        /// </summary>
        public Color LabelColor { get; set; }

        /// <summary>
        ///     Color of the axes
        /// </summary>
        public Color AxisColor { get; set; }

        public double InnerRadius
        {
            get
            {
                return _innerRadius;
            }
        }

        public double OuterRadius
        {
            get
            {
                return _outerRadius;
            }
        }

        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                LabelsVisible = value;
                ClockVisible = value;
                _visible = value;
            }
        }

        public bool ClockVisible
        {
            get
            {
                return _clockVisible;
            }
            set
            {
                int visible = value ? 1 : 0;
                _clockActor.SetVisibility(visible);
                _clockVisible = value;
            }
        }

        public bool LabelsVisible
        {
            get
            {
                return _labelsVisible;
            }
            set
            {
                int visible = value ? 1 : 0;
                _text0.SetVisibility(visible);
                _text90.SetVisibility(visible);
                _text180.SetVisibility(visible);
                _text270.SetVisibility(visible);
                _labelsVisible = value;
            }
        }

        /// <summary>
        ///     Label font size in points.
        /// </summary>
        public int FontSize { get; set; }

        public CircumferentialLocation.CircumferentialDisplayUnit CrosshairAngleDisplayUnit { get; set; }

        #endregion

        #region Public Methods

        public void SetSize(double radius)
        {
            _innerRadius = 1.1 * radius;
            _outerRadius = 1.02 * _innerRadius;

            _clock.SetInnerRadius(_innerRadius);
            _clock.SetOuterRadius(_outerRadius);
            _clockActor.SetPosition(0.0D, 0.0D, 0.0D);
        }

        public void Draw(vtkRenderer ren)
        {
            int offset = FontSize - 6; //empirical offset adjustment

            //set text
            if (CrosshairAngleDisplayUnit == CircumferentialLocation.CircumferentialDisplayUnit.Degrees)
            {
                _text0.SetInput("0");
                _text90.SetInput("90");
                _text180.SetInput("180");
                _text270.SetInput("270");
            }
            else
            {
                _text0.SetInput(CrosshairAngleDisplayUnit == CircumferentialLocation.CircumferentialDisplayUnit.OClock ? "12:00" : "0:00");
                _text90.SetInput("3:00");
                _text180.SetInput("6:00");
                _text270.SetInput("9:00");
            }

#if HIDE_RADIAL_POSITION_DISPLAY
                _text0.SetInput("");
                _text90.SetInput("");
                _text180.SetInput("");
                _text270.SetInput("");
#endif

            //0
            double x = 0.0;
            double y = _outerRadius;
            int[] display = ren.WorldToScreenCoordinate(x, y, 0.0D);
            _text0.SetDisplayPosition(display[0] - (FontSize / 4), display[1] + offset);

            //90
            x = _outerRadius * Math.Sin(Math.PI / 2.0D);
            y = _outerRadius * Math.Cos(Math.PI / 2.0D);
            display = ren.WorldToScreenCoordinate(x, y, 0.0D);
            _text90.SetDisplayPosition(display[0] + offset, display[1]);

            //180
            x = 0.0;
            y = -_outerRadius;
            display = ren.WorldToScreenCoordinate(x, y, 0.0D);
            _text180.SetDisplayPosition(display[0] - (int)(FontSize * .75), display[1] - offset);

            //270
            x = _outerRadius * Math.Sin(3.0D * Math.PI / 2.0D);
            y = _outerRadius * Math.Cos(3.0D * Math.PI / 2.0D);
            display = ren.WorldToScreenCoordinate(x, y, 0.0D);
            _text270.SetDisplayPosition(display[0] - offset, display[1]);

            //Adjust display options
            foreach (vtkTextActor l in _labels)
            {
                l.GetTextProperty().SetFontSize(FontSize);
                l.GetTextProperty().SetColor(LabelColor);
            }
            _clockActor.GetProperty().SetColor(AxisColor);

            //ren.Render();  //JTR 3/9/09:  disabling.  Should really be picked up by the "Render" of the parent. "Draw()" methods don't render.
        }

        #endregion

        #region IDispose Pattern Implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // Managed Resources
                if (disposing)
                {
                    // Dispose managed resources.
                    _clock.Dispose();
                    _clockMapper.Dispose();
                    _clockActor.Dispose();
                    _text0.Dispose();
                    _text90.Dispose();
                    _text180.Dispose();
                    _text270.Dispose();
                }
                //Unmanaged resources

                //Set Flag
                _disposed = true;
            }
        }

        #endregion
    }
}