﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: GridLayout.cs,v 1.9 2010/12/08 21:10:59 J.Rowe Exp $
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics.Layout
{
    /// <summary>
    /// Hold information needed to layout a "StripGrid", inherited from GridLayout. Must be inherited for partial implementation in App to add grid building logic. HeaterStripGridLayout for example
    /// </summary>
    public class StripGridLayout : GridLayout
    {
        #region Constructor

        public StripGridLayout()
        {
            RowsSpacing = 10;
            GridStrips = new List<GridStrip>();
        }

        #endregion

        public CartesianPoint Scale = new CartesianPoint(1, 1);
        
        /// <summary> int point distance in world coordinates between rows. Recommend .05*strip thickness </summary> 
        public int RowsSpacing { get; set; }

        /// <summary> Contains the strips that will be utilized. </summary>
        public List<GridStrip> GridStrips { get; set; }
        
        /// <summary> Virtual member to create actors from GridStrip ImageDatas and layout grid strips. By default places strips vertically and spaced according to the RowsSpacing number. </summary>
        public virtual void LayoutStrips()
        {
            if (GridStrips.Count == 0) return;

            //Put the sections in useful order, just in case they're arranged oddly (such as with a parallel for loop)
            List<GridStrip> orderedStrips = GridStrips.OrderBy(gs => gs.StartingSliceIdx).ToList();

            int stripCount = 0;
            foreach (GridStrip strip in orderedStrips)
            {
                int unscaledStartY = stripCount * RowsSpacing + stripCount * NumReadingsPerSlice;
                int unscaledEndY = unscaledStartY + NumReadingsPerSlice;
                strip.StartingPosition = new CartesianPoint(0, unscaledStartY * Scale.Y);
                strip.RowIndex = stripCount;

                List<GridSection> orderedSections = strip.GridSections.OrderBy(gs => gs.StartingSliceIdx).ToList();
                for (int actorCount = 0; actorCount < strip.GridSections.Count; actorCount++)
                {
                    GridSection thisGrid = orderedSections[actorCount];
                    if (!strip.ImageActors.ContainsKey(thisGrid)) 
                        strip.ImageActors.Add(thisGrid, thisGrid.CreateActor(Scalar));
                    vtkImageActor thisActor = strip.ImageActors[thisGrid];
                    thisActor.SetPosition((thisGrid.StartingSliceIdx - strip.StartingSliceIdx) * Scale.X, strip.StartingPosition.Y, 0);
                    thisActor.SetScale(Scale.X, Scale.Y, 1);
                }
                
                strip.EndingPosition = new CartesianPoint(orderedStrips.Last().EndingSliceIdx * Scale.X, unscaledEndY * Scale.Y);
                stripCount++;
            }
        }

        /// <summary> Virtual method to build GridStrips and fill ImageActors. Obviously this can't be done here, so inherit this object and figure out how to fill grids with data. </summary>
        public virtual void BuildStrips(object inspectionFile)
        {
            throw new NotImplementedException("Strip building must be implemented in App-specific implementation of StripGridLayout.");
        }

        public void ResetScale()
        {
            Scale = new CartesianPoint(1,1);
        }

        /// <summary> Scales the actors by the given factor relative to the current scale and lays them out to fit snugly together. Call ResetScale first to start at a [1,1] scale. </summary>
        public void ScaleActors(CartesianPoint scaleFactor)
        {
            Scale = new CartesianPoint(Scale.X * scaleFactor.X, Scale.Y * scaleFactor.Y);
            LayoutStrips();
        }
    }
}
