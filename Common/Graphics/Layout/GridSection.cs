﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics.Layout
{
    /// <summary>
    /// Wrapper class for storing meta data about a grid
    /// piece.  Useful for creating multi-grid composites
    /// such as the Image or Strip Chart view.
    /// 
    /// This also contains a List of ImageData, each corresponding
    /// to a particular scalar.  Each image data has the same
    /// structure, but contains the scalar values.
    /// </summary>
    public class GridSection : IDisposable
    {
        #region Private Fields
        private Guid _id = Guid.NewGuid();
        #endregion

        #region Constructor
        public GridSection()
        {
            Grids = new Dictionary<string, vtkImageData>();
        }
        public GridSection(GridStrip parent) : this()
        {
            Grids = new Dictionary<string, vtkImageData>();
            Parent = parent;
        }
        public GridSection(GridStrip parent, vtkImageData grid, string scalar) : this(parent)
        {
            Grids.Add(scalar, grid);
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Since all Grids have the same topology, need to just access one to get other properties.
        /// </summary>
        private vtkImageData FirstDataGrid
        {
            get
            {
                return Grids.First().Value;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the parent GridStrip.  A "strip" contains
        /// multiple GridSections
        /// </summary>
        /// <value>The parent.</value>
        public GridStrip Parent
        {
            get;
            set;
        }

        public int StartingSliceIdx
        {
            get;
            set;
        }

        public int EndingSliceIdx
        {
            get;
            set;
        }

        public int NumSlices
        {
            get { return EndingSliceIdx - StartingSliceIdx; }
        }

        public CartesianPoint StartingPosition
        {
            get;
            set;
        }

        public CartesianPoint EndingPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Unique identifier for this section.  This is automatically assigned when the object is created.
        /// </summary>
        /// <value>The ID.</value>
        public Guid Id
        {
            get
            {
                return _id;
            }
        }

        /// <summary> The grid section# of this grid in its row, starting at 0 for each row. </summary>
        public int SectionIndex { get; set; }

        public int XCount
        {
            get
            {
                return FirstDataGrid.GetDimensions()[0];
            }
        }

        public int YCount
        {
            get
            {
                return FirstDataGrid.GetDimensions()[1];
            }
        }

        public int YMin
        {
            get
            {
                return (int)FirstDataGrid.GetBounds()[2];
            }
        }

        public int YMax
        {
            get
            {
                return (int)FirstDataGrid.GetBounds()[3];
            }
        }

        public int NumPoints
        {
            get
            {
                return FirstDataGrid.GetNumberOfPoints().ToInt32(); //note: jtr- downcast in 64-bit to int.  Ok as this will be int sized
            }
        }

        /// <summary>
        /// Gets the origin of the Section.  This is used by 
        /// the vtk imagedata and actor to set the location.
        /// </summary>
        /// <value>The origin.</value>
        public CartesianPoint Origin
        {
            get
            {
                return new CartesianPoint(FirstDataGrid.GetOrigin());
            }
        }

        /// <summary>
        /// Gets the position of the grid.  I.e., the starting bounds. 
        /// </summary>
        /// <value>The origin.</value>
        public CartesianPoint Position
        {
            get
            {
                double[] extent = FirstDataGrid.GetBounds();
                return new CartesianPoint(extent[0], extent[2], extent[4]);
            }
        }

        /// <summary>
        /// Gets or sets the desired pixel spacing of the actor.
        /// </summary>
        /// <value>The origin.</value>
        public CartesianPoint Spacing
        {
            get
            {
                return new CartesianPoint(FirstDataGrid.GetSpacing());
            }
        }

        /// <summary>
        /// Stores reference to the image data grids
        /// </summary>
        /// <value>The grids.</value>
        public Dictionary<string, vtkImageData> Grids
        {
            get;
            set;
        }
        #endregion

        #region Public Methods
        /// <summary> Creates an instance of a new actor from the vtkImageData </summary>
        public vtkImageActor CreateActor(string scalar)
        {
            if (!Grids.ContainsKey(scalar)) return null;
            vtkImageActor actor = new vtkImageActor();
            actor.SetInput(Grids[scalar]);           
            return actor;
        }

        /// <summary> Creates a copy of an ImageData grid structure </summary>
        /// <returns></returns>
        public vtkImageData CloneGrid()
        {
            vtkImageData newGrid = new vtkImageData();
            newGrid.CopyStructure(FirstDataGrid);
            return newGrid;
        }
        #endregion

        #region IDispose Pattern Implementation
        private bool _disposed;
        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing) foreach (var grid in Grids) { grid.Value.Dispose(); }
            _disposed = true;
        }
        #endregion

        /// <summary> Determines if a grid section fits the parameters specified to be safely reused. Must have the same startIdx, endIdx, and contain all scalars.</summary>
        public bool IsRecyclable(int startIdx, int endIdx, List<string> scalarNames)
        {
            return StartingSliceIdx == startIdx
                && EndingSliceIdx == endIdx
                && scalarNames.Intersect(Grids.Keys).Count() == scalarNames.Count;
        }
    }
}
