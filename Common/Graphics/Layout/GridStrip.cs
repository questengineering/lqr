﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Created By:  Jim Rowe
// Date:  August 10, 2009
//
// Filename: $Id: GridStrip.cs,v 1.9 2010/09/09 19:25:53 J.Rowe Exp $
//
#endregion

using System;
using System.Collections.Generic;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;

namespace QuestIntegrity.Graphics.Layout
{
    /// <summary>
    /// A strip corresponds to a continguous length of pipe made up of a bunch of actors.
    /// This class contains information used when "strips" are offset in the Y direction, such as used for creating a strip-chart view.
    /// </summary>
    public class GridStrip
    {
        public GridStrip()
        {
            GridSections = new List<GridSection>();
            ImageActors = new Dictionary<GridSection, vtkImageActor>();
        }

        /// <summary> Gets or sets the index of the row. </summary>
        public int RowIndex { get; set; }

        /// <summary> Gets or sets the parent GridLayout.  Used for navigating up the layout. </summary>
        public GridLayout Parent { get; set; }

        public CartesianPoint StartingPosition { get; set; }

        public CartesianPoint EndingPosition { get; set; }

        public int StartingSliceIdx { get; set; }

        public int EndingSliceIdx { get; set; }

        public List<GridSection> GridSections { get; set; }
        public Dictionary<GridSection, vtkImageActor> ImageActors { get; set; }

        /// <summary> Gets a little more friendly string describing the grid strip. </summary>
        public override string ToString()
        {
            return String.Format("Row {0}|StartIdx {1}|EndIdx {2}", RowIndex, StartingSliceIdx, EndingSliceIdx);
        }
    }
}
