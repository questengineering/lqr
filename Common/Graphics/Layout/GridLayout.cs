﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: GridLayout.cs,v 1.9 2010/12/08 21:10:59 J.Rowe Exp $
//
#endregion

using System;

namespace QuestIntegrity.Graphics.Layout
{
    /// <summary> Hold base information needed to layout a simple grid. </summary>
    public class GridLayout
    {
        #region Public Properties
        public virtual int NumRows { get { throw new NotImplementedException("NumRows must be overridden."); } }

        /// <summary> The current scalar utilized internal to the class. Inherited versions of this class should use app-specific typed versions. </summary>
        public string Scalar { get; set; }

        /// <summary> Number of slices per Row </summary>
        public virtual int NumSlicesPerRow { get; set; }

        /// <summary> Number of reading per each slice </summary>
        public int NumReadingsPerSlice { get; protected set;}

        /// <summary> The starting axial index </summary>
        public int StartingIndex { get; set; }

        /// <summary> The ending axial index </summary>
        public int EndingIndex { get; set; }

        #endregion

        #region Public Methods
        
        /// <summary> Copies the meta data, but not the actual strips. </summary>
        public virtual GridLayout ShallowCopy()
        {
            GridLayout copyLayout = new GridLayout
            {
                StartingIndex = StartingIndex,
                EndingIndex = EndingIndex,
                NumReadingsPerSlice = NumReadingsPerSlice,
                NumSlicesPerRow = NumSlicesPerRow,
            };

            return copyLayout;
        }
        #endregion
    }
}
