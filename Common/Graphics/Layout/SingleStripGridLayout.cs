﻿using System;
using System.Linq;
using Kitware.VTK;

namespace QuestIntegrity.Graphics.Layout
{
    public class SingleStripGridLayout : GridLayout
    {
        /// <summary> The number of rows in this grid layout. Always 1. </summary>
        public override int NumRows { get { return 1; } }

        /// <summary> The gridStrip build by the BuildStrip method </summary>
        public GridStrip GridStrip { get; protected set; }

        public virtual void BuildStrip(object inspection, bool clearCache)
        { throw new NotImplementedException("Inherited versions of StripGridLayouts must be able to build their own strips using app-specific sources.");}

        /// <summary> Removes all rows from the renderer and clears out the internal lists.</summary>
        public void RemoveStripsFromRenderer(vtkRenderer renderer)
        {
            if (GridStrip == null || GridStrip.ImageActors.Count == 0) return;
            GridStrip.ImageActors.Values.ToList().ForEach(renderer.RemoveActor);
        }

        /// <summary> Adds the strip to the renderer </summary>
        public void AddStripToRenderer(vtkRenderer renderer)
        {
            GridStrip.ImageActors.Values.ToList().ForEach(a =>
            {
                a.SetInterpolate(0); //always flat interpolation
                renderer.AddActor(a);
            });
        }
    }
}
