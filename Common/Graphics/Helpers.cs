﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QuestIntegrity.Graphics
{
    public static class Helpers
    {
        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
        public enum DeviceCap
        {
            Vertres = 10,
            Desktopvertres = 117,

            // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
        }


        public static float GetScalingFactor()
        {
            var g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
            var desktop = g.GetHdc();
            var logicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.Vertres);
            var physicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.Desktopvertres);

            var screenScalingFactor = (float)physicalScreenHeight / logicalScreenHeight;

            return screenScalingFactor; // 1.25 = 125%
        }
    }
}
