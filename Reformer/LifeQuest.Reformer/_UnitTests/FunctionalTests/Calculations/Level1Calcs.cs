﻿using NUnit.Framework;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess;

namespace Reformer._UnitTests.FunctionalTests.Calculations
{

    [TestFixture, Description("Tests Level 1 calculations for OD data.")]
    class Level1Calcs
    {
        [Test, Description("Tests looking up a tube's status")]
        public void TubeLevel1Status()
        {
            //After a transition point
            var level1Result = RemainingLifeAssessmentLookup.GetArbitraryStatus(10, .208, 2.08, 4, RemainingLifeAssessmentLookup.HPModified);
            Assert.AreEqual(level1Result, RemainingLifeStatus.ConsultEngineering);
            //Right at a transition point.
            level1Result = RemainingLifeAssessmentLookup.GetArbitraryStatus(10, .208, 1.5, 4, RemainingLifeAssessmentLookup.HPModified);
            Assert.AreEqual(level1Result, RemainingLifeStatus.Okay);

            level1Result = RemainingLifeAssessmentLookup.GetArbitraryStatus(5, .296, 1.48, 5, RemainingLifeAssessmentLookup.HPModified);
            Assert.AreEqual(level1Result, RemainingLifeStatus.ConsultEngineering);
        }
    }
}
