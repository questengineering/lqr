﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Reformer.Data.Tubes;

namespace Reformer._UnitTests.FunctionalTests.DataManipulation
{
    [TestFixture, Description("Tests functions that move axial data")]
    class AxialAdjustment
    {
        ReformerTube _firstTube;
        List<float> _originalPositions;
        const float Delta = .0001f;

        [SetUp]
        public void Initialization()
        {
            //Load up a sample project to play with
            UnitTestHelpers.LoadSmallMANTISProject();
            _firstTube = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile.Tubes[0];
            _firstTube.GetPositions(out _originalPositions);
        }

        [TearDown]
        public void CleanUp() { }

        private void ResetPositionData()
        {
            _firstTube.SetPositions(_originalPositions);
        }

        [Test, Description("Tests axial shift of a tube")]
        public void AxialShiftTest()
        {
            const float amountShifted = 5;
            double firstTubeOriginalFirstPoint = _originalPositions.First();
            double firstTubeOriginalLastPoint = _originalPositions.Last();
            
            //perform the shift operation
            _firstTube.ShiftAxialData(amountShifted);
            
            //Get the modified positions
            List<float> adjustedPositions;
            _firstTube.GetPositions(out adjustedPositions);
            double firstTubeAdjustedFirstPoint = adjustedPositions.First();
            double firstTubeAdjustedLastPoint = adjustedPositions.Last();

            Assert.AreEqual(firstTubeOriginalFirstPoint + amountShifted, firstTubeAdjustedFirstPoint, Delta);
            Assert.AreEqual(firstTubeOriginalLastPoint + amountShifted, firstTubeAdjustedLastPoint, Delta);
            ResetPositionData();
        }

        [Test, Description("Tests axial flip of a tube")]
        public void AxialFlipTest()
        {
            double originalFirstPoint = _originalPositions.First();
            double originalLastPoint = _originalPositions.Last();
            int percent25Idx = (int)Math.Round(_originalPositions.Count * .25);
            double original25PerPoint = _originalPositions[percent25Idx];

            //perform the flip operation
            _firstTube.FlipAxialData();

            //Get the modified positions
            List<float> adjustedPositions;
            _firstTube.GetPositions(out adjustedPositions);
            double adjustedFirstPoint = adjustedPositions.First();
            double adjustedLastPoint = adjustedPositions.Last();
            double adjusted25PerPoint = originalLastPoint - adjustedPositions[_originalPositions.Count -1 - percent25Idx];
            //Make sure the first and last points stay the same, but make sure that some point in the middle is been switched. Say... rounded 25%
            
            Assert.AreEqual(originalFirstPoint, adjustedFirstPoint, Delta);
            Assert.AreEqual(original25PerPoint, adjusted25PerPoint, Delta);
            Assert.AreEqual(originalLastPoint, adjustedLastPoint, Delta);
            ResetPositionData();
        }

        [Test, Description("Tests axial shift of a tube")]
        public void AxialStretchTest()
        {
            //Set the position data of the first tube to be 0-20
            var origPos = Enumerable.Range(0, 20).Select(S => (float)S).ToList();
            _firstTube.SetPositions(origPos);

            //perform the shift operation
            _firstTube.StretchAxialData(5, 10, 15.25);

            //Get the modified positions
            List<float> newPos;
            _firstTube.GetPositions(out newPos);
           
            ResetPositionData();
        }
    }
}
