﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using QuestIntegrity.Core.Maths;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer._UnitTests.FunctionalTests.DataManipulation
{
    [TestFixture, Description("Tests functions that move axial data")]
    class RadialShift
    {
        [SetUp]
        public void Initialization() { }

        [TearDown]
        public void CleanUp() { }

        [Test, Description("Tests shifting all sensors simultaneously")]
        public void MeanRadialShiftTest()
        {
            const float amountShifted = .25f;
            const float delta = .00001f;
            //Load up a sample project to play with
            UnitTestHelpers.LoadSmallMANTISProject();
            var firstTube = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile.Tubes[0];
            
            //Get the original data
            List<float> originalMeans = firstTube.Get2DScalarStatisticalData(ReformerScalar.RadiusOutside, StatisticalMeasures.Median).First().Value;
            double firstTubeOriginalFirstPoint = originalMeans[0];
            double firstTubeOriginalLastPoint = originalMeans[originalMeans.Count - 1];
            
            //perform the shift operation
            firstTube.ShiftRadialData(ReformerScalar.RadiusOutside, amountShifted);
            
            //Get the modified data
            List<float> adjustedMeans = firstTube.Get2DScalarStatisticalData(ReformerScalar.RadiusOutside, StatisticalMeasures.Median).First().Value;
            double firstTubeAdjustedFirstPoint = adjustedMeans[0];
            double firstTubeAdjustedLastPoint = adjustedMeans[adjustedMeans.Count - 1];

            Assert.AreEqual(firstTubeOriginalFirstPoint + amountShifted, firstTubeAdjustedFirstPoint, delta);
            Assert.AreEqual(firstTubeOriginalLastPoint + amountShifted, firstTubeAdjustedLastPoint, delta);
        }

        [Test, Description("Tests shifting only one sensor")]
        public void SensorRadialShiftTest()
        {
            const float amountShifted = .25f;
            const float delta = .00001f;
            const int sensor = 1;
            //Load up a sample project to play with
            UnitTestHelpers.LoadSmallMANTISProject();
            var firstTube = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile.Tubes[0];

            //Get the original data
            List<float> originalSensorData;
            firstTube.Get2DScalarForSensor(ReformerScalar.RadiusOutside, sensor, out originalSensorData);
            double firstTubeOriginalFirstPoint = originalSensorData[0];
            double firstTubeOriginalLastPoint = originalSensorData[originalSensorData.Count - 1];

            //perform the shift operation
            firstTube.ShiftRadialSensorData(ReformerScalar.RadiusOutside, sensor, amountShifted);

            //Get the modified data
            List<float> adjustedSensorData;
            firstTube.Get2DScalarForSensor(ReformerScalar.RadiusOutside, sensor, out adjustedSensorData);
            double firstTubeAdjustedFirstPoint = adjustedSensorData[0];
            double firstTubeAdjustedLastPoint = adjustedSensorData[adjustedSensorData.Count - 1];

            Assert.AreEqual(firstTubeOriginalFirstPoint + amountShifted, firstTubeAdjustedFirstPoint, delta);
            Assert.AreEqual(firstTubeOriginalLastPoint + amountShifted, firstTubeAdjustedLastPoint, delta);
        }

    }
}
