﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Reformer.Data;
using Reformer.Data.DataManager;
using Reformer.Data.InspectionFile;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;

namespace Reformer._UnitTests.FunctionalTests.DataImporting
{
    [TestFixture, Description("Tests functions that import data from raw files to verify read mechanisms.")]
    public class DataImportingTests
    {
        private const string ServerPath = UnitTestHelpers.ServerTestPath;
        
        /// <summary> Creates a local copy of the file in a temporary file location and returns it. </summary>
        private FileInfo CreateTempFile(string FullFileName) { return new FileInfo(FullFileName).CopyTo(Path.Combine(Path.GetTempPath(), Path.GetFileName(FullFileName)) , true); }
        
        [SetUp]
        public void Initialization()
        {
             
        }

        [TearDown]
        public void CleanUp() { UnitTestHelpers.DeleteDefaultProjectDirectory(); }

        [Test, Description("Tests import of a MANTIS 8 sensor CSV File")]
        public void MantisCSVTest()
        {
            //Get the test file and datareader.
            const string fileName = "R01T001.csv";
            var newFile = CreateTempFile(Path.Combine(ServerPath, "MANTISCSV" ,fileName));
            
            //Setup an inspection and tube to put it in.
            var tmpInspection = UnitTestHelpers.CreateDefaultInspection();
            var rawFile = new RawFileInfo(newFile);
            Task loading = LifeQuestReformer.Instance.TheDataManager.LoadDataFilesAsync(new List<RawFileInfo> {rawFile}, new ProgressToken());
            loading.Wait();
            var tmpTube = tmpInspection.Tubes[0];
            Assert.AreEqual(tmpTube.StartPosition, 0d, .001);
            Assert.AreEqual(tmpTube.EndPosition, 397.24658, .001);
        }

        [Test, Description("Tests import of a LOTIS mean CSV File")]
        [TestCase("R01T001.csv", 0, 589, Category = "File Importing", Description = "Standard CSV file. Nothing wrong with it that I know of.")]
        [TestCase("R01T002.csv", 0, 497.6471, Category = "File Importing", Description = "CSV file that had the tool wiggle after zeroing. (has 2 zero points)")]
        public void LotisCSVTest(string fileName, double startPosition, double endPosition)
        {
            //Get the test file and datareader.
            var insp = LoadDefaultLOTISInspection();
            var newFile = CreateTempFile(Path.Combine(ServerPath, "LOTISCSV", fileName));
            var rawFile = new RawFileInfo(newFile);

            LifeQuestReformer.Instance.TheDataManager.LoadDataFilesAsync(new List<RawFileInfo> { rawFile }, new ProgressToken()).Wait();

            var tmpTube = insp.Tubes.Last();
            Assert.AreEqual(tmpTube.StartPosition, startPosition, .0001);
            Assert.AreEqual(tmpTube.EndPosition, endPosition, .0001);
        }

        [Test, Description("Tests import of a LOTIS .LOT File")]
        [TestCase("R01T001.LOT", 0, 536.696, Category = "File Importing", Description = "Standard LOT file. Nothing wrong with it that I know of.")]
        public void LotisTubTest(string fileName, double startPosition, double endPosition)
        {
            //Get the test file and datareader.
            var insp = LoadDefaultLOTISInspection();
            var newFile = CreateTempFile(Path.Combine(ServerPath, "LOTISLOT", fileName));
            var rawFile = new RawFileInfo(newFile);
            LifeQuestReformer.Instance.TheDataManager.LoadDataFilesAsync(new List<RawFileInfo> { rawFile }, new ProgressToken()).Wait();
            var tmpTube = insp.Tubes.Last();
            Assert.AreEqual(startPosition, tmpTube.StartPosition);
            Assert.AreEqual(endPosition, tmpTube.EndPosition, .001);
        }

        private ReformerInspection LoadDefaultLOTISInspection()
        {
            //Setup an inspection and tube to put it in.
            var tmpInspection = UnitTestHelpers.CreateDefaultInspection();
            tmpInspection.InspectionTool = Tool.LOTIS;
            return tmpInspection;
        }
    }
}
