﻿using System.Collections.Generic;
using NUnit.Framework;
using QuestIntegrity.Core.Maths;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Visualization;

namespace Reformer._UnitTests.FunctionalTests.Visualization
{
    [TestFixture, Description("Tests functions that create 3D Visualizations")]
    class vtk3D
    {
        ReformerInspection _insp;
        ReformerTube _firstTube;

        [SetUp]
        public void Initialization()
        {
            //Load up a sample project to play with
            UnitTestHelpers.LoadSmallMANTISProject();
            _insp = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;
            _firstTube = _insp.Tubes[0];
        }

        [TearDown]
        public void CleanUp() { }

        [Test, Description("Tests OuterRadius 3 Dimensional rendering")]
        public void OuterRadius3D()
        {
            int numPoints = _insp.NumRadialReadings * 4;
            var actor = VTKTubeCreatorBase.Get3DTube(_firstTube, ReformerScalar.RadiusOutside, 1, numPoints);

            List<float> positions;
            _firstTube.GetPositions(out positions);
            int numPointsInGrid = positions.Count * (numPoints + 1); //add 1 for the wrapping point per slice.

            Assert.AreEqual(numPointsInGrid, actor.GetMapper().GetInput().GetNumberOfPoints(), "Number of Grid Points are different.");
            Assert.NotNull(actor);
        }

        [Test, Description("Tests OuterRadiusMean 3 Dimensional rendering")]
        public void OuterRadiusMean3D()
        {
            int numPoints = _insp.NumRadialReadings * 4;
            var actor = VTKTubeCreatorBase.Get3DTube(_firstTube, ReformerScalar.RadiusOutside, StatisticalMeasures.Median, 1, numPoints);

            List<float> positions;
            _firstTube.GetPositions(out positions);
            int numPointsInGrid = positions.Count * (numPoints + 1); //add 1 for the wrapping point per slice.

            Assert.AreEqual(numPointsInGrid, actor.GetMapper().GetInput().GetNumberOfPoints(), "Number of Grid Points are different.");
            Assert.NotNull(actor);
        }

        [Test, Description("Tests OuterDiameter 3 Dimensional rendering")]
        public void OuterDiameter3D()
        {
            int numPoints = _insp.NumRadialReadings * 4;
            var actor = VTKTubeCreatorBase.Get3DDiameterTube(_firstTube, ReformerScalar.RadiusOutside, 1, numPoints);

            List<float> positions;
            _firstTube.GetPositions(out positions);
            int numPointsInGrid = positions.Count * (numPoints + 1); //add 1 for the wrapping point per slice.

            Assert.AreEqual(numPointsInGrid, actor.GetMapper().GetInput().GetNumberOfPoints(), "Number of Grid Points are different.");
            Assert.NotNull(actor);
        }

        [Test, Description("Tests Preview Grid 3D rendering")]
        public void PreviewGrid3D()
        {
            int numPoints = _insp.NumRadialReadings * 4;
            var grid = VTKTubeCreatorBase.CreatePreviewTubeGrid(_firstTube.StartPosition, _firstTube.EndPosition, numPoints, 5, .5);
            var actor = VTKTubeCreatorBase.MapGridToActor(grid);

            Assert.AreEqual(132, grid.GetNumberOfPoints(), "Number of Grid Points are different.");
            Assert.AreEqual(2, grid.GetDimensions()[0], "Grid dimensions (X) are different.");
            Assert.NotNull(actor);
        }

    }
}
