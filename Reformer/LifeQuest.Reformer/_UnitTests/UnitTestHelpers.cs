﻿using System.IO;
using Reformer.Data.InspectionFile;

namespace Reformer._UnitTests
{
    class UnitTestHelpers
    {
        public const string ServerTestPath = @"\\boul-backup-01\Data\Software\Projects\LifeQuest\LifeQuest Reformer\Testing";
        public static string SampleProjectPath { get { return Path.Combine(ServerTestPath, "SampleProjects"); } }

        public static string DefaultProjectDir { get { return Path.Combine(Path.GetTempPath(), "DefaultProjects"); } }


        /// <summary> Creates an empty inspection file with a default project to contain it. </summary>
        public static ReformerInspection CreateDefaultInspection()
        {
            DirectoryInfo defaultProjectDirInfo = new DirectoryInfo(DefaultProjectDir);
            if (!defaultProjectDirInfo.Exists) defaultProjectDirInfo.Create();
            LifeQuestReformer.Instance.TheDataManager.NewProject(Path.Combine(DefaultProjectDir, "TempProj.prj"));
            return LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;
        }

        /// <summary> Deletes the default project directory. </summary>
        public static void DeleteDefaultProjectDirectory()
        {
            DirectoryInfo projDir = new DirectoryInfo(DefaultProjectDir);
            if (projDir.Exists)
                projDir.Delete(true);
        }

        public static void LoadSmallMANTISProject()
        {
            string tmpProjFolder = CopyProjectDirToTemp(Path.Combine(SampleProjectPath, "SmallMantis"));
            LifeQuestReformer.Instance.TheDataManager.LoadProject(Path.Combine(tmpProjFolder, "SmallMantis.prjr"));
        }

        public static string CopyProjectDirToTemp(string TheDirectory)
        {
            string tmpDir = Path.GetTempPath();
            if (!Directory.Exists(tmpDir)) { Directory.CreateDirectory(tmpDir); }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = new DirectoryInfo(TheDirectory).GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(tmpDir, file.Name);
                file.CopyTo(temppath, true);
            }
            return tmpDir;
        }
    }
}
