﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/23/2012 1:23:25 PM
// Created by:   J.Foster
//
#endregion

using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Licensing;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.AutomaticUpdates;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.DataManager;
using Reformer.Data.Features;
using Reformer.Data.Materials;
using Reformer.Forms.Startup;
using Reformer.Resources;
using Reformer.ScreenLayout;

namespace Reformer
{
    public class LifeQuestReformer : LifeQuestApplicationBase
    {
        #region Private/Protected Properties

        private readonly ReformerSettings _reformerSettings;
        private readonly ReformerDataManager _dataManager = new ReformerDataManager();
        public readonly static LifeQuestReformer Instance = new LifeQuestReformer();
        
        #endregion

        #region Constructors
        public LifeQuestReformer()
        {
            _reformerSettings = new ReformerSettings();
            Layout = ReformerScreenLayout.Instance;
            //Just need to initialize since it will be accessed through the base class Singleton instance
            // ReSharper disable UnusedVariable
            var theGroupings = new ReformerFeatureTypeGroupings();
            var theMaterials = new ReformerMaterialsList();
            // ReSharper restore UnusedVariable

            //Just set the default AutoUpdate target for now. There is no other building yet.
            AutoUpdater.Instance.CurrentUpdateTarget = AutoUpdater.Config.Development;
            
            //Set the default units on load to inches, per Tim's request.
            DisplayUnits.Instance.AxialDistanceUnits = new Length(1.0, Length.LengthScale.Inches);
        }

        public new ReformerScreenLayout Layout
        {
            get { return base.Layout as ReformerScreenLayout; }
            set { base.Layout = value; }
        }

        #endregion

        #region Public Properties

        public ISettings ProductSettings
        {
            get { return _reformerSettings; }
        }

        public static string CompanyName { get { return Properties.Resources.CompanyName; } }

        public static string Product
        {
            get
            {
                return ((AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyProductAttribute))).Product;
            }
        }

        public Icon AppIcon
        {
            get
            {
                return ApplicationIcons.LifeQuest_128x113;
            }
        }

        public Image AppLogo
        {
            get { return ApplicationIcons.ReformerLogo; }
        }

        public Image AboutLogo
        {
            get
            {
                return ApplicationIcons.ReformerLogo;
            }
        }

        public string RegistryPath
        {
            get
            {
                const string staticPortion = @"Software\\Quest Integrity Group, LLC\\LifeQuest Reformer\\";
                string platform = Environment.Is64BitProcess ? "x64" : "x86";
                return string.Format("{0}{1}", staticPortion, platform);
            }
        }

        public ReformerDataManager TheDataManager
        {
            get { return _dataManager; }
        }

        public string ProjectFileName { get; set; }

        #endregion

        public void ShowLicenseError()
        {
            new LicenseError().ShowDialog();
        }

        public void ShowExpiryDateMessage()
        {
            string msg = string.Format("Your license will expire on {0}.  Please contact {1}.", LicenseManager.Instance.ExpirationDate.ToString("yyyy-MM-dd"), Properties.Resources.EmailTo);
            MessageBox.Show(msg, @"Expiring License", MessageBoxButtons.OK);
            new About().ShowDialog();
        }

        public ProductType GetProductType(out string ProductName)
        {
            ProductName = "Reformer";
            return ProductType.Product_LQR;
        }

        
    }
}
