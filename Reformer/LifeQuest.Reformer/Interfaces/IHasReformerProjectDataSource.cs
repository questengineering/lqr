#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 7/16/2008 8:20:04 AM
// Created by:   j.rowe
//
#endregion

using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.Project;

namespace Reformer.Interfaces
{
    /// <summary> Thread-safe Interface for a window that is tied to an inspection file. </summary>
    public interface IHasReformerProjectDataSource : IHasDataSource
    {
        /// <summary> Gets or sets a reference to the current project. </summary>
        ReformerProjectInfo CurrentProject { get; }
    }
}
