﻿namespace Reformer.Forms.SideBarControls
{
    sealed partial class ReformerProjectProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.uiOffices = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uiDateCreated = new DevExpress.XtraEditors.TextEdit();
            this.ProjectSource = new System.Windows.Forms.BindingSource(this.components);
            this.uiFileName = new DevExpress.XtraEditors.TextEdit();
            this.uiProjectState = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.uiProjectDate = new DevExpress.XtraEditors.DateEdit();
            this.uiEngineer = new DevExpress.XtraEditors.TextEdit();
            this.uiAnalyst = new DevExpress.XtraEditors.TextEdit();
            this.uiDescription = new DevExpress.XtraEditors.MemoEdit();
            this.uiProjectName = new DevExpress.XtraEditors.TextEdit();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciProjectName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFileName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciProjectState = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciProjectDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciEngineer = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciAnalyst = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciOffices = new DevExpress.XtraLayout.LayoutControlItem();
            this.BSInspectionFiles = new System.Windows.Forms.BindingSource(this.components);
            this.lciOffices = new DevExpress.XtraLayout.LayoutControlItem();
            this.BSInspectionFiles = new System.Windows.Forms.BindingSource(this.components);
            this.BSInspectionFiles = new System.Windows.Forms.BindingSource(this.components);
            this.uiOffices = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lciOffices = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiOffices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiOffices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateCreated.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiEngineer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnalyst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEngineer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAnalyst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspectionFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiOffices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOffices)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.uiOffices);
            this.lcMain.Controls.Add(this.uiDateCreated);
            this.lcMain.Controls.Add(this.uiFileName);
            this.lcMain.Controls.Add(this.uiProjectState);
            this.lcMain.Controls.Add(this.uiProjectDate);
            this.lcMain.Controls.Add(this.uiEngineer);
            this.lcMain.Controls.Add(this.uiAnalyst);
            this.lcMain.Controls.Add(this.uiDescription);
            this.lcMain.Controls.Add(this.uiProjectName);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2338, 337, 250, 350);
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(325, 480);
            this.lcMain.TabIndex = 4;
            this.lcMain.Text = "layoutControl1";
            // 
            // uiOffices
            // 
            this.uiOffices.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "ProjectOffice", true));
            this.uiOffices.Location = new System.Drawing.Point(77, 456);
            this.uiOffices.Name = "uiOffices";
            this.uiOffices.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiOffices.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.uiOffices.Size = new System.Drawing.Size(244, 20);
            this.uiOffices.StyleController = this.lcMain;
            this.uiOffices.TabIndex = 14;
            // 
            // uiDateCreated
            // 
            this.uiDateCreated.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "DateCreated", true));
            this.uiDateCreated.Location = new System.Drawing.Point(77, 432);
            this.uiDateCreated.Name = "uiDateCreated";
            this.uiDateCreated.Properties.ReadOnly = true;
            this.uiDateCreated.Size = new System.Drawing.Size(244, 20);
            this.uiDateCreated.StyleController = this.lcMain;
            this.uiDateCreated.TabIndex = 13;
            // 
            // ProjectSource
            // 
            this.ProjectSource.DataSource = typeof(Reformer.Data.Project.ReformerProjectInfo);
            // 
            // uiFileName
            // 
            this.uiFileName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "FilePath", true));
            this.uiFileName.Location = new System.Drawing.Point(77, 408);
            this.uiFileName.Name = "uiFileName";
            this.uiFileName.Properties.ReadOnly = true;
            this.uiFileName.Size = new System.Drawing.Size(244, 20);
            this.uiFileName.StyleController = this.lcMain;
            this.uiFileName.TabIndex = 12;
            // 
            // uiProjectState
            // 
            this.uiProjectState.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "ProjectStatus", true));
            this.uiProjectState.Location = new System.Drawing.Point(77, 384);
            this.uiProjectState.Name = "uiProjectState";
            this.uiProjectState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiProjectState.Size = new System.Drawing.Size(244, 20);
            this.uiProjectState.StyleController = this.lcMain;
            this.uiProjectState.TabIndex = 11;
            // 
            // uiProjectDate
            // 
            this.uiProjectDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "ProjectDate", true));
            this.uiProjectDate.EditValue = null;
            this.uiProjectDate.Location = new System.Drawing.Point(77, 360);
            this.uiProjectDate.Name = "uiProjectDate";
            this.uiProjectDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiProjectDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uiProjectDate.Size = new System.Drawing.Size(244, 20);
            this.uiProjectDate.StyleController = this.lcMain;
            this.uiProjectDate.TabIndex = 10;
            // 
            // uiEngineer
            // 
            this.uiEngineer.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "EngineerName", true));
            this.uiEngineer.Location = new System.Drawing.Point(77, 336);
            this.uiEngineer.Name = "uiEngineer";
            this.uiEngineer.Size = new System.Drawing.Size(244, 20);
            this.uiEngineer.StyleController = this.lcMain;
            this.uiEngineer.TabIndex = 9;
            // 
            // uiAnalyst
            // 
            this.uiAnalyst.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "AnalystName", true));
            this.uiAnalyst.Location = new System.Drawing.Point(77, 312);
            this.uiAnalyst.Name = "uiAnalyst";
            this.uiAnalyst.Size = new System.Drawing.Size(244, 20);
            this.uiAnalyst.StyleController = this.lcMain;
            this.uiAnalyst.TabIndex = 8;
            // 
            // uiDescription
            // 
            this.uiDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "Description", true));
            this.uiDescription.Location = new System.Drawing.Point(4, 44);
            this.uiDescription.Name = "uiDescription";
            this.uiDescription.Size = new System.Drawing.Size(317, 264);
            this.uiDescription.StyleController = this.lcMain;
            this.uiDescription.TabIndex = 7;
            // 
            // uiProjectName
            // 
            this.uiProjectName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ProjectSource, "ProjectName", true));
            this.uiProjectName.Location = new System.Drawing.Point(77, 4);
            this.uiProjectName.Name = "uiProjectName";
            this.uiProjectName.Size = new System.Drawing.Size(244, 20);
            this.uiProjectName.StyleController = this.lcMain;
            this.uiProjectName.TabIndex = 4;
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "layoutControlGroup1";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciProjectName,
            this.lciFileName,
            this.lciDateCreated,
            this.lciProjectState,
            this.lciProjectDate,
            this.lciEngineer,
            this.lciAnalyst,
            this.lciDescription,
            this.lciOffices});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.lcgMain.Size = new System.Drawing.Size(325, 480);
            this.lcgMain.TextVisible = false;
            // 
            // lciProjectName
            // 
            this.lciProjectName.Control = this.uiProjectName;
            this.lciProjectName.CustomizationFormText = "Project Name:";
            this.lciProjectName.Location = new System.Drawing.Point(0, 0);
            this.lciProjectName.Name = "lciProjectName";
            this.lciProjectName.Size = new System.Drawing.Size(321, 24);
            this.lciProjectName.Text = "Project Name:";
            this.lciProjectName.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciFileName
            // 
            this.lciFileName.Control = this.uiFileName;
            this.lciFileName.CustomizationFormText = "File Name:";
            this.lciFileName.Location = new System.Drawing.Point(0, 404);
            this.lciFileName.Name = "lciFileName";
            this.lciFileName.Size = new System.Drawing.Size(321, 24);
            this.lciFileName.Text = "File Name:";
            this.lciFileName.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciDateCreated
            // 
            this.lciDateCreated.Control = this.uiDateCreated;
            this.lciDateCreated.CustomizationFormText = "Date Created:";
            this.lciDateCreated.Location = new System.Drawing.Point(0, 428);
            this.lciDateCreated.Name = "lciDateCreated";
            this.lciDateCreated.Size = new System.Drawing.Size(321, 24);
            this.lciDateCreated.Text = "Date Created:";
            this.lciDateCreated.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciProjectState
            // 
            this.lciProjectState.Control = this.uiProjectState;
            this.lciProjectState.CustomizationFormText = "Project State:";
            this.lciProjectState.Location = new System.Drawing.Point(0, 380);
            this.lciProjectState.Name = "lciProjectState";
            this.lciProjectState.Size = new System.Drawing.Size(321, 24);
            this.lciProjectState.Text = "Project State:";
            this.lciProjectState.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciProjectDate
            // 
            this.lciProjectDate.Control = this.uiProjectDate;
            this.lciProjectDate.CustomizationFormText = "Project Date:";
            this.lciProjectDate.Location = new System.Drawing.Point(0, 356);
            this.lciProjectDate.Name = "lciProjectDate";
            this.lciProjectDate.Size = new System.Drawing.Size(321, 24);
            this.lciProjectDate.Text = "Project Date:";
            this.lciProjectDate.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciEngineer
            // 
            this.lciEngineer.Control = this.uiEngineer;
            this.lciEngineer.CustomizationFormText = "Engineer:";
            this.lciEngineer.Location = new System.Drawing.Point(0, 332);
            this.lciEngineer.Name = "lciEngineer";
            this.lciEngineer.Size = new System.Drawing.Size(321, 24);
            this.lciEngineer.Text = "Engineer:";
            this.lciEngineer.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciAnalyst
            // 
            this.lciAnalyst.Control = this.uiAnalyst;
            this.lciAnalyst.CustomizationFormText = "Analyst:";
            this.lciAnalyst.Location = new System.Drawing.Point(0, 308);
            this.lciAnalyst.Name = "lciAnalyst";
            this.lciAnalyst.Size = new System.Drawing.Size(321, 24);
            this.lciAnalyst.Text = "Analyst:";
            this.lciAnalyst.TextSize = new System.Drawing.Size(70, 13);
            // 
            // lciDescription
            // 
            this.lciDescription.Control = this.uiDescription;
            this.lciDescription.CustomizationFormText = "Description:";
            this.lciDescription.Location = new System.Drawing.Point(0, 24);
            this.lciDescription.Name = "lciDescription";
            this.lciDescription.Size = new System.Drawing.Size(321, 284);
            this.lciDescription.Text = "Description:";
            this.lciDescription.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciDescription.TextSize = new System.Drawing.Size(70, 13);
            // 
            // BSInspectionFiles
            // 
            this.BSInspectionFiles.DataSource = typeof(Reformer.Data.InspectionFile.ReformerInspection);

            // 
            // lciOffices
            // 
            this.lciOffices.Control = this.uiOffices;
            this.lciOffices.Location = new System.Drawing.Point(0, 452);
            this.lciOffices.Name = "lciOffices";
            this.lciOffices.Size = new System.Drawing.Size(321, 24);
            this.lciOffices.Text = "Project Office:";
            this.lciOffices.TextSize = new System.Drawing.Size(70, 13);
            // 
            // ReformerProjectProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "ReformerProjectProperties";
            this.Size = new System.Drawing.Size(325, 480);
            this.TabText = "Properties";
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiOffices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateCreated.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiEngineer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnalyst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProjectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEngineer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAnalyst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspectionFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiOffices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOffices)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraEditors.DateEdit uiProjectDate;
        private DevExpress.XtraEditors.TextEdit uiEngineer;
        private DevExpress.XtraEditors.TextEdit uiAnalyst;
        private DevExpress.XtraEditors.MemoEdit uiDescription;
        private DevExpress.XtraEditors.TextEdit uiProjectName;
        private DevExpress.XtraEditors.ImageComboBoxEdit uiProjectState;
        private DevExpress.XtraEditors.TextEdit uiFileName;
        private DevExpress.XtraEditors.TextEdit uiDateCreated;
        private System.Windows.Forms.BindingSource ProjectSource;
        private System.Windows.Forms.BindingSource BSInspectionFiles;
        private DevExpress.XtraLayout.LayoutControlItem lciProjectName;
        private DevExpress.XtraLayout.LayoutControlItem lciFileName;
        private DevExpress.XtraLayout.LayoutControlItem lciDateCreated;
        private DevExpress.XtraLayout.LayoutControlItem lciProjectState;
        private DevExpress.XtraLayout.LayoutControlItem lciProjectDate;
        private DevExpress.XtraLayout.LayoutControlItem lciEngineer;
        private DevExpress.XtraLayout.LayoutControlItem lciAnalyst;
        private DevExpress.XtraLayout.LayoutControlItem lciDescription;
        private DevExpress.XtraEditors.ComboBoxEdit uiOffices;
        private DevExpress.XtraLayout.LayoutControlItem lciOffices;
    }
}