﻿using System.Windows.Forms;
using QuestIntegrity.LifeQuest.Data;

namespace Reformer.Forms.SideBarControls
{
    public sealed partial class ReformerProjectProperties : ReformerBaseForm
    {
        public ReformerProjectProperties()
        {
            InitializeComponent();
            InitializeData();
            Dock = DockStyle.Fill; //Set the form to fill whatever form it is placed in.
        }

        private void InitializeData()
        {
            if (CurrentProject == null) return;
            UpdateDataSource();
            
            //Add in the options for Project Status
            uiProjectState.Properties.Items.Clear();
            uiProjectState.Properties.Items.AddEnum(typeof(ProjectStatus));
            uiOffices.Properties.Items.Clear();
            foreach (string s in QIGAddressLookup.GetOfficeList())
            {
                uiOffices.Properties.Items.Add(s);
            }
        }

        public override void UpdateDataSource()
        {
            ProjectSource.DataSource = CurrentProject;
        }
    }
}
