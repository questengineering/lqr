﻿using DevExpress.XtraBars;

namespace Reformer.Forms
{
    partial class ReformerMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReformerMain));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.statusBar = new DevExpress.XtraBars.Bar();
            this.TheProgressBar = new DevExpress.XtraBars.BarEditItem();
            this.riProgressBar = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.TheStatusText = new DevExpress.XtraBars.BarStaticItem();
            this.uiVersionNum = new DevExpress.XtraBars.BarStaticItem();
            this.MainMenu = new DevExpress.XtraBars.Bar();
            this.FileMenu = new DevExpress.XtraBars.BarSubItem();
            this.FileProjectMenu = new DevExpress.XtraBars.BarSubItem();
            this.FileProjectNewProject = new DevExpress.XtraBars.BarButtonItem();
            this.FileProjectOpenProject = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.ListRecentProjects = new DevExpress.XtraBars.BarListItem();
            this.FileProjectSaveProject = new DevExpress.XtraBars.BarButtonItem();
            this.FileCloseProject = new DevExpress.XtraBars.BarButtonItem();
            this.btnRepackDataFile = new DevExpress.XtraBars.BarButtonItem();
            this.FileExit = new DevExpress.XtraBars.BarButtonItem();
            this.tmpViewerWindow = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.OptionsUnits = new DevExpress.XtraBars.BarButtonItem();
            this.OptionsColorScale = new DevExpress.XtraBars.BarButtonItem();
            this.barTools = new DevExpress.XtraBars.BarSubItem();
            this.toolsMANTISShimAdjustment = new DevExpress.XtraBars.BarButtonItem();
            this.Help = new DevExpress.XtraBars.BarSubItem();
            this.HelpReportBug = new DevExpress.XtraBars.BarButtonItem();
            this.HelpAbout = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.FileInspectionAddInspectionData = new DevExpress.XtraBars.BarButtonItem();
            this.HelpReleaseNotes = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riProgressBar)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.statusBar,
            this.MainMenu});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.DockManager = this.dockManager;
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.FileMenu,
            this.FileProjectMenu,
            this.FileProjectNewProject,
            this.FileProjectOpenProject,
            this.FileInspectionAddInspectionData,
            this.FileCloseProject,
            this.FileExit,
            this.TheProgressBar,
            this.TheStatusText,
            this.FileProjectSaveProject,
            this.Help,
            this.btnRepackDataFile,
            this.uiVersionNum,
            this.barSubItem1,
            this.OptionsUnits,
            this.HelpAbout,
            this.HelpReportBug,
            this.barSubItem2,
            this.ListRecentProjects,
            this.OptionsColorScale,
            this.tmpViewerWindow,
            this.barTools,
            this.toolsMANTISShimAdjustment,
            this.HelpReleaseNotes});
            this.barManager.MainMenu = this.MainMenu;
            this.barManager.MaxItemId = 40;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riProgressBar});
            this.barManager.StatusBar = this.statusBar;
            // 
            // statusBar
            // 
            this.statusBar.BarName = "Status bar";
            this.statusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.statusBar.DockCol = 0;
            this.statusBar.DockRow = 0;
            this.statusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.statusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.TheProgressBar),
            new DevExpress.XtraBars.LinkPersistInfo(this.TheStatusText),
            new DevExpress.XtraBars.LinkPersistInfo(this.uiVersionNum)});
            this.statusBar.OptionsBar.AllowQuickCustomization = false;
            this.statusBar.OptionsBar.DrawDragBorder = false;
            this.statusBar.OptionsBar.UseWholeRow = true;
            this.statusBar.Text = "Status bar";
            // 
            // TheProgressBar
            // 
            this.TheProgressBar.Caption = "TheProgressBar";
            this.TheProgressBar.Edit = this.riProgressBar;
            this.TheProgressBar.EditValue = "0";
            this.TheProgressBar.Id = 10;
            this.TheProgressBar.Name = "TheProgressBar";
            this.TheProgressBar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.TheProgressBar.Width = 140;
            // 
            // riProgressBar
            // 
            this.riProgressBar.Name = "riProgressBar";
            // 
            // TheStatusText
            // 
            this.TheStatusText.Id = 11;
            this.TheStatusText.Name = "TheStatusText";
            this.TheStatusText.TextAlignment = System.Drawing.StringAlignment.Near;
            this.TheStatusText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // uiVersionNum
            // 
            this.uiVersionNum.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.uiVersionNum.Caption = "V: X.X.X.X";
            this.uiVersionNum.Id = 27;
            this.uiVersionNum.Name = "uiVersionNum";
            this.uiVersionNum.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // MainMenu
            // 
            this.MainMenu.BarName = "Main Menu";
            this.MainMenu.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.MainMenu.DockCol = 0;
            this.MainMenu.DockRow = 0;
            this.MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.FileMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTools),
            new DevExpress.XtraBars.LinkPersistInfo(this.Help)});
            this.MainMenu.OptionsBar.AllowQuickCustomization = false;
            this.MainMenu.OptionsBar.DrawDragBorder = false;
            this.MainMenu.OptionsBar.MultiLine = true;
            this.MainMenu.OptionsBar.UseWholeRow = true;
            this.MainMenu.Text = "Custom 3";
            // 
            // FileMenu
            // 
            this.FileMenu.Caption = "File";
            this.FileMenu.Id = 1;
            this.FileMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.FileProjectMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRepackDataFile, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileExit),
            new DevExpress.XtraBars.LinkPersistInfo(this.tmpViewerWindow)});
            this.FileMenu.Name = "FileMenu";
            // 
            // FileProjectMenu
            // 
            this.FileProjectMenu.Caption = "Project";
            this.FileProjectMenu.Id = 2;
            this.FileProjectMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.FileProjectNewProject),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileProjectOpenProject),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileProjectSaveProject),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileCloseProject)});
            this.FileProjectMenu.Name = "FileProjectMenu";
            // 
            // FileProjectNewProject
            // 
            this.FileProjectNewProject.Caption = "New Project";
            this.FileProjectNewProject.Id = 3;
            this.FileProjectNewProject.Name = "FileProjectNewProject";
            this.FileProjectNewProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileProjectNewProject_ItemClick);
            // 
            // FileProjectOpenProject
            // 
            this.FileProjectOpenProject.Caption = "Open Project";
            this.FileProjectOpenProject.Id = 4;
            this.FileProjectOpenProject.Name = "FileProjectOpenProject";
            this.FileProjectOpenProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileProjectOpenProject_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Recent Projects";
            this.barSubItem2.Id = 33;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ListRecentProjects)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // ListRecentProjects
            // 
            this.ListRecentProjects.Caption = "Recent Projects";
            this.ListRecentProjects.Id = 34;
            this.ListRecentProjects.Name = "ListRecentProjects";
            // 
            // FileProjectSaveProject
            // 
            this.FileProjectSaveProject.Caption = "Save Project";
            this.FileProjectSaveProject.Enabled = false;
            this.FileProjectSaveProject.Id = 12;
            this.FileProjectSaveProject.Name = "FileProjectSaveProject";
            this.FileProjectSaveProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileProjectSaveProject_ItemClick);
            // 
            // FileCloseProject
            // 
            this.FileCloseProject.Caption = "Close Project";
            this.FileCloseProject.Enabled = false;
            this.FileCloseProject.Id = 8;
            this.FileCloseProject.Name = "FileCloseProject";
            this.FileCloseProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileCloseProject_ItemClick);
            // 
            // btnRepackDataFile
            // 
            this.btnRepackDataFile.Caption = "Repack Data File";
            this.btnRepackDataFile.Id = 26;
            this.btnRepackDataFile.Name = "btnRepackDataFile";
            this.btnRepackDataFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRepackDataFile_ItemClick);
            // 
            // FileExit
            // 
            this.FileExit.Caption = "Exit";
            this.FileExit.Id = 9;
            this.FileExit.Name = "FileExit";
            this.FileExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileExit_ItemClick);
            // 
            // tmpViewerWindow
            // 
            this.tmpViewerWindow.Caption = "Viewer Window";
            this.tmpViewerWindow.Id = 36;
            this.tmpViewerWindow.Name = "tmpViewerWindow";
            this.tmpViewerWindow.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.tmpViewerWindow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.tmpViewerWindow_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Options";
            this.barSubItem1.Id = 28;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.OptionsUnits),
            new DevExpress.XtraBars.LinkPersistInfo(this.OptionsColorScale)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // OptionsUnits
            // 
            this.OptionsUnits.Caption = "Units";
            this.OptionsUnits.Id = 29;
            this.OptionsUnits.Name = "OptionsUnits";
            this.OptionsUnits.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OptionsUnits_ItemClick);
            // 
            // OptionsColorScale
            // 
            this.OptionsColorScale.Caption = "3D Color Scale";
            this.OptionsColorScale.Id = 35;
            this.OptionsColorScale.Name = "OptionsColorScale";
            this.OptionsColorScale.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OptionsColorScale_ItemClick);
            // 
            // barTools
            // 
            this.barTools.Caption = "Tools";
            this.barTools.Id = 37;
            this.barTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.toolsMANTISShimAdjustment)});
            this.barTools.Name = "barTools";
            // 
            // toolsMANTISShimAdjustment
            // 
            this.toolsMANTISShimAdjustment.Caption = "MANTIS Shim Adjustment";
            this.toolsMANTISShimAdjustment.Id = 38;
            this.toolsMANTISShimAdjustment.Name = "toolsMANTISShimAdjustment";
            this.toolsMANTISShimAdjustment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toolsMANTISShimAdjustment_ItemClick);
            // 
            // Help
            // 
            this.Help.Caption = "Help";
            this.Help.Id = 24;
            this.Help.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.HelpReportBug),
            new DevExpress.XtraBars.LinkPersistInfo(this.HelpAbout),
            new DevExpress.XtraBars.LinkPersistInfo(this.HelpReleaseNotes)});
            this.Help.Name = "Help";
            // 
            // HelpReportBug
            // 
            this.HelpReportBug.Caption = "Report a Bug";
            this.HelpReportBug.Id = 32;
            this.HelpReportBug.Name = "HelpReportBug";
            this.HelpReportBug.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.HelpReportBug_ItemClick);
            // 
            // HelpAbout
            // 
            this.HelpAbout.Caption = "About";
            this.HelpAbout.Id = 30;
            this.HelpAbout.Name = "HelpAbout";
            this.HelpAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.HelpAbout_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1327, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 714);
            this.barDockControlBottom.Size = new System.Drawing.Size(1327, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1327, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // FileInspectionAddInspectionData
            // 
            this.FileInspectionAddInspectionData.Caption = "Add Inspection Data";
            this.FileInspectionAddInspectionData.Id = 7;
            this.FileInspectionAddInspectionData.Name = "FileInspectionAddInspectionData";
            this.FileInspectionAddInspectionData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FileInspectionAddInspectionData_ItemClick);
            // 
            // HelpReleaseNotes
            // 
            this.HelpReleaseNotes.Caption = "Release Notes";
            this.HelpReleaseNotes.Id = 39;
            this.HelpReleaseNotes.Name = "HelpReleaseNotes";
            this.HelpReleaseNotes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.HelpReleaseNotes_ItemClick);
            // 
            // ReformerMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1327, 739);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ReformerMain";
            this.Text = "LifeQuest Reformer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Shown += new System.EventHandler(this.ReformerMain_Shown);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riProgressBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar statusBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private Bar MainMenu;
        private BarSubItem FileMenu;
        private BarSubItem FileProjectMenu;
        private BarButtonItem FileProjectNewProject;
        private BarButtonItem FileProjectOpenProject;
        private BarButtonItem FileInspectionAddInspectionData;
        private BarButtonItem FileCloseProject;
        private BarButtonItem FileExit;
        private BarEditItem TheProgressBar;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar riProgressBar;
        private BarStaticItem TheStatusText;
        private BarButtonItem FileProjectSaveProject;
        private BarSubItem Help;
        private BarButtonItem btnRepackDataFile;
        private BarStaticItem uiVersionNum;
        private BarSubItem barSubItem1;
        private BarButtonItem OptionsUnits;
        private BarButtonItem HelpAbout;
        private BarButtonItem HelpReportBug;
        private BarSubItem barSubItem2;
        private BarListItem ListRecentProjects;
        private BarButtonItem OptionsColorScale;
        private BarButtonItem tmpViewerWindow;
        private BarSubItem barTools;
        private BarButtonItem toolsMANTISShimAdjustment;
        private BarButtonItem HelpReleaseNotes;
    }
}