﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Menu;
using DevExpress.XtraTreeList.Nodes;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Tubes;

namespace Reformer.Forms.UserControls
{
    /// <summary>
    /// Creates a simple user control that allows browsing through inspections and checking pipes from within them.
    /// </summary>
    public partial class InspectionBrowserTreeList : UserControl
    {
        private TreeListColumn colInspection; //Inspection it belongs to
        private TreeListColumn colName; //Name to display
        private TreeListColumn colType; //Type (Inspection or tube)
        private TreeListColumn colTubeInfo;  //The ReformerTube object of a tube.
        private ReformerProjectInfo _activeProject;
        public event TubeCheckedEventHandler NodeChecked; //Triggered when an individual node's checkstate changes.
        public event TreeListMenuItemClickEventHandler NodesUnchecked; //Triggered when all of the nodes are unchecked at the same time.
        List<ReformerTube> _selectedTubes;
        public bool InspectionCheckable;
        
        public InspectionBrowserTreeList()
        {
            _selectedTubes = new List<ReformerTube>();
            InitializeComponent();
            InitColumns();
            tlInspectionBrowser.BeforeExpand += tlInspectionBrowser_BeforeExpand;
            tlInspectionBrowser.AfterExpand += tlInspectionBrowser_AfterExpand;
            tlInspectionBrowser.AfterCollapse += tlInspectionBrowser_AfterCollapse;
            tlInspectionBrowser.AfterCheckNode += tlInspectionBrowser_AfterCheckNode;
        }
        
        #region Public Methods

        /// <summary>
        /// Resets and sets up the view to show all inspections and Tubes
        /// </summary>
        public void InitData(ReformerProjectInfo TheProject, bool IsInspectionsCheckable = false)
        {
            tlInspectionBrowser.ClearNodes();
            InspectionCheckable = IsInspectionsCheckable;
            _activeProject = TheProject;
            InitInspections();
            UpdateFileCount();
        }

        public void InitData(ReformerProjectInfo TheProject, ReformerInspection TheInspection, bool IsInspectionsCheckable = false)
        {
            tlInspectionBrowser.ClearNodes();
            InspectionCheckable = IsInspectionsCheckable;
            _activeProject = TheProject;
            InitInspections(new List<ReformerInspection> { TheInspection });
            UpdateFileCount();

        }

        public void Clear()
        {
            tlInspectionBrowser.ClearNodes();
            _selectedTubes.Clear();
            UpdateFileCount();
        }
        #endregion


        #region Private Methods

        /// <summary>
        /// Creates nodes for inspections and allows for filtering to a subset of inspections.
        /// </summary>
        private void InitInspections(List<ReformerInspection> InspectionFilter = null)
        {
            tlInspectionBrowser.BeginUnboundLoad();

            foreach (ReformerInspection insp in _activeProject.InspectionFiles.OrderBy(x => x.DateInspected).ToList())
            {
                // Add this inspection if there was no filter, or if this is an okay inspection according to filter.
                if (!((InspectionFilter != null && InspectionFilter.Contains(insp)) | InspectionFilter == null))
                    continue;
                string inspName = insp.Name;
                TreeListNode node = tlInspectionBrowser.AppendNode(new object[] { insp, inspName, "Inspection", null }, null);
                node.StateImageIndex = 0;
                node.HasChildren = insp.Tubes.Count > 0;
                if (node.HasChildren)
                    node.Tag = true;
            }
            tlInspectionBrowser.EndUnboundLoad();
        }

        private void InitPipes(TreeListNode ParentNode)
        {
            ReformerInspection insp = (ParentNode[colInspection] as ReformerInspection);

            foreach (ReformerTube pi in insp.Tubes.OrderBy(x => x.Name).ToList())
            {
                TreeListNode node = tlInspectionBrowser.AppendNode(new object[] { insp, pi.Name, "Tube", pi }, ParentNode);
                node.StateImageIndex = 1;
                node.HasChildren = false;
                node.Checked = _selectedTubes.Contains(pi);                    
            }
        }

        private void InitColumns()
        {
            colInspection = new TreeListColumn();
            colName = new TreeListColumn();
            colType = new TreeListColumn();
            colTubeInfo = new TreeListColumn();

            colName.Caption = "Name";
            colName.FieldName = "Name";
            colName.VisibleIndex = 0;
            colName.Visible = true;

            colType.Caption = "Type";
            colType.FieldName = "Type";
            colType.VisibleIndex = 1;
            colType.Visible = false;

            colTubeInfo.Caption = "TubeInfo";
            colTubeInfo.FieldName = "TubeInfo";
            colTubeInfo.Name = "Tube";


            tlInspectionBrowser.Columns.AddRange(new[] {
            colInspection,
            colName,
            colType,
            colTubeInfo});
        }       

        #endregion

        #region Event Handlers

        private void tlInspectionBrowser_AfterCollapse(object sender, NodeEventArgs e)
        {
            if (e.Node.StateImageIndex != 1) e.Node.StateImageIndex = 0;
        }

        private void tlInspectionBrowser_AfterExpand(object sender, NodeEventArgs e)
        {
            if (e.Node.StateImageIndex != 1) e.Node.StateImageIndex = 2;
        }

        private void tlInspectionBrowser_BeforeExpand(object sender, BeforeExpandEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                Cursor currentCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                InitPipes(e.Node);
                e.Node.Tag = null;
                Cursor.Current = currentCursor;
            }
        }

        private void tlInspectionBrowser_AfterCheckNode(object sender, NodeEventArgs e)
        {
            ToggleNode(e.Node, true);
        }

        private void ToggleNode(TreeListNode OriginalCheckedNode, bool ThrowEvent)
        {
            //Make all children match the parent node if it's a folder.
            if (OriginalCheckedNode[colType].ToString() == "Inspection" && InspectionCheckable)
            {
                if (!OriginalCheckedNode.Expanded) { OriginalCheckedNode.Expanded = OriginalCheckedNode.Checked; }//Make sure it's expanded appropriately so files can be accounted for or lost.
                foreach (TreeListNode t in OriginalCheckedNode.Nodes)
                {
                    if (t[colType].ToString() == "Tube")
                    {
                        t.Checked = OriginalCheckedNode.Checked;
                        if (t.Checked && !_selectedTubes.Contains(t[colTubeInfo] as ReformerTube))
                            _selectedTubes.Add(t[colTubeInfo] as ReformerTube);
                        else if (!t.Checked && _selectedTubes.Contains(t[colTubeInfo] as ReformerTube))
                            _selectedTubes.Remove(t[colTubeInfo] as ReformerTube);
                    }
                }
            }
            // Record if this tube is selected in the internal list.
            if (OriginalCheckedNode[colType].ToString() == "Tube")
            {
                if (OriginalCheckedNode.Checked && !_selectedTubes.Contains(OriginalCheckedNode[colTubeInfo] as ReformerTube))
                    _selectedTubes.Add(OriginalCheckedNode[colTubeInfo] as ReformerTube);
                else if (!OriginalCheckedNode.Checked && _selectedTubes.Contains(OriginalCheckedNode[colTubeInfo] as ReformerTube))
                    _selectedTubes.Remove(OriginalCheckedNode[colTubeInfo] as ReformerTube);
                if (NodeChecked != null && ThrowEvent)
                    NodeChecked(this, new TubeCheckedEventArgs((OriginalCheckedNode[colTubeInfo] as ReformerTube), OriginalCheckedNode.Checked));
            }

            UpdateFileCount();
            //Trigger the event that a node was clicked if someone is listening in.
           
        }

        private void UpdateFileCount()
        {
            uiSelectedCount.Text = "Selected: " + _selectedTubes.Count.ToString();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The tubes currently checked on the control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<ReformerTube> SelectedTubes
        {
            get
            {
                return _selectedTubes;
            }
            set
            {
                _selectedTubes = value;
            }
        }
        
        #endregion

        private void uiNone_Click(object sender, EventArgs e)
        {
            tlInspectionBrowser.UncheckAll();
            _selectedTubes.Clear();
            UpdateFileCount();
            if (NodeChecked != null) NodeChecked(this, null);
            if (NodesUnchecked != null)
                NodesUnchecked(this, new TreeListMenuItemClickEventArgs(null,false,SummaryItemType.None,null,TreeListMenuType.Summary,null));
        }

        //The only way, according to documentation, to hide a checkbox: http://www.devexpress.com/Support/Center/Question/Details/Q183959
        private void tlInspectionBrowser_CustomDrawNodeCheckBox(object sender, CustomDrawNodeCheckBoxEventArgs e)
        {
            //Prevent the checkbox from being drawn if it's an inspection and not supposed to be checkable
            if (e.Node.HasChildren && !InspectionCheckable) e.Handled = true; 
        }

        public delegate void TubeCheckedEventHandler(object sender, TubeCheckedEventArgs e);

        /// <summary>
        /// Event arguments to indicate the selected scalar has changed
        /// </summary>
        public class TubeCheckedEventArgs : EventArgs
        {
            public TubeCheckedEventArgs(ReformerTube TheTube, bool IsChecked)
            {
                Checked = IsChecked;
                Tube = TheTube;
            }

            public ReformerTube Tube { get; private set; }

            public bool Checked { get; private set; }
        }

    }
}
