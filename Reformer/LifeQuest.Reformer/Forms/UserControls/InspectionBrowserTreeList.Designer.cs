﻿namespace Reformer.Forms.UserControls
{
    partial class InspectionBrowserTreeList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lcBase = new DevExpress.XtraLayout.LayoutControl();
            this.uiSelectedCount = new DevExpress.XtraEditors.LabelControl();
            this.tlInspectionBrowser = new DevExpress.XtraTreeList.TreeList();
            this.uiNone = new DevExpress.XtraEditors.SimpleButton();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSelectedCount = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcBase)).BeginInit();
            this.lcBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlInspectionBrowser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectedCount)).BeginInit();
            this.SuspendLayout();
            // 
            // lcBase
            // 
            this.lcBase.AllowCustomizationMenu = false;
            this.lcBase.Controls.Add(this.uiSelectedCount);
            this.lcBase.Controls.Add(this.tlInspectionBrowser);
            this.lcBase.Controls.Add(this.uiNone);
            this.lcBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcBase.Location = new System.Drawing.Point(0, 0);
            this.lcBase.Name = "lcBase";
            this.lcBase.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2364, 249, 250, 350);
            this.lcBase.Root = this.lcgRoot;
            this.lcBase.Size = new System.Drawing.Size(164, 258);
            this.lcBase.TabIndex = 0;
            // 
            // uiSelectedCount
            // 
            this.uiSelectedCount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.uiSelectedCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.uiSelectedCount.Location = new System.Drawing.Point(86, 238);
            this.uiSelectedCount.Name = "uiSelectedCount";
            this.uiSelectedCount.Size = new System.Drawing.Size(76, 13);
            this.uiSelectedCount.StyleController = this.lcBase;
            this.uiSelectedCount.TabIndex = 5;
            this.uiSelectedCount.Text = "Selected: 0";
            // 
            // tlInspectionBrowser
            // 
            this.tlInspectionBrowser.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tlInspectionBrowser.Location = new System.Drawing.Point(2, 2);
            this.tlInspectionBrowser.Name = "tlInspectionBrowser";
            this.tlInspectionBrowser.OptionsView.ShowCheckBoxes = true;
            this.tlInspectionBrowser.OptionsView.ShowColumns = false;
            this.tlInspectionBrowser.OptionsView.ShowHorzLines = false;
            this.tlInspectionBrowser.OptionsView.ShowIndicator = false;
            this.tlInspectionBrowser.Size = new System.Drawing.Size(160, 228);
            this.tlInspectionBrowser.TabIndex = 1;
            this.tlInspectionBrowser.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.Solid;
            this.tlInspectionBrowser.CustomDrawNodeCheckBox += new DevExpress.XtraTreeList.CustomDrawNodeCheckBoxEventHandler(this.tlInspectionBrowser_CustomDrawNodeCheckBox);
            // 
            // uiNone
            // 
            this.uiNone.Location = new System.Drawing.Point(2, 234);
            this.uiNone.Name = "uiNone";
            this.uiNone.Size = new System.Drawing.Size(80, 22);
            this.uiNone.StyleController = this.lcBase;
            this.uiNone.TabIndex = 4;
            this.uiNone.Text = "None";
            this.uiNone.Click += new System.EventHandler(this.uiNone_Click);
            // 
            // lcgRoot
            // 
            this.lcgRoot.CustomizationFormText = "lcgRoot";
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.lciSelectedCount});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgRoot.Size = new System.Drawing.Size(164, 258);
            this.lcgRoot.Text = "lcgRoot";
            this.lcgRoot.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.uiNone;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 232);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(84, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.tlInspectionBrowser;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(164, 232);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // lciSelectedCount
            // 
            this.lciSelectedCount.Control = this.uiSelectedCount;
            this.lciSelectedCount.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lciSelectedCount.CustomizationFormText = "lciSelectedCount";
            this.lciSelectedCount.Location = new System.Drawing.Point(84, 232);
            this.lciSelectedCount.Name = "lciSelectedCount";
            this.lciSelectedCount.Size = new System.Drawing.Size(80, 26);
            this.lciSelectedCount.Text = "lciSelectedCount";
            this.lciSelectedCount.TextSize = new System.Drawing.Size(0, 0);
            this.lciSelectedCount.TextToControlDistance = 0;
            this.lciSelectedCount.TextVisible = false;
            this.lciSelectedCount.TrimClientAreaToControl = false;
            // 
            // InspectionBrowserTreeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcBase);
            this.Name = "InspectionBrowserTreeList";
            this.Size = new System.Drawing.Size(164, 258);
            ((System.ComponentModel.ISupportInitialize)(this.lcBase)).EndInit();
            this.lcBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlInspectionBrowser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSelectedCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcBase;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraTreeList.TreeList tlInspectionBrowser;
        private DevExpress.XtraEditors.SimpleButton uiNone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl uiSelectedCount;
        private DevExpress.XtraLayout.LayoutControlItem lciSelectedCount;


    }
}
