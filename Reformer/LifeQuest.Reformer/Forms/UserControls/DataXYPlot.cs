﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Nevron.Chart;
using Nevron.Chart.WinForm;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;

namespace Reformer.Forms.UserControls
{
    public partial class DataXyPlot : UserControl
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected readonly NPointSeries DataCursorPointSeries = new NPointSeries { UseXValues = true, DataLabelStyle = { Format = "<xvalue> : <value>" }, Size = new NLength(10, NGraphicsUnit.Pixel) };

        public static Range<double> YAxisSavedRange = new Range<double>(-1,-1);
        private static double _defaultYSpread = 1;
        protected int ZoomPercent = 10;

        protected double MeasUnitMultiplier
        {
            get
            {
                if (CurrentInspection == null)
                    return 1;
                return CurrentInspection.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale);
            }
        }

        protected double AxialUnitMultiplier
        {
            get
            {
                if (CurrentInspection == null)
                    return 1;
                return CurrentInspection.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.AxialDistanceUnits.Scale);
            }
        }

        public DataXyPlot()
        {
            InitializeComponent();
        }

        protected virtual void InitializeChart()
        {
            PlotPanel.Controls.Add(TheChartControl);
            TheChartControl.Dock = DockStyle.Fill;
            TheChartControl.MouseDown += TheChartControl_MouseDown;
            TheChartControl.MouseUp += TheChartControl_MouseUp;
            TheChartControl.MouseMove += TheChartControl_MouseMove;
            TheChartControl.Settings.EnableJittering = false;
            Chart.Dock = DockStyle.Fill;
            Chart.BoundsMode = BoundsMode.Stretch;
            
            TheChartControl.AutoRefresh = true;

            XAxis.Cursors.Clear();
            XAxis.ScaleConfigurator = new NLinearScaleConfigurator();
            XScale.LabelStyle.ContentAlignment = ContentAlignment.MiddleCenter; 
            TheChartControl.MouseWheel += TheChartControlOnMouseWheel;
            UsePositionData = true;
            XScale.ViewRangeInflateMode = ScaleViewRangeInflateMode.Absolute; //Set as absolute so it doesn't mess with scaling required for expanded view around the inspected tube length.
        }

        protected virtual void InitializeDataCursor()
        {
            NDataCursorTool dataCursorTool = new NDataCursorTool();
            AxialCursor.BeginEndAxis = (int) StandardAxis.PrimaryY;
            AxialCursor.ValueChanged += CursorValueChanged;
            AxialCursor.SynchronizeOnMouseAction = MouseAction.Down;
            AxialCursor.StrokeStyle.Pattern = LinePattern.DashDotDot;
            AxialCursor.StrokeStyle.Factor = 2;
            XAxis.Cursors.Add(AxialCursor);

            MeasurementCursor.BeginEndAxis = (int) StandardAxis.PrimaryX;
            //MeasurementCursor.ValueChanged += CursorValueChanged;
            //MeasurementCursor.SynchronizeOnMouseAction = MouseAction.Down; //Don't track this by default anymore.
            MeasurementCursor.StrokeStyle.Pattern = LinePattern.DashDotDot;
            MeasurementCursor.StrokeStyle.Factor = 2;
            YAxis.Cursors.Add(MeasurementCursor);

            TheChartControl.Controller.Tools.Clear();
            TheChartControl.Controller.Tools.Add(dataCursorTool);
            TheChartControl.Controller.Selection.SelectedObjects.Add(Chart);
        }

        /// <summary> Clears all items from all groups in the ListView </summary>
        protected virtual void ClearDataCursorView()
        {
            DataCursorView.Items.Clear();
        }

        protected virtual void CursorValueChanged(object sender, EventArgs e)
        {
            //Only trigger updates to the data line values when the Axial cursor shoots off an update.
            if (sender == AxialCursor)
            {
                double closestLineValue = double.MaxValue;
                //Reset the data cursor points.
                DataCursorPointSeries.ClearDataPoints(); 
                if (!Chart.Series.Contains(DataCursorPointSeries)) Chart.Series.Add(DataCursorPointSeries); 

                double xValue = AxialCursor.Value;
                IEnumerable<PickedPoint> lineNamesAndValuesAtX = GetLineNamesAndValuesAtXValue(xValue);
                ListViewGroup lineGroup = DataCursorView.Groups["Tubes"];
                for (int idx = lineGroup.Items.Count - 1; idx >=0; idx--) { DataCursorView.Items.Remove(lineGroup.Items[idx]);}
                
                foreach (PickedPoint p in lineNamesAndValuesAtX)
                {
                    ListViewItem li = new ListViewItem(lineGroup) {Text = p.Name};
                    li.SubItems.Add(p.XValue.ToString("N3"));
                    li.SubItems.Add(p.YValue.ToString("N3"));
                    DataCursorView.Items.Add(li);
                    DataCursorPointSeries.AddDataPoint(new NDataPoint(p.BaseXValue, p.BaseYValue, string.Format("{0:N3}",p.YValue)));
                    DataCursorPointSeries.DataLabelStyle.Format ="<label>";
                    if (Math.Abs(MeasurementCursor.Value - p.BaseYValue) < Math.Abs(MeasurementCursor.Value - closestLineValue))
                        closestLineValue = p.BaseYValue;
                }
                if (closestLineValue != double.MaxValue) MeasurementCursor.Value = closestLineValue;
            }
            
            //Tim Haugen didn't seem to like this. Removing the cursor value.
            //Update the cursor values regardless of which cursor changed.
            //ListViewGroup cursorGroup = DataCursorView.Groups["Cursor"];
            //for (int idx = cursorGroup.Items.Count - 1; idx >= 0; idx--) { DataCursorView.Items.Remove(cursorGroup.Items[idx]); }
            //ListViewItem cursorLineItem = new ListViewItem(cursorGroup) {Text = @"The Cursor"};
            //string xCursorValue = ((AxialCursor.Value + XScale.LabelStyle.ValueOffset) * XScale.LabelStyle.ValueScale).ToString("##.000");
            //string yCursorValue = ((MeasurementCursor.Value + YScale.LabelStyle.ValueOffset) * YScale.LabelStyle.ValueScale).ToString("##.000");
            //cursorLineItem.SubItems.Add(xCursorValue);
            //cursorLineItem.SubItems.Add(yCursorValue);
            //DataCursorView.Items.Add(cursorLineItem);
            DataCursorView.Refresh(); //Force a refresh or it will lag behind.
        }

        protected IEnumerable<PickedPoint> GetLineNamesAndValuesAtXValue(double XValue)
        {
            double xMultiplier = XScale.LabelStyle.ValueScale;
            double yMultiplier = YScale.LabelStyle.ValueScale;

            List<PickedPoint> returnValues = new List<PickedPoint>();
            for (int seriesCount = 0; seriesCount < Chart.Series.Count; seriesCount++)
            {
                if (!(Chart.Series[seriesCount] is NLineSeries)) continue;
                NLineSeries s = (NLineSeries)Chart.Series[seriesCount];
                string lineName = s.Name;
                double xValueBaseUnits;
                int slice = FindClosestDataValueInData(s.XValues, XValue, true, out xValueBaseUnits);
                if (slice == -1) continue;
                double yValue = ((double)s.Values[slice] + YScale.LabelStyle.ValueOffset) * yMultiplier;
                double xValue = (xValueBaseUnits + XScale.LabelStyle.ValueOffset) * xMultiplier;
                PickedPoint newPoint = new PickedPoint
                {
                    Name = lineName, XValue = Math.Round(xValue,3), YValue = Math.Round(yValue, 3),
                    BaseXValue = xValueBaseUnits + XScale.LabelStyle.ValueOffset,
                    BaseYValue = s.Values[slice].ToDouble() + YScale.LabelStyle.ValueOffset
                };
                returnValues.Add(newPoint);
            }
            
            return returnValues;
        }

        protected int FindClosestDataValueInData(NDataSeriesDouble sourceList, double Value, bool NaNOutsideOfBounds, out double ClosestValue)
        {
            //Stopwatch sw = Stopwatch.StartNew();
            ClosestValue = double.NaN; //Initialize
            double minValue = double.MaxValue, maxValue = double.MinValue;
            int minIdx = 0, maxIdx = 0;

            //Find the max/min value regardless of order of data.
            for (int i = 0; i < sourceList.Count; i++)
            {
                double v = sourceList[i].ToDouble();
                if (v > maxValue)
                {
                    maxValue = v; maxIdx = i;
                }
                if (v < minValue)
                {
                    minValue = v; minIdx = i;
                }
            }

            int valueCount = sourceList.Count;
            double percentOfRange = Math.Abs(Value) / Math.Abs(maxValue - minValue);
            int guesstimateIdx = (int)(minIdx + (percentOfRange * (maxIdx - minIdx))).Constrain(0, sourceList.Count - 1);
            double guesstimate = sourceList[guesstimateIdx].ToDouble();

            //Check if it's outside of bounds.
            Value = Value.Constrain(minValue, maxValue);
            
            double lastDelta = double.NaN;
            double thisValue;

            int idx= -1;
            //int attempts = 0;
            if (Value >= guesstimate && minIdx < maxIdx || Value <= guesstimate && maxIdx < minIdx) //Then start at the index and check upwards
            {
                for (int i = Math.Max(guesstimateIdx - 1, 0); i < valueCount; i++)
                {
                    thisValue = (double)sourceList[i];
                    double thisDelta = thisValue - Value;
                    if (Math.Abs(thisDelta) > Math.Abs(lastDelta)) { break; }
                    lastDelta = thisDelta;
                    idx = i;
                    //attempts++;
                }
            }
            else //start at the index and check downwards
            {
                for (int i = Math.Min(guesstimateIdx + 1, valueCount - 1); i >= 0; i--)
                {
                    thisValue = (double)sourceList[i];
                    double thisDelta = thisValue - Value;
                    if (Math.Abs(thisDelta) > Math.Abs(lastDelta)) { break; }
                    lastDelta = thisDelta;
                    idx = i;
                    //attempts++;
                }
            }
            //Log.Info(string.Format("Took {0} tries to find the closets value in {1} ticks.", attempts, sw.ElapsedTicks));
            ClosestValue = sourceList[idx].ToDouble();
            return idx;
        }

        #region Public Properties

        /// <summary> Determines whether spacing of data is determined by position data or by slice (time) </summary>
        public bool UsePositionData;

        /// <summary> Determines if the drawn constant lines can be moved </summary>
        public bool DraggableVertConstLines;
        public bool DraggableHorzConstLines;

        public NChartControl TheChartControl = new NChartControl();
        public ReformerProjectInfo CurrentProject { get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; } }
        public ReformerInspection CurrentInspection { get { return CurrentProject != null ? CurrentProject.CurrentInspectionFile : null; } }
        public readonly NAxisCursor AxialCursor = new NAxisCursor();
        public readonly NAxisCursor MeasurementCursor = new NAxisCursor();
        public NChart Chart { get { return TheChartControl.Charts[0]; } }
        public NAxis XAxis { get { return Chart.Axis(StandardAxis.PrimaryX); } }
        public NAxis YAxis { get { return Chart.Axis(StandardAxis.PrimaryY); } }
        public NLinearScaleConfigurator XScale { get { return (NLinearScaleConfigurator)TheChartControl.Charts[0].Axis(StandardAxis.PrimaryX).ScaleConfigurator; } }
        public NLinearScaleConfigurator YScale { get { return (NLinearScaleConfigurator)TheChartControl.Charts[0].Axis(StandardAxis.PrimaryY).ScaleConfigurator; } }
        public NAxisConstLine DraggingLine;

        #endregion Public Properties

        #region Public Methods

        /// <summary> Removes all series and their data points from the chart. </summary>
        public virtual void ClearChart()
        {
            foreach (NSeries s in TheChartControl.Charts[0].Series) { s.ClearDataPoints(); }
            TheChartControl.Charts[0].Series.Clear();
        }

        /// <summary> Clears the chart and draws the given lines onto the XY plot, assumes Y values are in base units. </summary>
        public void AddLines(List<NLineSeries> Lines, string XAxisLabel, string YAxisLabel)
        {
            ClearChart();
            Lines.ForEach(L =>
            {
                if (L != null) TheChartControl.Charts[0].Series.Add(L);
            });
            SetDisplayUnits();
            SetAxisLabels(XAxisLabel, YAxisLabel);
            TheChartControl.Refresh();
        }

        protected void SetHorzConstLines(List<ConstLineValue> values , bool JustClearThem = false)
        {
            YAxis.ConstLines.Clear();
            if (JustClearThem) return;
            //create a new const line for each value and give it the proper alignment
            foreach (var v in values) 
            {
                NAxisConstLine constLine = new NAxisConstLine
                {
                    Tag = v.Tag,
                    Value = v.Value,
                    Text = v.Text,
                    TextAlignment = v.Alignment,
                    StrokeStyle = {Pattern = LinePattern.Dot, Factor = v.PatternSize, Color = v.Color},
                };
                YAxis.ConstLines.Add(constLine); 
            }
        }

        protected void SetVertConstLines(List<ConstLineValue> values, bool JustClearThem = false, float widthInPixels = 3f)
        {
            XAxis.ConstLines.Clear();
            if (JustClearThem) return;
            //create a new const line for each value and give it the proper alignment
            foreach (var v in values)
            {
                NAxisConstLine constLine = new NAxisConstLine
                {
                    Value = v.Value,
                    Text = v.Text,
                    TextAlignment = v.Alignment,
                    StrokeStyle = { Pattern = LinePattern.Dot, Factor = 3, Width = new NLength(widthInPixels, NGraphicsUnit.Pixel) },
                    Tag = v.Tag, 
                };
                
                XAxis.ConstLines.Add(constLine);
            }
        }

        public void SetDisplayUnits()
        {
            if ( CurrentInspection == null) return;

            XScale.LabelStyle.ValueScale = UsePositionData ?
                CurrentInspection.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.AxialDistanceUnits.Scale) :
                1;

            //Set up the measurement units on the Y Axis
            YScale.LabelStyle.ValueScale = CurrentInspection.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale);
        }

        #endregion Public Methods

        #region Private Methods

        
        #endregion Private Methods

        #region Plot Formatting Methods

        protected void SetAxisLabels(string XAxisLabel, string YAxisLabel)
        {
            XScale.Title.Text = XAxisLabel;
            YScale.Title.Text = YAxisLabel;
        }

        protected void SetXAxisOffset(double offset) { XScale.LabelStyle.ValueOffset = offset; }
        protected void SetYAxisOffset(double offset) { YScale.LabelStyle.ValueOffset = offset; }

        protected virtual void TheChartControlOnMouseWheel(object Sender, MouseEventArgs Args)
        {
            TheChartControl.AutoRefresh = false;
            switch (ModifierKeys)
            {
                case Keys.Shift: //Zoom the Y Axis
                    {
                        for (int cNum = 0; cNum < TheChartControl.Charts.Count; cNum++)
                        {
                            var thisYAxis = TheChartControl.Charts[cNum].Axis(StandardAxis.PrimaryY);
                            double oldMin = thisYAxis.ViewRange.Begin;
                            double oldMax = thisYAxis.ViewRange.End;
                            double midPoint = (oldMin + oldMax) / 2;
                            double newMax, newMin;
                            if (Args.Delta < 0) //zoom in towards the center
                            {
                                newMax = midPoint + (oldMax - midPoint) * (1 - ZoomPercent / 100d);
                                newMin = midPoint - (midPoint - oldMin) * (1 - ZoomPercent / 100d);
                            }
                            else
                            {
                                newMax = midPoint + (oldMax - midPoint) * (1 + ZoomPercent / 100d);
                                newMin = midPoint - (midPoint - oldMin) * (1 + ZoomPercent / 100d);
                            }
                            thisYAxis.View = new NRangeAxisView(new NRange1DD(newMin, newMax));
                        }
                    }
                    break;
                case Keys.Control: //Zoom the X Axis
                    {
                        for (int cNum = 0; cNum < TheChartControl.Charts.Count; cNum++)
                        {
                            var thisXAxis = TheChartControl.Charts[cNum].Axis(StandardAxis.PrimaryX);
                            double oldMin = thisXAxis.ViewRange.Begin;
                            double oldMax = thisXAxis.ViewRange.End;
                            double midPoint = (oldMin + oldMax) / 2;
                            double newMax, newMin;
                            if (Args.Delta < 0) //zoom in towards the center
                            {
                                newMax = midPoint + (oldMax - midPoint) * (1 - ZoomPercent / 100d);
                                newMin = midPoint - (midPoint - oldMin) * (1 - ZoomPercent / 100d);
                            }
                            else
                            {//zoom out away from the center
                                newMax = midPoint + (oldMax - midPoint) * (1 + ZoomPercent / 100d);
                                newMin = midPoint - (midPoint - oldMin) * (1 + ZoomPercent / 100d);
                            }
                            thisXAxis.View = new NRangeAxisView(new NRange1DD(newMin, newMax));
                        }
                    }
                    break;
                case Keys.Alt: //Shift the X Axis
                    {
                        for (int cNum = 0; cNum < TheChartControl.Charts.Count; cNum++)
                        {
                            var thisXAxis = TheChartControl.Charts[cNum].Axis(StandardAxis.PrimaryX);
                            double oldMin = thisXAxis.ViewRange.Begin;
                            double oldMax = thisXAxis.ViewRange.End;
                            double amountToMove = (oldMax - oldMin) * ZoomPercent / 100d;
                            double newMax, newMin;
                            if (Args.Delta < 0) //Shift Right
                            {
                                newMax = oldMax + amountToMove;
                                newMin = oldMin + amountToMove;
                            }
                            else
                            {
                                newMax = oldMax - amountToMove;
                                newMin = oldMin - amountToMove;
                            }
                            thisXAxis.View = new NRangeAxisView(new NRange1DD(newMin, newMax));
                        }
                    }
                    break;
                case Keys.None: //Shift the Y Axis
                    {
                        for (int cNum = 0; cNum < TheChartControl.Charts.Count; cNum++)
                        {
                            var thisYAxis = TheChartControl.Charts[cNum].Axis(StandardAxis.PrimaryY);
                            double oldMin = thisYAxis.ViewRange.Begin;
                            double oldMax = thisYAxis.ViewRange.End;
                            double amountToMove = (oldMax - oldMin) * ZoomPercent / 100d;
                            double newMax, newMin;
                            if (Args.Delta < 0) //Shift Up
                            {
                                newMax = oldMax + amountToMove;
                                newMin = oldMin + amountToMove;
                            }
                            else
                            {
                                newMax = oldMax - amountToMove;
                                newMin = oldMin - amountToMove;
                            }
                            thisYAxis.View = new NRangeAxisView(new NRange1DD(newMin, newMax));
                        }
                    }
                    break;
            }
            TheChartControl.Refresh();
            TheChartControl.AutoRefresh = true;
        }

        #endregion Plot Formatting Methods

        #region Event Handlers

        protected virtual void TheChartControl_MouseDown(object sender, MouseEventArgs e)
        {
            ((NChartControl)sender).Focus();
            NHitTestResult hitTestResult = TheChartControl.HitTest(e.X, e.Y);
            
            if (hitTestResult.ChartElement == ChartElement.AxisConstLine && DraggableVertConstLines)
            {
               NAxisConstLine pickedLine = (NAxisConstLine) hitTestResult.Object;
               if (XAxis.ConstLines.Contains(pickedLine) && DraggableVertConstLines)
                {
                    TheChartControl.Cursor = Cursors.SizeWE;
                    TheChartControl.Capture = true;
                    DraggingLine = pickedLine;
                }
               if (YAxis.ConstLines.Contains(pickedLine) && DraggableHorzConstLines)
                {
                    TheChartControl.Cursor = Cursors.SizeNS;
                    TheChartControl.Capture = true;
                    DraggingLine = pickedLine;
                }
            }

            AxialCursor.SynchronizeOnMouseAction |= MouseAction.Move;
            //MeasurementCursor.SynchronizeOnMouseAction |= MouseAction.Move;
        }

        protected virtual void TheChartControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (DraggingLine != null)
            {
                DraggingLine = null;
                TheChartControl.Cursor = Cursors.Default;
                TheChartControl.Capture = false;
            }
            AxialCursor.SynchronizeOnMouseAction = MouseAction.Down;
            //MeasurementCursor.SynchronizeOnMouseAction = MouseAction.Down;
        }

        protected void TheChartControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (DraggingLine == null) return;
            double xValue = ((NAxisCursor)XAxis.Cursors[0]).Value;
            double yValue = ((NAxisCursor)YAxis.Cursors[0]).Value;
            DraggingLine.Value = XAxis.ConstLines.Contains(DraggingLine) ? xValue : yValue;
        }

        /// <summary> Centers the Y axis around the middle value utilizing the YAxisSpread field </summary>
        protected void UpdateYAxis(double MinValue = double.NaN, double MaxValue = double.NaN, bool AutoRange = false, bool UseAutoSpread = false)
        {
            if (Chart.Series.Count == 0) return;
            if (UseAutoSpread)
            {
                if (YAxisSavedRange.Min != -1 && YAxisSavedRange.Max != -1) //Use the user's requested range, if possible.
                    YAxis.View = new NRangeAxisView(new NRange1DD(YAxisSavedRange.Min, YAxisSavedRange.Max));
                else
                { //Use a default spread if no requested range.
                    List<double> meanValues = new List<double>();
                    foreach (NSeries s in Chart.Series)
                    {
                        if (!(s is NLineSeries)) continue;
                        int valueCount = s.Values.Count;
                        double sumValues = 0;
                        sumValues += s.Values.Cast<ValueType>().Sum(value => value.ToDouble());
                        meanValues.Add(sumValues / valueCount);
                    }
                    double midValue = meanValues.Mean();
                    YAxis.View = new NRangeAxisView(new NRange1DD(midValue - _defaultYSpread / 2, midValue + _defaultYSpread / 2));
                }
            }
            else if (AutoRange) YAxis.View = new NContentAxisView();
            else
            {
                if (MinValue.IsNaN()) MinValue = YAxis.ViewRange.Begin;
                if (MaxValue.IsNaN()) MaxValue = YAxis.ViewRange.End;
                YAxis.View = new NRangeAxisView(new NRange1DD(MinValue, MaxValue));
            }
        }

        /// <summary> Sets the X Axis to have the selected range. If NaN, keeps it the same </summary>
        protected void UpdateXAxis(double MinValue = double.NaN, double MaxValue = double.NaN, bool AutoRange = false)
        {
            if (AutoRange) XAxis.View = new NContentAxisView();
            else
            {
                if (MinValue.IsNaN()) MinValue = XAxis.ViewRange.Begin;
                if (MaxValue.IsNaN()) MaxValue = XAxis.ViewRange.End;
                XAxis.View = new NRangeAxisView(new NRange1DD(MinValue, MaxValue));
            }
        }

        #endregion

        protected struct PickedPoint
        {
            public string Name;
            public double BaseXValue;
            public double XValue;
            public double YValue;
            public double BaseYValue;
        }

        protected struct ConstLineValue
        {
            public double Value;
            public ContentAlignment Alignment;
            public object Tag;
            public string Text;
            public Color Color;
            public int PatternSize;
        }
    }
}
