﻿namespace Reformer.Forms.UserControls
{
    partial class DataXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Tubes", System.Windows.Forms.HorizontalAlignment.Left);
            this.DataCursorView = new System.Windows.Forms.ListView();
            this._colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._colAxial = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._colMeas = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.PlotPanel = new DevExpress.XtraEditors.PanelControl();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciDataCursorView = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lciPlotPanel = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // DataCursorView
            // 
            this.DataCursorView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._colName,
            this._colAxial,
            this._colMeas});
            this.DataCursorView.GridLines = true;
            listViewGroup1.Header = "Tubes";
            listViewGroup1.Name = "Tubes";
            this.DataCursorView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1});
            this.DataCursorView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.DataCursorView.Location = new System.Drawing.Point(493, 7);
            this.DataCursorView.MultiSelect = false;
            this.DataCursorView.Name = "DataCursorView";
            this.DataCursorView.Size = new System.Drawing.Size(190, 435);
            this.DataCursorView.TabIndex = 2;
            this.DataCursorView.UseCompatibleStateImageBehavior = false;
            this.DataCursorView.View = System.Windows.Forms.View.Details;
            // 
            // _colName
            // 
            this._colName.Text = "Line";
            this._colName.Width = 64;
            // 
            // _colAxial
            // 
            this._colAxial.Text = "Axial";
            this._colAxial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colAxial.Width = 62;
            // 
            // _colMeas
            // 
            this._colMeas.Text = "Meas";
            this._colMeas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._colMeas.Width = 57;
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.PlotPanel);
            this.lcMain.Controls.Add(this.DataCursorView);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(690, 449);
            this.lcMain.TabIndex = 3;
            this.lcMain.Text = "layoutControl1";
            // 
            // PlotPanel
            // 
            this.PlotPanel.Location = new System.Drawing.Point(7, 7);
            this.PlotPanel.Name = "PlotPanel";
            this.PlotPanel.Size = new System.Drawing.Size(477, 435);
            this.PlotPanel.TabIndex = 4;
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "Root";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciDataCursorView,
            this.splitterItem1,
            this.lciPlotPanel});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "Root";
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.lcgMain.Size = new System.Drawing.Size(690, 449);
            this.lcgMain.Text = "Root";
            this.lcgMain.TextVisible = false;
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Control = this.DataCursorView;
            this.lciDataCursorView.CustomizationFormText = "lciDataCursorView";
            this.lciDataCursorView.Location = new System.Drawing.Point(486, 0);
            this.lciDataCursorView.MaxSize = new System.Drawing.Size(194, 0);
            this.lciDataCursorView.MinSize = new System.Drawing.Size(194, 24);
            this.lciDataCursorView.Name = "lciDataCursorView";
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 439);
            this.lciDataCursorView.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciDataCursorView.Text = "lciDataCursorView";
            this.lciDataCursorView.TextSize = new System.Drawing.Size(0, 0);
            this.lciDataCursorView.TextToControlDistance = 0;
            this.lciDataCursorView.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(481, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 439);
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Control = this.PlotPanel;
            this.lciPlotPanel.CustomizationFormText = "lciPlotPanel";
            this.lciPlotPanel.Location = new System.Drawing.Point(0, 0);
            this.lciPlotPanel.Name = "lciPlotPanel";
            this.lciPlotPanel.Size = new System.Drawing.Size(481, 439);
            this.lciPlotPanel.Text = "lciPlotPanel";
            this.lciPlotPanel.TextSize = new System.Drawing.Size(0, 0);
            this.lciPlotPanel.TextToControlDistance = 0;
            this.lciPlotPanel.TextVisible = false;
            // 
            // DataXyPlot
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lcMain);
            this.Name = "DataXyPlot";
            this.Size = new System.Drawing.Size(690, 449);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListView DataCursorView;
        public System.Windows.Forms.ColumnHeader _colName;
        public System.Windows.Forms.ColumnHeader _colAxial;
        public System.Windows.Forms.ColumnHeader _colMeas;
        public DevExpress.XtraLayout.LayoutControl lcMain;
        public DevExpress.XtraEditors.PanelControl PlotPanel;
        public DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        public DevExpress.XtraLayout.LayoutControlItem lciDataCursorView;
        public DevExpress.XtraLayout.SplitterItem splitterItem1;
        public DevExpress.XtraLayout.LayoutControlItem lciPlotPanel;





    }
}
