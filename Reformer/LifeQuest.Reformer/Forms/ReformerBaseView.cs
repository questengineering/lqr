﻿
using System;
using QuestIntegrity.LifeQuest.Common.Forms;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.Project;
using Reformer.Interfaces;

namespace Reformer.Forms
{
    public partial class ReformerBaseView : LifeQuestBaseView, IHasReformerProjectDataSource, IHasDisplayUnits
    {
        /// <summary> A flag on whether this form will react to changes in project structure (Inspection or data added/removed) </summary>
        protected bool ListeningToProjectChanges { get; set; }

        public ReformerBaseView()
        {
            InitializeComponent();
            ListeningToProjectChanges = true;
        }

        public virtual void UpdateDataSource()
        {
            if (ListeningToProjectChanges)
                throw new NotImplementedException("The base UpdateDataSource should never be called.");
        }

        public ReformerProjectInfo CurrentProject
        {
            get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; }
        }

        public virtual void UpdateDisplayUnits()
        {
           throw new NotImplementedException("Changes to Display Units should be accounted for.");
        }
    }
}
