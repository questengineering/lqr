﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using log4net;
using Microsoft.Win32;
using Nevron.Diagram;
using NGenerics.Extensions;
using QuestIntegrity.Core.Extensions.Strings;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.AutomaticUpdates;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Data;
using Reformer.Data.Project;
using Reformer.Forms.CustomTools;
using Reformer.Forms.Options;
using Reformer.Forms.Startup;
using Reformer.Resources;
using Reformer.ScreenLayout;
using Reformer.WindowManagement;
using Timer = System.Windows.Forms.Timer;

namespace Reformer.Forms
{
    public partial class ReformerMain : Main
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected LifeQuestReformer App { get { return LifeQuestReformer.Instance; } }
        protected readonly System.Windows.Forms.SaveFileDialog TheSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
        protected readonly System.Windows.Forms.OpenFileDialog TheOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
        protected readonly Timer AutoSaveTimer = new Timer();
        protected DateTime LastProgressBarUpdate;

        private static NDrawingDocument _storedDocumentExpansionMap; //TubeLayoutMap View.Document used for data retention on view changes
        private static NDrawingDocument _storedDocumentL1Assessment;

        public ReformerMain()
        {
            Log.Info("Loading Main Form.");
            InitializeComponent();
            ReformerWindowManager.Instance.Main = this;
            uiVersionNum.Caption = string.Format("Ver: {0}", ApplicationVersion.LicenseVersion);
            App.Layout.Initialize(dockManager, documentManager);
            SetupEvents();
            FillMRUList();
            AutoSaveTimer.Tick += (sender, args) => AutoSave();
            SetupAutoSave();
            
            if (Debugger.IsAttached)
                tmpViewerWindow.Visibility = BarItemVisibility.Always;

            Log.Info("Loaded Main Form.");
        }

        private void FillMRUList()
        {
            //Get up to 5 existing recent project files. Take won't error out if there's less than 5, it'll just get all it can.
            ListRecentProjects.Strings.Clear();
            object[] prjs = ProjectMRUList.Instance.Where(f => new FileInfo(f).Exists).Take(5).Cast<object>().ToArray();
            ListRecentProjects.Strings.AddRange(prjs);
        }

        private void SetupEvents()
        {
            App.TheDataManager.ProjectClosed += ProjectClosed;
        }

        public void SetupAutoSave()
        {
            AutoSaveTimer.Stop();
            if (!Properties.Settings.Default.AutoSave) return;
            Log.Info("Setting up AutoSave");
            AutoSaveTimer.Interval = Properties.Settings.Default.AutoSaveTimer*1000*60;
            AutoSaveTimer.Start();
        }
        #region Menu Events

        private void FileProjectNewProject_ItemClick(object sender, ItemClickEventArgs e)
        {
            TheSaveFileDialog.DefaultExt = FileExtensions.ProjectFile;
            TheSaveFileDialog.Filter = FileExtensions.ProjectFilter;
            TheSaveFileDialog.Title = @"Create New Project File";
            if (TheSaveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                App.TheDataManager.NewProject(TheSaveFileDialog.FileName);
                ReformerWindowManager.Instance.SetPanelLayout();
            }
            ClearTempStoredDocuments();
        }

        private void FileProjectOpenProject_ItemClick(object sender, ItemClickEventArgs e)
        {
            TheOpenFileDialog.DefaultExt = FileExtensions.ProjectFile;
            TheOpenFileDialog.Filter = FileExtensions.ProjectFilter;
            TheOpenFileDialog.Title = @"Select a Project File";
            if (TheOpenFileDialog.FileName.IsNullOrEmpty() && UsefulDirectories.DefaultLoadDirectory.Exists)
                TheOpenFileDialog.InitialDirectory = UsefulDirectories.DefaultLoadDirectory.ToString();
            if (TheOpenFileDialog.ShowDialog(this) != DialogResult.OK) return;
            LoadProject(TheOpenFileDialog.FileName);
            SetProjectMenus(true);
            ClearTempStoredDocuments();
        }

        private void LoadProject(string fullProjectFileName)
        {
            Cursor = Cursors.WaitCursor;
            try { App.TheDataManager.LoadProject(fullProjectFileName); }
            catch (Exception ex)
            {
                Log.Error("Unable to load project.", ex);
                throw;
            }
            finally { Cursor = Cursors.Default; }

            if (App.TheDataManager.CurrentProject != null)
            {
                ReformerWindowManager.Instance.SetPanelLayout();
                ProjectMRUList.Instance.AddToTop(App.TheDataManager.CurrentProject.FilePath);
                ProjectMRUList.Instance.SaveMRUList();
                FillMRUList();
                SetConditionalFileMenuOptions(FileType.ProjectFile);
            }
        }

        public static void TempStoreDocument(NDrawingDocument d, bool L1)
        {
            if (L1)
            {
                _storedDocumentL1Assessment = d;
                //System.Diagnostics.Debug.WriteLine("Setting L1");
            }
            else
            {
                _storedDocumentExpansionMap = d;
                //System.Diagnostics.Debug.WriteLine("Setting EMap");
            }
        }

        public static NDrawingDocument TempRetrieveDocument(bool L1)
        {
            //System.Diagnostics.Debug.WriteLine("Reclaiming something: "+L1.ToString());
            return L1 ? _storedDocumentL1Assessment : _storedDocumentExpansionMap;
        }

        public static void ClearTempStoredDocuments()
        {
            _storedDocumentL1Assessment = null;
            _storedDocumentExpansionMap = null;
        }

        public static bool StoredL1IsNull()
        {
            if (_storedDocumentL1Assessment == null)
                return true;
            return false;
        }
        public static bool StoredEMapIsNull()
        {
            if (_storedDocumentExpansionMap == null)
                return true;
            return false;
        }

        private void FileInspectionAddInspectionData_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void FileProjectSaveProject_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject != null)
            {
                SetupProgressBar(100, "Saving Project...");
                App.TheDataManager.SaveProject();
                UpdateProgressBar(100, "Project Saved.");
            }
        }

        private void FileCloseProject_ItemClick(object sender, ItemClickEventArgs e)
        {
            TryToCloseProject();
        }

        private void FileExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            TryToCloseProject();
            Application.Exit();
        }

        private void OptionsUnits_ItemClick(object sender, ItemClickEventArgs e)
        {
            new UnitOptions().ShowDialog(this);
        }

        private void HelpAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            new About().ShowDialog(this);
        }

        private void HelpReportBug_ItemClick(object sender, ItemClickEventArgs e)
        {
            UnhandledException.CreateBugReport();
        }

        /// <summary> This was removed so updates could go through the Field Software Auto Updater instead. </summary>
        private void HelpUpdateCheck_ItemClick(object sender, ItemClickEventArgs e)
        {
            Task updateTask = new Task(() =>
            {
                SetupProgressBar(100, "Checking for Update...");
                AutoUpdater.Instance.CheckForUpdate();
                var exitCode = AutoUpdater.Instance.ProcessExitCode;
                if (exitCode != AutoUpdater.ExitCode.UpdateNotRequired )
                {
                    HideProgressBar(); 
                    return;
                }

                UpdateProgressBar(100, "No Update Available.");
                Thread.Sleep(5000);
                HideProgressBar();
            });
            updateTask.Start();
        }

        private void btnRepackDataFile_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReformerProjectInfo prj = LifeQuestReformer.Instance.TheDataManager.CurrentProject;
            if (prj != null && prj.InspectionFiles.Count > 0)
            {
                prj.InspectionFiles.ForEach(insp => insp.RepackData());
            }
        }

        private void OptionsColorScale_ItemClick(object sender, ItemClickEventArgs e)
        {
            new ColorOptions().ShowDialog(this);
        }

        #endregion Menu Events

        #region Event Handlers

        /// <summary>
        ///     Event Handler that fires when the WindowManager has successfully closed a project. Resets the text of this form.
        /// </summary>
        private void ProjectClosed(object sender, EventArgs e)
        {
            Text = String.Format("{0}", LifeQuestReformer.Product);
            CloseAllPanels();
        }

        /// <summary> Thread safe setup of the progress bar. </summary>
        public void SetupProgressBar(int maxValue, string theText)
        {
            if (InvokeRequired) { BeginInvoke(new Action(() => SetupProgressBar(maxValue, theText))); return; }
            riProgressBar.Maximum = maxValue;
            TheProgressBar.Visibility = TheStatusText.Visibility = BarItemVisibility.Always;
            TheStatusText.Caption = theText;
            TheProgressBar.Refresh(); TheStatusText.Refresh();
        }

        /// <summary> Thread safe update of the progress bar. Hides progress bar after 5 uninterrupted seconds if max is reached </summary>
        public async void UpdateProgressBar(int? value = null, string theText = null)
        {
            if (InvokeRequired) { BeginInvoke(new Action(() => UpdateProgressBar(value, theText))); return;}
            
            if (value != null) TheProgressBar.EditValue = (int)value;
            if (theText != null) TheStatusText.Caption = theText;
            LastProgressBarUpdate = DateTime.Now;
            TheProgressBar.Refresh(); TheStatusText.Refresh();
            if ((int)TheProgressBar.EditValue == riProgressBar.Maximum)
            {
                await Task.Factory.StartNew(() => Thread.Sleep(5000));
                //Only hide the progress bar if nothing else has updated it between the last 'fill'
                if ((DateTime.Now - LastProgressBarUpdate).TotalMilliseconds < 4999)
                    return;
                TheProgressBar.EditValue = 0;
                HideProgressBar();
            }
        }

        /// <summary>
        /// Threadsafe destruction of the progress bar
        /// </summary>
        public void HideProgressBar()
        {
            if (InvokeRequired) { BeginInvoke(new Action(HideProgressBar)); return; }
            
            TheProgressBar.EditValue = 0;
            TheProgressBar.Visibility = TheStatusText.Visibility = BarItemVisibility.Never;
            TheStatusText.Caption = string.Empty;
            TheProgressBar.Refresh(); TheStatusText.Refresh();
        }

        /// <summary>
        /// Event handler that fires when the main form is closed.
        /// </summary>
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!e.Cancel && (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null || App.TheDataManager.CloseCurrentProject()))
            { }
            else
            {
                e.Cancel = true;
            }
        }

        private void ReformerMain_Shown(object sender, EventArgs e)
        {
            ShowSplashScreen();
        }

        #endregion Event Handlers

        #region Private Methods

        private void SetConditionalFileMenuOptions(FileType theFileType)
        {
            switch (theFileType)
            {
                case (FileType.ProjectFile):
                    SetMenusForFileLoadedState(ProjectState.ProjectLoaded);
                    break;
                case (FileType.InspectionFile):
                    SetMenusForFileLoadedState(ProjectState.SingleInspectionFileLoaded);
                    break;
            }
        }

        private void SetPanelLayout(PanelLayout layout)
        {
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null ||
                LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile == null)
            {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                LifeQuestReformer.Instance.Layout.SetPanelLayout(layout);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void OnPanelClosed(LifeQuestBaseForm form)
        {
            ReformerWindowManager.Instance.RemoveForm(form);
        }

        private void TryToCloseProject()
        {
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return;
            DialogResult result = MessageBox.Show(this, @"Save before closing?", @"Save before closing?", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Cancel) return;
            if (result == DialogResult.Yes) LifeQuestReformer.Instance.TheDataManager.SaveProject();
            LifeQuestReformer.Instance.TheDataManager.CloseCurrentProject();
            SetProjectMenus(false);
        }

        public void AutoSave()
        {
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return;
            try
            {
                SetCursor(Cursors.WaitCursor);
                Task saveTask = new Task(() =>
                {
                    SetupProgressBar(100, "AutoSaving Project...");
                    App.TheDataManager.SaveProject();
                    UpdateProgressBar(100, "AutoSaved Project");
                    BeginInvoke(new Action(() => SetCursor(Cursors.Default)));
                    Log.Info("AutoSave Triggered.");
                });
                saveTask.Start();
            }
            catch (Exception ex)
            {
                SetCursor(Cursors.Default);
                Log.Error("Error saving during AutoSave", ex);
                HideProgressBar();
            }
        }

        private void ShowSplashScreen()
        {
            SplashScreen splash = new SplashScreen();
            splash.ShowDialog(this);
            switch (splash.Action)
            {
                case WelcomeAction.Open:
                    if (splash.SelectedProject.Exists)
                        LoadProject(splash.SelectedProject.FullName);
                    else
                        FileProjectOpenProject_ItemClick(null, null);
                    break;
                case WelcomeAction.New:
                    FileProjectNewProject_ItemClick(null, null);
                    break;
                case WelcomeAction.ExitApplication:
                    Application.Exit();
                    break;
            }
        }

        #endregion Private Methods

        #region Overridden protected methods

        protected override void SetProjectMenus(bool projectLoaded)
        {
            FileCloseProject.Enabled = projectLoaded;
            FileProjectSaveProject.Enabled = projectLoaded;
        }

        protected override void SetInspectionLoadedMenus(bool inspectionFileLoaded)
        {
        }
        #endregion

        #region Public Methods

        /// <summary> Allows the tabs in the central form docking area to be hidden so no tabs can be closed. Used to prevent users from closing the main window. </summary>
        public void ToggleTabClose(bool allowClose)
        {
            TabbedView view = documentManager.View as TabbedView;
            if (view == null) return;
            view.DocumentProperties.AllowClose = false;
            view.DocumentProperties.AllowFloat = false;
            view.DocumentGroupProperties.ShowTabHeader = false;
        }

        #endregion Public Methods

        private void tmpViewerWindow_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetPanelLayout(ReformerPanelLayout.Customer);
        }

        private void toolsMANTISShimAdjustment_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show(this, @"This tool should only be used with manager consent to adjust MANTIS data affected by an offset due to shims shifting during inspection.", @"Manager Consent Only");
            using (var newForm = new PartialRadialAdjustment())
            {
                newForm.StartPosition = FormStartPosition.CenterParent;
                newForm.ShowDialog();
            }
        }

        private void HelpReleaseNotes_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (Registry.ClassesRoot.OpenSubKey("Word.Application") == null)
            {
                MessageBox.Show(@"Cannot open Release Notes. MS Word is not installed.");
                return;
            }
            Process.Start(Path.Combine(UsefulDirectories.TemplateDirectory.FullName, "ReleaseNotes.Docx"));
        }

    }
}
