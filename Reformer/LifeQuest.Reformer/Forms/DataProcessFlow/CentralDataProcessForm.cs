﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.DataProcessFlow;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow
{
    public partial class CentralDataProcessForm : ReformerBaseForm, IHasDisplayUnits
    {
        private BaseProcessWindow _currentWindow;
        XtraMessageBoxForm _newMessage;
        LabelControl _helpmessage;
        internal BaseProcessFlowItem CurrentItem { get; private set; }


        public CentralDataProcessForm()
        {
            InitializeComponent();
            ListeningToProjectChanges = false;
            InitializeHelpForm();
        }

        public void SetCurrentProcessItem(BaseProcessFlowItem processItem)
        {
            DetachCurrentWindow();
            CurrentItem = processItem;
            AttachNewWindow(processItem);
            if (ReformerWindowManager.ProcessFlowForm != null)
            {
                ReformerWindowManager.ProcessFlowForm.SelectProcessListItem(processItem);
            }

            lblCurrentItemName.Text = processItem == null ? "" : processItem.Name;
        }

        #region Private Methods

        private void AttachNewWindow(BaseProcessFlowItem processItem)
        {
            if (processItem == null)
            {
                return; //Just in case, I guess.
            }

            _currentWindow = (BaseProcessWindow)Activator.CreateInstance(processItem.ProcessWindowType);
            _currentWindow.SetCurrentInspection(CurrentProject.CurrentInspectionFile);
            _currentWindow.ProgressChanged += ProgressChanged;
            _currentWindow.Dock = DockStyle.Fill;
            pnlMainWindow.Controls.Add(_currentWindow);
        }

        private void DetachCurrentWindow()
        {
            if (_currentWindow == null)
            {
                return;
            }

            if (_currentWindow.ProgressChanged != null)
            {
                _currentWindow.ProgressChanged -= ProgressChanged;
            }

            if (pnlMainWindow.Controls.Contains(_currentWindow))
            {
                pnlMainWindow.Controls.Remove(_currentWindow);
            }

            _currentWindow.Closing = true;
            _currentWindow.Dispose();
        }

        #region Progress Bar Handling

        private void ProgressChanged(object sender, ProgressChangedEventArgs changedEventArgs)
        {
            UpdateProgress(changedEventArgs.ProgressPercentage, changedEventArgs.UserState.ToString());
        }

        private void UpdateProgress(int progressPercentage, string progressMessage)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(() => UpdateProgress(progressPercentage, progressMessage)));
                return;
            }
            ShowProgressBar();
            if (progressPercentage != -1)
            {
                ProgBar.EditValue = progressPercentage;
            }

            if (progressMessage != "SkipMessage")
            {
                ProgMessage.Text = progressMessage;
            }

            ProgBar.Update();
            ProgMessage.Update();

            if ((int) ProgBar.EditValue == ProgBar.Properties.Maximum)
            {
                HideProgressBar();
            }
        }

        /// <summary> Makes the progress bar (and text) visible immediately </summary>
        private void ShowProgressBar()
        {
            ProgBar.Visible = true;
            ProgMessage.Visible = true;
        }

        /// <summary> Hides the progress bar (and text) after 5 asynchronous seconds. </summary>
        private async void HideProgressBar()
        {
            Task waitTask = new Task(() => Thread.Sleep(5000));
            waitTask.Start();
            await waitTask;
            ProgBar.Visible = false;
            ProgMessage.Visible = false;
        }

        #endregion Progress Bar Handling

        private void MoveToPreviousProcessItem()
        {
            if (CurrentProject.CurrentInspectionFile == null)
            {
                return;
            }

            ProcessFlowList curFlow = CurrentProject.CurrentInspectionFile.ProcessFlow;
            int currentIdx = curFlow.FlowList.IndexOf(CurrentItem);

            if (currentIdx >= 1 && currentIdx < curFlow.FlowList.Count)
            {
                SetCurrentProcessItem(curFlow.FlowList[currentIdx - 1]);
            }
        }

        private void MoveToNextProcessItem()
        {
            if (CurrentProject.CurrentInspectionFile == null)
            {
                return;
            }

            ProcessFlowList curFlow = CurrentProject.CurrentInspectionFile.ProcessFlow;
            int currentIdx = curFlow.FlowList.IndexOf(CurrentItem);
            if (currentIdx == -1)
            {
                currentIdx = 0; //This will happen if the inspection type (or the ProcessList) changes, which only happens on the first step.
            }
            if (currentIdx >= 0 && currentIdx < curFlow.FlowList.Count - 1)
            {
                SetCurrentProcessItem(curFlow.FlowList[currentIdx + 1]);
                curFlow.FlowList[currentIdx].IsComplete = true;
            }
        }

        #endregion Private Methods
        
        #region Event Handling

        private void btnNext_Click(object sender, EventArgs e)
        {
            MoveToNextProcessItem();
            if (CurrentProject != null && Properties.Settings.Default.AutoSaveOnNext)
            {
                LifeQuestReformer.Instance.TheDataManager.SaveProject();
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            MoveToPreviousProcessItem();
        }

        #endregion Event Handling

        private void btnHelp_Click(object sender, EventArgs e)
        {
            if (CurrentItem == null)
            {
                return;
            }
            _helpmessage.Text = CurrentItem.Description;
            if (_newMessage.Visible != true)
            {
                InitializeHelpForm();
                _newMessage.Show(this);
            }
        }

        private void InitializeHelpForm()
        {
            _newMessage = new XtraMessageBoxForm();
            _helpmessage = new LabelControl {AutoSizeMode = LabelAutoSizeMode.Vertical, Dock = DockStyle.Fill};
            _newMessage.Controls.Add(_helpmessage);
            if (CurrentItem != null)
            {
                _helpmessage.Text = CurrentItem.Description;
            }
            _newMessage.StartPosition = FormStartPosition.CenterParent;
        }

        public void UpdateDisplayUnits()
        {
            var typedForm = _currentWindow as IHasDisplayUnits;
            if (typedForm != null) typedForm.UpdateDisplayUnits();
        }
    }
}
