﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.InspectionFile;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow
{
    public partial class DataProcessFlowForm : ReformerBaseForm
    {
        #region Constructor

        public DataProcessFlowForm(FormType TheType): this() { }

        #endregion Constructor

        ReformerInspection Insp
        {
            get { return InspSelector.EditValue as ReformerInspection; }
        }

        public DataProcessFlowForm()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;

            UpdateDataSource();
        }

        private void InspSelector_EditValueChanged(object sender, EventArgs e)
        {
            if (Insp != null) CurrentProject.CurrentInspectionFile = Insp;
            RefreshProcessFlow();
            ResetCurrentFlowItem();
        }

        public void RefreshProcessFlow()
        {
            if (Insp != null) BSProcessFlow.DataSource = Insp.ProcessFlow.FlowList;
        }

        public void SelectProcessListItem(BaseProcessFlowItem TheItem)
        {
            if (Insp != null) BSProcessFlow.CurrencyManager.Position = BSProcessFlow.IndexOf(TheItem);
        }

        private void btnOpenForm_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            OpenSelectedForm();
        }


        #region Private Methods

        private void OpenSelectedForm()
        {
            BaseProcessFlowItem processItem = TheView.GetFocusedRow() as BaseProcessFlowItem;
            if (processItem == null) return; //just in case

            ReformerWindowManager.ProcessFormManager.SetCurrentFlowItem(processItem);

        }

        public override void UpdateDataSource()
        {
            if (CurrentProject == null) return;
            InspSelector.Properties.DataSource = CurrentProject.InspectionFiles;
            if (CurrentProject.InspectionFiles.Count > 0)
                InspSelector.EditValue = CurrentProject.CurrentInspectionFile;
            
        }

        private void ResetCurrentFlowItem()
        {
            BaseProcessFlowItem processItem = TheView.GetFocusedRow() as BaseProcessFlowItem;
            if (processItem == null) return; //just in case

            BaseProcessFlowItem equivalentItem = CurrentProject.CurrentInspectionFile.ProcessFlow.FlowList.FirstOrDefault(P => P.Name == processItem.Name);
            if (equivalentItem != default(BaseProcessFlowItem))
                ReformerWindowManager.ProcessFormManager.SetCurrentFlowItem(equivalentItem);
            else
                ReformerWindowManager.ProcessFormManager.SetCurrentFlowItem(null);
                
            
        }

        #endregion Private Methods
    }
}
