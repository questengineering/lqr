﻿using System.Windows.Forms;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.InspectionFile;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;

namespace Reformer.Forms.DataProcessFlow
{
    public partial class BlankBaseForm : BaseProcessWindow
    {
        /// <summary> The process window currently contained within this form. </summary>
        protected BaseProcessWindow ProcessWindow;

        public BlankBaseForm()
        {
            InitializeComponent();
        }

        public void SetProcessWindow(BaseProcessWindow processWindow)
        {
            if (ProcessWindow != null) ClearCurrentProcessWindow();
            if (processWindow == null) return;
            ProcessWindow = processWindow;
            processWindow.Dock = DockStyle.Fill;
            Controls.Add(ProcessWindow);
            UpdateDataSource();
        }

        private void ClearCurrentProcessWindow()
        {
            Controls.Remove(ProcessWindow);
            ProcessWindow.Dispose();
            ProcessWindow = null;
        }

        public override sealed void UpdateDataSource()
        {
            if (ProcessWindow == null) return;
            ProcessWindow.UpdateDataSource();
        }

        public override void SetCurrentInspection(ReformerInspection insp)
        {
            ProcessWindow.SetCurrentInspection(insp);
            base.SetCurrentInspection(insp);
        }

        /// <summary> Updates display units if the window in view implements the interface. Otherwise does nothing. </summary>
        public override void UpdateDisplayUnits()
        {
            var unitsWindow = ProcessWindow as IHasDisplayUnits;
            if (unitsWindow != null) unitsWindow.UpdateDisplayUnits();
        }
    }
}
