﻿using DevExpress.XtraEditors;

namespace Reformer.Forms.DataProcessFlow
{
    sealed partial class DataProcessFlowForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataProcessFlowForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.InspSelector = new DevExpress.XtraEditors.LookUpEdit();
            this.TheGrid = new DevExpress.XtraGrid.GridControl();
            this.BSProcessFlow = new System.Windows.Forms.BindingSource(this.components);
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colIsComplete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpenForm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOpenForm = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.uiInspectionLabel = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.InspSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSProcessFlow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenForm)).BeginInit();
            this.SuspendLayout();
            // 
            // InspSelector
            // 
            this.InspSelector.EditValue = "";
            this.InspSelector.Location = new System.Drawing.Point(102, 0);
            this.InspSelector.Name = "InspSelector";
            this.InspSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.InspSelector.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateInspected", "Date")});
            this.InspSelector.Properties.DisplayMember = "Name";
            this.InspSelector.Properties.NullText = "";
            this.InspSelector.Size = new System.Drawing.Size(165, 20);
            this.InspSelector.TabIndex = 0;
            this.InspSelector.EditValueChanged += new System.EventHandler(this.InspSelector_EditValueChanged);
            // 
            // TheGrid
            // 
            this.TheGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TheGrid.DataSource = this.BSProcessFlow;
            this.TheGrid.Location = new System.Drawing.Point(-3, 26);
            this.TheGrid.MainView = this.TheView;
            this.TheGrid.Name = "TheGrid";
            this.TheGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.memoEdit,
            this.btnOpenForm});
            this.TheGrid.Size = new System.Drawing.Size(356, 409);
            this.TheGrid.TabIndex = 0;
            this.TheGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            // 
            // BSProcessFlow
            // 
            this.BSProcessFlow.DataSource = typeof(Reformer.Data.DataProcessFlow.BaseProcessFlowItem);
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colNotes,
            this.colIsComplete,
            this.colOpenForm});
            this.TheView.GridControl = this.TheGrid;
            this.TheView.Name = "TheView";
            this.TheView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.TheView.OptionsCustomization.AllowFilter = false;
            this.TheView.OptionsCustomization.AllowSort = false;
            this.TheView.OptionsFilter.AllowFilterEditor = false;
            this.TheView.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 198;
            // 
            // colNotes
            // 
            this.colNotes.AppearanceHeader.Options.UseTextOptions = true;
            this.colNotes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNotes.ColumnEdit = this.memoEdit;
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 1;
            this.colNotes.Width = 51;
            // 
            // memoEdit
            // 
            this.memoEdit.AutoHeight = false;
            this.memoEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoEdit.Name = "memoEdit";
            // 
            // colIsComplete
            // 
            this.colIsComplete.AppearanceCell.Options.UseTextOptions = true;
            this.colIsComplete.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsComplete.Caption = "Complete";
            this.colIsComplete.FieldName = "IsComplete";
            this.colIsComplete.MaxWidth = 54;
            this.colIsComplete.MinWidth = 54;
            this.colIsComplete.Name = "colIsComplete";
            this.colIsComplete.Visible = true;
            this.colIsComplete.VisibleIndex = 2;
            this.colIsComplete.Width = 54;
            // 
            // colOpenForm
            // 
            this.colOpenForm.AppearanceHeader.Options.UseTextOptions = true;
            this.colOpenForm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOpenForm.Caption = " ";
            this.colOpenForm.ColumnEdit = this.btnOpenForm;
            this.colOpenForm.FieldName = "colOpenForm";
            this.colOpenForm.MinWidth = 10;
            this.colOpenForm.Name = "colOpenForm";
            this.colOpenForm.OptionsColumn.AllowMove = false;
            this.colOpenForm.OptionsColumn.AllowShowHide = false;
            this.colOpenForm.OptionsColumn.AllowSize = false;
            this.colOpenForm.OptionsColumn.FixedWidth = true;
            this.colOpenForm.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colOpenForm.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.colOpenForm.Visible = true;
            this.colOpenForm.VisibleIndex = 3;
            this.colOpenForm.Width = 35;
            // 
            // btnOpenForm
            // 
            this.btnOpenForm.AutoHeight = false;
            serializableAppearanceObject1.Image = ((System.Drawing.Image)(resources.GetObject("serializableAppearanceObject1.Image")));
            serializableAppearanceObject1.Options.UseImage = true;
            this.btnOpenForm.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnOpenForm.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btnOpenForm.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnOpenForm.Name = "btnOpenForm";
            this.btnOpenForm.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnOpenForm.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnOpenForm_ButtonClick);
            // 
            // uiInspectionLabel
            // 
            this.uiInspectionLabel.Location = new System.Drawing.Point(2, 3);
            this.uiInspectionLabel.Name = "uiInspectionLabel";
            this.uiInspectionLabel.Size = new System.Drawing.Size(94, 13);
            this.uiInspectionLabel.TabIndex = 1;
            this.uiInspectionLabel.Text = "Current Inspection:";
            // 
            // DataProcessFlowForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.uiInspectionLabel);
            this.Controls.Add(this.TheGrid);
            this.Controls.Add(this.InspSelector);
            this.Name = "DataProcessFlowForm";
            this.Size = new System.Drawing.Size(353, 435);
            this.TabText = "Process Flow";
            ((System.ComponentModel.ISupportInitialize)(this.InspSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSProcessFlow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl TheGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private System.Windows.Forms.BindingSource BSProcessFlow;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colIsComplete;
        private LookUpEdit InspSelector;
        private DevExpress.XtraEditors.LabelControl uiInspectionLabel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit memoEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colOpenForm;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnOpenForm;
    }
}
