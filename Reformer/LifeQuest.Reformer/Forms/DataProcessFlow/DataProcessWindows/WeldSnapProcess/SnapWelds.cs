﻿using System;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSnapProcess
{
    public partial class SnapWelds : BaseProcessWindow, IHasDisplayUnits
    {
        public SnapWelds()
        {
            InitializeComponent();
        }

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            SetControlDataSources();
        }

        private void SetControlDataSources()
        {
            ThePlotControl.SetInspection(CurrentInspection);
        }

        public override void UpdateDisplayUnits()
        {
            ThePlotControl.UpdateDisplayUnits();
        }
    }
}
