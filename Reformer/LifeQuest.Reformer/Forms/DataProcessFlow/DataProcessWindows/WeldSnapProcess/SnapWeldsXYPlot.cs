﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nevron.Chart;
using Nevron.GraphicsCore;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.Features;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSnapProcess
{
    public sealed partial class SnapWeldsXyPlot : DataXyPlot, IHasDisplayUnits
    {

        private ReformerTube CurrentTube { get { return TubeSelecter.EditValue as ReformerTube; } }
        private readonly List<NLineSeries> _currentLines = new List<NLineSeries>();
        private ReformerScalar CurrentScalar { get { return CurrentTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside; } }
        private ReformerInspection RefInspection { get { return uiReferenceInsp.EditValue as ReformerInspection; } }
        private List<double> _originalAxialData;
        private readonly Action<int, string> _progressUpdateMethod;
            

        private ReformerTube RefTube
        {
            get
            {
                //return null if there's no CurrentTube, no RefInspection, or no tubes named the same as CurrentTube in RefInspection.
                if (CurrentTube == null || RefInspection == null || RefInspection.Tubes.All(T => CurrentTube.Name != T.Name)) return null;
                return RefInspection.Tubes.First(T => T.Name == CurrentTube.Name);
            }
        }

        public SnapWeldsXyPlot(Action<int, string> progressUpdateMethod = null)
        {
            InitializeComponent();
            InitializeChart();
            InitializeDataCursor();
            DraggableVertConstLines = true;
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            _progressUpdateMethod = progressUpdateMethod;
        }

        protected override void InitializeChart()
        {
            TheChartControl.Legends.Clear();
            base.InitializeChart();
        }

        internal void SetInspection(ReformerInspection Inspection)
        {
            if (Inspection == null) return;
            TubeSelecter.Properties.DataSource = Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.;
            List<ReformerInspection> otherInspFiles = CurrentProject.InspectionFiles.Where(I => I != CurrentInspection).ToList();
            uiReferenceInsp.Properties.DataSource = otherInspFiles;
            if (otherInspFiles.Count == 0) uiReferenceInsp.Enabled = false;
            uiReferenceInsp.EditValue = otherInspFiles.FirstOrDefault();

            MoveFirst();
        }


        #region Event Handlers

        private void PlotInputChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            DrawPlot();
        }

        private void TheChartControlOnKeyDown(object Sender, KeyEventArgs Args)
        {
            if (Args.KeyCode == Keys.Q) MovePrevious();
            else if (Args.KeyCode == Keys.E) MoveNext();
        }

        private void btnSnapOne_Click(object sender, EventArgs e)
        {
            Enabled = false;
            if (radSnapOptions.SelectedIndex == 0 && RefTube != null)
                CurrentTube.SnapWelds(RefTube);
            else if (RefTube != null)
                RefTube.SnapWelds(CurrentTube);
            
            DrawPlot();
            Enabled = true;
            TheChartControl.Focus();
        }

        #endregion Event Handlers

        #region Private Methods

        private void DrawPlot()
        {
            Stopwatch sw = Stopwatch.StartNew();
            SetYAxisOffset(0);
            DrawTubes();
            DrawWelds();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            UpdateXAxis(double.NaN, double.NaN, true);
            Log.Info(string.Format("Drew Tube Data for {0} in {1} ms", CurrentTube.Name, sw.ElapsedMilliseconds));
        }

        private void DrawTubes()
        {
            ClearChart();
            _currentLines.Clear();
            if (CurrentTube == null) return;

            //Gather a default-formatted line.
            NLineSeries newLine = CurrentTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            newLine.Name = CurrentTube.Name;
            for (int i = 0; i < newLine.Values.Count; i++) { newLine.Values[i] = (double)newLine.Values[i] * 2; } //Get Diameter from radius data
            _currentLines.Add(newLine);

            NLineSeries refLine = GetReferenceTubeLine();
            if (refLine != null) _currentLines.Add(refLine);
            
            //Store the original mean data for the tube for fast calcing later.
            _originalAxialData = new List<double>();
            foreach (double d in newLine.XValues) _originalAxialData.Add(d);

            //Place the tube data and labels on the graph
            string xLabel = string.Format("Axial Position ({0})", DefaultValues.AxialUnitSymbol);
            string yLabel = string.Format("{0} ({1})", CurrentScalar == ReformerScalar.RadiusInside ? "Diameter (Inside)" : "Diameter (Outside)", DefaultValues.MeasurementUnitSymbol);
            AddLines(_currentLines, xLabel, yLabel);
            TheChartControl.RecalcLayout();
        }
        
        /// <summary>Adds a point series for each tube that shows weld locations. </summary>
        private void DrawWelds()
        {
            var welds = CurrentTube.Features.Where(F => F.Type == ReformerFeatureType.Weld).ToList();
            if (RefTube != null) welds.AddRange(RefTube.Features.Where(F => F.Type == ReformerFeatureType.Weld));
            NPointSeries weldPoints = new NPointSeries
            {
                UseXValues = true,
                Size = new NLength(.1f, NRelativeUnit.ParentPercentage),
                DataLabelStyle = {Format = "<label>"}
            };
            //Set the point shape.
            NMarkerStyle featureStyle = new NMarkerStyle
            {
                PointShape = PointShape.Cross, Height = new NLength(10), Width = new NLength(10),
                Visible = true, BorderStyle = { Color = Color.Black }, FillStyle = new NColorFillStyle(Color.Black)
            };
            weldPoints.MarkerStyle = featureStyle;
            foreach (var weld in welds)
            {
                double yValue = weld.ParentTube.GetScalarStatisticalValue(CurrentScalar, CurrentTube.Inspection.DataMeasure, weld.SliceIndexStart);
                weldPoints.AddDataPoint(new NDataPoint(weld.StartPosition, yValue * 2, weld.Label));
            }
            Chart.Series.Add(weldPoints);
            TheChartControl.RecalcLayout();
        }

        /// <summary> Gets a formatted reference tube line. Returns null if not able to draw it. </summary>
        private NLineSeries GetReferenceTubeLine()
        {
            ReformerTube refTube = RefTube; //cache locally to avoid lots of lookups.
            if (refTube == null || !refTube.AvailableScalars.Contains(CurrentScalar)) return null;

            //Gather a default-formatted line.
            NLineSeries newLine = refTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            newLine.Name = "Reference";
            newLine.BorderStyle.Color = Color.Gray;
            for (int i = 0; i < newLine.Values.Count; i++) { newLine.Values[i] = (double)newLine.Values[i] * 2; } //Get Diameter from radius data
            return newLine;
        }

        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            int nextKey = 0;
            if (TubeSelecter.EditValue != null) nextKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }
        
        #endregion Private Methods

        public void UpdateDisplayUnits()
        {
            DrawPlot();
        }

        private async void btnSnapAll_Click(object sender, EventArgs e)
        {
            if (RefInspection == null || CurrentInspection == null) return;
            Stopwatch sw = Stopwatch.StartNew();
            Enabled = false;
            List<ReformerTube> tubesToSnap = new List<ReformerTube>(), targetTubes = new List<ReformerTube>();
            if (radSnapOptions.SelectedIndex == 0)
            {
                tubesToSnap = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
                if (RefInspection != null)
                    targetTubes = RefInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
            }
            else if (RefInspection != null)
            {
                tubesToSnap = RefInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
                targetTubes = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
            }

            await SnapWelds(tubesToSnap, targetTubes);

            OnProgressChanged(100, string.Format("Finished snapping all welds in: {0}s", sw.Elapsed.TotalSeconds));
            DrawPlot();
            Enabled = true;
        }

        private Task SnapWelds(List<ReformerTube> tubesToSnap, List<ReformerTube> targetTubes )
        {
            return Task.Run(() =>
            {
                int tubeCount = tubesToSnap.Count, tubeCounter = 0;
                foreach (ReformerTube tube in tubesToSnap)
                {
                    ReformerTube refTube = targetTubes.FirstOrDefault(T => T.Name == tube.Name);
                    if (refTube == null) continue;
                    tube.SnapWelds(refTube);
                    OnProgressChanged(++tubeCounter / tubeCount * 100 , "Snapped welds for: " + tube.Name);
                }
            }); 
        }

        private void OnProgressChanged(int Progress = -1, string Status = "SkipProgress") //TODO: Refactor to base class and let all XY Plots report progress
        {
            if (_progressUpdateMethod == null) return;
            _progressUpdateMethod(Progress, Status);
        }
    }
}
