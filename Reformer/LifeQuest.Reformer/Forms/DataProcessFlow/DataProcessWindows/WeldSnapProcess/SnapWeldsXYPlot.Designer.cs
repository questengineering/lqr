﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSnapProcess
{
    sealed partial class SnapWeldsXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SnapWeldsXyPlot));
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciTube = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiReferenceInsp = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciRefInsp = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSnapOne = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSnapAll = new DevExpress.XtraEditors.SimpleButton();
            this.lciSnapAll = new DevExpress.XtraLayout.LayoutControlItem();
            this.radSnapOptions = new DevExpress.XtraEditors.RadioGroup();
            this.lciSnapOptions = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReferenceInsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefInsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSnapAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSnapOptions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSnapOptions)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(493, 127);
            this.DataCursorView.Size = new System.Drawing.Size(190, 315);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.radSnapOptions);
            this.lcMain.Controls.Add(this.btnSnapAll);
            this.lcMain.Controls.Add(this.btnSnapOne);
            this.lcMain.Controls.Add(this.uiReferenceInsp);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.uiReferenceInsp, 0);
            this.lcMain.Controls.SetChildIndex(this.btnSnapOne, 0);
            this.lcMain.Controls.SetChildIndex(this.btnSnapAll, 0);
            this.lcMain.Controls.SetChildIndex(this.radSnapOptions, 0);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTube,
            this.lciRefInsp,
            this.layoutControlItem1,
            this.lciSnapAll,
            this.lciSnapOptions});
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(486, 120);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 319);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(481, 24);
            this.splitterItem1.Size = new System.Drawing.Size(5, 415);
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(542, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.gridLookUpEdit1View;
            this.TubeSelecter.Size = new System.Drawing.Size(141, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 6;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.PlotInputChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // lciTube
            // 
            this.lciTube.Control = this.TubeSelecter;
            this.lciTube.CustomizationFormText = "Tube";
            this.lciTube.Location = new System.Drawing.Point(481, 0);
            this.lciTube.Name = "lciTube";
            this.lciTube.Size = new System.Drawing.Size(199, 24);
            this.lciTube.Text = "Tube";
            this.lciTube.TextSize = new System.Drawing.Size(50, 13);
            // 
            // uiReferenceInsp
            // 
            this.uiReferenceInsp.Location = new System.Drawing.Point(547, 31);
            this.uiReferenceInsp.Name = "uiReferenceInsp";
            this.uiReferenceInsp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiReferenceInsp.Properties.DisplayMember = "Name";
            this.uiReferenceInsp.Properties.NullText = "";
            this.uiReferenceInsp.Properties.View = this.gridView1;
            this.uiReferenceInsp.Size = new System.Drawing.Size(136, 20);
            this.uiReferenceInsp.StyleController = this.lcMain;
            this.uiReferenceInsp.TabIndex = 15;
            this.uiReferenceInsp.EditValueChanged += new System.EventHandler(this.PlotInputChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colInspName
            // 
            this.colInspName.AppearanceCell.Options.UseTextOptions = true;
            this.colInspName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInspName.AppearanceHeader.Options.UseTextOptions = true;
            this.colInspName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInspName.Caption = "Inspection Name";
            this.colInspName.FieldName = "Name";
            this.colInspName.Name = "colInspName";
            this.colInspName.Visible = true;
            this.colInspName.VisibleIndex = 0;
            // 
            // lciRefInsp
            // 
            this.lciRefInsp.Control = this.uiReferenceInsp;
            this.lciRefInsp.CustomizationFormText = "Reference";
            this.lciRefInsp.Location = new System.Drawing.Point(486, 24);
            this.lciRefInsp.Name = "lciRefInsp";
            this.lciRefInsp.Size = new System.Drawing.Size(194, 24);
            this.lciRefInsp.Text = "Reference";
            this.lciRefInsp.TextSize = new System.Drawing.Size(50, 13);
            // 
            // btnSnapOne
            // 
            this.btnSnapOne.Location = new System.Drawing.Point(493, 55);
            this.btnSnapOne.Name = "btnSnapOne";
            this.btnSnapOne.Size = new System.Drawing.Size(86, 22);
            this.btnSnapOne.StyleController = this.lcMain;
            this.btnSnapOne.TabIndex = 16;
            this.btnSnapOne.Text = "Snap Welds";
            this.btnSnapOne.Click += new System.EventHandler(this.btnSnapOne_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnSnapOne;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(486, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(90, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // btnSnapAll
            // 
            this.btnSnapAll.Location = new System.Drawing.Point(583, 55);
            this.btnSnapAll.Name = "btnSnapAll";
            this.btnSnapAll.Size = new System.Drawing.Size(100, 22);
            this.btnSnapAll.StyleController = this.lcMain;
            this.btnSnapAll.TabIndex = 17;
            this.btnSnapAll.Text = "Snap All";
            this.btnSnapAll.Click += new System.EventHandler(this.btnSnapAll_Click);
            // 
            // lciSnapAll
            // 
            this.lciSnapAll.Control = this.btnSnapAll;
            this.lciSnapAll.CustomizationFormText = "layoutControlItem2";
            this.lciSnapAll.Location = new System.Drawing.Point(576, 48);
            this.lciSnapAll.Name = "lciSnapAll";
            this.lciSnapAll.Size = new System.Drawing.Size(104, 26);
            this.lciSnapAll.Text = "lciSnapAll";
            this.lciSnapAll.TextSize = new System.Drawing.Size(0, 0);
            this.lciSnapAll.TextToControlDistance = 0;
            this.lciSnapAll.TextVisible = false;
            // 
            // radSnapOptions
            // 
            this.radSnapOptions.Location = new System.Drawing.Point(493, 81);
            this.radSnapOptions.Name = "radSnapOptions";
            this.radSnapOptions.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Snap Black to Gray"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Snap Gray to Black")});
            this.radSnapOptions.Size = new System.Drawing.Size(190, 42);
            this.radSnapOptions.StyleController = this.lcMain;
            this.radSnapOptions.TabIndex = 18;
            // 
            // lciSnapOptions
            // 
            this.lciSnapOptions.Control = this.radSnapOptions;
            this.lciSnapOptions.CustomizationFormText = "lciSnapOptions";
            this.lciSnapOptions.Location = new System.Drawing.Point(486, 74);
            this.lciSnapOptions.MaxSize = new System.Drawing.Size(194, 46);
            this.lciSnapOptions.MinSize = new System.Drawing.Size(194, 46);
            this.lciSnapOptions.Name = "lciSnapOptions";
            this.lciSnapOptions.Size = new System.Drawing.Size(194, 46);
            this.lciSnapOptions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciSnapOptions.Text = "lciSnapOptions";
            this.lciSnapOptions.TextSize = new System.Drawing.Size(0, 0);
            this.lciSnapOptions.TextToControlDistance = 0;
            this.lciSnapOptions.TextVisible = false;
            // 
            // SnapWeldsXyPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "SnapWeldsXyPlot";
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReferenceInsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefInsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSnapAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSnapOptions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSnapOptions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraLayout.LayoutControlItem lciTube;
        private DevExpress.XtraEditors.GridLookUpEdit uiReferenceInsp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspName;
        private DevExpress.XtraLayout.LayoutControlItem lciRefInsp;
        private DevExpress.XtraEditors.SimpleButton btnSnapOne;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnSnapAll;
        private DevExpress.XtraLayout.LayoutControlItem lciSnapAll;
        private DevExpress.XtraEditors.RadioGroup radSnapOptions;
        private DevExpress.XtraLayout.LayoutControlItem lciSnapOptions;
    }
}
