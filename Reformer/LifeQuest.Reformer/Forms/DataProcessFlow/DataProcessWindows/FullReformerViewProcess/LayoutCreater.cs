﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Kitware.VTK;
using NGenerics.Extensions;
using QuestIntegrity.Graphics;
using QuestIntegrity.Graphics.ColorScaleBar;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.Options;
using Reformer.Visualization;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.FullReformerViewProcess
{
    internal class LayoutCreater: IDisposable , IHasColorScaleBar
    {

        #region Constructor

        #region Properties

        public RenderWindowControl WindowControl { get; set; }
        private vtkRenderer Renderer { get { return WindowControl.RenderWindow.GetRenderers().GetFirstRenderer(); } }
        private const int MinRadialPoints = 24;
        private readonly Dictionary<ReformerTube, vtkActor> _actorsOnDisplay = new Dictionary<ReformerTube, vtkActor>();
        private readonly vtkAssembly _assembly = new vtkAssembly();
        private readonly Layout3DAnnotations _annotations;
        public event EventHandler CameraChanged;
        private readonly vtkActor _highlightActor = new vtkActor();
        private readonly vtkOutlineFilter _highlightOutline = new vtkOutlineFilter();
        private readonly vtkPolyDataMapper _highlightMapper = vtkPolyDataMapper.New();
        private readonly Action<int, string> _progressUpdateMethod;
        
        #endregion Properties


        public LayoutCreater()
        {
            CreateMyRenderWindow();
            _annotations = new Layout3DAnnotations();
            _annotations.Header.SetPosition(Renderer.GetSize()[0] / 2f, Renderer.GetSize()[1] - 60);
            Renderer.AddActor(_annotations.Header);
            WindowControl.RenderWindow.GetInteractor().EndInteractionEvt += OnCameraChanged;
            _highlightMapper.SetInputConnection(_highlightOutline.GetOutputPort());
            _highlightActor.SetMapper(_highlightMapper);
            _highlightActor.GetProperty().SetColor(0, 0, 0);
            _highlightActor.SetVisibility(0);
            AttachColorBarChangedEvent();
        }

        public LayoutCreater(Action<int, string> progressUpdateMethod) : this()
        {
            _progressUpdateMethod = progressUpdateMethod;
        }

        /// <summary> Triggers the creation of a render window for this factory to use. Due to the C++/C# interface, this has to be called explicitly to setup everything. </summary>
        private void CreateMyRenderWindow()
        {
            WindowControl = new RenderWindowControl();
            WindowControl.CreateControl();
            WindowControl.Size = new Size(900, 800);
            Renderer.GetActiveCamera().ParallelProjectionOn();
            Renderer.GetActiveCamera().UseHorizontalViewAngleOn();
            //Renderer.GetActiveCamera().SetViewAngle(5);
            Renderer.SetBackground(1, 1, 1);

            double intensity = .5;
            vtkLight botLight = new vtkLight();
            botLight.SetPosition(-5000, 0, 0);
            botLight.SetIntensity(intensity);
            Renderer.AddLight(botLight);
            
            vtkLight topLight = new vtkLight();
            topLight.SetPosition(2000, 0, 0);
            topLight.SetIntensity(intensity);
            Renderer.AddLight(topLight);

            vtkLight rightLight = new vtkLight();
            rightLight.SetPosition(0, 2000, 0);
            rightLight.SetIntensity(intensity);
            Renderer.AddLight(rightLight);

            vtkLight leftLight = new vtkLight();
            leftLight.SetPosition(0, -2000, 0);
            leftLight.SetIntensity(intensity);
            Renderer.AddLight(leftLight);

            vtkLight frontLight = new vtkLight();
            frontLight.SetPosition(0, 0, 2000);
            frontLight.SetIntensity(intensity);
            Renderer.AddLight(frontLight);

            vtkLight backLight = new vtkLight();
            backLight.SetPosition(0, 0, -100);
            backLight.SetIntensity(intensity);
            Renderer.AddLight(backLight);
            
            vtkLight camLight = new vtkLight();
            camLight.SetLightTypeToCameraLight();
            camLight.SetIntensity(intensity * .85);
            Renderer.AddLight(camLight);
            
        }

        public void OnCameraChanged(vtkObject Sender, vtkObjectEventArgs e)
        {
            if (CameraChanged != null) CameraChanged(Renderer.GetActiveCamera(), e);
        }
        
        #endregion Constructor

        #region  Actions

        private void OnProgressChanged(int Progress = -1, string Status = "SkipProgress")
        {
            if (_progressUpdateMethod == null) return;
            _progressUpdateMethod(Progress, Status);
        }

        public void Update(bool GetData)
        {
            if (TubesToDraw == null) return;
            bool listHasChanged = _actorsOnDisplay.Keys.Intersect(TubesToDraw).Count() != TubesToDraw.Count() || _actorsOnDisplay.Count != TubesToDraw.Count();
            Stopwatch sw = Stopwatch.StartNew();

            if (listHasChanged | GetData) 
                { RemoveTubeActors(); Generate3DTubes(GetData); }
            RemoveAnnotations();
            OnProgressChanged(85, "Placing tubes and camera...");
            GenerateTubeLayout();
            FocusCamera(_assembly);
            UpdateGlyphs();
            OnProgressChanged(95, "Rendering...");
            WindowControl.RenderWindow.Render();
            OnProgressChanged(100, string.Format("Finished in: {0} s", sw.Elapsed.TotalSeconds.ToString("N1")));
        }

        private void UpdateGlyphs()
        {
            if (_actorsOnDisplay == null || _actorsOnDisplay.Count == 0) { return; }
            _annotations.AnnotateRows = Layout == ReformerLayout.Box;
            _annotations.UpdateAnnotations(Renderer, TubesToAnnotate, _actorsOnDisplay);
            PlaceDirectionIndicators();
        }

        private void RemoveTubeActors()
        {
            if (_actorsOnDisplay == null || _actorsOnDisplay.Count == 0){ return; }
            _actorsOnDisplay.Values.ToList().ForEach(Actor => 
            {
                _assembly.RemovePart(Actor);
                Renderer.RemoveActor(Actor);
                Actor.ReleaseGraphicsResources(WindowControl.RenderWindow);
                Actor.Dispose();
            });
            GC.Collect(); //Need to do this, otherwise it may error out for no reason.
           _actorsOnDisplay.Clear();
        }

        private void RemoveAnnotations()
        {
            _annotations.TubeAnnotations.Values.ToList().ForEach(Actor =>
            {
                Renderer.RemoveActor(Actor);
                Actor.ReleaseGraphicsResources(WindowControl.RenderWindow);
                Actor.Dispose();
            });
            _annotations.RowAnnotations.Values.ToList().ForEach(Actor =>
            {
                Renderer.RemoveActor(Actor);
                Actor.ReleaseGraphicsResources(WindowControl.RenderWindow);
                Actor.Dispose();
            });
            DirectionAnnotations.ForEach(Actor =>
            {
                Renderer.RemoveActor(Actor);
                Actor.ReleaseGraphicsResources(WindowControl.RenderWindow);
                Actor.Dispose();
            });

            _annotations.TubeAnnotations.Clear();
            _annotations.RowAnnotations.Clear();
            DirectionAnnotations.Clear();
        }

        private void FocusCamera(vtkProp3D Assembly)
        {
            double[] centerPoint = Assembly.GetCenter();
            Renderer.GetActiveCamera().SetFocalPoint(centerPoint[0], centerPoint[1], centerPoint[2]);
        }

        private void Generate3DTubes(bool GetRealData)
        {
            int tubeCounter = 0;
            
            //Figure out the appropriate axial scale factor for the 'longest' tube data we'll be displaying and apply it to all tubes.
            const int lengthOverThickness = 80;
            var firstTube = TubesToDraw.FirstOrDefault();
            if (firstTube == null) return;
            double designedWidth = Math.Max(firstTube.Inspection.ReformerInfo.DefaultID / 2, firstTube.Inspection.ReformerInfo.DefaultOD / 2);
            double maxLength = TubesToDraw.Max(t => t.EndPosition - t.StartPosition);
            double newLengthRequired = designedWidth * lengthOverThickness;
            double axialScaleFactor = newLengthRequired / maxLength;

            TubesToDraw.ForEach(Tube =>
                {
                    vtkActor act = GetActor(Tube, GetRealData, axialScaleFactor);
                    _actorsOnDisplay.Add(Tube, act);
                    Renderer.AddActor(act);
                    _assembly.AddPart(act);
                    OnProgressChanged((int)Math.Round((float)tubeCounter++ / Tube.Inspection.Tubes.Count * 100 * .8), "Assembled: " + Tube.Name);
                });
        }

        private void GenerateTubeLayout()
        {
            double yPosition = 0, zPosition = 0;
            int directionMult  = NumberingDirection == TubeNumberingDirection.Clockwise ? -1 : 1;

            double radiusInInches = CylindricalRadius;
            double degreesPerTube = 0;
            if (TubesToDraw.Count != 0)
                degreesPerTube = 360d / (TubesToDraw.Max(t => t.ReformerAttributes.TubeNumber));
                
            foreach (ReformerTube tube in TubesToDraw)
            {
                double tubeDiameter = ScalarKey2D == ReformerScalar.RadiusInside ? tube.Specs.DiameterInside : tube.Specs.DiameterOutside;
                switch (Layout)
                {
                    case ReformerLayout.Box:
                        
                        yPosition = (tube.ReformerAttributes.TubeNumber - 1) * (TubeSpacing + tubeDiameter - .1) * directionMult;
                        zPosition = (tube.ReformerAttributes.RowNumber - 1) * RowSpacing;
                        break;
                    case ReformerLayout.Cylindrical:
                        double radius = (radiusInInches + (TubeSpacing + tubeDiameter * tube.ReformerAttributes.RowNumber));
                        double angleInRadians = degreesPerTube * Math.PI / 180 * tube.ReformerAttributes.TubeNumber;
                        yPosition = radius * Math.Sin(angleInRadians) * directionMult;
                        zPosition = radius * Math.Cos(angleInRadians);
                        break;
                }
                //Force the tube to start at 0 X location //they asked not to do this the next 'bug' report.
                //var startPosition = tube.GetPositionsSubset(0, 0).First() * _actorsOnDisplay[tube].GetScale()[0];
                _actorsOnDisplay[tube].SetPosition(_actorsOnDisplay[tube].GetPosition()[0], yPosition, zPosition);
            }
        }
       
        
        private vtkActor GetActor(ReformerTube Tube, bool GetRealData, double axialScaleFactor)
        {
            //If this is a preview or it doesn't have the requested scalar, get a minimalistc grid. Otherwise get all the points (much longer)
            vtkActor actor;
            if (GetRealData && Tube.AvailableScalars.Contains(ScalarKey2D))
            {
                float lodfactor =  Tube.GetDataDimensions(ScalarKey2D)[0] / 1000f;
                actor = VTKTubeCreatorBase.Get3DDiameterTube(Tube, ScalarKey2D, lodfactor, GetRecommendedPointsPerTube(Tube));
            }
            else
            {
                var tubeDiameter = ScalarKey2D == ReformerScalar.RadiusInside ? Tube.Specs.DiameterInside : Tube.Specs.DiameterOutside;
                vtkStructuredGrid grid = VTKTubeCreatorBase.CreatePreviewTubeGrid(Tube.StartPosition, Tube.EndPosition, GetRecommendedPointsPerTube(Tube), tubeDiameter, 0);
                actor = VTKTubeCreatorBase.MapGridToActor(grid);
            }    

            if (!GetRealData || !Tube.AvailableScalars.Contains(ScalarKey2D))
            {
                actor.GetProperty().SetColor(.6, .6, .6);//Set preview actors grey.
            } 
            else
            {
                actor.GetMapper().SetLookupTable(ColorScaleBar.ColorTransferFunction);
                actor.GetMapper().UseLookupTableScalarRangeOn();
            }
                        
            actor.SetScale(axialScaleFactor, 1, 1); //scale the actor and its start position appropriately.
            return actor;
        }
        
        public int GetRecommendedPointsPerTube(ReformerTube Tube)
        {
            if (Tube.Inspection.NumRadialReadings <= 16)
                return (int)Math.Ceiling((double)MinRadialPoints / Tube.Inspection.NumRadialReadings) * Tube.Inspection.NumRadialReadings; //Gets the closet number above MinRadialPoints that is a whole divisor
            //otherwise this is probably a LOTIS, with 128 sensor readings, and we need to downsample that.
            return 16; // 128 divided by 4
        }

        
        #endregion Preview Actions

        #region Public Methods

        public void SetCameraPosition(double[] CamPosition, double Roll)
        {
            Renderer.GetActiveCamera().SetPosition(CamPosition[0], CamPosition[1], CamPosition[2]);
            Renderer.GetActiveCamera().SetRoll(Roll);
        }

        public void ResetCamera() { Renderer.ResetCamera(); }

        #endregion Public Methods

        #region Drawing Properties

        private List<ReformerTube> _tubesToDraw = new List<ReformerTube>();
        private List<ReformerTube> _tubesToAnnotate = new List<ReformerTube>();
        
        public List<ReformerTube> TubesToDraw
        {
            get { return _tubesToDraw.OrderBy(Tube => Tube.Name).ToList();}
            set { _tubesToDraw = value; }
        }

        public List<ReformerTube> TubesToAnnotate
        {
            get { return _tubesToAnnotate.OrderBy(Tube => Tube.Name).ToList(); }
            set { _tubesToAnnotate = value; }
        }

        public ReformerScalar ScalarKey2D { get; set; }

        public ReformerLayout Layout { get; set; }

        public float RowSpacing { get; set; }

        public float TubeSpacing { get; set; }

        public float CylindricalRadius { get; set; }

        public TubeNumberingDirection NumberingDirection { get; set; }

        #endregion Drawing Properties
        
        public void Dispose()
        {
            RemoveTubeActors();
            RemoveAnnotations();
            Renderer.GetActiveCamera().ModifiedEvt -= OnCameraChanged;
            DetachColorBarChangedEvent();
            if (ColorScaleBar != null) ColorScaleBar.Clear(Renderer);
            
        }
        
        internal void HighlightTube(ReformerTube Tube)
        {
            if (Tube != null & _highlightActor.GetVisibility() == 0 && _actorsOnDisplay.Keys.Contains(Tube))
            {
                _highlightOutline.SetInput(_actorsOnDisplay[Tube].GetMapper().GetInputAsDataSet());
                var scale = _actorsOnDisplay[Tube].GetScale();
                _highlightActor.SetScale(scale[0], scale[1], scale[2]);
                Renderer.AddActor(_highlightActor);
                double[] position = _actorsOnDisplay[Tube].GetPosition();
                _highlightActor.SetPosition(position[0], position[1], position[2]);
                _highlightActor.SetVisibility(1);
            }
            else if (_highlightActor.GetVisibility() == 1)
            {
                Renderer.RemoveActor(_highlightActor);
                _highlightActor.SetVisibility(0);
            }
            Renderer.Render();
        }

        /// <summary>  Takes a HD snapshot of the screen and compresses it to the desired size.  </summary>
        public void TakeScreenshot(string Folder, string FileName)
        {
            const int magLevel = 4;
            //Adjust vtkActor2D types, otherwise they'll be out of position during magnification.
            double[] originalHeaderPos = _annotations.Header.GetPosition();
            _annotations.Header.SetPosition(originalHeaderPos[0] * magLevel, originalHeaderPos[1] * magLevel);

            //Renderer.GetRenderWindow().Render();
            vtkWindowToImageFilter windowToImageFilter = new vtkWindowToImageFilter();
            string tmpFullFileName = Folder + "\\" + "tmp" + FileName;
            windowToImageFilter.SetInput(WindowControl.RenderWindow);
            windowToImageFilter.SetMagnification(magLevel);
            windowToImageFilter.Update();

            vtkJPEGWriter writer = new vtkJPEGWriter();
            writer.SetFileName(tmpFullFileName);
            writer.SetInputConnection(windowToImageFilter.GetOutputPort());
            writer.Write();

            Image srcImage = Image.FromFile(tmpFullFileName);
            using (Bitmap newImage = new Bitmap(srcImage, new Size(1172, 1015)))
            {
                srcImage.Dispose();
                string fullFileName = Folder + "\\" + FileName;
                newImage.Save(fullFileName, ImageFormat.Jpeg);
            }

            File.Delete(tmpFullFileName);
            _annotations.Header.SetPosition(originalHeaderPos[0], originalHeaderPos[1]);
            Renderer.GetRenderWindow().Render();
        }

        public void SetColorBar(ColorScaleBar CSB)
        {
            if (ColorScaleBar != null) ColorScaleBar.Clear(Renderer);
            CSB.Draw(Renderer);
            ColorScaleBar = CSB;
            _actorsOnDisplay.Values.ToList().ForEach(actor => actor.GetMapper().SetLookupTable(CSB.ColorTransferFunction));
            WindowControl.RenderWindow.Render();
        }

        public void RefreshColorBar()
        {
            ColorScaleBar.Build();
            ColorScaleBar.Draw(Renderer);
        }

        public ColorScaleBar ColorScaleBar { get; set; }

        public MapDirections Row1Direction { get; set; }

        public void AttachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged += HandleColorBarChangedEvent;
        }

        public void DetachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged -= HandleColorBarChangedEvent;
        }

        public void HandleColorBarChangedEvent(object Sender, ColorScaleChangedEventArgs Args)
        {
            if (ColorScaleBar.ActiveScalarName != Args.TheScalarRange.ScalarName) return;
            //double the color bar values on this local version as we're dealing with diameter now.
            ColorScaleBar.SetRange(Args.TheScalarRange);
            ColorScaleBar.MinMaxRanges.ForEach(scalarRange => { scalarRange.Min = scalarRange.Min * 2; scalarRange.Max = scalarRange.Max * 2; });
            
            ColorScaleBar.Build();
            ColorScaleBar.Draw(Renderer);
            WindowControl.RenderWindow.Render();
        }

        public List<vtkFollower> DirectionAnnotations = new List<vtkFollower>();

        private void PlaceDirectionIndicators()
        {
            double[] bounds = _assembly.GetBounds();
            List<MapDirections> mD = ((MapDirections[])Enum.GetValues(typeof(MapDirections))).ToList();
            int firstID = mD.IndexOf(Row1Direction);
            int rightID; Math.DivRem(firstID + 2, mD.Count, out rightID);
            int rearID; Math.DivRem(firstID + 4, mD.Count, out rearID);
            int leftID; Math.DivRem(firstID + 6, mD.Count, out leftID);
            int directionMult = NumberingDirection == TubeNumberingDirection.Clockwise ? 1 : 1;

            vtkVectorText row1Text = new vtkVectorText(); row1Text.SetText(mD[firstID].ToString()); 
            vtkFollower row1Follower = MapAnnotationToActor(row1Text);
            vtkVectorText right1Text = new vtkVectorText(); right1Text.SetText(mD[rightID].ToString());
            vtkFollower right1Follower = MapAnnotationToActor(right1Text);
            vtkVectorText rearRowText = new vtkVectorText(); rearRowText.SetText(mD[rearID].ToString());
            vtkFollower rearFollower = MapAnnotationToActor(rearRowText);
            vtkVectorText left1Text = new vtkVectorText(); left1Text.SetText(mD[leftID].ToString());
            vtkFollower left1Follower = MapAnnotationToActor(left1Text);

            const int offset = 20;
            double xPlacement = bounds[1] + offset;
            double yCenter = (bounds[3] + bounds[2]) / 2;
            double zCenter = (bounds[5] + bounds[4]) / 2;

            PlaceAnnotation(xPlacement, yCenter * directionMult, bounds[4] - offset, row1Follower);
            PlaceAnnotation(xPlacement, bounds[3] + offset, zCenter, right1Follower);
            PlaceAnnotation(xPlacement, yCenter * directionMult, bounds[5] + offset, rearFollower);
            PlaceAnnotation(xPlacement, bounds[2] - offset, zCenter, left1Follower);
        }

        private void PlaceAnnotation(double X, double Y, double Z, vtkFollower follower)
        {
            follower.SetPosition(X, Y, Z);
            follower.SetCamera(Renderer.GetActiveCamera());
            follower.SetVisibility(true);
            Renderer.AddActor(follower);
            DirectionAnnotations.Add(follower);
        }

        private static vtkFollower MapAnnotationToActor(vtkVectorText vectorText)
        {
            vtkPolyDataMapper newTextMapper = vtkPolyDataMapper.New();
            newTextMapper.SetInputConnection(vectorText.GetOutputPort());
            vtkFollower newFollower = new vtkFollower();
            newFollower.SetMapper(newTextMapper);
            newFollower.SetScale(5, 5, 5);
            newFollower.GetProperty().SetColor(0, 0, 0);
            return newFollower;
        }


        internal class Layout3DAnnotations
        {
            public Dictionary<ReformerTube, vtkFollower> TubeAnnotations = new Dictionary<ReformerTube, vtkFollower>();
            public Dictionary<ReformerTube, vtkFollower> RowAnnotations = new Dictionary<ReformerTube, vtkFollower>();
            public vtkTextActor Header = new vtkTextActor();
            public bool AnnotateRows = true;

            private vtkFollower AnnotateTube(vtkActor TubeActor, string Annotation, vtkRenderer Renderer)
            {
                vtkVectorText newTubeText = new vtkVectorText();
                newTubeText.SetText(Annotation);

                vtkPolyDataMapper newTextMapper = vtkPolyDataMapper.New();
                newTextMapper.SetInputConnection(newTubeText.GetOutputPort());
                vtkFollower newAnnotationTop = new vtkFollower();
                newAnnotationTop.SetMapper(newTextMapper);
                newAnnotationTop.SetScale(5, 5, 5);
                newAnnotationTop.GetProperty().SetColor(0, 0, 0);
                newAnnotationTop.RotateZ(90);

                double[] tubeBounds = TubeActor.GetBounds();
                double[] tubeCenter = TubeActor.GetCenter();
                newAnnotationTop.SetPosition(tubeBounds[0] - 5, tubeCenter[1], tubeCenter[2]);
                newAnnotationTop.SetCamera(Renderer.GetActiveCamera());

                Renderer.AddActor(newAnnotationTop);

                return newAnnotationTop;
            }

            private vtkFollower AnnotateRow(vtkActor TubeActor, string Annotation, vtkRenderer Renderer)
            {
                vtkVectorText newRowText = new vtkVectorText();
                newRowText.SetText(Annotation);

                vtkPolyDataMapper newTextMapper = vtkPolyDataMapper.New();
                newTextMapper.SetInputConnection(newRowText.GetOutputPort());
                vtkFollower newAnnotationTop = new vtkFollower();
                newAnnotationTop.SetMapper(newTextMapper);
                newAnnotationTop.SetScale(5, 5, 5);
                newAnnotationTop.GetProperty().SetColor(0, 0, 0);
                newAnnotationTop.RotateZ(90);

                double[] tubeBounds = TubeActor.GetBounds();
                double[] tubeCenter = TubeActor.GetCenter();
                newAnnotationTop.SetPosition(tubeBounds[1] + 30, tubeCenter[1], tubeCenter[2]);
                newAnnotationTop.SetCamera(Renderer.GetActiveCamera());

                Renderer.AddActor(newAnnotationTop);

                return newAnnotationTop;
            }

            private void UpdateHeader(ReformerTube Tube)
            {
                vtkTextProperty tProp = Header.GetTextProperty();
                tProp.SetFontSize(8);
                tProp.SetFontFamilyToArial();
                tProp.SetJustificationToCentered();
                tProp.BoldOn();
                tProp.SetColor(0, 0, 0);
                Header.SetTextScaleModeToViewport();
                Header.SetInput(Tube == null ? "No Reformer Name Given" : Tube.Inspection.ReformerInfo.Refinery + Environment.NewLine + Tube.Inspection.ReformerInfo.ReformerName );
            }

            public void UpdateAnnotations(vtkRenderer Renderer, List<ReformerTube> TubesToAnnotate, Dictionary<ReformerTube, vtkActor> TubesAndTheirActors)
            {
                List<int> rowsAnnotated = new List<int>();
                int minRow = TubesToAnnotate.Min(T => T.ReformerAttributes.RowNumber);
                int maxRow = TubesToAnnotate.Max(T => T.ReformerAttributes.RowNumber);
                foreach (ReformerTube tube in TubesToAnnotate)
                {
                    if (TubeAnnotations.ContainsKey(tube)) continue; //If it already exists, then no need to create it.
                    string tubeAnnotationText = string.Format("Tube {0}", tube.ReformerAttributes.TubeNumber);
                    vtkActor tubeActor = TubesAndTheirActors[tube];
                    vtkFollower tubeAnnotation = AnnotateTube(tubeActor, tubeAnnotationText, Renderer);
                    TubeAnnotations.Add(tube, tubeAnnotation);

                    //if (rowsAnnotated.Contains(tube.ReformerAttributes.RowNumber)) continue; //This gives one row indicator per row.
                    //Turn off the row indicators for all but the first and last row.
                    int rowNum = tube.ReformerAttributes.RowNumber;
                    if (maxRow <= 1 || rowNum == 0 || (rowNum != minRow && rowNum != maxRow) || !AnnotateRows) continue;
                    string rowAnnotationText = string.Format("Row {0}", tube.Inspection.ReformerInfo.UseRowLetters ? tube.ReformerAttributes.RowLetter.ToString() : tube.ReformerAttributes.RowNumber.ToString());
                    vtkFollower rowAnnotation = AnnotateRow(tubeActor, rowAnnotationText, Renderer);
                    RowAnnotations.Add(tube, rowAnnotation);
                    rowsAnnotated.Add(tube.ReformerAttributes.RowNumber);
                }

                UpdateHeader(TubesToAnnotate.Count > 0 ? TubesToAnnotate.First() : null);
            }


        }

        public void SetDefaultCameraLocation(CameraLocationName cameraLocation)
        {
            if (_actorsOnDisplay.Count <= 0) return;
            var camera = Renderer.GetActiveCamera();
                
            float frontOrBackModifier = cameraLocation.ToString().Contains("Front") ? 1 : -1;
            float rightOrLeftModifier = 0;
            if (cameraLocation.ToString().Contains("Left"))
            {
                rightOrLeftModifier = -1;
            }
            else if (cameraLocation.ToString().Contains("Right"))
            {
                rightOrLeftModifier = 1;
            }
                
            //var currentPositions = camera.GetPosition();
            //var firstTubeActor = _actorsOnDisplay.First().Value;
            var boundsOfAssembly = _assembly.GetBounds();
            //var firstTubeBounds = firstTubeActor.GetBounds();
            var xSize = boundsOfAssembly[1] - boundsOfAssembly[0];
            //var xTubeSize = firstTubeBounds[1] - firstTubeBounds[0];
            var ySize = boundsOfAssembly[3] - boundsOfAssembly[2];
            //var numTubesInRow = _actorsOnDisplay.Keys.Max(T => T.ReformerAttributes.TubeNumber);
            var numRows = _actorsOnDisplay.Keys.Max(T => T.ReformerAttributes.RowNumber);
            float radiansInOneDegree = .0174532925f; 
            var yDistanceAtAngle = ySize / 2 / Math.Sin(1.2 * radiansInOneDegree)  * rightOrLeftModifier;
            double xFlyAboveAdjustment = 0;
            if (cameraLocation != CameraLocationName.Back && cameraLocation != CameraLocationName.Front)
            {
                xFlyAboveAdjustment = -1 * 2 * xSize;
            }

            double xPosition = xSize / 2 + xFlyAboveAdjustment;
            //double xTubePosition = xTubeSize / 2;
            double yPosition = yDistanceAtAngle;
            double zPosition = (xSize + numRows * (ySize + RowSpacing - 1)) * frontOrBackModifier * 1.25;

            camera.SetPosition(xPosition, yPosition, zPosition);
            FocusCamera(_assembly);
            //Roll the camera so the a tube appears to be vertical, or a change in the X direction is vertical.
            var zeroScreenPoint = Renderer.WorldToScreenCoordinate(0, 0, 0);
            var otherScreenPoint = Renderer.WorldToScreenCoordinate(100000, 0, 0);
            double changeInY = otherScreenPoint[1] - zeroScreenPoint[1];
            double changeInX = otherScreenPoint[0] - zeroScreenPoint[0];
            var rollDegrees = 180 / Math.PI * Math.Atan(changeInY / changeInX);
            if (changeInY < 0 && changeInX < 0) rollDegrees += 180;
            //Change in X should be 100, change in Y should be 0

            camera.Roll((90 + rollDegrees) * -1);
               

            foreach (var thing in DirectionAnnotations)
            {
                thing.SetVisibility(1);
            }
            DirectionAnnotations.OrderByDescending(T => T.GetDistanceFromCamera(camera)).First().SetVisibility(false);
                
            Renderer.ResetCameraClippingRange();
            WindowControl.RenderWindow.Render();
        }
    }
    
    public enum CameraLocationName
    {
        Front,
        FrontRight,
        FrontLeft,
        Back,
        BackRight,
        BackLeft
    }
}
