﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using NGenerics.Extensions;
using QuestIntegrity.Core.Units;
using QuestIntegrity.Graphics.ColorScaleBar;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.FullReformerViewProcess
{
    public sealed partial class FullReformerView : BaseProcessWindow
    {
        private readonly LayoutCreater _layoutCreater;
        private readonly bool _settingDefaults;
        
        #region Properties

       
        private ReformerLayout ReformerType
        { get { return (ReformerLayout)Enum.Parse(typeof(ReformerLayout), radRefType.EditValue.ToString()); } }

        private TubeNumberingDirection NumberingDirection
        { get  { return (TubeNumberingDirection) Enum.Parse(typeof(TubeNumberingDirection), uiNumberingDirection.EditValue.ToString()); } }

        //Tim redacted his statement they will only ever export Inner Radius data.
        private ReformerScalar TheScalar 
        { get { return CurrentInspection != null && CurrentInspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside; } }

        public MapDirections Row1Direction { get { return (MapDirections)Enum.Parse(typeof(MapDirections), uiRow1Direction.EditValue.ToString()); } }

        public float RowSpacingInInches 
        { get { return float.Parse(uiRowSpacing.EditValue.ToString()) * (float)DisplayUnits.Instance.MeasurementUnits.ToInches; } }

        public float TubeSpacingInInches
        { get { return float.Parse(uiTubeSpacing.EditValue.ToString()) * (float)DisplayUnits.Instance.MeasurementUnits.ToInches; } }

        public float RadiusInInches
        { get { return float.Parse(uiCylindricalRefRadius.EditValue.ToString()) * (float)DisplayUnits.Instance.AxialDistanceUnits.ToInches; } }

        public List<ReformerTube> TubesToDraw
        { 
            get 
            {
                var rows = TubeView.GetSelectedRows();
                List<ReformerTube> tubesSelected = new List<ReformerTube>();
                foreach (int rowHandle in rows)
                {
                    var tube = TubeView.GetRow(rowHandle) as ReformerTube;
                    if (tubesSelected.Contains(tube) || tube == null) continue;
                    tubesSelected.Add(tube);
                }
                return tubesSelected.OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).ToList();
            } 
        }

        #endregion Properties

        public FullReformerView()
        {
            _settingDefaults = true;
            InitializeComponent();
            _layoutCreater = new LayoutCreater(OnProgressChanged);
            UpdateDisplayUnits();
            SetUIValues();
            pnlPlanView.Controls.Add(_layoutCreater.WindowControl);
            _settingDefaults = false;
        }
        
        #region Private Methods

        #region Initializing Stuff

        /// <summary> Set the labels of items to contain the proper unit indicators. </summary>
        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDisplayUnits));
                return;
            }
            string spacingUnits = DefaultValues.MeasurementUnitSymbol;
            string axialUnits = DefaultValues.AxialUnitSymbol;
            lciTubeSpacing.Text = string.Format("Tube Spacing ({0})", spacingUnits);
            lciRowSpacing.Text = string.Format("Row Spacing ({0})", spacingUnits);
            lciCylindricalRefRadius.Text = String.Format("Reformer Radius ({0})", axialUnits);
            UpdateLayout();
            
        }

        private void SetUIValues()
        {
            //Set up some data sources
            uiNumberingDirection.Properties.Items.AddRange(Enum.GetValues(typeof(TubeNumberingDirection)));
            uiNumberingDirection.EditValue = TubeNumberingDirection.Clockwise;

            uiRow1Direction.Properties.Items.AddRange(Enum.GetValues(typeof(MapDirections)));
            uiRow1Direction.EditValue = MapDirections.N;

            bool inches = DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches;
            uiRowSpacing.EditValue = inches ? 100 : 2500;
            uiTubeSpacing.EditValue = inches ? 2 : 55;
            uiCylindricalRefRadius.EditValue = inches ? 144 : 3500;

            uiCameraLocation.SelectedIndex = 1;
        }

        private void SetInspectionDefaults()
        {
            SetDefaultColorBarLayout(CurrentInspection.Parent.ColorScale);
        }

        #endregion Initializing Stuff

        /// <summary> Triggers an update of changed items in the graphics window. </summary>
        private void UpdateLayout()
        {
            if (_settingDefaults) return;
            Cursor = Cursors.WaitCursor;
            if (CurrentInspection != null)
            {
                _layoutCreater.TubesToDraw = TubesToDraw;
                _layoutCreater.TubesToAnnotate = GetTubesToAnnotate();
            }
            
            _layoutCreater.ScalarKey2D = TheScalar;
            _layoutCreater.Layout = ReformerType;
            _layoutCreater.RowSpacing = RowSpacingInInches;
            _layoutCreater.TubeSpacing = TubeSpacingInInches;
            _layoutCreater.CylindricalRadius = RadiusInInches;
            _layoutCreater.NumberingDirection = NumberingDirection;
            _layoutCreater.Row1Direction = Row1Direction;
            _layoutCreater.Update(false);
            _layoutCreater.ResetCamera();
            Cursor = Cursors.Default;
        }

        /// <summary> Gets a list of tubes that should be annotated in the layout. The first and last of each row. </summary>
        private List<ReformerTube> GetTubesToAnnotate()
        {
            List<ReformerTube> returnTubes = new List<ReformerTube>();
            if (TubesToDraw.Count == 0) return returnTubes;
            int maxRow = TubesToDraw.Max(T => T.ReformerAttributes.RowNumber);
            if (ReformerType == ReformerLayout.Box)
            {
                for (int i = 0; i <= maxRow; i++)
                {
                    List<ReformerTube> tubesInRow = TubesToDraw.Where(T => T.ReformerAttributes.RowNumber == i).ToList();
                    if (tubesInRow.Count == 0) continue;
                    returnTubes.Add(tubesInRow.OrderBy(T => T.ReformerAttributes.RowNumber).First());
                    returnTubes.Add(tubesInRow.OrderBy(T => T.ReformerAttributes.RowNumber).Last());
                }
            }
            else if (TubesToDraw.Count > 0) //annotate up to 3 tubes, evenly separated.
            {
                returnTubes.Add(TubesToDraw.First());
                returnTubes.Add(TubesToDraw[(int)Math.Floor(TubesToDraw.Count / 3d)]);
                returnTubes.Add(TubesToDraw[(int)Math.Floor(TubesToDraw.Count / 3d * 2)]);
            }
            return returnTubes;
        }

        private void GetActualData()
        {
            if (CurrentInspection == null) return;
            int tubesWithoutIrData = TubesToDraw.Count(T => !T.AvailableScalars.Contains(ReformerScalar.RadiusInside));
            if (tubesWithoutIrData > 0)
            {
                OnProgressChanged(0, string.Format("No Inner Radius data for {0} tubes. Cannot plot the reformer.", tubesWithoutIrData));
                return;
            }
            Cursor = Cursors.WaitCursor;
            _layoutCreater.Update(true);
            Cursor = Cursors.Default;
        }

        private void ExportImage()
        {
            string defaultName = "FullReformerXX.jpg";
            if (CurrentInspection != null) defaultName = CurrentInspection.Name + "-" + defaultName;
            FileDialog file = new SaveFileDialog
                {
                    DefaultExt = ".jpg",
                    Title = @"Save an Image File",
                    Filter = @"JPeg Image|*.jpg",
                    InitialDirectory = UsefulDirectories.FullReformer.FullName,
                    FileName = defaultName
                };

            if (file.ShowDialog(this) != DialogResult.OK) return;
            
            FileInfo info = new FileInfo(file.FileName);
            if (info.Directory != null) _layoutCreater.TakeScreenshot(info.Directory.ToString(), info.Name);
        }

        #endregion Private Methods

        
        #region Event Handlers

        /// <summary> Enable or disable Row selection depending on the type of reformer tube layout. </summary>
        private void radRefType_SelectedIndexChanged(object sender, EventArgs e)
        {
            uiRowSpacing.Enabled = ReformerType == ReformerLayout.Box;
            uiCylindricalRefRadius.Enabled = ReformerType == ReformerLayout.Cylindrical;
        }
        
        
        private void SetDefaultColorBarLayout(ColorScaleBar CSB)
        {
            ColorScaleBar localColorBar = new ColorScaleBar();
            localColorBar.CopyFrom(CSB);
            //double the color bar values on this local version as we're dealing with diameter now.
            localColorBar.MinMaxRanges.ForEach(scalarRange => { scalarRange.Min = scalarRange.Min * 2; scalarRange.Max = scalarRange.Max * 2; });
            localColorBar.ActiveScalarName = TheScalar.ToString();
            localColorBar.Orientation = ColorScaleBar.ColorScaleBarOrientation.Vertical;
            localColorBar.Height = .8;
            localColorBar.Width = .1;
            localColorBar.Position = new[] { .87, .1 };
            localColorBar.Build();
            string measSymbol = DisplayUnits.Instance.AxialDistanceUnits.Scale == Length.LengthScale.Millimeters ? "mm" : "in.";
            localColorBar.Title = string.Format("{0} ({1})", "Diameter", measSymbol); //this has been flopped a couple times. Don't budge next time because it's dumb... will do it next time...
            _layoutCreater.SetColorBar(localColorBar);
            
        }

        private void btnDraw_Click(object sender, EventArgs e)
        {
            if (CurrentInspection != null )
            GetActualData();
        }

        private void UIValueChanged(object sender, EventArgs e)
        {
            UpdateLayout();
        }

        private void btnExportImage_Click(object sender, EventArgs e)
        {
            ExportImage();
        }

        //The OnHandleCreated method has to add the graphics panel back in if it's been removed. Otherwise VTK will crash (give a black screen in debug, crash at end user)
        protected override void OnHandleCreated(EventArgs e)
        {
            if (pnlPlanView != null)  { pnlPlanView.Controls.Add(_layoutCreater.WindowControl); }
            base.OnHandleCreated(e);
        }

        //The OnHandleDestroyed method has to remove the VTK graphics panel so it doesn't get screwed up. Otherwise VTK will crash (give a black screen in debug, crash at end user)
        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (pnlPlanView != null && !Closing) { pnlPlanView.Controls.Remove(_layoutCreater.WindowControl); }
            base.OnHandleDestroyed(e);
        }

        #endregion Event Handlers

        public override void UpdateDataSource()
        {
            if (InvokeRequired) { Invoke(new Action(UpdateDataSource)); return; }

            CurrentInspection = CurrentProject.CurrentInspectionFile;
            if (CurrentInspection != null)
            {
                SetInspectionDefaults();
                uiTubes.Properties.DataSource = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.;
                uiTubes.SuspendLayout();
                //To select all tubes by default we have to open the dropdown, expnad all groups, select the items, and then close up the popup. Kinda silly.
                uiTubes.ShowPopup();
                TubeView.ExpandAllGroups();
                TubeView.SelectAll();
                uiTubes.ClosePopup();
                uiTubes.ResumeLayout();
                UpdateLayout();
            }
        }

        private void uiRow1Direction_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateLayout();
        }

        private void uiTubes_Closed(object sender, ClosedEventArgs e)
        {
            UpdateLayout();
        }

        private void uiCameraLocation_EditValueChanged(object sender, EventArgs e)
        {
       }

        private void uiCameraLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            _layoutCreater.SetDefaultCameraLocation((CameraLocationName)Enum.Parse(typeof(CameraLocationName), (string)uiCameraLocation.EditValue));
        }
       
        
    }
}
