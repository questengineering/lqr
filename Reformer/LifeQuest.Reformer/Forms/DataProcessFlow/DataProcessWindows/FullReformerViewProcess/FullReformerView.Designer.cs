﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.FullReformerViewProcess
{
    sealed partial class FullReformerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            _layoutCreater.Dispose();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radRefType = new DevExpress.XtraEditors.RadioGroup();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.uiCameraLocation = new DevExpress.XtraEditors.RadioGroup();
            this.uiTubes = new DevExpress.XtraEditors.GridLookUpEdit();
            this.TubeView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiControlHints = new System.Windows.Forms.TextBox();
            this.uiRow1Direction = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pnlPlanView = new DevExpress.XtraEditors.XtraScrollableControl();
            this.btnExportImage = new DevExpress.XtraEditors.SimpleButton();
            this.uiRowSpacing = new DevExpress.XtraEditors.SpinEdit();
            this.uiTubeSpacing = new DevExpress.XtraEditors.SpinEdit();
            this.btnGetData = new DevExpress.XtraEditors.SimpleButton();
            this.uiCylindricalRefRadius = new DevExpress.XtraEditors.SpinEdit();
            this.uiNumberingDirection = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lciMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciPlanView = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNumberingDirection = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCylindricalRefRadius = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTubeSpacing = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRowSpacing = new DevExpress.XtraLayout.LayoutControlItem();
            this.Type = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRow1Face = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciControls = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTubes = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCamera = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.radRefType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCameraLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRow1Direction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRowSpacing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeSpacing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCylindricalRefRadius.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNumberingDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlanView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumberingDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCylindricalRefRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRowSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRow1Face)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCamera)).BeginInit();
            this.SuspendLayout();
            // 
            // radRefType
            // 
            this.radRefType.EditValue = "Box";
            this.radRefType.Location = new System.Drawing.Point(33, 26);
            this.radRefType.Name = "radRefType";
            this.radRefType.Properties.Columns = 2;
            this.radRefType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Box", "Box"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Cylindrical", "Cylindrical")});
            this.radRefType.Size = new System.Drawing.Size(141, 25);
            this.radRefType.StyleController = this.lcMain;
            this.radRefType.TabIndex = 4;
            this.radRefType.SelectedIndexChanged += new System.EventHandler(this.radRefType_SelectedIndexChanged);
            this.radRefType.EditValueChanged += new System.EventHandler(this.UIValueChanged);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.uiCameraLocation);
            this.lcMain.Controls.Add(this.uiTubes);
            this.lcMain.Controls.Add(this.uiControlHints);
            this.lcMain.Controls.Add(this.uiRow1Direction);
            this.lcMain.Controls.Add(this.pnlPlanView);
            this.lcMain.Controls.Add(this.btnExportImage);
            this.lcMain.Controls.Add(this.radRefType);
            this.lcMain.Controls.Add(this.uiRowSpacing);
            this.lcMain.Controls.Add(this.uiTubeSpacing);
            this.lcMain.Controls.Add(this.btnGetData);
            this.lcMain.Controls.Add(this.uiCylindricalRefRadius);
            this.lcMain.Controls.Add(this.uiNumberingDirection);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2513, 322, 250, 350);
            this.lcMain.Root = this.lciMain;
            this.lcMain.Size = new System.Drawing.Size(796, 551);
            this.lcMain.TabIndex = 1;
            this.lcMain.Text = "layoutControl1";
            // 
            // uiCameraLocation
            // 
            this.uiCameraLocation.Location = new System.Drawing.Point(2, 217);
            this.uiCameraLocation.Name = "uiCameraLocation";
            this.uiCameraLocation.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Front", "Front"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("FrontRight", "Front Right"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("FrontLeft", "Front Left"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Back", "Back"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("BackLeft", "Back Left"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("BackRight", "Back Right")});
            this.uiCameraLocation.Size = new System.Drawing.Size(172, 112);
            this.uiCameraLocation.StyleController = this.lcMain;
            this.uiCameraLocation.TabIndex = 29;
            this.uiCameraLocation.SelectedIndexChanged += new System.EventHandler(this.uiCameraLocation_SelectedIndexChanged);
            this.uiCameraLocation.EditValueChanged += new System.EventHandler(this.uiCameraLocation_EditValueChanged);
            // 
            // uiTubes
            // 
            this.uiTubes.Location = new System.Drawing.Point(34, 2);
            this.uiTubes.Name = "uiTubes";
            this.uiTubes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTubes.Properties.DisplayMember = "Name";
            this.uiTubes.Properties.NullText = "";
            this.uiTubes.Properties.View = this.TubeView;
            this.uiTubes.Size = new System.Drawing.Size(140, 20);
            this.uiTubes.StyleController = this.lcMain;
            this.uiTubes.TabIndex = 28;
            this.uiTubes.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.uiTubes_Closed);
            // 
            // TubeView
            // 
            this.TubeView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow});
            this.TubeView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.TubeView.GroupCount = 1;
            this.TubeView.Name = "TubeView";
            this.TubeView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.TubeView.OptionsSelection.MultiSelect = true;
            this.TubeView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.TubeView.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.TubeView.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            this.TubeView.OptionsView.ShowGroupPanel = false;
            this.TubeView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Tube";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // uiControlHints
            // 
            this.uiControlHints.Enabled = false;
            this.uiControlHints.Location = new System.Drawing.Point(2, 349);
            this.uiControlHints.Multiline = true;
            this.uiControlHints.Name = "uiControlHints";
            this.uiControlHints.ReadOnly = true;
            this.uiControlHints.Size = new System.Drawing.Size(172, 62);
            this.uiControlHints.TabIndex = 27;
            this.uiControlHints.Text = "Ctrl+Mouse - Rotate\r\nShift+Mouse  - Pan\r\nRightClick+Mouse - Zoom\r\nMiddle Scroll -" +
    " Zoom";
            // 
            // uiRow1Direction
            // 
            this.uiRow1Direction.Location = new System.Drawing.Point(65, 151);
            this.uiRow1Direction.Name = "uiRow1Direction";
            this.uiRow1Direction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiRow1Direction.Properties.DropDownRows = 8;
            this.uiRow1Direction.Size = new System.Drawing.Size(109, 20);
            this.uiRow1Direction.StyleController = this.lcMain;
            this.uiRow1Direction.TabIndex = 26;
            this.uiRow1Direction.SelectedIndexChanged += new System.EventHandler(this.uiRow1Direction_SelectedIndexChanged);
            // 
            // pnlPlanView
            // 
            this.pnlPlanView.Location = new System.Drawing.Point(178, 2);
            this.pnlPlanView.Name = "pnlPlanView";
            this.pnlPlanView.Size = new System.Drawing.Size(616, 547);
            this.pnlPlanView.TabIndex = 25;
            // 
            // btnExportImage
            // 
            this.btnExportImage.Location = new System.Drawing.Point(90, 175);
            this.btnExportImage.Name = "btnExportImage";
            this.btnExportImage.Size = new System.Drawing.Size(84, 22);
            this.btnExportImage.StyleController = this.lcMain;
            this.btnExportImage.TabIndex = 23;
            this.btnExportImage.Text = "Export Image";
            this.btnExportImage.Click += new System.EventHandler(this.btnExportImage_Click);
            // 
            // uiRowSpacing
            // 
            this.uiRowSpacing.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.uiRowSpacing.Location = new System.Drawing.Point(93, 55);
            this.uiRowSpacing.Name = "uiRowSpacing";
            this.uiRowSpacing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uiRowSpacing.Size = new System.Drawing.Size(81, 20);
            this.uiRowSpacing.StyleController = this.lcMain;
            this.uiRowSpacing.TabIndex = 8;
            this.uiRowSpacing.EditValueChanged += new System.EventHandler(this.UIValueChanged);
            // 
            // uiTubeSpacing
            // 
            this.uiTubeSpacing.EditValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.uiTubeSpacing.Location = new System.Drawing.Point(73, 79);
            this.uiTubeSpacing.Name = "uiTubeSpacing";
            this.uiTubeSpacing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uiTubeSpacing.Size = new System.Drawing.Size(101, 20);
            this.uiTubeSpacing.StyleController = this.lcMain;
            this.uiTubeSpacing.TabIndex = 7;
            this.uiTubeSpacing.EditValueChanged += new System.EventHandler(this.UIValueChanged);
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(2, 175);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(84, 22);
            this.btnGetData.StyleController = this.lcMain;
            this.btnGetData.TabIndex = 13;
            this.btnGetData.Text = "Get Color Data";
            this.btnGetData.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // uiCylindricalRefRadius
            // 
            this.uiCylindricalRefRadius.EditValue = new decimal(new int[] {
            144,
            0,
            0,
            0});
            this.uiCylindricalRefRadius.Location = new System.Drawing.Point(41, 103);
            this.uiCylindricalRefRadius.Name = "uiCylindricalRefRadius";
            this.uiCylindricalRefRadius.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.uiCylindricalRefRadius.Size = new System.Drawing.Size(133, 20);
            this.uiCylindricalRefRadius.StyleController = this.lcMain;
            this.uiCylindricalRefRadius.TabIndex = 10;
            this.uiCylindricalRefRadius.EditValueChanged += new System.EventHandler(this.UIValueChanged);
            // 
            // uiNumberingDirection
            // 
            this.uiNumberingDirection.Location = new System.Drawing.Point(105, 127);
            this.uiNumberingDirection.Name = "uiNumberingDirection";
            this.uiNumberingDirection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiNumberingDirection.Size = new System.Drawing.Size(69, 20);
            this.uiNumberingDirection.StyleController = this.lcMain;
            this.uiNumberingDirection.TabIndex = 11;
            this.uiNumberingDirection.EditValueChanged += new System.EventHandler(this.UIValueChanged);
            // 
            // lciMain
            // 
            this.lciMain.CustomizationFormText = "lciMain";
            this.lciMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lciMain.GroupBordersVisible = false;
            this.lciMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciPlanView,
            this.lciNumberingDirection,
            this.lciCylindricalRefRadius,
            this.lciTubeSpacing,
            this.lciRowSpacing,
            this.Type,
            this.lciRow1Face,
            this.lciControls,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.lciTubes,
            this.lciCamera});
            this.lciMain.Location = new System.Drawing.Point(0, 0);
            this.lciMain.Name = "lciMain";
            this.lciMain.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.lciMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciMain.Size = new System.Drawing.Size(796, 551);
            this.lciMain.Text = "lciMain";
            this.lciMain.TextVisible = false;
            // 
            // lciPlanView
            // 
            this.lciPlanView.Control = this.pnlPlanView;
            this.lciPlanView.CustomizationFormText = "lciPlanView";
            this.lciPlanView.Location = new System.Drawing.Point(176, 0);
            this.lciPlanView.Name = "lciPlanView";
            this.lciPlanView.Size = new System.Drawing.Size(620, 551);
            this.lciPlanView.Text = "lciPlanView";
            this.lciPlanView.TextSize = new System.Drawing.Size(0, 0);
            this.lciPlanView.TextToControlDistance = 0;
            this.lciPlanView.TextVisible = false;
            // 
            // lciNumberingDirection
            // 
            this.lciNumberingDirection.Control = this.uiNumberingDirection;
            this.lciNumberingDirection.CustomizationFormText = "Numbering Direction";
            this.lciNumberingDirection.Location = new System.Drawing.Point(0, 125);
            this.lciNumberingDirection.Name = "lciNumberingDirection";
            this.lciNumberingDirection.Size = new System.Drawing.Size(176, 24);
            this.lciNumberingDirection.Text = "Numbering Direction:";
            this.lciNumberingDirection.TextSize = new System.Drawing.Size(100, 13);
            // 
            // lciCylindricalRefRadius
            // 
            this.lciCylindricalRefRadius.Control = this.uiCylindricalRefRadius;
            this.lciCylindricalRefRadius.CustomizationFormText = "Radius";
            this.lciCylindricalRefRadius.Location = new System.Drawing.Point(0, 101);
            this.lciCylindricalRefRadius.Name = "lciCylindricalRefRadius";
            this.lciCylindricalRefRadius.Size = new System.Drawing.Size(176, 24);
            this.lciCylindricalRefRadius.Text = "Radius:";
            this.lciCylindricalRefRadius.TextSize = new System.Drawing.Size(36, 13);
            // 
            // lciTubeSpacing
            // 
            this.lciTubeSpacing.Control = this.uiTubeSpacing;
            this.lciTubeSpacing.CustomizationFormText = "Tube Spacing";
            this.lciTubeSpacing.Location = new System.Drawing.Point(0, 77);
            this.lciTubeSpacing.Name = "lciTubeSpacing";
            this.lciTubeSpacing.Size = new System.Drawing.Size(176, 24);
            this.lciTubeSpacing.Text = "Tube Spacing:";
            this.lciTubeSpacing.TextSize = new System.Drawing.Size(68, 13);
            // 
            // lciRowSpacing
            // 
            this.lciRowSpacing.Control = this.uiRowSpacing;
            this.lciRowSpacing.CustomizationFormText = "Row Spacing (xx):";
            this.lciRowSpacing.Location = new System.Drawing.Point(0, 53);
            this.lciRowSpacing.MaxSize = new System.Drawing.Size(176, 24);
            this.lciRowSpacing.MinSize = new System.Drawing.Size(145, 24);
            this.lciRowSpacing.Name = "lciRowSpacing";
            this.lciRowSpacing.Size = new System.Drawing.Size(176, 24);
            this.lciRowSpacing.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciRowSpacing.Text = "Row Spacing (xx):";
            this.lciRowSpacing.TextSize = new System.Drawing.Size(88, 13);
            // 
            // Type
            // 
            this.Type.Control = this.radRefType;
            this.Type.CustomizationFormText = "Type:";
            this.Type.Location = new System.Drawing.Point(0, 24);
            this.Type.MaxSize = new System.Drawing.Size(176, 29);
            this.Type.MinSize = new System.Drawing.Size(176, 29);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(176, 29);
            this.Type.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.Type.Text = "Type:";
            this.Type.TextSize = new System.Drawing.Size(28, 13);
            // 
            // lciRow1Face
            // 
            this.lciRow1Face.Control = this.uiRow1Direction;
            this.lciRow1Face.CustomizationFormText = "Row 1 Face:";
            this.lciRow1Face.Location = new System.Drawing.Point(0, 149);
            this.lciRow1Face.Name = "lciRow1Face";
            this.lciRow1Face.Size = new System.Drawing.Size(176, 24);
            this.lciRow1Face.Text = "Row 1 Face:";
            this.lciRow1Face.TextSize = new System.Drawing.Size(60, 13);
            // 
            // lciControls
            // 
            this.lciControls.Control = this.uiControlHints;
            this.lciControls.CustomizationFormText = "Control Hints";
            this.lciControls.Location = new System.Drawing.Point(0, 331);
            this.lciControls.MaxSize = new System.Drawing.Size(176, 82);
            this.lciControls.MinSize = new System.Drawing.Size(176, 82);
            this.lciControls.Name = "lciControls";
            this.lciControls.Size = new System.Drawing.Size(176, 82);
            this.lciControls.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciControls.Text = "Control Hints";
            this.lciControls.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciControls.TextSize = new System.Drawing.Size(62, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 413);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(176, 138);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnGetData;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(88, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnExportImage;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(88, 173);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(88, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // lciTubes
            // 
            this.lciTubes.Control = this.uiTubes;
            this.lciTubes.CustomizationFormText = "Tubes";
            this.lciTubes.Location = new System.Drawing.Point(0, 0);
            this.lciTubes.Name = "lciTubes";
            this.lciTubes.Size = new System.Drawing.Size(176, 24);
            this.lciTubes.Text = "Tubes";
            this.lciTubes.TextSize = new System.Drawing.Size(29, 13);
            // 
            // lciCamera
            // 
            this.lciCamera.Control = this.uiCameraLocation;
            this.lciCamera.CustomizationFormText = "Set Camera Location";
            this.lciCamera.Location = new System.Drawing.Point(0, 199);
            this.lciCamera.MaxSize = new System.Drawing.Size(176, 132);
            this.lciCamera.MinSize = new System.Drawing.Size(176, 132);
            this.lciCamera.Name = "lciCamera";
            this.lciCamera.Size = new System.Drawing.Size(176, 132);
            this.lciCamera.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCamera.Text = "Set Camera Location";
            this.lciCamera.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciCamera.TextSize = new System.Drawing.Size(99, 13);
            // 
            // FullReformerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "FullReformerView";
            this.Size = new System.Drawing.Size(796, 551);
            ((System.ComponentModel.ISupportInitialize)(this.radRefType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiCameraLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRow1Direction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRowSpacing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeSpacing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCylindricalRefRadius.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNumberingDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlanView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumberingDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCylindricalRefRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRowSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRow1Face)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCamera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnGetData;
        private DevExpress.XtraEditors.RadioGroup radRefType;
        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraEditors.SpinEdit uiRowSpacing;
        private DevExpress.XtraEditors.SpinEdit uiTubeSpacing;
        private DevExpress.XtraEditors.SpinEdit uiCylindricalRefRadius;
        private DevExpress.XtraEditors.ComboBoxEdit uiNumberingDirection;
        private DevExpress.XtraLayout.LayoutControlGroup lciMain;
        private DevExpress.XtraEditors.SimpleButton btnExportImage;
        private DevExpress.XtraEditors.XtraScrollableControl pnlPlanView;
        private DevExpress.XtraLayout.LayoutControlItem lciPlanView;
        private DevExpress.XtraEditors.ComboBoxEdit uiRow1Direction;
        private System.Windows.Forms.TextBox uiControlHints;
        private DevExpress.XtraLayout.LayoutControlItem lciNumberingDirection;
        private DevExpress.XtraLayout.LayoutControlItem lciCylindricalRefRadius;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSpacing;
        private DevExpress.XtraLayout.LayoutControlItem lciRowSpacing;
        private DevExpress.XtraLayout.LayoutControlItem Type;
        private DevExpress.XtraLayout.LayoutControlItem lciRow1Face;
        private DevExpress.XtraLayout.LayoutControlItem lciControls;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.GridLookUpEdit uiTubes;
        private DevExpress.XtraGrid.Views.Grid.GridView TubeView;
        private DevExpress.XtraLayout.LayoutControlItem lciTubes;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraEditors.RadioGroup uiCameraLocation;
        private DevExpress.XtraLayout.LayoutControlItem lciCamera;


    }
}
