﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class Single3DExport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (GraphicsWindow!= null) GraphicsWindow.Dispose();
            DetachColorBarChangedEvent();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ScrollableGraphicsPanel = new DevExpress.XtraEditors.XtraScrollableControl();
            this.lcMenuItems = new DevExpress.XtraLayout.LayoutControl();
            this.btnExportAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportOne = new DevExpress.XtraEditors.SimpleButton();
            this.uiTubeRotation = new DevExpress.XtraEditors.SpinEdit();
            this.chkVertical = new DevExpress.XtraEditors.CheckEdit();
            this.uiStatMeasures = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uiScalar = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uiTube = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTubes = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgMenuItems = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTube = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciScalar = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciStatMeasure = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTubeRotation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMenuItems)).BeginInit();
            this.lcMenuItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeRotation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVertical.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiStatMeasures.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiScalar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTubes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMenuItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciScalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStatMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeRotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // ScrollableGraphicsPanel
            // 
            this.ScrollableGraphicsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScrollableGraphicsPanel.Location = new System.Drawing.Point(180, 0);
            this.ScrollableGraphicsPanel.Name = "ScrollableGraphicsPanel";
            this.ScrollableGraphicsPanel.Size = new System.Drawing.Size(456, 277);
            this.ScrollableGraphicsPanel.TabIndex = 4;
            this.ScrollableGraphicsPanel.TabStop = false;
            // 
            // lcMenuItems
            // 
            this.lcMenuItems.Controls.Add(this.btnExportAll);
            this.lcMenuItems.Controls.Add(this.btnExportOne);
            this.lcMenuItems.Controls.Add(this.uiTubeRotation);
            this.lcMenuItems.Controls.Add(this.chkVertical);
            this.lcMenuItems.Controls.Add(this.uiStatMeasures);
            this.lcMenuItems.Controls.Add(this.uiScalar);
            this.lcMenuItems.Controls.Add(this.uiTube);
            this.lcMenuItems.Dock = System.Windows.Forms.DockStyle.Left;
            this.lcMenuItems.Location = new System.Drawing.Point(0, 0);
            this.lcMenuItems.Name = "lcMenuItems";
            this.lcMenuItems.Root = this.lcgMenuItems;
            this.lcMenuItems.Size = new System.Drawing.Size(180, 277);
            this.lcMenuItems.TabIndex = 0;
            this.lcMenuItems.Text = "layoutControl1";
            // 
            // btnExportAll
            // 
            this.btnExportAll.Location = new System.Drawing.Point(92, 121);
            this.btnExportAll.Name = "btnExportAll";
            this.btnExportAll.Size = new System.Drawing.Size(86, 22);
            this.btnExportAll.StyleController = this.lcMenuItems;
            this.btnExportAll.TabIndex = 12;
            this.btnExportAll.Text = "Export All";
            this.btnExportAll.Click += new System.EventHandler(this.btnExportAllFromInspection_ItemClick);
            // 
            // btnExportOne
            // 
            this.btnExportOne.Location = new System.Drawing.Point(2, 121);
            this.btnExportOne.Name = "btnExportOne";
            this.btnExportOne.Size = new System.Drawing.Size(86, 22);
            this.btnExportOne.StyleController = this.lcMenuItems;
            this.btnExportOne.TabIndex = 11;
            this.btnExportOne.Text = "Export One";
            this.btnExportOne.Click += new System.EventHandler(this.btnExportCurrentImage_ItemClick);
            // 
            // uiTubeRotation
            // 
            this.uiTubeRotation.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiTubeRotation.Location = new System.Drawing.Point(91, 97);
            this.uiTubeRotation.Name = "uiTubeRotation";
            this.uiTubeRotation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTubeRotation.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.uiTubeRotation.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.uiTubeRotation.Properties.IsFloatValue = false;
            this.uiTubeRotation.Properties.Mask.EditMask = "N00";
            this.uiTubeRotation.Properties.MaxValue = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.uiTubeRotation.Size = new System.Drawing.Size(87, 20);
            this.uiTubeRotation.StyleController = this.lcMenuItems;
            this.uiTubeRotation.TabIndex = 8;
            this.uiTubeRotation.EditValueChanged += new System.EventHandler(this.RotationChanged);
            // 
            // chkVertical
            // 
            this.chkVertical.Location = new System.Drawing.Point(2, 74);
            this.chkVertical.Name = "chkVertical";
            this.chkVertical.Properties.Caption = "Vertical";
            this.chkVertical.Size = new System.Drawing.Size(176, 19);
            this.chkVertical.StyleController = this.lcMenuItems;
            this.chkVertical.TabIndex = 7;
            this.chkVertical.CheckedChanged += new System.EventHandler(this.chkVertical_CheckedChanged);
            // 
            // uiStatMeasures
            // 
            this.uiStatMeasures.Location = new System.Drawing.Point(34, 50);
            this.uiStatMeasures.Name = "uiStatMeasures";
            this.uiStatMeasures.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiStatMeasures.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.uiStatMeasures.Size = new System.Drawing.Size(144, 20);
            this.uiStatMeasures.StyleController = this.lcMenuItems;
            this.uiStatMeasures.TabIndex = 6;
            this.uiStatMeasures.SelectedIndexChanged += new System.EventHandler(this.SubScalarChanged);
            // 
            // uiScalar
            // 
            this.uiScalar.Location = new System.Drawing.Point(34, 26);
            this.uiScalar.Name = "uiScalar";
            this.uiScalar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiScalar.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.uiScalar.Size = new System.Drawing.Size(144, 20);
            this.uiScalar.StyleController = this.lcMenuItems;
            this.uiScalar.TabIndex = 5;
            this.uiScalar.SelectedIndexChanged += new System.EventHandler(this.ScalarChanged);
            // 
            // uiTube
            // 
            this.uiTube.Location = new System.Drawing.Point(34, 2);
            this.uiTube.Name = "uiTube";
            this.uiTube.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTube.Properties.DataSource = this.BSTubes;
            this.uiTube.Properties.DisplayMember = "Name";
            this.uiTube.Properties.NullText = "";
            this.uiTube.Properties.View = this.gridLookUpEdit1View;
            this.uiTube.Size = new System.Drawing.Size(144, 20);
            this.uiTube.StyleController = this.lcMenuItems;
            this.uiTube.TabIndex = 4;
            this.uiTube.EditValueChanged += new System.EventHandler(this.TubeChanged);
            // 
            // BSTubes
            // 
            this.BSTubes.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // lcgMenuItems
            // 
            this.lcgMenuItems.CustomizationFormText = "lcgMenuItems";
            this.lcgMenuItems.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMenuItems.GroupBordersVisible = false;
            this.lcgMenuItems.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTube,
            this.lciScalar,
            this.lciStatMeasure,
            this.layoutControlItem1,
            this.lciTubeRotation,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.lcgMenuItems.Location = new System.Drawing.Point(0, 0);
            this.lcgMenuItems.Name = "lcgMenuItems";
            this.lcgMenuItems.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMenuItems.Size = new System.Drawing.Size(180, 277);
            this.lcgMenuItems.Text = "lcgMenuItems";
            this.lcgMenuItems.TextVisible = false;
            // 
            // lciTube
            // 
            this.lciTube.Control = this.uiTube;
            this.lciTube.CustomizationFormText = "Tube";
            this.lciTube.Location = new System.Drawing.Point(0, 0);
            this.lciTube.Name = "lciTube";
            this.lciTube.Size = new System.Drawing.Size(180, 24);
            this.lciTube.Text = "Tube";
            this.lciTube.TextSize = new System.Drawing.Size(29, 13);
            // 
            // lciScalar
            // 
            this.lciScalar.Control = this.uiScalar;
            this.lciScalar.CustomizationFormText = "Scalar";
            this.lciScalar.Location = new System.Drawing.Point(0, 24);
            this.lciScalar.Name = "lciScalar";
            this.lciScalar.Size = new System.Drawing.Size(180, 24);
            this.lciScalar.Text = "Scalar";
            this.lciScalar.TextSize = new System.Drawing.Size(29, 13);
            // 
            // lciStatMeasure
            // 
            this.lciStatMeasure.Control = this.uiStatMeasures;
            this.lciStatMeasure.CustomizationFormText = "*Stat";
            this.lciStatMeasure.Location = new System.Drawing.Point(0, 48);
            this.lciStatMeasure.Name = "lciStatMeasure";
            this.lciStatMeasure.Size = new System.Drawing.Size(180, 24);
            this.lciStatMeasure.Text = "*Stat";
            this.lciStatMeasure.TextSize = new System.Drawing.Size(29, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkVertical;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(180, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciTubeRotation
            // 
            this.lciTubeRotation.Control = this.uiTubeRotation;
            this.lciTubeRotation.CustomizationFormText = "layoutControlItem2";
            this.lciTubeRotation.Location = new System.Drawing.Point(0, 95);
            this.lciTubeRotation.Name = "lciTubeRotation";
            this.lciTubeRotation.Size = new System.Drawing.Size(180, 24);
            this.lciTubeRotation.Text = "Tube Rotation (°)";
            this.lciTubeRotation.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciTubeRotation.TextSize = new System.Drawing.Size(84, 13);
            this.lciTubeRotation.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnExportOne;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(90, 158);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnExportAll;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(90, 119);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(90, 158);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // Single3DExport
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.Controls.Add(this.ScrollableGraphicsPanel);
            this.Controls.Add(this.lcMenuItems);
            this.DoubleBuffered = true;
            this.Name = "Single3DExport";
            this.Size = new System.Drawing.Size(636, 277);
            ((System.ComponentModel.ISupportInitialize)(this.lcMenuItems)).EndInit();
            this.lcMenuItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeRotation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVertical.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiStatMeasures.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiScalar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTubes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMenuItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciScalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStatMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeRotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl ScrollableGraphicsPanel;
        private DevExpress.XtraLayout.LayoutControl lcMenuItems;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMenuItems;
        private DevExpress.XtraEditors.GridLookUpEdit uiTube;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem lciTube;
        private System.Windows.Forms.BindingSource BSTubes;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraEditors.ComboBoxEdit uiScalar;
        private DevExpress.XtraLayout.LayoutControlItem lciScalar;
        private DevExpress.XtraEditors.ComboBoxEdit uiStatMeasures;
        private DevExpress.XtraLayout.LayoutControlItem lciStatMeasure;
        private DevExpress.XtraEditors.CheckEdit chkVertical;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SpinEdit uiTubeRotation;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeRotation;
        private DevExpress.XtraEditors.SimpleButton btnExportAll;
        private DevExpress.XtraEditors.SimpleButton btnExportOne;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}
