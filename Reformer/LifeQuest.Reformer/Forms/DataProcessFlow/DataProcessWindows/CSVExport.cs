﻿#region Copyright Quest Integrity Group, LLC 2012C:\Dev\LifeQuest\LifeQuest.Reformer\Forms\DataTools\ReformerTubeAdjustment.cs
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 12/4/2012 15:20:04 PM
// Created by:   J.Foster
//
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.Core.Maths;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public sealed partial class CSVExport : BaseProcessWindow
    {
        private readonly FolderBrowserDialog _folderBrowser = new FolderBrowserDialog();
        
        #region Constructors

        public CSVExport()
        {
            InitializeComponent();
            UpdateDataSource();
        }

               
        #endregion

        #region Public Methods

        /// <summary>
        /// Takes over the generic Run command and instead does something more useful.
        /// </summary>
        public void Run()
        {
            Cursor currentCursor = Cursor.Current;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor); 
            DirectoryInfo outputDir = new DirectoryInfo(folderSelecter.Text);  
            if (!outputDir.Exists)
            {
                OnProgressChanged(100, "Error: Output directory does not exist.");
                return;
            }
            if (radManOrLot.EditValue.ToString() == "MANTIS")
                ExportMANTIS(tubeSelecter.SelectedTubes, outputDir);
            else
                ExportLOTIS(tubeSelecter.SelectedTubes, outputDir);
            
            ReformerWindowManager.Instance.Main.SetCursor(currentCursor);     
        }
        
        #endregion

        #region Exporting Algorithms

        /// <summary>
        /// Gathers and exports ID data in LOTIS format for the tubes passed to it. The tubes must have valid IR data for this to work.
        /// </summary>
        private void ExportLOTIS(IEnumerable<ReformerTube> Tubes, DirectoryInfo SaveDirectory)
        {
            const int headerRow = 8;
            int tubesSkipped = 0, tubeCounter = 1;
            //Now go gather the average diameter and axial position data for each tube
            var reformerTubes = Tubes as ReformerTube[] ?? Tubes.ToArray();
            foreach (ReformerTube tube in reformerTubes)
            {
                List<string> avgDiamData = new List<string>();
                List<string> axialData = new List<string>();

                //Add blank header placeholders to arrays
                for (int headerIdx = 1; headerIdx < headerRow; headerIdx ++)
                {
                    avgDiamData.Add(string.Empty);
                    axialData.Add(string.Empty);
                }
                //Add actual headers as the proper position
                axialData.Add("MidPoint");
                avgDiamData.Add("Avg Diam");
                //Add one more blank line to match LOTIS format
                axialData.Add(string.Empty);
                avgDiamData.Add(string.Empty);

                //Get the raw radius data and then convert it to diameter while formatting it for output
                if (tube.AvailableScalars.Contains(ReformerScalar.RadiusInside))
                {
                    avgDiamData.AddRange(tube.Get2DScalarStatisticalData(ReformerScalar.RadiusInside, tube.Inspection.DataMeasure).First().Value.Select(Point => (Point * 2).ToString("0.#####")));
                    List<float> positions;
                    tube.GetPositions(out positions);
                    axialData.AddRange(positions.Select(Point => Point.ToString("F5")));
                    
                    List<object> outputDataByColumn = new List<object>
                        {
                            axialData,
                            null,
                            avgDiamData
                        };
                    FileInfo saveLocation = new FileInfo(SaveDirectory.ToString() + Path.DirectorySeparatorChar + 
                        tube.Inspection.Name + Path.DirectorySeparatorChar +
                        tube.Name + ".csv");
                    WriteDataToCsvFile(outputDataByColumn, saveLocation, headerRow);
                    OnProgressChanged((int)Math.Round((float)tubeCounter / reformerTubes.Count() * 100), string.Format("Wrote: {0}", saveLocation.FullName));
                }
                else  { tubesSkipped++; }
                tubeCounter++;
                
            }
            //Tell the user how it went.
            OnProgressChanged(100, "All Done");
            string doneMessage = tubesSkipped > 0 ? string.Format("{0} tubes skipped due to lack of Inner Radius Data.", tubesSkipped) : "Output Completed Successfully.";
            MessageBox.Show(this, doneMessage);
        }

        private void WriteDataToCsvFile(List<object> OutputDataByColumn, FileInfo FullFile, int HeaderRows)
        {
            if (FullFile.Directory != null) Directory.CreateDirectory(FullFile.Directory.ToString());
            StreamWriter csvWriter = new StreamWriter(FullFile.FullName, false);
            for (int row = 0; row < ((IList)OutputDataByColumn[0]).Count; row++)
            {
                OutputDataByColumn.ForEach(Column =>
                    {
                        //Write the actual data if it's a column of data, otherwise it'll be blank.
                        if (Column != null)
                        {
                            string data = ((IList)Column)[row].ToString(); 
                            csvWriter.Write(data);
                        }
                        //Insert a comma or a linebreak.
                        bool lastColumn = Column == OutputDataByColumn[OutputDataByColumn.Count - 1];
                        if (!lastColumn & row >= HeaderRows - 1) { csvWriter.Write(","); }
                        else if (lastColumn) { csvWriter.WriteLine(); }
                    });                
            }
            csvWriter.Close();
        }

        /// <summary>
        /// Recreates a MANTIS .csv file with data.
        /// </summary>
        /// <param name="Tubes"></param>
        /// <param name="SaveDirectory"></param>
        private void ExportMANTIS(IEnumerable<ReformerTube> Tubes, DirectoryInfo SaveDirectory)
        {
            const int headerRow = 10;
            int tubesSkipped = 0, tubeCounter = 1;
            //No go gather the average diameter and axial position data for each tube
            var reformerTubes = Tubes.ToArray();
            foreach (ReformerTube tube in reformerTubes)
            {
                IEnumerable<List<string>> headerData = GenerateMANTISHeader(tube,headerRow);
                List<string> avgDiamData = new List<string>();
                List<float> axialData = new List<float>();
                List<List<string>> sensorData = GetMANTISSensorData(tube);
                
                //Get the raw radius data and then convert it to diameter while formatting it for output
                if (tube.AvailableScalars.Contains(ReformerScalar.RadiusOutside))
                {
                    avgDiamData.AddRange(tube.Get2DScalarStatisticalData(ReformerScalar.RadiusOutside, StatisticalMeasures.Mean).First().Value.Select(Point => (Point * 2).ToString("0.#####")));
                    List<float> positions;
                    tube.GetPositions(out positions);
                    axialData.AddRange(positions);

                    //Shift the axial data to start at 0 for Bob's graphics algorithm.
                    axialData = ShiftToStartAtZero(axialData);

                    List<object> outputDataByColumn = CombineMANTISDataAndHeader(headerData, axialData, avgDiamData, sensorData);
                    
                    FileInfo saveLocation = new FileInfo(SaveDirectory.ToString() + Path.DirectorySeparatorChar +
                        tube.Inspection.Name + Path.DirectorySeparatorChar +
                        tube.Name + ".csv");
                    WriteDataToCsvFile(outputDataByColumn, saveLocation, headerRow);
                    OnProgressChanged((int)Math.Round((float)tubeCounter / reformerTubes.Count() * 100), string.Format("Wrote: {0}", saveLocation.FullName));
                }
                else { tubesSkipped++; }
                tubeCounter++;
            }
            //Tell the user how it went.
            OnProgressChanged(100, "All Done");
            string doneMessage = tubesSkipped > 0 ? string.Format("{0} tubes skipped due to lack of Inner Radius Data.", tubesSkipped) : "Output Completed Successfully."; 
            MessageBox.Show(this, doneMessage);
        }

        private static List<float> ShiftToStartAtZero(List<float> Data)
        {
            float max = Data.Max();
            List<float> returnData = Data.Select(D => D - max).ToList();
            return returnData;
        }

        private List<object> CombineMANTISDataAndHeader(IEnumerable<List<string>> HeaderData, IEnumerable<float> AxialData, List<string> AvgDiamData, List<List<string>> SensorData)
        {
            List<object> outputDataByColumn = new List<object>();
            outputDataByColumn.AddRange(HeaderData);
            List<string> sAxialData = AxialData.Select(D => D.ToString("0.#####")).ToList(); //Convert list of doubles to list of string.
            for (int columnNum = 0; columnNum < outputDataByColumn.Count; columnNum++)
            {
                List<string> sourceList;
                switch (columnNum)
                {
                    case 0: case 1:
                        sourceList = sAxialData;
                        break;
                    case 2: case 3:
                        sourceList = AvgDiamData;
                        break;
                    default:
                        sourceList = SensorData[columnNum - 4];
                        break;
                }
                ((List<string>)outputDataByColumn[columnNum]).AddRange(sourceList);
            }
            return outputDataByColumn;
        }

        private List<List<string>> GetMANTISSensorData(ReformerTube Tube)
        {
            List<List<string>> returnData = new List<List<string>>();
            for (int sensor = 1; sensor <= 8; sensor++)
            {
                List<string> sensorData = new List<string>();

                if (sensor <= Tube.Inspection.NumRadialReadings)
                {
                    List<float> outputData;
                    Tube.Get2DScalarForSensor(ReformerScalar.RadiusOutside, sensor, out outputData);
                    //Make sure this is radius data. Individual sensors are reported as radius.
                    sensorData.AddRange(outputData.Select(Point => Point.ToString("0.#####")).ToList());                    
                }
                else //If there's only 6 sensors, put 'NaN' into the 7 and 8 slots for legacy software compatibility.
                { for (int index = 0; index < returnData[0].Count; index++) { sensorData.Add("NaN"); } }
                returnData.Add(sensorData);
            }
            return returnData;
        }

        /// <summary>  Returns a standard header for a MANTIS file. </summary>
        /// <returns>List of column headers, 'Axial Position,,Ave Diametr,,Sensor1++</returns>
        private IEnumerable<List<string>> GenerateMANTISHeader(ReformerTube Tube, int HeaderRow)
        {
            List<List<string>> header = new List<List<string>>();

            for (int col = 1; col <= 8 + 4; col++) //8 required sensors, and 4 data columns.
            {
                int sensor = col - 4;
                List<String> rowData = new List<string>();

                //Add blank header placeholders to arrays
                for (int headerIdx = 1; headerIdx < HeaderRow; headerIdx++)
                {
                    rowData.Add(headerIdx == 1 && col == 1 ? Tube.Inspection.NumRadialReadings.ToString("0") : string.Empty);
                }
                
                //Add the column headers to line 10
                switch (col)
                {
                    case 1:
                        rowData.Add("Axial Position");
                        break;
                    case 2: case 4:
                        rowData.Add(string.Empty);
                        break;
                    case 3:
                        rowData.Add("Ave Diametr");
                        break;
                    default:
                        rowData.Add("Sensor" + sensor);
                        break;
                }
                header.Add(rowData);
            }
            return header;
        }

        #endregion Exporting Algorithms

        #region Event Handlers

        private void folderSelecter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            _folderBrowser.Description = @"Select the folder to output to.";
            _folderBrowser.ShowNewFolderButton = true;
            DialogResult theResult = _folderBrowser.ShowDialog(this);
            if (theResult == DialogResult.OK)
            {
                folderSelecter.Text = _folderBrowser.SelectedPath;
                OnProgressChanged(100, "Selected output directory.");
            }
            OnProgressChanged(100, "Cencelled selecting output directory.");
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            Run();
        }

        #endregion Event Handlers

        public override void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDataSource)); //restart method on the proper thread
                return;
            }
            tubeSelecter.InitData(CurrentProject, true);
            
            if (CurrentInspection == null) return;
            try
            {
                DirectoryInfo newDir = new DirectoryInfo(Path.Combine(CurrentProject.DirectoryPath, "FinalCSVs\\"));
                if (!newDir.Exists) newDir.Create();
                folderSelecter.Text = newDir.FullName;
                _folderBrowser.SelectedPath = newDir.FullName;
                if (CurrentInspection.InspectionTool == Tool.LOTIS) //Remove the MANTIS option altogether.
                    radManOrLot.Properties.Items.Remove(radManOrLot.Properties.Items.GetItemByValue("MANTIS"));
                radManOrLot.SelectedIndex = 0;
            }
            catch {Log.Debug("Failed to remove LOTIS item from radio button group. No big deal.");}
        }

        public void UpdateDisplayUnits()
        {
            //Blank on purpose.
        }
        
    }
}