﻿using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    sealed partial class CSVExport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CSVExport));
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnGo = new DevExpress.XtraEditors.SimpleButton();
            this.lblDescription = new DevExpress.XtraEditors.LabelControl();
            this.radManOrLot = new DevExpress.XtraEditors.RadioGroup();
            this.tubeSelecter = new InspectionBrowserTreeList();
            this.folderSelecter = new DevExpress.XtraEditors.ButtonEdit();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTubeSelecter = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciManOrLot = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFolderSelection = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGoButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radManOrLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.folderSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciManOrLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFolderSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGoButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 338);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "Tool Notes";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.BackColor = System.Drawing.Color.LemonChiffon;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(9, 351);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(690, 78);
            this.label8.TabIndex = 42;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(302, 28);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(100, 20);
            this.imageComboBoxEdit1.TabIndex = 46;
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnGo);
            this.lcMain.Controls.Add(this.lblDescription);
            this.lcMain.Controls.Add(this.radManOrLot);
            this.lcMain.Controls.Add(this.tubeSelecter);
            this.lcMain.Controls.Add(this.folderSelecter);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2674, 232, 250, 350);
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(702, 453);
            this.lcMain.TabIndex = 44;
            this.lcMain.Text = "layoutControl1";
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(165, 61);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(49, 22);
            this.btnGo.StyleController = this.lcMain;
            this.btnGo.TabIndex = 8;
            this.btnGo.Text = "Go";
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblDescription.Location = new System.Drawing.Point(165, 31);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(535, 26);
            this.lblDescription.StyleController = this.lcMain;
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = "Each tube\'s file will be placed into its own inspection folder under whatever dir" +
    "ectory is selected as the output. If a file already exists, it will be overwritt" +
    "en.";
            // 
            // radManOrLot
            // 
            this.radManOrLot.EditValue = "MANTIS";
            this.radManOrLot.Location = new System.Drawing.Point(540, 2);
            this.radManOrLot.Name = "radManOrLot";
            this.radManOrLot.Properties.Columns = 2;
            this.radManOrLot.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("MANTIS", "MANTIS"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("LOTIS", "LOTIS")});
            this.radManOrLot.Size = new System.Drawing.Size(160, 25);
            this.radManOrLot.StyleController = this.lcMain;
            this.radManOrLot.TabIndex = 5;
            // 
            // tubeSelecter
            // 
            this.tubeSelecter.Location = new System.Drawing.Point(2, 2);
            this.tubeSelecter.Name = "tubeSelecter";
            this.tubeSelecter.Size = new System.Drawing.Size(159, 449);
            this.tubeSelecter.TabIndex = 4;
            // 
            // folderSelecter
            // 
            this.folderSelecter.Location = new System.Drawing.Point(239, 2);
            this.folderSelecter.Name = "folderSelecter";
            this.folderSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.folderSelecter.Size = new System.Drawing.Size(297, 20);
            this.folderSelecter.StyleController = this.lcMain;
            this.folderSelecter.TabIndex = 6;
            this.folderSelecter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.folderSelecter_ButtonClick);
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "layoutControlGroup1";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTubeSelecter,
            this.emptySpaceItem1,
            this.lciManOrLot,
            this.lciFolderSelection,
            this.layoutControlItem1,
            this.lciGoButton,
            this.emptySpaceItem3});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "Root";
            this.lcgMain.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMain.Size = new System.Drawing.Size(702, 453);
            this.lcgMain.Text = "Root";
            this.lcgMain.TextVisible = false;
            // 
            // lciTubeSelecter
            // 
            this.lciTubeSelecter.Control = this.tubeSelecter;
            this.lciTubeSelecter.CustomizationFormText = "lciTubeSelecter";
            this.lciTubeSelecter.Location = new System.Drawing.Point(0, 0);
            this.lciTubeSelecter.MaxSize = new System.Drawing.Size(163, 0);
            this.lciTubeSelecter.MinSize = new System.Drawing.Size(163, 24);
            this.lciTubeSelecter.Name = "lciTubeSelecter";
            this.lciTubeSelecter.Size = new System.Drawing.Size(163, 453);
            this.lciTubeSelecter.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciTubeSelecter.Text = "lciTubeSelecter";
            this.lciTubeSelecter.TextSize = new System.Drawing.Size(0, 0);
            this.lciTubeSelecter.TextToControlDistance = 0;
            this.lciTubeSelecter.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(163, 85);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(539, 368);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciManOrLot
            // 
            this.lciManOrLot.Control = this.radManOrLot;
            this.lciManOrLot.CustomizationFormText = "lciManOrLot";
            this.lciManOrLot.Location = new System.Drawing.Point(538, 0);
            this.lciManOrLot.MaxSize = new System.Drawing.Size(164, 29);
            this.lciManOrLot.MinSize = new System.Drawing.Size(164, 29);
            this.lciManOrLot.Name = "lciManOrLot";
            this.lciManOrLot.Size = new System.Drawing.Size(164, 29);
            this.lciManOrLot.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciManOrLot.Text = "lciManOrLot";
            this.lciManOrLot.TextSize = new System.Drawing.Size(0, 0);
            this.lciManOrLot.TextToControlDistance = 0;
            this.lciManOrLot.TextVisible = false;
            // 
            // lciFolderSelection
            // 
            this.lciFolderSelection.Control = this.folderSelecter;
            this.lciFolderSelection.CustomizationFormText = "Output Folder:";
            this.lciFolderSelection.Location = new System.Drawing.Point(163, 0);
            this.lciFolderSelection.Name = "lciFolderSelection";
            this.lciFolderSelection.Size = new System.Drawing.Size(375, 29);
            this.lciFolderSelection.Text = "Output Folder:";
            this.lciFolderSelection.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblDescription;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(163, 29);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(539, 30);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciGoButton
            // 
            this.lciGoButton.Control = this.btnGo;
            this.lciGoButton.CustomizationFormText = "lciGoButton";
            this.lciGoButton.Location = new System.Drawing.Point(163, 59);
            this.lciGoButton.MaxSize = new System.Drawing.Size(53, 26);
            this.lciGoButton.MinSize = new System.Drawing.Size(53, 26);
            this.lciGoButton.Name = "lciGoButton";
            this.lciGoButton.Size = new System.Drawing.Size(53, 26);
            this.lciGoButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciGoButton.Text = "lciGoButton";
            this.lciGoButton.TextSize = new System.Drawing.Size(0, 0);
            this.lciGoButton.TextToControlDistance = 0;
            this.lciGoButton.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(216, 59);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(486, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ReformerCSVExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Name = "ReformerCSVExport";
            this.Size = new System.Drawing.Size(702, 453);
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radManOrLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.folderSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciManOrLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFolderSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGoButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraEditors.RadioGroup radManOrLot;
        private InspectionBrowserTreeList tubeSelecter;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSelecter;
        private DevExpress.XtraLayout.LayoutControlItem lciManOrLot;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ButtonEdit folderSelecter;
        private DevExpress.XtraLayout.LayoutControlItem lciFolderSelection;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LabelControl lblDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnGo;
        private DevExpress.XtraLayout.LayoutControlItem lciGoButton;
      
    }
}
