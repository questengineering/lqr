﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.EddyCurrentExamination
{
    sealed partial class LOTISEddyCurrentXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LOTISEddyCurrentXyPlot));
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciTubeSelecter = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiHints = new DevExpress.XtraEditors.LabelControl();
            this.lciHints = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiPolarPlot = new Nevron.Chart.WinForm.NChartControl();
            this.lciPolarPlot = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblMouseHints = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblMouseHints2 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnFlipAxialData = new DevExpress.XtraEditors.SimpleButton();
            this.lciFlipAxialData = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiRangeXy = new DevExpress.XtraEditors.SpinEdit();
            this.lciXYRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiRangeR = new DevExpress.XtraEditors.SpinEdit();
            this.lciRRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnRecalcViewExtents = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPolarPlot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFlipAxialData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRangeXy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXYRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRangeR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(803, 31);
            this.DataCursorView.Size = new System.Drawing.Size(190, 106);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnRecalcViewExtents);
            this.lcMain.Controls.Add(this.uiRangeR);
            this.lcMain.Controls.Add(this.uiRangeXy);
            this.lcMain.Controls.Add(this.btnFlipAxialData);
            this.lcMain.Controls.Add(this.lblMouseHints2);
            this.lcMain.Controls.Add(this.lblMouseHints);
            this.lcMain.Controls.Add(this.uiPolarPlot);
            this.lcMain.Controls.Add(this.uiHints);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.OptionsPrint.AppearanceGroupCaption.BackColor = System.Drawing.Color.LightGray;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseBackColor = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseFont = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseTextOptions = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lcMain.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcMain.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lcMain.OptionsPrint.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lcMain.Size = new System.Drawing.Size(1000, 505);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.uiHints, 0);
            this.lcMain.Controls.SetChildIndex(this.uiPolarPlot, 0);
            this.lcMain.Controls.SetChildIndex(this.lblMouseHints, 0);
            this.lcMain.Controls.SetChildIndex(this.lblMouseHints2, 0);
            this.lcMain.Controls.SetChildIndex(this.btnFlipAxialData, 0);
            this.lcMain.Controls.SetChildIndex(this.uiRangeXy, 0);
            this.lcMain.Controls.SetChildIndex(this.uiRangeR, 0);
            this.lcMain.Controls.SetChildIndex(this.btnRecalcViewExtents, 0);
            // 
            // PlotPanel
            // 
            this.PlotPanel.Size = new System.Drawing.Size(787, 491);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciXYRange,
            this.lciTubeSelecter,
            this.lciRRange,
            this.lciFlipAxialData,
            this.lciPolarPlot,
            this.lciHints,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1});
            this.lcgMain.Size = new System.Drawing.Size(1000, 505);
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(796, 24);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 110);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(791, 0);
            this.splitterItem1.Size = new System.Drawing.Size(5, 495);
            this.splitterItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Size = new System.Drawing.Size(791, 495);
            this.lciPlotPanel.Text = "XY Near";
            this.lciPlotPanel.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciPlotPanel.TextToControlDistance = 0;
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(832, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DataSource = this.BSTube;
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.gridLookUpEdit1View;
            this.TubeSelecter.Size = new System.Drawing.Size(161, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 5;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.ItemChangedSoRedraw);
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // lciTubeSelecter
            // 
            this.lciTubeSelecter.Control = this.TubeSelecter;
            this.lciTubeSelecter.CustomizationFormText = "Tube";
            this.lciTubeSelecter.Location = new System.Drawing.Point(796, 0);
            this.lciTubeSelecter.Name = "lciTubeSelecter";
            this.lciTubeSelecter.Size = new System.Drawing.Size(194, 24);
            this.lciTubeSelecter.Text = "Tube";
            this.lciTubeSelecter.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciTubeSelecter.TextSize = new System.Drawing.Size(24, 13);
            this.lciTubeSelecter.TextToControlDistance = 5;
            // 
            // uiHints
            // 
            this.uiHints.Location = new System.Drawing.Point(803, 451);
            this.uiHints.Name = "uiHints";
            this.uiHints.Size = new System.Drawing.Size(185, 13);
            this.uiHints.StyleController = this.lcMain;
            this.uiHints.TabIndex = 15;
            this.uiHints.Text = "Q / E move to next and previous tube.";
            // 
            // lciHints
            // 
            this.lciHints.Control = this.uiHints;
            this.lciHints.CustomizationFormText = "lciHints";
            this.lciHints.Location = new System.Drawing.Point(796, 444);
            this.lciHints.Name = "lciHints";
            this.lciHints.Size = new System.Drawing.Size(194, 17);
            this.lciHints.Text = "lciHints";
            this.lciHints.TextSize = new System.Drawing.Size(0, 0);
            this.lciHints.TextVisible = false;
            // 
            // uiPolarPlot
            // 
            this.uiPolarPlot.AutoRefresh = false;
            this.uiPolarPlot.BackColor = System.Drawing.SystemColors.Control;
            this.uiPolarPlot.InputKeys = new System.Windows.Forms.Keys[0];
            this.uiPolarPlot.Location = new System.Drawing.Point(803, 257);
            this.uiPolarPlot.Name = "uiPolarPlot";
            this.uiPolarPlot.Size = new System.Drawing.Size(190, 190);
            this.uiPolarPlot.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("uiPolarPlot.State")));
            this.uiPolarPlot.TabIndex = 17;
            this.uiPolarPlot.Text = "nChartControl1";
            // 
            // lciPolarPlot
            // 
            this.lciPolarPlot.Control = this.uiPolarPlot;
            this.lciPolarPlot.CustomizationFormText = "Impedence Plane Plot";
            this.lciPolarPlot.Location = new System.Drawing.Point(796, 234);
            this.lciPolarPlot.MaxSize = new System.Drawing.Size(194, 210);
            this.lciPolarPlot.MinSize = new System.Drawing.Size(194, 210);
            this.lciPolarPlot.Name = "lciPolarPlot";
            this.lciPolarPlot.Size = new System.Drawing.Size(194, 210);
            this.lciPolarPlot.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPolarPlot.Text = "Impedence Plane Plot";
            this.lciPolarPlot.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciPolarPlot.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // lblMouseHints
            // 
            this.lblMouseHints.Location = new System.Drawing.Point(803, 468);
            this.lblMouseHints.Name = "lblMouseHints";
            this.lblMouseHints.Size = new System.Drawing.Size(174, 13);
            this.lblMouseHints.StyleController = this.lcMain;
            this.lblMouseHints.TabIndex = 18;
            this.lblMouseHints.Text = "Ctrl/Alt-Wheel horizontal zoom/scroll";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblMouseHints;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(796, 461);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // lblMouseHints2
            // 
            this.lblMouseHints2.Location = new System.Drawing.Point(803, 485);
            this.lblMouseHints2.Name = "lblMouseHints2";
            this.lblMouseHints2.Size = new System.Drawing.Size(179, 13);
            this.lblMouseHints2.StyleController = this.lcMain;
            this.lblMouseHints2.TabIndex = 19;
            this.lblMouseHints2.Text = "Shift/None-Wheel vertical zoom/scroll";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblMouseHints2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(796, 478);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // btnFlipAxialData
            // 
            this.btnFlipAxialData.Location = new System.Drawing.Point(803, 215);
            this.btnFlipAxialData.Name = "btnFlipAxialData";
            this.btnFlipAxialData.Size = new System.Drawing.Size(62, 22);
            this.btnFlipAxialData.StyleController = this.lcMain;
            this.btnFlipAxialData.TabIndex = 21;
            this.btnFlipAxialData.Text = "Flip Axial";
            this.btnFlipAxialData.ToolTip = "Flips axial data for all tubes that contain Eddy Current data. Does not affect OD" +
    "/ID Axial data.";
            this.btnFlipAxialData.Click += new System.EventHandler(this.btnFlipAxialData_Click);
            // 
            // lciFlipAxialData
            // 
            this.lciFlipAxialData.Control = this.btnFlipAxialData;
            this.lciFlipAxialData.CustomizationFormText = "lciFlipAxialData";
            this.lciFlipAxialData.Location = new System.Drawing.Point(796, 208);
            this.lciFlipAxialData.MaxSize = new System.Drawing.Size(66, 26);
            this.lciFlipAxialData.MinSize = new System.Drawing.Size(66, 26);
            this.lciFlipAxialData.Name = "lciFlipAxialData";
            this.lciFlipAxialData.Size = new System.Drawing.Size(194, 26);
            this.lciFlipAxialData.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciFlipAxialData.Text = "lciFlipAxialData";
            this.lciFlipAxialData.TextSize = new System.Drawing.Size(0, 0);
            this.lciFlipAxialData.TextVisible = false;
            // 
            // uiRangeXy
            // 
            this.uiRangeXy.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiRangeXy.Location = new System.Drawing.Point(858, 141);
            this.uiRangeXy.Name = "uiRangeXy";
            this.uiRangeXy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiRangeXy.Size = new System.Drawing.Size(135, 20);
            this.uiRangeXy.StyleController = this.lcMain;
            this.uiRangeXy.TabIndex = 22;
            this.uiRangeXy.EditValueChanged += new System.EventHandler(this.uiRangeXy_EditValueChanged);
            // 
            // lciXYRange
            // 
            this.lciXYRange.Control = this.uiRangeXy;
            this.lciXYRange.CustomizationFormText = "XY Range:";
            this.lciXYRange.Location = new System.Drawing.Point(796, 134);
            this.lciXYRange.Name = "lciXYRange";
            this.lciXYRange.Size = new System.Drawing.Size(194, 24);
            this.lciXYRange.Text = "XY Range:";
            this.lciXYRange.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciXYRange.TextSize = new System.Drawing.Size(50, 13);
            this.lciXYRange.TextToControlDistance = 5;
            // 
            // uiRangeR
            // 
            this.uiRangeR.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiRangeR.Location = new System.Drawing.Point(853, 165);
            this.uiRangeR.Name = "uiRangeR";
            this.uiRangeR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiRangeR.Size = new System.Drawing.Size(140, 20);
            this.uiRangeR.StyleController = this.lcMain;
            this.uiRangeR.TabIndex = 23;
            this.uiRangeR.EditValueChanged += new System.EventHandler(this.uiRangeXy_EditValueChanged);
            // 
            // lciRRange
            // 
            this.lciRRange.Control = this.uiRangeR;
            this.lciRRange.CustomizationFormText = "R Range:";
            this.lciRRange.Location = new System.Drawing.Point(796, 158);
            this.lciRRange.Name = "lciRRange";
            this.lciRRange.Size = new System.Drawing.Size(194, 24);
            this.lciRRange.Text = "R Range:";
            this.lciRRange.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciRRange.TextSize = new System.Drawing.Size(45, 13);
            this.lciRRange.TextToControlDistance = 5;
            // 
            // btnRecalcViewExtents
            // 
            this.btnRecalcViewExtents.Location = new System.Drawing.Point(803, 189);
            this.btnRecalcViewExtents.Name = "btnRecalcViewExtents";
            this.btnRecalcViewExtents.Size = new System.Drawing.Size(81, 22);
            this.btnRecalcViewExtents.StyleController = this.lcMain;
            this.btnRecalcViewExtents.TabIndex = 24;
            this.btnRecalcViewExtents.Text = "Recalc Ranges";
            this.btnRecalcViewExtents.Click += new System.EventHandler(this.btnRecalcViewExtents_Click);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnRecalcViewExtents;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(796, 182);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(85, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(881, 182);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(109, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LOTISEddyCurrentXyPlot
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Name = "LOTISEddyCurrentXyPlot";
            this.Size = new System.Drawing.Size(1000, 505);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPolarPlot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFlipAxialData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRangeXy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciXYRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRangeR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSelecter;
        private DevExpress.XtraEditors.LabelControl uiHints;
        private DevExpress.XtraLayout.LayoutControlItem lciHints;
        private Nevron.Chart.WinForm.NChartControl uiPolarPlot;
        private DevExpress.XtraLayout.LayoutControlItem lciPolarPlot;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl lblMouseHints2;
        private DevExpress.XtraEditors.LabelControl lblMouseHints;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton btnFlipAxialData;
        private DevExpress.XtraLayout.LayoutControlItem lciFlipAxialData;
        private DevExpress.XtraEditors.SpinEdit uiRangeR;
        private DevExpress.XtraEditors.SpinEdit uiRangeXy;
        private DevExpress.XtraLayout.LayoutControlItem lciXYRange;
        private DevExpress.XtraLayout.LayoutControlItem lciRRange;
        private DevExpress.XtraEditors.SimpleButton btnRecalcViewExtents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
