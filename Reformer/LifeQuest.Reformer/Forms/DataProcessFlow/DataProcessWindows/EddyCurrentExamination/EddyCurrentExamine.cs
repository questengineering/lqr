﻿using System.Drawing;
using System.Windows.Forms;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.InspectionFile;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.EddyCurrentExamination
{
    public partial class EddyCurrentExamine : BaseProcessWindow, IHasDisplayUnits
    {
        readonly MANTISEddyCurrentXyPlot MANTISEddyPlot;
        readonly LOTISEddyCurrentXyPlot LOTISEddyPlot;
        readonly ReformerInspection inspectionOnLoad;
        
        public EddyCurrentExamine()
        {
            InitializeComponent();
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject != null && LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile != null)
                inspectionOnLoad = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;

            if (inspectionOnLoad == null || inspectionOnLoad.InspectionTool == Tool.MANTIS)
            {
                MANTISEddyPlot = new MANTISEddyCurrentXyPlot(OnProgressChanged)
                {
                    Dock = DockStyle.Fill,
                    Location = new Point(0, 0),
                    Name = "EddyPlot",
                    Size = new Size(1043, 426),
                    TabIndex = 0
                };
                Controls.Add(MANTISEddyPlot);
            }
            else
            {
                LOTISEddyPlot = new LOTISEddyCurrentXyPlot(OnProgressChanged)
                {
                    Dock = DockStyle.Fill,
                    Location = new Point(0, 0),
                    Name = "EddyPlot",
                    Size = new Size(1043, 426),
                    TabIndex = 0
                };
                Controls.Add(LOTISEddyPlot);
            }
        }

        public override void UpdateDataSource()
        {
            if (inspectionOnLoad == null) return;
            if (inspectionOnLoad.InspectionTool == Tool.LOTIS)
                LOTISEddyPlot.SetInspection(CurrentInspection);
            else
                MANTISEddyPlot.SetInspection(CurrentInspection);
        }

        public void UpdateDisplayUnits()
        {
            if (inspectionOnLoad == null) return;
            if (inspectionOnLoad.InspectionTool == Tool.LOTIS)
                LOTISEddyPlot.UpdateDisplayUnits();
            else
                MANTISEddyPlot.UpdateDisplayUnits();
        }
    }
}
