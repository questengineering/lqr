﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nevron.Chart;
using Nevron.Chart.WinForm;
using Nevron.Collections;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.EddyCurrentExamination
{
    public sealed partial class MANTISEddyCurrentXyPlot : DataXyPlot, IHasDisplayUnits
    {
        private ReformerTube CurrentTube { get { return TubeSelecter.EditValue as ReformerTube; } }
        
        private readonly Action<int, string> _progressUpdateMethod;
        private NChart _polarChart;
        private IEnumerable<NChart> AllCharts { get { return new List<NChart> {_xyNearChart, _rNearChart, _aNearChart, _xyFarChart, _rFarChart, _aFarChart}; } }

        private readonly NChart _xyNearChart = new NCartesianChart { Name = "XY Near" };
        private readonly NChart _rNearChart = new NCartesianChart { Name = "R Near" };
        private readonly NChart _aNearChart = new NCartesianChart { Name = "A Near" };
        private readonly NChart _xyFarChart = new NCartesianChart { Name = "XY Far" };
        private readonly NChart _rFarChart = new NCartesianChart { Name = "R Far" };
        private readonly NChart _aFarChart = new NCartesianChart { Name = "A Far" };
        private NChart _activeChart;
        /// <summary> Stores what the calced range for XY Near should be in the polar plot so it can be set on datacursor movement </summary>
        private NRangeAxisView _polarNearRange = new NRangeAxisView(new NRange1DD(-2, 2));
        /// <summary> Stores what the calced range for XY Far should be in the polar plot so it can be set on datacursor movement. </summary>
        private NRangeAxisView _polarFarRange = new NRangeAxisView(new NRange1DD(-2, 2));

        #region Constructor

        public MANTISEddyCurrentXyPlot()
        {
            InitializeComponent();
            InitializeChart();
            InitializeDataCursor();
            InitializePolarPlot();
            DoubleBuffered = true;
        }

        private void InitializePolarPlot()
        {
            uiPolarPlot.Legends.Clear();
            _polarChart = uiPolarPlot.Charts[0];
            _polarChart.Dock = DockStyle.Fill;
            _polarChart.BoundsMode = BoundsMode.Stretch;
            _polarChart.Axis(StandardAxis.PrimaryX).View = new NRangeAxisView(new NRange1DD(-2, 2));
            _polarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-2, 2));
            uiPolarPlot.AutoRefresh = true;
            //changed scale anchor type to cross instead of docking. Also removed major grid and labels on axes in designer (in case it breaks during a nevron update).
        }

        public MANTISEddyCurrentXyPlot(Action<int, string> progressUpdateMethod) : this()
        {
            _progressUpdateMethod = progressUpdateMethod;
        }

        #endregion Constructor

        protected override void InitializeDataCursor()
        {
            NDataCursorTool dataCursorTool = new NDataCursorTool();
            TheChartControl.Controller.Tools.Clear();
            TheChartControl.Controller.Tools.Add(dataCursorTool);
            
            //Create a data cursor that will get passed around to the selected chart.
            AxialCursor.BeginEndAxis = (int) StandardAxis.PrimaryY;
            AxialCursor.SynchronizeOnMouseAction = MouseAction.Down;
            AxialCursor.StrokeStyle.Pattern = LinePattern.DashDotDot; 
            AxialCursor.StrokeStyle.Factor = 2;
            AxialCursor.ValueChanged += CursorValueChanged; //Only allow one chart to shoot changed events since they're all synced.
        }

        protected override void InitializeChart()
        {
            base.InitializeChart();
            PlotPanel.Controls.Add(TheChartControl);
            TheChartControl.Legends.Clear();
            TheChartControl.Charts.Clear();
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            //Set up 6 equally (vertically) spaced charts.
            for (int chartNum = 0; chartNum < AllCharts.Count(); chartNum++)
            {
                NChart thisChart = AllCharts.ToArray()[chartNum];
                TheChartControl.Charts.Add(thisChart);
                thisChart.BoundsMode = BoundsMode.Stretch;
                thisChart.Location = new NPointL(new NLength(0, NRelativeUnit.ParentPercentage), new NLength(16.6666f * chartNum, NRelativeUnit.ParentPercentage));
                thisChart.Size = new NSizeL(new NLength(99.5f, NRelativeUnit.ParentPercentage), new NLength(16.6666f, NRelativeUnit.ParentPercentage));
                
                
                thisChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator = new NLinearScaleConfigurator();
                var xScale = (NLinearScaleConfigurator)thisChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator;
                xScale.LabelStyle.ContentAlignment = ContentAlignment.MiddleCenter; 
                xScale.ViewRangeInflateMode = ScaleViewRangeInflateMode.Absolute;

                var yScale = ((NLinearScaleConfigurator)thisChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator);
                yScale.LabelStyle.KeepInsideRuler = true;
                yScale.Title.Text = thisChart.Name;
                yScale.Title.Angle = new NScaleLabelAngle(90);
                yScale.LabelStyle.Angle = new NScaleLabelAngle(90);
                yScale.ViewRangeInflateMode = ScaleViewRangeInflateMode.Absolute;
                if (chartNum != AllCharts.Count() -1)
                {
                    xScale.AutoLabels = false;
                }
            }
            
        }

        protected override void TheChartControl_MouseDown(object sender, MouseEventArgs e)
        {
            TheChartControl.Focus(); //this makes it so the key down events get picked up to move next/previous
            //Remove the cursor from the previously active chart if needed. SOmething hits weird up/down conditions during debugging. DOn't know if it'd happen in normal use.
            if (_activeChart != null && _activeChart.Axis(StandardAxis.PrimaryX).Cursors.Contains(AxialCursor))
                _activeChart.Axis(StandardAxis.PrimaryX).Cursors.Remove(AxialCursor);

            _activeChart = TheChartControl.HitTest(e.Location).Chart;
            if (_activeChart == null) return;
            _activeChart.Axis(StandardAxis.PrimaryX).Cursors.Add(AxialCursor);
            AxialCursor.SynchronizeOnMouseAction |= MouseAction.Move;
            if (!TheChartControl.Controller.Selection.SelectedObjects.Contains(_activeChart))
                TheChartControl.Controller.Selection.SelectedObjects.Add(_activeChart);
        }

        protected override void TheChartControl_MouseUp(object sender, MouseEventArgs e)
        {
            NChartControl selectedChartControl = (NChartControl)sender;
            if (selectedChartControl == null) return;
            if (_activeChart != null && _activeChart.Axis(StandardAxis.PrimaryX).Cursors.Contains(AxialCursor))
                _activeChart.Axis(StandardAxis.PrimaryX).Cursors.Remove(AxialCursor);
            if (_activeChart != null && TheChartControl.Controller.Selection.SelectedObjects.Contains(_activeChart))
                TheChartControl.Controller.Selection.SelectedObjects.Add(_activeChart);
            AxialCursor.SynchronizeOnMouseAction = MouseAction.Down;
        }

        internal void SetInspection(ReformerInspection Inspection)
        {
            if (Inspection == null) return;
            BSTube.DataSource = Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.EddyData));
            MoveFirst();
        }

        private void ItemChangedSoRedraw(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            DrawPlot();
        }

        private void DrawPlot()
        {
            Stopwatch sw = Stopwatch.StartNew();
            DrawTube();
            
            OnProgressChanged(100, string.Format("Drew {0} in {1:N1}s", CurrentTube.Name, sw.Elapsed.TotalSeconds));
            Log.Info(string.Format("Drew Tube Data for {0} in {1} ms", CurrentTube.Name, sw.ElapsedMilliseconds));
        }

        private void DrawTube()
        {
            ClearChart();
            if (CurrentTube == null || !CurrentTube.AvailableScalars.Contains(ReformerScalar.EddyData)) return;

            //Gather the lines and set their visible names.
            List<NLineSeries> lines = CurrentTube.GetAllEddyLines();

            lines[0].Name = "X Near";
            lines[1].Name = "Y Near";
            lines[2].Name = "X Far";
            lines[3].Name = "Y Far";
            lines[4].Name = "R Near";
            lines[5].Name = "A Near";
            lines[6].Name = "R Far";
            lines[7].Name = "A Far";

            //Check if the overall inspection mins and maxes have been calculated before
            if (CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude.IsNanOrZero())
            {
                GetDefaultViewExtents();
            }

            var xy = CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude;
            var r = CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude;
            uiRangeXy.Value = Math.Round(CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude, 1).ToDecimal();
            uiRangeR.Value = Math.Round(CurrentTube.Inspection.ViewExtents.EddyCurrentRMagnitude, 1).ToDecimal();

            _xyNearChart.Series.Add(lines[0]);
            _xyNearChart.Series.Add(lines[1]);
            _xyNearChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-xy, xy));
            _xyNearChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();

            _rNearChart.Series.Add(lines[4]);
            _rNearChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-r, r));
            _rNearChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();

            _aNearChart.Series.Add(lines[5]);
            _aNearChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(0, 360));
            _aNearChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();
            _aNearChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator.RulerStyle.BorderStyle.Width = new NLength(5);
            ((NLinearScaleConfigurator)_aNearChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).MajorTickMode = MajorTickMode.CustomTicks;
            ((NLinearScaleConfigurator)_aNearChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).CustomMajorTicks = new NDoubleList(new[] { 90, 180d, 270 });
            ((NLinearScaleConfigurator)_aNearChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).LabelFitModes = new[] { LabelFitMode.AutoScale };
            ((NLinearScaleConfigurator)_aNearChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).NumberOfTicksPerLabel = 2;

            _xyFarChart.Series.Add(lines[2]);
            _xyFarChart.Series.Add(lines[3]);
            _xyFarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-xy, xy));
            _xyFarChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();

            _rFarChart.Series.Add(lines[6]);
            _rFarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-r, r));
            _rFarChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();

            _aFarChart.Series.Add(lines[7]);
            _aFarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(0, 360));
            _aFarChart.Axis(StandardAxis.PrimaryX).View = new NContentAxisView();
            ((NLinearScaleConfigurator)_aFarChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).MajorTickMode = MajorTickMode.CustomTicks;
            ((NLinearScaleConfigurator)_aFarChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).CustomMajorTicks = new NDoubleList(new[] { 90, 180d, 270 });
            ((NLinearScaleConfigurator)_aFarChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).LabelFitModes = new[] { LabelFitMode.AutoScale };
            ((NLinearScaleConfigurator)_aFarChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).NumberOfTicksPerLabel = 2;

            _polarNearRange = new NRangeAxisView(new NRange1DD(-xy, xy));
            _polarFarRange = new NRangeAxisView(new NRange1DD(-xy, xy));
        }

        private void GetDefaultViewExtents()
        {
            if (CurrentTube == null) return;
            var defaultRanges = ReformerViewExtents.GetEddyCurrentRanges(CurrentTube.Inspection);
            double absXyMaxFromOrigin = Math.Max(Math.Abs(defaultRanges[0].Min), Math.Abs(defaultRanges[0].Max));
            double absRMaxFromOrigin = Math.Max(Math.Abs(defaultRanges[1].Min), Math.Abs(defaultRanges[1].Max));

            CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude = absXyMaxFromOrigin;
            CurrentTube.Inspection.ViewExtents.EddyCurrentRMagnitude = absRMaxFromOrigin;
            uiRangeXy.EditValue = Math.Round(absXyMaxFromOrigin,1);
            uiRangeR.EditValue = Math.Round(absRMaxFromOrigin, 1);

        }


        protected override void CursorValueChanged(object sender, EventArgs e)
        {
            NAxisCursor sourceCursor = (NAxisCursor)sender;
            double xValue = sourceCursor.Value;
            
            ListViewGroup lineGroup = DataCursorView.Groups["Tubes"];
            for (int idx = lineGroup.Items.Count - 1; idx >= 0; idx--) { DataCursorView.Items.Remove(lineGroup.Items[idx]); }

            double closestValue;
            int slice = FindClosestDataValueInData(((NLineSeries)_rNearChart.Series[0]).XValues, xValue, true, out closestValue);

            List<PickedPoint> dataCursorPoints = new List<PickedPoint>();
            if (slice != -1)
            {
                dataCursorPoints.AddRange(from c in AllCharts from NLineSeries series in c.Series select new PickedPoint
                    {
                        BaseXValue = series.XValues[slice].ToSingle(), BaseYValue = series.Values[slice].ToSingle(), Name = series.Name,
                        XValue = series.XValues[slice].ToSingle(), YValue = series.Values[slice].ToSingle()
                    });
            }

            foreach (PickedPoint p in dataCursorPoints)
            {
                ListViewItem li = new ListViewItem(lineGroup) { Text = p.Name };
                li.SubItems.Add(p.XValue.ToString("N3"));
                li.SubItems.Add(p.YValue.ToString("N3"));
                DataCursorView.Items.Add(li);
            }
            DataCursorView.Refresh();

            _polarChart.Series.Clear();
            
            bool nearXy = AllCharts.Find(C => C.Axis(StandardAxis.PrimaryX).Cursors.Contains(sourceCursor)).Name.ToLower().Contains("near");
            NSmoothLineSeries polarSeries;
            if (_polarChart.Series.Count == 0)
            {
                polarSeries = new NSmoothLineSeries {UseXValues = true};
                _polarChart.Series.Add(polarSeries);
            }
            else
                polarSeries = (NSmoothLineSeries)_polarChart.Series[0];
            polarSeries.ClearDataPoints();
            double x = nearXy ? dataCursorPoints.Find(P => P.Name == "X Near").YValue : dataCursorPoints.Find(P => P.Name == "X Far").YValue;
            double y = nearXy ? dataCursorPoints.Find(P => P.Name == "Y Near").YValue : dataCursorPoints.Find(P => P.Name == "Y Far").YValue;
            polarSeries.AddDataPoint(new NDataPoint(0,0));
            polarSeries.AddDataPoint(new NDataPoint(x * .5, y * .75 ));
            polarSeries.AddDataPoint(new NDataPoint(x, y));
            polarSeries.AddDataPoint(new NDataPoint(x * .5, y * .25));
            polarSeries.AddDataPoint(new NDataPoint(0,0));
            polarSeries.DataLabelStyle.Visible = false;
            polarSeries.Use1DInterpolationForXYScatter = false;
            _polarChart.Axis(StandardAxis.PrimaryY).View = _polarChart.Axis(StandardAxis.PrimaryX).View = nearXy ? _polarNearRange : _polarFarRange;
            _polarChart.Refresh();
        }

        public override void ClearChart()
        {
            foreach (var chart in AllCharts)
            {
                foreach (NSeries s in chart.Series) { s.ClearDataPoints(); }
                chart.Series.Clear();
            }
        }
        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            int nextKey = 0;
            if (TubeSelecter.EditValue != null) nextKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }

        private void TheChartControlOnKeyDown(object Sender, KeyEventArgs Args)
        {
            if (Args.KeyCode == Keys.Q) MovePrevious();
            else if (Args.KeyCode == Keys.E) MoveNext();
        }

        private void OnProgressChanged(int Progress = -1, string Status = "SkipProgress")
        {
            if (_progressUpdateMethod == null) return;
            _progressUpdateMethod(Progress, Status);
        }

        private async void btnFlipAxialData_Click(object sender, EventArgs e)
        {
            if (CurrentInspection == null) return;
            Enabled = false; Cursor = Cursors.WaitCursor;
            Stopwatch sw = Stopwatch.StartNew();
            OnProgressChanged(0, "Flipping axial data.");
            int tubeNum = 0;
            int numTubes = CurrentInspection.Tubes.Count;
            Task flipTask = new Task(() => CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.EddyData)).ForEach(T =>
            {
                T.FlipEddyAxialData();
                OnProgressChanged((int)Math.Round((double)tubeNum++ / numTubes * 95), "Flipped " + T.Name);
            }));
            flipTask.Start();
            await flipTask;
            OnProgressChanged(95, "Repacking data file.");
            CurrentInspection.RepackData();
            OnProgressChanged(99, string.Format("Finished flipping axial data in: {0:N1}s.", sw.Elapsed.TotalSeconds));
            Log.Info(string.Format("Flipped EC Data for {0} tubes in {1} ms", numTubes, sw.ElapsedMilliseconds));
            DrawPlot();
            Enabled = true; Cursor = Cursors.Default;
        }


        public void UpdateDisplayUnits()
        {
            DrawTube();
        }

        private void uiRangeXy_EditValueChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            double xy = uiRangeXy.Value.ToDouble();
            double r = uiRangeR.Value.ToDouble();
            CurrentTube.Inspection.ViewExtents.EddyCurrentRMagnitude = r;
            CurrentTube.Inspection.ViewExtents.EddyCurrentXyMagnitude = xy;
            _xyNearChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-xy, xy));
            _rNearChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-r, r));
            _xyFarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-xy, xy));
            _rFarChart.Axis(StandardAxis.PrimaryY).View = new NRangeAxisView(new NRange1DD(-r, r));
        }

        private void btnDefaultView_Click(object sender, EventArgs e)
        {
            GetDefaultViewExtents();
        }
    }

    
}
