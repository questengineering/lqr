﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class QiirImportExport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.uiExportInspection = new DevExpress.XtraEditors.GridLookUpEdit();
            this.bsCurrentProject = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiAddBlankInspection = new DevExpress.XtraEditors.ButtonEdit();
            this.uiInspections = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciInspections = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciAddBlankInspection = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciExportInspection = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiImportInspection = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiExportInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCurrentProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAddBlankInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspections.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAddBlankInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciExportInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.uiImportInspection);
            this.lcMain.Controls.Add(this.uiExportInspection);
            this.lcMain.Controls.Add(this.uiAddBlankInspection);
            this.lcMain.Controls.Add(this.uiInspections);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(757, 479);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "layoutControl1";
            // 
            // uiExportInspection
            // 
            this.uiExportInspection.Location = new System.Drawing.Point(139, 60);
            this.uiExportInspection.Name = "uiExportInspection";
            this.uiExportInspection.Properties.ActionButtonIndex = 1;
            this.uiExportInspection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Export", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Export the selected inspection to standalone file (.Qiir)", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiExportInspection.Properties.DataSource = this.bsCurrentProject;
            this.uiExportInspection.Properties.DisplayMember = "Name";
            this.uiExportInspection.Properties.NullText = "";
            this.uiExportInspection.Properties.View = this.gridView1;
            this.uiExportInspection.Size = new System.Drawing.Size(143, 20);
            this.uiExportInspection.StyleController = this.lcMain;
            this.uiExportInspection.TabIndex = 5;
            this.uiExportInspection.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.uiExportInspection_ButtonClick);
            // 
            // bsCurrentProject
            // 
            this.bsCurrentProject.DataMember = "InspectionFiles";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.FieldName = "Name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // uiAddBlankInspection
            // 
            this.uiAddBlankInspection.Location = new System.Drawing.Point(139, 12);
            this.uiAddBlankInspection.Name = "uiAddBlankInspection";
            this.uiAddBlankInspection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "Add", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Add an inspection with the name of the text written in this box (Must be unique a" +
                    "nd non-blank)", null, null, true)});
            this.uiAddBlankInspection.Size = new System.Drawing.Size(143, 20);
            this.uiAddBlankInspection.StyleController = this.lcMain;
            this.uiAddBlankInspection.TabIndex = 5;
            this.uiAddBlankInspection.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.uiAddBlankInspection_ButtonClick);
            // 
            // uiInspections
            // 
            this.uiInspections.Location = new System.Drawing.Point(139, 36);
            this.uiInspections.Name = "uiInspections";
            this.uiInspections.Properties.ActionButtonIndex = 1;
            this.uiInspections.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus, "Delete", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Delete the selected inspection from the project.", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiInspections.Properties.DataSource = this.bsCurrentProject;
            this.uiInspections.Properties.DisplayMember = "Name";
            this.uiInspections.Properties.NullText = "";
            this.uiInspections.Properties.View = this.gridLookUpEdit1View;
            this.uiInspections.Size = new System.Drawing.Size(143, 20);
            this.uiInspections.StyleController = this.lcMain;
            this.uiInspections.TabIndex = 4;
            this.uiInspections.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.uiInspections_ButtonClick);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "lcgMain";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciInspections,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.lciAddBlankInspection,
            this.lciExportInspection,
            this.layoutControlItem1});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.Size = new System.Drawing.Size(757, 479);
            this.lcgMain.Text = "lcgMain";
            this.lcgMain.TextVisible = false;
            // 
            // lciInspections
            // 
            this.lciInspections.Control = this.uiInspections;
            this.lciInspections.CustomizationFormText = "Inspections";
            this.lciInspections.Location = new System.Drawing.Point(0, 24);
            this.lciInspections.Name = "lciInspections";
            this.lciInspections.OptionsToolTip.ToolTip = "Inspections that exist in the current project.";
            this.lciInspections.Size = new System.Drawing.Size(274, 24);
            this.lciInspections.Text = "Delete Existing Inspection";
            this.lciInspections.TextSize = new System.Drawing.Size(124, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(274, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(463, 459);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(274, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(274, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(274, 361);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciAddBlankInspection
            // 
            this.lciAddBlankInspection.Control = this.uiAddBlankInspection;
            this.lciAddBlankInspection.CustomizationFormText = "layoutControlItem1";
            this.lciAddBlankInspection.Location = new System.Drawing.Point(0, 0);
            this.lciAddBlankInspection.Name = "lciAddBlankInspection";
            this.lciAddBlankInspection.Size = new System.Drawing.Size(274, 24);
            this.lciAddBlankInspection.Text = "Add Blank Inspection";
            this.lciAddBlankInspection.TextSize = new System.Drawing.Size(124, 13);
            // 
            // lciExportInspection
            // 
            this.lciExportInspection.Control = this.uiExportInspection;
            this.lciExportInspection.CustomizationFormText = "Export Inspection";
            this.lciExportInspection.Location = new System.Drawing.Point(0, 48);
            this.lciExportInspection.Name = "lciExportInspection";
            this.lciExportInspection.OptionsToolTip.ToolTip = "Export the selected inspection to a standalone file so it can be imported into ot" +
    "her projects (.qiir)";
            this.lciExportInspection.Size = new System.Drawing.Size(274, 24);
            this.lciExportInspection.Text = "Export Inspection";
            this.lciExportInspection.TextSize = new System.Drawing.Size(124, 13);
            // 
            // uiImportInspection
            // 
            this.uiImportInspection.Location = new System.Drawing.Point(12, 84);
            this.uiImportInspection.Name = "uiImportInspection";
            this.uiImportInspection.Size = new System.Drawing.Size(270, 22);
            this.uiImportInspection.StyleController = this.lcMain;
            this.uiImportInspection.TabIndex = 7;
            this.uiImportInspection.Text = "Import Inspection (.qiir)";
            this.uiImportInspection.Click += new System.EventHandler(this.uiImportInspection_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.uiImportInspection;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(274, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // QiirImportExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "QiirImportExport";
            this.Size = new System.Drawing.Size(757, 479);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiExportInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCurrentProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAddBlankInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspections.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAddBlankInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciExportInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraEditors.GridLookUpEdit uiInspections;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem lciInspections;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource bsCurrentProject;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.ButtonEdit uiAddBlankInspection;
        private DevExpress.XtraLayout.LayoutControlItem lciAddBlankInspection;
        private DevExpress.XtraEditors.GridLookUpEdit uiExportInspection;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraLayout.LayoutControlItem lciExportInspection;
        private DevExpress.XtraEditors.SimpleButton uiImportInspection;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
