﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class InspectionInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.uiDefaultNextInspectionYears = new DevExpress.XtraEditors.SpinEdit();
            this.uiDefaultTubeAge = new DevExpress.XtraEditors.SpinEdit();
            this.uiDateInspected = new DevExpress.XtraEditors.DateEdit();
            this.BSInspFile = new System.Windows.Forms.BindingSource(this.components);
            this.uiNewTubes = new DevExpress.XtraEditors.CheckEdit();
            this.uiUseRowLetters = new DevExpress.XtraEditors.CheckEdit();
            this.BSInspSetup = new System.Windows.Forms.BindingSource(this.components);
            this.uiTubeMaterial = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uiNotes = new DevExpress.XtraEditors.MemoEdit();
            this.uiDefaultID = new DevExpress.XtraEditors.CalcEdit();
            this.uiDefaultOD = new DevExpress.XtraEditors.CalcEdit();
            this.uiInspectionTool = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uiRefineryName = new DevExpress.XtraEditors.TextEdit();
            this.uiReformerName = new DevExpress.XtraEditors.TextEdit();
            this.uiCustomerName = new DevExpress.XtraEditors.TextEdit();
            this.uiName = new DevExpress.XtraEditors.TextEdit();
            this.uiDefaultODTolMin = new DevExpress.XtraEditors.TextEdit();
            this.uiDefaultODTolMax = new DevExpress.XtraEditors.TextEdit();
            this.uiDefaultIDTolMin = new DevExpress.XtraEditors.TextEdit();
            this.uiDefaultIDTolMax = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCustomerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciReformerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRefineryName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciInspectionTool = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultOD = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultID = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTubeMaterial = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciUseRowLetters = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDateInspected = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultODTolMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultODTolMax = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultIDTolMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultIDTolMax = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultTubeAge = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultNextInsp = new DevExpress.XtraLayout.LayoutControlItem();
            this.BSProject = new System.Windows.Forms.BindingSource(this.components);
            this.lnkSetID = new System.Windows.Forms.LinkLabel();
            this.lnkSetOD = new System.Windows.Forms.LinkLabel();
            this.defaultToolTipController1 = new DevExpress.Utils.DefaultToolTipController(this.components);
            this.lnkSetAge = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultNextInspectionYears.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultTubeAge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateInspected.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateInspected.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNewTubes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiUseRowLetters.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeMaterial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultOD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspectionTool.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRefineryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReformerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCustomerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultODTolMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultODTolMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultIDTolMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultIDTolMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReformerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefineryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspectionTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultOD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciUseRowLetters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateInspected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultODTolMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultODTolMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultIDTolMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultIDTolMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultTubeAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNextInsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSProject)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl
            // 
            this.LayoutControl.Controls.Add(this.uiDefaultNextInspectionYears);
            this.LayoutControl.Controls.Add(this.uiDefaultTubeAge);
            this.LayoutControl.Controls.Add(this.uiDateInspected);
            this.LayoutControl.Controls.Add(this.uiNewTubes);
            this.LayoutControl.Controls.Add(this.uiUseRowLetters);
            this.LayoutControl.Controls.Add(this.uiTubeMaterial);
            this.LayoutControl.Controls.Add(this.uiNotes);
            this.LayoutControl.Controls.Add(this.uiDefaultID);
            this.LayoutControl.Controls.Add(this.uiDefaultOD);
            this.LayoutControl.Controls.Add(this.uiInspectionTool);
            this.LayoutControl.Controls.Add(this.uiRefineryName);
            this.LayoutControl.Controls.Add(this.uiReformerName);
            this.LayoutControl.Controls.Add(this.uiCustomerName);
            this.LayoutControl.Controls.Add(this.uiName);
            this.LayoutControl.Controls.Add(this.uiDefaultODTolMin);
            this.LayoutControl.Controls.Add(this.uiDefaultODTolMax);
            this.LayoutControl.Controls.Add(this.uiDefaultIDTolMin);
            this.LayoutControl.Controls.Add(this.uiDefaultIDTolMax);
            this.LayoutControl.DataSource = this.BSInspFile;
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.LayoutControl.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(609, 139, 625, 476);
            this.LayoutControl.Root = this.layoutControlGroup1;
            this.LayoutControl.Size = new System.Drawing.Size(344, 564);
            this.LayoutControl.TabIndex = 0;
            this.LayoutControl.Text = "dataLayoutControl1";
            // 
            // uiDefaultNextInspectionYears
            // 
            this.uiDefaultNextInspectionYears.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uiDefaultNextInspectionYears.Location = new System.Drawing.Point(261, 252);
            this.uiDefaultNextInspectionYears.Name = "uiDefaultNextInspectionYears";
            this.uiDefaultNextInspectionYears.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDefaultNextInspectionYears.Properties.IsFloatValue = false;
            this.uiDefaultNextInspectionYears.Properties.Mask.EditMask = "N00";
            this.uiDefaultNextInspectionYears.Properties.MaxValue = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.uiDefaultNextInspectionYears.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uiDefaultNextInspectionYears.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultNextInspectionYears.StyleController = this.LayoutControl;
            this.uiDefaultNextInspectionYears.TabIndex = 18;
            // 
            // uiDefaultTubeAge
            // 
            this.uiDefaultTubeAge.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uiDefaultTubeAge.Location = new System.Drawing.Point(99, 252);
            this.uiDefaultTubeAge.Name = "uiDefaultTubeAge";
            this.uiDefaultTubeAge.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDefaultTubeAge.Properties.IsFloatValue = false;
            this.uiDefaultTubeAge.Properties.Mask.EditMask = "N00";
            this.uiDefaultTubeAge.Properties.MaxValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.uiDefaultTubeAge.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uiDefaultTubeAge.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultTubeAge.StyleController = this.LayoutControl;
            this.uiDefaultTubeAge.TabIndex = 17;
            // 
            // uiDateInspected
            // 
            this.uiDateInspected.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspFile, "DateInspected", true));
            this.uiDateInspected.EditValue = null;
            this.uiDateInspected.Location = new System.Drawing.Point(99, 36);
            this.uiDateInspected.Name = "uiDateInspected";
            this.uiDateInspected.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDateInspected.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDateInspected.Size = new System.Drawing.Size(233, 20);
            this.uiDateInspected.StyleController = this.LayoutControl;
            this.uiDateInspected.TabIndex = 16;
            // 
            // BSInspFile
            // 
            this.BSInspFile.DataSource = typeof(Reformer.Data.InspectionFile.ReformerInspection);
            // 
            // uiNewTubes
            // 
            this.uiNewTubes.Location = new System.Drawing.Point(12, 533);
            this.uiNewTubes.Name = "uiNewTubes";
            this.uiNewTubes.Properties.Caption = "New Tubes / Baseline";
            this.uiNewTubes.Size = new System.Drawing.Size(160, 19);
            this.uiNewTubes.StyleController = this.LayoutControl;
            this.uiNewTubes.TabIndex = 15;
            this.uiNewTubes.EditValueChanged += new System.EventHandler(this.ValidateImmediately);
            // 
            // uiUseRowLetters
            // 
            this.uiUseRowLetters.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "UseRowLetters", true));
            this.uiUseRowLetters.Location = new System.Drawing.Point(176, 533);
            this.uiUseRowLetters.Name = "uiUseRowLetters";
            this.uiUseRowLetters.Properties.Caption = "Use Row Letters";
            this.uiUseRowLetters.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.uiUseRowLetters.Size = new System.Drawing.Size(156, 19);
            this.uiUseRowLetters.StyleController = this.LayoutControl;
            this.uiUseRowLetters.TabIndex = 14;
            // 
            // BSInspSetup
            // 
            this.BSInspSetup.DataSource = typeof(Reformer.Data.InspectionFile.ReformerSetup);
            // 
            // uiTubeMaterial
            // 
            this.uiTubeMaterial.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "DefaultMaterial", true));
            this.uiTubeMaterial.Location = new System.Drawing.Point(99, 276);
            this.uiTubeMaterial.Name = "uiTubeMaterial";
            this.uiTubeMaterial.Properties.Appearance.Options.UseTextOptions = true;
            this.uiTubeMaterial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiTubeMaterial.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTubeMaterial.Size = new System.Drawing.Size(233, 20);
            this.uiTubeMaterial.StyleController = this.LayoutControl;
            this.uiTubeMaterial.TabIndex = 12;
            this.uiTubeMaterial.EditValueChanged += new System.EventHandler(this.uiTubeMaterial_EditValueChanged);
            // 
            // uiNotes
            // 
            this.uiNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "Notes", true));
            this.uiNotes.Location = new System.Drawing.Point(12, 316);
            this.uiNotes.Name = "uiNotes";
            this.uiNotes.Size = new System.Drawing.Size(320, 213);
            this.uiNotes.StyleController = this.LayoutControl;
            this.uiNotes.TabIndex = 11;
            // 
            // uiDefaultID
            // 
            this.uiDefaultID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "DefaultIDDisplayUnits", true));
            this.uiDefaultID.Location = new System.Drawing.Point(99, 204);
            this.uiDefaultID.Name = "uiDefaultID";
            this.uiDefaultID.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDefaultID.Properties.DisplayFormat.FormatString = "N3";
            this.uiDefaultID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDefaultID.Properties.EditFormat.FormatString = "N3";
            this.uiDefaultID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDefaultID.Size = new System.Drawing.Size(233, 20);
            this.uiDefaultID.StyleController = this.LayoutControl;
            this.uiDefaultID.TabIndex = 10;
            this.uiDefaultID.Validated += new System.EventHandler(this.DefaultEditValueChanged);
            // 
            // uiDefaultOD
            // 
            this.uiDefaultOD.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "DefaultODDisplayUnits", true));
            this.uiDefaultOD.Location = new System.Drawing.Point(99, 156);
            this.uiDefaultOD.Name = "uiDefaultOD";
            this.uiDefaultOD.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultOD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultOD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDefaultOD.Properties.DisplayFormat.FormatString = "N3";
            this.uiDefaultOD.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDefaultOD.Properties.EditFormat.FormatString = "N3";
            this.uiDefaultOD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDefaultOD.Size = new System.Drawing.Size(233, 20);
            this.uiDefaultOD.StyleController = this.LayoutControl;
            this.uiDefaultOD.TabIndex = 9;
            this.uiDefaultOD.Validated += new System.EventHandler(this.DefaultEditValueChanged);
            // 
            // uiInspectionTool
            // 
            this.uiInspectionTool.Location = new System.Drawing.Point(99, 132);
            this.uiInspectionTool.Name = "uiInspectionTool";
            this.uiInspectionTool.Properties.Appearance.Options.UseTextOptions = true;
            this.uiInspectionTool.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiInspectionTool.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiInspectionTool.Size = new System.Drawing.Size(233, 20);
            this.uiInspectionTool.StyleController = this.LayoutControl;
            this.uiInspectionTool.TabIndex = 8;
            this.uiInspectionTool.SelectedIndexChanged += new System.EventHandler(this.ValidateImmediately);
            // 
            // uiRefineryName
            // 
            this.uiRefineryName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "Refinery", true));
            this.uiRefineryName.Location = new System.Drawing.Point(99, 84);
            this.uiRefineryName.Name = "uiRefineryName";
            this.uiRefineryName.Properties.Appearance.Options.UseTextOptions = true;
            this.uiRefineryName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiRefineryName.Size = new System.Drawing.Size(233, 20);
            this.uiRefineryName.StyleController = this.LayoutControl;
            this.uiRefineryName.TabIndex = 7;
            // 
            // uiReformerName
            // 
            this.uiReformerName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "ReformerName", true));
            this.uiReformerName.Location = new System.Drawing.Point(99, 108);
            this.uiReformerName.Name = "uiReformerName";
            this.uiReformerName.Properties.Appearance.Options.UseTextOptions = true;
            this.uiReformerName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiReformerName.Size = new System.Drawing.Size(233, 20);
            this.uiReformerName.StyleController = this.LayoutControl;
            this.uiReformerName.TabIndex = 6;
            // 
            // uiCustomerName
            // 
            this.uiCustomerName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspSetup, "Customer", true));
            this.uiCustomerName.Location = new System.Drawing.Point(99, 60);
            this.uiCustomerName.Name = "uiCustomerName";
            this.uiCustomerName.Properties.Appearance.Options.UseTextOptions = true;
            this.uiCustomerName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiCustomerName.Size = new System.Drawing.Size(233, 20);
            this.uiCustomerName.StyleController = this.LayoutControl;
            this.uiCustomerName.TabIndex = 5;
            // 
            // uiName
            // 
            this.uiName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.BSInspFile, "Name", true));
            this.uiName.EditValue = "Inspection Name";
            this.uiName.Location = new System.Drawing.Point(99, 12);
            this.uiName.Name = "uiName";
            this.uiName.Properties.Appearance.Options.UseTextOptions = true;
            this.uiName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiName.Size = new System.Drawing.Size(233, 20);
            this.uiName.StyleController = this.LayoutControl;
            this.uiName.TabIndex = 4;
            // 
            // uiDefaultODTolMin
            // 
            this.uiDefaultODTolMin.EditValue = "";
            this.uiDefaultODTolMin.Location = new System.Drawing.Point(99, 180);
            this.uiDefaultODTolMin.Name = "uiDefaultODTolMin";
            this.uiDefaultODTolMin.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultODTolMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultODTolMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uiDefaultODTolMin.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultODTolMin.StyleController = this.LayoutControl;
            this.uiDefaultODTolMin.TabIndex = 4;
            // 
            // uiDefaultODTolMax
            // 
            this.uiDefaultODTolMax.EditValue = "";
            this.uiDefaultODTolMax.Location = new System.Drawing.Point(261, 180);
            this.uiDefaultODTolMax.Name = "uiDefaultODTolMax";
            this.uiDefaultODTolMax.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultODTolMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultODTolMax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uiDefaultODTolMax.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultODTolMax.StyleController = this.LayoutControl;
            this.uiDefaultODTolMax.TabIndex = 4;
            // 
            // uiDefaultIDTolMin
            // 
            this.uiDefaultIDTolMin.EditValue = "";
            this.uiDefaultIDTolMin.Location = new System.Drawing.Point(99, 228);
            this.uiDefaultIDTolMin.Name = "uiDefaultIDTolMin";
            this.uiDefaultIDTolMin.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultIDTolMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultIDTolMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uiDefaultIDTolMin.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultIDTolMin.StyleController = this.LayoutControl;
            this.uiDefaultIDTolMin.TabIndex = 4;
            // 
            // uiDefaultIDTolMax
            // 
            this.uiDefaultIDTolMax.EditValue = "";
            this.uiDefaultIDTolMax.Location = new System.Drawing.Point(261, 228);
            this.uiDefaultIDTolMax.Name = "uiDefaultIDTolMax";
            this.uiDefaultIDTolMax.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultIDTolMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiDefaultIDTolMax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uiDefaultIDTolMax.Size = new System.Drawing.Size(71, 20);
            this.uiDefaultIDTolMax.StyleController = this.LayoutControl;
            this.uiDefaultIDTolMax.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(344, 564);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciName,
            this.lciCustomerName,
            this.lciReformerName,
            this.lciRefineryName,
            this.lciInspectionTool,
            this.lciDefaultOD,
            this.lciDefaultID,
            this.lciNotes,
            this.lciTubeMaterial,
            this.lciUseRowLetters,
            this.layoutControlItem1,
            this.lciDateInspected,
            this.lciDefaultODTolMin,
            this.lciDefaultODTolMax,
            this.lciDefaultIDTolMin,
            this.lciDefaultIDTolMax,
            this.lciDefaultTubeAge,
            this.lciDefaultNextInsp});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(324, 544);
            // 
            // lciName
            // 
            this.lciName.Control = this.uiName;
            this.lciName.CustomizationFormText = "Inspection Name";
            this.lciName.Location = new System.Drawing.Point(0, 0);
            this.lciName.Name = "lciName";
            this.lciName.Size = new System.Drawing.Size(324, 24);
            this.lciName.Text = "Inspection Name:";
            this.lciName.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciCustomerName
            // 
            this.lciCustomerName.Control = this.uiCustomerName;
            this.lciCustomerName.CustomizationFormText = "Customer Name";
            this.lciCustomerName.Location = new System.Drawing.Point(0, 48);
            this.lciCustomerName.Name = "lciCustomerName";
            this.lciCustomerName.Size = new System.Drawing.Size(324, 24);
            this.lciCustomerName.Text = "Customer Name:";
            this.lciCustomerName.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciReformerName
            // 
            this.lciReformerName.Control = this.uiReformerName;
            this.lciReformerName.CustomizationFormText = "Reformer ID";
            this.lciReformerName.Location = new System.Drawing.Point(0, 96);
            this.lciReformerName.Name = "lciReformerName";
            this.lciReformerName.Size = new System.Drawing.Size(324, 24);
            this.lciReformerName.Text = "Reformer ID:";
            this.lciReformerName.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciRefineryName
            // 
            this.lciRefineryName.Control = this.uiRefineryName;
            this.lciRefineryName.CustomizationFormText = "RefineryName";
            this.lciRefineryName.Location = new System.Drawing.Point(0, 72);
            this.lciRefineryName.Name = "lciRefineryName";
            this.lciRefineryName.Size = new System.Drawing.Size(324, 24);
            this.lciRefineryName.Text = "Refinery Name:";
            this.lciRefineryName.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciInspectionTool
            // 
            this.lciInspectionTool.Control = this.uiInspectionTool;
            this.lciInspectionTool.CustomizationFormText = "Inspection Tool";
            this.lciInspectionTool.Location = new System.Drawing.Point(0, 120);
            this.lciInspectionTool.Name = "lciInspectionTool";
            this.lciInspectionTool.Size = new System.Drawing.Size(324, 24);
            this.lciInspectionTool.Text = "Inspection Tool:";
            this.lciInspectionTool.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultOD
            // 
            this.lciDefaultOD.Control = this.uiDefaultOD;
            this.lciDefaultOD.CustomizationFormText = "Default OD (un)";
            this.lciDefaultOD.Location = new System.Drawing.Point(0, 144);
            this.lciDefaultOD.Name = "lciDefaultOD";
            this.lciDefaultOD.Size = new System.Drawing.Size(324, 24);
            this.lciDefaultOD.Text = "Nominal OD (un):";
            this.lciDefaultOD.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultID
            // 
            this.lciDefaultID.Control = this.uiDefaultID;
            this.lciDefaultID.CustomizationFormText = "Default ID (un)";
            this.lciDefaultID.Location = new System.Drawing.Point(0, 192);
            this.lciDefaultID.Name = "lciDefaultID";
            this.lciDefaultID.Size = new System.Drawing.Size(324, 24);
            this.lciDefaultID.Text = "Nominal ID (un):";
            this.lciDefaultID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciNotes
            // 
            this.lciNotes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciNotes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciNotes.Control = this.uiNotes;
            this.lciNotes.CustomizationFormText = "Notes";
            this.lciNotes.Location = new System.Drawing.Point(0, 288);
            this.lciNotes.Name = "lciNotes";
            this.lciNotes.Size = new System.Drawing.Size(324, 233);
            this.lciNotes.Text = "Notes:";
            this.lciNotes.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciNotes.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciTubeMaterial
            // 
            this.lciTubeMaterial.Control = this.uiTubeMaterial;
            this.lciTubeMaterial.CustomizationFormText = "layoutControlItem1";
            this.lciTubeMaterial.Location = new System.Drawing.Point(0, 264);
            this.lciTubeMaterial.Name = "lciTubeMaterial";
            this.lciTubeMaterial.Size = new System.Drawing.Size(324, 24);
            this.lciTubeMaterial.Text = "Tube Material:";
            this.lciTubeMaterial.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciUseRowLetters
            // 
            this.lciUseRowLetters.Control = this.uiUseRowLetters;
            this.lciUseRowLetters.CustomizationFormText = "Use Row Letters";
            this.lciUseRowLetters.Location = new System.Drawing.Point(164, 521);
            this.lciUseRowLetters.Name = "lciUseRowLetters";
            this.lciUseRowLetters.Size = new System.Drawing.Size(160, 23);
            this.lciUseRowLetters.Text = "Use Row Letters";
            this.lciUseRowLetters.TextSize = new System.Drawing.Size(0, 0);
            this.lciUseRowLetters.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.uiNewTubes;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 521);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(164, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciDateInspected
            // 
            this.lciDateInspected.Control = this.uiDateInspected;
            this.lciDateInspected.CustomizationFormText = "Date Inspected";
            this.lciDateInspected.Location = new System.Drawing.Point(0, 24);
            this.lciDateInspected.Name = "lciDateInspected";
            this.lciDateInspected.Size = new System.Drawing.Size(324, 24);
            this.lciDateInspected.Text = "Date Inspected:";
            this.lciDateInspected.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultODTolMin
            // 
            this.lciDefaultODTolMin.Control = this.uiDefaultODTolMin;
            this.lciDefaultODTolMin.CustomizationFormText = "od+";
            this.lciDefaultODTolMin.Location = new System.Drawing.Point(0, 168);
            this.lciDefaultODTolMin.Name = "lciDefaultODTolMin";
            this.lciDefaultODTolMin.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultODTolMin.Text = "OD Tolerance +:";
            this.lciDefaultODTolMin.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultODTolMax
            // 
            this.lciDefaultODTolMax.Control = this.uiDefaultODTolMax;
            this.lciDefaultODTolMax.CustomizationFormText = "od-";
            this.lciDefaultODTolMax.Location = new System.Drawing.Point(162, 168);
            this.lciDefaultODTolMax.Name = "lciDefaultODTolMax";
            this.lciDefaultODTolMax.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultODTolMax.Text = "OD Tolerance -:";
            this.lciDefaultODTolMax.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultIDTolMin
            // 
            this.lciDefaultIDTolMin.Control = this.uiDefaultIDTolMin;
            this.lciDefaultIDTolMin.CustomizationFormText = "Inspection Name";
            this.lciDefaultIDTolMin.Location = new System.Drawing.Point(0, 216);
            this.lciDefaultIDTolMin.Name = "lciDefaultIDTolMin";
            this.lciDefaultIDTolMin.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultIDTolMin.Text = "ID Tolerance -:";
            this.lciDefaultIDTolMin.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultIDTolMax
            // 
            this.lciDefaultIDTolMax.Control = this.uiDefaultIDTolMax;
            this.lciDefaultIDTolMax.CustomizationFormText = "Inspection Name";
            this.lciDefaultIDTolMax.Location = new System.Drawing.Point(162, 216);
            this.lciDefaultIDTolMax.Name = "lciDefaultIDTolMax";
            this.lciDefaultIDTolMax.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultIDTolMax.Text = "ID Tolerance +:";
            this.lciDefaultIDTolMax.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultTubeAge
            // 
            this.lciDefaultTubeAge.Control = this.uiDefaultTubeAge;
            this.lciDefaultTubeAge.Location = new System.Drawing.Point(0, 240);
            this.lciDefaultTubeAge.Name = "lciDefaultTubeAge";
            this.lciDefaultTubeAge.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultTubeAge.Text = "Tube Age (yrs):";
            this.lciDefaultTubeAge.TextSize = new System.Drawing.Size(84, 13);
            // 
            // lciDefaultNextInsp
            // 
            this.lciDefaultNextInsp.Control = this.uiDefaultNextInspectionYears;
            this.lciDefaultNextInsp.Location = new System.Drawing.Point(162, 240);
            this.lciDefaultNextInsp.Name = "lciDefaultNextInsp";
            this.lciDefaultNextInsp.Size = new System.Drawing.Size(162, 24);
            this.lciDefaultNextInsp.Text = "Next Insp (yrs):";
            this.lciDefaultNextInsp.TextSize = new System.Drawing.Size(84, 13);
            // 
            // BSProject
            // 
            this.BSProject.AllowNew = false;
            this.BSProject.DataSource = typeof(Reformer.Data.Project.ReformerProjectInfo);
            // 
            // lnkSetID
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.lnkSetID, DevExpress.Utils.DefaultBoolean.Default);
            this.lnkSetID.AutoSize = true;
            this.lnkSetID.Location = new System.Drawing.Point(339, 231);
            this.lnkSetID.Name = "lnkSetID";
            this.lnkSetID.Size = new System.Drawing.Size(23, 13);
            this.lnkSetID.TabIndex = 1;
            this.lnkSetID.TabStop = true;
            this.lnkSetID.Text = "Set";
            this.defaultToolTipController1.SetTitle(this.lnkSetID, "Set ID Tolerances");
            this.defaultToolTipController1.SetToolTip(this.lnkSetID, "Click \'Set\' to update all tubes in the project with the default ID Tolerance valu" +
        "es seen here. This is not reversable.");
            this.defaultToolTipController1.SetToolTipIconType(this.lnkSetID, DevExpress.Utils.ToolTipIconType.Information);
            this.lnkSetID.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSetID_LinkClicked);
            // 
            // lnkSetOD
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.lnkSetOD, DevExpress.Utils.DefaultBoolean.Default);
            this.lnkSetOD.AutoSize = true;
            this.lnkSetOD.Location = new System.Drawing.Point(339, 183);
            this.lnkSetOD.Name = "lnkSetOD";
            this.lnkSetOD.Size = new System.Drawing.Size(23, 13);
            this.lnkSetOD.TabIndex = 2;
            this.lnkSetOD.TabStop = true;
            this.lnkSetOD.Text = "Set";
            this.defaultToolTipController1.SetTitle(this.lnkSetOD, "Set OD Tolerances");
            this.defaultToolTipController1.SetToolTip(this.lnkSetOD, "Click \'Set\' to update all tubes in the project with the default OD Tolerance valu" +
        "es seen here. This is not reversable.");
            this.defaultToolTipController1.SetToolTipIconType(this.lnkSetOD, DevExpress.Utils.ToolTipIconType.Exclamation);
            this.lnkSetOD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSetOD_LinkClicked);
            // 
            // lnkSetAge
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.lnkSetAge, DevExpress.Utils.DefaultBoolean.Default);
            this.lnkSetAge.AutoSize = true;
            this.lnkSetAge.Location = new System.Drawing.Point(339, 255);
            this.lnkSetAge.Name = "lnkSetAge";
            this.lnkSetAge.Size = new System.Drawing.Size(23, 13);
            this.lnkSetAge.TabIndex = 3;
            this.lnkSetAge.TabStop = true;
            this.lnkSetAge.Text = "Set";
            this.defaultToolTipController1.SetTitle(this.lnkSetAge, "Click \'Set\' to update all tubes in the project with the default Tube Age and Next" +
        " Inspection. This is not reversable.");
            this.defaultToolTipController1.SetToolTipIconType(this.lnkSetAge, DevExpress.Utils.ToolTipIconType.Information);
            this.lnkSetAge.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSetAge_LinkClicked);
            // 
            // InspectionInput
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lnkSetAge);
            this.Controls.Add(this.lnkSetOD);
            this.Controls.Add(this.lnkSetID);
            this.Controls.Add(this.LayoutControl);
            this.Name = "InspectionInput";
            this.Size = new System.Drawing.Size(1230, 564);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultNextInspectionYears.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultTubeAge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateInspected.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDateInspected.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNewTubes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiUseRowLetters.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeMaterial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultOD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspectionTool.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRefineryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReformerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCustomerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultODTolMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultODTolMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultIDTolMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultIDTolMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReformerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefineryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspectionTool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultOD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciUseRowLetters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateInspected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultODTolMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultODTolMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultIDTolMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultIDTolMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultTubeAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNextInsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSProject)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.BindingSource BSProject;
        private DevExpress.XtraEditors.TextEdit uiName;
        private DevExpress.XtraLayout.LayoutControlItem lciName;
        private System.Windows.Forms.BindingSource BSInspFile;
        private DevExpress.XtraEditors.ComboBoxEdit uiInspectionTool;
        private DevExpress.XtraEditors.TextEdit uiRefineryName;
        private DevExpress.XtraEditors.TextEdit uiReformerName;
        private DevExpress.XtraEditors.TextEdit uiCustomerName;
        private DevExpress.XtraLayout.LayoutControlItem lciCustomerName;
        private DevExpress.XtraLayout.LayoutControlItem lciReformerName;
        private DevExpress.XtraLayout.LayoutControlItem lciRefineryName;
        private DevExpress.XtraLayout.LayoutControlItem lciInspectionTool;
        private DevExpress.XtraEditors.CalcEdit uiDefaultID;
        private DevExpress.XtraEditors.CalcEdit uiDefaultOD;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultOD;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultID;
        private System.Windows.Forms.BindingSource BSInspSetup;
        private DevExpress.XtraEditors.MemoEdit uiNotes;
        private DevExpress.XtraLayout.LayoutControlItem lciNotes;
        private DevExpress.XtraEditors.ComboBoxEdit uiTubeMaterial;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeMaterial;
        private DevExpress.XtraEditors.CheckEdit uiUseRowLetters;
        private DevExpress.XtraLayout.LayoutControlItem lciUseRowLetters;
        private DevExpress.XtraEditors.CheckEdit uiNewTubes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DateEdit uiDateInspected;
        private DevExpress.XtraLayout.LayoutControlItem lciDateInspected;
        private DevExpress.XtraEditors.TextEdit uiDefaultODTolMin;
        private DevExpress.XtraEditors.TextEdit uiDefaultODTolMax;
        private DevExpress.XtraEditors.TextEdit uiDefaultIDTolMin;
        private DevExpress.XtraEditors.TextEdit uiDefaultIDTolMax;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultODTolMin;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultODTolMax;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultIDTolMin;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultIDTolMax;
        private System.Windows.Forms.LinkLabel lnkSetID;
        private System.Windows.Forms.LinkLabel lnkSetOD;
        private DevExpress.Utils.DefaultToolTipController defaultToolTipController1;
        private DevExpress.XtraEditors.SpinEdit uiDefaultTubeAge;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultTubeAge;
        private DevExpress.XtraEditors.SpinEdit uiDefaultNextInspectionYears;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultNextInsp;
        private System.Windows.Forms.LinkLabel lnkSetAge;
    }
}
