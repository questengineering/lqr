﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.AxialAdjustmentProcess
{
    sealed partial class TubeAxialAdjustmentXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TubeAxialAdjustmentXyPlot));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MissedStartAdjust = new DevExpress.XtraEditors.TrackBarControl();
            this.lciTubeSelecter = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMissedStartAdjust = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.MissedStartText = new DevExpress.XtraEditors.CalcEdit();
            this.lciMissedEndText = new DevExpress.XtraLayout.LayoutControlItem();
            this.MissedEndText = new DevExpress.XtraEditors.CalcEdit();
            this.MissedEndAdjust = new DevExpress.XtraEditors.TrackBarControl();
            this.lciMissedEndAdjust = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiDefaultText = new DevExpress.XtraEditors.MemoEdit();
            this.btnApply = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnApplyAll = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiMissedStartTotal = new DevExpress.XtraEditors.TextEdit();
            this.lciTotalStartAdjusted = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiMissedEndTotal = new DevExpress.XtraEditors.TextEdit();
            this.lciTotalEndAdjusted = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkFlipAll = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblHint = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnResetOneTube = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnResetAllTubes = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnFlip = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkIsFlipped = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtFilterWidth = new DevExpress.XtraEditors.TextEdit();
            this.lciFilterWidth = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFilterHeight = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtFilterHeight = new DevExpress.XtraEditors.ButtonEdit();
            this.lblLOTISFilter = new DevExpress.XtraEditors.LabelControl();
            this.lciLOTISFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnFilterAll = new DevExpress.XtraEditors.SimpleButton();
            this.lciFilterAll = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnFilterThis = new DevExpress.XtraEditors.SimpleButton();
            this.lciFilterThis = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartAdjust.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedStartAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndAdjust.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMissedStartTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalStartAdjusted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMissedEndTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalEndAdjusted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFlipAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFlipped.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLOTISFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterThis)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(803, 31);
            this.DataCursorView.Size = new System.Drawing.Size(190, 139);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnFilterThis);
            this.lcMain.Controls.Add(this.btnFilterAll);
            this.lcMain.Controls.Add(this.lblLOTISFilter);
            this.lcMain.Controls.Add(this.txtFilterWidth);
            this.lcMain.Controls.Add(this.chkIsFlipped);
            this.lcMain.Controls.Add(this.btnFlip);
            this.lcMain.Controls.Add(this.btnResetAllTubes);
            this.lcMain.Controls.Add(this.btnResetOneTube);
            this.lcMain.Controls.Add(this.lblHint);
            this.lcMain.Controls.Add(this.chkFlipAll);
            this.lcMain.Controls.Add(this.uiMissedEndTotal);
            this.lcMain.Controls.Add(this.uiMissedStartTotal);
            this.lcMain.Controls.Add(this.btnApplyAll);
            this.lcMain.Controls.Add(this.btnApply);
            this.lcMain.Controls.Add(this.MissedEndAdjust);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.Controls.Add(this.MissedStartAdjust);
            this.lcMain.Controls.Add(this.uiDefaultText);
            this.lcMain.Controls.Add(this.MissedEndText);
            this.lcMain.Controls.Add(this.MissedStartText);
            this.lcMain.Controls.Add(this.txtFilterHeight);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1560, 113, 250, 350);
            this.lcMain.Size = new System.Drawing.Size(1000, 409);
            this.lcMain.Controls.SetChildIndex(this.txtFilterHeight, 0);
            this.lcMain.Controls.SetChildIndex(this.MissedStartText, 0);
            this.lcMain.Controls.SetChildIndex(this.MissedEndText, 0);
            this.lcMain.Controls.SetChildIndex(this.uiDefaultText, 0);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.MissedStartAdjust, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.MissedEndAdjust, 0);
            this.lcMain.Controls.SetChildIndex(this.btnApply, 0);
            this.lcMain.Controls.SetChildIndex(this.btnApplyAll, 0);
            this.lcMain.Controls.SetChildIndex(this.uiMissedStartTotal, 0);
            this.lcMain.Controls.SetChildIndex(this.uiMissedEndTotal, 0);
            this.lcMain.Controls.SetChildIndex(this.chkFlipAll, 0);
            this.lcMain.Controls.SetChildIndex(this.lblHint, 0);
            this.lcMain.Controls.SetChildIndex(this.btnResetOneTube, 0);
            this.lcMain.Controls.SetChildIndex(this.btnResetAllTubes, 0);
            this.lcMain.Controls.SetChildIndex(this.btnFlip, 0);
            this.lcMain.Controls.SetChildIndex(this.chkIsFlipped, 0);
            this.lcMain.Controls.SetChildIndex(this.txtFilterWidth, 0);
            this.lcMain.Controls.SetChildIndex(this.lblLOTISFilter, 0);
            this.lcMain.Controls.SetChildIndex(this.btnFilterAll, 0);
            this.lcMain.Controls.SetChildIndex(this.btnFilterThis, 0);
            // 
            // PlotPanel
            // 
            this.PlotPanel.Size = new System.Drawing.Size(787, 317);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTubeSelecter,
            this.lciMissedStartAdjust,
            this.layoutControlItem5,
            this.lciMissedEndText,
            this.lciMissedEndAdjust,
            this.lciDefaultNote,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.lciTotalStartAdjusted,
            this.lciTotalEndAdjusted,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem2,
            this.layoutControlItem9,
            this.lciFilterWidth,
            this.lciFilterHeight,
            this.lciLOTISFilter,
            this.lciFilterAll,
            this.lciFilterThis});
            this.lcgMain.Size = new System.Drawing.Size(1000, 409);
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(796, 24);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 143);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(791, 0);
            this.splitterItem1.Size = new System.Drawing.Size(5, 399);
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Size = new System.Drawing.Size(791, 321);
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(881, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DataSource = this.BSTube;
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.gridLookUpEdit1View;
            this.TubeSelecter.Size = new System.Drawing.Size(112, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 5;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.TubeSelector_EditValueChanged);
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // MissedStartAdjust
            // 
            this.MissedStartAdjust.EditValue = -50;
            this.MissedStartAdjust.Location = new System.Drawing.Point(84, 328);
            this.MissedStartAdjust.Name = "MissedStartAdjust";
            this.MissedStartAdjust.Properties.HighlightSelectedRange = false;
            this.MissedStartAdjust.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.MissedStartAdjust.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MissedStartAdjust.Properties.Maximum = 50;
            this.MissedStartAdjust.Properties.Minimum = -50;
            this.MissedStartAdjust.Properties.TickFrequency = 5;
            this.MissedStartAdjust.Properties.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.MissedStartAdjust.Size = new System.Drawing.Size(316, 45);
            this.MissedStartAdjust.StyleController = this.lcMain;
            this.MissedStartAdjust.TabIndex = 5;
            this.MissedStartAdjust.Value = -50;
            this.MissedStartAdjust.EditValueChanged += new System.EventHandler(this.StartAdjustment_EditValueChanged);
            // 
            // lciTubeSelecter
            // 
            this.lciTubeSelecter.Control = this.TubeSelecter;
            this.lciTubeSelecter.CustomizationFormText = "Tube";
            this.lciTubeSelecter.Location = new System.Drawing.Point(796, 0);
            this.lciTubeSelecter.Name = "lciTubeSelecter";
            this.lciTubeSelecter.Size = new System.Drawing.Size(194, 24);
            this.lciTubeSelecter.Text = "Tube";
            this.lciTubeSelecter.TextSize = new System.Drawing.Size(74, 13);
            // 
            // lciMissedStartAdjust
            // 
            this.lciMissedStartAdjust.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciMissedStartAdjust.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciMissedStartAdjust.Control = this.MissedStartAdjust;
            this.lciMissedStartAdjust.CustomizationFormText = "layoutControlItem2";
            this.lciMissedStartAdjust.Location = new System.Drawing.Point(77, 321);
            this.lciMissedStartAdjust.Name = "lciMissedStartAdjust";
            this.lciMissedStartAdjust.Size = new System.Drawing.Size(320, 78);
            this.lciMissedStartAdjust.Text = "Missed Start Adjustment (in)";
            this.lciMissedStartAdjust.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMissedStartAdjust.TextLocation = DevExpress.Utils.Locations.Bottom;
            this.lciMissedStartAdjust.TextSize = new System.Drawing.Size(136, 13);
            this.lciMissedStartAdjust.TextToControlDistance = 0;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.MissedStartText;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 321);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(77, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(77, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(77, 78);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // MissedStartText
            // 
            this.MissedStartText.Location = new System.Drawing.Point(7, 328);
            this.MissedStartText.Name = "MissedStartText";
            this.MissedStartText.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MissedStartText.Properties.DisplayFormat.FormatString = "N3";
            this.MissedStartText.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MissedStartText.Properties.EditFormat.FormatString = "N3";
            this.MissedStartText.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MissedStartText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.MissedStartText.Size = new System.Drawing.Size(73, 20);
            this.MissedStartText.StyleController = this.lcMain;
            this.MissedStartText.TabIndex = 7;
            this.MissedStartText.EditValueChanged += new System.EventHandler(this.StartAdjustment_EditValueChanged);
            // 
            // lciMissedEndText
            // 
            this.lciMissedEndText.Control = this.MissedEndText;
            this.lciMissedEndText.CustomizationFormText = "Missed End";
            this.lciMissedEndText.Location = new System.Drawing.Point(633, 321);
            this.lciMissedEndText.MaxSize = new System.Drawing.Size(80, 24);
            this.lciMissedEndText.MinSize = new System.Drawing.Size(80, 24);
            this.lciMissedEndText.Name = "lciMissedEndText";
            this.lciMissedEndText.Size = new System.Drawing.Size(80, 78);
            this.lciMissedEndText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciMissedEndText.Text = "Missed End";
            this.lciMissedEndText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMissedEndText.TextSize = new System.Drawing.Size(0, 0);
            this.lciMissedEndText.TextToControlDistance = 0;
            this.lciMissedEndText.TextVisible = false;
            // 
            // MissedEndText
            // 
            this.MissedEndText.Location = new System.Drawing.Point(640, 328);
            this.MissedEndText.Name = "MissedEndText";
            this.MissedEndText.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MissedEndText.Properties.DisplayFormat.FormatString = "N3";
            this.MissedEndText.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MissedEndText.Properties.EditFormat.FormatString = "N3";
            this.MissedEndText.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MissedEndText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.MissedEndText.Size = new System.Drawing.Size(76, 20);
            this.MissedEndText.StyleController = this.lcMain;
            this.MissedEndText.TabIndex = 8;
            this.MissedEndText.EditValueChanged += new System.EventHandler(this.EndAdjustment_EditValueChanged);
            // 
            // MissedEndAdjust
            // 
            this.MissedEndAdjust.EditValue = -50;
            this.MissedEndAdjust.Location = new System.Drawing.Point(404, 328);
            this.MissedEndAdjust.Name = "MissedEndAdjust";
            this.MissedEndAdjust.Properties.HighlightSelectedRange = false;
            this.MissedEndAdjust.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.MissedEndAdjust.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MissedEndAdjust.Properties.Maximum = 50;
            this.MissedEndAdjust.Properties.Minimum = -50;
            this.MissedEndAdjust.Properties.TickFrequency = 5;
            this.MissedEndAdjust.Properties.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.MissedEndAdjust.Size = new System.Drawing.Size(232, 45);
            this.MissedEndAdjust.StyleController = this.lcMain;
            this.MissedEndAdjust.TabIndex = 6;
            this.MissedEndAdjust.Value = -50;
            this.MissedEndAdjust.EditValueChanged += new System.EventHandler(this.EndAdjustment_EditValueChanged);
            // 
            // lciMissedEndAdjust
            // 
            this.lciMissedEndAdjust.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciMissedEndAdjust.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciMissedEndAdjust.Control = this.MissedEndAdjust;
            this.lciMissedEndAdjust.CustomizationFormText = "Missed End Adjustment (in)";
            this.lciMissedEndAdjust.Location = new System.Drawing.Point(397, 321);
            this.lciMissedEndAdjust.Name = "lciMissedEndAdjust";
            this.lciMissedEndAdjust.Size = new System.Drawing.Size(236, 78);
            this.lciMissedEndAdjust.Text = "Missed End Adjustment (in)";
            this.lciMissedEndAdjust.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMissedEndAdjust.TextLocation = DevExpress.Utils.Locations.Bottom;
            this.lciMissedEndAdjust.TextSize = new System.Drawing.Size(130, 13);
            this.lciMissedEndAdjust.TextToControlDistance = 0;
            // 
            // lciDefaultNote
            // 
            this.lciDefaultNote.Control = this.uiDefaultText;
            this.lciDefaultNote.CustomizationFormText = "lciDefaultNote";
            this.lciDefaultNote.Location = new System.Drawing.Point(796, 256);
            this.lciDefaultNote.Name = "lciDefaultNote";
            this.lciDefaultNote.Size = new System.Drawing.Size(194, 59);
            this.lciDefaultNote.TextSize = new System.Drawing.Size(0, 0);
            this.lciDefaultNote.TextVisible = false;
            // 
            // uiDefaultText
            // 
            this.uiDefaultText.EditValue = "";
            this.uiDefaultText.Location = new System.Drawing.Point(803, 263);
            this.uiDefaultText.Name = "uiDefaultText";
            this.uiDefaultText.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultText.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.uiDefaultText.Size = new System.Drawing.Size(190, 55);
            this.uiDefaultText.StyleController = this.lcMain;
            this.uiDefaultText.TabIndex = 10;
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(720, 354);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(74, 22);
            this.btnApply.StyleController = this.lcMain;
            this.btnApply.TabIndex = 11;
            this.btnApply.Text = "Apply";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnApply;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(713, 347);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(78, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(78, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // btnApplyAll
            // 
            this.btnApplyAll.Location = new System.Drawing.Point(720, 380);
            this.btnApplyAll.Name = "btnApplyAll";
            this.btnApplyAll.Size = new System.Drawing.Size(74, 22);
            this.btnApplyAll.StyleController = this.lcMain;
            this.btnApplyAll.TabIndex = 12;
            this.btnApplyAll.Text = "Apply All";
            this.btnApplyAll.Click += new System.EventHandler(this.btnApplyAll_Click);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnApplyAll;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(713, 373);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // uiMissedStartTotal
            // 
            this.uiMissedStartTotal.Location = new System.Drawing.Point(803, 213);
            this.uiMissedStartTotal.Name = "uiMissedStartTotal";
            this.uiMissedStartTotal.Properties.ReadOnly = true;
            this.uiMissedStartTotal.Size = new System.Drawing.Size(93, 20);
            this.uiMissedStartTotal.StyleController = this.lcMain;
            this.uiMissedStartTotal.TabIndex = 13;
            // 
            // lciTotalStartAdjusted
            // 
            this.lciTotalStartAdjusted.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciTotalStartAdjusted.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciTotalStartAdjusted.Control = this.uiMissedStartTotal;
            this.lciTotalStartAdjusted.CustomizationFormText = "Total Start:";
            this.lciTotalStartAdjusted.Location = new System.Drawing.Point(796, 190);
            this.lciTotalStartAdjusted.Name = "lciTotalStartAdjusted";
            this.lciTotalStartAdjusted.Size = new System.Drawing.Size(97, 40);
            this.lciTotalStartAdjusted.Text = "Total Start (in):";
            this.lciTotalStartAdjusted.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciTotalStartAdjusted.TextSize = new System.Drawing.Size(74, 13);
            // 
            // uiMissedEndTotal
            // 
            this.uiMissedEndTotal.Location = new System.Drawing.Point(900, 213);
            this.uiMissedEndTotal.Name = "uiMissedEndTotal";
            this.uiMissedEndTotal.Properties.ReadOnly = true;
            this.uiMissedEndTotal.Size = new System.Drawing.Size(93, 20);
            this.uiMissedEndTotal.StyleController = this.lcMain;
            this.uiMissedEndTotal.TabIndex = 14;
            // 
            // lciTotalEndAdjusted
            // 
            this.lciTotalEndAdjusted.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciTotalEndAdjusted.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciTotalEndAdjusted.Control = this.uiMissedEndTotal;
            this.lciTotalEndAdjusted.CustomizationFormText = "Total End:";
            this.lciTotalEndAdjusted.Location = new System.Drawing.Point(893, 190);
            this.lciTotalEndAdjusted.Name = "lciTotalEndAdjusted";
            this.lciTotalEndAdjusted.Size = new System.Drawing.Size(97, 40);
            this.lciTotalEndAdjusted.Text = "Total End (in):";
            this.lciTotalEndAdjusted.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciTotalEndAdjusted.TextSize = new System.Drawing.Size(74, 13);
            // 
            // chkFlipAll
            // 
            this.chkFlipAll.Location = new System.Drawing.Point(759, 328);
            this.chkFlipAll.Name = "chkFlipAll";
            this.chkFlipAll.Properties.Caption = "All";
            this.chkFlipAll.Size = new System.Drawing.Size(35, 19);
            this.chkFlipAll.StyleController = this.lcMain;
            this.chkFlipAll.TabIndex = 15;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.chkFlipAll;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(752, 321);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(39, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // lblHint
            // 
            this.lblHint.Location = new System.Drawing.Point(803, 322);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(145, 13);
            this.lblHint.StyleController = this.lcMain;
            this.lblHint.TabIndex = 17;
            this.lblHint.Text = "Q/E Move Previous/Next Tube";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lblHint;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(796, 315);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // btnResetOneTube
            // 
            this.btnResetOneTube.Location = new System.Drawing.Point(803, 237);
            this.btnResetOneTube.Name = "btnResetOneTube";
            this.btnResetOneTube.Size = new System.Drawing.Size(93, 22);
            this.btnResetOneTube.StyleController = this.lcMain;
            this.btnResetOneTube.TabIndex = 18;
            this.btnResetOneTube.Text = "Reset";
            this.btnResetOneTube.ToolTip = "Resets the start and end positions, but does not un-flip data.";
            this.btnResetOneTube.Click += new System.EventHandler(this.btnResetOneTube_Click);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnResetOneTube;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(796, 230);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(97, 26);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // btnResetAllTubes
            // 
            this.btnResetAllTubes.Location = new System.Drawing.Point(900, 237);
            this.btnResetAllTubes.Name = "btnResetAllTubes";
            this.btnResetAllTubes.Size = new System.Drawing.Size(93, 22);
            this.btnResetAllTubes.StyleController = this.lcMain;
            this.btnResetAllTubes.TabIndex = 19;
            this.btnResetAllTubes.Text = "Reset All";
            this.btnResetAllTubes.ToolTip = "Resets the start and end positions, but does not un-flip data.";
            this.btnResetAllTubes.Click += new System.EventHandler(this.btnResetAllTubes_Click);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnResetAllTubes;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(893, 230);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(97, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // btnFlip
            // 
            this.btnFlip.Location = new System.Drawing.Point(720, 328);
            this.btnFlip.Name = "btnFlip";
            this.btnFlip.Size = new System.Drawing.Size(35, 22);
            this.btnFlip.StyleController = this.lcMain;
            this.btnFlip.TabIndex = 12;
            this.btnFlip.Text = "Flip";
            this.btnFlip.Click += new System.EventHandler(this.btnFlip_Click);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnFlip;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(713, 321);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(39, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // chkIsFlipped
            // 
            this.chkIsFlipped.Location = new System.Drawing.Point(803, 174);
            this.chkIsFlipped.Name = "chkIsFlipped";
            this.chkIsFlipped.Properties.Caption = "Is Flipped?";
            this.chkIsFlipped.Properties.ReadOnly = true;
            this.chkIsFlipped.Size = new System.Drawing.Size(190, 19);
            this.chkIsFlipped.StyleController = this.lcMain;
            this.chkIsFlipped.TabIndex = 20;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.chkIsFlipped;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(796, 167);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(194, 23);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // txtFilterWidth
            // 
            this.txtFilterWidth.EditValue = "0";
            this.txtFilterWidth.Location = new System.Drawing.Point(803, 356);
            this.txtFilterWidth.Name = "txtFilterWidth";
            this.txtFilterWidth.Properties.Mask.EditMask = "f0";
            this.txtFilterWidth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtFilterWidth.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtFilterWidth.Size = new System.Drawing.Size(93, 20);
            this.txtFilterWidth.StyleController = this.lcMain;
            this.txtFilterWidth.TabIndex = 21;
            // 
            // lciFilterWidth
            // 
            this.lciFilterWidth.Control = this.txtFilterWidth;
            this.lciFilterWidth.Location = new System.Drawing.Point(796, 349);
            this.lciFilterWidth.Name = "lciFilterWidth";
            this.lciFilterWidth.Size = new System.Drawing.Size(97, 24);
            this.lciFilterWidth.TextSize = new System.Drawing.Size(0, 0);
            this.lciFilterWidth.TextVisible = false;
            // 
            // lciFilterHeight
            // 
            this.lciFilterHeight.Control = this.txtFilterHeight;
            this.lciFilterHeight.Location = new System.Drawing.Point(893, 349);
            this.lciFilterHeight.Name = "lciFilterHeight";
            this.lciFilterHeight.Size = new System.Drawing.Size(97, 24);
            this.lciFilterHeight.Text = "by";
            this.lciFilterHeight.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciFilterHeight.TextSize = new System.Drawing.Size(12, 13);
            this.lciFilterHeight.TextToControlDistance = 5;
            // 
            // txtFilterHeight
            // 
            this.txtFilterHeight.EditValue = "1";
            this.txtFilterHeight.Location = new System.Drawing.Point(917, 356);
            this.txtFilterHeight.Name = "txtFilterHeight";
            this.txtFilterHeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Revert", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtFilterHeight.Properties.Mask.EditMask = "f0";
            this.txtFilterHeight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtFilterHeight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtFilterHeight.Size = new System.Drawing.Size(76, 20);
            this.txtFilterHeight.StyleController = this.lcMain;
            this.txtFilterHeight.TabIndex = 22;
            this.txtFilterHeight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFilterHeight_ButtonClick);
            // 
            // lblLOTISFilter
            // 
            this.lblLOTISFilter.Location = new System.Drawing.Point(803, 339);
            this.lblLOTISFilter.Name = "lblLOTISFilter";
            this.lblLOTISFilter.Size = new System.Drawing.Size(114, 13);
            this.lblLOTISFilter.StyleController = this.lcMain;
            this.lblLOTISFilter.TabIndex = 23;
            this.lblLOTISFilter.Text = "LOTIS Filter Parameters";
            // 
            // lciLOTISFilter
            // 
            this.lciLOTISFilter.Control = this.lblLOTISFilter;
            this.lciLOTISFilter.Location = new System.Drawing.Point(796, 332);
            this.lciLOTISFilter.Name = "lciLOTISFilter";
            this.lciLOTISFilter.Size = new System.Drawing.Size(194, 17);
            this.lciLOTISFilter.TextSize = new System.Drawing.Size(0, 0);
            this.lciLOTISFilter.TextVisible = false;
            // 
            // btnFilterAll
            // 
            this.btnFilterAll.Location = new System.Drawing.Point(900, 380);
            this.btnFilterAll.Name = "btnFilterAll";
            this.btnFilterAll.Size = new System.Drawing.Size(93, 22);
            this.btnFilterAll.StyleController = this.lcMain;
            this.btnFilterAll.TabIndex = 24;
            this.btnFilterAll.Text = "Filter All";
            this.btnFilterAll.Click += new System.EventHandler(this.btnFilterThis_Click);
            // 
            // lciFilterAll
            // 
            this.lciFilterAll.Control = this.btnFilterAll;
            this.lciFilterAll.Location = new System.Drawing.Point(893, 373);
            this.lciFilterAll.Name = "lciFilterAll";
            this.lciFilterAll.Size = new System.Drawing.Size(97, 26);
            this.lciFilterAll.TextSize = new System.Drawing.Size(0, 0);
            this.lciFilterAll.TextVisible = false;
            // 
            // btnFilterThis
            // 
            this.btnFilterThis.Location = new System.Drawing.Point(803, 380);
            this.btnFilterThis.Name = "btnFilterThis";
            this.btnFilterThis.Size = new System.Drawing.Size(93, 22);
            this.btnFilterThis.StyleController = this.lcMain;
            this.btnFilterThis.TabIndex = 25;
            this.btnFilterThis.Text = "Filter This";
            this.btnFilterThis.Click += new System.EventHandler(this.btnFilterThis_Click);
            // 
            // lciFilterThis
            // 
            this.lciFilterThis.Control = this.btnFilterThis;
            this.lciFilterThis.Location = new System.Drawing.Point(796, 373);
            this.lciFilterThis.Name = "lciFilterThis";
            this.lciFilterThis.Size = new System.Drawing.Size(97, 26);
            this.lciFilterThis.TextSize = new System.Drawing.Size(0, 0);
            this.lciFilterThis.TextVisible = false;
            // 
            // TubeAxialAdjustmentXyPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "TubeAxialAdjustmentXyPlot";
            this.Size = new System.Drawing.Size(1000, 409);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartAdjust.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedStartAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedStartText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndAdjust.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MissedEndAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMissedStartTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalStartAdjusted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMissedEndTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalEndAdjusted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFlipAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsFlipped.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLOTISFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFilterThis)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraEditors.TrackBarControl MissedStartAdjust;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSelecter;
        private DevExpress.XtraLayout.LayoutControlItem lciMissedStartAdjust;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TrackBarControl MissedEndAdjust;
        private DevExpress.XtraLayout.LayoutControlItem lciMissedEndText;
        private DevExpress.XtraLayout.LayoutControlItem lciMissedEndAdjust;
        private DevExpress.XtraEditors.MemoEdit uiDefaultText;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultNote;
        private DevExpress.XtraEditors.SimpleButton btnApplyAll;
        private DevExpress.XtraEditors.SimpleButton btnApply;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit uiMissedEndTotal;
        private DevExpress.XtraEditors.TextEdit uiMissedStartTotal;
        private DevExpress.XtraLayout.LayoutControlItem lciTotalStartAdjusted;
        private DevExpress.XtraLayout.LayoutControlItem lciTotalEndAdjusted;
        private DevExpress.XtraEditors.CalcEdit MissedEndText;
        private DevExpress.XtraEditors.CalcEdit MissedStartText;
        private DevExpress.XtraEditors.CheckEdit chkFlipAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl lblHint;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnResetAllTubes;
        private DevExpress.XtraEditors.SimpleButton btnResetOneTube;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton btnFlip;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.CheckEdit chkIsFlipped;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtFilterWidth;
        private DevExpress.XtraEditors.ButtonEdit txtFilterHeight;
        private DevExpress.XtraLayout.LayoutControlItem lciFilterWidth;
        private DevExpress.XtraLayout.LayoutControlItem lciFilterHeight;
        private DevExpress.XtraEditors.LabelControl lblLOTISFilter;
        private DevExpress.XtraLayout.LayoutControlItem lciLOTISFilter;
        private DevExpress.XtraEditors.SimpleButton btnFilterThis;
        private DevExpress.XtraEditors.SimpleButton btnFilterAll;
        private DevExpress.XtraLayout.LayoutControlItem lciFilterAll;
        private DevExpress.XtraLayout.LayoutControlItem lciFilterThis;
    }
}
