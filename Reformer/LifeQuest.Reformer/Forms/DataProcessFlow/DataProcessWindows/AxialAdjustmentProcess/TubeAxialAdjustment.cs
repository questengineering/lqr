﻿using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.AxialAdjustmentProcess
{
    public partial class TubeAxialAdjustment : BaseProcessWindow, IHasDisplayUnits
    {
        public TubeAxialAdjustment()
        {
            InitializeComponent();
        }

        public override void UpdateDataSource()
        {
            TheAdjustmentPlot.SetInspection(CurrentInspection);
        }

        public void UpdateDisplayUnits()
        {
            TheAdjustmentPlot.UpdateDisplayUnits();
        }
    }
}
