﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using Nevron.Chart;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.AxialAdjustmentProcess
{
    public sealed partial class TubeAxialAdjustmentXyPlot : DataXyPlot, IHasDisplayUnits
    {
        private ReformerTube CurrentTube => TubeSelecter.EditValue as ReformerTube;
        private readonly List<NLineSeries> _currentLines = new List<NLineSeries>();
        private double StartOffset => MissedStartText.Value.ToDouble();
        private double EndOffset => MissedEndText.Value.ToDouble();
        private ReformerScalar CurrentScalar => CurrentTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside;
        private List<float> _originalPosData = new List<float>();
        private readonly Action<int, string> _progressUpdateMethod;
        private bool _updatingTextValues = false;
        
        public TubeAxialAdjustmentXyPlot(Action<int, string> progressUpdateMethod = null)
        {
            InitializeComponent();
            InitializeChart();
            InitializeDataCursor();
            _progressUpdateMethod = progressUpdateMethod;
            DraggableVertConstLines = true;
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            MissedStartAdjust.EditValue = MissedEndAdjust.EditValue = 0; //Due to visual inheritance issues, have to set this manually.
            lciFilterHeight.Visibility = lciFilterWidth.Visibility = lciLOTISFilter.Visibility = lciFilterThis.Visibility = lciFilterAll.Visibility = CurrentInspection?.InspectionTool == Tool.LOTIS ? LayoutVisibility.Always : LayoutVisibility.Never;
        }

        protected override void InitializeChart()
        {
            TheChartControl.Legends.Clear();
            base.InitializeChart();
        }

        internal void SetInspection(ReformerInspection Inspection)
        {
            if (Inspection == null) return;
            BSTube.DataSource = Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.
            Tool tool = Inspection.InspectionTool;
            if (tool == Tool.LOTIS)
                uiDefaultText.Text = $"Default Values for {tool} are: ({DefaultValues.MissedStartData[tool]} in. and {DefaultValues.MissedEndData[tool]} in.)";
            else if (tool == Tool.MANTIS)
                uiDefaultText.Text = $"Default Values for {tool} are: ({DefaultValues.MissedStartData[tool]} in. and {DefaultValues.MissedEndData[tool]} in.). If Downflow, reverse them.";
            
            MoveFirst();
        }

        private void TubeSelector_EditValueChanged(object sender, EventArgs e)
        {
            DrawPlot();
        }

        private void DrawPlot()
        {
            if (CurrentTube == null) return;
            Stopwatch sw = Stopwatch.StartNew();
            DrawCurrentTube();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            UpdateMissedStart();
            UpdateMissedEnd();
            chkIsFlipped.Checked = CurrentTube.AxiallyFlipped;
            Log.Info($"Drew Tube Data for {CurrentTube.Name} in {sw.ElapsedMilliseconds} ms");
        }

        private void DrawCurrentTube()
        {
            ClearChart();
            _currentLines.Clear();
            if (CurrentTube == null) return;

            NLineSeries newLine = CurrentTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            CurrentTube.GetPositions(out _originalPosData); //cache position data for drawing later.
            newLine.Name = CurrentTube.Name;
            newLine.Tag = CurrentTube.ID;
            for (int i = 0; i < newLine.Values.Count; i++) { newLine.Values[i] = (double)newLine.Values[i] * 2;} //Get Diameter from radius data
            _currentLines.Add(newLine);

            string xLabel = $"Axial Position ({DefaultValues.AxialUnitSymbol})";
            string yLabel = $"{(CurrentScalar.DisplayName.ToLower().Contains("inside") ? "Diameter (Inside)" : "Diameter (Outside)")} ({DefaultValues.MeasurementUnitSymbol})";
            AddLines(_currentLines, xLabel, yLabel);
            uiMissedStartTotal.EditValue = CurrentTube.Specs.MissedStart;
            uiMissedEndTotal.EditValue = CurrentTube.Specs.MissedEnd;
            TheChartControl.RecalcLayout();
        }

        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            int nextKey = 0;
            if (TubeSelecter.EditValue != null) nextKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }

        private void TheChartControlOnKeyDown(object Sender, KeyEventArgs Args)
        {
            if (Args.KeyCode == Keys.Q) MovePrevious();
            else if (Args.KeyCode == Keys.E) MoveNext();
        }

        private void EndAdjustment_EditValueChanged(object sender, EventArgs e)
        {
            if (_updatingTextValues) return;
            if (sender == MissedEndText)
            {
                _updatingTextValues = true;
                MissedEndAdjust.EditValue = MissedEndText.EditValue.ToDouble();
                _updatingTextValues = false;
            }
            else
            {
                _updatingTextValues = true; 
                MissedEndText.EditValue = MissedEndAdjust.EditValue;
                _updatingTextValues = false;
            }
            UpdateMissedEnd();
        }

        private void StartAdjustment_EditValueChanged(object sender, EventArgs e)
        {
            if (_updatingTextValues) return;
            if (sender == MissedStartText)
            {
                _updatingTextValues = true;
                MissedStartAdjust.EditValue = MissedStartText.EditValue.ToDouble();
                _updatingTextValues = false;
            }
            else 
            {
                _updatingTextValues = true;
                MissedStartText.EditValue = MissedStartAdjust.EditValue;
                _updatingTextValues = false;
            }
            UpdateMissedStart();
        }

        private void UpdateMissedStart()
        {
            if (CurrentTube == null) return;
            //move the line series accordingly.
            var currentTubeSeriesxValues = ((NLineSeries)Chart.Series[0]).XValues;
            var startOffset = StartOffset;
            for (int i = 0; i < currentTubeSeriesxValues.Count; i++) currentTubeSeriesxValues[i] = _originalPosData[i] + startOffset;

            UpdateXAxis(0, CurrentTube.EndPosition + CurrentTube.Specs.MissedEnd + EndOffset + startOffset);
            TheChartControl.Refresh();
        }
        
        private void UpdateMissedEnd()
        {
            if (CurrentTube == null) return;
            MissedEndText.Text = EndOffset.ToString("N3");
            UpdateXAxis(0, CurrentTube.EndPosition + CurrentTube.Specs.MissedEnd + EndOffset + StartOffset);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnApply.Enabled = btnApplyAll.Enabled = false; Cursor = Cursors.WaitCursor;
            AdjustTube(CurrentTube);
            DrawPlot();
            MissedStartAdjust.EditValue = MissedEndAdjust.EditValue = 0;
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            btnApply.Enabled = btnApplyAll.Enabled = true; Cursor = Cursors.Default;
        }

        private void AdjustTube(ReformerTube Tube)
        {
            Tube.ShiftAxialData(StartOffset);
            Tube.Specs.MissedStart += (float)StartOffset;
            Tube.Specs.MissedEnd += (float)EndOffset;
        }

        private void ResetTube(ReformerTube Tube)
        {
            Tube.ShiftAxialData(-Tube.Specs.MissedStart);
            Tube.Specs.MissedStart = 0;
            Tube.Specs.MissedEnd = 0;
        }

        private void btnApplyAll_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnApply.Enabled = btnApplyAll.Enabled = false; Cursor = Cursors.WaitCursor;
            int counter = 0;
            Parallel.ForEach(CurrentTube.Inspection.Tubes,T =>
            {
                if (!T.AvailableScalars.Contains(ReformerScalar.AxialPosition)) return;
                AdjustTube(T);
                counter = Interlocked.Increment(ref counter);
                OnProgressChanged((int)Math.Round(counter / (float)T.Inspection.Tubes.Count * 100), "Adjusted: " + T.Name);
            });
            DrawPlot();
            MissedStartAdjust.EditValue = MissedEndAdjust.EditValue = 0;
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            OnProgressChanged(100, "Finished Adjusting All Tubes.");
            btnApply.Enabled = btnApplyAll.Enabled = true; Cursor = Cursors.Default;
        }

        private void OnProgressChanged(int Progress = -1, string Status = "SkipProgress") //TODO: Refactor this to base class and let all XY Plots report status
        {
            if (_progressUpdateMethod == null) return;
            _progressUpdateMethod(Progress, Status);
        }

        public void UpdateDisplayUnits()
        {
            DrawPlot();
        }

        private void btnResetOneTube_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = false; Cursor = Cursors.WaitCursor;

            OnProgressChanged(10, "Resetting: " + CurrentTube.Name);
            ResetTube(CurrentTube);
            OnProgressChanged(75, "Reset: " + CurrentTube.Name);
            
            DrawPlot();
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            OnProgressChanged(100, "Finished Resetting: " + CurrentTube.Name);
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = true; Cursor = Cursors.Default;
        }

        private void btnResetAllTubes_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = false; Cursor = Cursors.WaitCursor;
            int counter = 0;
            Parallel.ForEach(CurrentTube.Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)), T =>
            {
                ResetTube(T);
                counter = Interlocked.Increment(ref counter);
                OnProgressChanged((int)Math.Round(counter / (float)T.Inspection.Tubes.Count * 100), "Reset: " + T.Name);
            });
            DrawPlot();
            MissedStartAdjust.EditValue = MissedEndAdjust.EditValue = 0;
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            OnProgressChanged(100, "Finished Resetting All Tubes.");
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = true; Cursor = Cursors.Default;
        }

        private async void btnFlip_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            Enabled = false; Cursor = Cursors.WaitCursor;
            int counter = 0;
            Stopwatch sw = Stopwatch.StartNew();
            List<ReformerTube> tubesToAdjust = new List<ReformerTube>();
            if (chkFlipAll.Checked)
                tubesToAdjust.AddRange(CurrentTube.Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)));
            else
                tubesToAdjust.Add(CurrentTube);

            Task newTask = Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(tubesToAdjust, T =>
                {
                    T.FlipAxialData();
                    T.AxiallyFlipped = !T.AxiallyFlipped;
                    counter = Interlocked.Increment(ref counter);
                    OnProgressChanged((int)Math.Round(counter / (float)T.Inspection.Tubes.Count * 100), "Flipped: " + T.Name);
                });
            });

            await newTask;
            DrawPlot();
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            OnProgressChanged(100, $"Finished Flipping {tubesToAdjust.Count} tube(s) in {sw.Elapsed.TotalSeconds.ToString("N1")} s");
            Enabled = true; Cursor = Cursors.Default;
        }

        private void txtFilterHeight_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (CurrentTube == null) return;
            Enabled = false; Cursor = Cursors.WaitCursor;
            try
            {
               if (e.Button.Kind == ButtonPredefines.Delete)
                    ApplyFilter(CurrentTube, 0, 0);
            }
            finally
            {
                Enabled = true; Cursor = Cursors.Default;
                DrawPlot();
            }
            
        }

        private void ApplyFilter(ReformerTube tube, int filterRadius, int filterAxialRadius)
        {
            Stopwatch sw = Stopwatch.StartNew();
            float[,] scalarData;

            //Create the 'original' dataset if it doesn't exist yet.
            if (!tube.AvailableScalars.Contains(ReformerScalar.RadiusInsideOriginal))
            {
                tube.Get2DScalarData(ReformerScalar.RadiusInside, out scalarData);
                tube.Set2DScalarData(ReformerScalar.RadiusInsideOriginal, scalarData);
            }
            else
            {
                tube.Get2DScalarData(ReformerScalar.RadiusInsideOriginal, out scalarData);
            }

            
            for (int centerAxialIdx = 0; centerAxialIdx < scalarData.GetLength(0); centerAxialIdx++)
            {
                for (int centerSensor = 0; centerSensor < scalarData.GetLength(1); centerSensor++)
                {
                    List<float> valueBuffer = new List<float>();
                    for (int sensorIdx = -filterRadius; sensorIdx <= filterRadius; sensorIdx++)
                    {
                        int actualSensorIdx = (centerSensor + sensorIdx)%scalarData.GetLength(1);
                        if (actualSensorIdx < 0) actualSensorIdx += scalarData.GetLength(1);
                        valueBuffer.Add(scalarData[centerAxialIdx, actualSensorIdx]);
                    }
                    for (int axialIdx = -filterAxialRadius; axialIdx <= filterAxialRadius; axialIdx++)
                    {
                        int actualIdx = centerAxialIdx + axialIdx;
                        if (!actualIdx.Between(0, scalarData.GetLength(0) - 1)) continue;

                        valueBuffer.Add(scalarData[actualIdx, centerSensor]);
                    }
                    scalarData[centerAxialIdx, centerSensor] = valueBuffer.Mean(true);
                }
            }
            tube.Set2DScalarData(ReformerScalar.RadiusInside, scalarData);
            OnProgressChanged(100, $"Finished applying {filterAxialRadius} axial and {filterRadius} circumferential point filter to {tube.Name} in {sw.Elapsed.TotalSeconds.ToString("N1")} s");
        }

        private void btnFilterThis_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            Enabled = false; Cursor = Cursors.WaitCursor;
            try
            {
                int filterRadius = txtFilterHeight.EditValue.ToInt32(0).Constrain(0, 100);
                int filterAxialRadius = txtFilterWidth.EditValue.ToInt32(0).Constrain(0, 100);

                if (sender == btnFilterThis)
                    ApplyFilter(CurrentTube, filterRadius, filterAxialRadius);
                else if (sender == btnFilterAll)
                    CurrentInspection.Tubes.ForEach(t => ApplyFilter(t, filterRadius, filterAxialRadius));
            }
            finally
            {
                Enabled = true;
                Cursor = Cursors.Default;
                DrawPlot();
            }
        }
    }
}
