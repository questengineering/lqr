﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nevron.Chart;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess
{
    public sealed partial class TubeRadialAdjustmentXyPlot : DataXyPlot, IHasDisplayUnits
    {
        private ReformerTube CurrentTube { get { return TubeSelecter.EditValue as ReformerTube; } }
        private readonly List<NLineSeries> _currentLines = new List<NLineSeries>();
        private const double SliderRangeInInches = .5;
        private double InchesPerSliderUnit { get { return SliderRangeInInches / (ShiftAdustSlider.Properties.Maximum - ShiftAdustSlider.Properties.Minimum); } }
        private double ShiftOffset { get { return InchesPerSliderUnit * ShiftAdustSlider.Value; } }
        private ReformerScalar CurrentScalar { get { return CurrentTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside; } }
        private double MeasUnitFactor { get { return CurrentInspection.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale); } }
        private ReformerInspection RefInspection { get { return uiReferenceInsp.EditValue as ReformerInspection; } }
        private bool AdjustRefLine { get { return chkShiftRef.Checked; } }

        private ReformerTube RefTube
        {
            get
            { 
                //return null if there's no CurrentTube, no RefInspection, or no tubes named the same as CurrentTube in RefInspection.
                if (CurrentTube == null || RefInspection == null || RefInspection.Tubes.All(T => CurrentTube.Name != T.Name)) return null;
                return RefInspection.Tubes.First(T => T.Name == CurrentTube.Name);
            }
        }

        private List<double> _originalMeanData, _refMeanData; 

        public TubeRadialAdjustmentXyPlot()
        {
            InitializeComponent();
            UpdateDisplayUnits();
            InitializeChart();
            InitializeDataCursor();
            DraggableVertConstLines = true;
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            ShiftAdustSlider.EditValue = 0; //Due to visual inheritance issues, have to set this manually.
        }

        protected override void InitializeChart()
        {
            TheChartControl.Legends.Clear();
            base.InitializeChart();
        }

        internal void SetInspection(ReformerInspection Inspection)
        {
            if (Inspection == null) return;
            BSTube.DataSource = Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
            List<ReformerInspection> otherInspFiles = CurrentProject.InspectionFiles.Where(I => I != CurrentInspection).ToList();
            uiReferenceInsp.Properties.DataSource = otherInspFiles;
            if (otherInspFiles.Count == 0)
            {
                uiReferenceInsp.Enabled = false;
                chkShiftRef.Enabled = false;
            }
            else
            {
                uiReferenceInsp.EditValue = otherInspFiles[0];
            }
            MoveFirst();
        }

        private void TubeSelector_EditValueChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            ShiftText.EditValue = 0;
            DrawPlot();
        }

        private void DrawPlot()
        {
            if (CurrentTube == null) return;
            Stopwatch sw = Stopwatch.StartNew();
            SetYAxisOffset(0);
            DrawTubes();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            UpdateXAxis(CurrentTube.StartPosition - CurrentTube.Specs.MissedStart, CurrentTube.EndPosition + CurrentTube.Specs.MissedEnd);
            UpdateShiftValues();
            DrawTubeHorzConstLines();
            Log.Info(string.Format("Drew Tube Data for {0} in {1} ms", CurrentTube.Name, sw.ElapsedMilliseconds));
        }

        private void DrawTubeHorzConstLines()
        {
            double lowerTol, upperTol;
            if (CurrentScalar == ReformerScalar.RadiusInside)
            {
                lowerTol = CurrentTube.Specs.DiameterInside + CurrentTube.Specs.IDTolDown;
                upperTol = CurrentTube.Specs.DiameterInside + CurrentTube.Specs.IDTolUp;
            }
            else
            {
                lowerTol = CurrentTube.Specs.DiameterOutside + CurrentTube.Specs.ODTolDown;
                upperTol = CurrentTube.Specs.DiameterOutside + CurrentTube.Specs.ODTolUp;
            }
            
            ConstLineValue upperTolLine = new ConstLineValue { Alignment = ContentAlignment.TopRight, Value = upperTol, Color = Color.Gray, PatternSize = 2, Text = (upperTol * MeasUnitFactor).ToString(DefaultValues.MeasHashFormat) };
            ConstLineValue lowerTolLine = new ConstLineValue { Alignment = ContentAlignment.BottomRight, Value = lowerTol, Color = Color.Gray, PatternSize = 2, Text = (lowerTol * MeasUnitFactor).ToString(DefaultValues.MeasHashFormat) };
            var values = new List<ConstLineValue> { upperTolLine, lowerTolLine };
            SetHorzConstLines(values);
        }

        private void DrawTubes()
        {
            ClearChart();
            _currentLines.Clear();
            if (CurrentTube == null) return;
            
            //Gather a default-formatted line.
            NLineSeries newLine = CurrentTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            newLine.Name = CurrentTube.Name;
            for (int i = 0; i < newLine.Values.Count; i++) { newLine.Values[i] = (double)newLine.Values[i] * 2;} //Get Diameter from radius data
            _currentLines.Add(newLine);
            _originalMeanData = new List<double>();
            foreach (double d in newLine.Values) _originalMeanData.Add(d);

            _refMeanData = new List<double>();
            NLineSeries refLine = GetReferenceTubeLine();
            if (refLine != null)
            {
                _currentLines.Add(refLine);
                foreach (double d in refLine.Values) _refMeanData.Add(d);
            }

            //Place the tube data and labels on the graph
            string xLabel = string.Format("Axial Position ({0})", DefaultValues.AxialUnitSymbol);
            string yLabel = string.Format("{0} ({1})", CurrentScalar.DisplayName.ToLower().Contains("inside") ? "Diameter (Inside)" : "Diameter (Outside)", DefaultValues.MeasurementUnitSymbol);
            AddLines(_currentLines, xLabel, yLabel);

            ReformerTube tubeToShowShiftFor = AdjustRefLine && RefTube != null ? RefTube : CurrentTube;
            uiTotalShift.EditValue = Math.Round(tubeToShowShiftFor.Specs.RadialShift * 2 * MeasUnitFactor, 3);
            TheChartControl.RecalcLayout();
        }

        /// <summary> Gets a formatted reference tube line. Returns null if not able to draw it. </summary>
        private NLineSeries GetReferenceTubeLine()
        {
            ReformerTube refTube = RefTube; //cache locally to avoid lots of lookups.
            if (refTube == null || !refTube.AvailableScalars.Contains(CurrentScalar)) return null;
            
            //Gather a default-formatted line.
            NLineSeries newLine = refTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            newLine.Name = "Reference";
            newLine.BorderStyle.Color = Color.Gray;
            for (int i = 0; i < newLine.Values.Count; i++) { newLine.Values[i] = (double)newLine.Values[i] * 2; } //Get Diameter from radius data
            return newLine;
        }

        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            int nextKey = 0;
            if (TubeSelecter.EditValue != null) nextKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }

        private void TheChartControlOnKeyDown(object Sender, KeyEventArgs Args)
        {
            if (Args.KeyCode == Keys.Q) MovePrevious();
            else if (Args.KeyCode == Keys.E) MoveNext();
        }

        private void ShiftValueChanged(object sender, EventArgs e)
        {
            if (sender == ShiftText) { ShiftAdustSlider.EditValue = (ShiftText.EditValue.ToDouble() / MeasUnitFactor) / InchesPerSliderUnit; }
            UpdateShiftValues();
        }

        private void UpdateShiftValues()
        {
            if (CurrentTube == null) return;
            ShiftText.Text = (ShiftOffset * MeasUnitFactor).ToString("N3");
            string targetLineName = AdjustRefLine ? "Reference" : CurrentTube.Name;
            List<double> unshiftedData = AdjustRefLine ? _refMeanData : _originalMeanData;
            List<NSeries> allSeries = Chart.Series.Cast<NSeries>().ToList();
            NSeries seriesToChange = allSeries.FirstOrDefault(S => S.Name == targetLineName);
            if (seriesToChange == null) return;
            for (int i = 0; i < seriesToChange.Values.Count; i++) { seriesToChange.Values[i] = unshiftedData[i] + ShiftOffset; }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            bool applyAllStatus = btnApplyAll.Enabled;
            btnApply.Enabled = btnApplyAll.Enabled = false; Cursor = Cursors.WaitCursor;
            AdjustTube(AdjustRefLine ? RefTube : CurrentTube);
            ShiftAdustSlider.EditValue = 0;
            UpdateShiftValues();
            DrawTubes();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            btnApply.Enabled = true; Cursor = Cursors.Default;
            btnApplyAll.Enabled = applyAllStatus; //separate because ref line adjustment may have this disable to start with.
        }

        private void AdjustTube(ReformerTube Tube)
        {
            if (Tube == null) return;
            foreach (ReformerScalar scalar in Tube.AvailableScalars.Where(S => S == ReformerScalar.RadiusInside || S == ReformerScalar.RadiusOutside))
            {
                Tube.ShiftRadialData(scalar, ShiftOffset / 2); //Divide by 2 because we're changing radius from diameter input, so effect is halved.
            }
            Tube.Specs.RadialShift += (float)ShiftOffset / 2;
        }

        private void ResetTube(ReformerTube Tube)
        {
            if (Tube == null) return;
            foreach (ReformerScalar scalar in Tube.AvailableScalars.Where(S => S == ReformerScalar.RadiusInside || S == ReformerScalar.RadiusOutside))
            {
                Tube.ShiftRadialData(scalar, -Tube.Specs.RadialShift); //Remove any radial shifts that have been applied.
            }
            Tube.Specs.RadialShift = 0;
        }

        private void btnApplyAll_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnApply.Enabled = btnApplyAll.Enabled = false; Cursor = Cursors.WaitCursor;
            Parallel.ForEach(CurrentTube.Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)), AdjustTube);
            ShiftAdustSlider.EditValue = 0;
            UpdateShiftValues();
            DrawTubes();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            btnApply.Enabled = btnApplyAll.Enabled = true; Cursor = Cursors.Default;
        }

        private void uiReferenceInsp_EditValueChanged(object sender, EventArgs e)
        {
            DrawPlot();
        }

        private void btnSaveViewExtents_Click(object sender, EventArgs e)
        {
            YAxisSavedRange = new Range<double>(YAxis.ViewRange.Begin, YAxis.ViewRange.End);
        }

        public void UpdateDisplayUnits()
        {
            if (CurrentTube != null) DrawPlot();
            lciTotalShift.Text = string.Format("Total Shift ({0})", DefaultValues.MeasurementUnitSymbol);
        }

        private void chkShiftRef_CheckedChanged(object sender, EventArgs e)
        {
            ShiftText.EditValue = 0;
            if (CurrentTube != null)
                DrawPlot();
            btnApplyAll.Enabled = !chkShiftRef.Checked;
        }

        private void btnResetOneTube_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            bool applyAllStatus = btnApplyAll.Enabled;
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = false; Cursor = Cursors.WaitCursor;
            ResetTube(AdjustRefLine ? RefTube : CurrentTube);
            ShiftAdustSlider.EditValue = 0;
            UpdateShiftValues();
            DrawTubes();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            btnResetOneTube.Enabled = true; Cursor = Cursors.Default;
            btnResetAllTubes.Enabled = applyAllStatus; //separate because ref line adjustment may have this disable to start with.
        }

        private void btnResetAllTubes_Click(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = false; Cursor = Cursors.WaitCursor;
            Parallel.ForEach(CurrentTube.Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)), ResetTube);
            ShiftAdustSlider.EditValue = 0;
            UpdateShiftValues();
            DrawTubes();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            btnResetOneTube.Enabled = btnResetAllTubes.Enabled = true; Cursor = Cursors.Default;
        }
    }
}
