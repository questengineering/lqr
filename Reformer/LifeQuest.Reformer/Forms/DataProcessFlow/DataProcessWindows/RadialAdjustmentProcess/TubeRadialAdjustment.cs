﻿using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess
{
    public partial class TubeRadialAdjustment : BaseProcessWindow, IHasDisplayUnits
    {
        public TubeRadialAdjustment()
        {
            InitializeComponent();
        }

        public override void UpdateDataSource()
        {
            TheAdjustmentPlot.SetInspection(CurrentInspection);
        }

        public override void UpdateDisplayUnits()
        {
            TheAdjustmentPlot.UpdateDisplayUnits();
        }
    }
}
