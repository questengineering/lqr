﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess
{
    partial class TubeRadialAdjustment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TheAdjustmentPlot = new Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess.TubeRadialAdjustmentXyPlot();
            this.SuspendLayout();
            // 
            // TheAdjustmentPlot
            // 
            this.TheAdjustmentPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TheAdjustmentPlot.Location = new System.Drawing.Point(0, 0);
            this.TheAdjustmentPlot.Name = "TheAdjustmentPlot";
            this.TheAdjustmentPlot.Size = new System.Drawing.Size(1043, 426);
            this.TheAdjustmentPlot.TabIndex = 0;
            // 
            // TubeAxialAdjustment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TheAdjustmentPlot);
            this.Name = "TubeAxialAdjustment";
            this.Size = new System.Drawing.Size(1043, 426);
            this.ResumeLayout(false);

        }

        #endregion
        RadialAdjustmentProcess.TubeRadialAdjustmentXyPlot TheAdjustmentPlot;

    }
}
