﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess
{
    sealed partial class TubeRadialAdjustmentXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TubeRadialAdjustmentXyPlot));
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciTubeSelecter = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMissedEndText = new DevExpress.XtraLayout.LayoutControlItem();
            this.ShiftText = new DevExpress.XtraEditors.CalcEdit();
            this.ShiftAdustSlider = new DevExpress.XtraEditors.TrackBarControl();
            this.lciMissedEndAdjust = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDefaultNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiDefaultText = new DevExpress.XtraEditors.MemoEdit();
            this.btnApply = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnApplyAll = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiTotalShift = new DevExpress.XtraEditors.TextEdit();
            this.lciTotalShift = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiReferenceInsp = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciReferenceInsp = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiHints = new DevExpress.XtraEditors.LabelControl();
            this.lciHints = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnSaveViewExtents = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkShiftRef = new System.Windows.Forms.CheckBox();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnResetOneTube = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnResetAllTubes = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftAdustSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftAdustSlider.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTotalShift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReferenceInsp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReferenceInsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(803, 79);
            this.DataCursorView.Size = new System.Drawing.Size(190, 124);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnResetAllTubes);
            this.lcMain.Controls.Add(this.btnResetOneTube);
            this.lcMain.Controls.Add(this.chkShiftRef);
            this.lcMain.Controls.Add(this.btnSaveViewExtents);
            this.lcMain.Controls.Add(this.uiHints);
            this.lcMain.Controls.Add(this.uiReferenceInsp);
            this.lcMain.Controls.Add(this.uiTotalShift);
            this.lcMain.Controls.Add(this.btnApplyAll);
            this.lcMain.Controls.Add(this.btnApply);
            this.lcMain.Controls.Add(this.ShiftAdustSlider);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.Controls.Add(this.uiDefaultText);
            this.lcMain.Controls.Add(this.ShiftText);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.Size = new System.Drawing.Size(1000, 409);
            this.lcMain.Controls.SetChildIndex(this.ShiftText, 0);
            this.lcMain.Controls.SetChildIndex(this.uiDefaultText, 0);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.ShiftAdustSlider, 0);
            this.lcMain.Controls.SetChildIndex(this.btnApply, 0);
            this.lcMain.Controls.SetChildIndex(this.btnApplyAll, 0);
            this.lcMain.Controls.SetChildIndex(this.uiTotalShift, 0);
            this.lcMain.Controls.SetChildIndex(this.uiReferenceInsp, 0);
            this.lcMain.Controls.SetChildIndex(this.uiHints, 0);
            this.lcMain.Controls.SetChildIndex(this.btnSaveViewExtents, 0);
            this.lcMain.Controls.SetChildIndex(this.chkShiftRef, 0);
            this.lcMain.Controls.SetChildIndex(this.btnResetOneTube, 0);
            this.lcMain.Controls.SetChildIndex(this.btnResetAllTubes, 0);
            // 
            // PlotPanel
            // 
            this.PlotPanel.Size = new System.Drawing.Size(722, 395);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTubeSelecter,
            this.lciDefaultNote,
            this.lciTotalShift,
            this.lciMissedEndAdjust,
            this.lciMissedEndText,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.lciReferenceInsp,
            this.lciHints,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.lcgMain.Size = new System.Drawing.Size(1000, 409);
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(796, 72);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 128);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(791, 0);
            this.splitterItem1.Size = new System.Drawing.Size(5, 399);
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Size = new System.Drawing.Size(726, 399);
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(832, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DataSource = this.BSTube;
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.gridLookUpEdit1View;
            this.TubeSelecter.Size = new System.Drawing.Size(161, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 5;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.TubeSelector_EditValueChanged);
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // lciTubeSelecter
            // 
            this.lciTubeSelecter.Control = this.TubeSelecter;
            this.lciTubeSelecter.CustomizationFormText = "Tube";
            this.lciTubeSelecter.Location = new System.Drawing.Point(796, 0);
            this.lciTubeSelecter.Name = "lciTubeSelecter";
            this.lciTubeSelecter.Size = new System.Drawing.Size(194, 24);
            this.lciTubeSelecter.Text = "Tube";
            this.lciTubeSelecter.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciTubeSelecter.TextSize = new System.Drawing.Size(24, 13);
            this.lciTubeSelecter.TextToControlDistance = 5;
            // 
            // lciMissedEndText
            // 
            this.lciMissedEndText.Control = this.ShiftText;
            this.lciMissedEndText.CustomizationFormText = "Missed End";
            this.lciMissedEndText.Location = new System.Drawing.Point(726, 271);
            this.lciMissedEndText.Name = "lciMissedEndText";
            this.lciMissedEndText.Size = new System.Drawing.Size(65, 24);
            this.lciMissedEndText.Text = "Missed End";
            this.lciMissedEndText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMissedEndText.TextSize = new System.Drawing.Size(0, 0);
            this.lciMissedEndText.TextToControlDistance = 0;
            this.lciMissedEndText.TextVisible = false;
            // 
            // ShiftText
            // 
            this.ShiftText.Location = new System.Drawing.Point(733, 278);
            this.ShiftText.Name = "ShiftText";
            this.ShiftText.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ShiftText.Properties.DisplayFormat.FormatString = "N3";
            this.ShiftText.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ShiftText.Properties.EditFormat.FormatString = "N3";
            this.ShiftText.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ShiftText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.ShiftText.Size = new System.Drawing.Size(61, 20);
            this.ShiftText.StyleController = this.lcMain;
            this.ShiftText.TabIndex = 8;
            this.ShiftText.EditValueChanged += new System.EventHandler(this.ShiftValueChanged);
            // 
            // ShiftAdustSlider
            // 
            this.ShiftAdustSlider.Location = new System.Drawing.Point(733, 46);
            this.ShiftAdustSlider.Name = "ShiftAdustSlider";
            this.ShiftAdustSlider.Properties.HighlightSelectedRange = false;
            this.ShiftAdustSlider.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.ShiftAdustSlider.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ShiftAdustSlider.Properties.Maximum = 500;
            this.ShiftAdustSlider.Properties.Minimum = -500;
            this.ShiftAdustSlider.Properties.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.ShiftAdustSlider.Properties.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.ShiftAdustSlider.Size = new System.Drawing.Size(45, 228);
            this.ShiftAdustSlider.StyleController = this.lcMain;
            this.ShiftAdustSlider.TabIndex = 6;
            this.ShiftAdustSlider.EditValueChanged += new System.EventHandler(this.ShiftValueChanged);
            // 
            // lciMissedEndAdjust
            // 
            this.lciMissedEndAdjust.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciMissedEndAdjust.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciMissedEndAdjust.Control = this.ShiftAdustSlider;
            this.lciMissedEndAdjust.CustomizationFormText = "Shift";
            this.lciMissedEndAdjust.Location = new System.Drawing.Point(726, 26);
            this.lciMissedEndAdjust.MaxSize = new System.Drawing.Size(65, 0);
            this.lciMissedEndAdjust.MinSize = new System.Drawing.Size(65, 62);
            this.lciMissedEndAdjust.Name = "lciMissedEndAdjust";
            this.lciMissedEndAdjust.Size = new System.Drawing.Size(65, 245);
            this.lciMissedEndAdjust.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciMissedEndAdjust.Text = "Shift";
            this.lciMissedEndAdjust.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMissedEndAdjust.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciMissedEndAdjust.TextSize = new System.Drawing.Size(22, 13);
            this.lciMissedEndAdjust.TextToControlDistance = 0;
            // 
            // lciDefaultNote
            // 
            this.lciDefaultNote.Control = this.uiDefaultText;
            this.lciDefaultNote.CustomizationFormText = "lciDefaultNote";
            this.lciDefaultNote.Location = new System.Drawing.Point(796, 240);
            this.lciDefaultNote.Name = "lciDefaultNote";
            this.lciDefaultNote.Size = new System.Drawing.Size(194, 142);
            this.lciDefaultNote.Text = "lciDefaultNote";
            this.lciDefaultNote.TextSize = new System.Drawing.Size(0, 0);
            this.lciDefaultNote.TextToControlDistance = 0;
            this.lciDefaultNote.TextVisible = false;
            // 
            // uiDefaultText
            // 
            this.uiDefaultText.EditValue = "";
            this.uiDefaultText.Location = new System.Drawing.Point(803, 247);
            this.uiDefaultText.Name = "uiDefaultText";
            this.uiDefaultText.Properties.Appearance.Options.UseTextOptions = true;
            this.uiDefaultText.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.uiDefaultText.Size = new System.Drawing.Size(190, 138);
            this.uiDefaultText.StyleController = this.lcMain;
            this.uiDefaultText.TabIndex = 10;
            this.uiDefaultText.UseOptimizedRendering = true;
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(733, 302);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(61, 22);
            this.btnApply.StyleController = this.lcMain;
            this.btnApply.TabIndex = 11;
            this.btnApply.Text = "Apply";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnApply;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(726, 295);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // btnApplyAll
            // 
            this.btnApplyAll.Location = new System.Drawing.Point(733, 328);
            this.btnApplyAll.Name = "btnApplyAll";
            this.btnApplyAll.Size = new System.Drawing.Size(61, 22);
            this.btnApplyAll.StyleController = this.lcMain;
            this.btnApplyAll.TabIndex = 12;
            this.btnApplyAll.Text = "Apply All";
            this.btnApplyAll.Click += new System.EventHandler(this.btnApplyAll_Click);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnResetOneTube;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(726, 321);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // uiTotalShift
            // 
            this.uiTotalShift.Location = new System.Drawing.Point(803, 223);
            this.uiTotalShift.Name = "uiTotalShift";
            this.uiTotalShift.Properties.ReadOnly = true;
            this.uiTotalShift.Size = new System.Drawing.Size(190, 20);
            this.uiTotalShift.StyleController = this.lcMain;
            this.uiTotalShift.TabIndex = 13;
            // 
            // lciTotalShift
            // 
            this.lciTotalShift.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciTotalShift.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciTotalShift.Control = this.uiTotalShift;
            this.lciTotalShift.CustomizationFormText = "Total Start:";
            this.lciTotalShift.Location = new System.Drawing.Point(796, 200);
            this.lciTotalShift.Name = "lciTotalShift";
            this.lciTotalShift.Size = new System.Drawing.Size(194, 40);
            this.lciTotalShift.Text = "Total Shift (un):";
            this.lciTotalShift.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciTotalShift.TextSize = new System.Drawing.Size(76, 13);
            // 
            // uiReferenceInsp
            // 
            this.uiReferenceInsp.Location = new System.Drawing.Point(858, 31);
            this.uiReferenceInsp.Name = "uiReferenceInsp";
            this.uiReferenceInsp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiReferenceInsp.Properties.DisplayMember = "Name";
            this.uiReferenceInsp.Properties.NullText = "";
            this.uiReferenceInsp.Properties.View = this.gridView1;
            this.uiReferenceInsp.Size = new System.Drawing.Size(135, 20);
            this.uiReferenceInsp.StyleController = this.lcMain;
            this.uiReferenceInsp.TabIndex = 14;
            this.uiReferenceInsp.EditValueChanged += new System.EventHandler(this.uiReferenceInsp_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colInspName
            // 
            this.colInspName.AppearanceCell.Options.UseTextOptions = true;
            this.colInspName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInspName.AppearanceHeader.Options.UseTextOptions = true;
            this.colInspName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInspName.Caption = "Inspection Name";
            this.colInspName.FieldName = "Name";
            this.colInspName.Name = "colInspName";
            this.colInspName.Visible = true;
            this.colInspName.VisibleIndex = 0;
            // 
            // lciReferenceInsp
            // 
            this.lciReferenceInsp.Control = this.uiReferenceInsp;
            this.lciReferenceInsp.CustomizationFormText = "Reference";
            this.lciReferenceInsp.Location = new System.Drawing.Point(796, 24);
            this.lciReferenceInsp.Name = "lciReferenceInsp";
            this.lciReferenceInsp.OptionsToolTip.ToolTip = "Allows a reference tube from another inspection to be placed on the plot.";
            this.lciReferenceInsp.Size = new System.Drawing.Size(194, 24);
            this.lciReferenceInsp.Text = "Reference";
            this.lciReferenceInsp.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciReferenceInsp.TextSize = new System.Drawing.Size(50, 13);
            this.lciReferenceInsp.TextToControlDistance = 5;
            // 
            // uiHints
            // 
            this.uiHints.Location = new System.Drawing.Point(803, 389);
            this.uiHints.Name = "uiHints";
            this.uiHints.Size = new System.Drawing.Size(185, 13);
            this.uiHints.StyleController = this.lcMain;
            this.uiHints.TabIndex = 15;
            this.uiHints.Text = "Q / E move to next and previous tube.";
            // 
            // lciHints
            // 
            this.lciHints.Control = this.uiHints;
            this.lciHints.CustomizationFormText = "lciHints";
            this.lciHints.Location = new System.Drawing.Point(796, 382);
            this.lciHints.Name = "lciHints";
            this.lciHints.Size = new System.Drawing.Size(194, 17);
            this.lciHints.Text = "lciHints";
            this.lciHints.TextSize = new System.Drawing.Size(0, 0);
            this.lciHints.TextToControlDistance = 0;
            this.lciHints.TextVisible = false;
            // 
            // btnSaveViewExtents
            // 
            this.btnSaveViewExtents.Location = new System.Drawing.Point(733, 7);
            this.btnSaveViewExtents.Name = "btnSaveViewExtents";
            this.btnSaveViewExtents.Size = new System.Drawing.Size(61, 22);
            this.btnSaveViewExtents.StyleController = this.lcMain;
            this.btnSaveViewExtents.TabIndex = 16;
            this.btnSaveViewExtents.Text = "Save View";
            this.btnSaveViewExtents.ToolTip = "Saves the current center and Y Axis view extents so that all tubes will share it " +
    "when analysing.";
            this.btnSaveViewExtents.Click += new System.EventHandler(this.btnSaveViewExtents_Click);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnSaveViewExtents;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(726, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // chkShiftRef
            // 
            this.chkShiftRef.Location = new System.Drawing.Point(803, 55);
            this.chkShiftRef.Name = "chkShiftRef";
            this.chkShiftRef.Size = new System.Drawing.Size(190, 20);
            this.chkShiftRef.TabIndex = 17;
            this.chkShiftRef.Text = "Adjust Reference Line";
            this.chkShiftRef.UseVisualStyleBackColor = true;
            this.chkShiftRef.CheckedChanged += new System.EventHandler(this.chkShiftRef_CheckedChanged);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkShiftRef;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(796, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // btnResetOneTube
            // 
            this.btnResetOneTube.Location = new System.Drawing.Point(733, 354);
            this.btnResetOneTube.Name = "btnResetOneTube";
            this.btnResetOneTube.Size = new System.Drawing.Size(61, 22);
            this.btnResetOneTube.StyleController = this.lcMain;
            this.btnResetOneTube.TabIndex = 18;
            this.btnResetOneTube.Text = "Reset";
            this.btnResetOneTube.Click += new System.EventHandler(this.btnResetOneTube_Click);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnApplyAll;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(726, 347);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // btnResetAllTubes
            // 
            this.btnResetAllTubes.Location = new System.Drawing.Point(733, 380);
            this.btnResetAllTubes.Name = "btnResetAllTubes";
            this.btnResetAllTubes.Size = new System.Drawing.Size(61, 22);
            this.btnResetAllTubes.StyleController = this.lcMain;
            this.btnResetAllTubes.TabIndex = 19;
            this.btnResetAllTubes.Text = "Reset All";
            this.btnResetAllTubes.Click += new System.EventHandler(this.btnResetAllTubes_Click);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnResetAllTubes;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(726, 373);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // TubeRadialAdjustmentXyPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "TubeRadialAdjustmentXyPlot";
            this.Size = new System.Drawing.Size(1000, 409);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftAdustSlider.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftAdustSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMissedEndAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDefaultNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDefaultText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTotalShift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTotalShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiReferenceInsp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReferenceInsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciHints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSelecter;
        private DevExpress.XtraEditors.TrackBarControl ShiftAdustSlider;
        private DevExpress.XtraLayout.LayoutControlItem lciMissedEndText;
        private DevExpress.XtraLayout.LayoutControlItem lciMissedEndAdjust;
        private DevExpress.XtraEditors.MemoEdit uiDefaultText;
        private DevExpress.XtraLayout.LayoutControlItem lciDefaultNote;
        private DevExpress.XtraEditors.SimpleButton btnApplyAll;
        private DevExpress.XtraEditors.SimpleButton btnApply;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit uiTotalShift;
        private DevExpress.XtraLayout.LayoutControlItem lciTotalShift;
        private DevExpress.XtraEditors.CalcEdit ShiftText;
        private DevExpress.XtraEditors.GridLookUpEdit uiReferenceInsp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem lciReferenceInsp;
        private DevExpress.XtraGrid.Columns.GridColumn colInspName;
        private DevExpress.XtraEditors.LabelControl uiHints;
        private DevExpress.XtraLayout.LayoutControlItem lciHints;
        private DevExpress.XtraEditors.SimpleButton btnSaveViewExtents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.CheckBox chkShiftRef;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnResetAllTubes;
        private DevExpress.XtraEditors.SimpleButton btnResetOneTube;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}
