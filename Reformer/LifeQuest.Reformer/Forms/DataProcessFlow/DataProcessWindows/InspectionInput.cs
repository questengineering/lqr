﻿using System;
using System.Linq;
using NGenerics.Extensions;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.LifeQuest.Common.Engineering;
using Reformer.Data;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.Materials;
using Reformer.Data.Scalars;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class InspectionInput : BaseProcessWindow
    {
        public InspectionInput()
        {
            InitializeComponent();
            UpdateDataSource();
            UpdateDisplayUnits();
        }

        public sealed override void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            SetControlDataSources();
            SetInputSelections();
        }

        private void SetInputSelections()
        {
            uiInspectionTool.Properties.Items.Clear();
            uiInspectionTool.Properties.Items.AddRange(Enum.GetValues(typeof(Tool)));

            if (CurrentInspection != null)
            {
                uiInspectionTool.SelectedItem = CurrentInspection.InspectionTool;
            }

            uiTubeMaterial.Properties.Items.Clear();
            uiTubeMaterial.Properties.Items.AddRange(ReformerMaterialsList.Instance);

            if (CurrentInspection != null)
            {
                uiNewTubes.Checked = CurrentInspection.ReformerInfo.NewTubes;
            }

            uiDefaultIDTolMin.EditValue = DefaultValues.TubeTolerances.IDTolDown;
            uiDefaultIDTolMax.EditValue = DefaultValues.TubeTolerances.IDTolUp;
            uiDefaultODTolMin.EditValue = DefaultValues.TubeTolerances.ODTolDown;
            uiDefaultODTolMax.EditValue = DefaultValues.TubeTolerances.ODTolUp;

        }

        private void SetControlDataSources()
        {
            if (CurrentInspection != null)
            {
                BSInspFile.DataSource = CurrentInspection;
                BSInspSetup.DataSource = CurrentInspection.ReformerInfo;
            }

            if (CurrentProject != null)
            {
                BSProject.DataSource = CurrentProject;
            }
        }

        public override void UpdateDisplayUnits()
        {
            lciDefaultOD.Text = $"Nominal OD ({DefaultValues.MeasurementUnitSymbol}):";
            lciDefaultID.Text = $"Nominal ID ({DefaultValues.MeasurementUnitSymbol}):";

            lciDefaultODTolMax.Text = $"OD Tol max ({DefaultValues.MeasurementUnitSymbol}):";
            lciDefaultIDTolMin.Text = $"ID Tol min ({DefaultValues.MeasurementUnitSymbol}):";
            lciDefaultODTolMin.Text = $"OD Tol min ({DefaultValues.MeasurementUnitSymbol}):";
            lciDefaultIDTolMax.Text = $"ID Tol max ({DefaultValues.MeasurementUnitSymbol}):";
            
            BSInspSetup.ResetBindings(false); //triger the data to update and match the display units.
        }

        private void DefaultEditValueChanged(object sender, EventArgs e)
        {
            if (CurrentInspection == null)
            {
                return;
            }

            Set3DScales();
        }

        private void Set3DScales()
        {
            CurrentProject.ColorScaleBasis = new PipeSize(uiDefaultID.Value, (uiDefaultOD.Value - uiDefaultID.Value) / 2);
            var orScale = CurrentProject.ColorScale.MinMaxRanges.First(S => S.ScalarName == ReformerScalar.RadiusOutside.ToString());
            var irScale = CurrentProject.ColorScale.MinMaxRanges.First(S => S.ScalarName == ReformerScalar.RadiusInside.ToString());
            orScale.Min = DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusOutside].Min * CurrentInspection.ReformerInfo.DefaultOD / 2;
            orScale.Max = DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusOutside].Max * CurrentInspection.ReformerInfo.DefaultOD / 2;
            irScale.Min = DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusInside].Min * CurrentInspection.ReformerInfo.DefaultID / 2;
            irScale.Max = DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusInside].Max * CurrentInspection.ReformerInfo.DefaultID / 2;
        }

        //Makes sure the tubes stay in sync with changing reformer materials.
        private void uiTubeMaterial_EditValueChanged(object sender, EventArgs e)
        {
            if (!(uiTubeMaterial.EditValue is ReformerMaterial) && CurrentInspection == null)
            {
                return;
            }

            CurrentInspection.Tubes.ForEach(T => T.ReformerAttributes.Material = (ReformerMaterial)uiTubeMaterial.EditValue);
        }

        /// <summary> Set values immediately so the process flow list updates along with it instead of waiting for focus to move. Databinding was not behaving well. </summary>
        private void ValidateImmediately(object sender, EventArgs e)
        {
            if (sender == uiNewTubes && CurrentInspection.ReformerInfo.NewTubes != uiNewTubes.Checked)
            {
                CurrentInspection.ReformerInfo.NewTubes = uiNewTubes.Checked;
                ProcessFlowList.SetProcessFlowList(CurrentInspection);
            }

            if (sender == uiInspectionTool && CurrentInspection.InspectionTool != (Tool)uiInspectionTool.EditValue)
            {
                CurrentInspection.InspectionTool = (Tool)uiInspectionTool.EditValue;
                ProcessFlowList.SetProcessFlowList(CurrentInspection);
            }

        }

        private void lnkSetOD_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes == null)
            {
                return;
            }

            if (uiDefaultODTolMax.EditValue != null)
            {
                CurrentInspection.Tubes.ForEach(T => T.Specs.ODTolUpDisplayUnits = (float) uiDefaultODTolMax.EditValue);
            }

            if (uiDefaultODTolMin.EditValue != null)
            {
                CurrentInspection.Tubes.ForEach(T => T.Specs.ODTolDownDisplayUnits = (float) uiDefaultODTolMin.EditValue);
            }

            OnProgressChanged(100, "Set all tube's OD Tolerances");
        }

        private void lnkSetID_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes == null)
            {
                return;
            }

            if (uiDefaultIDTolMax.EditValue != null)
            {
                CurrentInspection.Tubes.ForEach(T => T.Specs.IDTolUpDisplayUnits = (float)uiDefaultIDTolMax.EditValue);
            }

            if (uiDefaultIDTolMin.EditValue != null)
            {
                CurrentInspection.Tubes.ForEach(T => T.Specs.IDTolDownDisplayUnits = (float)uiDefaultIDTolMin.EditValue);
            }

            OnProgressChanged(100, "Set all tube's ID Tolerances");
        }

        private void lnkSetAge_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes == null)
            {
                return;
            }

            CurrentInspection.Tubes.ForEach(T => T.Specs.YearsSinceLastInspection = uiDefaultTubeAge.Value);
            CurrentInspection.Tubes.ForEach(T => T.Specs.YearsUntilNextInspection = uiDefaultNextInspectionYears.Value.ToInt32());
            OnProgressChanged(100, "Set all tube's Age and Next Inspection");
        }
        
    }
}
