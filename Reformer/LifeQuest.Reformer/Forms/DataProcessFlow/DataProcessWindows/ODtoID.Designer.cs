﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    sealed partial class ODtoID
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ODtoID));
            this.lcBackDrop = new DevExpress.XtraLayout.LayoutControl();
            this.lblNote = new DevExpress.XtraEditors.LabelControl();
            this.gridValues = new DevExpress.XtraGrid.GridControl();
            this.viewValues = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOxidationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDGrowth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDGrowthRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiCurrentLength = new DevExpress.XtraEditors.CalcEdit();
            this.uiDesignedLength = new DevExpress.XtraEditors.CalcEdit();
            this.uiWT = new DevExpress.XtraEditors.CalcEdit();
            this.btnGo = new DevExpress.XtraEditors.SimpleButton();
            this.uiTubeSelector = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.BSReformerTube = new System.Windows.Forms.BindingSource(this.components);
            this.uiCalcMethod = new DevExpress.XtraEditors.RadioGroup();
            this.uiOxRate = new DevExpress.XtraEditors.TextEdit();
            this.lcgBackDrop = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTubeSelector = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciCalcMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgSimpleOptions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciWT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciOxRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCurrentLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDesignedLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGoButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.colNomID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiEquation = new System.Windows.Forms.PictureBox();
            this.lciEquation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcBackDrop)).BeginInit();
            this.lcBackDrop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCurrentLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiWT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCalcMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiOxRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBackDrop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalcMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSimpleOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOxRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCurrentLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDesignedLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGoButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiEquation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEquation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // lcBackDrop
            // 
            this.lcBackDrop.Controls.Add(this.uiEquation);
            this.lcBackDrop.Controls.Add(this.lblNote);
            this.lcBackDrop.Controls.Add(this.gridValues);
            this.lcBackDrop.Controls.Add(this.uiCurrentLength);
            this.lcBackDrop.Controls.Add(this.uiDesignedLength);
            this.lcBackDrop.Controls.Add(this.uiWT);
            this.lcBackDrop.Controls.Add(this.btnGo);
            this.lcBackDrop.Controls.Add(this.uiTubeSelector);
            this.lcBackDrop.Controls.Add(this.uiCalcMethod);
            this.lcBackDrop.Controls.Add(this.uiOxRate);
            this.lcBackDrop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcBackDrop.Location = new System.Drawing.Point(0, 0);
            this.lcBackDrop.Name = "lcBackDrop";
            this.lcBackDrop.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1715, 217, 383, 350);
            this.lcBackDrop.Root = this.lcgBackDrop;
            this.lcBackDrop.Size = new System.Drawing.Size(845, 521);
            this.lcBackDrop.TabIndex = 2;
            this.lcBackDrop.Text = "layoutControl1";
            // 
            // lblNote
            // 
            this.lblNote.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblNote.Location = new System.Drawing.Point(2, 259);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(204, 39);
            this.lblNote.StyleController = this.lcBackDrop;
            this.lblNote.TabIndex = 21;
            this.lblNote.Text = "Note: ID Growth is converted RefID vs MaxID and Years Since Last Inspection is li" +
    "mited to 6 years.";
            // 
            // gridValues
            // 
            this.gridValues.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            this.gridValues.Location = new System.Drawing.Point(210, 2);
            this.gridValues.MainView = this.viewValues;
            this.gridValues.Name = "gridValues";
            this.gridValues.Size = new System.Drawing.Size(633, 475);
            this.gridValues.TabIndex = 20;
            this.gridValues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewValues});
            // 
            // viewValues
            // 
            this.viewValues.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colAge,
            this.colOxidationRate,
            this.colNomOD,
            this.colNomID,
            this.colMaxOD,
            this.colRefID,
            this.colMaxID,
            this.colIDGrowth,
            this.colIDGrowthRate});
            this.viewValues.CustomizationFormBounds = new System.Drawing.Rectangle(913, 470, 210, 172);
            this.viewValues.GridControl = this.gridValues;
            this.viewValues.Name = "viewValues";
            this.viewValues.OptionsDetail.EnableMasterViewMode = false;
            this.viewValues.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colAge
            // 
            this.colAge.AppearanceCell.Options.UseTextOptions = true;
            this.colAge.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAge.AppearanceHeader.Options.UseTextOptions = true;
            this.colAge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAge.Caption = "Yrs Since Last Inspection";
            this.colAge.DisplayFormat.FormatString = "F2";
            this.colAge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAge.FieldName = "Specs.YearsSinceLastInspection";
            this.colAge.Name = "colAge";
            this.colAge.OptionsColumn.AllowEdit = false;
            this.colAge.OptionsColumn.ReadOnly = true;
            this.colAge.Visible = true;
            this.colAge.VisibleIndex = 4;
            // 
            // colOxidationRate
            // 
            this.colOxidationRate.AppearanceCell.Options.UseTextOptions = true;
            this.colOxidationRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOxidationRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOxidationRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOxidationRate.Caption = "Ox Rate";
            this.colOxidationRate.DisplayFormat.FormatString = "F6";
            this.colOxidationRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOxidationRate.FieldName = "ReformerAttributes.OxidationRateUsedDisplayUnits";
            this.colOxidationRate.Name = "colOxidationRate";
            this.colOxidationRate.OptionsColumn.AllowEdit = false;
            this.colOxidationRate.OptionsColumn.ReadOnly = true;
            this.colOxidationRate.Visible = true;
            this.colOxidationRate.VisibleIndex = 3;
            // 
            // colNomOD
            // 
            this.colNomOD.AppearanceCell.Options.UseTextOptions = true;
            this.colNomOD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomOD.AppearanceHeader.Options.UseTextOptions = true;
            this.colNomOD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomOD.Caption = "Nom OD";
            this.colNomOD.DisplayFormat.FormatString = "F2";
            this.colNomOD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNomOD.FieldName = "Specs.DiameterOutsideInDisplayUnits";
            this.colNomOD.Name = "colNomOD";
            this.colNomOD.OptionsColumn.AllowEdit = false;
            this.colNomOD.OptionsColumn.ReadOnly = true;
            this.colNomOD.Visible = true;
            this.colNomOD.VisibleIndex = 5;
            // 
            // colMaxOD
            // 
            this.colMaxOD.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxOD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxOD.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxOD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxOD.Caption = "Max OD";
            this.colMaxOD.DisplayFormat.FormatString = "F2";
            this.colMaxOD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxOD.FieldName = "ReformerAttributes.PickedLevel1MaxODDisplayUnits";
            this.colMaxOD.Name = "colMaxOD";
            this.colMaxOD.OptionsColumn.AllowEdit = false;
            this.colMaxOD.OptionsColumn.ReadOnly = true;
            this.colMaxOD.Visible = true;
            this.colMaxOD.VisibleIndex = 2;
            // 
            // colRefID
            // 
            this.colRefID.AppearanceCell.Options.UseTextOptions = true;
            this.colRefID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefID.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefID.Caption = "Ref ID";
            this.colRefID.DisplayFormat.FormatString = "F2";
            this.colRefID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRefID.FieldName = "ReformerAttributes.PickedLevel1RefIDDisplayUnits";
            this.colRefID.Name = "colRefID";
            this.colRefID.OptionsColumn.AllowEdit = false;
            this.colRefID.OptionsColumn.ReadOnly = true;
            this.colRefID.Visible = true;
            this.colRefID.VisibleIndex = 7;
            // 
            // colMaxID
            // 
            this.colMaxID.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxID.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxID.Caption = "Max ID";
            this.colMaxID.DisplayFormat.FormatString = "F2";
            this.colMaxID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxID.FieldName = "ReformerAttributes.PickedLevel1MaxIDDisplayUnits";
            this.colMaxID.Name = "colMaxID";
            this.colMaxID.OptionsColumn.AllowEdit = false;
            this.colMaxID.OptionsColumn.ReadOnly = true;
            this.colMaxID.Visible = true;
            this.colMaxID.VisibleIndex = 1;
            // 
            // colIDGrowth
            // 
            this.colIDGrowth.AppearanceCell.Options.UseTextOptions = true;
            this.colIDGrowth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowth.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDGrowth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowth.Caption = "ID Growth";
            this.colIDGrowth.DisplayFormat.FormatString = "P2";
            this.colIDGrowth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIDGrowth.FieldName = "ReformerAttributes.Level1IDGrowth";
            this.colIDGrowth.Name = "colIDGrowth";
            this.colIDGrowth.OptionsColumn.AllowEdit = false;
            this.colIDGrowth.OptionsColumn.ReadOnly = true;
            this.colIDGrowth.Visible = true;
            this.colIDGrowth.VisibleIndex = 8;
            // 
            // colIDGrowthRate
            // 
            this.colIDGrowthRate.AppearanceCell.Options.UseTextOptions = true;
            this.colIDGrowthRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowthRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDGrowthRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowthRate.Caption = "Growth Rate";
            this.colIDGrowthRate.DisplayFormat.FormatString = "P2";
            this.colIDGrowthRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIDGrowthRate.FieldName = "ReformerAttributes.Level1GrowthRate";
            this.colIDGrowthRate.Name = "colIDGrowthRate";
            this.colIDGrowthRate.OptionsColumn.AllowEdit = false;
            this.colIDGrowthRate.OptionsColumn.ReadOnly = true;
            this.colIDGrowthRate.Visible = true;
            this.colIDGrowthRate.VisibleIndex = 9;
            // 
            // uiCurrentLength
            // 
            this.uiCurrentLength.Location = new System.Drawing.Point(99, 182);
            this.uiCurrentLength.Name = "uiCurrentLength";
            this.uiCurrentLength.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiCurrentLength.Properties.DisplayFormat.FormatString = "N3";
            this.uiCurrentLength.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiCurrentLength.Properties.EditFormat.FormatString = "N3";
            this.uiCurrentLength.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiCurrentLength.Properties.Mask.EditMask = "N3";
            this.uiCurrentLength.Size = new System.Drawing.Size(104, 20);
            this.uiCurrentLength.StyleController = this.lcBackDrop;
            this.uiCurrentLength.TabIndex = 16;
            // 
            // uiDesignedLength
            // 
            this.uiDesignedLength.Location = new System.Drawing.Point(99, 158);
            this.uiDesignedLength.Name = "uiDesignedLength";
            this.uiDesignedLength.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDesignedLength.Properties.DisplayFormat.FormatString = "N3";
            this.uiDesignedLength.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedLength.Properties.EditFormat.FormatString = "N3";
            this.uiDesignedLength.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedLength.Properties.Mask.EditMask = "N3";
            this.uiDesignedLength.Size = new System.Drawing.Size(104, 20);
            this.uiDesignedLength.StyleController = this.lcBackDrop;
            this.uiDesignedLength.TabIndex = 15;
            // 
            // uiWT
            // 
            this.uiWT.Location = new System.Drawing.Point(99, 110);
            this.uiWT.Name = "uiWT";
            this.uiWT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiWT.Properties.DisplayFormat.FormatString = "N3";
            this.uiWT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiWT.Properties.EditFormat.FormatString = "N3";
            this.uiWT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiWT.Properties.Mask.EditMask = "N3";
            this.uiWT.Size = new System.Drawing.Size(104, 20);
            this.uiWT.StyleController = this.lcBackDrop;
            this.uiWT.TabIndex = 12;
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(141, 233);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(65, 22);
            this.btnGo.StyleController = this.lcBackDrop;
            this.btnGo.TabIndex = 11;
            this.btnGo.Text = "Convert";
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // uiTubeSelector
            // 
            this.uiTubeSelector.Location = new System.Drawing.Point(2, 18);
            this.uiTubeSelector.Name = "uiTubeSelector";
            this.uiTubeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTubeSelector.Properties.DataSource = this.BSReformerTube;
            this.uiTubeSelector.Properties.DisplayMember = "Name";
            this.uiTubeSelector.Properties.ValidateOnEnterKey = true;
            this.uiTubeSelector.Properties.ValueMember = "ID";
            this.uiTubeSelector.Size = new System.Drawing.Size(204, 20);
            this.uiTubeSelector.StyleController = this.lcBackDrop;
            this.uiTubeSelector.TabIndex = 10;
            this.uiTubeSelector.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.uiTubeSelector_Closed);
            this.uiTubeSelector.Validated += new System.EventHandler(this.uiTubeSelector_Validated);
            // 
            // BSReformerTube
            // 
            this.BSReformerTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // uiCalcMethod
            // 
            this.uiCalcMethod.Location = new System.Drawing.Point(2, 58);
            this.uiCalcMethod.Name = "uiCalcMethod";
            this.uiCalcMethod.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Simple", "Simple"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("ConstantVolume", "Constant Volume")});
            this.uiCalcMethod.Size = new System.Drawing.Size(204, 27);
            this.uiCalcMethod.StyleController = this.lcBackDrop;
            this.uiCalcMethod.TabIndex = 6;
            // 
            // uiOxRate
            // 
            this.uiOxRate.EditValue = "0.000000";
            this.uiOxRate.Location = new System.Drawing.Point(99, 206);
            this.uiOxRate.Name = "uiOxRate";
            this.uiOxRate.Properties.Mask.EditMask = "N6";
            this.uiOxRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uiOxRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uiOxRate.Size = new System.Drawing.Size(104, 20);
            this.uiOxRate.StyleController = this.lcBackDrop;
            this.uiOxRate.TabIndex = 17;
            // 
            // lcgBackDrop
            // 
            this.lcgBackDrop.CustomizationFormText = "Root";
            this.lcgBackDrop.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.lcgBackDrop.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgBackDrop.GroupBordersVisible = false;
            this.lcgBackDrop.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTubeSelector,
            this.emptySpaceItem1,
            this.lciCalcMethod,
            this.lcgSimpleOptions,
            this.layoutControlGroup1,
            this.lciGoButton,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.lciNote,
            this.lciEquation,
            this.emptySpaceItem3});
            this.lcgBackDrop.Name = "Root";
            this.lcgBackDrop.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgBackDrop.Size = new System.Drawing.Size(845, 521);
            this.lcgBackDrop.TextVisible = false;
            // 
            // lciTubeSelector
            // 
            this.lciTubeSelector.Control = this.uiTubeSelector;
            this.lciTubeSelector.CustomizationFormText = "Tubes";
            this.lciTubeSelector.Location = new System.Drawing.Point(0, 0);
            this.lciTubeSelector.MaxSize = new System.Drawing.Size(208, 40);
            this.lciTubeSelector.MinSize = new System.Drawing.Size(208, 40);
            this.lciTubeSelector.Name = "lciTubeSelector";
            this.lciTubeSelector.Size = new System.Drawing.Size(208, 40);
            this.lciTubeSelector.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciTubeSelector.Text = "Tubes";
            this.lciTubeSelector.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciTubeSelector.TextSize = new System.Drawing.Size(91, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 300);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(208, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(208, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(208, 221);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciCalcMethod
            // 
            this.lciCalcMethod.Control = this.uiCalcMethod;
            this.lciCalcMethod.CustomizationFormText = "Calculation Method";
            this.lciCalcMethod.Location = new System.Drawing.Point(0, 40);
            this.lciCalcMethod.MaxSize = new System.Drawing.Size(208, 47);
            this.lciCalcMethod.MinSize = new System.Drawing.Size(208, 47);
            this.lciCalcMethod.Name = "lciCalcMethod";
            this.lciCalcMethod.Size = new System.Drawing.Size(208, 47);
            this.lciCalcMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCalcMethod.Text = "Calculation Method";
            this.lciCalcMethod.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciCalcMethod.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lcgSimpleOptions
            // 
            this.lcgSimpleOptions.CustomizationFormText = "layoutControlGroup1";
            this.lcgSimpleOptions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciWT});
            this.lcgSimpleOptions.Location = new System.Drawing.Point(0, 87);
            this.lcgSimpleOptions.Name = "lcgSimpleOptions";
            this.lcgSimpleOptions.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgSimpleOptions.Size = new System.Drawing.Size(208, 48);
            this.lcgSimpleOptions.Text = "Simple Options";
            // 
            // lciWT
            // 
            this.lciWT.Control = this.uiWT;
            this.lciWT.CustomizationFormText = "layoutControlItem1";
            this.lciWT.Location = new System.Drawing.Point(0, 0);
            this.lciWT.Name = "lciWT";
            this.lciWT.Size = new System.Drawing.Size(202, 24);
            this.lciWT.Text = "Wall Thickness:";
            this.lciWT.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Constant Volume Options";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciOxRate,
            this.lciCurrentLength,
            this.lciDesignedLength});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 135);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(208, 96);
            this.layoutControlGroup1.Text = "Optional Constant Volume Options";
            // 
            // lciOxRate
            // 
            this.lciOxRate.Control = this.uiOxRate;
            this.lciOxRate.CustomizationFormText = "Oxidation Rate:";
            this.lciOxRate.Location = new System.Drawing.Point(0, 48);
            this.lciOxRate.Name = "lciOxRate";
            this.lciOxRate.Size = new System.Drawing.Size(202, 24);
            this.lciOxRate.Text = "Oxidation Rate:";
            this.lciOxRate.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lciCurrentLength
            // 
            this.lciCurrentLength.Control = this.uiCurrentLength;
            this.lciCurrentLength.CustomizationFormText = "Current Length:";
            this.lciCurrentLength.Location = new System.Drawing.Point(0, 24);
            this.lciCurrentLength.Name = "lciCurrentLength";
            this.lciCurrentLength.Size = new System.Drawing.Size(202, 24);
            this.lciCurrentLength.Text = "Current Length:";
            this.lciCurrentLength.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lciDesignedLength
            // 
            this.lciDesignedLength.Control = this.uiDesignedLength;
            this.lciDesignedLength.CustomizationFormText = "Designed Length:";
            this.lciDesignedLength.Location = new System.Drawing.Point(0, 0);
            this.lciDesignedLength.Name = "lciDesignedLength";
            this.lciDesignedLength.Size = new System.Drawing.Size(202, 24);
            this.lciDesignedLength.Text = "Designed Length:";
            this.lciDesignedLength.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lciGoButton
            // 
            this.lciGoButton.Control = this.btnGo;
            this.lciGoButton.CustomizationFormText = "lciGoButton";
            this.lciGoButton.Location = new System.Drawing.Point(139, 231);
            this.lciGoButton.MaxSize = new System.Drawing.Size(69, 26);
            this.lciGoButton.MinSize = new System.Drawing.Size(69, 26);
            this.lciGoButton.Name = "lciGoButton";
            this.lciGoButton.Size = new System.Drawing.Size(69, 26);
            this.lciGoButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciGoButton.TextSize = new System.Drawing.Size(0, 0);
            this.lciGoButton.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 231);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(139, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridValues;
            this.layoutControlItem1.Location = new System.Drawing.Point(208, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(637, 479);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciNote
            // 
            this.lciNote.Control = this.lblNote;
            this.lciNote.Location = new System.Drawing.Point(0, 257);
            this.lciNote.Name = "lciNote";
            this.lciNote.Size = new System.Drawing.Size(208, 43);
            this.lciNote.TextSize = new System.Drawing.Size(0, 0);
            this.lciNote.TextVisible = false;
            // 
            // colNomID
            // 
            this.colNomID.AppearanceCell.Options.UseTextOptions = true;
            this.colNomID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomID.AppearanceHeader.Options.UseTextOptions = true;
            this.colNomID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomID.Caption = "Nom ID";
            this.colNomID.FieldName = "Specs.DiameterInsideInDisplayUnits";
            this.colNomID.Name = "colNomID";
            this.colNomID.OptionsColumn.AllowEdit = false;
            this.colNomID.OptionsColumn.ReadOnly = true;
            this.colNomID.Visible = true;
            this.colNomID.VisibleIndex = 6;
            // 
            // uiEquation
            // 
            this.uiEquation.Image = ((System.Drawing.Image)(resources.GetObject("uiEquation.Image")));
            this.uiEquation.Location = new System.Drawing.Point(210, 481);
            this.uiEquation.Name = "uiEquation";
            this.uiEquation.Size = new System.Drawing.Size(359, 38);
            this.uiEquation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.uiEquation.TabIndex = 22;
            this.uiEquation.TabStop = false;
            // 
            // lciEquation
            // 
            this.lciEquation.Control = this.uiEquation;
            this.lciEquation.Location = new System.Drawing.Point(208, 479);
            this.lciEquation.MaxSize = new System.Drawing.Size(363, 42);
            this.lciEquation.MinSize = new System.Drawing.Size(363, 42);
            this.lciEquation.Name = "lciEquation";
            this.lciEquation.Size = new System.Drawing.Size(363, 42);
            this.lciEquation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciEquation.TextSize = new System.Drawing.Size(0, 0);
            this.lciEquation.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(571, 479);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(274, 42);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ODtoID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.lcBackDrop);
            this.Name = "ODtoID";
            this.Size = new System.Drawing.Size(845, 521);
            ((System.ComponentModel.ISupportInitialize)(this.lcBackDrop)).EndInit();
            this.lcBackDrop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCurrentLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiWT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTubeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCalcMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiOxRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgBackDrop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCalcMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSimpleOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOxRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCurrentLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDesignedLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGoButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiEquation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEquation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcBackDrop;
        private DevExpress.XtraEditors.RadioGroup uiCalcMethod;
        private DevExpress.XtraLayout.LayoutControlGroup lcgBackDrop;
        private DevExpress.XtraLayout.LayoutControlItem lciCalcMethod;
        private DevExpress.XtraEditors.CheckedComboBoxEdit uiTubeSelector;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeSelector;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnGo;
        private DevExpress.XtraLayout.LayoutControlItem lciGoButton;
        private System.Windows.Forms.BindingSource BSReformerTube;
        private DevExpress.XtraEditors.CalcEdit uiWT;
        private DevExpress.XtraLayout.LayoutControlGroup lcgSimpleOptions;
        private DevExpress.XtraLayout.LayoutControlItem lciWT;
        private DevExpress.XtraEditors.CalcEdit uiDesignedLength;
        private DevExpress.XtraLayout.LayoutControlItem lciDesignedLength;
        private DevExpress.XtraEditors.CalcEdit uiCurrentLength;
        private DevExpress.XtraLayout.LayoutControlItem lciCurrentLength;
        private DevExpress.XtraLayout.LayoutControlItem lciOxRate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit uiOxRate;
        private DevExpress.XtraGrid.GridControl gridValues;
        private DevExpress.XtraGrid.Views.Grid.GridView viewValues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colAge;
        private DevExpress.XtraGrid.Columns.GridColumn colOxidationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colNomOD;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxOD;
        private DevExpress.XtraGrid.Columns.GridColumn colRefID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colIDGrowth;
        private DevExpress.XtraGrid.Columns.GridColumn colIDGrowthRate;
        private DevExpress.XtraEditors.LabelControl lblNote;
        private DevExpress.XtraLayout.LayoutControlItem lciNote;
        private System.Windows.Forms.PictureBox uiEquation;
        private DevExpress.XtraGrid.Columns.GridColumn colNomID;
        private DevExpress.XtraLayout.LayoutControlItem lciEquation;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}
