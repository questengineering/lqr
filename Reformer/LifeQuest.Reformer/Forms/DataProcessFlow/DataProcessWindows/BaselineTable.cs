﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Microsoft.Office.Interop.Excel;
using QuestIntegrity.Core.Extensions.Strings;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Action = System.Action;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public sealed partial class BaselineTable : BaseProcessWindow
    {
        private const string sortedByRowNumber = "Sorted By Row Number";
        private const string sortedByTubeNumber = "Sorted By Tube Number";

        public BaselineTable()
        {
            InitializeComponent();
            UpdateDataSource();
            UpdateDisplayUnits();
            PopulateOrderCombo();
        }
 
        static string PosSymbol => DisplayUnits.Instance.AxialDistanceUnits.Scale == Length.LengthScale.Inches ? "Inch" : "mm";

        static string MeasSymbol => DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? "Inch" : "mm";

        //According to Tim (5/4/2014), meas values need 3 and 2 decimal places with trailing 0's.
        private static string MeasUnitDisplayFormat => DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches
            ? "N3"
            : "N2";

        //According to Tim (5/4/2014), no axial decimal places.
        private static string AxialUnitDisplayFormat => "N0";

        private static string MeasUnitExcelFormat => DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches
            ? "#.000"
            : "#.00";

        private static string PosUnitExcelFormat => "0";

        public ODorID CurrentData => (CurrentInspection != null && CurrentInspection.InspectionTool == Tool.LOTIS) ? ODorID.ID : ODorID.OD;

        private static FileInfo TemplateFile => new FileInfo(Path.Combine(UsefulDirectories.TemplateDirectory.FullName, "BaselineTable.xlsm"));

        #region Event Handlers

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportToExcel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, @"Error Exporting: " + ex.Message);
            }
        }

        #endregion Event Handlers

        public override void UpdateDataSource()
        {
            if (InvokeRequired) { Invoke(new Action(UpdateDataSource)); return; }
            CurrentInspection = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;
            SetupColumns();
            UpdateDisplayUnits();
            orderBy.EditValue = LifeQuestReformer.Instance.TheDataManager.CurrentProject.OrderBaselineBy;
        }

        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDisplayUnits));
                return;
            }
            string dataSymbol = CurrentData.ToString();
            colMaxDiam.Caption = $"Max {dataSymbol} ({MeasSymbol})";
            colRelMaxPos.Caption = $"Position- ({PosSymbol})";
            colMinDiam.Caption = $"Min {dataSymbol} ({MeasSymbol})";
            colMinPosition.Caption = $"Position- ({PosSymbol})";
            
            colMaxDiam.DisplayFormat.FormatString = MeasUnitDisplayFormat;
            colMinDiam.DisplayFormat.FormatString = MeasUnitDisplayFormat;
            colRelMaxPos.DisplayFormat.FormatString = AxialUnitDisplayFormat;
            colMinPosition.DisplayFormat.FormatString = AxialUnitDisplayFormat;

            TheGrid.RefreshDataSource();
         }

        private async void ExportToExcel()
        {
            Cursor = Cursors.WaitCursor;
            await new TaskFactory().StartNew(() =>
            {
                OnProgressChanged(10, "Opening Template in Excel.");
                string templateName = LifeQuestReformer.Instance.TheDataManager.CurrentProject.OrderBaselineBy == OrderBy.TubeNumber ? "TubeSort" : "RowTubeSort";
                Application xlApp = OpenTemplate(templateName);
                OnProgressChanged(30, "Writing Data to Template");
                //Because this messes with the view, invoke to the same thread it was created on.
                if (InvokeRequired)
                    Invoke(new Action(() => WriteData(xlApp, true)));
                else
                    WriteData(xlApp, true);
                OnProgressChanged(50, "Formatting Data.");
                SetColumnFormatting(xlApp);
                OnProgressChanged(70, "Adding Inspection Information.");
                WriteInspectionInfo(xlApp, LifeQuestReformer.Instance.TheDataManager.CurrentProject.OrderBaselineBy == OrderBy.RowNumber ? sortedByRowNumber.ToUpper() : sortedByTubeNumber.ToUpper());
                OnProgressChanged(90, "Finished Exporting First Table to Excel.");

                //NOTE: No longer required according to Tim Haugen on 4th Feb, 2016
                //OnProgressChanged(60, "Opening Template in Excel.");
                //xlApp = OpenTemplate("MaxDiamSort");
                //OnProgressChanged(70, "Writing Data to Template");
                ////Because this messes with the view, invoke to the same thread it was created on.
                //if (InvokeRequired)
                //    Invoke(new Action(() => WriteData(xlApp, false)));
                //else
                //    WriteData(xlApp, false);
                //OnProgressChanged(80, "Formatting Data.");
                //SetColumnFormatting(xlApp);
                //OnProgressChanged(90, "Adding Inspection Information.");
                //WriteInspectionInfo(xlApp, "SORTED BY MAXIMUM DIAMETER");
                OnProgressChanged(100, "Finished Exporting to Excel.");
            });
            Cursor = Cursors.Default;
        }

        private void WriteInspectionInfo(Application xlApp, string header)
        {
            Workbook theWorkbook = xlApp.ActiveWorkbook;

            ManufacturingSpecs specs = CurrentInspection.Tubes.First(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).Specs;
            bool useOD = CurrentData == ODorID.OD;
            double designed = useOD ? specs.DiameterOutsideInDisplayUnits : specs.DiameterInsideInDisplayUnits;
            var toolTolUp = useOD ? DefaultValues.ToolTolerances[Tool.MANTIS].Max : DefaultValues.ToolTolerances[Tool.LOTIS].Max;
            var toolTolDown = useOD ? DefaultValues.ToolTolerances[Tool.MANTIS].Min : DefaultValues.ToolTolerances[Tool.LOTIS].Min;
            var tolUp = useOD ? specs.ODTolUpDisplayUnits : specs.IDTolUpDisplayUnits;
            var tolDown = useOD ? specs.ODTolDownDisplayUnits : specs.IDTolDownDisplayUnits;

            //Do rounding shenanigans at every step.
            double maxTol = Math.Round(Math.Round(designed, DefaultValues.MeasNumDecimals) + tolUp, DefaultValues.MeasNumDecimals);
            double maxTolWithToolTol = Math.Round(maxTol + toolTolUp, DefaultValues.MeasNumDecimals);
            double minTol = Math.Round(designed + tolDown, DefaultValues.MeasNumDecimals);
            double minTolWithToolTol = Math.Round(minTol + toolTolDown, DefaultValues.MeasNumDecimals);

            string nomOD = CurrentInspection.ReformerInfo.DefaultODDisplayUnits.ToString(DefaultValues.MeasHashFormat);
            string nomID = CurrentInspection.ReformerInfo.DefaultIDDisplayUnits.ToString(DefaultValues.MeasHashFormat);
            string nomWall = ((CurrentInspection.ReformerInfo.DefaultODDisplayUnits - CurrentInspection.ReformerInfo.DefaultIDDisplayUnits) / 2).ToString(DefaultValues.MeasHashFormat);
            
            //I used a find and replace mechanism for this template because some cells needed letter formatting which cannot be done in cell formulas that use Named Ranges.
            foreach (Worksheet sheet in theWorkbook.Sheets)
            {
                sheet.Cells.Replace("[MeasUn]", MeasSymbol);
                sheet.Cells.Replace("[PosUn]", PosSymbol);
                if (TheView.RowCount > 0)
                {
                    sheet.Cells.Replace("[MaxTolerance]", maxTol.ToString(MeasUnitDisplayFormat));
                    sheet.Cells.Replace("[MinTolerance]", minTol.ToString(MeasUnitDisplayFormat));
                }
                sheet.Cells.Replace("[ClientName]", CurrentInspection.ReformerInfo.Customer.ToUpper());
                sheet.Cells.Replace("[ReformerName]", CurrentInspection.ReformerInfo.ReformerName.ToUpper());
                sheet.Cells.Replace("[NominalText]", string.Format("Nom OD: {0} {3} Nom ID: {1} {3} Nom Wall: {2} {3}", nomOD, nomID, nomWall, DefaultValues.MeasurementUnitSymbolConverted));
                sheet.Cells.Replace("[SortedByText]", header);
                sheet.Cells.Replace("[IntOrExt]", CurrentData == ODorID.ID ? "Internal" : "External");
            }
            theWorkbook.Names.Item("MinTol").RefersToRange.Value2 = minTol.ToString(MeasUnitDisplayFormat);
            theWorkbook.Names.Item("MaxTol").RefersToRange.Value2 = maxTol.ToString(MeasUnitDisplayFormat);
            theWorkbook.Names.Item("MinTolWithToolTol").RefersToRange.Value2 = minTolWithToolTol.ToString(MeasUnitDisplayFormat);
            theWorkbook.Names.Item("MaxTolWithToolTol").RefersToRange.Value2 = maxTolWithToolTol.ToString(MeasUnitDisplayFormat);
            theWorkbook.Names.Item("ToolTolUp").RefersToRange.Value2 = toolTolUp.ToString(MeasUnitDisplayFormat); 
            theWorkbook.Names.Item("Unit").RefersToRange.Value2 = MeasSymbol.ToLower();
            xlApp.Run("ExportCode.ExportToWord");
            xlApp.Workbooks[1].Save();
            xlApp.Workbooks[1].Close();
            xlApp.Workbooks.Close();
            xlApp.Quit();
        }

        private void SetColumnFormatting(_Application XlApp)
        {
            Workbook theWorkbook = XlApp.ActiveWorkbook;
            Worksheet theTable = (Worksheet)theWorkbook.Sheets.Item["Table"];
            theTable.Range[theTable.Cells[6, 3], theTable.Cells[500, 3]].NumberFormat = MeasUnitExcelFormat;
            theTable.Range[theTable.Cells[6, 6], theTable.Cells[500, 6]].NumberFormat = PosUnitExcelFormat;
            theTable.Range[theTable.Cells[6, 7], theTable.Cells[500, 7]].NumberFormat = MeasUnitExcelFormat;
            theTable.Range[theTable.Cells[6, 10], theTable.Cells[500, 10]].NumberFormat = PosUnitExcelFormat;
        }

        private void WriteData(_Application XlApp, bool rowTubeSort)
        {
            object[,] dataToWrite = new object[TheView.RowCount, TheView.VisibleColumns.Count];
            TheView.SortInfo.Clear();

            if (rowTubeSort)
                TheView.SortInfo.Add(colOrderIndex, ColumnSortOrder.Ascending);
            else
                TheView.SortInfo.Add(colMaxDiam, ColumnSortOrder.Descending);
            
            TheView.MoveFirst();

            for (int rowCount = 0; rowCount < TheView.RowCount; rowCount++)
            {
                int rowHandle = TheView.GetSelectedRows()[0];
                for (int colNum = 0; colNum < TheView.VisibleColumns.Count; colNum++)
                {
                    GridColumn theColumn = TheView.VisibleColumns[colNum];
                    dataToWrite[rowCount, colNum] = TheView.GetRowCellDisplayText(rowHandle, theColumn);
                }
                
                TheView.MoveNext();
            }

            Workbook theWorkbook = XlApp.ActiveWorkbook;
            Worksheet theDataSheet = (Worksheet)theWorkbook.Sheets.Item["Input"];
            theDataSheet.Range[theDataSheet.Cells[1,1], theDataSheet.Cells[TheView.RowCount, TheView.VisibleColumns.Count]].Value = dataToWrite;
        }

        private Application OpenTemplate(string sortText)
        {
            Application xlApp = new Application { Visible = true };
            try
            {
                xlApp.Workbooks.Open(TemplateFile.FullName);
                string quickSavePath = Path.Combine(UsefulDirectories.Tables.FullName, "BaselineTable-" + sortText);
                xlApp.DisplayAlerts = false;
                xlApp.ActiveWorkbook.SaveAs(quickSavePath, 52); //52 is xlsm format
            }
            catch (Exception ex)
            {
                OnProgressChanged(100, "Error opening template file: " + ex.Message);
                Log.Error("Error opening template file: " + ex.Message);
            }

            return xlApp;
        }

        private void TheView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsSetData) return;
            BindingSource mainDataSource = (BindingSource)TheGrid.DataSource;
            ReformerTube tube = (ReformerTube)mainDataSource[e.ListSourceRowIndex];
            double unusedValue;
            //Changing field names at runtime didn't work, so I get to manually do this per column. Yay!
            if (CurrentData == ODorID.OD)
            {
                //Max Positioning and Values
                if (e.Column == colMaxDiam)
                    e.Value = tube.ReformerAttributes.PickedOverallMaxODDisplayUnits;
                if (e.Column == colSection)
                    e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits);
                if (e.Column == colWeldNearestMax)
                    e.Value = tube.GetClosestWeld(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, out unusedValue);
                if (e.Column == colRelMaxPos)
                    e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, "");
                //Ref Positioning and values
                if (e.Column == colMinDiam)
                    e.Value = tube.ReformerAttributes.PickedOverallRefODDisplayUnits;
                if (e.Column == colSectionMin)
                    e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits);
                if (e.Column == colWeldNearestMin)
                    e.Value = tube.GetClosestWeld(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits, out unusedValue);
                if (e.Column == colMinPosition)
                    e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits, "");
                
            }
            else
            {
                //Max Positioning and values
                if (e.Column == colMaxDiam)
                    e.Value = tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits;
                if (e.Column == colSection)
                    e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits);
                if (e.Column == colWeldNearestMax)
                    e.Value = tube.GetClosestWeld(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, out unusedValue);
                if (e.Column == colRelMaxPos)
                    e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, "");
                //Ref Positioning and values
                if (e.Column == colMinDiam)
                    e.Value = tube.ReformerAttributes.PickedOverallRefIDDisplayUnits;
                if (e.Column == colSectionMin)
                    e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits);
                if (e.Column == colWeldNearestMin)
                    e.Value = tube.GetClosestWeld(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits, out unusedValue);
                if (e.Column == colMinPosition)
                    e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits, "");

                
            }
            //If the value is going to be blank, 0, or an empty string, set it to ' so that excel will not show a value.
            if (e.Value == null || e.Value.ToString().IsNullOrEmpty()) e.Value = "'";
        }

        private void SetupColumns()
        {
            BSReformerInspection.DataSource = CurrentInspection;
            colRowNumber.FieldName = CurrentInspection.ReformerInfo.UseRowLetters ? "ReformerAttributes.RowLetter" : "ReformerAttributes.RowNumber";
            UpdateDisplayUnits();
        }

        private void btnOpenOutputFolder_Click(object sender, EventArgs e)
        {
            Process.Start(UsefulDirectories.Tables.FullName);
        }

        private void PopulateOrderCombo()
        {
            List<KeyValuePair<Enum, string>> list = new List<KeyValuePair<Enum, string>>();

            list.Add(new KeyValuePair<Enum, string>(OrderBy.RowNumber, sortedByRowNumber));
            list.Add(new KeyValuePair<Enum, string>(OrderBy.TubeNumber, sortedByTubeNumber));

            orderBy.Properties.DisplayMember = "Value";
            orderBy.Properties.ValueMember = "Key";
            orderBy.Properties.ShowHeader = false;
            orderBy.Properties.ShowFooter = false;

            // This makes sure we only display the DisplayMamber field
            orderBy.Properties.Columns.Clear();
            orderBy.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value"));
            orderBy.Properties.DataSource = list;
        }

        private void orderBy_EditValueChanged(object sender, EventArgs e)
        {
            LifeQuestReformer.Instance.TheDataManager.CurrentProject.OrderBaselineBy = (OrderBy)orderBy.EditValue;

            GridColumn column = null;

            string colName = LifeQuestReformer.Instance.TheDataManager.CurrentProject.OrderBaselineBy == OrderBy.TubeNumber ? "colTubeNumber" : "colRowNumber";
            column = TheView.Columns.First(x => x.Name == colName);

            if (column != null)
            {
                TheView.ClearSorting();
                column.SortOrder = ColumnSortOrder.Ascending;
                // If its a row sort, add sort order to tubes to ensure they are numbered in order.
                if (column == colRowNumber)
                {
                    TheView.Columns.First(x => x.Name == "colTubeNumber").SortOrder = ColumnSortOrder.Ascending;
                }
            }
        }
    }
}
