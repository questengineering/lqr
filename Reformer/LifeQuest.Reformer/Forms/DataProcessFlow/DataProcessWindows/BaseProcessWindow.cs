﻿using System;
using System.ComponentModel;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.InspectionFile;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    /// <summary> The basic functionality required for all Process windows so other forms know what's up. </summary>
    public partial class BaseProcessWindow : ReformerBaseForm, IHasDisplayUnits
    {
        public BaseProcessFlowItem ProcessFlowItem { get; private set; }
        /// <summary> Flag to indicate whether this has been triggered to close. 
        /// This helps controls with VTK windows properly destroy items that need to be added and removed when a handle destroyed call is performed during an undocking/redocking. </summary>
        public bool Closing;
        protected ReformerInspection CurrentInspection;
        
        #region Constructors

        public BaseProcessWindow()
        {
            InitializeComponent();
        }

        public BaseProcessWindow(BaseProcessFlowItem processFlowItem) : this()
        {
            ProcessFlowItem = processFlowItem;
            OnProgressChanged(5, @"Finished Building Base Window");
        }

        #endregion Constructors

        #region Public Methods

        public virtual void SetCurrentInspection(ReformerInspection insp)
        {
            CurrentInspection = insp;
            UpdateDataSource();
        }

        public virtual void UpdateDisplayUnits()
        {

        }

        #endregion

        #region Event Handling

        public EventHandler<ProgressChangedEventArgs> ProgressChanged;
        
        protected void OnProgressChanged(int progressPercent = -1, string progressMessage = "SkipMessage")
        {
            if (ProgressChanged != null) ProgressChanged(this, new ProgressChangedEventArgs(progressPercent, progressMessage));
        }

        #endregion Event Handling

        
    }
}
