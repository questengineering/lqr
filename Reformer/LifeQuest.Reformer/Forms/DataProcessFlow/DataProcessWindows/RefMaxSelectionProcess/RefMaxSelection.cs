﻿using System;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RefMaxSelectionProcess
{
    public partial class RefMaxSelection : BaseProcessWindow, IHasDisplayUnits
    {
        public RefMaxSelection()
        {
            InitializeComponent();
        }

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            SetControlDataSources();
        }

        private void SetControlDataSources()
        {
            ThePlotControl.SetInspection(CurrentInspection);
        }

        public override void UpdateDisplayUnits()
        {
            ThePlotControl.UpdateDisplayUnits();
        }
    }
}
