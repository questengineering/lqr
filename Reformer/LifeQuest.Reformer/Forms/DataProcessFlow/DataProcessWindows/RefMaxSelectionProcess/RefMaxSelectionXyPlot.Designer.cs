﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RefMaxSelectionProcess
{
    partial class RefMaxSelectionXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefMaxSelectionXyPlot));
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lciTube = new DevExpress.XtraLayout.LayoutControlItem();
            this.newlciTubeSelecter = new DevExpress.XtraLayout.LayoutControlItem();
            this.radComparisonMethod = new DevExpress.XtraEditors.RadioGroup();
            this.lciCompareMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgAgedToAgedOptions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciAgedToAgedOption = new DevExpress.XtraLayout.LayoutControlItem();
            this.radAgedToAgedOption = new DevExpress.XtraEditors.RadioGroup();
            this.uiAxiallyLockRefAndMax = new DevExpress.XtraEditors.CheckEdit();
            this.lciAxiallyLockRefAndMax = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newlciTubeSelecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComparisonMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompareMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAgedToAgedOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAgedToAgedOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAgedToAgedOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAxiallyLockRefAndMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAxiallyLockRefAndMax)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.Size = new System.Drawing.Size(323, 283);
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(322, 217);
            this.DataCursorView.Size = new System.Drawing.Size(190, 59);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.uiAxiallyLockRefAndMax);
            this.lcMain.Controls.Add(this.radAgedToAgedOption);
            this.lcMain.Controls.Add(this.radComparisonMethod);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.Size = new System.Drawing.Size(519, 283);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.radComparisonMethod, 0);
            this.lcMain.Controls.SetChildIndex(this.radAgedToAgedOption, 0);
            this.lcMain.Controls.SetChildIndex(this.uiAxiallyLockRefAndMax, 0);
            // 
            // PlotPanel
            // 
            this.PlotPanel.Size = new System.Drawing.Size(306, 269);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.newlciTubeSelecter,
            this.lciCompareMethod,
            this.lcgAgedToAgedOptions,
            this.lciAxiallyLockRefAndMax});
            this.lcgMain.Size = new System.Drawing.Size(519, 283);
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(315, 210);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 63);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(310, 0);
            this.splitterItem1.Size = new System.Drawing.Size(5, 273);
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Size = new System.Drawing.Size(310, 273);
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(351, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DataSource = this.BSTube;
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.gridLookUpEdit1View;
            this.TubeSelecter.Size = new System.Drawing.Size(161, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 4;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.TubeSelector_EditValueChanged);
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 2;
            // 
            // lciTube
            // 
            this.lciTube.Control = this.TubeSelecter;
            this.lciTube.CustomizationFormText = "Tube";
            this.lciTube.Location = new System.Drawing.Point(0, 0);
            this.lciTube.Name = "lciTube";
            this.lciTube.Size = new System.Drawing.Size(207, 24);
            this.lciTube.Text = "Tube";
            this.lciTube.TextSize = new System.Drawing.Size(43, 13);
            this.lciTube.TextToControlDistance = 5;
            // 
            // newlciTubeSelecter
            // 
            this.newlciTubeSelecter.Control = this.TubeSelecter;
            this.newlciTubeSelecter.CustomizationFormText = "Tube";
            this.newlciTubeSelecter.Location = new System.Drawing.Point(315, 0);
            this.newlciTubeSelecter.Name = "newlciTubeSelecter";
            this.newlciTubeSelecter.Size = new System.Drawing.Size(194, 24);
            this.newlciTubeSelecter.Text = "Tube";
            this.newlciTubeSelecter.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.newlciTubeSelecter.TextSize = new System.Drawing.Size(24, 13);
            this.newlciTubeSelecter.TextToControlDistance = 5;
            // 
            // radComparisonMethod
            // 
            this.radComparisonMethod.Location = new System.Drawing.Point(322, 47);
            this.radComparisonMethod.Name = "radComparisonMethod";
            this.radComparisonMethod.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Current Inspection Only"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Aged to Baseline Inspection"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Aged to Aged Inspection")});
            this.radComparisonMethod.Size = new System.Drawing.Size(190, 63);
            this.radComparisonMethod.StyleController = this.lcMain;
            this.radComparisonMethod.TabIndex = 12;
            this.radComparisonMethod.SelectedIndexChanged += new System.EventHandler(this.radComparisonMethod_SelectedIndexChanged);
            // 
            // lciCompareMethod
            // 
            this.lciCompareMethod.Control = this.radComparisonMethod;
            this.lciCompareMethod.CustomizationFormText = "Comparison Method";
            this.lciCompareMethod.Location = new System.Drawing.Point(315, 24);
            this.lciCompareMethod.MaxSize = new System.Drawing.Size(0, 83);
            this.lciCompareMethod.MinSize = new System.Drawing.Size(99, 83);
            this.lciCompareMethod.Name = "lciCompareMethod";
            this.lciCompareMethod.Size = new System.Drawing.Size(194, 83);
            this.lciCompareMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCompareMethod.Text = "Comparison Method";
            this.lciCompareMethod.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciCompareMethod.TextSize = new System.Drawing.Size(95, 13);
            // 
            // lcgAgedToAgedOptions
            // 
            this.lcgAgedToAgedOptions.CustomizationFormText = "Aged to Aged Options";
            this.lcgAgedToAgedOptions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciAgedToAgedOption});
            this.lcgAgedToAgedOptions.Location = new System.Drawing.Point(315, 130);
            this.lcgAgedToAgedOptions.Name = "lcgAgedToAgedOptions";
            this.lcgAgedToAgedOptions.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.lcgAgedToAgedOptions.Size = new System.Drawing.Size(194, 80);
            this.lcgAgedToAgedOptions.Text = "Aged to Aged Selection";
            // 
            // lciAgedToAgedOption
            // 
            this.lciAgedToAgedOption.Control = this.radAgedToAgedOption;
            this.lciAgedToAgedOption.CustomizationFormText = "lciAgedToAgedOption";
            this.lciAgedToAgedOption.Location = new System.Drawing.Point(0, 0);
            this.lciAgedToAgedOption.MaxSize = new System.Drawing.Size(180, 47);
            this.lciAgedToAgedOption.MinSize = new System.Drawing.Size(180, 47);
            this.lciAgedToAgedOption.Name = "lciAgedToAgedOption";
            this.lciAgedToAgedOption.Size = new System.Drawing.Size(180, 47);
            this.lciAgedToAgedOption.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciAgedToAgedOption.Text = "lciAgedToAgedOption";
            this.lciAgedToAgedOption.TextSize = new System.Drawing.Size(0, 0);
            this.lciAgedToAgedOption.TextToControlDistance = 0;
            this.lciAgedToAgedOption.TextVisible = false;
            // 
            // radAgedToAgedOption
            // 
            this.radAgedToAgedOption.Enabled = false;
            this.radAgedToAgedOption.Location = new System.Drawing.Point(329, 163);
            this.radAgedToAgedOption.Name = "radAgedToAgedOption";
            this.radAgedToAgedOption.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Level 1 Growth"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Overall Growth")});
            this.radAgedToAgedOption.Size = new System.Drawing.Size(176, 43);
            this.radAgedToAgedOption.StyleController = this.lcMain;
            this.radAgedToAgedOption.TabIndex = 13;
            this.radAgedToAgedOption.SelectedIndexChanged += new System.EventHandler(this.radAgedToAgedOption_SelectedIndexChanged);
            // 
            // uiAxiallyLockRefAndMax
            // 
            this.uiAxiallyLockRefAndMax.Location = new System.Drawing.Point(322, 114);
            this.uiAxiallyLockRefAndMax.Name = "uiAxiallyLockRefAndMax";
            this.uiAxiallyLockRefAndMax.Properties.Caption = "Axially Lock Ref and Max";
            this.uiAxiallyLockRefAndMax.Size = new System.Drawing.Size(190, 19);
            this.uiAxiallyLockRefAndMax.StyleController = this.lcMain;
            this.uiAxiallyLockRefAndMax.TabIndex = 14;
            // 
            // lciAxiallyLockRefAndMax
            // 
            this.lciAxiallyLockRefAndMax.Control = this.uiAxiallyLockRefAndMax;
            this.lciAxiallyLockRefAndMax.CustomizationFormText = "lciAxiallyLockRefAndMax";
            this.lciAxiallyLockRefAndMax.Location = new System.Drawing.Point(315, 107);
            this.lciAxiallyLockRefAndMax.Name = "lciAxiallyLockRefAndMax";
            this.lciAxiallyLockRefAndMax.Size = new System.Drawing.Size(194, 23);
            this.lciAxiallyLockRefAndMax.Text = "lciAxiallyLockRefAndMax";
            this.lciAxiallyLockRefAndMax.TextSize = new System.Drawing.Size(0, 0);
            this.lciAxiallyLockRefAndMax.TextToControlDistance = 0;
            this.lciAxiallyLockRefAndMax.TextVisible = false;
            // 
            // RefMaxSelectionXyPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "RefMaxSelectionXyPlot";
            this.Size = new System.Drawing.Size(519, 283);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newlciTubeSelecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComparisonMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompareMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgAgedToAgedOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAgedToAgedOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAgedToAgedOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAxiallyLockRefAndMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAxiallyLockRefAndMax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem lciTube;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraLayout.LayoutControlItem newlciTubeSelecter;
        private DevExpress.XtraEditors.RadioGroup radComparisonMethod;
        private DevExpress.XtraLayout.LayoutControlItem lciCompareMethod;
        private DevExpress.XtraLayout.LayoutControlGroup lcgAgedToAgedOptions;
        private DevExpress.XtraEditors.RadioGroup radAgedToAgedOption;
        private DevExpress.XtraLayout.LayoutControlItem lciAgedToAgedOption;
        private DevExpress.XtraEditors.CheckEdit uiAxiallyLockRefAndMax;
        private DevExpress.XtraLayout.LayoutControlItem lciAxiallyLockRefAndMax;
    }
}
