﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Nevron.Chart;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.Features;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.RefMaxSelectionProcess
{
    public partial class RefMaxSelectionXyPlot : DataXyPlot, IHasDisplayUnits
    {
        private ReformerTube CurrentTube { get { return TubeSelecter.EditValue as ReformerTube; } }

        private ReformerTube PreviousInspectionTube
        {
            get
            {
                var prevInsp = PreviousInspection;
                if (prevInsp == null || CurrentTube == null || prevInsp.Tubes.All(I => I.Name != CurrentTube.Name)) return null;
                return prevInsp.Tubes.First(I => I.Name == CurrentTube.Name);
            }
        }

        private ReformerInspection OldestOrBaselineInspection { get; set; }

        private ReformerTube OldestOrBaselineInspectionTube
        {
            get
            {
                var oldestInsp = OldestOrBaselineInspection;
                if (oldestInsp == null || CurrentTube == null || oldestInsp.Tubes.All(I => I.Name != CurrentTube.Name)) return null;
                return oldestInsp.Tubes.First(I => I.Name == CurrentTube.Name);
            }
        }

        private ReformerInspection PreviousInspection { get; set; }

        private bool AxiallyLockRefAndMax { get { return uiAxiallyLockRefAndMax.Checked; } }

        private CreepCalcMethod CurrentComparisonType { get { return (CreepCalcMethod)radComparisonMethod.EditValue; } }
        private CachedData _currentData;
        private CachedData _previousData;
        private CachedData _oldestOrBaselineData;
        private bool ShowPreviousLine { get { return CurrentComparisonType == CreepCalcMethod.AgedToAged && radAgedToAgedOption.SelectedIndex == 0; } }

        private bool ShowOldestOrBaslineLine
        {
            get
            {
                return (CurrentComparisonType == CreepCalcMethod.AgedToAged && radAgedToAgedOption.SelectedIndex == 1) || CurrentComparisonType == CreepCalcMethod.AgedToBaseline;
            }
        }

        ReformerScalar CurrentScalar { get { return CurrentTube != null && CurrentTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside;}}

        public RefMaxSelectionXyPlot()
        {
            InitializeComponent();
            InitializeChart();
            InitializeDataCursor();
            DraggableVertConstLines = true;
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            radComparisonMethod.Properties.Items[0].Value = CreepCalcMethod.SameInspection;
            radComparisonMethod.Properties.Items[1].Value = CreepCalcMethod.AgedToBaseline;
            radComparisonMethod.Properties.Items[2].Value = CreepCalcMethod.AgedToAged;
        }

        protected override sealed void InitializeChart()
        {
            base.InitializeChart();
        }

        internal void SetInspection(ReformerInspection inspection)
        {
            if (inspection == null) return;
            BSTube.DataSource = inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.

            //Load the list of other inspections and select a reference immediately if there is more than 1 in the project.
            var otherInspections = CurrentProject.InspectionFiles.Where(I => I != inspection).ToList();
            bool olderInspExists = otherInspections.Any(I => I.DateInspected < inspection.DateInspected);

            if (otherInspections.Count > 0 && olderInspExists)
            {
                radComparisonMethod.Properties.Items[1].Enabled = true;
                radComparisonMethod.Properties.Items[2].Enabled = true;
                radComparisonMethod.SelectedIndex = otherInspections.Any(I => I.ReformerInfo.NewTubes) ? 1 : 2;
                radAgedToAgedOption.Enabled = CurrentComparisonType == CreepCalcMethod.AgedToAged;
            }
            else
            {
                radComparisonMethod.Properties.Items[1].Enabled = false;
                radComparisonMethod.Properties.Items[2].Enabled = false;
                radComparisonMethod.SelectedIndex = 0;
            }
            
            MoveFirst();
        }

        private void TubeSelector_EditValueChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            VerifyComparisonMethod();
            RedrawPlot();
        }

        private void RedrawPlot()
        {
            Stopwatch sw = Stopwatch.StartNew();
            DrawTubeData();
            DrawFeatures();
            UpdateYAxis(double.NaN, double.NaN, false, true);
            UpdateXAxis(CurrentTube.StartPosition - CurrentTube.Specs.MissedStart, CurrentTube.EndPosition + CurrentTube.Specs.MissedEnd);
            //Update the cursor location
            CursorValueChanged(AxialCursor, null);
            Log.Info(string.Format("Drew Tube Data for {0} in {1} ms", CurrentTube.Name, sw.ElapsedMilliseconds)); sw.Restart();
        }

        private void DrawTubeData()
        {
            ClearChart();
            ClearDataCursorView();
            if (CurrentTube == null) return;
            List<NLineSeries> linesToShow = new List<NLineSeries>();
            NLineSeries currentLine = CurrentTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
            for (int i = 0; i < currentLine.Values.Count; i++) { currentLine.Values[i] = (double)currentLine.Values[i] * 2; }
            currentLine.Name = CurrentTube.Inspection.DateInspected.ToShortDateString();
            _currentData = CacheLine(currentLine);
            linesToShow.Add(currentLine);

            if (PreviousInspectionTube != null)
            {
                NLineSeries previousLine = PreviousInspectionTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
                for (int i = 0; i < previousLine.Values.Count; i++) { previousLine.Values[i] = (double)previousLine.Values[i] * 2; }
                previousLine.Name = PreviousInspectionTube.Inspection.DateInspected.ToShortDateString();
                previousLine.BorderStyle.Color = Color.Red;
                _previousData = CacheLine(previousLine);
                if (ShowPreviousLine)
                    linesToShow.Add(previousLine);
            }
            
            if (OldestOrBaselineInspectionTube != null)
            {
                NLineSeries oldestLine = OldestOrBaselineInspectionTube.GetDefaultFormattedLine(CurrentScalar, CurrentTube.Inspection.DataMeasure).First();
                for (int i = 0; i < oldestLine.Values.Count; i++) { oldestLine.Values[i] = (double)oldestLine.Values[i] * 2; }
                oldestLine.Name = OldestOrBaselineInspectionTube.Inspection.DateInspected.ToShortDateString();
                oldestLine.BorderStyle.Color = Color.Red;
                _oldestOrBaselineData = CacheLine(oldestLine); 
                if (ShowOldestOrBaslineLine)
                    linesToShow.Add(oldestLine);
            }

            string xLabel = string.Format("Axial Position ({0})", DefaultValues.AxialUnitSymbol);
            string yLabel = string.Format("{0} ({1})", CurrentScalar.DisplayName.ToLower().Contains("inside") ? "Diameter (Inside)" : "Diameter (Outside)", DefaultValues.MeasurementUnitSymbol);
            AddLines(linesToShow, xLabel, yLabel);
        }

        private static CachedData CacheLine(NLineSeries line)
        {
            if (line == null) return new CachedData();
            return new CachedData
            {
                Name = line.Name,
                MedianRadiusData = new List<double>(line.Values.Cast<double>().ToList()),
                Positions = new List<double>(line.XValues.Cast<double>().ToList()),
                Line = line
            };
        }

        /// <summary>Adds a point series for each tube that shows weld locations. </summary>
        private void DrawWelds()
        {
            var welds = CurrentTube.Features.Where(f => f.Type == ReformerFeatureType.Weld).ToList();
            NPointSeries weldPoints = new NPointSeries
            {
                UseXValues = true,
                Size = new NLength(.1f, NRelativeUnit.ParentPercentage),
                DataLabelStyle = { Visible = false }
            };
            //Set the point shape.
            NMarkerStyle featureStyle = new NMarkerStyle
            {
                PointShape = PointShape.Cross,
                Height = new NLength(10),
                Width = new NLength(10),
                Visible = true,
                BorderStyle = { Color = Color.Black },
                FillStyle = new NColorFillStyle(Color.Black)
            };
            weldPoints.MarkerStyle = featureStyle;
            foreach (var weld in welds)
            {
                double yValue = weld.ParentTube.GetScalarStatisticalValue(CurrentScalar, CurrentTube.Inspection.DataMeasure, weld.SliceIndexStart);
                weldPoints.AddDataPoint(new NDataPoint(weld.StartPosition, yValue * 2, weld.Label));
            }
            Chart.Series.Add(weldPoints);
            TheChartControl.Controller.Selection.Clear();
            TheChartControl.Controller.Selection.Add(weldPoints);
            
        }

        private void DrawVerticalLines()
        {
            var values = GetRefMaxLines();
            SetVertConstLines(values.ToList());
        }

        private void DrawHorizontalLines()
        {
            double lowerTol, upperTol;
            if (CurrentScalar == ReformerScalar.RadiusInside)
            {
                lowerTol = CurrentTube.Specs.DiameterInside + CurrentTube.Specs.IDTolDown;
                upperTol = CurrentTube.Specs.DiameterInside + CurrentTube.Specs.IDTolUp;
            }
            else
            {
                lowerTol = CurrentTube.Specs.DiameterOutside + CurrentTube.Specs.ODTolDown;
                upperTol = CurrentTube.Specs.DiameterOutside + CurrentTube.Specs.ODTolUp;
            }

            double upperTolText = CurrentInspection.Units.LengthUnitSystem.Convert(upperTol, DisplayUnits.Instance.MeasurementUnits.Scale);
            double lowerTolText = CurrentInspection.Units.LengthUnitSystem.Convert(lowerTol, DisplayUnits.Instance.MeasurementUnits.Scale);


            ConstLineValue upperTolLine = new ConstLineValue { Alignment = ContentAlignment.TopRight, Value = upperTol, Text = upperTolText.ToString(DefaultValues.MeasHashFormat), Color = Color.Gray, PatternSize = 5 };
            ConstLineValue lowerTolLine = new ConstLineValue { Alignment = ContentAlignment.BottomRight, Value = lowerTol, Text = lowerTolText.ToString(DefaultValues.MeasHashFormat), Color = Color.Gray, PatternSize = 5 };
            var values = new List<ConstLineValue> {upperTolLine, lowerTolLine};
            SetHorzConstLines(values);
        }

        protected override void CursorValueChanged(object sender, EventArgs e)
        {
            if (DraggingLine != null)
            {
                PointNames typedName = (PointNames)Enum.Parse(typeof(PointNames), (string)DraggingLine.Tag);
                UpdateVertLinePosition(typedName, (float)((NAxisCursor)XAxis.Cursors[0]).Value);
                DrawHorizontalLines();
                DrawVerticalLines();
            }
            
            //Only trigger updates to the data line values when the Axial cursor shoots off an update.
            if (sender == AxialCursor)
            {
                double closestLineValue = double.MaxValue;
                //Reset the data cursor points.
                DataCursorPointSeries.ClearDataPoints(); 
                if (!Chart.Series.Contains(DataCursorPointSeries)) Chart.Series.Add(DataCursorPointSeries); 

                double xValue = AxialCursor.Value;
                List<PickedPoint> lineNamesAndValuesAtX = GetLineNamesAndValuesAtXValue(xValue).ToList();

                //Tim requested to remove the indicator when dragging the overall ref point indicator when picking the Aged-To-Aged comparison point. Kind of a pain.
                if (CurrentComparisonType != CreepCalcMethod.SameInspection && DraggingLine != null && DraggingLine.Tag.ToString().Contains("Ref"))
                {
                    lineNamesAndValuesAtX.Remove(lineNamesAndValuesAtX.FirstOrDefault(f => f.Name == CurrentInspection.DateInspected.ToShortDateString()));
                }
                else if (CurrentComparisonType != CreepCalcMethod.SameInspection && DraggingLine != null && DraggingLine.Tag.ToString().Contains("Max"))
                {
                    lineNamesAndValuesAtX.Remove(lineNamesAndValuesAtX.FirstOrDefault(f => f.Name != CurrentInspection.DateInspected.ToShortDateString()));
                }


                ListViewGroup lineGroup = DataCursorView.Groups["Tubes"];
                for (int idx = lineGroup.Items.Count - 1; idx >=0; idx--) { DataCursorView.Items.Remove(lineGroup.Items[idx]);}
                
                foreach (PickedPoint p in lineNamesAndValuesAtX)
                {
                    ListViewItem li = new ListViewItem(lineGroup) {Text = p.Name};
                    li.SubItems.Add(p.XValue.ToString("N3"));
                    li.SubItems.Add(p.YValue.ToString("N3"));
                    DataCursorView.Items.Add(li);
                    DataCursorPointSeries.AddDataPoint(new NDataPoint(p.BaseXValue, p.BaseYValue, string.Format("{0:N3}",p.YValue)));
                    DataCursorPointSeries.DataLabelStyle.Format ="<label>";
                    if (Math.Abs(MeasurementCursor.Value - p.BaseYValue) < Math.Abs(MeasurementCursor.Value - closestLineValue))
                        closestLineValue = p.BaseYValue;
                }
                if (Math.Abs(closestLineValue - double.MaxValue) > .001) MeasurementCursor.Value = closestLineValue;
            }
            
            //Add a data point showing the current OD/ID strain at the given point if a ref point is selected.
           
            if (DataCursorView.Groups["Strain"] == null) DataCursorView.Groups.Add("Strain", "Strain");
            ListViewGroup strainGroup = DataCursorView.Groups["Strain"];
            for (int idx = strainGroup.Items.Count - 1; idx >= 0; idx--) { DataCursorView.Items.Remove(strainGroup.Items[idx]); }
            List<PickedPoint> lineValue = GetLineNamesAndValuesAtXValue(AxialCursor.Value).ToList();
            if (lineValue.Count == 0) return;
            var maxFeature = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.OverallGrowthMax);
            if (maxFeature == null) return;
            double maxValue = _currentData.MedianRadiusData[maxFeature.SliceIndexStart];
            double maxValueDisplayUnits = maxValue * MeasUnitMultiplier;
            double maxPos = _currentData.Positions[maxFeature.SliceIndexStart];
            double maxPosDisplayUnits = maxPos * AxialUnitMultiplier;

            //The max line item is always the same regardless of comparison type.
            ListViewItem maxLineItem = new ListViewItem(strainGroup) { Text = @"Max" };
            maxLineItem.SubItems.Add(maxPosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
            maxLineItem.SubItems.Add(maxValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));

            ListViewItem minLineItem = new ListViewItem(strainGroup) { Text = @"Min" };
            ListViewItem strainLineItem = new ListViewItem(strainGroup) { Text = CurrentScalar == ReformerScalar.RadiusOutside ? @"OD Strain" : @"ID Strain" };
            switch (CurrentComparisonType)
            {
                case CreepCalcMethod.SameInspection:
                    if (!CurrentTube.ReformerAttributes.IsValidPickedOverallRefSlice) return; //no way to calculate strain
                    
                    double refValue = _currentData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double refValueDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(refValue, DisplayUnits.Instance.MeasurementUnits.Scale);

                    double refPos = _currentData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double refPosDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(refPos, DisplayUnits.Instance.AxialDistanceUnits.Scale);
                    string strain = ((maxValue - refValue) / refValue * 100).ToString("0.00");

                    minLineItem.SubItems.Add(refPosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
                    minLineItem.SubItems.Add(refValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));
                    strainLineItem.SubItems.Add(strain);
                    break;
                case CreepCalcMethod.AgedToBaseline:
                    if (OldestOrBaselineInspectionTube == null) return;
                    double refBaseValue = _oldestOrBaselineData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double refBaseValueDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(refBaseValue, DisplayUnits.Instance.MeasurementUnits.Scale);

                    double refBasePos = _oldestOrBaselineData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double refBasePosDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(refBasePos, DisplayUnits.Instance.AxialDistanceUnits.Scale);


                    minLineItem.SubItems.Add(refBasePosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
                    minLineItem.SubItems.Add(refBaseValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));

                    string agedToBaseStrain = ((maxValue - refBaseValue) / refBaseValue * 100).ToString("0.00");
                    strainLineItem = new ListViewItem(strainGroup) { Text = CurrentScalar == ReformerScalar.RadiusOutside ? @"OD Strain" : @"ID Strain" };
                    strainLineItem.SubItems.Add(agedToBaseStrain);
                    break;
                case CreepCalcMethod.AgedToAged:
                    var level1MaxFeature = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.Level1GrowthMax);
                    if (OldestOrBaselineInspectionTube == null || PreviousInspectionTube == null || level1MaxFeature == null) return;

                    //Level 1 list items (strain, ref, and max)
                    double level1MaxYValueDisplayUnits = _currentData.MedianRadiusData[level1MaxFeature.SliceIndexStart] * MeasUnitMultiplier;
                    double level1MaxPosDisplayUnits = _currentData.Positions[level1MaxFeature.SliceIndexStart] * AxialUnitMultiplier;
                    double level1RefYValueDisplayUnits = _previousData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice] * MeasUnitMultiplier;
                    double level1RefPosDisplayUnits = _previousData.Positions[CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice] * AxialUnitMultiplier;
                    ListViewItem level1LineItem = new ListViewItem(strainGroup) { Text = @"Level 1" };
                    string agedToAgedLevel1Strain = ((level1MaxYValueDisplayUnits - level1RefYValueDisplayUnits) / level1RefYValueDisplayUnits * 100).ToString("#0.00");
                    level1LineItem.SubItems.Add(agedToAgedLevel1Strain);
                    ListViewItem level1RefLineItem = new ListViewItem(strainGroup) { Text = @"L1 Ref" };
                    level1RefLineItem.SubItems.Add(level1RefPosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
                    level1RefLineItem.SubItems.Add(level1RefYValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));
                    ListViewItem level1MaxLineItem = new ListViewItem(strainGroup) { Text = @"L1 Max" };
                    level1MaxLineItem.SubItems.Add(level1MaxPosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
                    level1MaxLineItem.SubItems.Add(level1MaxYValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));
                    DataCursorView.Items.Add(level1LineItem);
                    DataCursorView.Items.Add(level1MaxLineItem);
                    DataCursorView.Items.Add(level1RefLineItem);

                    //Overall List Items (strain, ref, and max)
                    double overallRefYValue = _oldestOrBaselineData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double overallRefYValueDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(overallRefYValue, DisplayUnits.Instance.MeasurementUnits.Scale);
                    double overallRefPos = _oldestOrBaselineData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    double overallRefPosDisplayUnits = CurrentInspection.Units.LengthUnitSystem.Convert(overallRefPos, DisplayUnits.Instance.AxialDistanceUnits.Scale);
                    strainLineItem = new ListViewItem(strainGroup) { Text = @"Overall" };
                    string agedToAgedOverallStrain = ((maxValue - overallRefYValue) / overallRefYValue * 100).ToString("#0.00");
                    strainLineItem.SubItems.Add(agedToAgedOverallStrain);

                    minLineItem.SubItems.Add(overallRefPosDisplayUnits.ToString(DefaultValues.AxialHashFormat));
                    minLineItem.SubItems.Add(overallRefYValueDisplayUnits.ToString(DefaultValues.MeasHashFormat));

                    break;
            }
            DataCursorView.Items.Add(strainLineItem);
            DataCursorView.Items.Add(maxLineItem);
            DataCursorView.Items.Add(minLineItem);
            
            DataCursorView.Refresh(); //Force a refresh or it will lag behind.
            
            Chart.Refresh();
        }

        /// <summary> Get the index of a value that is closest to the target value </summary>
        private static int FindClosestAxialPositionIndex(float targetPoint, List<double> values)
        {
            if (values == null) return -1;
           return values.IndexOf(values.Aggregate((a, b) => Math.Abs(targetPoint - a) < Math.Abs(targetPoint - b) ? a : b));
        }

        private void UpdateVertLinePosition(PointNames pointName, float positionInBaseUnits)
        {
            switch (pointName)
            {
                case PointNames.OverallMaxPoint:
                    int overallMaxSlice = FindClosestAxialPositionIndex(positionInBaseUnits, _currentData.Positions);
                    CurrentTube.Features.First(f => f.Type == ReformerFeatureType.OverallGrowthMax).SliceIndexStart = overallMaxSlice;
                    if (CurrentComparisonType == CreepCalcMethod.SameInspection || CurrentComparisonType == CreepCalcMethod.AgedToBaseline)
                    {
                        CurrentTube.Features.First(f => f.Type == ReformerFeatureType.Level1GrowthMax).SliceIndexStart = overallMaxSlice;
                    }
                    if (AxiallyLockRefAndMax) //Move the overall (and optionally Level 1) ref points as well.
                    {
                        CachedData tmpOverallCache = CurrentComparisonType == CreepCalcMethod.SameInspection ? _currentData : _oldestOrBaselineData;
                        int tmpOverallRefSlice = FindClosestAxialPositionIndex(positionInBaseUnits, tmpOverallCache.Positions);
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = tmpOverallRefSlice;
                        if (CurrentComparisonType == CreepCalcMethod.AgedToBaseline || CurrentComparisonType == CreepCalcMethod.SameInspection)
                            CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = tmpOverallRefSlice;
                    }
                    break;
                case PointNames.Level1MaxPoint:
                    int level1MaxSlice = FindClosestAxialPositionIndex(positionInBaseUnits, _currentData.Positions);
                    CurrentTube.Features.First(f => f.Type == ReformerFeatureType.Level1GrowthMax).SliceIndexStart = level1MaxSlice;
                    if (CurrentComparisonType == CreepCalcMethod.SameInspection)
                    {
                        CurrentTube.Features.First(f => f.Type == ReformerFeatureType.OverallGrowthMax).SliceIndexStart = level1MaxSlice;
                    }
                    if (AxiallyLockRefAndMax) //Move the Level 1 (and optionally overall) ref points as well.
                    {
                        CachedData tmpOverallCache = CurrentComparisonType == CreepCalcMethod.SameInspection ? _currentData : _previousData;
                        int tmpOverallRefSlice = FindClosestAxialPositionIndex(positionInBaseUnits, tmpOverallCache.Positions);
                        CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = tmpOverallRefSlice;
                        if (CurrentComparisonType == CreepCalcMethod.AgedToBaseline || CurrentComparisonType == CreepCalcMethod.SameInspection)
                            CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = tmpOverallRefSlice;
                    }
                    break;
                case PointNames.OverallRefPoint:
                    CachedData overallCacheToUse = CurrentComparisonType == CreepCalcMethod.SameInspection ? _currentData : _oldestOrBaselineData;
                    int overallRefSlice = FindClosestAxialPositionIndex(positionInBaseUnits, overallCacheToUse.Positions);
                    CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = overallRefSlice;
                    if (CurrentComparisonType == CreepCalcMethod.AgedToBaseline || CurrentComparisonType == CreepCalcMethod.SameInspection)
                        CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = overallRefSlice;
                    if (AxiallyLockRefAndMax) //Move the overall (and optionally Level 1) max points as well.
                    {
                        int tmpOverallMaxSlice = FindClosestAxialPositionIndex(positionInBaseUnits, _currentData.Positions);
                        CurrentTube.Features.First(f => f.Type == ReformerFeatureType.OverallGrowthMax).SliceIndexStart = tmpOverallMaxSlice;
                        if (CurrentComparisonType == CreepCalcMethod.AgedToBaseline || CurrentComparisonType == CreepCalcMethod.SameInspection)
                            CurrentTube.Features.First(f => f.Type == ReformerFeatureType.Level1GrowthMax).SliceIndexStart = tmpOverallMaxSlice;
                    }
                    break;
                case PointNames.Level1RefPoint:
                    int level1RefSlice = FindClosestAxialPositionIndex(positionInBaseUnits, _previousData.Positions);
                    CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = level1RefSlice;
                    if (CurrentComparisonType == CreepCalcMethod.SameInspection)
                    {
                        int level1RefSliceOverall = FindClosestAxialPositionIndex(positionInBaseUnits, _previousData.Positions);
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = level1RefSliceOverall;
                    }
                     if (AxiallyLockRefAndMax) //Move the overall (and optionally Level 1) max points as well.
                    {
                        int tmpOverallMaxSlice = FindClosestAxialPositionIndex(positionInBaseUnits, _currentData.Positions);
                        CurrentTube.Features.First(f => f.Type == ReformerFeatureType.Level1GrowthMax).SliceIndexStart = tmpOverallMaxSlice;
                        if (CurrentComparisonType == CreepCalcMethod.AgedToBaseline || CurrentComparisonType == CreepCalcMethod.SameInspection)
                            CurrentTube.Features.First(f => f.Type == ReformerFeatureType.OverallGrowthMax).SliceIndexStart = tmpOverallMaxSlice;
                    }
                    break;
            }
        }

        private void DrawFeatures()
        {
            XAxis.ConstLines.Clear();
            YAxis.ConstLines.Clear();
            DrawWelds();
            DrawVerticalLines();
            DrawHorizontalLines();
        }

        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            int nextKey = 0;
            if (TubeSelecter.EditValue != null) nextKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }

        private void TheChartControlOnKeyDown(object sender, KeyEventArgs args)
        {
            if (args.KeyCode == Keys.Q) MovePrevious();
            else if (args.KeyCode == Keys.E) MoveNext();
        }

        public void UpdateDisplayUnits()
        {
            DrawTubeData();
        }

        private IEnumerable<ConstLineValue> GetRefMaxLines()
        {
            List<ConstLineValue> constLines = new List<ConstLineValue>();

            switch ((CreepCalcMethod)radComparisonMethod.EditValue)
            {
                //Make a ref and max draggable line for a single inspection comparison
                case CreepCalcMethod.SameInspection:
                    //Get a default reference slice if there's no valid one selected yet.
                    if (!CurrentTube.ReformerAttributes.IsValidPickedOverallRefSlice)
                    {
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = GetDefaultMinSlice(CurrentTube);
                    }

                    var refPointPosition = _currentData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    var refPointYValue = _currentData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    ConstLineValue refConst = new ConstLineValue
                    {
                        Tag = PointNames.OverallRefPoint.ToString(),
                        Value = refPointPosition,
                        Alignment = ContentAlignment.TopLeft,
                        Text = "Ref Point: " + CurrentInspection.Units.LengthUnitSystem.Convert(refPointYValue, DisplayUnits.Instance.MeasurementUnits.Scale).ToString(DefaultValues.MeasHashFormat),
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    constLines.Add(refConst);

                    ReformerFeatureInfo sameInspOverallMax = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.OverallGrowthMax);
                    ReformerFeatureInfo sameInsplevel1Max = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.Level1GrowthMax);
                    //Get a default max reference features if there's no valid ones selected yet.
                    if (sameInspOverallMax == null || sameInsplevel1Max == null)
                    {
                        int maxSlice = GetDefaultMaxSlice(CurrentTube);
                        sameInspOverallMax = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.OverallGrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "Overall Max"
                        };

                        sameInsplevel1Max = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.Level1GrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "L1 Max"
                        };
                        CurrentTube.Features.AddRange(new[] { sameInspOverallMax, sameInsplevel1Max });
                    }

                    //only show one line. When that one updates, we can update both the level 1 and overall growth values
                    var sameInspOverallMaxPosition = _currentData.Positions[sameInspOverallMax.SliceIndexStart];
                    var sameInspOverallMaxYValue = _currentData.MedianRadiusData[sameInspOverallMax.SliceIndexStart];

                    ConstLineValue sameinspOverallMaxConst = new ConstLineValue
                    {
                        Tag = PointNames.Level1MaxPoint.ToString(),
                        Value = sameInspOverallMaxPosition,
                        Alignment = ContentAlignment.BottomRight,
                        Text = "Max Point: " + CurrentInspection.Units.LengthUnitSystem.Convert(sameInspOverallMaxYValue, DisplayUnits.Instance.MeasurementUnits.Scale).ToString(DefaultValues.MeasHashFormat),
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    constLines.Add(sameinspOverallMaxConst);

                    break;
                case CreepCalcMethod.AgedToBaseline:
                    //Get a default max slice if there's no valid one selected yet.
                    ReformerFeatureInfo overallMax = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.OverallGrowthMax);
                    //Get a default max reference features if there's no valid ones selected yet.
                    if (overallMax == null)
                    {
                        int maxSlice = GetDefaultMaxSlice(CurrentTube);
                        overallMax = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.OverallGrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "Overall Max"
                        };
                        //Make a level 1 growth max feature, but don't draw it on the plot since it is the same point in this case.
                        ReformerFeatureInfo level1Max = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.Level1GrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "L1 Max"
                        };
                        CurrentTube.Features.AddRange(new[] { overallMax, level1Max });
                    }

                    var agedToBaselineOverallMaxPosition = _currentData.Positions[overallMax.SliceIndexStart];
                    //var agedToBaselineOverallMaxYValue = _currentData.MedianRadiusData[overallMax.SliceIndexStart];

                    ConstLineValue agedToBaselineOverallMaxConst = new ConstLineValue
                    {
                        Tag = PointNames.OverallMaxPoint.ToString(),
                        Value = agedToBaselineOverallMaxPosition,
                        Alignment = ContentAlignment.BottomRight,
                        Text = "Overall Growth - New",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    constLines.Add(agedToBaselineOverallMaxConst);
                    
                    //Get default reference slices second because they're right under the max points if there's no valid one selected yet for the reference points.
                    if (!CurrentTube.ReformerAttributes.IsValidPickedOverallRefSlice)
                    {
                        int refSlice = FindClosestAxialPositionIndex((float)CurrentTube.Features.First(f => f.Type == ReformerFeatureType.OverallGrowthMax).StartPosition, _oldestOrBaselineData.Positions);
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = refSlice;
                    }

                    //Assume there is only overall ref and max points plotted.
                    var agedToBaselineOverallRefPointPosition = _oldestOrBaselineData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    //var agedToBaselineOverallRefPointYValue = _oldestOrBaselineData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];

                   ConstLineValue agedToBaselineOverallRefConst = new ConstLineValue
                    {
                        Tag = PointNames.OverallRefPoint.ToString(),
                        Value = agedToBaselineOverallRefPointPosition,
                        Alignment = ContentAlignment.TopLeft,
                        Text = "Overall Growth - Old",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    constLines.Add(agedToBaselineOverallRefConst);

                    break;
                case CreepCalcMethod.AgedToAged:
                    //Get default reference slices if there's no valid one selected yet for the reference points.
                    if (!CurrentTube.ReformerAttributes.IsValidPickedOverallRefSlice || !CurrentTube.ReformerAttributes.IsValidPickedLevel1RefSlice)
                    {
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = GetDefaultMinSlice(OldestOrBaselineInspectionTube);
                        CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = GetDefaultMinSlice(PreviousInspectionTube);
                    }

                    var agedToAgedOverallRefPointPosition = _oldestOrBaselineData.Positions[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];
                    //var agedToAgedOverallRefPointYValue = _oldestOrBaselineData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice];

                    var agedToAgedLevel1RefPointPosition = _previousData.Positions[CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice];
                    //var agedToAgedLevel1RefPointYValue = _previousData.MedianRadiusData[CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice];

                    ConstLineValue agedToAgedOverallRefConst = new ConstLineValue
                    {
                        Tag = PointNames.OverallRefPoint.ToString(),
                        Value = agedToAgedOverallRefPointPosition,
                        Alignment = ContentAlignment.TopLeft,
                        Text = "Overall Growth - Old",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    if (radAgedToAgedOption.SelectedIndex == 1)
                        constLines.Add(agedToAgedOverallRefConst);

                    ConstLineValue agedToAgedLevel1RefConst = new ConstLineValue
                    {
                        Tag = PointNames.Level1RefPoint.ToString(),
                        Value = agedToAgedLevel1RefPointPosition,
                        Alignment = ContentAlignment.BottomLeft,
                        Text = "Growth Rate - Previous",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    if (radAgedToAgedOption.SelectedIndex == 0)
                        constLines.Add(agedToAgedLevel1RefConst);

                    //Get a default maxerence slice if there's no valid one selected yet.
                    if (!CurrentTube.ReformerAttributes.IsValidPickedOverallRefSlice)
                    {
                        CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = GetDefaultMaxSlice(CurrentTube);
                    }

                    ReformerFeatureInfo agedToAgedOverallMax = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.OverallGrowthMax);
                    ReformerFeatureInfo agedToAgedLevel1Max = CurrentTube.Features.FirstOrDefault(f => f.Type == ReformerFeatureType.Level1GrowthMax);
                    //Get a default max reference features if there's no valid ones selected yet.
                    if (agedToAgedOverallMax == null || agedToAgedLevel1Max == null)
                    {
                        int maxSlice = GetDefaultMaxSlice(CurrentTube);
                        agedToAgedOverallMax = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.OverallGrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "Overall Max"
                        };

                        agedToAgedLevel1Max = new ReformerFeatureInfo(CurrentTube)
                        {
                            Type = ReformerFeatureType.Level1GrowthMax,
                            SliceIndexStart = maxSlice,
                            Label = "L1 Max"
                        };
                        CurrentTube.Features.AddRange(new[] { agedToAgedOverallMax, agedToAgedLevel1Max });
                    }

                    var agedToAgedOverallMaxPosition = _currentData.Positions[agedToAgedOverallMax.SliceIndexStart];
                    //var agedToAgedOverallMaxYValue = _currentData.MedianRadiusData[agedToAgedOverallMax.SliceIndexStart];

                    var agedToAgedLevel1MaxPosition = _currentData.Positions[agedToAgedLevel1Max.SliceIndexStart];
                    //var agedToAgedLevel1MaxYValue = _currentData.MedianRadiusData[agedToAgedLevel1Max.SliceIndexStart];

                    ConstLineValue agedToAgedOverallMaxConst = new ConstLineValue
                    {
                        Tag = PointNames.OverallMaxPoint.ToString(),
                        Value = agedToAgedOverallMaxPosition,
                        Alignment = ContentAlignment.TopRight,
                        Text = "Overall Growth - Max",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    if (radAgedToAgedOption.SelectedIndex == 1)
                        constLines.Add(agedToAgedOverallMaxConst);

                    ConstLineValue agedToAgedLevel1MaxConst = new ConstLineValue
                    {
                        Tag = PointNames.Level1MaxPoint.ToString(),
                        Value = agedToAgedLevel1MaxPosition,
                        Alignment = ContentAlignment.BottomRight,
                        Text = "Growth Rate - Max",
                        Color = Color.Blue,
                        PatternSize = 2
                    };
                    if (radAgedToAgedOption.SelectedIndex == 0)
                        constLines.Add(agedToAgedLevel1MaxConst);

                    break;

            }
            
            return constLines;
        }

        private int GetDefaultMaxSlice(ReformerTube tube)
        {
            var scalarData = tube.Get2DScalarStatisticalData(CurrentScalar, CurrentTube.Inspection.DataMeasure).First().Value;
            
            var weldSlices = tube.Features.Where(f => f.Type == ReformerFeatureType.Weld).Select(w => w.SliceIndexStart).ToList();
            float maxValue = float.MinValue;
            int maxValueSlice = -1;
            for (int i = 0; i < scalarData.Count; i++)
            {
                bool skip = i < 20 || i > scalarData.Count - 20; //Don't mark the beginning or end of a tube.
                foreach (int weldSlice in weldSlices)
                {
                    if (i.Between(weldSlice - 30, weldSlice + 30)) skip = true; //Don't mark right around welds. Sort of slow way of doing it, could refactor later
                }
                if (scalarData[i] <= maxValue) skip = true;
                if (skip) continue;
                //If it gets this far, store the slice and value
                maxValueSlice = i;
                maxValue = scalarData[i];
            }

            Log.DebugFormat("Found default max point for {0} at slice {1}, value of {2}", tube.Name, maxValueSlice, maxValue);
            return maxValueSlice;
        }

        /// <summary> Retrieves a default OverallGrowthRef feature by finding the minimum value in the tube that's not on a weld. </summary>
        private int GetDefaultMinSlice(ReformerTube tube)
        {
            var scalarData = tube.Get2DScalarStatisticalData(CurrentScalar, CurrentTube.Inspection.DataMeasure).First().Value;
            var weldSlices = tube.Features.Where(f => f.Type == ReformerFeatureType.Weld).Select(w => w.SliceIndexStart).ToList();
            float minValue = float.MaxValue; //default to max value so first value will always be lower
            int minValueSlice = -1;
            for (int i = 0; i < scalarData.Count; i++)
            {
                bool skip = i < 20 || i > scalarData.Count - 20; //Don't mark the beginning or end of a tube.
                foreach (int weldSlice in weldSlices)
                {
                    if (i.Between(weldSlice - 30, weldSlice + 30)) skip = true; //Don't mark right around welds. Sort of slow way of doing it, could refactor later
                }
                if (scalarData[i] >= minValue) skip = true;
                if (skip) continue;
                //If it gets this far, store the slice and value
                minValueSlice = i;
                minValue = scalarData[i];
            }
            Log.DebugFormat("Found default min point for {0} at slice {1}, value of {2}", tube.Name, minValueSlice, minValue);
            return minValueSlice;
            
        }

        /// <summary> Makes sure the currnet inspection gets properly setup when the comparison method changes. </summary>
        private void radComparisonMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            VerifyComparisonMethod();
            radAgedToAgedOption.Enabled = CurrentComparisonType == CreepCalcMethod.AgedToAged;
            RedrawPlot();
        }

        private void VerifyComparisonMethod()
        {
            switch (CurrentComparisonType)
            {
                case CreepCalcMethod.SameInspection:
                    PreviousInspection = CurrentInspection;
                    OldestOrBaselineInspection = CurrentInspection;
                    CurrentTube.ReformerAttributes.CalcMethod = CreepCalcMethod.SameInspection;
                    CurrentTube.ReformerAttributes.OldestOrBaselineTubeGUID = CurrentTube.ID;
                    CurrentTube.ReformerAttributes.PreviousTubeGUID = CurrentTube.ID;
                    break;
                case CreepCalcMethod.AgedToBaseline:
                    //Find the earliest inspection that contains this tube and has actual inspection data (found using available scalar axial position)
                    OldestOrBaselineInspection = CurrentProject.InspectionFiles.OrderBy(i => i.DateInspected).FirstOrDefault(i => i.Tubes.Any(t => t.Name == CurrentTube.Name && t.AvailableScalars.Contains(ReformerScalar.AxialPosition)));
                    if (OldestOrBaselineInspection == null || OldestOrBaselineInspection == CurrentInspection)
                        radComparisonMethod.EditValue = CreepCalcMethod.SameInspection;
                    else
                    {
                        var refTube = OldestOrBaselineInspection.Tubes.First(T => T.Name == CurrentTube.Name);
                        CurrentTube.ReformerAttributes.CalcMethod = CreepCalcMethod.AgedToBaseline;
                        CurrentTube.ReformerAttributes.OldestOrBaselineTubeGUID = refTube.ID;
                        CurrentTube.ReformerAttributes.PreviousTubeGUID = refTube.ID;
                    }
                    break;
                case CreepCalcMethod.AgedToAged:
                    //Find aged inspection that is not the current inspection and contains data for this tube.
                    var agedInspectionsWithThisTube = CurrentProject.InspectionFiles.OrderBy(i => i.DateInspected)
                        .Where(I => I != CurrentInspection && I.DateInspected < CurrentInspection.DateInspected && 
                            I.Tubes.Any(t => t.Name == CurrentTube.Name && t.AvailableScalars.Contains(ReformerScalar.AxialPosition))).ToList();

                    OldestOrBaselineInspection = agedInspectionsWithThisTube[0];
                    PreviousInspection = agedInspectionsWithThisTube.Last();

                    if (OldestOrBaselineInspection == null || PreviousInspection == null) //just in case they select this erroneously, somehow. Should be impossible
                        radComparisonMethod.EditValue = CreepCalcMethod.SameInspection;
                    else
                    {
                        var oldestTube = OldestOrBaselineInspection.Tubes.FirstOrDefault(T => T.Name == CurrentTube.Name && T.AvailableScalars.Contains(ReformerScalar.AxialPosition));
                        var previousTube = PreviousInspection.Tubes.FirstOrDefault(T => T.Name == CurrentTube.Name && T.AvailableScalars.Contains(ReformerScalar.AxialPosition));

                        //If the tube only exists in one of the inspections, make both the same inspection.
                        if (oldestTube == null && previousTube != null) OldestOrBaselineInspection = PreviousInspection;
                        if (previousTube == null && oldestTube != null) PreviousInspection = OldestOrBaselineInspection;

                        if (oldestTube == null || previousTube == null) // no tube with the right name to allow this type of comparison
                            radComparisonMethod.EditValue = CreepCalcMethod.SameInspection;
                        else
                        {
                            CurrentTube.ReformerAttributes.CalcMethod = CreepCalcMethod.AgedToAged;
                            CurrentTube.ReformerAttributes.OldestOrBaselineTubeGUID = oldestTube.ID;
                            CurrentTube.ReformerAttributes.PreviousTubeGUID = previousTube.ID;
                        }
                    }
                    break;
            }
        }

        private void radAgedToAgedOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            RedrawPlot();
        }

        private enum PointNames
        {
            OverallRefPoint, 
            Level1RefPoint,
            OverallMaxPoint,
            Level1MaxPoint
        }
    }
    
    struct CachedData
    {
        public string Name;
        public List<double> Positions;
        public List<double> MedianRadiusData;
        public NLineSeries Line;
    }

    

}
