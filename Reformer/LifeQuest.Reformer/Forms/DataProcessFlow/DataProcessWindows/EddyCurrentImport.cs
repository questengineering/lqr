﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.Core.Extensions;
using Reformer.Data;
using Reformer.Data.DataManager;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class EddyCurrentImport : BaseProcessWindow
    {
        readonly FolderBrowserDialog _browser = new FolderBrowserDialog();
        private string CurrentDir { get {  return uiSourceFolder.EditValue.IsNull() ? string.Empty : uiSourceFolder.EditValue.ToString();} }

        #region Constructors and Setup

        public EddyCurrentImport()
        {
            InitializeComponent();
        }

        private void SetupFolderBrowserDialog()
        {
            _browser.Description = @"Select the folder containing the Eddy Current CSV files for this inspection.";
            if (CurrentInspection != null)
            {
                _browser.SelectedPath = CurrentInspection.DirectoryPath;
                _browser.Description = CurrentInspection.InspectionTool == Tool.LOTIS
                    ? _browser.Description = @"Select the folder containing the LOTIS Eddy Current LEC files for this inspection."
                    : _browser.Description = @"Select the folder containing the MANTIS Eddy Current CSV files for this inspection.";
            }
            _browser.ShowNewFolderButton = false;
        }
        
        #endregion Constructors and Setup

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            if (CurrentInspection != null)
                BSInspection.DataSource = CurrentInspection;
            SetupFolderBrowserDialog();
        }
        
        #region Event Handlers

        private void uiSourceFolder_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Caption == "Open") OpenCurrentFolder();
            if (e.Button.Caption == "Select") SelectFolder();
        }

        private void TheView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) { TheView.DeleteSelectedRows(); }
        }

        #endregion Event Handlers

        #region Private Methods

        private async void SelectFolder()
        {
            OnProgressChanged(5, "Selecting Folder.");
            if (_browser.ShowDialog(this) == DialogResult.OK)
            {
                OnProgressChanged(10, "Folder Selected.");
                uiSourceFolder.EditValue = _browser.SelectedPath;
                //Asynchronously gather all the files in the folder using neat-o syntax.
                Task<List<RawFileInfo>> refreshTask = new Task<List<RawFileInfo>>(RefreshItemsInSelectedFolder);
                refreshTask.Start();
                TheGrid.DataSource = await refreshTask;
                OnProgressChanged(100, "Folder selection complete.");
            }
            else
                OnProgressChanged(100, "Folder selected cancelled.");
        }

        //Gathers all the items in the selected folder that match a search pattern based off of the tool type and ui selection.
        private List<RawFileInfo> RefreshItemsInSelectedFolder()
        {
            List<RawFileInfo> rawFileDatas = new List<RawFileInfo>();
            OnProgressChanged(15, "Building list of valid files.");
            DirectoryInfo currentDir = new DirectoryInfo(CurrentDir);
            if (!currentDir.Exists) return rawFileDatas;
            
            string searchPattern = CurrentInspection.InspectionTool == Tool.LOTIS ? "*_EC_Data.lec" : "*_EC_Data.csv";
            FileInfo[] matchedFiles = currentDir.GetFiles(searchPattern);
            OnProgressChanged(25, string.Format("Found {0} files.", matchedFiles.Length));

            int progressCount = 0;
            Parallel.ForEach(matchedFiles, delegate(FileInfo TheFile)
            {
                RawFileInfo fileInfoData = new RawFileInfo(TheFile);
                rawFileDatas.Add(fileInfoData);
                OnProgressChanged((int)(25 + (progressCount++ / (float)matchedFiles.Length) * .74), "Analyzed: + " + TheFile.Name);
            });
            return rawFileDatas;
        }

        private void OpenCurrentFolder()
        {
            if (CurrentDir != string.Empty)
            {
                var currentDir = new DirectoryInfo(CurrentDir);
                if (currentDir.Exists) Process.Start(currentDir.FullName);
                else OnProgressChanged(100, string.Format("Could not open directory: {0}.", currentDir.FullName));
            }
            else OnProgressChanged(100, string.Format("Could not open directory."));
        }

        #endregion Private Methods

        private void btnImportFiles_Click(object sender, EventArgs e)
        {
            ImportRawFiles();
        }

        private async void ImportRawFiles()
        {
            btnImportFiles.Enabled = false;
            List<RawFileInfo> rawFiles = ((List<RawFileInfo>)TheGrid.DataSource);
            ProgressToken token = new ProgressToken();
            token.ProgressChanged += (Sender, Args) => OnProgressChanged((int)((float)Args.Value / rawFiles.Count * 100));
            token.StatusChanged += (Sender, Args) => OnProgressChanged(-1, Args.Value);
            try
            {
                await LifeQuestReformer.Instance.TheDataManager.LoadEddyCurrentDataFilesAsync(rawFiles, token);
                OnProgressChanged(100, string.Format("Finished importing {0} files.", rawFiles.Count)); 
            }
            catch(Exception ex)
            {
                OnProgressChanged(100, "Error importing: " + ex.Message);
                Log.Error("Wrong file type attempted to import by user.");
            }
            
            btnImportFiles.Enabled = true;
        }
    }
}
