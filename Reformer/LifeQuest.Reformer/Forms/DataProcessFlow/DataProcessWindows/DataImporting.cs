﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using Reformer.Data;
using Reformer.Data.DataManager;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class DataImporting : BaseProcessWindow
    {
        readonly FolderBrowserDialog _browser = new FolderBrowserDialog();
        private string CurrentDir { get {  return uiSourceFolder.EditValue.IsNull() ? string.Empty : uiSourceFolder.EditValue.ToString();} }

        #region Constructors and Setup

        public DataImporting()
        {
            InitializeComponent();
        }

        private void SetupFolderBrowserDialog()
        {
            _browser.Description = @"Select the folder containing the raw files for this inspection.";
            if (CurrentInspection != null) _browser.SelectedPath = CurrentInspection.DirectoryPath;
            _browser.ShowNewFolderButton = false;
        }
        
        #endregion Constructors and Setup

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            if (CurrentInspection != null)
            {
                BSInspection.DataSource = CurrentInspection;
                SetExtensionOptions();
                SetupFolderBrowserDialog();
            }
        }

        

        #region Event Handlers

        private void uiSourceFolder_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Caption == "Open") OpenCurrentFolder();
            if (e.Button.Caption == "Select") SelectFolder();
        }

        private void TheView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) { TheView.DeleteSelectedRows(); }
        }

        #endregion Event Handlers

        #region Private Methods

        private async void SelectFolder()
        {
            OnProgressChanged(5, "Selecting Folder.");
            if (_browser.ShowDialog(this) == DialogResult.OK)
            {
                OnProgressChanged(10, "Folder Selected.");
                uiSourceFolder.EditValue = _browser.SelectedPath;
                //Asynchronously gather all the files in the folder using neat-o syntax.
                Task<List<RawFileInfo>> refreshTask = new Task<List<RawFileInfo>>(RefreshItemsInSelectedFolder);
                refreshTask.Start();
                TheGrid.DataSource = await refreshTask;
                OnProgressChanged(100, "Folder selection complete.");
            }
            else
                OnProgressChanged(100, "Folder selected cancelled.");
        }

        //Gathers all the items in the selected folder that match a search pattern based off of the tool type and ui selection.
        private List<RawFileInfo> RefreshItemsInSelectedFolder()
        {
            List<RawFileInfo> rawFileDatas = new List<RawFileInfo>();
            OnProgressChanged(15, "Building list of valid files.");
            DirectoryInfo currentDir = new DirectoryInfo(CurrentDir);
            if (!currentDir.Exists) return rawFileDatas;

            string searchPattern = chkLotFiles.Checked ? "*.lot" : "*.csv";
            FileInfo[] matchedFiles = currentDir.GetFiles(searchPattern);
            OnProgressChanged(25, $"Found {matchedFiles.Length} files.");

            int progressCount = 0;
            foreach (var matchedFile in matchedFiles)
            {
                RawFileInfo fileInfoData = new RawFileInfo(matchedFile);
                if (!fileInfoData.InvalidFileName)
                    rawFileDatas.Add(fileInfoData);
                OnProgressChanged((int)(15 + (progressCount++ / (float)matchedFiles.Length) * .84), "Analyzed: + " + matchedFile.Name);
            }
            return rawFileDatas;
        }

        private void OpenCurrentFolder()
        {
            if (CurrentDir != string.Empty)
            {
                var currentDir = new DirectoryInfo(CurrentDir);
                if (currentDir.Exists) Process.Start(currentDir.FullName);
                else OnProgressChanged(100, $"Could not open directory: {currentDir.FullName}.");
            }
            else OnProgressChanged(100, "Could not open directory.");
        }

        private void SetExtensionOptions()
        {
            chkLotFiles.Enabled = chkLotFiles.Checked = CurrentInspection.InspectionTool == Tool.LOTIS;
        }

        #endregion Private Methods

        private void btnImportFiles_Click(object sender, EventArgs e)
        {
            ImportRawFiles();
        }

        private async void ImportRawFiles()
        {
            btnImportFiles.Enabled = false;
            List<RawFileInfo> rawFiles = ((List<RawFileInfo>)TheGrid.DataSource);
            if (rawFiles == null || rawFiles.Count == 0)
            {
                OnProgressChanged(100, "No files to import."); return;
            }

            ProgressToken token = new ProgressToken();
            token.ProgressChanged += (sender, args) => OnProgressChanged((int)((float)args.Value / rawFiles.Count * 100));
            token.StatusChanged += (sender, args) => OnProgressChanged(-1, args.Value);
            try
            {
                await LifeQuestReformer.Instance.TheDataManager.LoadDataFilesAsync(rawFiles, token);
                OnProgressChanged(100, $"Finished importing {rawFiles.Count} files."); 
            }
            catch(DataException ex)
            {
                OnProgressChanged(100, "Error: Improper file type (LOTIS/MANTIS).");
                Log.ErrorFormat("Wrong file type attempted to import by user. {0}", ex.Message);
            }
            
            btnImportFiles.Enabled = true;
        }

        private void chkLotFiles_CheckedChanged(object sender, EventArgs e)
        {
            colEncoder.Visible = chkLotFiles.Checked;
        }
    }

    public class RawFileInfo
    {
        private int _encoder = 1;
        public int Row { get; set; }
        public int Tube { get; set; }
        public string FileName { get; private set; }
        public string TubeName { get { return FileName.Substring(0, 7); } }
        public string Extension { get; private set; }
        public float SizeInMb { get; private set; }
        public FileInfo RawFile { get; set; }
        public bool InvalidFileName { get; private set; }
        
        public int Encoder
        {
            get { return _encoder; }
            set { _encoder = value.Constrain(1, 2); }
        }

        public RawFileInfo(FileInfo theFile)
        {
            SizeInMb = theFile.Exists ? theFile.Length * 9.53674e-7f : -1;
            RawFile = theFile;
            ParseFileName(theFile.Name);
        }

        private void ParseFileName(string theFileName)
        {
            bool rowNumValid = false, tubeNumValid = false;

            if (theFileName.Length >= 11) //prevents substring from failing from weird filenames
            {
                int row, tube;
                rowNumValid = int.TryParse(theFileName.Substring(1, 2), out row);
                Row = row;
                tubeNumValid = int.TryParse(theFileName.Substring(4, 3), out tube);
                Tube = tube;
                FileName = Path.GetFileNameWithoutExtension(theFileName);
                Extension = Path.GetExtension(theFileName);
            }
            InvalidFileName = !rowNumValid || !tubeNumValid;
        }

        public override string ToString() { return FileName; }
    }
}
