﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess
{
    sealed partial class FeatureTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.TheGrid = new DevExpress.XtraGrid.GridControl();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRowNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxODDisplayUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxODSection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelativeMaxODPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxIDDisplayUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxIDSection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelativeMaxIDPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxODGrowth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxIDGrowth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colODWeld = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDWeld = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTestCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnExport);
            this.lcMain.Controls.Add(this.TheGrid);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(720, 483);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "layoutControl1";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(88, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(58, 22);
            this.btnExport.StyleController = this.lcMain;
            this.btnExport.TabIndex = 8;
            this.btnExport.Text = "Export";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // TheGrid
            // 
            this.TheGrid.DataSource = this.BSTube;
            this.TheGrid.Location = new System.Drawing.Point(0, 26);
            this.TheGrid.MainView = this.TheView;
            this.TheGrid.Name = "TheGrid";
            this.TheGrid.Size = new System.Drawing.Size(720, 457);
            this.TheGrid.TabIndex = 4;
            this.TheGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRowNum,
            this.colTubeNum,
            this.colMaxODDisplayUnits,
            this.colMaxODSection,
            this.colRelativeMaxODPos,
            this.colRefOD,
            this.colMaxIDDisplayUnits,
            this.colMaxIDSection,
            this.colRelativeMaxIDPos,
            this.colRefID,
            this.colMaxODGrowth,
            this.colMaxIDGrowth,
            this.colODWeld,
            this.colIDWeld,
            this.colTestCol});
            this.TheView.CustomizationFormBounds = new System.Drawing.Rectangle(2477, 425, 210, 179);
            this.TheView.GridControl = this.TheGrid;
            this.TheView.Name = "TheView";
            this.TheView.OptionsDetail.EnableMasterViewMode = false;
            this.TheView.OptionsView.ShowGroupPanel = false;
            this.TheView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRowNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.TheView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvMain_CustomUnboundColumnData);
            // 
            // colRowNum
            // 
            this.colRowNum.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colRowNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNum.Caption = "Row";
            this.colRowNum.FieldName = "ReformerAttributes.RowNumber";
            this.colRowNum.Name = "colRowNum";
            this.colRowNum.OptionsColumn.AllowEdit = false;
            this.colRowNum.OptionsColumn.ReadOnly = true;
            this.colRowNum.Visible = true;
            this.colRowNum.VisibleIndex = 0;
            this.colRowNum.Width = 34;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.OptionsColumn.AllowEdit = false;
            this.colTubeNum.OptionsColumn.ReadOnly = true;
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 1;
            this.colTubeNum.Width = 29;
            // 
            // colMaxODDisplayUnits
            // 
            this.colMaxODDisplayUnits.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxODDisplayUnits.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODDisplayUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxODDisplayUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODDisplayUnits.Caption = "Max OD (Units)";
            this.colMaxODDisplayUnits.DisplayFormat.FormatString = "N2";
            this.colMaxODDisplayUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxODDisplayUnits.FieldName = "ReformerAttributes.PickedOverallMaxODDisplayUnits";
            this.colMaxODDisplayUnits.Name = "colMaxODDisplayUnits";
            this.colMaxODDisplayUnits.OptionsColumn.AllowEdit = false;
            this.colMaxODDisplayUnits.OptionsColumn.ReadOnly = true;
            this.colMaxODDisplayUnits.Visible = true;
            this.colMaxODDisplayUnits.VisibleIndex = 2;
            this.colMaxODDisplayUnits.Width = 57;
            // 
            // colMaxODSection
            // 
            this.colMaxODSection.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxODSection.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODSection.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxODSection.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODSection.Caption = "Section";
            this.colMaxODSection.FieldName = "colMaxODSection";
            this.colMaxODSection.Name = "colMaxODSection";
            this.colMaxODSection.OptionsColumn.AllowEdit = false;
            this.colMaxODSection.OptionsColumn.ReadOnly = true;
            this.colMaxODSection.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colMaxODSection.Visible = true;
            this.colMaxODSection.VisibleIndex = 3;
            this.colMaxODSection.Width = 50;
            // 
            // colRelativeMaxODPos
            // 
            this.colRelativeMaxODPos.AppearanceCell.Options.UseTextOptions = true;
            this.colRelativeMaxODPos.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelativeMaxODPos.AppearanceHeader.Options.UseTextOptions = true;
            this.colRelativeMaxODPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelativeMaxODPos.Caption = "Max OD Pos (Units)";
            this.colRelativeMaxODPos.DisplayFormat.FormatString = "N2";
            this.colRelativeMaxODPos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRelativeMaxODPos.FieldName = "colRelativeMaxODPos";
            this.colRelativeMaxODPos.Name = "colRelativeMaxODPos";
            this.colRelativeMaxODPos.OptionsColumn.AllowEdit = false;
            this.colRelativeMaxODPos.OptionsColumn.ReadOnly = true;
            this.colRelativeMaxODPos.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colRelativeMaxODPos.Visible = true;
            this.colRelativeMaxODPos.VisibleIndex = 5;
            this.colRelativeMaxODPos.Width = 49;
            // 
            // colRefOD
            // 
            this.colRefOD.AppearanceCell.Options.UseTextOptions = true;
            this.colRefOD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefOD.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefOD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefOD.Caption = "Ref OD (Units)";
            this.colRefOD.DisplayFormat.FormatString = "N2";
            this.colRefOD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRefOD.FieldName = "ReformerAttributes.PickedOverallRefODDisplayUnits";
            this.colRefOD.Name = "colRefOD";
            this.colRefOD.OptionsColumn.AllowEdit = false;
            this.colRefOD.OptionsColumn.ReadOnly = true;
            this.colRefOD.Visible = true;
            this.colRefOD.VisibleIndex = 6;
            this.colRefOD.Width = 64;
            // 
            // colMaxIDDisplayUnits
            // 
            this.colMaxIDDisplayUnits.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxIDDisplayUnits.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDDisplayUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxIDDisplayUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDDisplayUnits.Caption = "Max ID (Units)";
            this.colMaxIDDisplayUnits.DisplayFormat.FormatString = "N2";
            this.colMaxIDDisplayUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxIDDisplayUnits.FieldName = "ReformerAttributes.PickedOverallMaxIDDisplayUnits";
            this.colMaxIDDisplayUnits.Name = "colMaxIDDisplayUnits";
            this.colMaxIDDisplayUnits.OptionsColumn.AllowEdit = false;
            this.colMaxIDDisplayUnits.OptionsColumn.ReadOnly = true;
            this.colMaxIDDisplayUnits.Width = 83;
            // 
            // colMaxIDSection
            // 
            this.colMaxIDSection.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxIDSection.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDSection.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxIDSection.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDSection.Caption = "Section";
            this.colMaxIDSection.FieldName = "colMaxIDSection";
            this.colMaxIDSection.Name = "colMaxIDSection";
            this.colMaxIDSection.OptionsColumn.AllowEdit = false;
            this.colMaxIDSection.OptionsColumn.ReadOnly = true;
            this.colMaxIDSection.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // colRelativeMaxIDPos
            // 
            this.colRelativeMaxIDPos.AppearanceCell.Options.UseTextOptions = true;
            this.colRelativeMaxIDPos.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelativeMaxIDPos.AppearanceHeader.Options.UseTextOptions = true;
            this.colRelativeMaxIDPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelativeMaxIDPos.Caption = "gridColumn1";
            this.colRelativeMaxIDPos.DisplayFormat.FormatString = "N2";
            this.colRelativeMaxIDPos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRelativeMaxIDPos.FieldName = "colMaxIDPosDisplayUnits";
            this.colRelativeMaxIDPos.Name = "colRelativeMaxIDPos";
            this.colRelativeMaxIDPos.OptionsColumn.AllowEdit = false;
            this.colRelativeMaxIDPos.OptionsColumn.ReadOnly = true;
            this.colRelativeMaxIDPos.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // colRefID
            // 
            this.colRefID.AppearanceCell.Options.UseTextOptions = true;
            this.colRefID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefID.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefID.Caption = "Ref ID (Units)";
            this.colRefID.DisplayFormat.FormatString = "N2";
            this.colRefID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRefID.FieldName = "ReformerAttributes.PickedOverallRefIDDisplayUnits";
            this.colRefID.Name = "colRefID";
            this.colRefID.OptionsColumn.AllowEdit = false;
            this.colRefID.OptionsColumn.ReadOnly = true;
            // 
            // colMaxODGrowth
            // 
            this.colMaxODGrowth.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxODGrowth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODGrowth.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxODGrowth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxODGrowth.Caption = "Max % OD Growth";
            this.colMaxODGrowth.DisplayFormat.FormatString = "N2";
            this.colMaxODGrowth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxODGrowth.FieldName = "ReformerAttributes.OverallODGrowth";
            this.colMaxODGrowth.Name = "colMaxODGrowth";
            this.colMaxODGrowth.OptionsColumn.AllowEdit = false;
            this.colMaxODGrowth.OptionsColumn.ReadOnly = true;
            this.colMaxODGrowth.Visible = true;
            this.colMaxODGrowth.VisibleIndex = 7;
            this.colMaxODGrowth.Width = 64;
            // 
            // colMaxIDGrowth
            // 
            this.colMaxIDGrowth.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxIDGrowth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDGrowth.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxIDGrowth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxIDGrowth.Caption = "Max % ID Growth";
            this.colMaxIDGrowth.DisplayFormat.FormatString = "N2";
            this.colMaxIDGrowth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxIDGrowth.FieldName = "ReformerAttributes.OverallIDGrowth";
            this.colMaxIDGrowth.Name = "colMaxIDGrowth";
            this.colMaxIDGrowth.OptionsColumn.AllowEdit = false;
            this.colMaxIDGrowth.OptionsColumn.ReadOnly = true;
            this.colMaxIDGrowth.Visible = true;
            this.colMaxIDGrowth.VisibleIndex = 8;
            this.colMaxIDGrowth.Width = 70;
            // 
            // colODWeld
            // 
            this.colODWeld.AppearanceCell.Options.UseTextOptions = true;
            this.colODWeld.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODWeld.AppearanceHeader.Options.UseTextOptions = true;
            this.colODWeld.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODWeld.Caption = "Weld";
            this.colODWeld.FieldName = "colODWeld";
            this.colODWeld.Name = "colODWeld";
            this.colODWeld.OptionsColumn.AllowEdit = false;
            this.colODWeld.OptionsColumn.ReadOnly = true;
            this.colODWeld.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colODWeld.Visible = true;
            this.colODWeld.VisibleIndex = 4;
            this.colODWeld.Width = 64;
            // 
            // colIDWeld
            // 
            this.colIDWeld.AppearanceCell.Options.UseTextOptions = true;
            this.colIDWeld.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDWeld.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDWeld.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDWeld.Caption = "Weld";
            this.colIDWeld.FieldName = "colIDWeld";
            this.colIDWeld.Name = "colIDWeld";
            this.colIDWeld.OptionsColumn.AllowEdit = false;
            this.colIDWeld.OptionsColumn.ReadOnly = true;
            this.colIDWeld.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colIDWeld.Width = 66;
            // 
            // colTestCol
            // 
            this.colTestCol.AppearanceCell.BackColor = System.Drawing.Color.Transparent;
            this.colTestCol.AppearanceCell.ForeColor = System.Drawing.Color.Silver;
            this.colTestCol.AppearanceCell.Options.UseBackColor = true;
            this.colTestCol.AppearanceCell.Options.UseForeColor = true;
            this.colTestCol.AppearanceCell.Options.UseTextOptions = true;
            this.colTestCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTestCol.AppearanceHeader.Options.UseTextOptions = true;
            this.colTestCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTestCol.Caption = "% ID Growth Rate";
            this.colTestCol.DisplayFormat.FormatString = "N2";
            this.colTestCol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTestCol.FieldName = "ReformerAttributes.Level1GrowthRate";
            this.colTestCol.Name = "colTestCol";
            this.colTestCol.OptionsColumn.AllowEdit = false;
            this.colTestCol.Visible = true;
            this.colTestCol.VisibleIndex = 9;
            this.colTestCol.Width = 48;
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "layoutControlGroup1";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciGrid,
            this.emptySpaceItem1,
            this.layoutControlItem1});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMain.Size = new System.Drawing.Size(720, 483);
            this.lcgMain.TextVisible = false;
            // 
            // lciGrid
            // 
            this.lciGrid.Control = this.TheGrid;
            this.lciGrid.CustomizationFormText = "lciGrid";
            this.lciGrid.Location = new System.Drawing.Point(0, 26);
            this.lciGrid.Name = "lciGrid";
            this.lciGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciGrid.Size = new System.Drawing.Size(720, 457);
            this.lciGrid.TextSize = new System.Drawing.Size(0, 0);
            this.lciGrid.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(377, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(343, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnExport;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(86, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(62, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(62, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(62, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // Level1Table
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "Level1Table";
            this.Size = new System.Drawing.Size(720, 483);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraGrid.GridControl TheGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraLayout.LayoutControlItem lciGrid;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNum;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxODDisplayUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colRelativeMaxODPos;
        private DevExpress.XtraGrid.Columns.GridColumn colRefOD;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxODGrowth;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxIDGrowth;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxIDDisplayUnits;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colRelativeMaxIDPos;
        private DevExpress.XtraGrid.Columns.GridColumn colRefID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxODSection;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxIDSection;
        private DevExpress.XtraGrid.Columns.GridColumn colODWeld;
        private DevExpress.XtraGrid.Columns.GridColumn colIDWeld;
        private DevExpress.XtraGrid.Columns.GridColumn colTestCol;
    }
}
