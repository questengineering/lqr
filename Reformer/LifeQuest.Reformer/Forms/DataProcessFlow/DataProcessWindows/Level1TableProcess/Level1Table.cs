﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using Microsoft.Office.Interop.Excel;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Materials;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;
using Action = System.Action;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess
{
    public sealed partial class FeatureTable : BaseProcessWindow
    {
        #region Private Properties

        //According to Tim (5/4/2014), meas values need 3 and 2 decimal places with trailing 0's.
        private static string MeasUnitDisplayFormat
        {
            get
            {
                return DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches 
                    ? "F3" 
                    : "F2";
            }
        }

        //According to Tim (5/4/2014), no axial decimal places.
        private static string AxialUnitDisplayFormat { get { return "N0"; } }

        private static string MeasUnitExcelFormat
        {
            get
            {
                return DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches
                    ? "0.000"
                    : "0.00";
            }
        }

        private static string PosUnitExcelFormat
        {
            get { return "0"; }
        }

        private static FileInfo TemplateFile { get { return new FileInfo(Path.Combine(UsefulDirectories.TemplateDirectory.FullName, "MinWallTemplate.xlsm")); }}
        private ODorID DataToShow { get; set; }

        #endregion Private Properties

        #region Constructor and Initialization

        public FeatureTable()
        {
            InitializeComponent();
            UpdateDisplayUnits();
        }

        public override void UpdateDataSource()
        {
            if (InvokeRequired) { Invoke(new Action(UpdateDataSource)); return; }
            if (CurrentInspection != null)
            { 
                InitializeGrid();
                DataToShow = CurrentInspection.InspectionTool == Tool.LOTIS ? ODorID.ID : ODorID.OD;
                ResetColumns();
            }
        }

        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDisplayUnits));
                return;
            }
            colMaxODDisplayUnits.Caption = $"Max OD ({DefaultValues.MeasurementUnitSymbol})";
            colMaxIDDisplayUnits.Caption = $"Max ID ({DefaultValues.MeasurementUnitSymbol})";
            colRelativeMaxODPos.Caption = $"Max OD Pos ({DefaultValues.AxialUnitSymbol})";
            colRelativeMaxIDPos.Caption = $"Max ID Pos ({DefaultValues.AxialUnitSymbol})";
            colRefOD.Caption = $"Ref OD ({DefaultValues.MeasurementUnitSymbol})";
            colRefID.Caption = $"Ref ID ({DefaultValues.MeasurementUnitSymbol})";

            colMaxODDisplayUnits.DisplayFormat.FormatString = MeasUnitDisplayFormat;
            colMaxIDDisplayUnits.DisplayFormat.FormatString = MeasUnitDisplayFormat;
            colRelativeMaxIDPos.DisplayFormat.FormatString = AxialUnitDisplayFormat;
            colRelativeMaxODPos.DisplayFormat.FormatString = AxialUnitDisplayFormat;
            colRefOD.DisplayFormat.FormatString = MeasUnitDisplayFormat;
            colRefID.DisplayFormat.FormatString = MeasUnitDisplayFormat;

            TheGrid.RefreshDataSource();
        }

        private void InitializeGrid()
        {
            TheGrid.DataSource = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.;
            //Set up the sorting algorithm by two columns. Users can do this by Shift-Clicking columns, too.
            TheView.SortInfo.ClearAndAddRange(new[]
                {
                    new GridColumnSortInfo(colRowNum, ColumnSortOrder.Ascending), 
                    new GridColumnSortInfo(colTubeNum, ColumnSortOrder.Ascending) 
                });
        }

        #endregion Constructor and Initialization

        #region Private Methods

        private async void CalcLevel1Results(IEnumerable<ReformerTube> tubes)
        {
            Log.Info(string.Format("Initializing Level 1 Calcs"));
            Stopwatch sw = Stopwatch.StartNew();
            OnProgressChanged(0, "Calculating Level 1 results");
            int counter = 0;
            int count = CurrentInspection.Tubes.Count;
            
            //For Debugging
            //foreach (ReformerTube T in Tubes)
            //{
            //    T.ReformerAttributes.Level1Result = RemainingLifeAssessmentLookup.GetTubeStatus(T);
            //    OnProgressChanged((int)Math.Round(counter++ / (double)count * 100), "Calculated " + T.Name);
            //}
            await new TaskFactory().StartNew( () =>
            {
                Parallel.ForEach(tubes, T =>
                {
                    if (T.ReformerAttributes.Material == null) return;
                    //wasn't expecting to have 2 lists, sort of ghetto. Refactoring opportunity, though.
                    var listToUse = T.ReformerAttributes.Material.Name.ToLower().Contains("hk") ? 
                        RemainingLifeAssessmentLookup.HK40 : //HK40 has its own list.
                        RemainingLifeAssessmentLookup.HPModified;

                    T.ReformerAttributes.Level1Result = RemainingLifeAssessmentLookup.GetTubeStatus(T, listToUse);
                    OnProgressChanged((int)Math.Round(counter++ / (double)count * 100), "Calculated " + T.Name);
                });
            });

            foreach (ReformerTube t in tubes)//TODO is this the desired value to display? #33A?
            {
                //System.Diagnostics.Debug.WriteLine(t.Name +" L1GR: "+t.ReformerAttributes.Level1GrowthRate.ToString("F2"));

            }

            TheGrid.RefreshDataSource();
            OnProgressChanged(100, $"Calculated {count} Level 1 Results in {sw.ElapsedMilliseconds}ms.");
            Log.Info($"Calculated {count} Level 1 Results in {sw.ElapsedMilliseconds}ms.");
        }

        #endregion Private Methods

        #region Event Handlers

        private void btnExport_Click(object sender, EventArgs e) { if (CurrentInspection != null) ExportToTemplate(); }

        private void btnAutoCalcAll_Click(object sender, EventArgs e) { CalcLevel1Results(CurrentInspection.Tubes.ToList()); }

        private void gvMain_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsSetData) return;
            IList<ReformerTube> mainDataSource = (IList<ReformerTube>)TheGrid.DataSource;
            ReformerTube tube = mainDataSource[e.ListSourceRowIndex];
            double unusedValue;
            if (e.Column == colRelativeMaxODPos)
                e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, AxialUnitDisplayFormat);
            if (e.Column == colRelativeMaxIDPos)
                e.Value = tube.GetClosestTubeSectionRelativePosition(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, AxialUnitDisplayFormat);
            if (e.Column == colMaxODSection)
                e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits);
            if (e.Column == colMaxIDSection)
                e.Value = tube.GetClosestTubeSection(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits);
            if (e.Column == colODWeld || e.Column == colIDWeld)
            {
                if (!tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits.IsNaN())
                    e.Value = tube.GetClosestWeld(tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits, out unusedValue);
            }
                
        }

        #endregion Event Handlers

        #region Private Methods

        private void ResetColumns()
        {
            //Set visible columns based off of the selected option
            colRefID.Visible = false;
            colMaxIDDisplayUnits.Visible = false;
            colRelativeMaxIDPos.Visible = false;
            colMaxODGrowth.Visible = false;
            colMaxODDisplayUnits.Visible = false;
            colRelativeMaxODPos.Visible = false;
            colRefOD.Visible = false;
            colMaxODSection.Visible = colMaxIDSection.Visible = false;
            colODWeld.Visible = colODWeld.Visible = false;

            if (DataToShow == ODorID.OD)
            {
                colMaxODDisplayUnits.VisibleIndex = 2;
                colMaxODSection.VisibleIndex = 3;
                colODWeld.VisibleIndex = 4;
                colRelativeMaxODPos.VisibleIndex = 5;
                colRefOD.VisibleIndex = 6;
                colMaxODGrowth.VisibleIndex = 7;
                colMaxIDGrowth.VisibleIndex = 8;
                colTestCol.VisibleIndex = 9;
            }
            else
            {
                colMaxIDDisplayUnits.VisibleIndex = 2;
                colMaxIDSection.VisibleIndex = 3;
                colIDWeld.VisibleIndex = 4;
                colRelativeMaxIDPos.VisibleIndex = 5;
                colRefID.VisibleIndex = 6;
                colMaxIDGrowth.VisibleIndex = 7;
            }
            if (CurrentInspection != null)
                colRowNum.FieldName = CurrentInspection.ReformerInfo.UseRowLetters ? "ReformerAttributes.RowLetter":"ReformerAttributes.RowNumber";
        }

        /// <summary> Writes 2 versions of the data to a template. One is row sorted, the other is sorted by ID growth </summary>
        private void ExportToTemplate()
        {
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            Enabled = false;
            string destDir = UsefulDirectories.Tables.FullName;
            Application xlApp = null;
            try
            {
                //Sort by row/tube.
                TheView.SortInfo.ClearAndAddRange(new [] {new GridColumnSortInfo(colRowNum, ColumnSortOrder.Ascending), new GridColumnSortInfo(colTubeNum, ColumnSortOrder.Ascending)});
                xlApp = OpenTemplate();
                if (xlApp == null) return;
                xlApp.DisplayAlerts = false;
                WriteData(xlApp, "SORTED BY ROW NUMBER");

                SetColumnFormatting(xlApp, CurrentInspection);
                
                //Delete the worksheet that corresponds to a different tool type (delete OD for LOTIS, ID for MANTIS)
                xlApp.Workbooks[1].Sheets[CurrentInspection.InspectionTool == Tool.LOTIS ? "OD Report Table" : "ID Report Table"].Delete();
                
                xlApp.Workbooks[1].SaveAs(Path.Combine(destDir, "Table-RowSort"));
                xlApp.Run("ThisWorkbook.ExportToWord");
                xlApp.Workbooks[1].Close();
                xlApp.Workbooks.Close();
                xlApp.Quit();

                //Sort by ID% growth.
                TheView.SortInfo.ClearAndAddRange(new[] { new GridColumnSortInfo(colMaxIDGrowth, ColumnSortOrder.Descending)});
                xlApp = OpenTemplate();
                if (xlApp == null) return;
                xlApp.DisplayAlerts = false;
                WriteData(xlApp, "SORTED BY ID GROWTH");
                SetColumnFormatting(xlApp, CurrentInspection);

                //Delete the worksheet that corresponds to a different tool type (delete OD for LOTIS, ID for MANTIS)
                xlApp.Workbooks[1].Sheets[CurrentInspection.InspectionTool == Tool.LOTIS ? "OD Report Table" : "ID Report Table"].Delete();
                xlApp.Workbooks[1].SaveAs(Path.Combine(destDir, "Table-IDGrowthSort"));
                xlApp.Run("ThisWorkbook.ExportToWord");
                xlApp.Workbooks[1].Close();
                xlApp.Workbooks.Close();
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                if (xlApp != null)
                {
                    xlApp.DisplayAlerts = false;
                    xlApp.Workbooks.Close();
                    xlApp.Quit();
                }
                Log.Error("Error exporting to Excel", ex);
                OnProgressChanged(100, "Error exporting to Excel: " + ex.Message);
            }
            Enabled = true;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
        }

        private static void SetColumnFormatting(Application XlApp, ReformerInspection insp)
        {
            //OD sheet first
            Workbook theWorkbook = XlApp.ActiveWorkbook;
            Worksheet theODTable = (Worksheet)theWorkbook.Sheets.Item["OD Report Table"];

            string nomOD = insp.ReformerInfo.DefaultODDisplayUnits.ToString(DefaultValues.MeasHashFormat);
            string nomID = insp.ReformerInfo.DefaultIDDisplayUnits.ToString(DefaultValues.MeasHashFormat);
            string nomWall = ((insp.ReformerInfo.DefaultODDisplayUnits - insp.ReformerInfo.DefaultIDDisplayUnits) / 2).ToString(DefaultValues.MeasHashFormat);
            theODTable.Range[theODTable.Cells[4, 1], theODTable.Cells[4, 1]].Value2 = string.Format("Nom OD: {0} {3}  Nom ID: {1} {3}  Nom Wall: {2} {3}", nomOD, nomID, nomWall, DefaultValues.MeasurementUnitSymbolConverted);
            theODTable.Range[theODTable.Cells[5, 3], theODTable.Cells[500, 3]].NumberFormat = MeasUnitExcelFormat;
            theODTable.Range[theODTable.Cells[5, 5], theODTable.Cells[500, 5]].NumberFormat = PosUnitExcelFormat;
            theODTable.Range[theODTable.Cells[5, 6], theODTable.Cells[500, 6]].NumberFormat = MeasUnitExcelFormat;
            theODTable.Range[theODTable.Cells[5, 9], theODTable.Cells[500, 9]].NumberFormat = "0.00";
            theODTable.Range[theODTable.Cells[5, 10], theODTable.Cells[500, 10]].NumberFormat = "0.000000"; //6 decimal places for oxidation rate

            Workbook theOtherWorkbook = XlApp.ActiveWorkbook;
            Worksheet theIDTable = (Worksheet)theOtherWorkbook.Sheets.Item["ID Report Table"];
            theIDTable.Range[theIDTable.Cells[4, 1], theIDTable.Cells[4, 1]].Value2 = string.Format("Nom OD: {0} {3}  Nom ID: {1} {3}  Nom Wall: {2} {3}", nomOD, nomID, nomWall, DefaultValues.MeasurementUnitSymbolConverted);
            theIDTable.Range[theIDTable.Cells[5, 3], theIDTable.Cells[500, 3]].NumberFormat = MeasUnitExcelFormat;
            theIDTable.Range[theIDTable.Cells[5, 5], theIDTable.Cells[500, 5]].NumberFormat = PosUnitExcelFormat;
            theIDTable.Range[theIDTable.Cells[5, 6], theIDTable.Cells[500, 6]].NumberFormat = MeasUnitExcelFormat;
            theIDTable.Range[theIDTable.Cells[5, 9], theIDTable.Cells[500, 9]].NumberFormat = "0.00";
            theIDTable.Range[theIDTable.Cells[5, 10], theIDTable.Cells[500, 10]].NumberFormat = "0.000000"; //6 decimal places for oxidation rate
        }
        
        private void WriteData(Application XlApp, string header)
        {
            Sheets xlSheets = XlApp.ActiveWorkbook.Sheets;
            Worksheet xlInputSheet = (Worksheet)xlSheets.Item["Input"];
            var firstTubeWithData = CurrentInspection.Tubes.First(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition));
            //Write the meta-data the template needs
            xlInputSheet.Range["MeasUnits"].Value = DefaultValues.MeasurementUnitSymbolConverted;
            xlInputSheet.Range["AxialUnits"].Value = DefaultValues.MeasurementUnitSymbolConverted;
            xlInputSheet.Range["RefineryName"].Value = CurrentInspection.ReformerInfo.Customer.ToUpper();
            xlInputSheet.Range["ReformerName"].Value = CurrentInspection.ReformerInfo.ReformerName.ToUpper();
            xlInputSheet.Range["NomOD"].Value = firstTubeWithData.Specs.DiameterOutsideInDisplayUnits;
            xlInputSheet.Range["NomID"].Value = firstTubeWithData.Specs.DiameterInsideInDisplayUnits;
            xlInputSheet.Range["NomWall"].Value = firstTubeWithData.Specs.ThicknessWallInDisplayUnits;
            xlInputSheet.Range["InputType"].Value = DataToShow.ToString();
            xlInputSheet.Range["HeaderText"].Value = header.ToUpper();
            
            //Figure out where data writing is going to start.
            int startCol = int.Parse(xlInputSheet.Range["RawDataStartColumn"].Value.ToString());
            int startRow = int.Parse(xlInputSheet.Range["RawDataStartRow"].Value.ToString());
            int rowCount = 0;

            string[,] outputData = new string[CurrentInspection.Tubes.Count(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)), 13];
            TheView.MoveFirst();
            const string formulaStart = "=Value(\"";
            const string formulaEnd = "\")";
            for (int i = 0; i < TheView.RowCount; i++)
            {
                ReformerTube tube = (ReformerTube)TheView.GetRow(TheView.GetSelectedRows()[0]);
               
                outputData[rowCount, 0] = CurrentInspection.ReformerInfo.UseRowLetters ? tube.ReformerAttributes.RowLetter.ToString(CultureInfo.InvariantCulture) : tube.ReformerAttributes.RowNumber.ToString(CultureInfo.InvariantCulture);
                outputData[rowCount, 1] = $"{formulaStart}{tube.ReformerAttributes.TubeNumber}{formulaEnd}";
                if (DataToShow == ODorID.OD)
                {
                    outputData[rowCount, 2] = $"{formulaStart}{tube.ReformerAttributes.PickedOverallMaxODDisplayUnits.ToString(MeasUnitDisplayFormat)}{formulaEnd}";
                    outputData[rowCount, 3] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colMaxODSection)}";
                    outputData[rowCount, 4] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colODWeld)}";
                    outputData[rowCount, 5] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colRelativeMaxODPos)}";
                    outputData[rowCount, 6] = $"{formulaStart}{tube.ReformerAttributes.PickedOverallRefODDisplayUnits.ToString(MeasUnitDisplayFormat)}{formulaEnd}";
                }
                else
                {
                    outputData[rowCount, 2] = $"{formulaStart}{tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits.ToString(MeasUnitDisplayFormat)}{formulaEnd}";
                    outputData[rowCount, 3] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colMaxIDSection)}";
                    outputData[rowCount, 4] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colIDWeld)}";
                    outputData[rowCount, 5] = $"{TheView.GetRowCellDisplayText(TheView.GetSelectedRows()[0], colRelativeMaxIDPos)}";
                    outputData[rowCount, 6] = $"{formulaStart}{tube.ReformerAttributes.PickedOverallRefIDDisplayUnits.ToString(MeasUnitDisplayFormat)}{formulaEnd}";
                }
                outputData[rowCount, 7] = $"{formulaStart}{tube.ReformerAttributes.OverallODGrowth.ToString("0.00")}{formulaEnd}";
                outputData[rowCount, 8] = $"{formulaStart}{tube.ReformerAttributes.OverallIDGrowth.ToString("0.00")}{formulaEnd}";
                outputData[rowCount, 9] = tube.ReformerAttributes.Level1GrowthRate.ToString("0.00"); //%/yr units.
                outputData[rowCount, 10] = (tube.ReformerAttributes.OxidationRateUsedBaseUnits * DefaultValues.MeasBaseToDisplayUnits).ToString("0.000000");//6 decimal places for oxidation rate
                outputData[rowCount, 11] = "=\"" + tube.ReformerAttributes.CreepDamage + "\"";
                outputData[rowCount, 12] = "=\"" + tube.ReformerAttributes.Level1Result + "\"";
                
                TheView.MoveNext();
                rowCount++;
            }

            Range targetRange = xlInputSheet.Range[xlInputSheet.Cells[startRow, startCol], xlInputSheet.Cells[startRow + rowCount - 1, startCol + outputData.GetLength(1) - 1]];
            targetRange.FormulaArray = outputData;

            XlApp.Visible = true;
           
            XlApp.Workbooks[1].Sheets["OD Report Table"].Columns[11].Delete();
            XlApp.Workbooks[1].Sheets["ID Report Table"].Columns[10].Delete();
           
            XlApp.Workbooks[1].Sheets["OD Report Table"].Columns[10].Delete();
            XlApp.Workbooks[1].Sheets["ID Report Table"].Columns[9].Delete();
            
        }

        private Application OpenTemplate()
        {
            Application xlApp = new Application {Visible = false};
            try
            {
                xlApp.Workbooks.Open(TemplateFile.FullName);
            }
            catch (Exception ex)
            {
                OnProgressChanged(100, "Error opening template file: " + ex.Message);
                Log.Error("Error opening template file: " + ex.Message);
            }

            return xlApp;
        }

        #endregion Event Handlers

        

       
    }
}
