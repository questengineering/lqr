﻿using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Maths;
using Reformer.Data.Tubes;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess
{
    public class RemainingLifeAssessmentLookup
    {
        public static RemainingLifeAssessmentLookup Instance = new RemainingLifeAssessmentLookup();
        public static readonly List<RemainingLifeValue> HPModified = GetRLAHPModAndMicroAlloy();
        public static readonly List<RemainingLifeValue> MicroAlloy = GetRLAHPModAndMicroAlloy();
        public static readonly List<RemainingLifeValue> HK40 = GetRLAHK40();
        
        private RemainingLifeAssessmentLookup() {}

        /// <summary> Looks up the tube status given the diagrams provided in the Excel sheet. </summary>
        public static RemainingLifeStatus GetTubeStatus(ReformerTube tube, List<RemainingLifeValue> listToUse)
        {
            ReformerData tubeData = tube.ReformerAttributes;
            //Return out of bounds if unuseable data in Tube.
            if (tube.IsNull() || tube.Specs.YearsUntilNextInspection == 0 || tubeData.Level1GrowthRate.IsNaN() || tubeData.Level1IDGrowth.IsNaN()) return RemainingLifeStatus.OutOfBounds;
            
            //Check for the remaining life value that fits this tube's data.
            foreach (RemainingLifeValue rlv in listToUse.Where(Rlv => Rlv.Age == tube.Specs.YearsUntilNextInspection))
            {
                //subtract .001 from min to include values insignificantly close to or exactly at a transition
                if (!tubeData.Level1GrowthRate.Between(rlv.IDGrowthRateRange.Min - .001, rlv.IDGrowthRateRange.Max)) continue;
                if (!tubeData.OverallIDGrowth.Between(rlv.IDGrowthRange.Min - .001, rlv.IDGrowthRange.Max)) continue;
                return rlv.Status;
            }

            //If nothing matches, it's out of bounds.
            return RemainingLifeStatus.OutOfBounds;
        }

        /// <summary> Looks up the status given the diagrams provided in the Excel sheet. Not linked to object types like a Tube. </summary>
        public static RemainingLifeStatus GetArbitraryStatus(int ageInYears, double maxIDGrowthRate, double maxIDGrowth, int nextInspYears, List<RemainingLifeValue> listToUse)
        {
            //Return out of bounds if unuseable data.
            if (ageInYears == 0 || maxIDGrowthRate.IsNaN() || maxIDGrowth.IsNaN()) return RemainingLifeStatus.OutOfBounds;

            //Check for the remaining life value that fits this tube's data.
            foreach (RemainingLifeValue rlv in listToUse.Where(Rlv => Rlv.Age == nextInspYears))
            {
                //Between is inclusive of end values.
                if (!maxIDGrowthRate.Between(rlv.IDGrowthRateRange.Min, rlv.IDGrowthRateRange.Max)) continue;
                if (!maxIDGrowth.Between(rlv.IDGrowthRange.Min, rlv.IDGrowthRange.Max)) continue;
                return rlv.Status;
            }

            //If nothing matches, it's out of bounds.
            return RemainingLifeStatus.OutOfBounds;
        }

        private static List<RemainingLifeValue> GetRLAHPModAndMicroAlloy()
        {
            //Can't serialize this as it is 'confidential'.
            var returnList = new List<RemainingLifeValue>
            {
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(double.MinValue,0.7), IDGrowthRange = new Range<double>(double.MinValue,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(double.MinValue,2.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.9,1.1), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.1,1.3), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.3,1.5), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.5,1.7), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(double.MinValue,0.2), IDGrowthRange = new Range<double>(3.0,7.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.2,0.7), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(2.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.9,1.1), IDGrowthRange = new Range<double>(2.0,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.1,1.2), IDGrowthRange = new Range<double>(1.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.2,1.3), IDGrowthRange = new Range<double>(1.5,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.3,1.5), IDGrowthRange = new Range<double>(1.0,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.5,1.7), IDGrowthRange = new Range<double>(0.5,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.7,2.2), IDGrowthRange = new Range<double>(double.MinValue,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(2.2,2.7), IDGrowthRange = new Range<double>(double.MinValue,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(2.7,3.2), IDGrowthRange = new Range<double>(double.MinValue,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(3.2,3.7), IDGrowthRange = new Range<double>(double.MinValue,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(3.7,4.2), IDGrowthRange = new Range<double>(double.MinValue,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(4.2,4.7), IDGrowthRange = new Range<double>(double.MinValue,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(4.7,5.2), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(5.2,5.7), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(5.7,6.2), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(6.2,6.7), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                 
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(double.MinValue,0.2), IDGrowthRange = new Range<double>(7.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.2,0.7), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.7,1.2), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.2,1.7), IDGrowthRange = new Range<double>(5.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.7,2.2), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(2.2,2.7), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(2.7,3.2), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(3.2,3.7), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(3.7,4.2), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(4.2,4.7), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(4.7,5.2), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(5.2,5.7), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(5.7,6.2), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(6.2,6.7), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(6.7,double.MaxValue), IDGrowthRange = new Range<double>(0.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},

                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.0,0.3), IDGrowthRange = new Range<double>(0,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0,2.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(0,2.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(0,1.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(0,1.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(0,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(3.0,7.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.1,0.3), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(2.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(2.0,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(1.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(1.0,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(0.5,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.8,1.1), IDGrowthRange = new Range<double>(double.MinValue,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.1,1.3), IDGrowthRange = new Range<double>(double.MinValue,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.3,1.6), IDGrowthRange = new Range<double>(double.MinValue,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.6,1.8), IDGrowthRange = new Range<double>(double.MinValue,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.8,2.1), IDGrowthRange = new Range<double>(double.MinValue,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.1,2.3), IDGrowthRange = new Range<double>(double.MinValue,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.3,2.6), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.6,2.8), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.8,3.1), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(3.1,3.3), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(7.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.1,0.3), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.3,0.6), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.6,0.8), IDGrowthRange = new Range<double>(5.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.8,1.1), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.1,1.3), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.3,1.6), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.6,1.8), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.8,2.1), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.1,2.3), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.3,2.6), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.6,2.8), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(2.8,3.1), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(3.1,3.3), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(3.3,double.MaxValue), IDGrowthRange = new Range<double>(0.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                

                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.2), IDGrowthRange = new Range<double>(0,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(0,2.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0,1.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(0,1.0), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.2), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(2.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(1.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(1.0,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.5,0.7), IDGrowthRange = new Range<double>(double.MinValue,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(double.MinValue,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(double.MinValue,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.0,1.3), IDGrowthRange = new Range<double>(double.MinValue,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.3,1.4), IDGrowthRange = new Range<double>(double.MinValue,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.4,1.5), IDGrowthRange = new Range<double>(double.MinValue,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.5,1.7), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.7,1.9), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.9,2.0), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(2.0,2.2), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},


                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.2), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.2,0.4), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(5.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.5,0.7), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.0,1.3), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.3,1.4), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.4,1.5), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.5,1.7), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.7,1.9), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(1.9,2.0), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(2.0,2.2), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(2.2,double.MaxValue), IDGrowthRange = new Range<double>(0.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},


                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(0,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(0,2.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(0,1.5), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0,0.5), Status = RemainingLifeStatus.Okay},
                 
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(2.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(1.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0.5,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(0.0,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.5,0.7), IDGrowthRange = new Range<double>(0.0,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(0.0,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(0.0,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(0.0,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(0.0,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.1,1.3), IDGrowthRange = new Range<double>(0.0,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.3,1.4), IDGrowthRange = new Range<double>(0.0,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.4,1.5), IDGrowthRange = new Range<double>(0.0,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.5,1.6), IDGrowthRange = new Range<double>(0.0,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.1,0.3), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(5.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.5,0.7), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.1,1.3), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.3,1.4), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.4,1.5), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.5,1.6), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(1.6,double.MaxValue), IDGrowthRange = new Range<double>(0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                

                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(0,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(0,2.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(0,1.0), Status = RemainingLifeStatus.Okay},
                 
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(2.0,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(1.0,5.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0.0,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(0.0,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(0.0,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(0.0,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(0.0,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(0.0,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(0.0,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(0.0,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.1,1.2), IDGrowthRange = new Range<double>(0.0,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.2,1.3), IDGrowthRange = new Range<double>(0.0,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(5.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.1,1.2), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.2,1.3), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(1.3,double.MaxValue), IDGrowthRange = new Range<double>(0.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},

                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(0,3.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(0,1.5), Status = RemainingLifeStatus.Okay},
                 
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(3.0,6.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(1.5,6.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(0.0,5.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0.0,4.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(0.0,4.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(0.0,3.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(0.0,3.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(0.0,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(0.0,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(0.0,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(0.0,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(6.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(6.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(5.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(4.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.4,0.5), IDGrowthRange = new Range<double>(4.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.5,0.6), IDGrowthRange = new Range<double>(3.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.6,0.7), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.7,0.8), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.8,0.9), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.9,1.0), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(1.0,1.1), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(1.1,double.MaxValue), IDGrowthRange = new Range<double>(0.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
            };
            return returnList;
        }

        private static List<RemainingLifeValue> GetRLAHK40()
        {
            //Can't serialize this as it is 'confidential'.
            var returnList = new List<RemainingLifeValue>
            {
                //Age is Excel sheet (named 1-yr currently)
                //PercentInnerGrowthMin is Upper bound of a color (Green, yellow, or red) rounded up (IE: 1 is really the range (.5-1)
                //PercentInnerGrowthMax is Upper bound of a color, rounded up (IE: 1 is really the range (.5-1)
                //First row and first column are removed due to the rounding mechanism, they represent negative growth which are invalid.

                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(double.MinValue,0.2), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.2,0.4), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(double.MinValue,0.2), IDGrowthRange = new Range<double>(1.0,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.2,0.4), IDGrowthRange = new Range<double>(0.5,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.4,0.9), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.9,1.4), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.4,1.9), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.9,2.4), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.0,0.4), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.4,0.9), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(0.9,1.4), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.4,1.9), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(1.9,2.4), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 1, IDGrowthRateRange = new Range<double>(2.4,double.MaxValue), IDGrowthRange = new Range<double>(double.MinValue,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.Okay},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(1.0,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(0.5,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.2,0.4), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.4,0.7), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.9,1.2), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(double.MinValue,0.2), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.2,0.4), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.4,0.7), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.7,0.9), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(0.9,1.2), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 2, IDGrowthRateRange = new Range<double>(1.2,double.MaxValue), IDGrowthRange = new Range<double>(double.MinValue,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(0,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(0.5,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.1,0.3), IDGrowthRange = new Range<double>(double.MinValue,2), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.4,0.6), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.6,0.8), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.1,0.3), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.4,0.6), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.6,0.8), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 3, IDGrowthRateRange = new Range<double>(0.8,double.MaxValue), IDGrowthRange = new Range<double>(double.MinValue,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                

                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(0,0.5), Status = RemainingLifeStatus.Okay},
                
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(0.5,2.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.4,0.6), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                 
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(3.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(2.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.4,0.6), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 4, IDGrowthRateRange = new Range<double>(0.6,double.MaxValue), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                 
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                 
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(double.MinValue,0.1), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 5, IDGrowthRateRange = new Range<double>(0.4,double.MaxValue), IDGrowthRange = new Range<double>(double.MinValue,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(double.MinValue,2.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(double.MinValue,1.5), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(double.MinValue,1.0), Status = RemainingLifeStatus.ConsultEngineering},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(double.MinValue,0.5), Status = RemainingLifeStatus.ConsultEngineering},
                   
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.0,0.1), IDGrowthRange = new Range<double>(2.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.1,0.2), IDGrowthRange = new Range<double>(1.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.2,0.3), IDGrowthRange = new Range<double>(1.0,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.3,0.4), IDGrowthRange = new Range<double>(0.5,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                new RemainingLifeValue {Age = 6, IDGrowthRateRange = new Range<double>(0.4,double.MaxValue), IDGrowthRange = new Range<double>(double.MinValue,double.MaxValue), Status = RemainingLifeStatus.RetireNow},
                 
               
            };
            return returnList;
        }
    }

    

    public struct RemainingLifeValue
    {
        /// <summary> Age represents how long the tube will be in service until the next inspection. </summary>
        internal double Age;
        internal Range<double> IDGrowthRange;
        internal Range<double> IDGrowthRateRange;
        internal RemainingLifeStatus Status { get; set; }
    }

    
}
