﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess
{
    public class RemainingLifeStatus
    {
        //{ NotCalculated, Okay, ConsultEngineering, RetireNow, OutOfBounds }
        public static readonly RemainingLifeStatus NotCalculated = new RemainingLifeStatus("NotCalculated", "Not Calculated");
        public static readonly RemainingLifeStatus Okay = new RemainingLifeStatus("Okay", "Passes Level 1");
        public static readonly RemainingLifeStatus ConsultEngineering = new RemainingLifeStatus("ConsultEngineering", "Consult Engineering");
        public static readonly RemainingLifeStatus RetireNow = new RemainingLifeStatus("RetireNow", "Retire Now or Consult Engineering");
        public static readonly RemainingLifeStatus OutOfBounds = new RemainingLifeStatus("OutOfBounds", "Out of Bounds");

        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public RemainingLifeStatus() { }
        protected RemainingLifeStatus(string TheValue, string TheDisplayName) { Value = TheValue; DisplayName = TheDisplayName; }

        public override string ToString() { return DisplayName; }

        public string Value { get; set; }
        public string DisplayName { get; set;}

        public static List<RemainingLifeStatus> AllPossibleValues
        {
            get { return typeof(RemainingLifeStatus).GetFields().Select(F => F.GetValue(null) as RemainingLifeStatus).Where(F => F != null).ToList(); }
        }
        
        #region Equality Comparing
        //Similar to structure defined in MSDN: http://msdn.microsoft.com/en-us/library/ms173147(v=vs.80).aspx
        //These are utilized because XMLDeserialized versions of statics(like FeatureType.None) will not equality-compare with instantiated versions at runtime as expected.
        //Instead of testing for the same reference to objects, this will check if the Value string of each is the same.

        public override bool Equals(Object obj)
        {
            if (obj == null) return false; // fails existance
            RemainingLifeStatus typedObj = obj as RemainingLifeStatus;
            if (typedObj == null) return false; // fails cast.
            return (string.Equals(Value, typedObj.Value));
        }

        protected bool Equals(RemainingLifeStatus other)
        {
            return string.Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }

        public static bool operator ==(RemainingLifeStatus A, RemainingLifeStatus B)
        {
            if (ReferenceEquals(A, B)) return true; //Means the same instance, or both null.
            if ((object)A == null || (object)B == null) return false; //one is null and one isn't, so not the same.
            return string.Equals(A.Value, B.Value);
        }

        public static bool operator !=(RemainingLifeStatus A, RemainingLifeStatus B)
        {
            return !(A == B);
        }

        #endregion Equality Comparing

        
    }
}
