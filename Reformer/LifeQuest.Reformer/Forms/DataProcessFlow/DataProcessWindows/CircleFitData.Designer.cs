﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class CircleFitData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCircleFit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCircleFit
            // 
            this.btnCircleFit.Location = new System.Drawing.Point(12, 12);
            this.btnCircleFit.Name = "btnCircleFit";
            this.btnCircleFit.Size = new System.Drawing.Size(124, 23);
            this.btnCircleFit.TabIndex = 0;
            this.btnCircleFit.Text = "Circle Fit Each Slice";
            this.btnCircleFit.UseVisualStyleBackColor = true;
            this.btnCircleFit.Click += new System.EventHandler(this.btnCircleFit_Click);
            // 
            // CircleFitData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCircleFit);
            this.Name = "CircleFitData";
            this.Size = new System.Drawing.Size(489, 358);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCircleFit;
    }
}
