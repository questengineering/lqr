﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public sealed partial class ODtoID : BaseProcessWindow
    {
        #region Constructors

        public ODtoID()
        {
            InitializeComponent();
            UpdateDataSource();
            uiCalcMethod.SelectedIndex = 1;
            UpdateDisplayUnits();           
           
        }

        #endregion

        private List<ReformerTube> SelectedTubes => uiTubeSelector.Properties.Items.GetCheckedValues().Select(Tguid => CurrentInspection.Tubes.First(P => P.ID == (Guid)Tguid)).ToList();
        

        public void Run()
        {
            if (SelectedTubes.Count == 0) return;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            InputConstants input = new InputConstants();
            double axialMultiplier = Length.Convert(1, CurrentInspection.Units.LengthUnitScale, DisplayUnits.Instance.AxialDistanceUnits.Scale);
            double measMultiplier = Length.Convert(1, CurrentInspection.Units.LengthUnitScale, DisplayUnits.Instance.MeasurementUnits.Scale);

            input.CalculationType = (CalculationType)Enum.Parse(typeof(CalculationType), uiCalcMethod.EditValue.ToString());
            if (input.CalculationType == CalculationType.Simple)
                input.WallThickness = uiWT.EditValue.ToDouble() * measMultiplier;
            else
            {
                input.InitialLength = uiDesignedLength.EditValue.ToDouble() * axialMultiplier;
                input.RateOfOxidation = uiOxRate.EditValue.ToDouble() * measMultiplier;
                input.FinalLength = uiCurrentLength.EditValue.ToDouble() * axialMultiplier;
            }
            List<string> errorsWithInput = input.ValidateInput();
            if (errorsWithInput.Count == 0)
            {
                OnProgressChanged(0, "Converting OD to ID...");
                ConvertODtoID(input, SelectedTubes);
                OnProgressChanged(90, "Repacking Data...");
                CurrentInspection.RepackData();
                OnProgressChanged(100, "Finished converting OD to ID.");
            }
            else
            {
                string formattedOutput = "";
                errorsWithInput.ForEach(p => formattedOutput += p + "\n\n");
                MessageBox.Show(this, formattedOutput,@"Error with inputs.",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);     
        }
        
        #region Private/Protected Methods

        public override void UpdateDataSource()
        {
            if (CurrentInspection == null) return;
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDataSource));
                return;
            }
            gridValues.DataSource = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
            uiTubeSelector.Properties.DataSource = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
        }               

        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDisplayUnits)); //Restart the method on the proper thread
                return;
            }
            lciWT.Text = string.Format("Wall Thickness ({0})", DefaultValues.MeasurementUnitSymbol);
            lciOxRate.Text = string.Format("Oxidation Rate ({0}/yr)", DefaultValues.MeasurementUnitSymbol);
            lciDesignedLength.Text = string.Format("Initial Length({0})", DefaultValues.AxialUnitSymbol);
            lciCurrentLength.Text = string.Format("Current Length ({0})", DefaultValues.AxialUnitSymbol);

            colNomOD.DisplayFormat.FormatString = colNomID.DisplayFormat.FormatString = colRefID.DisplayFormat.FormatString = colMaxOD.DisplayFormat.FormatString = colMaxID.DisplayFormat.FormatString = "f3"; //show 3 points so the calculations are easier to manually reproduce.
            colIDGrowthRate.DisplayFormat.FormatString = colIDGrowth.DisplayFormat.FormatString = "f2";
        }

        /// <summary> Fills the inner radius scalar array with calcualted values based on the outer radius data and the input constants that have been supplied. </summary>
        private static void ConvertODtoID(InputConstants inputConstants, IEnumerable<ReformerTube> tubes)
        {
            foreach (ReformerTube tube in tubes)
            {
                float[,] outerData;
                tube.Get2DScalarData(ReformerScalar.RadiusOutside, out outerData);
                float[,] innerData = new float[outerData.GetLength(0), outerData.GetLength(1)];
                inputConstants.NomID = tube.Specs.DiameterInside;
                inputConstants.NomOD = tube.Specs.DiameterOutside;
                if (inputConstants.CalculationType == CalculationType.Simple)
                {
                    for (int i = 0; i < outerData.GetLength(0); i++)
                    {
                        for (int j = 0; j < outerData.GetLength(1); j++)
                        {
                            innerData[i, j] = outerData[i, j] - (float)inputConstants.WallThickness;
                        }
                    }
                }
                else if (inputConstants.CalculationType == CalculationType.ConstantVolume)
                {
                    for (int i = 0; i < outerData.GetLength(0); i++)
                    {
                        for (int j = 0; j < outerData.GetLength(1); j++)
                        {
                            innerData[i, j] = ConvertORtoIRUsingConstantVolume(inputConstants, outerData[i,j], tube.Specs.YearsSinceLastInspection);
                        }
                    }
                }
                tube.ReformerAttributes.OxidationRateUsedBaseUnits = inputConstants.RateOfOxidation; //Already in base units
                HDF5DotNet.H5.Close(); //I'm not sure why this is needed. I only know it's related to the PickedLevel1RefODDisplayUnits thing.
                ReformerHdf5Helper.SetTubeData(tube, ReformerScalar.RadiusInside, innerData);
                HDF5DotNet.H5.Close(); //I'm not sure why this is needed. I only know it's related to the PickedLevel1RefODDisplayUnits thing.
            }
        }

        public static float ConvertORtoIRUsingConstantVolume(InputConstants inputConstants, double measOR, decimal ageInYears)
        {
            double measOD = measOR * 2;
            double oxidation = 2 * inputConstants.RateOfOxidation * (double)ageInYears;
            double oxidationAdjustment = Math.Pow(measOD + oxidation, 2);
            double lengthAdjustment = inputConstants.InitialLength / inputConstants.FinalLength;
            double nominalAdjustment = Math.Pow(inputConstants.NomOD, 2) - Math.Pow(inputConstants.NomID, 2); //per Tim Haugen email 3/8/2018, use Ref OD and Nom ID instead of Nom/Nom
            double mainEquation = oxidationAdjustment - lengthAdjustment * nominalAdjustment;
            var answer = (Math.Pow(mainEquation, .5) / 2).ToSingle();
            return answer;
        }

        private void UpdateSampleValues()
        {
            var selectedTubes = SelectedTubes;
            if (selectedTubes.Count <= 0) return;
            var sampleTube = selectedTubes[0];            
        }

        #endregion Private Methods

        #region Event Handlers

        private void btnGo_Click(object sender, EventArgs e)
        {
            Run();
            gridValues.RefreshDataSource();
        }

        private void uiTubeSelector_Validated(object sender, EventArgs e)
        {
            if (SelectedTubes.Count <= 0) return;

            ReformerTube selectedTube = SelectedTubes[0];
            uiDesignedLength.EditValue = Math.Round(selectedTube.LengthInDisplayUnits, 3);
            uiCurrentLength.EditValue = Math.Round(selectedTube.LengthInDisplayUnits, 3);
            uiWT.EditValue = Math.Round(selectedTube.Specs.ThicknessWallInDisplayUnits, 3);
            //Set Ox-rate to the default value given by Tim.
            uiOxRate.EditValue = Length.Convert(DefaultValues.OxidationRate, Length.LengthScale.Inches, DisplayUnits.Instance.MeasurementUnits.Scale);
        }

        private void uiTubeSelector_Closed(object sender, ClosedEventArgs e)
        {
            uiTubeSelector.DoValidate();
            UpdateSampleValues();
        }

        

        #endregion Event Handlers

        #region Enums

        public enum CalculationType
        {
            Simple,
            ConstantVolume
        }
        #endregion Enums

        #region Private Classes

        public class InputConstants
        {           
            
            public double RateOfOxidation { get; set; } = double.NaN;
            public double InitialLength { get; set; } = double.NaN;
            public double FinalLength { get; set; } = double.NaN;
            public double NomOD { get; set; } = double.NaN;
            public double NomID { get; set; } = double.NaN;
            public double WallThickness { get; set; } = double.NaN;
            public CalculationType CalculationType {get; set;}
            
            /// <summary>
            /// Checks for NaN values in the appropriate input based on the calculation type. Also checks if variables make sense.
            /// </summary>
            /// <returns></returns>
            public List<string> ValidateInput()
            {
                List<string> errorStrings = new List<string>();
                if (CalculationType == CalculationType.Simple)
                {
                    if (double.IsNaN(WallThickness)) errorStrings.Add("No Wall Thickness Given.");
                    else if (WallThickness <= 0) errorStrings.Add("Wall Thickness is less than or equal to 0.");
                }
                else if (CalculationType == CalculationType.ConstantVolume)
                {
                    //These first few values should be given initial values by the UI resetting so it always syncs up to data.
                    if (double.IsNaN(RateOfOxidation)) errorStrings.Add("No Oxidation Rate Given.");
                    //These next few have some special requirements to not crash the calculation.
                    if (double.IsNaN(InitialLength)) errorStrings.Add("No Initial Length Given.");
                    else if (FinalLength == 0) errorStrings.Add("Initial Length is equal to 0. To negate length effects, make both initial and final lengths nonzero and equal.");
                   if (double.IsNaN(FinalLength)) errorStrings.Add("No Final Length Given.");
                    else if (FinalLength == 0) errorStrings.Add("Final Length is equal to 0. To negate length effects, make both initial and final lengths nonzero and equal.");
                }
                return errorStrings;
            }            

        }
        
        #endregion private classes

       
    }
}