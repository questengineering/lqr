﻿using System;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSelectionProcess
{
    public partial class WeldSelection : BaseProcessWindow, IHasDisplayUnits
    {
        public WeldSelection()
        {
            InitializeComponent();
        }

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateDataSource));
                return;
            }
            SetControlDataSources();
        }

        private void SetControlDataSources()
        {
            ThePlotControl.SetInspection(CurrentInspection);
        }

        public override void UpdateDisplayUnits()
        {
            ThePlotControl.UpdateDisplayUnits();
        }
    }
}
