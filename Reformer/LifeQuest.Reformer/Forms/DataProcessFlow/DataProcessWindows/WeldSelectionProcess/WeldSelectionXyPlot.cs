﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Nevron.Chart;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Arrays;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Data;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Features;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.UserControls;
using Cursors = System.Windows.Forms.Cursors;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSelectionProcess
{
    public sealed partial class WeldSelectionXyPlot : DataXyPlot, IHasDisplayUnits
    {
        private ReformerTube CurrentTube { get { return TubeSelecter.EditValue as ReformerTube; } }
        private NLineSeries _currentLine;
        /// <summary> Converts the UI sensitivity input to a value usable in the weld pick mechanism. </summary>
        private float WeldPickSensitivity { get { return 1 - (float)uiSensitivity.Value; } }

        public Action<int, string> ProgressChangedHandler
        {
            get;
            private set;
        }

        public WeldSelectionXyPlot(Action<int, string> progressUpdateMethod = null)
        {
            InitializeComponent();
            InitializeChart();
            InitializeDataCursor();
            DraggableVertConstLines = true;
            TheChartControl.KeyDown += TheChartControlOnKeyDown;
            ProgressChangedHandler = progressUpdateMethod;
        }

        internal void SetInspection(ReformerInspection Inspection)
        {
            if (Inspection == null) return;
            BSTube.DataSource = Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.;
            //Set default sensitivity
            uiSensitivity.Value = Inspection.InspectionTool == Tool.LOTIS ? .85m : .6m;
            uiWeldStartNum.Value = 1;// TH wants this to default to 1, but also wants it editable. 3/31/14
            MoveFirst();
        }

        private void TubeSelector_EditValueChanged(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            BSFeatures.DataSource = CurrentTube.Features;
            RedrawAllData();
        }

        private void RedrawAllData()
        {
            Stopwatch sw = Stopwatch.StartNew();
            DrawCurrentTube();
            UpdateYAxis(Double.NaN, Double.NaN, false, true);
            UpdateXAxis(CurrentTube.StartPosition - CurrentTube.Specs.MissedStart, CurrentTube.EndPosition + CurrentTube.Specs.MissedEnd);
            Log.Info(string.Format("Drew Tube Data for {0} in {1} ms", CurrentTube.Name, sw.Elapsed)); sw.Restart();
            DrawWelds();
        }

        /// <summary> Draws only tube lines and applies X and Y labels. </summary>
        private void DrawCurrentTube()
        {
            ClearChart();
            if (CurrentTube == null) return;
            ReformerScalar scalar = CurrentTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside;
            _currentLine = CurrentTube.GetDefaultFormattedLine(scalar, CurrentTube.Inspection.DataMeasure).First();
            for (int i = 0; i < _currentLine.Values.Count; i++) { _currentLine.Values[i] = (double)_currentLine.Values[i] * 2; }
            _currentLine.Name = CurrentTube.Name;
            string xLabel = string.Format("Axial Position ({0})", DefaultValues.AxialUnitSymbol);
            string yLabel = string.Format("{0} ({1})", scalar.DisplayName.ToLower().Contains("inside") ? "Diameter (Inside)" : "Diameter (Outside)", DefaultValues.MeasurementUnitSymbol);
            AddLines(new List<NLineSeries> { _currentLine }, xLabel, yLabel);
            TheChartControl.RecalcLayout();
        }

        private void DrawWelds()
        {
            XAxis.ConstLines.Clear();
            var welds = CurrentTube.Features.Where(F => F.Type == ReformerFeatureType.Weld);
            foreach (var weld in welds)
            {
                NAxisConstLine weldLine = new NAxisConstLine
                {
                    Text = string.IsNullOrWhiteSpace(weld.Label) ? "-" : weld.Label,
                    TextAlignment = ContentAlignment.TopLeft,
                    Value = weld.StartPosition,
                    StrokeStyle = {Color = Color.Blue, Pattern = LinePattern.Dash, Factor = 2, Width = new NLength(3f, NGraphicsUnit.Pixel)}
                };
                XAxis.ConstLines.Add(weldLine);
                weldLine.Tag = weld;
            }
            TheChartControl.RecalcLayout();
        }

        private void UpdateWeldPosition(ReformerFeatureInfo Weld, double PositionInBaseUnits)
        {
            double closestValue;
            Weld.SliceIndexStart = FindClosestDataValueInData(_currentLine.XValues, PositionInBaseUnits, false, out closestValue);
            Weld.SliceIndexEnd = Weld.SliceIndexStart;
        }

        protected override void CursorValueChanged(object sender, EventArgs e)
        {
            if (DraggingLine != null)
            {
                ReformerFeatureInfo movingWeld = (ReformerFeatureInfo)(DraggingLine).Tag;
                UpdateWeldPosition(movingWeld, ((NAxisCursor)XAxis.Cursors[0]).Value);
            }
            base.CursorValueChanged(sender, e);
        }

        protected override void TheChartControl_MouseUp(object sender, MouseEventArgs e)
        {
            if ((Keyboard.GetKeyStates(Key.W) & KeyStates.Down) > 0)
            {
                double xCursorValue = ((NAxisCursor)XAxis.Cursors[0]).Value;
                AddWeldToCurrentTube(xCursorValue);
            }
            else
                base.TheChartControl_MouseUp(sender, e);
        }

        private void AddWeldToCurrentTube(double XinBaseUnits)
        {
            if (_currentLine == null || CurrentTube == null) return;

            //Make sure the value isn't outside of the valid range by constraining to the current X Values.
            double constrainedValue = XinBaseUnits.Constrain(_currentLine.XValues[0].ToDouble(), _currentLine.XValues[_currentLine.XValues.Count - 1].ToDouble());
            double unusedValue;
            int slice = FindClosestDataValueInData(_currentLine.XValues, constrainedValue, true, out unusedValue);

            AddWeldToThisTube(CurrentTube, slice);
            if (chkAutoName.Checked) AutoNameWelds(CurrentTube);
            DrawWelds();
        }

        private static void AddWeldToThisTube(ReformerTube tube, int slice)
        {
            ReformerFeatureInfo newFeature = new ReformerFeatureInfo(tube)
            {
                Type = ReformerFeatureType.Weld,
                SliceIndexStart = slice,
                SliceIndexEnd = slice
            };

            tube.Features.Add(newFeature);
        }
        
        private void AutoNameWelds(ReformerTube tube)
        {
            const string prefix = "W-";
            //Get the welds in the tube sorted from bottom to top.
            var welds = tube.Features.Where(F => F.Type == ReformerFeatureType.Weld).OrderByDescending(F => F.SliceIndexStart).ToList();
            for (int i = 0; i < welds.Count; i++) { welds[i].Label = string.Format("{0}{1}", prefix, i + uiWeldStartNum.Value); }
        }

        private void TheGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Delete) return;

            TheView.DeleteSelectedRows();
            if (chkAutoName.Checked) AutoNameWelds(CurrentTube);
            TheChartControl.Focus();
            DrawWelds();
        }

        internal void MoveFirst()
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            TubeSelecter.EditValue = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber).First();
        }

        internal void MoveNext()
        {
            if (CurrentInspection == null) return;
            ReformerTube currentTube = (ReformerTube)TubeSelecter.EditValue;
            var nextKey = TubeSelecter.Properties.GetIndexByKeyValue(currentTube) + 1;
            var nextItem = TubeSelecter.Properties.GetKeyValue(nextKey);
            if (nextItem != null) TubeSelecter.EditValue = nextItem;
        }

        internal void MovePrevious()
        {
            if (CurrentInspection == null) return;
            int prevKey = 0;
            if (TubeSelecter.EditValue != null) prevKey = TubeSelecter.Properties.GetIndexByKeyValue(TubeSelecter.EditValue) - 1;
            var prevItem = TubeSelecter.Properties.GetKeyValue(prevKey);
            if (prevItem != null) TubeSelecter.EditValue = prevItem;
        }

        private void TheChartControlOnKeyDown(object Sender, KeyEventArgs Args)
        {
            if (Args.KeyCode == Keys.Q) MovePrevious();
            else if (Args.KeyCode == Keys.E) MoveNext();
            else if (Args.KeyCode == Keys.Space) DeleteRest();
        }

        /// <summary> A hacked in way to trim data. Should probably be moved out of here at some point. </summary>
        private void DeleteRest()
        {
            double xCursorValue = ((NAxisCursor)XAxis.Cursors[0]).Value;
            double unusedValue;
            int slice = FindClosestDataValueInData(_currentLine.XValues, xCursorValue, true, out unusedValue);
            var axialData = CurrentTube.Positions.GetRange(0, slice);
            ReformerHdf5Helper.SetTubeData(CurrentTube, ReformerScalar.AxialPosition, axialData);

            if (CurrentTube.AvailableScalars.Contains(ReformerScalar.RadiusInside))
            {
                float[,] radiusInsideData;
                CurrentTube.Get2DScalarData(ReformerScalar.RadiusInside, out radiusInsideData);
                float[,] trimmedRadiusInsideData = new float[slice, CurrentInspection.NumRadialReadings];
                for (int idx = 0; idx < slice; idx++)
                {
                    for (int sensor = 0; sensor < CurrentInspection.NumRadialReadings; sensor++)
                    {
                        trimmedRadiusInsideData[idx, sensor] = radiusInsideData[idx, sensor];
                    }
                }

                ReformerHdf5Helper.SetTubeData(CurrentTube, ReformerScalar.RadiusInside, trimmedRadiusInsideData);
            }
            if (CurrentTube.AvailableScalars.Contains(ReformerScalar.RadiusOutside))
            {
                float[,] radiusOutsideData;
                CurrentTube.Get2DScalarData(ReformerScalar.RadiusOutside, out radiusOutsideData);
                float[,] trimmedRadiusOutsideData = new float[slice, CurrentInspection.NumRadialReadings];
                for (int idx = 0; idx < slice; idx++)
                {
                    for (int sensor = 0; sensor < CurrentInspection.NumRadialReadings; sensor++)
                    {
                        trimmedRadiusOutsideData[idx, sensor] = radiusOutsideData[idx, sensor];
                    }
                }

                ReformerHdf5Helper.SetTubeData(CurrentTube, ReformerScalar.RadiusOutside, trimmedRadiusOutsideData);
            }

            //Remove all things picked beyond the end. Shouldn't be any, but who knows!
            var featuresToRemove = CurrentTube.Features.Where(f => f.SliceIndexStart >= slice).ToList();
            featuresToRemove.ForEach(f => CurrentTube.Features.Remove(f));

            //Invalidate picked slices as necesary
            if (CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice > slice)
                CurrentTube.ReformerAttributes.PickedOverallGrowthRefSlice = ReformerData.DefaultInt;
            if (CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice > slice)
                CurrentTube.ReformerAttributes.PickedLevel1GrowthRefSlice = ReformerData.DefaultInt;
            
            RedrawAllData();
        }

        private void TubeSelecterView_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column == colWeldPicked && e.Row != null)
                e.Value = ((ReformerTube)e.Row).Features.Count(F => F.Type == ReformerFeatureType.Weld) > 0;
        }

        public void UpdateDisplayUnits()
        {
            RedrawAllData();
        }

        private async void btnAutoPickAllWelds_Click(object sender, EventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            var answer = MessageBox.Show(this, @"Pick new Welds for all tubes in the current inspections? This is irreversible.", @"Pick New Welds?", MessageBoxButtons.YesNo);
            if (answer != DialogResult.Yes) return;

            BSFeatures.DataSource = null;
            Enabled = false;
            Cursor = Cursors.WaitCursor;
            await PickWelds(WeldPickSensitivity, CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)));

            BSFeatures.DataSource = CurrentTube.Features;
            RedrawAllData();
            Cursor = Cursors.Default;
            Enabled = true;
        }

        private Task PickWelds(float sensitivity, IEnumerable<ReformerTube> tubes )
        {
            Task pickTask = new Task(() =>
            {
                ReformerScalar scalar = CurrentInspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside;
                int tubeCounter = 0;
                foreach (ReformerTube tube in tubes)
                {
                    //Remove all features from the tube.
                    var welds = tube.Features.Where(F => F.Type == ReformerFeatureType.Weld).ToList();
                    foreach (ReformerFeatureInfo f in welds)
                    {
                        tube.Features.Remove(f);
                    }

                    //Don't allow welds to be picked within the first few slices of data.
                    List<float> scalarData = tube.Get2DScalarStatisticalData(scalar, tube.Inspection.DataMeasure).First().Value;
                    List<Tuple<int, float>> sliceDiffs = new List<Tuple<int, float>>();
                    int startSlice = 30, endSlice = scalarData.Count - 20; //Watch out for data on the ends that may bounce around a bit too much

                    for (int slice = startSlice; slice < endSlice; slice++)
                    {
                        sliceDiffs.Add(new Tuple<int, float>(slice, scalarData[slice] - scalarData[slice - 1]));
                    }

                    //Order the slices by their difference. from the previous We're looking for higest diff, so highest number first is best.
                    sliceDiffs = sliceDiffs.OrderByDescending(T => Math.Abs(T.Item2)).ToList();
                    float meanSliceDiff = sliceDiffs.Select(F => Math.Abs(F.Item2)).Median();
                    float maxSliceDiff = Math.Abs(sliceDiffs[0].Item2);

                    //Figure out the minimum drop that's acceptable for a weld. Constrain it in case data gets weird.
                    float smallestWeldDiff = sensitivity * (maxSliceDiff - meanSliceDiff);

                    List<int> slicesChecked = new List<int>();
                    foreach (Tuple<int, float> diffPoint in sliceDiffs)
                    {
                        //Don't allow more than 5 welds.
                        if (tube.Features.Count(F => F.Type == ReformerFeatureType.Weld) > 4) break; 

                        //Stop checking if the diffs aren't big enough anymore
                        if (Math.Abs(diffPoint.Item2) < smallestWeldDiff) break;
                        //Skip to the next if the current drop is within certain slices in either direction of a previously checked weld location.
                        int nearMinSlice = Math.Max(diffPoint.Item1 - 10, startSlice);
                        int mearMaxSlice = Math.Min(diffPoint.Item1 + 10, endSlice);
                        int nearerMinSlice = Math.Max(diffPoint.Item1 - 5, startSlice);
                        int nearerMaxSlice = Math.Min(diffPoint.Item1 + 5, endSlice);

                        //Don't allow welds within 50 slices in each direction of other welds. But only look for weld values within 10 in case tube shows severe bulging or swelling.
                        if (slicesChecked.Any(p => p.Between(nearMinSlice - 50, mearMaxSlice + 50)))
                            continue;

                        List<float> nearbyData = scalarData.GetRange(nearMinSlice, mearMaxSlice - nearMinSlice + 1).ToList();
                        List<float> nearerbyData = scalarData.GetRange(nearerMinSlice, nearerMaxSlice - nearerMinSlice + 1).ToList();
                        
                        //Check if the mean value of data is above or below the surrounding mean value to determine if we should be looking for an upward or downward thing.
                        float nearestWeldValue = Math.Abs(nearerbyData.Max() - nearbyData.Mean()) > Math.Abs(nearerbyData.Min() - nearbyData.Mean()) ? nearerbyData.Max() : nearerbyData.Min();
                        int nearestWeldIndex = nearMinSlice + nearbyData.IndexOf(nearestWeldValue);
                        slicesChecked.Add(nearestWeldIndex);

                        AddWeldToThisTube(tube, nearestWeldIndex);
                    }
                    AutoNameWelds(tube);

                    int progressNum = (int)Math.Round(++tubeCounter / (float)CurrentInspection.Tubes.Count * 100);
                    string progressText = string.Format("Found welds {0} welds for {1}", tube.Features.Count(F => F.Type == ReformerFeatureType.Weld), tube.Name);
                    ProgressChangedHandler.Invoke(progressNum, progressText);
                }
                ProgressChangedHandler.Invoke(100, "Finished picking welds");
            });
            pickTask.Start();
            return pickTask;
        }

        private async void btnAutoPickThis_Click(object sender, EventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            BSFeatures.DataSource = null;
            Enabled = false;
            Cursor = Cursors.WaitCursor;
            await PickWelds(WeldPickSensitivity, new List<ReformerTube> { CurrentTube });

            BSFeatures.DataSource = CurrentTube.Features;
            RedrawAllData();
            Cursor = Cursors.Default;
            Enabled = true;
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            if (CurrentInspection == null || CurrentInspection.Tubes.Count == 0) return;
            var answer = MessageBox.Show(this, @"Clear welds for all tubes in the current inspections? This is irreversible.", @"Clear Welds?", MessageBoxButtons.YesNo);
            if (answer != DialogResult.Yes) return;

            BSFeatures.DataSource = null;
            Enabled = false;
            Cursor = Cursors.WaitCursor;
            foreach (var tube in CurrentInspection.Tubes)
            {
                tube.Features = tube.Features.Where(f => f.Type != ReformerFeatureType.Weld).ToBindingList();
            }

            BSFeatures.DataSource = CurrentTube.Features;
            RedrawAllData();
            Cursor = Cursors.Default;
            Enabled = true;
        }

        private void btnClearTubeWelds_Click(object sender, EventArgs e)
        {
            BSFeatures.DataSource = null;
            Enabled = false;
            Cursor = Cursors.WaitCursor;

            CurrentTube.Features = CurrentTube.Features.Where(f => f.Type != ReformerFeatureType.Weld).ToBindingList();
            
            BSFeatures.DataSource = CurrentTube.Features;
            RedrawAllData();
            Cursor = Cursors.Default;
            Enabled = true;
        }
    }
}
