﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSelectionProcess
{
    sealed partial class WeldSelectionXyPlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeldSelectionXyPlot));
            this.TubeSelecter = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSTube = new System.Windows.Forms.BindingSource(this.components);
            this.TubeSelecterView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeldPicked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkAutoName = new DevExpress.XtraEditors.CheckEdit();
            this.TheGrid = new DevExpress.XtraGrid.GridControl();
            this.BSFeatures = new System.Windows.Forms.BindingSource(this.components);
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPositionInDisplayUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.uiNote = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciWeldStartNum = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiWeldStartNum = new DevExpress.XtraEditors.SpinEdit();
            this.lciTubes = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFeatures = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnAutoPickAllWelds = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.uiSensitivity = new DevExpress.XtraEditors.SpinEdit();
            this.lciSensitivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnAutoPickThis = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnClearAll = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnClearTubeWelds = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecterView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSFeatures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWeldStartNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiWeldStartNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFeatures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiSensitivity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSensitivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // TheChartControl
            // 
            this.TheChartControl.Size = new System.Drawing.Size(323, 283);
            this.TheChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("TheChartControl.State")));
            // 
            // DataCursorView
            // 
            this.DataCursorView.Location = new System.Drawing.Point(479, 55);
            this.DataCursorView.Size = new System.Drawing.Size(190, 67);
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.labelControl2);
            this.lcMain.Controls.Add(this.btnClearTubeWelds);
            this.lcMain.Controls.Add(this.btnClearAll);
            this.lcMain.Controls.Add(this.btnAutoPickThis);
            this.lcMain.Controls.Add(this.uiSensitivity);
            this.lcMain.Controls.Add(this.btnAutoPickAllWelds);
            this.lcMain.Controls.Add(this.labelControl1);
            this.lcMain.Controls.Add(this.uiNote);
            this.lcMain.Controls.Add(this.TheGrid);
            this.lcMain.Controls.Add(this.TubeSelecter);
            this.lcMain.Controls.Add(this.chkAutoName);
            this.lcMain.Controls.Add(this.uiWeldStartNum);
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2687, 113, 250, 350);
            this.lcMain.Size = new System.Drawing.Size(681, 447);
            this.lcMain.Controls.SetChildIndex(this.uiWeldStartNum, 0);
            this.lcMain.Controls.SetChildIndex(this.DataCursorView, 0);
            this.lcMain.Controls.SetChildIndex(this.PlotPanel, 0);
            this.lcMain.Controls.SetChildIndex(this.chkAutoName, 0);
            this.lcMain.Controls.SetChildIndex(this.TubeSelecter, 0);
            this.lcMain.Controls.SetChildIndex(this.TheGrid, 0);
            this.lcMain.Controls.SetChildIndex(this.uiNote, 0);
            this.lcMain.Controls.SetChildIndex(this.labelControl1, 0);
            this.lcMain.Controls.SetChildIndex(this.btnAutoPickAllWelds, 0);
            this.lcMain.Controls.SetChildIndex(this.uiSensitivity, 0);
            this.lcMain.Controls.SetChildIndex(this.btnAutoPickThis, 0);
            this.lcMain.Controls.SetChildIndex(this.btnClearAll, 0);
            this.lcMain.Controls.SetChildIndex(this.btnClearTubeWelds, 0);
            this.lcMain.Controls.SetChildIndex(this.labelControl2, 0);
            // 
            // PlotPanel
            // 
            this.PlotPanel.Size = new System.Drawing.Size(468, 433);
            // 
            // lcgMain
            // 
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.lciTubes,
            this.lciFeatures,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.lciWeldStartNum,
            this.lciSensitivity,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.lcgMain.Size = new System.Drawing.Size(681, 447);
            // 
            // lciDataCursorView
            // 
            this.lciDataCursorView.Location = new System.Drawing.Point(472, 48);
            this.lciDataCursorView.Size = new System.Drawing.Size(194, 71);
            // 
            // splitterItem1
            // 
            this.splitterItem1.Location = new System.Drawing.Point(666, 0);
            this.splitterItem1.Size = new System.Drawing.Size(5, 437);
            // 
            // lciPlotPanel
            // 
            this.lciPlotPanel.Size = new System.Drawing.Size(472, 437);
            // 
            // TubeSelecter
            // 
            this.TubeSelecter.Location = new System.Drawing.Point(532, 7);
            this.TubeSelecter.Name = "TubeSelecter";
            this.TubeSelecter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TubeSelecter.Properties.DataSource = this.BSTube;
            this.TubeSelecter.Properties.DisplayMember = "Name";
            this.TubeSelecter.Properties.NullText = "";
            this.TubeSelecter.Properties.View = this.TubeSelecterView;
            this.TubeSelecter.Size = new System.Drawing.Size(137, 20);
            this.TubeSelecter.StyleController = this.lcMain;
            this.TubeSelecter.TabIndex = 4;
            this.TubeSelecter.EditValueChanged += new System.EventHandler(this.TubeSelector_EditValueChanged);
            // 
            // BSTube
            // 
            this.BSTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // TubeSelecterView
            // 
            this.TubeSelecterView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTubeNum,
            this.colWeldPicked});
            this.TubeSelecterView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.TubeSelecterView.Name = "TubeSelecterView";
            this.TubeSelecterView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.TubeSelecterView.OptionsView.ShowGroupPanel = false;
            this.TubeSelecterView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.TubeSelecterView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.TubeSelecterView_CustomUnboundColumnData);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "ReformerAttributes.RowNumber";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 2;
            // 
            // colTubeNum
            // 
            this.colTubeNum.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum.Caption = "Tube #";
            this.colTubeNum.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum.Name = "colTubeNum";
            this.colTubeNum.Visible = true;
            this.colTubeNum.VisibleIndex = 3;
            // 
            // colWeldPicked
            // 
            this.colWeldPicked.Caption = "Picked?";
            this.colWeldPicked.FieldName = "WeldPicked";
            this.colWeldPicked.Name = "colWeldPicked";
            this.colWeldPicked.OptionsColumn.AllowEdit = false;
            this.colWeldPicked.OptionsColumn.AllowFocus = false;
            this.colWeldPicked.OptionsColumn.ReadOnly = true;
            this.colWeldPicked.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.colWeldPicked.Visible = true;
            this.colWeldPicked.VisibleIndex = 0;
            // 
            // chkAutoName
            // 
            this.chkAutoName.EditValue = true;
            this.chkAutoName.Location = new System.Drawing.Point(479, 31);
            this.chkAutoName.Name = "chkAutoName";
            this.chkAutoName.Properties.Caption = "AutoName";
            this.chkAutoName.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chkAutoName.Size = new System.Drawing.Size(72, 19);
            this.chkAutoName.StyleController = this.lcMain;
            this.chkAutoName.TabIndex = 5;
            this.chkAutoName.ToolTip = "Determines if all welds in a tube are auto-named as new welds are added, assuming" +
    " the right side of the data is the bottom of the tube.";
            // 
            // TheGrid
            // 
            this.TheGrid.DataSource = this.BSFeatures;
            this.TheGrid.Location = new System.Drawing.Point(479, 218);
            this.TheGrid.MainView = this.TheView;
            this.TheGrid.Name = "TheGrid";
            this.TheGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.MemoEdit});
            this.TheGrid.Size = new System.Drawing.Size(190, 171);
            this.TheGrid.TabIndex = 7;
            this.TheGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            this.TheGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TheGrid_KeyDown);
            // 
            // BSFeatures
            // 
            this.BSFeatures.DataSource = typeof(Reformer.Data.Features.ReformerFeatureInfo);
            this.BSFeatures.Filter = "";
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabel,
            this.colPositionInDisplayUnits,
            this.colNotes});
            this.TheView.GridControl = this.TheGrid;
            this.TheView.Name = "TheView";
            this.TheView.OptionsCustomization.AllowGroup = false;
            this.TheView.OptionsSelection.MultiSelect = true;
            this.TheView.OptionsView.ShowGroupPanel = false;
            this.TheView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPositionInDisplayUnits, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colLabel
            // 
            this.colLabel.AppearanceCell.Options.UseTextOptions = true;
            this.colLabel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLabel.AppearanceHeader.Options.UseTextOptions = true;
            this.colLabel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLabel.FieldName = "Label";
            this.colLabel.Name = "colLabel";
            this.colLabel.Visible = true;
            this.colLabel.VisibleIndex = 0;
            // 
            // colPositionInDisplayUnits
            // 
            this.colPositionInDisplayUnits.AppearanceCell.Options.UseTextOptions = true;
            this.colPositionInDisplayUnits.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPositionInDisplayUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colPositionInDisplayUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPositionInDisplayUnits.Caption = "Axial";
            this.colPositionInDisplayUnits.DisplayFormat.FormatString = "N3";
            this.colPositionInDisplayUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPositionInDisplayUnits.FieldName = "PositionInDisplayUnits";
            this.colPositionInDisplayUnits.Name = "colPositionInDisplayUnits";
            this.colPositionInDisplayUnits.OptionsColumn.AllowEdit = false;
            this.colPositionInDisplayUnits.OptionsColumn.ReadOnly = true;
            this.colPositionInDisplayUnits.Visible = true;
            this.colPositionInDisplayUnits.VisibleIndex = 1;
            // 
            // colNotes
            // 
            this.colNotes.AppearanceCell.Options.UseTextOptions = true;
            this.colNotes.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNotes.AppearanceHeader.Options.UseTextOptions = true;
            this.colNotes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNotes.ColumnEdit = this.MemoEdit;
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 2;
            // 
            // MemoEdit
            // 
            this.MemoEdit.AutoHeight = false;
            this.MemoEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MemoEdit.Name = "MemoEdit";
            // 
            // uiNote
            // 
            this.uiNote.Location = new System.Drawing.Point(479, 393);
            this.uiNote.Name = "uiNote";
            this.uiNote.Size = new System.Drawing.Size(146, 13);
            this.uiNote.StyleController = this.lcMain;
            this.uiNote.TabIndex = 8;
            this.uiNote.Text = "W+Click: New Draggable Weld";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.uiNote;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 249);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(195, 17);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(479, 427);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 13);
            this.labelControl1.StyleController = this.lcMain;
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Q/E: Previous/Next Tube";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControl1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 266);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(195, 17);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkAutoName;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(472, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(76, 24);
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // lciWeldStartNum
            // 
            this.lciWeldStartNum.Control = this.uiWeldStartNum;
            this.lciWeldStartNum.CustomizationFormText = "layoutControlItem5";
            this.lciWeldStartNum.Location = new System.Drawing.Point(548, 24);
            this.lciWeldStartNum.Name = "lciWeldStartNum";
            this.lciWeldStartNum.OptionsToolTip.ToolTip = "The number to start numbering welds at. MANTIS usually starts at 2 because the fi" +
    "rst is not visible in data.";
            this.lciWeldStartNum.Size = new System.Drawing.Size(118, 24);
            this.lciWeldStartNum.Text = "Start";
            this.lciWeldStartNum.TextSize = new System.Drawing.Size(49, 13);
            // 
            // uiWeldStartNum
            // 
            this.uiWeldStartNum.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiWeldStartNum.Location = new System.Drawing.Point(608, 31);
            this.uiWeldStartNum.Name = "uiWeldStartNum";
            this.uiWeldStartNum.Properties.Appearance.Options.UseTextOptions = true;
            this.uiWeldStartNum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.uiWeldStartNum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiWeldStartNum.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.uiWeldStartNum.Properties.IsFloatValue = false;
            this.uiWeldStartNum.Properties.Mask.EditMask = "N00";
            this.uiWeldStartNum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.uiWeldStartNum.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uiWeldStartNum.Size = new System.Drawing.Size(61, 20);
            this.uiWeldStartNum.StyleController = this.lcMain;
            this.uiWeldStartNum.TabIndex = 6;
            // 
            // lciTubes
            // 
            this.lciTubes.Control = this.TubeSelecter;
            this.lciTubes.CustomizationFormText = "Tube";
            this.lciTubes.Location = new System.Drawing.Point(472, 0);
            this.lciTubes.Name = "lciTubes";
            this.lciTubes.Size = new System.Drawing.Size(194, 24);
            this.lciTubes.Text = "Tube";
            this.lciTubes.TextSize = new System.Drawing.Size(49, 13);
            // 
            // lciFeatures
            // 
            this.lciFeatures.Control = this.TheGrid;
            this.lciFeatures.CustomizationFormText = "Features";
            this.lciFeatures.Location = new System.Drawing.Point(472, 195);
            this.lciFeatures.Name = "lciFeatures";
            this.lciFeatures.Size = new System.Drawing.Size(194, 191);
            this.lciFeatures.Text = "Features";
            this.lciFeatures.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciFeatures.TextSize = new System.Drawing.Size(49, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.uiNote;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(472, 386);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.labelControl1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(472, 420);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // btnAutoPickAllWelds
            // 
            this.btnAutoPickAllWelds.Location = new System.Drawing.Point(574, 150);
            this.btnAutoPickAllWelds.Name = "btnAutoPickAllWelds";
            this.btnAutoPickAllWelds.Size = new System.Drawing.Size(95, 22);
            this.btnAutoPickAllWelds.StyleController = this.lcMain;
            this.btnAutoPickAllWelds.TabIndex = 10;
            this.btnAutoPickAllWelds.Text = "AutoPick All";
            this.btnAutoPickAllWelds.ToolTip = "Replace all current weld selections in all tubes and attempt to pick welds automa" +
    "tically";
            this.btnAutoPickAllWelds.Click += new System.EventHandler(this.btnAutoPickAllWelds_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnAutoPickAllWelds;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(567, 143);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(99, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // uiSensitivity
            // 
            this.uiSensitivity.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.uiSensitivity.Location = new System.Drawing.Point(619, 126);
            this.uiSensitivity.Name = "uiSensitivity";
            this.uiSensitivity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiSensitivity.Properties.DisplayFormat.FormatString = "N2";
            this.uiSensitivity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiSensitivity.Properties.EditFormat.FormatString = "N2";
            this.uiSensitivity.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiSensitivity.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.uiSensitivity.Properties.Mask.EditMask = "N2";
            this.uiSensitivity.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.uiSensitivity.Size = new System.Drawing.Size(50, 20);
            this.uiSensitivity.StyleController = this.lcMain;
            this.uiSensitivity.TabIndex = 11;
            // 
            // lciSensitivity
            // 
            this.lciSensitivity.Control = this.uiSensitivity;
            this.lciSensitivity.CustomizationFormText = "Sensitivity";
            this.lciSensitivity.Location = new System.Drawing.Point(559, 119);
            this.lciSensitivity.Name = "lciSensitivity";
            this.lciSensitivity.Size = new System.Drawing.Size(107, 24);
            this.lciSensitivity.Text = "Sensitivity";
            this.lciSensitivity.TextSize = new System.Drawing.Size(49, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(472, 119);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(87, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnAutoPickThis
            // 
            this.btnAutoPickThis.Location = new System.Drawing.Point(479, 150);
            this.btnAutoPickThis.Name = "btnAutoPickThis";
            this.btnAutoPickThis.Size = new System.Drawing.Size(91, 22);
            this.btnAutoPickThis.StyleController = this.lcMain;
            this.btnAutoPickThis.TabIndex = 12;
            this.btnAutoPickThis.Text = "AutoPick This";
            this.btnAutoPickThis.Click += new System.EventHandler(this.btnAutoPickThis_Click);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnAutoPickThis;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(472, 143);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(95, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(574, 176);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(95, 22);
            this.btnClearAll.StyleController = this.lcMain;
            this.btnClearAll.TabIndex = 13;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnClearAll;
            this.layoutControlItem8.Location = new System.Drawing.Point(567, 169);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(99, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // btnClearTubeWelds
            // 
            this.btnClearTubeWelds.Location = new System.Drawing.Point(479, 176);
            this.btnClearTubeWelds.Name = "btnClearTubeWelds";
            this.btnClearTubeWelds.Size = new System.Drawing.Size(91, 22);
            this.btnClearTubeWelds.StyleController = this.lcMain;
            this.btnClearTubeWelds.TabIndex = 14;
            this.btnClearTubeWelds.Text = "Clear This";
            this.btnClearTubeWelds.Click += new System.EventHandler(this.btnClearTubeWelds_Click);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnClearTubeWelds;
            this.layoutControlItem9.Location = new System.Drawing.Point(472, 169);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(95, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(479, 410);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(140, 13);
            this.labelControl2.StyleController = this.lcMain;
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Space: Delete Following Data";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControl2;
            this.layoutControlItem10.Location = new System.Drawing.Point(472, 403);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsToolTip.ToolTip = "Delete data to the right of the selected point. No undo available, so be careful!" +
    "";
            this.layoutControlItem10.Size = new System.Drawing.Size(194, 17);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // WeldSelectionXyPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "WeldSelectionXyPlot";
            this.Size = new System.Drawing.Size(681, 447);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDataCursorView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPlotPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TubeSelecterView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSFeatures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciWeldStartNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiWeldStartNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFeatures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiSensitivity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSensitivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit TubeSelecter;
        private DevExpress.XtraGrid.Views.Grid.GridView TubeSelecterView;
        private System.Windows.Forms.BindingSource BSTube;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum;
        private DevExpress.XtraEditors.CheckEdit chkAutoName;
        private DevExpress.XtraGrid.GridControl TheGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private System.Windows.Forms.BindingSource BSFeatures;
        private DevExpress.XtraGrid.Columns.GridColumn colLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionInDisplayUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit MemoEdit;
        private DevExpress.XtraEditors.LabelControl uiNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem lciWeldStartNum;
        private DevExpress.XtraLayout.LayoutControlItem lciTubes;
        private DevExpress.XtraLayout.LayoutControlItem lciFeatures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn colWeldPicked;
        private DevExpress.XtraEditors.SpinEdit uiWeldStartNum;
        private DevExpress.XtraEditors.SimpleButton btnAutoPickAllWelds;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SpinEdit uiSensitivity;
        private DevExpress.XtraLayout.LayoutControlItem lciSensitivity;
        private DevExpress.XtraEditors.SimpleButton btnAutoPickThis;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton btnClearAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnClearTubeWelds;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
