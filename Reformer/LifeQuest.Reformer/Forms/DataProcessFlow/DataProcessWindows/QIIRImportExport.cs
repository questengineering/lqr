﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using Ionic.Zip;
using NGenerics.Extensions;
using QuestIntegrity.Core.Extensions.Strings;
using Reformer.Data.DataManager;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.InspectionFile;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class QiirImportExport : BaseProcessWindow
    {
        private static ReformerDataManager TheDataManager { get { return LifeQuestReformer.Instance.TheDataManager; } }
        readonly OpenFileDialog _fileDialog = new OpenFileDialog();

        public QiirImportExport()
        {
            InitializeComponent();
            SetupFileDialog();
        }

        private void SetupFileDialog()
        {
            _fileDialog.Filter = @"LQR Inspection File|*.qiir";
            _fileDialog.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            _fileDialog.Multiselect = false;
        }

        private void uiInspections_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Caption != "Delete") return;
            e.Button.Enabled = false;
            DeleteInspection(uiInspections.EditValue as ReformerInspection);
            e.Button.Enabled = true;
        }

        private void uiAddBlankInspection_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (CurrentProject == null) return;
            e.Button.Enabled = false;
            CreateNewInspection(uiAddBlankInspection.Text);
            e.Button.Enabled = true;
        }

        private void uiImportInspection_Click(object sender, EventArgs e)
        {
            if (CurrentProject == null) return;
            uiImportInspection.Enabled = false;
            ImportInspection();
            uiImportInspection.Enabled = true;
        }
            
        private void ImportInspection()
        {
            if (_fileDialog.ShowDialog(ParentForm) != DialogResult.OK) return;
            Stopwatch sw = Stopwatch.StartNew();
            string tempPath = Path.Combine(Path.GetTempPath(), "LQRExtraction");
            List<string> embeddedfiles;
            using (ZipFile zipFile = ZipFile.Read(_fileDialog.OpenFile()))
            {
                zipFile.ExtractAll(tempPath, ExtractExistingFileAction.OverwriteSilently);
                embeddedfiles = zipFile.EntryFileNames.ToList();
            }
            FileInfo tempDataFile = new FileInfo(Path.Combine(tempPath, embeddedfiles.First(F => F.Contains(Resources.FileExtensions.HDF5Extension))));
            FileInfo tempInspFile = new FileInfo(Path.Combine(tempPath, embeddedfiles.First(F => F.Contains(Resources.FileExtensions.StandaloneInspection))));
            //Deserialize the inspection file.
            FileStream fs = new FileStream(tempInspFile.FullName, FileMode.Open);
            XmlSerializer serializer = new XmlSerializer(typeof(ReformerInspection));
            ReformerInspection loadedInspection = (ReformerInspection)serializer.Deserialize(fs);
            LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Add(loadedInspection);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            fs.Close();
            //Copy over the data file.
            FileInfo destFile = new FileInfo(Path.Combine(CurrentProject.DirectoryPath, tempDataFile.Name));
            if (destFile.Exists) throw new Exception(string.Format("Data file with the same name '{0}' already exists.", destFile.FullName));
            File.Copy(tempDataFile.FullName, destFile.FullName, true);
            //Cleanup
            tempDataFile.Delete();
            tempInspFile.Delete();
            OnProgressChanged(100, string.Format("Loaded the inspection file '{0}' in {1} s", loadedInspection.Name, sw.Elapsed.TotalSeconds.ToString("N1")));
        }

        private void uiExportInspection_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Caption != "Export") return;
            e.Button.Enabled = false;
            ExportInspection(uiExportInspection.EditValue as ReformerInspection);
            e.Button.Enabled = true;
        }

        #region Private Methods

        /// <summary> Exports an inspection to a .Qiir file, basically a zip file with a serialized inspection and a copy of the data file. </summary>
        private void ExportInspection(ReformerInspection TheInspection)
        {
            if (TheInspection == null) return;
            Stopwatch sw = Stopwatch.StartNew();
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            XmlSerializer serializer = new XmlSerializer(typeof(ReformerInspection));
            FileInfo tmpFile = new FileInfo(Path.Combine(Path.GetTempPath(), TheInspection.Name + Resources.FileExtensions.StandaloneInspection));
            if (tmpFile.Exists) tmpFile.Delete();

            FileStream fs = new FileStream(tmpFile.FullName, FileMode.CreateNew);
            serializer.Serialize(fs, TheInspection);
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(tmpFile.FullName);
                zip.AddFile(TheInspection.DataFileFullName);
                string exportName = Path.Combine(CurrentProject.DirectoryPath, TheInspection.Name + "_" + DateTime.Now.ToString("d-MM-yy_HHmmss") + Resources.FileExtensions.StandaloneInspection);
                zip.Save(exportName);
            }

            fs.Close();
            tmpFile.Delete();//Clean up
            
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
            OnProgressChanged(100, string.Format("Exported {0} in {1} s", TheInspection.Name, sw.Elapsed.TotalSeconds.ToString("N1")));
        }

        /// <summary> Deletes the Current Inspection. </summary>
        private void DeleteInspection(ReformerInspection TheInspection)
        {
            if (CurrentInspection == null) return;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            if (DialogResult.Yes == MessageBox.Show(ParentForm, string.Format("Are you sure you would like to delete inspection: {0} and its associated data file?", TheInspection.Name), @"Delete Inspection", MessageBoxButtons.YesNo))
                TheInspection.Delete();
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
        }

        /// <summary> Checks for name validity and creates an inspection with that given name. </summary>
        private void CreateNewInspection(string NewInspectionName)
        {
            //Check for invalid input
            if (NewInspectionName.IsNullOrEmpty() || CurrentProject.InspectionFiles.Any(Insp => Insp.Name == NewInspectionName))
            {
                OnProgressChanged(100, string.Format("Cannot create Inspection with name: {0}", NewInspectionName));
                return;
            }
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            //Create a new inspection file and copy some paramaters to help smoothen the process
            var newInsp = TheDataManager.CreateNewInspection(NewInspectionName, TheDataManager.GetDataFileName(CurrentProject.DirectoryPath, NewInspectionName));
            newInsp.InspectionTool = CurrentInspection.InspectionTool;
            newInsp.ReformerInfo.Customer = CurrentInspection.ReformerInfo.Customer;
            newInsp.ReformerInfo.Refinery = CurrentInspection.ReformerInfo.Refinery;
            newInsp.ReformerInfo.ReformerName = CurrentInspection.ReformerInfo.ReformerName;
            newInsp.ReformerInfo.DefaultID = CurrentInspection.ReformerInfo.DefaultID;
            newInsp.ReformerInfo.DefaultOD = CurrentInspection.ReformerInfo.DefaultOD;
            newInsp.ReformerInfo.UseRowLetters = CurrentInspection.ReformerInfo.UseRowLetters;
            newInsp.ReformerInfo.DefaultMaterial = CurrentInspection.ReformerInfo.DefaultMaterial;
            ProcessFlowList.SetProcessFlowList(newInsp);
            //Update the process flows of each inspection so they know there's multiple inspections now.
            CurrentProject.InspectionFiles.ForEach(I => ProcessFlowList.UpdateProcesses(I.ProcessFlow));

            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
        }

        #endregion Private Methods

        #region Public Methods

        public override void UpdateDataSource()
        {
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return;
            if (InvokeRequired) { Invoke(new Action(UpdateDataSource)); return; } //Make sure it's on the right thread
            bsCurrentProject.DataSource = LifeQuestReformer.Instance.TheDataManager.CurrentProject;
        }

        #endregion Public Methods

        
    }

    
}
