﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuestIntegrity.Core.Maths;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class CircleFitData : BaseProcessWindow
    {
        public CircleFitData()
        {
            InitializeComponent();
        }

        public override void UpdateDataSource() { } //Doesn't need to do anything yet.

        private async void btnCircleFit_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            btnCircleFit.Enabled = false;
            await new TaskFactory().StartNew(CircleFitTheTubes);
            Cursor = Cursors.Default;
            btnCircleFit.Enabled = true;
        }

        private void CircleFitTheTubes()
        {
            Stopwatch sw = Stopwatch.StartNew();
            if (CurrentInspection == null) { OnProgressChanged(100, "No Inspection to Modify"); return; }
            int tubeCounter = 0, tubeCount = CurrentInspection.Tubes.Count;
            Parallel.ForEach(CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)), tube =>
            {
                OnProgressChanged((int)Math.Round(++tubeCounter / (float)tubeCount * 100f * .90), string.Format("Fitting {0}", tube.Name));
                foreach (ReformerScalar scalar in tube.AvailableScalars.Where(S => S.Value.Contains("Radius")))
                {
                    float[,] scalarData;
                    tube.Get2DScalarData(scalar, out scalarData);
                    int sensorCount = scalarData.GetLength(1);
                    double[] x = new double[sensorCount];
                    double[] y = new double[sensorCount];
                    for (int slice = 0; slice < scalarData.GetLength(0); slice++)
                    {
                        float origMean = 0;
                        for (int sensor = 0; sensor < sensorCount; sensor++)
                        {
                            float radius = scalarData[slice, sensor];
                            float degrees = 360f * sensor / sensorCount;
                            x[sensor] = radius * Trigonometry.CosInDegrees(degrees);
                            y[sensor] = radius * Trigonometry.SinInDegrees(degrees);
                            origMean += radius / sensorCount;
                        }
                        double[] fitCircle = Circle.CircleFit2D(x, y);
                        double fitMean = fitCircle[2];
                        double meanDiff = fitMean - origMean;
                        for (int sensor = 0; sensor < sensorCount; sensor++)
                        {
                            double xAdjusted = x[sensor] - fitCircle[0];
                            double yAdjusted = y[sensor] - fitCircle[1];
                            double rAdjusted = Math.Sqrt((yAdjusted * yAdjusted) + (xAdjusted * xAdjusted));
                            scalarData[slice, sensor] = (float)(rAdjusted - meanDiff); //keep the mean value the same as before. Only applies significantly to poorly calibrated data.
                            //scalarData[slice, sensor] = (float)(rAdjusted);
                        }
                    }
                    ReformerHdf5Helper.SetTubeData(tube, scalar, scalarData);
                }
            });
            OnProgressChanged(95, "Repacking Data File...");
            CurrentInspection.RepackData();
            OnProgressChanged(100, string.Format("Finished in {0}s", sw.Elapsed.TotalSeconds.ToString("N1")));
        }
       
    }
}
