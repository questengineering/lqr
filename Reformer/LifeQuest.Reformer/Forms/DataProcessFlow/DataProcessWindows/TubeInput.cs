﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using NGenerics.Extensions;
using Reformer.Data.Scalars;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class TubeInput : BaseProcessWindow
    {
        ColumnHeaderExtender extender;
        static bool downFlowBool = true;
        public TubeInput()
        {
            InitializeComponent();
            extender = new ColumnHeaderExtender(TheView);
            extender.ActivateHeaderExtender();
            extender.AddExtendColumn(colDownFlow, OnButtonClick);
        }

        private void OnButtonClick(GridColumn obj)
        {
            var dialog = MessageBox.Show(this, string.Format("Set DownFlow to {0} for all tubes?", downFlowBool), "Change all tubes?", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                CurrentInspection.Tubes.ForEach(x => x.Specs.DownFlow = downFlowBool);
                downFlowBool = !downFlowBool;
                TheView.RefreshData();
            }
        }

        public override void UpdateDataSource()
        {
            if (CurrentInspection != null)
            {
                reformerTubeBindingSource.DataSource = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
                if (CurrentInspection.ReformerInfo.NewTubes)
                {
                    colYearsSinceLastInspection.Visible = false;
                    colYearsUntilNextInspection.Visible = false;
                }
            }
        }
        private void OnFormClosing(object sender, FormClosingEventArgs e) {
            extender.DeactivateHeaderExtender();
        }


        #region Multi-Select/edit methods

        bool _lockEvents; //Multi-Cell changing done according to: http://www.devexpress.com/Support/Center/Example/Details/E2779
        private void OnCellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (_lockEvents) return;
            _lockEvents = true;
            SetSelectedCellsValues(e.Value);
            _lockEvents = false;
        }

        private void SetSelectedCellsValues(object value)
        {
            try {
                TheView.BeginUpdate();
                foreach (GridCell cell in TheView.GetSelectedCells())
                    TheView.SetRowCellValue(cell.RowHandle, cell.Column, value);
                }
            catch (Exception ex)
            {Log.Error("Error modifying multiple cell values.", ex); }
            finally { TheView.EndUpdate(); }
        }

        private void TheView_MouseUp(object sender, MouseEventArgs e)
        {
            if (!GetInSelectedCell(e)) return;
            DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            TheView.ShowEditorByMouse();
        }

        private void TheView_MouseDown(object sender, MouseEventArgs e)
        {
            if (!GetInSelectedCell(e)) return;
            GridHitInfo hi = TheView.CalcHitInfo(e.Location);
            if (TheView.FocusedRowHandle == hi.RowHandle)
            {
                TheView.FocusedColumn = hi.Column;
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
        }

        private bool GetInSelectedCell(MouseEventArgs e)
        {
            GridHitInfo hi = TheView.CalcHitInfo(e.Location);
            return hi.InRowCell && TheView.IsCellSelected(hi.RowHandle, hi.Column);
        }

        #endregion Multi-Select/edit methods

        public override void UpdateDisplayUnits()
        {
            reformerTubeBindingSource.ResetBindings(false);
        }
    }

    public class ColumnHeaderExtender
    {
        GridView view;
        SkinButtonObjectPainter customButtonPainter;
        EditorButtonObjectInfoArgs args;
        Size buttonSize;

        public ColumnHeaderExtender(GridView view)
        {
            this.view = view;
            buttonSize = new Size(14, 14);

        }

        public void ActivateHeaderExtender()
        {
            CreateButtonPainter();
            CreateButtonInfoArgs();
            SubscribeToEvents();
        }


        public void AddExtendColumn(GridColumn column, Action<GridColumn> action)
        {
            _columnAndActions[column] = action;
        }

        private Dictionary<GridColumn, Action<GridColumn>> _columnAndActions = new Dictionary<GridColumn, Action<GridColumn>>();

        private void CreateButtonInfoArgs()
        {
            EditorButton btn = new EditorButton(ButtonPredefines.Glyph);
            args = new EditorButtonObjectInfoArgs(btn, new DevExpress.Utils.AppearanceObject());
        }

        private void CreateButtonPainter()
        {
            customButtonPainter = new SkinButtonObjectPainter(DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
        }

        private void SubscribeToEvents()
        {
            view.CustomDrawColumnHeader += OnCustomDrawColumnHeader;
            view.MouseDown += OnMouseDown;
            view.MouseUp += OnMouseUp;
            view.MouseMove += OnMouseMove;
        }

        void OnMouseUp(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;

            if (!_columnAndActions.ContainsKey(column)) return;

            if (IsButtonRect(e.Location, column))
            {
                SetButtonState(column, ObjectState.Normal);
                _columnAndActions[column](column);
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
        }


        void OnMouseMove(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;

            if (!_columnAndActions.ContainsKey(column)) return;

            if (IsButtonRect(e.Location, column))
                SetButtonState(column, ObjectState.Hot);
            else
                SetButtonState(column, ObjectState.Normal);
        }

        void OnMouseDown(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;

            if (!_columnAndActions.ContainsKey(column)) return;

            if (IsButtonRect(e.Location, column))
                SetButtonState(column, ObjectState.Pressed);
        }

        private void SetButtonState(GridColumn column, ObjectState state)
        {
            column.Tag = state;
            view.InvalidateColumnHeader(column);
        }

        private bool IsButtonRect(Point point, GridColumn column)
        {

            GraphicsInfo info = new GraphicsInfo();
            info.AddGraphics(null);
            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridColumnInfoArgs columnArgs = viewInfo.ColumnsInfo[column];
            Rectangle buttonRect = CalcButtonRect(columnArgs, info.Graphics);
            info.ReleaseGraphics();
            return buttonRect.Contains(point);
        }

        private Rectangle CalcButtonRect(GridColumnInfoArgs columnArgs, System.Drawing.Graphics gr)
        {
            Rectangle columnRect = columnArgs.Bounds;
            int innerElementsWidth = CalcInnerElementsMinWidth(columnArgs, gr);
            Rectangle buttonRect = new Rectangle(columnRect.Right - innerElementsWidth - buttonSize.Width - 2,
                columnRect.Y + columnRect.Height / 2 - buttonSize.Height / 2, buttonSize.Width, buttonSize.Height);
            return buttonRect;
        }

        private int CalcInnerElementsMinWidth(GridColumnInfoArgs columnArgs, System.Drawing.Graphics gr)
        {
            bool canDrawMode = true;
            return columnArgs.InnerElements.CalcMinSize(gr, ref canDrawMode).Width;
        }

        void OnCustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null) return;


            if (!_columnAndActions.ContainsKey(e.Column)) return;

            DefaultDrawColumnHeader(e);
            DrawCustomButton(e);
            e.Handled = true;
        }

        private void DrawCustomButton(ColumnHeaderCustomDrawEventArgs e)
        {
            SetUpButtonInfoArgs(e);
            customButtonPainter.DrawObject(args);
        }

        private void SetUpButtonInfoArgs(ColumnHeaderCustomDrawEventArgs e)
        {
            args.Cache = e.Cache;
            args.Bounds = CalcButtonRect(e.Info, e.Graphics);
            ObjectState state = ObjectState.Normal;
            if (e.Column.Tag is ObjectState)
                state = (ObjectState)e.Column.Tag;
            args.State = state;
        }

        private static void DefaultDrawColumnHeader(ColumnHeaderCustomDrawEventArgs e)
        {
            e.Painter.DrawObject(e.Info);
        }

        private void UnsubscribeFromEvents()
        {
            view.CustomDrawColumnHeader -= OnCustomDrawColumnHeader;
            view.MouseDown -= OnMouseDown;
            view.MouseUp -= OnMouseUp;
            view.MouseMove -= OnMouseMove;
        }

        public void DeactivateHeaderExtender()
        {
            UnsubscribeFromEvents();

        }
    }
}
