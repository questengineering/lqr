﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class TubeInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.lblNote = new DevExpress.XtraEditors.LabelControl();
            this.TubeGrid = new DevExpress.XtraGrid.GridControl();
            this.reformerTubeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDownFlow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearsUntilNextInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDTolDown = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riCalcEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.colIDTolUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colODTolDown = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colODTolUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearsSinceLastInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTubeGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TubeGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reformerTubeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCalcEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.AllowCustomizationMenu = false;
            this.lcMain.Controls.Add(this.lblNote);
            this.lcMain.Controls.Add(this.TubeGrid);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.layoutControlGroup1;
            this.lcMain.Size = new System.Drawing.Size(917, 569);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "LCMain";
            // 
            // lblNote
            // 
            this.lblNote.Location = new System.Drawing.Point(2, 554);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(369, 13);
            this.lblNote.StyleController = this.lcMain;
            this.lblNote.TabIndex = 5;
            this.lblNote.Text = "Hint: Change multiple values at once by dragging and changing a cell\'s value.";
            // 
            // TubeGrid
            // 
            this.TubeGrid.DataSource = this.reformerTubeBindingSource;
            this.TubeGrid.Location = new System.Drawing.Point(2, 2);
            this.TubeGrid.MainView = this.TheView;
            this.TubeGrid.Name = "TubeGrid";
            this.TubeGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riCalcEdit});
            this.TubeGrid.Size = new System.Drawing.Size(913, 548);
            this.TubeGrid.TabIndex = 4;
            this.TubeGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            // 
            // reformerTubeBindingSource
            // 
            this.reformerTubeBindingSource.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn1,
            this.colName,
            this.colNomID,
            this.colNomOD,
            this.colDownFlow,
            this.colYearsUntilNextInspection,
            this.colIDTolDown,
            this.colIDTolUp,
            this.colODTolDown,
            this.colODTolUp,
            this.colYearsSinceLastInspection});
            this.TheView.GridControl = this.TubeGrid;
            this.TheView.Name = "TheView";
            this.TheView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.TheView.OptionsDetail.EnableMasterViewMode = false;
            this.TheView.OptionsDetail.ShowDetailTabs = false;
            this.TheView.OptionsSelection.MultiSelect = true;
            this.TheView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.TheView.OptionsView.ShowDetailButtons = false;
            this.TheView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.TheView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.OnCellValueChanged);
            this.TheView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TheView_MouseDown);
            this.TheView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TheView_MouseUp);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Tube #";
            this.gridColumn2.FieldName = "ReformerAttributes.TubeNumber";
            this.gridColumn2.MaxWidth = 67;
            this.gridColumn2.MinWidth = 67;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 67;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Row #";
            this.gridColumn1.FieldName = "ReformerAttributes.RowNumber";
            this.gridColumn1.MaxWidth = 70;
            this.gridColumn1.MinWidth = 70;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 70;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.MaxWidth = 60;
            this.colName.MinWidth = 60;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 60;
            // 
            // colNomID
            // 
            this.colNomID.AppearanceCell.Options.UseTextOptions = true;
            this.colNomID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomID.AppearanceHeader.Options.UseTextOptions = true;
            this.colNomID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomID.Caption = "Nom ID";
            this.colNomID.DisplayFormat.FormatString = "N3";
            this.colNomID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNomID.FieldName = "Specs.DiameterInsideInDisplayUnits";
            this.colNomID.Name = "colNomID";
            this.colNomID.Visible = true;
            this.colNomID.VisibleIndex = 3;
            this.colNomID.Width = 67;
            // 
            // colNomOD
            // 
            this.colNomOD.AppearanceCell.Options.UseTextOptions = true;
            this.colNomOD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomOD.AppearanceHeader.Options.UseTextOptions = true;
            this.colNomOD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNomOD.Caption = "Nom OD";
            this.colNomOD.DisplayFormat.FormatString = "N3";
            this.colNomOD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNomOD.FieldName = "Specs.DiameterOutsideInDisplayUnits";
            this.colNomOD.Name = "colNomOD";
            this.colNomOD.Visible = true;
            this.colNomOD.VisibleIndex = 4;
            this.colNomOD.Width = 67;
            // 
            // colDownFlow
            // 
            this.colDownFlow.AppearanceCell.Options.UseTextOptions = true;
            this.colDownFlow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDownFlow.AppearanceHeader.Options.UseTextOptions = true;
            this.colDownFlow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDownFlow.Caption = "DownFlow";
            this.colDownFlow.FieldName = "Specs.DownFlow";
            this.colDownFlow.Name = "colDownFlow";
            this.colDownFlow.Visible = true;
            this.colDownFlow.VisibleIndex = 5;
            this.colDownFlow.Width = 67;
            // 
            // colYearsUntilNextInspection
            // 
            this.colYearsUntilNextInspection.AppearanceCell.Options.UseTextOptions = true;
            this.colYearsUntilNextInspection.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearsUntilNextInspection.AppearanceHeader.Options.UseTextOptions = true;
            this.colYearsUntilNextInspection.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearsUntilNextInspection.Caption = "Next Insp (yrs)";
            this.colYearsUntilNextInspection.FieldName = "Specs.YearsUntilNextInspection";
            this.colYearsUntilNextInspection.Name = "colYearsUntilNextInspection";
            this.colYearsUntilNextInspection.ToolTip = "How many years this tube will run until the next inspection.";
            this.colYearsUntilNextInspection.Visible = true;
            this.colYearsUntilNextInspection.VisibleIndex = 10;
            this.colYearsUntilNextInspection.Width = 95;
            // 
            // colIDTolDown
            // 
            this.colIDTolDown.AppearanceCell.Options.UseTextOptions = true;
            this.colIDTolDown.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDTolDown.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDTolDown.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDTolDown.Caption = "ID Tol-";
            this.colIDTolDown.ColumnEdit = this.riCalcEdit;
            this.colIDTolDown.DisplayFormat.FormatString = "N3";
            this.colIDTolDown.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIDTolDown.FieldName = "Specs.IDTolDownDisplayUnits";
            this.colIDTolDown.Name = "colIDTolDown";
            this.colIDTolDown.Visible = true;
            this.colIDTolDown.VisibleIndex = 6;
            this.colIDTolDown.Width = 67;
            // 
            // riCalcEdit
            // 
            this.riCalcEdit.AutoHeight = false;
            this.riCalcEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riCalcEdit.Name = "riCalcEdit";
            // 
            // colIDTolUp
            // 
            this.colIDTolUp.AppearanceCell.Options.UseTextOptions = true;
            this.colIDTolUp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDTolUp.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDTolUp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDTolUp.Caption = "ID Tol+";
            this.colIDTolUp.ColumnEdit = this.riCalcEdit;
            this.colIDTolUp.DisplayFormat.FormatString = "N3";
            this.colIDTolUp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIDTolUp.FieldName = "Specs.IDTolUpDisplayUnits";
            this.colIDTolUp.Name = "colIDTolUp";
            this.colIDTolUp.Visible = true;
            this.colIDTolUp.VisibleIndex = 7;
            this.colIDTolUp.Width = 67;
            // 
            // colODTolDown
            // 
            this.colODTolDown.AppearanceCell.Options.UseTextOptions = true;
            this.colODTolDown.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODTolDown.AppearanceHeader.Options.UseTextOptions = true;
            this.colODTolDown.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODTolDown.Caption = "OD Tol-";
            this.colODTolDown.ColumnEdit = this.riCalcEdit;
            this.colODTolDown.DisplayFormat.FormatString = "N3";
            this.colODTolDown.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colODTolDown.FieldName = "Specs.ODTolDownDisplayUnits";
            this.colODTolDown.Name = "colODTolDown";
            this.colODTolDown.Visible = true;
            this.colODTolDown.VisibleIndex = 8;
            this.colODTolDown.Width = 67;
            // 
            // colODTolUp
            // 
            this.colODTolUp.AppearanceCell.Options.UseTextOptions = true;
            this.colODTolUp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODTolUp.AppearanceHeader.Options.UseTextOptions = true;
            this.colODTolUp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODTolUp.Caption = "OD Tol+";
            this.colODTolUp.ColumnEdit = this.riCalcEdit;
            this.colODTolUp.DisplayFormat.FormatString = "N3";
            this.colODTolUp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colODTolUp.FieldName = "Specs.ODTolUpDisplayUnits";
            this.colODTolUp.Name = "colODTolUp";
            this.colODTolUp.Visible = true;
            this.colODTolUp.VisibleIndex = 9;
            this.colODTolUp.Width = 67;
            // 
            // colYearsSinceLastInspection
            // 
            this.colYearsSinceLastInspection.AppearanceCell.Options.UseTextOptions = true;
            this.colYearsSinceLastInspection.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearsSinceLastInspection.AppearanceHeader.Options.UseTextOptions = true;
            this.colYearsSinceLastInspection.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYearsSinceLastInspection.Caption = "Yrs Since Last Insp";
            this.colYearsSinceLastInspection.FieldName = "Specs.YearsSinceLastInspection";
            this.colYearsSinceLastInspection.Name = "colYearsSinceLastInspection";
            this.colYearsSinceLastInspection.Visible = true;
            this.colYearsSinceLastInspection.VisibleIndex = 11;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTubeGrid,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(917, 569);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lciTubeGrid
            // 
            this.lciTubeGrid.Control = this.TubeGrid;
            this.lciTubeGrid.CustomizationFormText = "Tubes";
            this.lciTubeGrid.Location = new System.Drawing.Point(0, 0);
            this.lciTubeGrid.Name = "lciTubeGrid";
            this.lciTubeGrid.Size = new System.Drawing.Size(917, 552);
            this.lciTubeGrid.Text = "Tubes";
            this.lciTubeGrid.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciTubeGrid.TextSize = new System.Drawing.Size(0, 0);
            this.lciTubeGrid.TextToControlDistance = 0;
            this.lciTubeGrid.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblNote;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 552);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(917, 17);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // TubeInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "TubeInput";
            this.Size = new System.Drawing.Size(917, 569);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TubeGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reformerTubeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCalcEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl TubeGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeGrid;
        private System.Windows.Forms.BindingSource reformerTubeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDTolDown;
        private DevExpress.XtraGrid.Columns.GridColumn colIDTolUp;
        private DevExpress.XtraGrid.Columns.GridColumn colODTolDown;
        private DevExpress.XtraGrid.Columns.GridColumn colODTolUp;
        private DevExpress.XtraGrid.Columns.GridColumn colNomID;
        private DevExpress.XtraGrid.Columns.GridColumn colNomOD;
        private DevExpress.XtraGrid.Columns.GridColumn colDownFlow;
        private DevExpress.XtraGrid.Columns.GridColumn colYearsUntilNextInspection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit riCalcEdit;
        private DevExpress.XtraEditors.LabelControl lblNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearsSinceLastInspection;
    }
}
