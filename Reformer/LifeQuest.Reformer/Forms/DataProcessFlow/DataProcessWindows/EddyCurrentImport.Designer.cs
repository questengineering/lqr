﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class EddyCurrentImport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.chkOverwriteDuplicates = new DevExpress.XtraEditors.CheckEdit();
            this.btnImportFiles = new DevExpress.XtraEditors.SimpleButton();
            this.lblNote = new DevExpress.XtraEditors.LabelControl();
            this.TheGrid = new DevExpress.XtraGrid.GridControl();
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTube = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiSourceFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.LCG = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciSourceFolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFolderGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.BSInspection = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).BeginInit();
            this.LayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverwriteDuplicates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiSourceFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSourceFolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFolderGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspection)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl
            // 
            this.LayoutControl.Controls.Add(this.chkOverwriteDuplicates);
            this.LayoutControl.Controls.Add(this.btnImportFiles);
            this.LayoutControl.Controls.Add(this.lblNote);
            this.LayoutControl.Controls.Add(this.TheGrid);
            this.LayoutControl.Controls.Add(this.uiSourceFolder);
            this.LayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl.Name = "LayoutControl";
            this.LayoutControl.Root = this.LCG;
            this.LayoutControl.Size = new System.Drawing.Size(444, 516);
            this.LayoutControl.TabIndex = 0;
            this.LayoutControl.Text = "layoutControl1";
            // 
            // chkOverwriteDuplicates
            // 
            this.chkOverwriteDuplicates.EditValue = true;
            this.chkOverwriteDuplicates.Enabled = false;
            this.chkOverwriteDuplicates.Location = new System.Drawing.Point(12, 482);
            this.chkOverwriteDuplicates.Name = "chkOverwriteDuplicates";
            this.chkOverwriteDuplicates.Properties.Caption = "Overwrite Duplicates";
            this.chkOverwriteDuplicates.Size = new System.Drawing.Size(165, 19);
            this.chkOverwriteDuplicates.StyleController = this.LayoutControl;
            this.chkOverwriteDuplicates.TabIndex = 9;
            this.chkOverwriteDuplicates.ToolTip = "If selected, tubes that are being imported and already exist in the inspection wi" +
    "ll be over-written. If de-selected, they will be skipped.";
            // 
            // btnImportFiles
            // 
            this.btnImportFiles.Location = new System.Drawing.Point(339, 482);
            this.btnImportFiles.Name = "btnImportFiles";
            this.btnImportFiles.Size = new System.Drawing.Size(83, 22);
            this.btnImportFiles.StyleController = this.LayoutControl;
            this.btnImportFiles.TabIndex = 8;
            this.btnImportFiles.Text = "Import Files";
            this.btnImportFiles.Click += new System.EventHandler(this.btnImportFiles_Click);
            // 
            // lblNote
            // 
            this.lblNote.Location = new System.Drawing.Point(12, 465);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(333, 13);
            this.lblNote.StyleController = this.LayoutControl;
            this.lblNote.TabIndex = 7;
            this.lblNote.Text = "Note: Delete unwanted files from above grid to avoid importing them.";
            // 
            // TheGrid
            // 
            this.TheGrid.Location = new System.Drawing.Point(81, 36);
            this.TheGrid.MainView = this.TheView;
            this.TheGrid.Name = "TheGrid";
            this.TheGrid.Size = new System.Drawing.Size(341, 425);
            this.TheGrid.TabIndex = 6;
            this.TheGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRow,
            this.colTube,
            this.colSize});
            this.TheView.GridControl = this.TheGrid;
            this.TheView.GroupCount = 1;
            this.TheView.Name = "TheView";
            this.TheView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.TheView.OptionsSelection.MultiSelect = true;
            this.TheView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRow, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTube, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.TheView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TheView_KeyDown);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "FileName";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRow
            // 
            this.colRow.AppearanceCell.Options.UseTextOptions = true;
            this.colRow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.AppearanceHeader.Options.UseTextOptions = true;
            this.colRow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRow.Caption = "Row #";
            this.colRow.FieldName = "Row";
            this.colRow.Name = "colRow";
            this.colRow.Visible = true;
            this.colRow.VisibleIndex = 1;
            // 
            // colTube
            // 
            this.colTube.AppearanceCell.Options.UseTextOptions = true;
            this.colTube.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTube.AppearanceHeader.Options.UseTextOptions = true;
            this.colTube.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTube.Caption = "Tube #";
            this.colTube.FieldName = "Tube";
            this.colTube.Name = "colTube";
            this.colTube.Visible = true;
            this.colTube.VisibleIndex = 1;
            // 
            // colSize
            // 
            this.colSize.AppearanceCell.Options.UseTextOptions = true;
            this.colSize.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSize.AppearanceHeader.Options.UseTextOptions = true;
            this.colSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSize.Caption = "Size (MB)";
            this.colSize.DisplayFormat.FormatString = "N2";
            this.colSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSize.FieldName = "SizeInMB";
            this.colSize.Name = "colSize";
            this.colSize.OptionsColumn.AllowEdit = false;
            this.colSize.OptionsColumn.ReadOnly = true;
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 2;
            // 
            // uiSourceFolder
            // 
            this.uiSourceFolder.Location = new System.Drawing.Point(81, 12);
            this.uiSourceFolder.Name = "uiSourceFolder";
            this.uiSourceFolder.Properties.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            serializableAppearanceObject1.BackColor = System.Drawing.SystemColors.Control;
            serializableAppearanceObject1.Options.UseBackColor = true;
            this.uiSourceFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Open", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.uiSourceFolder.Size = new System.Drawing.Size(341, 20);
            this.uiSourceFolder.StyleController = this.LayoutControl;
            this.uiSourceFolder.TabIndex = 4;
            this.uiSourceFolder.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.uiSourceFolder_ButtonClick);
            // 
            // LCG
            // 
            this.LCG.CustomizationFormText = "LCG";
            this.LCG.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LCG.GroupBordersVisible = false;
            this.LCG.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciSourceFolder,
            this.lciFolderGrid,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.emptySpaceItem2});
            this.LCG.Location = new System.Drawing.Point(0, 0);
            this.LCG.Name = "LCG";
            this.LCG.Size = new System.Drawing.Size(444, 516);
            this.LCG.Text = "LCG";
            this.LCG.TextVisible = false;
            // 
            // lciSourceFolder
            // 
            this.lciSourceFolder.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.lciSourceFolder.Control = this.uiSourceFolder;
            this.lciSourceFolder.CustomizationFormText = "layoutControlItem1";
            this.lciSourceFolder.Location = new System.Drawing.Point(0, 0);
            this.lciSourceFolder.Name = "lciSourceFolder";
            this.lciSourceFolder.Size = new System.Drawing.Size(414, 24);
            this.lciSourceFolder.Text = "Source Folder";
            this.lciSourceFolder.TextSize = new System.Drawing.Size(66, 13);
            // 
            // lciFolderGrid
            // 
            this.lciFolderGrid.Control = this.TheGrid;
            this.lciFolderGrid.CustomizationFormText = "Files in Folder";
            this.lciFolderGrid.Location = new System.Drawing.Point(0, 24);
            this.lciFolderGrid.MaxSize = new System.Drawing.Size(414, 0);
            this.lciFolderGrid.MinSize = new System.Drawing.Size(414, 24);
            this.lciFolderGrid.Name = "lciFolderGrid";
            this.lciFolderGrid.Size = new System.Drawing.Size(414, 429);
            this.lciFolderGrid.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciFolderGrid.Text = "Files in Folder";
            this.lciFolderGrid.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblNote;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 453);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(414, 17);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnImportFiles;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(327, 470);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(87, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(169, 470);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(158, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkOverwriteDuplicates;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 470);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(169, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(414, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 496);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // BSInspection
            // 
            this.BSInspection.DataSource = typeof(Reformer.Data.InspectionFile.ReformerInspection);
            // 
            // EddyCurrentImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LayoutControl);
            this.Name = "EddyCurrentImport";
            this.Size = new System.Drawing.Size(444, 516);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl)).EndInit();
            this.LayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkOverwriteDuplicates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiSourceFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSourceFolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFolderGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSInspection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl LayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup LCG;
        private DevExpress.XtraEditors.ButtonEdit uiSourceFolder;
        private DevExpress.XtraLayout.LayoutControlItem lciSourceFolder;
        private System.Windows.Forms.BindingSource BSInspection;
        private DevExpress.XtraGrid.GridControl TheGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private DevExpress.XtraLayout.LayoutControlItem lciFolderGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRow;
        private DevExpress.XtraGrid.Columns.GridColumn colTube;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl lblNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit chkOverwriteDuplicates;
        private DevExpress.XtraEditors.SimpleButton btnImportFiles;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
