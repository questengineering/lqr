﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using QuestIntegrity.Core.Maths;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class Level3Export : BaseProcessWindow
    {
        private const string Level3SummaryFileName = "Level3Summary.xlsx";

        private readonly List<CachedLevel3SummaryItem> _level3Items = new List<CachedLevel3SummaryItem>(); 
        public Level3Export()
        {
            InitializeComponent();
        }

        private void GenerateLevel3ItemList()
        {
            _level3Items.Clear();
            foreach (ReformerTube tube in CurrentInspection.Tubes.Where(t => t.AvailableScalars.Contains(ReformerScalar.RadiusInside)).OrderBy(t => t.OrderIndex))
            {
                CachedLevel3SummaryItem sumItem = new CachedLevel3SummaryItem
                {
                    Tube = tube,
                    TubeName = tube.Name,

                    Level1GrowthRatePercentPerYear = tube.ReformerAttributes.Level1GrowthRate.ToString("F2"),
                    OverallGrowthPercent = tube.ReformerAttributes.OverallIDGrowth.ToString("F2"),
                    Level1MaxID = (tube.ReformerAttributes.PickedLevel1MaxIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3"),
                    Level1PreviousID = (tube.ReformerAttributes.PickedLevel1RefIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3"),
                    Level1Age = tube.Specs.YearsSinceLastInspection.ToString("F0"),
                    Level1YearsUntilNextInspection = tube.Specs.YearsUntilNextInspection.ToString("F0"),

                    OverallMaxID = (tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3"),
                    OverallBaselineID = (tube.ReformerAttributes.PickedOverallRefIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3"),
                    OverallAge = tube.Specs.OverallAge.ToString("F0"),
                };
                _level3Items.Add(sumItem);
            }
        }

        public async override void UpdateDataSource()
        {
            Cursor = Cursors.WaitCursor;
            await Task.Factory.StartNew(GenerateLevel3ItemList);
            gridLevel3Data.DataSource = _level3Items;
            Cursor = Cursors.Default;
        } 

        private void btnExportSummary_Click(object sender, EventArgs e)
        {
            string fileName = Path.Combine(UsefulDirectories.Tables.FullName, Level3SummaryFileName);
            FileInfo targetFile = new FileInfo(fileName);

            if (!CheckFileCanBeExportedTo(targetFile))
                return;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            Enabled = false;
            
            ExportSummary(targetFile);
            
            Enabled = true;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
        }

        private bool CheckFileCanBeExportedTo(FileInfo file)
        {
            if (!file.Exists) return true;
            try
            {
                file.Delete();
            }
            catch (Exception)
            {
                var result = MessageBox.Show(this, "Cannot delete previous level 3 summary, so it may be in use. Retry?\r\n\r\n" + file.FullName, @"Can't delete",  MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                    return CheckFileCanBeExportedTo(file);
                return false;
            }
            return true;
        }

        private void ExportSummary(FileInfo targetFile)
        {
            
            OnProgressChanged(5, "Opening Excel.");
            Application xlApp = new Application {Visible = true, DisplayAlerts = false};
            Workbook wb = xlApp.Workbooks.Add();
            Worksheet sheet = wb.Sheets[1];
            try
            {
                OnProgressChanged(10, "Writing Column Headers.");
                string[] columnHeaders = { "Tube Name", "Level 1 Growth Rate % per year", "Overall ID Creep Growth %", "Level 1 Max ID", "Level 1 Previous ID", "Level 1 Age", "Overall Max ID", "Overall Baseline ID", "Overall Age" };

                for (int col = 1; col <= columnHeaders.Length; col++)
                {
                    Range cell = sheet.Cells[1, col];
                    cell.Value = columnHeaders[col - 1];
                    cell.Columns.AutoFit();
                }
                
                for (int row = 2; row < _level3Items.Count + 2; row++)
                {
                    CachedLevel3SummaryItem item = _level3Items[row - 2];
                    OnProgressChanged(15 + (int)(row / (_level3Items.Count + 2d) * 80), $"Writing {item.TubeName} data.");
                    var cell = sheet.Range["A" + row];
                    cell.Value = item.GetFieldsSplitByTab();
                    cell.TextToColumns(cell, XlTextParsingType.xlDelimited, XlTextQualifier.xlTextQualifierNone, false, true);
                }
                OnProgressChanged(98, "Saving and closing Excel.");
                xlApp.Workbooks[1].SaveAs(targetFile.FullName);
                xlApp.Workbooks[1].Close();
                OnProgressChanged(100, "Finished exporting summary data.");
            }
            catch (Exception ex)
            {
                xlApp.DisplayAlerts = false;
                xlApp.Workbooks.Close();
                Log.Error("Error exporting to Excel", ex);
                OnProgressChanged(100, "Error exporting to Excel: " + ex.Message);
            }
            xlApp.Quit();
            
        }

        private void btnOpenExportFolder_Click(object sender, EventArgs e)
        {
            Process.Start(UsefulDirectories.Tables.FullName);
        }

        private void btnExportDetailedInfo_Click(object sender, EventArgs e)
        {
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.WaitCursor);
            Enabled = false;

            ExportTubeData();

            Enabled = true;
            ReformerWindowManager.Instance.Main.SetCursor(Cursors.Default);
        }

        private void ExportTubeData()
        {
            OnProgressChanged(5, "Opening Excel.");
            Application xlApp = new Application { Visible = false, DisplayAlerts = false };
            
            try
            {
                OnProgressChanged(10, "Writing Column Headers.");
                
                foreach (var item in _level3Items)
                {
                    int overallProgress =(int)(_level3Items.IndexOf(item) / (_level3Items.Count + 2d) * 100);
                    OnProgressChanged(overallProgress, $"Writing {item.TubeName} data.");
                    
                    string fileName = Path.Combine(UsefulDirectories.Tables.FullName, item.TubeName + ".xlsx");
                    FileInfo targetFile = new FileInfo(fileName);

                    if (!CheckFileCanBeExportedTo(targetFile))
                        return;
                    
                    Workbook wb = xlApp.Workbooks.Add();
                    if (wb.Sheets.Count < 2) wb.Sheets.Add();
                    if (wb.Sheets.Count < 3) wb.Sheets.Add();

                    Worksheet currentDataSheet = wb.Sheets[1];
                    currentDataSheet.Name = "CurrentTubeData";
                    
                    Worksheet previousDataSheet = wb.Sheets[2];
                    previousDataSheet.Name = "PreviousTubeData";
                    
                    Worksheet originalDataSheet = wb.Sheets[3];
                    originalDataSheet.Name = "OriginalTubeData";

                    Worksheet metaDataSheet = wb.Sheets.Add();
                    metaDataSheet.Name = "MetaData";

                    List<string> columnHeaders = new List<string>{ "Axial Data (in)", "", "Mean ID (in)", "", "Sensor 1 IR (in)", "Sensor 2 IR (in)", "Sensor 3 IR (in)", "Sensor 4 IR (in)", "Sensor 5 IR (in)", "Sensor 6 IR (in)"};
                    if (item.Tube.Inspection.NumRadialReadings > 6)
                        columnHeaders.AddRange(new []{"Sensor 7 IR (in)", "Sensor 8 IR (in)"});
                    var allData = item.GetDataArray();
                    var currentData = allData[0];
                    var previousData = allData[1];
                    var originalData = allData[2];
                        

                    //format to match raw data CSVs
                    List<Worksheet> dataSheets = new List<Worksheet> { currentDataSheet, previousDataSheet, originalDataSheet };
                    foreach (var sheet in dataSheets)
                    {
                        for (int col = 1; col <= columnHeaders.Count; col++)
                        {
                            Range cell = sheet.Cells[10, col];
                            cell.Value = columnHeaders[col - 1];
                            cell.Columns.AutoFit();
                        }

                        //add the sensor# to the topleft corner
                        ((Range)sheet.Cells[1, 1]).Value = item.Tube.Inspection.NumRadialReadings;

                    }

                    //write the actual data to all three sheets
                    Range start1Cell = currentDataSheet.Cells[11, 1];
                    Range end1Cell = currentDataSheet.Cells[10 + currentData.GetLength(0), currentData.GetLength(1)];
                    currentDataSheet.Range[start1Cell, end1Cell].Value = currentData;

                    Range start2Cell = previousDataSheet.Cells[11, 1];
                    Range end2Cell = previousDataSheet.Cells[10 + previousData.GetLength(0), previousData.GetLength(1)];
                    previousDataSheet.Range[start2Cell, end2Cell].Value = previousData;

                    Range start3Cell = originalDataSheet.Cells[11, 1];
                    Range end3Cell = originalDataSheet.Cells[10 + originalData.GetLength(0), originalData.GetLength(1)];
                    originalDataSheet.Range[start3Cell, end3Cell].Value = originalData;

                    //Write the meta data.
                    OnProgressChanged(overallProgress, $"Writing {item.TubeName} meta data.");
                    metaDataSheet.Cells[1, 1].Value = "Customer Name";
                    metaDataSheet.Cells[1, 2].Value = item.Tube.Inspection.ReformerInfo.Customer;
                    metaDataSheet.Cells[2, 1].Value = "Plant Name";
                    metaDataSheet.Cells[2, 2].Value = item.Tube.Inspection.ReformerInfo.Refinery;
                    metaDataSheet.Cells[3, 1].Value = "Reformer Name";
                    metaDataSheet.Cells[3, 2].Value = item.Tube.Inspection.ReformerInfo.ReformerName;
                    metaDataSheet.Cells[4, 1].Value = "Date Inspected";
                    metaDataSheet.Cells[4, 2].Value = item.Tube.Inspection.DateInspected;

                    //Write level 1 values
                    metaDataSheet.Cells[6, 1].Value = "Level 1 Growth Rate % Per Year";
                    metaDataSheet.Cells[6, 2].Value = item.Tube.ReformerAttributes.Level1GrowthRate.ToString("F2");
                    metaDataSheet.Cells[7, 1].Value = "Level 1 Max ID";
                    metaDataSheet.Cells[7, 2].Value = (item.Tube.ReformerAttributes.PickedLevel1MaxIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3");
                    metaDataSheet.Cells[8, 1].Value = "Level 1 Max Axial Position";
                    metaDataSheet.Cells[8, 2].Value = (item.Tube.ReformerAttributes.PickedLevel1MaxODPositionDisplayUnits * DefaultValues.AxialDisplayToBaseUnits).ToString("F1");
                    metaDataSheet.Cells[9, 1].Value = "Level 1 Previous ID";
                    metaDataSheet.Cells[9, 2].Value = (item.Tube.ReformerAttributes.PickedLevel1RefIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3");
                    metaDataSheet.Cells[10, 1].Value = "Level 1 Previous Axial Position";
                    metaDataSheet.Cells[10, 2].Value = (item.Tube.ReformerAttributes.PickedLevel1RefPositionDisplayUnits * DefaultValues.AxialDisplayToBaseUnits).ToString("F1");
                    metaDataSheet.Cells[11, 1].Value = "Level 1 Years Since Last Inspection";
                    metaDataSheet.Cells[11, 2].Value = item.Tube.Specs.YearsSinceLastInspection;
                    metaDataSheet.Cells[12, 1].Value = "Level 1 Years Until Next Inspection";
                    metaDataSheet.Cells[12, 2].Value = item.Tube.Specs.YearsUntilNextInspection;

                    //write overall growth values
                    metaDataSheet.Cells[14, 1].Value = "Overall ID Creep Growth %";
                    metaDataSheet.Cells[14, 2].Value = item.Tube.ReformerAttributes.OverallIDGrowth.ToString("F2");
                    metaDataSheet.Cells[15, 1].Value = "Overall Max ID";
                    metaDataSheet.Cells[15, 2].Value = (item.Tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3");
                    metaDataSheet.Cells[16, 1].Value = "Overall Max ID Axial Position";
                    metaDataSheet.Cells[16, 2].Value = (item.Tube.ReformerAttributes.PickedOverallMaxPositionDisplayUnits * DefaultValues.AxialDisplayToBaseUnits).ToString("F1");
                    metaDataSheet.Cells[17, 1].Value = "Overall Baseline ID";
                    metaDataSheet.Cells[17, 2].Value = (item.Tube.ReformerAttributes.PickedOverallRefIDDisplayUnits * DefaultValues.MeasDisplayToBaseUnits).ToString("F3");
                    metaDataSheet.Cells[18, 1].Value = "Overall Baseline Axial Position";
                    metaDataSheet.Cells[18, 2].Value = (item.Tube.ReformerAttributes.PickedOverallRefPositionDisplayUnits * DefaultValues.AxialDisplayToBaseUnits).ToString("F1");
                    metaDataSheet.Cells[19, 1].Value = "Overall Age";
                    metaDataSheet.Cells[19, 2].Value = item.OverallAge;

                    ////Add a chart to the sheet.
                    //Chart chart = (Chart)metaDataSheet.Shapes.AddChart(XlChartType.xlXYScatterLinesNoMarkers, 300, 100, 700, 500);
                    //chart.Legend.Delete();
                    //SeriesCollection chartSeries = chart.SeriesCollection();
                    //var currentDataAxialRange = currentDataSheet.Range[currentDataSheet.Cells[11, 1], currentDataSheet.Cells[11 + currentData.GetLength(0), 1]];
                    //var currentDataSeries = chartSeries.Add();




                    OnProgressChanged(overallProgress, "Saving and closing Excel.");
                    xlApp.Workbooks[1].SaveAs(targetFile.FullName);
                    xlApp.Workbooks[1].Close();
                }
                
                OnProgressChanged(100, "Finished exporting summary data.");
            }
            catch (Exception ex)
            {
                xlApp.DisplayAlerts = false;
                xlApp.Workbooks.Close();
                Log.Error("Error exporting to Excel", ex);
                OnProgressChanged(100, "Error exporting to Excel: " + ex.Message);
            }
            xlApp.Quit();
        }
    }

    [Serializable]
    public class CachedLevel3SummaryItem
    {
        public ReformerTube Tube { get; set; }
        public string TubeName { get;  set; }
        public string Level1GrowthRatePercentPerYear { get;  set; }
        public string OverallGrowthPercent { get; set; }
        public string Level1MaxID { get;  set; }
        public string Level1PreviousID { get;  set; }
        public string Level1Age { get;  set; }
        public string Level1YearsUntilNextInspection { get; set; }
        public string OverallMaxID { get;  set; }
        public string OverallBaselineID { get;  set; }
        public string OverallAge { get; set; }

        /// <summary> Gets the level 3 summary data in the specific format requested. </summary>
        public string GetFieldsSplitByTab()
        {
            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}", "\t",
                TubeName,
                Level1GrowthRatePercentPerYear,
                OverallGrowthPercent,
                Level1MaxID,
                Level1PreviousID,
                Level1Age,
                OverallMaxID,
                OverallBaselineID,
                OverallAge
                );
        }

        public List<object[,]> GetDataArray()
        {
            var returnArrays = new List<object[,]>();
            returnArrays.Add(GetTubeData(Tube));
            returnArrays.Add(GetTubeData(Tube.ReformerAttributes.PreviousTube));
            returnArrays.Add(GetTubeData(Tube.ReformerAttributes.OldestOrBaselineTube));
            return returnArrays;
        }

        private static object[,] GetTubeData(ReformerTube tube)
        {
            if (tube == null) return new object[1,1];

            //Get the tube's data we'll be writing out.
            List<float> axialData = tube.Positions;
            float[,] irData;
            tube.Get2DScalarData(ReformerScalar.RadiusInside, out irData);
            var irMeanData = tube.Get2DScalarStatisticalData(ReformerScalar.RadiusInside, StatisticalMeasures.Mean).First().Value;
            int numSlices = irData.GetLength(0);
            int numSensors = irData.GetLength(1);

            object[,] returnArray = new object[numSlices, numSensors + 4];
            for (int slice = 0; slice < numSlices; slice++)
            {
                returnArray[slice, 0] = axialData[slice].ToString("F1");
                returnArray[slice, 2] = (irMeanData[slice] * 2d).ToString("F3");
                for (int sensor = 0; sensor < numSensors; sensor++)
                {
                    returnArray[slice, 4 + sensor] = irData[slice, sensor].ToString("F3");
                }
            }
            return returnArray;
        }
    }
}
