﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Nevron.Diagram;
using Nevron.Diagram.Extensions;
using Nevron.Diagram.Filters;
using Nevron.Diagram.Shapes;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Extensions.Strings;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Materials;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess;
using Length = QuestIntegrity.Core.Units.Length;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess
{
    public partial class TubeLayoutMapControl : UserControl
    {
        NBasicShapesFactory _shapeFactory;
        public ReformerInspection CurrentInspection { get; set; }
        public MapTypes MapType { get; set; }
        const float LeftPadding = 1f, RightPadding = 1f;
        const string TubeIdentifier = "Tube:";
        const string TubeIndicatorIdentifier = "TubeIndicator:";
        const string RowIndicatorIdentifier = "RowIndicatorIdentifier:";

        private ChartTextFieldHolder TextHolder;

        public TubeLayoutMapControl()
        {
            InitializeComponent();
            InitializeDrawingItems();
            LoadFreshOrStored();
            TextHolder = new ChartTextFieldHolder();
        }

        #region Initialization 

         private void InitializeDrawingItems()
        {
            _shapeFactory = new NBasicShapesFactory(DrawingDoc);
            //Hide the non-useful bars by default.
            CommandBar.Toolbars.ToArray().ToList().ForEach(Tb => { if (Tb.Text != @"Main Menu" & Tb.Text != @"Format") Tb.Visible = false; });
        }

        public void LoadFreshOrStored()
        {
            if (MapType == MapTypes.Level1Assessment)
            {
                if (ReformerMain.StoredL1IsNull())
                {
                    //System.Diagnostics.Debug.WriteLine("L1 is Null");
                    LoadFreshTemplate();
                    //System.Diagnostics.Debug.WriteLine("LoadFreshOrStored() .StoredL1IsNull()");
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("L1 is NOTNull");
                    TheView.Document = DrawingDoc = ReformerMain.TempRetrieveDocument(true);
                }
            }
            else if (MapType == MapTypes.ExpansionMap)
            {
                if (ReformerMain.StoredEMapIsNull())
                {
                    //System.Diagnostics.Debug.WriteLine("EMap is Null");
                    LoadFreshTemplate();
                    //System.Diagnostics.Debug.WriteLine("LoadFreshOrStored() .StoredEMapIsNull()");
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("EMap is NOTNull");
                    TheView.Document = DrawingDoc = ReformerMain.TempRetrieveDocument(false);
                }
            }
        }

        /// <summary> Loads a template based on the type of inspection and parameters set up therein. </summary>
         public void LoadFreshTemplate()
         {
             TheView.Document = null;
             if (CurrentInspection == null) return;
             string templateName = "";
             if (MapType == MapTypes.Level1Assessment) //TODO make redundant check a parameter
                 templateName = "Level1Template.ndb";
             else if (MapType == MapTypes.ExpansionMap)
                templateName = CurrentInspection.ReformerInfo.NewTubes ? "ExpansionMapTemplate(NewTubes).ndb" : "ExpansionMapTemplate(OldTubes).ndb";
             
             DrawingDoc = new NPersistencyManager().LoadDrawingFromFile(Path.Combine(UsefulDirectories.TemplateDirectory.FullName, templateName));
             TheView.Document = DrawingDoc;

             SetOfficeAddressStrings(); //initial post-load
        }

        #endregion Initialization

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.HandleDestroyed += TubeLayoutMapControl_HandleDestroyed;
        }

        void TubeLayoutMapControl_HandleDestroyed(object sender, EventArgs e)
        {
            
            if (DrawingDoc != null)
            {
                ReformerMain.TempStoreDocument(DrawingDoc, MapType == MapTypes.Level1Assessment); //DrawingDoc or TheView.Document ?
                //System.Diagnostics.Debug.WriteLine("TempStoring Document");
            }
        }

        public void TriggerStoreDocument()
        {
            ReformerMain.TempStoreDocument(DrawingDoc, MapType == MapTypes.Level1Assessment);
        }


        #region Drawing Stuff

        public void Redraw()
        {
             ClearDrawing();
             if (CurrentInspection == null) return;
             Cursor = Cursors.WaitCursor;
             DrawingDoc.BeginUpdate();

             DrawTubes();
             LabelTubes();
             SetTextValues();
             DrawingDoc.EndUpdate();
             Cursor = Cursors.Default;
         }

        private void LabelTubes()
        {
            foreach (ReformerTube tube in CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition))) //There may be tubes with eddy current data only, so check for axial data.)
            {
                if (tube.ReformerAttributes.TubeNumber % 5 == 0 | tube.ReformerAttributes.TubeNumber == 1)
                {
                    float labelWidth = .323f; //this is about what's needed for 2 decimal places.
                    NShape tubeCircle = DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().First(shape => shape.Name.Contains(TubeIdentifier) && shape.Name.Contains(tube.Name));
                    NPointF topLeftPoint = new NPointF(tubeCircle.Bounds.X - (labelWidth - tubeCircle.Width) / 2, tubeCircle.Bounds.Y - tubeCircle.Bounds.Height);
                    NSizeF size = new NSizeF(labelWidth, tubeCircle.Height);
                    
                    NTextShape tubeIndicator = new NTextShape(tube.ReformerAttributes.TubeNumber.ToString("D"), topLeftPoint, size)
                    {
                        Name = string.Format("{0} {1}", TubeIndicatorIdentifier, tube.Name),
                        Style = {TextStyle = new NTextStyle {FontStyle = {EmSize = new NLength(15)}}}
                    };
                    //tubeIndicator.Style.TextStyle.
                    DrawingDoc.ActiveLayer.AddChild(tubeIndicator);

                    //Add a row indicator at the appropriate position.
                    if (tube.ReformerAttributes.TubeNumber != 1) continue;
                    string rowText = CurrentInspection.ReformerInfo.UseRowLetters? tube.ReformerAttributes.RowLetter.ToString() : tube.ReformerAttributes.RowNumber.ToString("D");
                    NPointF rowTopLeftPoint = new NPointF(tubeCircle.Bounds.X - labelWidth, tubeCircle.Bounds.Y);
                    NTextShape rowIndicator = new NTextShape(rowText, rowTopLeftPoint, size)
                    {
                        Name = string.Format("{0} {1}", RowIndicatorIdentifier, rowText),
                        Style = { TextStyle = new NTextStyle { FontStyle = { EmSize = new NLength(15) } } }
                    };
                    DrawingDoc.ActiveLayer.AddChild(rowIndicator);
                }
            }
        }

        /// <summary> Removes all tubes that have been drawn using the auto-algorithm based on name identifier </summary>
        private void ClearDrawing()
        {
            foreach (NShape shape in DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().Where(shape => shape.Name.Contains(TubeIdentifier))) { DrawingDoc.ActiveLayer.RemoveChild(shape); }
            foreach (NShape shape in DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().Where(shape => shape.Name.Contains(TubeIndicatorIdentifier))) { DrawingDoc.ActiveLayer.RemoveChild(shape); }
            foreach (NShape shape in DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().Where(shape => shape.Name.Contains(RowIndicatorIdentifier))) { DrawingDoc.ActiveLayer.RemoveChild(shape); }
        }

        private void DrawTubes()
        {
            int maxTubeCount = CurrentInspection.Tubes.Max(T => T.ReformerAttributes.TubeNumber);
            int maxRowCount = CurrentInspection.Tubes.Max(T => T.ReformerAttributes.RowNumber);
            float circleDiam = FindBestCircleDiameter(maxTubeCount);
            float startY = GetStartingHeight(maxRowCount);

            foreach (ReformerTube tube in CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)))
            {
                NShape circle = _shapeFactory.CreateShape(BasicShapes.Ellipse);
                circle.Ports.Visible = false;
                circle.Style = GetCircleStyle(tube);
                float topLeftX = LeftPadding + (tube.ReformerAttributes.TubeNumber - 1) * circleDiam * 1.25f;
                float topLeftY = startY + circleDiam * 2f * (tube.ReformerAttributes.RowNumber - 1);
                circle.Bounds = new NRectangleF(topLeftX, topLeftY, circleDiam, circleDiam);
                circle.Name = TubeIdentifier + tube.Name;
                DrawingDoc.ActiveLayer.AddChild(circle);
            }
        }

        private void SetTextValues()
        {
            string headerText;
            if (MapType == MapTypes.ExpansionMap)
            {
                if (CurrentInspection.InspectionTool == Tool.MANTIS)
                {
                    headerText = CurrentInspection.ReformerInfo.NewTubes ? "Tube OD Layout Map" : "ID Tube Expansion Map";
                }
                else
                {
                    headerText = CurrentInspection.ReformerInfo.NewTubes ? "Tube ID Bore Layout Map" : "ID Tube Expansion Map";
                }
            }
            else headerText = "Level 1 Assessment Tube Map";
            
            SetItemText("Header", headerText);
            SetItemText("Date", DateTime.Now.ToString("MMMM dd, yyyy"));
            SetItemText("ClientName", CurrentInspection.ReformerInfo.Customer);
            SetItemText("ToolType", CurrentInspection.InspectionTool.ToString());
            string userName;
            try
            {
                userName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName; //Sometimes this line fails if it can't reach the DS Server for some reason. 
            }
            catch (Exception)
            { userName = "UnableToGetUserName"; }
            SetItemText("DrawnBy", userName);
            SetItemText("DrawingNumber", CurrentInspection.ReformerInfo.ReformerName);
            SetItemText("Title", string.Format("{0}: {1}", CurrentInspection.ReformerInfo.Customer, CurrentInspection.ReformerInfo.ReformerName));
            SetItemText("Material", CurrentInspection.ReformerInfo.DefaultMaterial.ToString());
            SetItemText("NumberTubesInspected", CurrentInspection.Tubes.Count.ToString("N0"));
            SetOfficeAddressStrings();
            SetUnitValues();
        }

        private void SetOfficeAddressStrings()
        {
            //valid projectoffice strings are keys to address lookup Dictionary
            string t = LifeQuestReformer.Instance.TheDataManager.CurrentProject.ProjectOffice;
            //array of three address strings returned by lookup
            string[] slist = QIGAddressLookup.GetAddressStringsByKey(t);
            //place company name as first of four address strings in template fields
            //place address strings in last three of four template fields (by field id in Nevron DB)
            for(int i = 0; i < QIGAddressLookup.ElementIDs.Length; i++)
            {
                SetItemTextById(QIGAddressLookup.ElementIDs[i], i == 0 ? LifeQuestReformer.CompanyName : slist[i - 1]);
            }
        }

        private void SetUnitValues()
        {
            double idInInches = CurrentInspection.ReformerInfo.DefaultID;
            double odInInches = CurrentInspection.ReformerInfo.DefaultOD;
            double lengthInInches = CurrentInspection.Tubes.FirstOrDefault()?.Specs.LengthDisplayUnits ?? 0;
            double idInMm = Length.Convert(idInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
            double odInMm = Length.Convert(odInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
            double lengthInmm = Length.Convert(lengthInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);

            bool isMetric = DefaultValues.AxialUnitSymbol.Contains("m");
            SetItemText("OuterDiameter", string.Format(!isMetric ? "{0} ({1})" : "{1} ({0})", (odInInches).ToString("F3") + " in.", (odInMm).ToString("F1") + " mm"));
            SetItemText("InnerDiameter", string.Format(!isMetric ? "{0} ({1})" : "{1} ({0})", idInInches.ToString("F3") + " in.", idInMm.ToString("F1") + " mm"));
            SetItemText("TubeLength", string.Format(!isMetric ? "{0} ({1})" : "{1} ({0})", lengthInInches.ToString("F1") + " in.", lengthInmm.ToString("F1") + " mm"));

            //Set Legend Values based off of the first tube's tolerance, if applicable.
            if (MapType != MapTypes.ExpansionMap) return;
            if (CurrentInspection.ReformerInfo.NewTubes) SetNewTubeLegend(isMetric, idInInches, (odInInches - idInInches) / 2);
            else SetOldTubeLegend();
        }

        private void SetOldTubeLegend()
        {
            if (CurrentInspection.Tubes.Count == 0) return;
            ReformerMaterial firstDataTube = CurrentInspection.Tubes.First(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ReformerAttributes.Material;
            SetItemText("Legend:>50Text", string.Format(">= {0}%", firstDataTube.CreepValues.GreaterThanFiftyPercent.ToString("F2")));
            SetItemText("Legend:50To>50Text", string.Format("{0}% to {1}%", firstDataTube.CreepValues.FiftyPercent.ToString("F2"), (firstDataTube.CreepValues.GreaterThanFiftyPercent - .01).ToString("F2")));
            SetItemText("Legend:40To50Text", string.Format("{0}% to {1}%", firstDataTube.CreepValues.FortyPercent.ToString("F2"), (firstDataTube.CreepValues.FiftyPercent - .01).ToString("F2")));
            SetItemText("Legend:30To40Text", string.Format("{0}% to {1}%", firstDataTube.CreepValues.ThirtyPercent.ToString("F2"), (firstDataTube.CreepValues.FortyPercent - .01).ToString("F2")));
            SetItemText("Legend:GoodText", string.Format("< {0}%", firstDataTube.CreepValues.ThirtyPercent.ToString("F2")));   
        }

        private void SetNewTubeLegend(bool isMetric, double idInInches, double wtInInches)
        {
            double idInMm = Length.Convert(idInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
            double wtInMm = Length.Convert(wtInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
            double odInInches = idInInches + wtInInches * 2;
            double odInMm = idInMm + wtInMm * 2;
            if (CurrentInspection.InspectionTool == Tool.MANTIS)
            {
                double odTolDownInInches = CurrentInspection.Tubes.Count > 0 ? CurrentInspection.Tubes[0].Specs.ODTolDown : 0;
                double odTolDownInMm = Length.Convert(odTolDownInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
                double odTolUpInInches = CurrentInspection.Tubes.Count > 0 ? CurrentInspection.Tubes[0].Specs.ODTolUp : 0;
                double odTolUpInMm = Length.Convert(odTolUpInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
                SetItemText("Legend:GoodText", string.Format("{0} {1} - {2} {1} OD", isMetric ? (odInMm + odTolDownInMm).ToString("F1") : (odInInches + odTolDownInInches).ToString("F3"), isMetric ? "mm" : "in.", isMetric ? (odInMm + odTolUpInMm).ToString("F1") : (odInInches + odTolUpInInches).ToString("F3")));
                SetItemText("Legend:HighText", string.Format("> {0} {1} MAX. OD", isMetric ? (odInMm + odTolUpInMm).ToString("F1") : (odInInches + odTolUpInInches).ToString("F3"), isMetric ? "mm" : "in."));
                SetItemText("Legend:LowText", string.Format("< {0} {1} MIN. OD", isMetric ? (odInMm + odTolDownInMm).ToString("F1") : (odInInches + odTolDownInInches).ToString("F3"), isMetric ? "mm" : "in."));
                SetItemText("Legend:LowAndHighText", string.Format("> {0} {1} and < {2} {1} OD", isMetric ? (odInMm + odTolUpInMm).ToString("F1") : (odInInches + odTolUpInInches).ToString("F3"), isMetric ? "mm" : "in.", isMetric ? (odInMm + odTolDownInMm).ToString("F1") : (odInInches + odTolDownInInches).ToString("F3")));
            }
            else
            {
                double idTolDownInInches = CurrentInspection.Tubes.Count > 0 ? CurrentInspection.Tubes[0].Specs.IDTolDown : 0;
                double idTolDownInMm = Length.Convert(idTolDownInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
                double idTolUpInInches = CurrentInspection.Tubes.Count > 0 ? CurrentInspection.Tubes[0].Specs.IDTolUp : 0;
                double idTolUpInMm = Length.Convert(idTolUpInInches, Length.LengthScale.Millimeters, Length.LengthScale.Inches);
                SetItemText("Legend:GoodText", string.Format("{0} {1} - {2} {1} ID", isMetric ? (idInMm + idTolDownInMm).ToString("F1") : (idInInches + idTolDownInInches).ToString("F3"), isMetric ? "mm" : "in.", isMetric ? (idInMm + idTolUpInMm).ToString("F1") : (idInInches + idTolUpInInches).ToString("F3")));
                SetItemText("Legend:HighText", string.Format("> {0} {1} MAX. ID", isMetric ? (idInMm + idTolUpInMm).ToString("F1") : (idInInches + idTolUpInInches).ToString("F3"), isMetric ? "mm" : "in."));
                SetItemText("Legend:LowText", string.Format("< {0} {1} MIN. ID", isMetric ? (idInMm + idTolDownInMm).ToString("F1") : (idInInches + idTolDownInInches).ToString("F3"), isMetric ? "mm" : "in."));
                SetItemText("Legend:LowAndHighText", string.Format("> {0} {1} and < {2} {1} ID", isMetric ? (idInMm + idTolUpInMm).ToString("F1") : (idInInches + idTolUpInInches).ToString("F3"), isMetric ? "mm" : "in.", isMetric ? (idInMm + idTolDownInMm).ToString("F1") : (idInInches + idTolDownInInches).ToString("F3")));
            }
        }

        /// <summary>  Sets the text of the given diagram object (found by name).  </summary>
        private void SetItemText(string ItemName, string TextToGive)
        {
            if (TextToGive.IsNullOrEmpty()) TextToGive = "N/A";
            foreach (NShape shape in DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().Where(shape => shape.Name == ItemName)) { shape.Text = TextToGive;}
        }

        /// <summary>  Sets the text of the given diagram object (found by Id).  </summary>
        private void SetItemTextById(int id, string TextToGive)
        {
            if (TextToGive.IsNullOrEmpty()) TextToGive = "N/A";
            foreach (NShape shape in DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().Where(shape => shape.Id == id)) { shape.Text = TextToGive;}
        }

        public void RestoreTextFieldsFromHolder()
        {
            int[] list = TextHolder.GetFieldIds();

            foreach (int i in list)
            {
                string s = TextHolder.GetTextItem(i);
                if (!s.IsNullOrEmpty())
                {
                    SetItemTextById(i, s);
                }
            }
        }

        public void LoadTextFieldsIntoHolder()
        {
            int[] list = TextHolder.GetFieldIds();
            TextHolder.Purge();
            foreach (int i in list)
            {
                //NShape t = TheView.Document.Descendants(NFilters.Shape2D, 2).Cast<NShape>().SingleOrDefault(shape => shape.Id == i);
                NShape s = DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().SingleOrDefault(shape => shape.Id == i);
                if (s != null)
                {
                    TextHolder.AddTextItem(s.Text);
                }
            }
        }

        private NStyle GetCircleStyle(ReformerTube Tube)
        {
            NStyle returnStyle = new NStyle {StrokeStyle = new NStrokeStyle()};
            NFillStyle legendFillStyle;
            if (MapType == MapTypes.ExpansionMap) { legendFillStyle = CurrentInspection.ReformerInfo.NewTubes ? GetNewTubeLegendStyle(Tube) : GetOldTubeLegendStyle(Tube); }
            else { legendFillStyle = GetLevel1LegendStyle(Tube); }

            returnStyle.FillStyle = legendFillStyle;
            return returnStyle;
        }

        private NFillStyle GetLevel1LegendStyle(ReformerTube Tube)
        {
            string legendItemName = "Legend:NoData";
            if (Tube.ReformerAttributes.Level1Result == RemainingLifeStatus.RetireNow) legendItemName = "Legend:RetireNow";
            if (Tube.ReformerAttributes.Level1Result == RemainingLifeStatus.ConsultEngineering) legendItemName = "Legend:ConsultEngineering";
            if (Tube.ReformerAttributes.Level1Result == RemainingLifeStatus.Okay) legendItemName = "Legend:Passes";
            NShape shape = FindShapeByName(legendItemName);
            return shape != null ? shape.Style.FillStyle : new NColorFillStyle(Color.White);
        }

        /// <summary> Uses ID data (for both MANTIS and LOTIS, according to Tim), to determine the circle color by comparing growth to the tube's material. </summary>
        private NFillStyle GetOldTubeLegendStyle(ReformerTube Tube)
        {
            double growth = Math.Round(Tube.ReformerAttributes.OverallIDGrowth, 2);
            string legendItemName = "Legend:NoData";
            if (Tube.ReformerAttributes.Material == null) return new NColorFillStyle(Color.White);

            if (growth >= Tube.ReformerAttributes.Material.CreepValues.GreaterThanFiftyPercent) legendItemName = "Legend:>50";
            else if (growth >= Tube.ReformerAttributes.Material.CreepValues.FiftyPercent) legendItemName = "Legend:50To>50";
            else if (growth >= Tube.ReformerAttributes.Material.CreepValues.FortyPercent) legendItemName = "Legend:40To50";
            else if (growth >= Tube.ReformerAttributes.Material.CreepValues.ThirtyPercent) legendItemName = "Legend:30To40";
            else if (growth >= 0) legendItemName = "Legend:Good";
            NShape shape = FindShapeByName(legendItemName);
            return shape != null ? shape.Style.FillStyle : new NColorFillStyle(Color.White);
        }

        private NFillStyle GetNewTubeLegendStyle(ReformerTube Tube)
        {
            string legendItemName = "Legend:Good";
            if (CurrentInspection.InspectionTool == Tool.MANTIS)
            {
                double maxOD = Tube.ReformerAttributes.PickedOverallMaxODDisplayUnits;
                double refOD = Tube.ReformerAttributes.PickedOverallRefODDisplayUnits;
                double designedTolUp = Math.Round(Tube.Specs.ODTolUpDisplayUnits + Tube.Specs.DiameterOutsideInDisplayUnits, DefaultValues.MeasNumDecimals);
                double designedTolDown = Math.Round(Tube.Specs.ODTolDownDisplayUnits + Tube.Specs.DiameterOutsideInDisplayUnits, DefaultValues.MeasNumDecimals);
                if (maxOD > designedTolUp) legendItemName = "Legend:High";
                if (refOD < designedTolDown) legendItemName = "Legend:Low";
                if (maxOD > designedTolUp && refOD < designedTolDown) legendItemName = "Legend:LowAndHigh";
                if (maxOD.IsNanOrZero()) legendItemName = "Legend:NoData";
            }
            else
            {
                double maxID = Tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits;
                double refID = Tube.ReformerAttributes.PickedOverallRefIDDisplayUnits;
                double designedTolUp = Math.Round(Tube.Specs.IDTolUpDisplayUnits + Tube.Specs.DiameterInsideInDisplayUnits, DefaultValues.MeasNumDecimals);
                double designedTolDown = Math.Round(Tube.Specs.IDTolDownDisplayUnits + Tube.Specs.DiameterInsideInDisplayUnits, DefaultValues.MeasNumDecimals);
                if (maxID > designedTolUp) legendItemName = "Legend:High";
                if (refID < designedTolDown) legendItemName = "Legend:Low";
                if (maxID > designedTolUp && refID < designedTolDown) legendItemName = "Legend:LowAndHigh";
                if (Tube.ReformerAttributes.PickedOverallMaxIDDisplayUnits.IsNanOrZero()) legendItemName = "Legend:NoData";
            }
            NShape shape = FindShapeByName(legendItemName);
            return shape != null ? shape.Style.FillStyle : new NColorFillStyle(Color.White);
        }

        private NShape FindShapeByName(string ShapeName)
        {
            //Try to find a shape by name. If it doesn't exist, return null;
            return DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().All(shape => shape.Name != ShapeName) 
                ? null 
                : DrawingDoc.Descendants(NFilters.Shape2D, 2).Cast<NShape>().First(shape => shape.Name == ShapeName);
        }

        private float GetStartingHeight(int MaxRowCount)
        {
            return 1.5f; //I was going to make this more complicated, but said screw it instead as this worked better.
        }

        private float FindBestCircleDiameter(int MaxItemsPerRow)
        {
            var docWidthInInches = DrawingDoc.Width;
            float usefulSpace = docWidthInInches - LeftPadding - RightPadding;

            float widthPerTube = usefulSpace / (MaxItemsPerRow - 1);
            return widthPerTube * .8f;
        }

        #endregion Drawing Stuff
    }
}
