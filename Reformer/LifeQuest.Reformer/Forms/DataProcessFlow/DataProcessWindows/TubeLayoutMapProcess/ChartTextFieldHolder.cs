﻿using System;
using System.Collections.Generic;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess
{
    class ChartTextFieldHolder
    {
        private int[] _fieldIDs = {217, 215, 979, 981, 205, 113, 121, 139, 143, 125, 173, 181};

        private List<string> TextItems = new List<string>();

        public int[] GetFieldIds()
        {
            return _fieldIDs;
        }

        public void Purge()
        {
            TextItems.Clear();
        }

        public string GetTextItem(int idx)
        {
            if (TextItems.Count > 0)
            {
                int i = Array.IndexOf(_fieldIDs, idx);
                return TextItems[i];
            }
            else
            {
                return "";
            }
        }

        public void AddTextItem(string text)
        {
            TextItems.Add(text);
        }

    }
}
