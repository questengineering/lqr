﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess
{
    sealed partial class TubeLayoutMap
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tubeLayoutMapControl1 = new Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess.TubeLayoutMapControl();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.uiMapType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnDrawSample = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMapType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiMapType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // tubeLayoutMapControl1
            // 
            this.tubeLayoutMapControl1.CurrentInspection = null;
            this.tubeLayoutMapControl1.Location = new System.Drawing.Point(12, 38);
            this.tubeLayoutMapControl1.MapType = Reformer.Data.MapTypes.ExpansionMap;
            this.tubeLayoutMapControl1.Name = "tubeLayoutMapControl1";
            this.tubeLayoutMapControl1.Size = new System.Drawing.Size(792, 424);
            this.tubeLayoutMapControl1.TabIndex = 0;
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.uiMapType);
            this.lcMain.Controls.Add(this.btnDrawSample);
            this.lcMain.Controls.Add(this.tubeLayoutMapControl1);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.layoutControlGroup1;
            this.lcMain.Size = new System.Drawing.Size(816, 474);
            this.lcMain.TabIndex = 1;
            this.lcMain.Text = "layoutControl1";
            // 
            // uiMapType
            // 
            this.uiMapType.Location = new System.Drawing.Point(62, 12);
            this.uiMapType.Name = "uiMapType";
            this.uiMapType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMapType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.uiMapType.Size = new System.Drawing.Size(96, 20);
            this.uiMapType.StyleController = this.lcMain;
            this.uiMapType.TabIndex = 5;
            this.uiMapType.SelectedIndexChanged += new System.EventHandler(this.uiMapType_SelectedIndexChanged);
            // 
            // btnDrawSample
            // 
            this.btnDrawSample.Location = new System.Drawing.Point(162, 12);
            this.btnDrawSample.Name = "btnDrawSample";
            this.btnDrawSample.Size = new System.Drawing.Size(85, 22);
            this.btnDrawSample.StyleController = this.lcMain;
            this.btnDrawSample.TabIndex = 4;
            this.btnDrawSample.Text = "Draw Sample";
            this.btnDrawSample.Click += new System.EventHandler(this.btnDrawSample_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.lciMapType,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(816, 474);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tubeLayoutMapControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(796, 428);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnDrawSample;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(150, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(89, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // lciMapType
            // 
            this.lciMapType.Control = this.uiMapType;
            this.lciMapType.CustomizationFormText = "Map Type";
            this.lciMapType.Location = new System.Drawing.Point(0, 0);
            this.lciMapType.Name = "lciMapType";
            this.lciMapType.Size = new System.Drawing.Size(150, 26);
            this.lciMapType.Text = "Map Type";
            this.lciMapType.TextSize = new System.Drawing.Size(47, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(239, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(557, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TubeLayoutMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lcMain);
            this.Name = "TubeLayoutMap";
            this.Size = new System.Drawing.Size(816, 474);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiMapType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TubeLayoutMapControl tubeLayoutMapControl1;
        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit uiMapType;
        private DevExpress.XtraEditors.SimpleButton btnDrawSample;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem lciMapType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

        
        
    }
}
