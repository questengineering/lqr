﻿using System;
using Reformer.Data;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess
{
    public sealed partial class TubeLayoutMap : BaseProcessWindow
    {
        
        public TubeLayoutMap()
        {
            InitializeComponent();
            UpdateDataSource();
        }
        
        private MapTypes MapType
        {
            get { return (MapTypes)Enum.Parse(typeof(MapTypes), uiMapType.EditValue.ToString()); }
            set { uiMapType.EditValue = value; }
        }
        
        public override void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDataSource));
                return;
            }
            CurrentInspection = CurrentProject.CurrentInspectionFile;
            InitializeMenus();
            tubeLayoutMapControl1.CurrentInspection = CurrentProject.CurrentInspectionFile;
            tubeLayoutMapControl1.LoadFreshOrStored();
        }

        private void btnDrawSample_Click(object sender, EventArgs e)
        {
            ReformerMain.TempStoreDocument(tubeLayoutMapControl1.DrawingDoc, MapType == MapTypes.Level1Assessment);
            tubeLayoutMapControl1.LoadTextFieldsIntoHolder();
            Enabled = false;
            tubeLayoutMapControl1.Redraw();
            Enabled = true;
            tubeLayoutMapControl1.RestoreTextFieldsFromHolder();
        }

        private void InitializeMenus()
        {
            uiMapType.Properties.Items.Clear();
            //5/4/2014 new tubes don't get level 1 assessments.
            if (CurrentInspection != null) 
                uiMapType.Properties.Items.Add(MapTypes.ExpansionMap);
            MapType = MapTypes.ExpansionMap;
        }

        private void uiMapType_SelectedIndexChanged(object sender, EventArgs e)
        {
            tubeLayoutMapControl1.LoadTextFieldsIntoHolder();
            tubeLayoutMapControl1.MapType = MapType;
            OnProgressChanged(0, "Loading Template...");
            tubeLayoutMapControl1.LoadFreshOrStored();
            tubeLayoutMapControl1.RestoreTextFieldsFromHolder();
            OnProgressChanged(100, "Loaded Template.");
        }
        
       
       // private void ExportImage()
       // {
       //     NImageExporter exporter = new NImageExporter(nDrawDoc);
       //     exporter.Resolution = 300;
       //     exporter.GraphicsSettings.SmoothingMode = SmoothingMode.HighQuality;
       //     SaveFileDialog saveDialog = new SaveFileDialog();
       //     saveDialog.DefaultExt = ".png";
       //     saveDialog.Filter = "PNG Image (*.png)|*.png";
       //     saveDialog.InitialDirectory = LifeQuestReformer.Instance.TheDataManager.CurrentProject.DirectoryPath;
       //     saveDialog.FileName = MapType.ToString();
       //     DialogResult result = saveDialog.ShowDialog(this);
       //     if (result == DialogResult.OK)
       //         exporter.SaveToImageFile(saveDialog.FileName, ImageFormat.Png);
       // }

       // #endregion Private/Protected Methods

       // #region Events

    }
}
