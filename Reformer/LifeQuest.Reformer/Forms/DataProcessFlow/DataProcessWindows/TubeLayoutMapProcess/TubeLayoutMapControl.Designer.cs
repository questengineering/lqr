﻿using Nevron.Diagram;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess
{
    partial class TubeLayoutMapControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TubeLayoutMapControl));
            this.CommandBar = new Nevron.Diagram.WinForm.Commands.NDiagramCommandBarsManager();
            this.topPanel = new System.Windows.Forms.Panel();
            this.TheView = new Nevron.Diagram.WinForm.NDrawingView();
            this.DrawingDoc = new Nevron.Diagram.NDrawingDocument();
            this.TheBackPanel = new DevExpress.XtraEditors.PanelControl();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTheView = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheBackPanel)).BeginInit();
            this.TheBackPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTheView)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.AutoRefresh = true;
            this.CommandBar.LibraryBrowser = null;
            this.CommandBar.PanAndZoomControl = null;
            this.CommandBar.ParentControl = this.topPanel;
            this.CommandBar.PropertyBrowser = null;
            this.CommandBar.RefreshInterval = 300;
            this.CommandBar.StatusBar = null;
            this.CommandBar.View = this.TheView;
            // 
            // topPanel
            // 
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(2, 2);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1048, 53);
            this.topPanel.TabIndex = 4;
            // 
            // TheView
            // 
            this.TheView.AllowDrop = true;
            this.TheView.DesignTimeState = ((Nevron.Diagram.NBinaryState)(resources.GetObject("TheView.DesignTimeState")));
            this.TheView.Document = this.DrawingDoc;
            this.TheView.Location = new System.Drawing.Point(12, 12);
            this.TheView.Name = "TheView";
            this.TheView.Size = new System.Drawing.Size(1024, 353);
            this.TheView.TabIndex = 0;
            this.TheView.Text = "nDrawingView1";
            // 
            // DrawingDoc
            // 
            this.DrawingDoc.DesignTimeState = ((Nevron.Diagram.NBinaryState)(resources.GetObject("DrawingDoc.DesignTimeState")));
            // 
            // TheBackPanel
            // 
            this.TheBackPanel.Controls.Add(this.lcMain);
            this.TheBackPanel.Controls.Add(this.topPanel);
            this.TheBackPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TheBackPanel.Location = new System.Drawing.Point(0, 0);
            this.TheBackPanel.Name = "TheBackPanel";
            this.TheBackPanel.Size = new System.Drawing.Size(1052, 434);
            this.TheBackPanel.TabIndex = 0;
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.TheView);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(2, 55);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(1048, 377);
            this.lcMain.TabIndex = 5;
            this.lcMain.Text = "layoutControl1";
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "lcgMain";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTheView});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.Size = new System.Drawing.Size(1048, 377);
            this.lcgMain.TextVisible = false;
            // 
            // lciTheView
            // 
            this.lciTheView.Control = this.TheView;
            this.lciTheView.CustomizationFormText = "lciTheView";
            this.lciTheView.Location = new System.Drawing.Point(0, 0);
            this.lciTheView.Name = "lciTheView";
            this.lciTheView.Size = new System.Drawing.Size(1028, 357);
            this.lciTheView.TextSize = new System.Drawing.Size(0, 0);
            this.lciTheView.TextVisible = false;
            // 
            // TubeLayoutMapControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.TheBackPanel);
            this.Name = "TubeLayoutMapControl";
            this.Size = new System.Drawing.Size(1052, 434);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheBackPanel)).EndInit();
            this.TheBackPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTheView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraLayout.LayoutControl lcMain;
        public DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        public DevExpress.XtraLayout.LayoutControlItem lciTheView;
        public NDrawingDocument DrawingDoc;
        public DevExpress.XtraEditors.PanelControl TheBackPanel;
        public Nevron.Diagram.WinForm.NDrawingView TheView;
        public Nevron.Diagram.WinForm.Commands.NDiagramCommandBarsManager CommandBar;
        public System.Windows.Forms.Panel topPanel;



    }
}
