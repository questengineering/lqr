﻿using System.Collections.Generic;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    partial class Level3Export
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExportSummary = new System.Windows.Forms.Button();
            this.btnExportDetailedInfo = new System.Windows.Forms.Button();
            this.btnOpenExportFolder = new System.Windows.Forms.Button();
            this.gridLevel3Data = new DevExpress.XtraGrid.GridControl();
            this.bsSummaryItem = new System.Windows.Forms.BindingSource(this.components);
            this.viewLevel3Data = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTubeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel1GrowthRatePercentPerYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverallGrowthPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel1MaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel1PreviousID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel1Age = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLevel1YearsUntilNextInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverallMaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverallBaselineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverallAge = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridLevel3Data)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSummaryItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewLevel3Data)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExportSummary
            // 
            this.btnExportSummary.Location = new System.Drawing.Point(4, 4);
            this.btnExportSummary.Name = "btnExportSummary";
            this.btnExportSummary.Size = new System.Drawing.Size(94, 23);
            this.btnExportSummary.TabIndex = 0;
            this.btnExportSummary.Text = "Export Summary";
            this.btnExportSummary.UseVisualStyleBackColor = true;
            this.btnExportSummary.Click += new System.EventHandler(this.btnExportSummary_Click);
            // 
            // btnExportDetailedInfo
            // 
            this.btnExportDetailedInfo.Location = new System.Drawing.Point(104, 4);
            this.btnExportDetailedInfo.Name = "btnExportDetailedInfo";
            this.btnExportDetailedInfo.Size = new System.Drawing.Size(94, 23);
            this.btnExportDetailedInfo.TabIndex = 1;
            this.btnExportDetailedInfo.Text = "Export Details";
            this.btnExportDetailedInfo.UseVisualStyleBackColor = true;
            this.btnExportDetailedInfo.Click += new System.EventHandler(this.btnExportDetailedInfo_Click);
            // 
            // btnOpenExportFolder
            // 
            this.btnOpenExportFolder.Location = new System.Drawing.Point(204, 4);
            this.btnOpenExportFolder.Name = "btnOpenExportFolder";
            this.btnOpenExportFolder.Size = new System.Drawing.Size(108, 23);
            this.btnOpenExportFolder.TabIndex = 2;
            this.btnOpenExportFolder.Text = "Open Output Folder";
            this.btnOpenExportFolder.UseVisualStyleBackColor = true;
            this.btnOpenExportFolder.Click += new System.EventHandler(this.btnOpenExportFolder_Click);
            // 
            // gridLevel3Data
            // 
            this.gridLevel3Data.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLevel3Data.DataSource = this.bsSummaryItem;
            this.gridLevel3Data.Location = new System.Drawing.Point(4, 34);
            this.gridLevel3Data.MainView = this.viewLevel3Data;
            this.gridLevel3Data.Name = "gridLevel3Data";
            this.gridLevel3Data.Size = new System.Drawing.Size(1028, 351);
            this.gridLevel3Data.TabIndex = 3;
            this.gridLevel3Data.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewLevel3Data});
            // 
            // bsSummaryItem
            // 
            this.bsSummaryItem.DataSource = typeof(Reformer.Forms.DataProcessFlow.DataProcessWindows.CachedLevel3SummaryItem);
            // 
            // viewLevel3Data
            // 
            this.viewLevel3Data.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTubeName,
            this.colLevel1GrowthRatePercentPerYear,
            this.colOverallGrowthPercent,
            this.colLevel1MaxID,
            this.colLevel1PreviousID,
            this.colLevel1Age,
            this.colLevel1YearsUntilNextInspection,
            this.colOverallMaxID,
            this.colOverallBaselineID,
            this.colOverallAge});
            this.viewLevel3Data.GridControl = this.gridLevel3Data;
            this.viewLevel3Data.Name = "viewLevel3Data";
            this.viewLevel3Data.OptionsView.ShowGroupPanel = false;
            // 
            // colTubeName
            // 
            this.colTubeName.FieldName = "TubeName";
            this.colTubeName.Name = "colTubeName";
            this.colTubeName.OptionsColumn.AllowEdit = false;
            this.colTubeName.OptionsColumn.ReadOnly = true;
            this.colTubeName.Visible = true;
            this.colTubeName.VisibleIndex = 0;
            // 
            // colLevel1GrowthRatePercentPerYear
            // 
            this.colLevel1GrowthRatePercentPerYear.FieldName = "Level1GrowthRatePercentPerYear";
            this.colLevel1GrowthRatePercentPerYear.Name = "colLevel1GrowthRatePercentPerYear";
            this.colLevel1GrowthRatePercentPerYear.OptionsColumn.AllowEdit = false;
            this.colLevel1GrowthRatePercentPerYear.OptionsColumn.ReadOnly = true;
            this.colLevel1GrowthRatePercentPerYear.Visible = true;
            this.colLevel1GrowthRatePercentPerYear.VisibleIndex = 1;
            // 
            // colOverallGrowthPercent
            // 
            this.colOverallGrowthPercent.FieldName = "OverallGrowthPercent";
            this.colOverallGrowthPercent.Name = "colOverallGrowthPercent";
            this.colOverallGrowthPercent.OptionsColumn.AllowEdit = false;
            this.colOverallGrowthPercent.OptionsColumn.ReadOnly = true;
            this.colOverallGrowthPercent.Visible = true;
            this.colOverallGrowthPercent.VisibleIndex = 2;
            // 
            // colLevel1MaxID
            // 
            this.colLevel1MaxID.FieldName = "Level1MaxID";
            this.colLevel1MaxID.Name = "colLevel1MaxID";
            this.colLevel1MaxID.OptionsColumn.AllowEdit = false;
            this.colLevel1MaxID.OptionsColumn.ReadOnly = true;
            this.colLevel1MaxID.Visible = true;
            this.colLevel1MaxID.VisibleIndex = 3;
            // 
            // colLevel1PreviousID
            // 
            this.colLevel1PreviousID.FieldName = "Level1PreviousID";
            this.colLevel1PreviousID.Name = "colLevel1PreviousID";
            this.colLevel1PreviousID.OptionsColumn.AllowEdit = false;
            this.colLevel1PreviousID.OptionsColumn.ReadOnly = true;
            this.colLevel1PreviousID.Visible = true;
            this.colLevel1PreviousID.VisibleIndex = 4;
            // 
            // colLevel1Age
            // 
            this.colLevel1Age.FieldName = "Level1Age";
            this.colLevel1Age.Name = "colLevel1Age";
            this.colLevel1Age.OptionsColumn.AllowEdit = false;
            this.colLevel1Age.OptionsColumn.ReadOnly = true;
            this.colLevel1Age.Visible = true;
            this.colLevel1Age.VisibleIndex = 5;
            // 
            // colLevel1YearsUntilNextInspection
            // 
            this.colLevel1YearsUntilNextInspection.FieldName = "Level1YearsUntilNextInspection";
            this.colLevel1YearsUntilNextInspection.Name = "colLevel1YearsUntilNextInspection";
            this.colLevel1YearsUntilNextInspection.OptionsColumn.AllowEdit = false;
            this.colLevel1YearsUntilNextInspection.OptionsColumn.ReadOnly = true;
            this.colLevel1YearsUntilNextInspection.Visible = true;
            this.colLevel1YearsUntilNextInspection.VisibleIndex = 6;
            // 
            // colOverallMaxID
            // 
            this.colOverallMaxID.FieldName = "OverallMaxID";
            this.colOverallMaxID.Name = "colOverallMaxID";
            this.colOverallMaxID.OptionsColumn.AllowEdit = false;
            this.colOverallMaxID.OptionsColumn.ReadOnly = true;
            this.colOverallMaxID.Visible = true;
            this.colOverallMaxID.VisibleIndex = 7;
            // 
            // colOverallBaselineID
            // 
            this.colOverallBaselineID.FieldName = "OverallBaselineID";
            this.colOverallBaselineID.Name = "colOverallBaselineID";
            this.colOverallBaselineID.OptionsColumn.AllowEdit = false;
            this.colOverallBaselineID.OptionsColumn.ReadOnly = true;
            this.colOverallBaselineID.Visible = true;
            this.colOverallBaselineID.VisibleIndex = 8;
            // 
            // colOverallAge
            // 
            this.colOverallAge.FieldName = "OverallAge";
            this.colOverallAge.Name = "colOverallAge";
            this.colOverallAge.OptionsColumn.AllowEdit = false;
            this.colOverallAge.OptionsColumn.ReadOnly = true;
            this.colOverallAge.Visible = true;
            this.colOverallAge.VisibleIndex = 9;
            // 
            // Level3Export
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridLevel3Data);
            this.Controls.Add(this.btnOpenExportFolder);
            this.Controls.Add(this.btnExportDetailedInfo);
            this.Controls.Add(this.btnExportSummary);
            this.Name = "Level3Export";
            this.Size = new System.Drawing.Size(1035, 388);
            ((System.ComponentModel.ISupportInitialize)(this.gridLevel3Data)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSummaryItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewLevel3Data)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExportSummary;
        private System.Windows.Forms.Button btnExportDetailedInfo;
        private System.Windows.Forms.Button btnOpenExportFolder;
        private DevExpress.XtraGrid.GridControl gridLevel3Data;
        private DevExpress.XtraGrid.Views.Grid.GridView viewLevel3Data;
        private System.Windows.Forms.BindingSource bsSummaryItem;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeName;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel1GrowthRatePercentPerYear;
        private DevExpress.XtraGrid.Columns.GridColumn colOverallGrowthPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel1MaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel1PreviousID;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel1Age;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel1YearsUntilNextInspection;
        private DevExpress.XtraGrid.Columns.GridColumn colOverallMaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colOverallBaselineID;
        private DevExpress.XtraGrid.Columns.GridColumn colOverallAge;

    }
}
