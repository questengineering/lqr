﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Kitware.VTK;
using NGenerics.Extensions;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using QuestIntegrity.Graphics.ColorScaleBar;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.Options;
using Reformer.Graphics;
using Reformer.Visualization;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    public partial class Single3DExport : BaseProcessWindow, IHasColorScaleBar
    {
        private readonly vtkTextActor _headerActor = new vtkTextActor();
        private readonly vtkTextActor _axisTitleActor = new vtkTextActor();
        public Reformer3DWindow GraphicsWindow { get; private set; }
        protected bool UseRowLetters { get { return CurrentInspection != null && CurrentInspection.ReformerInfo.UseRowLetters; } }
        protected bool Vertical { get { return chkVertical.Checked; } }
        protected readonly SaveFileDialog TheSaveFileDialog = new SaveFileDialog();

        protected Size VerticalSize = new Size(634, 1014);
        protected Size HorizontalSize = new Size(1014, 500);
        /// <summary> Describes the shape of the longest tube gathered in the dataset that all others should be compared to. </summary>
        private const int LengthOverThickness = (int) (1014/70.5);

        private double _currentRotation;
        protected vtkActor CurrentTubeActor;
        public ReformerTube CurrentTube { get { return uiTube.EditValue as ReformerTube; } set { uiTube.EditValue = value; } }
        internal ReformerScalar SelectedScalarKey { get { return uiScalar.EditValue as ReformerScalar; } }
        private bool _changingTubes;
        
        /// <summary> Determines the appropriate level of detail for a given tube's data. Limits axial data to 2500 slices. </summary>
        private float LevelOfDetail 
        { 
            get 
            { 
                if (CurrentTube == null) return 1;
                int axialIndexLength = (int)CurrentTube.GetDataDimensions(ReformerScalar.AxialPosition)[0];
                if (axialIndexLength > 2500)
                    return axialIndexLength / 2500f;
                return 1;
            } 
        }

        private int MinRadialPoints
        {
            get
            {
                if (CurrentTube == null) return 0;
                int leastGreaterDivisor = (int) Math.Ceiling(75d/CurrentTube.Inspection.NumRadialReadings);
                return CurrentTube.Inspection.NumRadialReadings*leastGreaterDivisor;
            }
        }
        
        #region Initialization Methods

        public Single3DExport()
        {
            InitializeComponent();
            SetupGraphics();
            UpdateDataSource();
            AttachColorBarChangedEvent();
        }

        private void SetupMenus()
        {
            uiStatMeasures.Properties.Items.Clear();
            uiStatMeasures.Properties.Items.Add("None");
            if (CurrentInspection == null) return;
            uiStatMeasures.Properties.Items.AddRange(new[] { StatisticalMeasures.Minimum, CurrentInspection.DataMeasure, StatisticalMeasures.Maximum });
        }

        private void SetupGraphics()
        {
            GraphicsWindow = new Reformer3DWindow
            {
                Location = new Point(0,0),
                Anchor = AnchorStyles.Top | AnchorStyles.Left,
                Dock = DockStyle.None,
                Name = "TheGraphics3DWindow",
                Size = HorizontalSize,
            };
            
            ScrollableGraphicsPanel.Controls.Add(GraphicsWindow);
            ScrollableGraphicsPanel.Controls.SetChildIndex(GraphicsWindow, 0);
            GraphicsWindow.Renderer.GetActiveCamera().ParallelProjectionOn();

            vtkTextProperty tProp = _headerActor.GetTextProperty();
            tProp.SetFontSize(18);
            tProp.SetFontFamilyToArial();
            tProp.SetJustificationToCentered();
            tProp.BoldOn();
            tProp.SetColor(0, 0, 0);
            GraphicsWindow.Renderer.AddActor2D(_headerActor);

            GraphicsWindow.SetInteractive(false);
            _axisTitleActor.SetOrientation(90);
            _axisTitleActor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport();
            vtkTextProperty tpTitle = _axisTitleActor.GetTextProperty();
            tpTitle.SetJustificationToCentered();
            tpTitle.ItalicOff();
            tpTitle.BoldOff();
            tpTitle.SetColor(0, 0, 0);
            tpTitle.SetFontSize(20);
            GraphicsWindow.Renderer.AddActor2D(_axisTitleActor);
        }

        public override sealed void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDataSource));
                return;
            }
            BSTubes.DataSource = CurrentInspection != null ? CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)) : null;
            SetupMenus();
        }

        #endregion Initialization Methods

        #region Event Handlers

        private void ScalarChanged(object sender, EventArgs e)
        {
            if (SelectedScalarKey == null) return;
            GraphicsWindow.SetScalarOnColorScaleBar(SelectedScalarKey);
            DrawTube();
        }

        private void SubScalarChanged(object sender, EventArgs e) { DrawTube(); }

        private void TubeChanged(object sender, EventArgs e)
        {
            if (CurrentTube != null)
            {
                _changingTubes = true;
                uiScalar.Properties.Items.Clear();
                uiScalar.Properties.Items.AddRange(CurrentTube.AvailableScalars.Where(S => S.DisplayName.Contains("Radius")).ToList());
                uiScalar.EditValue = CurrentInspection.InspectionTool == Tool.MANTIS ? ReformerScalar.RadiusOutside : ReformerScalar.RadiusInside;
                _changingTubes = false;
            }
            DrawTube();
        }

        private void chkVertical_CheckedChanged(object sender, EventArgs e)
        {
            GraphicsWindow.Size = Vertical ? VerticalSize : HorizontalSize;
            if (CurrentTubeActor != null) DrawTube();
        }

        //The OnHandleCreated method has to add the graphics panel back in if it's been removed. Otherwise VTK will crash (give a black screen in debug, crash at end user)
        protected override void OnHandleCreated(EventArgs e)
        {
            if (ScrollableGraphicsPanel != null) { ScrollableGraphicsPanel.Controls.Add(GraphicsWindow); }
            base.OnHandleCreated(e);
        }

        //The OnHandleDestroyed method has to remove the VTK graphics panel so it doesn't get screwed up. Otherwise VTK will crash (give a black screen in debug, crash at end user)
        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (ScrollableGraphicsPanel != null) { ScrollableGraphicsPanel.Controls.Remove(GraphicsWindow); }
            base.OnHandleDestroyed(e);
        }

        private void btnExportCurrentImage_ItemClick(object sender, EventArgs e)
        {
            if (CurrentTube == null) return;
            TheSaveFileDialog.FileName = string.Format("{0}.jpg", CurrentTube.Name);
            TheSaveFileDialog.InitialDirectory = UsefulDirectories.ThreeDImage.FullName;
            TheSaveFileDialog.Filter = @"JPG files (*.jpg)|*.jpg";
            if (TheSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                ExportScreenshot(TheSaveFileDialog.FileName);
                OnProgressChanged(100, string.Format("Exported: {0}", CurrentTube.Name));
            }
        }

        private void btnExportAllFromInspection_ItemClick(object sender, EventArgs e)
        {
            if (CurrentInspection == null) { OnProgressChanged(100, "No Inspection selected. Cannot export."); return; }
            if (SelectedScalarKey == null) { OnProgressChanged(100, "No Scalar Selected. Cannot export."); return; }
            try
            {
                int tubeCounter = 1;
                foreach (ReformerTube tube in CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList().OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber))
                {
                    CurrentTube = tube;
                    string fileName = Path.Combine(UsefulDirectories.ThreeDImage.FullName, string.Format("{0}.jpg", CurrentTube.Name));
                    ExportScreenshot(fileName);
                    OnProgressChanged((int)Math.Round(tubeCounter / (double)CurrentInspection.Tubes.Count * 100), string.Format("Exported: {0}", tube.Name));
                    tubeCounter++;
                }
                OnProgressChanged(100, string.Format("Successfully written to: {0}", UsefulDirectories.ThreeDImage.FullName));
            }
            catch (Exception ex)
            {
                Log.Error("Failed Exporting all items from 3D", ex);
                OnProgressChanged(100, string.Format("Failed to export all items due to  the following error message: {0}", ex.Message));
            }
            
        }

        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired) { BeginInvoke(new Action(UpdateDisplayUnits)); return; }
            DrawTube();
        }

        private void RotationChanged(object sender, EventArgs e) { SetTubeRotation(); GraphicsWindow.Render(); }

        public void AttachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged += HandleColorBarChangedEvent;
        }

        public void DetachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged -= HandleColorBarChangedEvent;
        }

        public void HandleColorBarChangedEvent(object Sender, ColorScaleChangedEventArgs Args)
        {
            if (GraphicsWindow.ColorScaleBar.ActiveScalarName != Args.TheScalarRange.ScalarName) return;
            //double the color bar values on this local version as we're dealing with diameter now.
            GraphicsWindow.ColorScaleBar.SetRange(Args.TheScalarRange);
            GraphicsWindow.ColorScaleBar.MinMaxRanges.ForEach(scalarRange => { scalarRange.Min = scalarRange.Min; scalarRange.Max = scalarRange.Max; });

            GraphicsWindow.ColorScaleBar.Build();
            GraphicsWindow.ColorScaleBar.Draw(GraphicsWindow.Renderer);
            GraphicsWindow.RenderWindow.Render();
        }

        #endregion Event Handlers
        
        #region Drawing Methods

        private void DrawTube()
        {
            if (CurrentTubeActor != null)
            {
                CurrentTubeActor.ReleaseGraphicsResources(GraphicsWindow.RenderWindow);
                GraphicsWindow.Renderer.RemoveActor(CurrentTubeActor);
            }
            if (CurrentTube == null | SelectedScalarKey == null | _changingTubes) return;

            StatisticalMeasures stats;
            vtkActor actor;
            if (uiStatMeasures.EditValue != null && Enum.TryParse(uiStatMeasures.EditValue.ToString(), out stats))
                actor = VTKTubeCreatorBase.Get3DTube(CurrentTube, SelectedScalarKey, stats, LevelOfDetail, MinRadialPoints);
            else
                actor = VTKTubeCreatorBase.Get3DTube(CurrentTube, SelectedScalarKey, LevelOfDetail, MinRadialPoints);
             
            SetColorScale(actor);
            SetTubeRotation();
            GraphicsWindow.Renderer.AddActor(actor);
            int partitions;
            var validTubes = CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList();
            Range<double> axesRange = DefaultValues.GetAxesRange(validTubes, out partitions);
            double maxRangeInInches = DisplayUnits.Instance.AxialDistanceUnits.Convert(axesRange.Max, Length.LengthScale.Inches);
            ForceShape(actor, maxRangeInInches);
            var tubeBounds = actor.GetBounds();
            SetCamera(maxRangeInInches, tubeBounds[3] - tubeBounds[2]);
            SetTubeTitle();
            CurrentTubeActor = actor;
            SetAxes(actor, axesRange.Max, partitions);
            if (Vertical)
                GraphicsWindow.Renderer.GetActiveCamera().Elevation(10);
            else
                GraphicsWindow.Renderer.GetActiveCamera().Azimuth(-10);
            GraphicsWindow.Render();
        }

        private void SetTubeTitle()
        {
            //Set up the Header Text
            double xMid = GraphicsWindow.Left + GraphicsWindow.Width / 2;
            double yPos = GraphicsWindow.Bottom - 30;
            _headerActor.SetPosition(xMid, yPos);
            _headerActor.SetInput(CurrentTube.GetHeaderText(UseRowLetters));
        }

        private void SetAxes(vtkActor Actor, double maxRange, int partitions)
        {
            //Adjust the axes length to make a nice, rounded ending number at the end of the tube's drawn length
            

            double[] actorBounds = Actor.GetBounds();
            double[] axesBounds = actorBounds;
            axesBounds[0] = 0;
            axesBounds[1] = DisplayUnits.Instance.AxialDistanceUnits.Convert(maxRange, Length.LengthScale.Inches);
            axesBounds[4] -= 5;

            GraphicsWindow.SetAxisVisible(true);
            GraphicsWindow.DrawXAxes(axesBounds, 0, maxRange, partitions + 1);

            string title = DefaultValues.AxialUnitSymbol;
            _axisTitleActor.SetInput(string.Format("Inspected Tube Length ({0})", title));
            if (Vertical)
            {
                _axisTitleActor.SetPosition(.25, .45);
                _axisTitleActor.SetOrientation(90);
            }
            else
            {
                _axisTitleActor.SetPosition(.5, .25);
                _axisTitleActor.SetOrientation(0);
            }
            
        }

        private void SetTubeRotation() 
        {
            if (CurrentTubeActor == null) return; 
            CurrentTubeActor.RotateX(uiTubeRotation.EditValue.ToDouble() - _currentRotation);
            _currentRotation = uiTubeRotation.EditValue.ToDouble();
        }

        private void ForceShape(vtkActor Actor, double maxTubeLengthInInches)
        {
            double newWidth = 1 / (LengthOverThickness / maxTubeLengthInInches) / 4;
            Actor.SetScale(1, newWidth, newWidth);
        }

        private void SetCamera(double maxAxisRange, double maxTubeWidthInWorldCoords)
        {
            //GraphicsWindow.Renderer.ResetCamera();
           
            vtkCamera theCamera = GraphicsWindow.Camera;
            var parallelScale = theCamera.GetParallelScale();
            var parScaleModifier = Vertical ? .12: .25;
            theCamera.SetParallelScale((maxAxisRange / LengthOverThickness) / parScaleModifier);
            theCamera.SetFocalPoint(maxAxisRange / 2, 0, 0);
            theCamera.SetPosition(maxAxisRange / 2, -2000, 0); //y value isn't used due to parallel projection, just need it to give a side of the tube.
            theCamera.SetClippingRange(.1, 10000);
            theCamera.SetViewUp(0, 0, 1);

            if (Vertical)
                theCamera.SetViewUp(-1, 0, 0);
              
            //theCamera.Zoom(Vertical ? 1.2 : 1.4);
        }

        private void SetColorScale(vtkActor Actor)
        {
            GraphicsWindow.ColorScaleBar.Orientation = Vertical ? ColorScaleBar.ColorScaleBarOrientation.Vertical : ColorScaleBar.ColorScaleBarOrientation.Horizontal;
            GraphicsWindow.ColorScaleBar.Width = Vertical ? .15 : .9;
            GraphicsWindow.ColorScaleBar.Height = Vertical ? .9 : .15;
            GraphicsWindow.ColorScaleBar.Position = Vertical ? new[] { .68, .11 } : new[] { .05, .05 };
            GraphicsWindow.ColorScaleBar.Title = string.Format("{0} ({1})", "Radius", DefaultValues.AxialUnitSymbol);
            GraphicsWindow.SetScalarOnColorScaleBar(SelectedScalarKey);
            GraphicsWindow.SetDefaultActorProperties(Actor, GraphicsWindow.ColorScaleBar);
        }

        public void ExportScreenshot(string FullFileName)
        {
            vtkWindowToImageFilter imageFilter = new vtkWindowToImageFilter();
            imageFilter.SetInput(GraphicsWindow.RenderWindow);
            imageFilter.Update();
            vtkJPEGWriter fileWriter = new vtkJPEGWriter();
            fileWriter.SetFileName(FullFileName);
            fileWriter.SetInputConnection(imageFilter.GetOutputPort());
            fileWriter.Write();
        }

        #endregion Drawing Methods

        
    }
}
