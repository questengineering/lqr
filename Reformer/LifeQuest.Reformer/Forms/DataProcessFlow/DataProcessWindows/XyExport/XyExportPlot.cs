﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Nevron;
using Nevron.Chart;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.Features;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;

namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.XyExport
{
    public sealed partial class XyExport : BaseProcessWindow
    {
        
#region Constructor

        public XyExport()
        {
            InitializeComponent();
            UpdateDataSource();
            TheChart.DisplayOnLegend = theChartControl.Legends[0];
            theChartControl.Settings.EnableJittering = false;
        }

#endregion Constructor

        #region Public Properties

        public ReformerTube MainTube
        {
            get { return uiTube.EditValue as ReformerTube; }
            set
            {
                CurrentInspection = value == null? null : value.Inspection;
                uiTube.EditValue = value;
            }
        }

        public ReformerTube RefTube
        {
            get
            {
                if (MainTube == null) return null;
                if (ComparisonMethod == InspectionComparison.Level1) return MainTube.ReformerAttributes.PreviousTube;
                if (ComparisonMethod == InspectionComparison.Overall) return MainTube.ReformerAttributes.OldestOrBaselineTube;
                return null;
            }
        }

        private ODorID DataToShow
        {
            get
            {
                if (MainTube != null) return MainTube.Inspection.InspectionTool == Tool.MANTIS ? ODorID.OD : ODorID.ID;
                return ODorID.OD;
            }
        }

        private bool UseSmoothing { get { return chkSmoothData.Checked; } }
        private int SmoothingValues { get { return uiPointSmoothing.Value.ToInt32(); } }
        private NChart TheChart { get { return theChartControl.Charts[0]; } }
        private bool ManualYAxisControl { get { return chkManualYAxis.Checked; } }
        private ReformerScalar ScalarKey { get { return DataToShow == ODorID.ID ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside; } }

        private InspectionComparison ComparisonMethod
        {
            get
            {
                if (CurrentProject.InspectionFiles.Count < 2) return InspectionComparison.None;
                if (uiComparisonMethod.SelectedIndex == 0) return InspectionComparison.Level1;
                if (uiComparisonMethod.SelectedIndex == 1) return InspectionComparison.Overall;
                return InspectionComparison.None;
            }
        }

        /// <summary>  Gathers and returns the Tube Diameter Data.  </summary>
        public List<float> GetTubeData(ReformerTube tube)
        {
            List<float> theRawValues = tube.Get2DScalarStatisticalData(ScalarKey, tube.Inspection.DataMeasure).First().Value;
            List<float> diamValues = (from float scalar in theRawValues select scalar * 2).ToList(); //double it for diameter
            if (!UseSmoothing || SmoothingValues > diamValues.Count || SmoothingValues <= 0) return diamValues;

            //Smooth the data in the forward direction.
            List<float> smoothedValues = new List<float>(diamValues);
            for (int i = 0; i < diamValues.Count - SmoothingValues; i++ )
            { smoothedValues[i] = smoothedValues.GetRange(i, SmoothingValues).Sum() / SmoothingValues; }
            
            //Smooth the data in the backward direction.
            for (int i = diamValues.Count - 1; i > SmoothingValues; i--)
            { smoothedValues[i] = smoothedValues.GetRange(i - SmoothingValues, SmoothingValues).Sum() / SmoothingValues; }

            return smoothedValues;
        }

        #endregion Public Properties

        #region Private/Protected Methods

        #region Setup/Formatting Methods
        
        private void FormatNewLine(ref NLineSeries TheLine, Color TheColor, string LineName)
        {
            TheLine.DataLabelStyle.Visible = false;
            TheLine.MarkerStyle.Visible = false;
            TheLine.BorderStyle.Width = new NLength(1, NGraphicsUnit.Pixel);
            TheLine.Name = LineName;
            TheLine.UseXValues = true;
            TheLine.Legend.Mode = SeriesLegendMode.Series;
            TheLine.BorderStyle.Color = TheColor;
        }

        private void UpdateLabels()
        {
            for (int i = 0; i < theChartControl.Labels.Count; i++)
            {
                NLabel label = theChartControl.Labels[i];
                switch (label.Name)
                {
                    case "txtMinDiam":
                        if (DataToShow == ODorID.ID)
                        {
                            label.Text = ComparisonMethod == InspectionComparison.Level1 
                                ? MainTube.ReformerAttributes.PickedLevel1RefIDDisplayUnits.ToString("0.000") 
                                : MainTube.ReformerAttributes.PickedOverallRefIDDisplayUnits.ToString("0.000");
                        }
                        else
                        {
                            label.Text = ComparisonMethod == InspectionComparison.Level1
                                ? MainTube.ReformerAttributes.PickedLevel1RefODDisplayUnits.ToString("0.000")
                                : MainTube.ReformerAttributes.PickedOverallRefODDisplayUnits.ToString("0.000");
                        }
                        break;
                    case "txtMaxDiam":
                        if (DataToShow == ODorID.ID)
                        {
                            label.Text = ComparisonMethod == InspectionComparison.Level1 
                                ? MainTube.ReformerAttributes.PickedLevel1MaxIDDisplayUnits.ToString("0.000")
                                : MainTube.ReformerAttributes.PickedOverallMaxIDDisplayUnits.ToString("0.000");
                        }
                        else
                        {
                            label.Text = ComparisonMethod == InspectionComparison.Level1
                                ? MainTube.ReformerAttributes.PickedLevel1MaxODDisplayUnits.ToString("0.000")
                                : MainTube.ReformerAttributes.PickedOverallMaxODDisplayUnits.ToString("0.000");
                        }
                        break;
                    case "lblIDGrowth":
                        if (CurrentInspection.ReformerInfo.NewTubes || MainTube.ReformerAttributes.OverallIDGrowth.IsNaN()) //hide ID% growth for baseline inspections or invalid data.
                            label.Visible = false;
                        else if (CurrentInspection.InspectionTool == Tool.LOTIS)
                            label.Text = "Measured     % ID Growth";
                        else if (CurrentInspection.InspectionTool == Tool.MANTIS)
                            label.Text = "Calculated   % ID Growth";
                        break;
                    case "txtIDGrowth":
                        if (CurrentInspection.ReformerInfo.NewTubes || MainTube.ReformerAttributes.OverallIDGrowth.IsNaN()) //hide ID% growth for baseline inspections or invalid data.
                            label.Visible = false;
                        label.Text = (MainTube.ReformerAttributes.OverallIDGrowth / 100).ToString("p2");
                        break;
                    case "HeaderClientName":
                        if (!string.IsNullOrEmpty(CurrentInspection.ReformerInfo.Refinery))
                            label.Text = CurrentInspection.ReformerInfo.Refinery;
                        break;
                    case "HeaderReformerName":
                        if (!string.IsNullOrEmpty(CurrentInspection.ReformerInfo.ReformerName))
                            label.Text = CurrentInspection.ReformerInfo.ReformerName;
                        break;
                    case "HeaderTubeName":
                        label.Text = MainTube.GetHeaderText(CurrentInspection.ReformerInfo.UseRowLetters);
                        break;
                }
            }
            theChartControl.Refresh();
        }

        private void UpdateChartView()
        {
            NAxis xAxis = TheChart.Axis(StandardAxis.PrimaryX);
            NLinearScaleConfigurator xScale = ((NLinearScaleConfigurator) xAxis.ScaleConfigurator);
            NAxis yAxis = TheChart.Axis(StandardAxis.PrimaryY);
            NLinearScaleConfigurator yScale = ((NLinearScaleConfigurator) yAxis.ScaleConfigurator);
            if (chkVertical.Checked)
            {
                TheChart.SetPredefinedChartStyle(PredefinedChartStyle.HorizontalLeft);
                xScale.Title.Angle = new NScaleLabelAngle(90);
                yScale.Title.Angle = new NScaleLabelAngle(0);
                if (theChartControl.Dock == DockStyle.None) theChartControl.Size = new Size(659, 1000);
            }
            else
            {
                TheChart.SetPredefinedChartStyle(PredefinedChartStyle.Vertical);
                xScale.Title.Angle = new NScaleLabelAngle(0);
                yScale.Title.Angle = new NScaleLabelAngle(90);
                if (theChartControl.Dock == DockStyle.None) theChartControl.Size = new Size(1000, 659);
            }

            if (ManualYAxisControl)
            {
                double minY = uiYCenter.EditValue.ToDouble() - uiYSpread.EditValue.ToDouble() / 2;
                double maxY = uiYCenter.EditValue.ToDouble() + uiYSpread.EditValue.ToDouble() / 2;
                double minYBaseUnit = Length.Convert(minY, CurrentInspection.Units.LengthUnitScale, DisplayUnits.Instance.MeasurementUnits.Scale);
                double maxYBaseUnit = Length.Convert(maxY, CurrentInspection.Units.LengthUnitScale, DisplayUnits.Instance.MeasurementUnits.Scale);
                yAxis.View = new NRangeAxisView(new NRange1DD(minYBaseUnit, maxYBaseUnit));
            }
            else if (MainTube != null) //Auto Scale it if not in manual mode.
            {
                //Add on to the beginning and the designed tube length
                int equalPartitions;
                var range = DefaultValues.GetAxesRange(MainTube.Inspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(), out equalPartitions);
                double maxRange = Math.Ceiling(DisplayUnits.Instance.MeasurementUnits.Convert(range.Max, Length.LengthScale.Inches));
                xAxis.View = new NRangeAxisView(new NRange1DD(0, maxRange)); //Always start the XY Plot at 0
                xScale.MajorTickMode = MajorTickMode.CustomStep;
                xScale.CustomStep = DisplayUnits.Instance.AxialDistanceUnits.Scale == Length.LengthScale.Millimeters ? 1000 / 25.4 : 50;

                //Account for the padding added at the beginning.
                double autoCenterY = DataToShow == ODorID.OD ? MainTube.Specs.DiameterOutside : MainTube.Specs.DiameterInside;double autoCenterYDisplayUnits = autoCenterY;
                double minY = autoCenterYDisplayUnits - .25;
                double maxY = autoCenterYDisplayUnits + .25;

                yAxis.View = new NRangeAxisView(new NRange1DD(minY, maxY));
            }
            theChartControl.RecalcLayout();
            theChartControl.Refresh();
        }

        #endregion Setup/Formatting Methods

        #region Drawing/DataGathering Methods
        private void ReDrawTubeData()
        {
            TheChart.Series.Clear();
            if (MainTube != null)
            {
                UpdateLabels();
                UpdateDisplayUnits();
                GatherNewPlotData();
                PlotWelds();
                PlotMinAndMaxPoints();
                GatherConstLines();
            }
            UpdateChartView();
            
        }

        private void GatherConstLines()
        {
            TheChart.Axis(StandardAxis.PrimaryY).ConstLines.Clear();
            if (MainTube == null) return;
            double converstionFactor = Length.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale, MainTube.Inspection.Units.LengthUnitScale);
            
            double minODValue = MainTube.Specs.DiameterOutside + MainTube.Specs.ODTolDown;
            double maxODValue = MainTube.Specs.DiameterOutside + MainTube.Specs.ODTolUp;
            double minIDValue = MainTube.Specs.DiameterInside + MainTube.Specs.IDTolDown;
            double maxIDValue = MainTube.Specs.DiameterInside + MainTube.Specs.IDTolUp;

            double minValueToUse = DataToShow == ODorID.ID ? minIDValue : minODValue;
            NAxisConstLine newMinLine = new NAxisConstLine
            {
                StrokeStyle = {Pattern  = LinePattern.Dash, Width = new NLength(1), Color = Color.Green},
                FillStyle = new NColorFillStyle(new NArgbColor(125, Color.Green)),
                TextAlignment = chkVertical.Checked ?ContentAlignment.TopRight : ContentAlignment.BottomRight,
                Text = "Min Threshold: " + (minValueToUse * converstionFactor).ToString(DefaultValues.MeasHashFormat),
                Value = minValueToUse,
            };
            TheChart.Axis(StandardAxis.PrimaryY).ConstLines.Add(newMinLine);

            double maxValueToUse = DataToShow == ODorID.ID ? maxIDValue : maxODValue;
            NAxisConstLine newMaxLine = new NAxisConstLine
            {
                StrokeStyle = { Pattern = LinePattern.Dash, Width = new NLength(1), Color = Color.Red },
                FillStyle = new NColorFillStyle(new NArgbColor(125, Color.Red)),
                TextAlignment = chkVertical.Checked ? ContentAlignment.BottomRight : ContentAlignment.TopRight,
                Text = "Max Threshold: " + (maxValueToUse * converstionFactor).ToString(DefaultValues.MeasHashFormat),
                Value = maxValueToUse,
            };
            TheChart.Axis(StandardAxis.PrimaryY).ConstLines.Add(newMaxLine);
        }

        private void FormatFeatureSeries(NPointSeries FeatureSeries, PointShape shape, Color fillColor, Color borderColor)
        {
            FeatureSeries.Size = new NLength(.01f, NRelativeUnit.ParentPercentage);
            FeatureSeries.DataLabelStyle.Format = "<label>";
            FeatureSeries.Legend.Mode = SeriesLegendMode.None;
            
            //Set the point shape.
            NMarkerStyle featureStyle = new NMarkerStyle
            {
                PointShape = shape,
                Height = new NLength(5),
                Width = new NLength(5),
                Visible = true,
                BorderStyle = { Color = borderColor },
                FillStyle = new NColorFillStyle(fillColor)
            };
            FeatureSeries.MarkerStyle = featureStyle;
            TheChart.Series.Add(FeatureSeries);
        }

        private void PlotMinAndMaxPoints()
        {
            //this is a little ghetto because it was wrenched in at the last minute by request.
            List<double> xPositions = new List<double>();
            List<double> yValues = new List<double>();
            List<string> labels = new List<string> { "Ref", "Max" };

            NLineSeries mainSeries = null, refSeries = null;
            foreach (var s in from object s in TheChart.Series where s is NLineSeries && ((NLineSeries)s).Tag == MainTube select s)
                mainSeries = (NLineSeries)s;
            foreach (var s in from object s in TheChart.Series where s is NLineSeries && ((NLineSeries)s).Tag == RefTube select s)
                refSeries = (NLineSeries)s;
            if (mainSeries == null) return;

            ReformerFeatureInfo maxFeature;
            if (ComparisonMethod == InspectionComparison.Level1 && MainTube.ReformerAttributes.IsValidPickedLevel1RefSlice)
            {
                if (refSeries != null)
                {
                    xPositions.Add((double)refSeries.XValues[MainTube.ReformerAttributes.PickedLevel1GrowthRefSlice]);
                    yValues.Add((double)refSeries.Values[MainTube.ReformerAttributes.PickedLevel1GrowthRefSlice]);
                }       
                maxFeature = MainTube.Features.First(F => F.Type == ReformerFeatureType.Level1GrowthMax);
                xPositions.Add(maxFeature.StartPosition);
                yValues.Add((double)mainSeries.Values[maxFeature.SliceIndexStart]);
            }
            else if (ComparisonMethod == InspectionComparison.Overall && MainTube.ReformerAttributes.IsValidPickedOverallRefSlice)
            {
                if (refSeries != null)
                {
                    xPositions.Add((double)refSeries.XValues[MainTube.ReformerAttributes.PickedOverallGrowthRefSlice]);
                    yValues.Add((double)refSeries.Values[MainTube.ReformerAttributes.PickedOverallGrowthRefSlice]);
                }
                maxFeature = MainTube.Features.First(F => F.Type == ReformerFeatureType.OverallGrowthMax);
                xPositions.Add(maxFeature.StartPosition);
                yValues.Add((double)mainSeries.Values[maxFeature.SliceIndexStart]);
            }
            else if (ComparisonMethod == InspectionComparison.None && MainTube.ReformerAttributes.IsValidPickedOverallRefSlice)
            {
                //Only plot the ref point if it coincides with this inspection. Otherwise there will be no ref point visible.
                if (MainTube.ReformerAttributes.OldestOrBaselineTube == MainTube)
                {
                    xPositions.Add((double)mainSeries.XValues[MainTube.ReformerAttributes.PickedOverallGrowthRefSlice]);
                    yValues.Add((double)mainSeries.Values[MainTube.ReformerAttributes.PickedOverallGrowthRefSlice]);
                }
                else
                {
                    xPositions.Add(double.MinValue);
                    yValues.Add(double.MinValue);
                }
                maxFeature = MainTube.Features.First(F => F.Type == ReformerFeatureType.OverallGrowthMax);
                xPositions.Add(maxFeature.StartPosition);
                yValues.Add((double)mainSeries.Values[maxFeature.SliceIndexStart]);
            }

            //Create the points object that will represent this feature and add it to the chart.
            NPointSeries newSeries = new NPointSeries
            {
                UseXValues = true,
                Size = new NLength(1f, NRelativeUnit.ParentPercentage),
                DataLabelStyle = { Format = "<label>" },
                Legend = { Mode = SeriesLegendMode.None }
            };
            newSeries.XValues.AddRange(xPositions);
            newSeries.Values.AddRange(yValues);
            newSeries.Labels.AddRange(labels);

            FormatFeatureSeries(newSeries, PointShape.Cylinder, Color.Black, Color.Black);
            newSeries.DataLabelStyle.VertAlign = VertAlign.Top;
        }

        private void PlotWelds()
        {
            NPointSeries newWeldSeries = MainTube.GetFeaturePointSeries(ScalarKey, new[] { ReformerFeatureType.Weld });
            //Modify the points to hit 'smoothed' data.
            newWeldSeries.ClearDataPoints();
            //Find the smoothed series in the chart.
            NLineSeries smoothedSeries = null;
            foreach (var s in from object s in TheChart.Series where s is NLineSeries && ((NLineSeries)s).Tag == MainTube select s)
            {
                smoothedSeries = (NLineSeries)s;
            }
            if (smoothedSeries == null) return;

            //Add data points from the smoothed series instead.
            foreach (ReformerFeatureInfo f in MainTube.Features.Where(F => F.Type == ReformerFeatureType.Weld))
            {
                newWeldSeries.AddDataPoint(new NDataPoint((double)smoothedSeries.XValues[f.SliceIndexStart], (double)smoothedSeries.Values[f.SliceIndexStart], f.Label));
            }

            FormatFeatureSeries(newWeldSeries, PointShape.LineDiagonalCross, Color.Gray, Color.Gray); //changed from PointShape.Cross so as to not obscure the line as much
            newWeldSeries.DataLabelStyle.VertAlign = VertAlign.Bottom;

            newWeldSeries.DataLabelStyle.ArrowLength = new NLength(4f, NRelativeUnit.ParentPercentage);
            newWeldSeries.DataLabelStyle.ArrowStrokeStyle.Color = BackColor;
        }

        private void GatherNewPlotData()
        {
            AddLine(MainTube, Color.Black);
            if (ComparisonMethod == InspectionComparison.Level1 && MainTube != null && MainTube.ReformerAttributes.PreviousTube != null)
                AddLine(MainTube.ReformerAttributes.PreviousTube, Color.LightSeaGreen);
            else if (ComparisonMethod == InspectionComparison.Overall && MainTube != null && MainTube.ReformerAttributes.OldestOrBaselineTube != null)
                AddLine(MainTube.ReformerAttributes.OldestOrBaselineTube, Color.LightSeaGreen);
        }

        private void AddLine( ReformerTube TheTube, Color TheColor)
        {
            NLineSeries theLine = new NLineSeries();
            List<float> pos;
            TheTube.GetPositions(out pos);
            theLine.XValues.AddRange(pos);
            theLine.Values.AddRange(GetTubeData(TheTube));
            theLine.Tag = TheTube;
            string lineName = string.Format("Mean Diameter - {0}/{1}", TheTube.Inspection.DateInspected.Month, TheTube.Inspection.DateInspected.Year);
            FormatNewLine(ref theLine, TheColor, lineName);
            TheChart.Series.Add(theLine);
        }

        #endregion Drawing/DataGathering Methods

        private void ExportScreenShot(bool ShowInputDialog)
        {
            string defaultName = "XYPlot.jpg";
            if (MainTube != null) defaultName = MainTube.Name + ".jpg";
            UpdateChartView();
            if (ShowInputDialog)
            {
                SaveFileDialog theSaver = new SaveFileDialog
                {
                    DefaultExt = ".jpg",
                    Title = @"Save an Image File",
                    Filter = @"jpg Image|*.jpg",
                    InitialDirectory = UsefulDirectories.DiameterGraphs.FullName,
                    FileName = defaultName
                };
                if (theSaver.ShowDialog(this) == DialogResult.OK)
                    theChartControl.ImageExporter.SaveToFile(theSaver.FileName, new NJpegImageFormat());
            }
            else
            {
                FileInfo savePath = new FileInfo(string.Format("{0}\\{1}", UsefulDirectories.DiameterGraphs.FullName,defaultName));
                theChartControl.ImageExporter.SaveToFile(savePath.FullName, new NJpegImageFormat());
            }
        }

        private void ExportAll()
        {
            try
            {
                OnProgressChanged(0, "Exporting...");
                Cursor = Cursors.WaitCursor;
                int tubeCounter = 1;
                theChartControl.Dock = DockStyle.None;
                foreach (ReformerTube tube in CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList().OrderBy(T => T.ReformerAttributes.RowNumber).ThenBy(T => T.ReformerAttributes.TubeNumber))
                {
                    MainTube = tube;
                    ExportScreenShot(false);
                    OnProgressChanged((int)Math.Round(tubeCounter++ / (double)CurrentInspection.Tubes.Count * 100d), "Drawing: " + tube.Name);
                }
                theChartControl.Dock = DockStyle.Fill;
            }
            finally
            {
                OnProgressChanged(100, string.Format("Finished exporting all XY plots for {0} tubes.", CurrentInspection.Tubes.Count));
                Cursor = Cursors.Default;
            }
        }

        #endregion Private/Protected Methods


        #region Event Handlers

        private void TriggerRedraw(object sender, EventArgs e) { ReDrawTubeData(); }

        private void ChartExtentsChanged(object sender, EventArgs e) { UpdateChartView(); }

        private void bbiExportOne_ItemClick(object sender, EventArgs e)
        {
            theChartControl.Dock = DockStyle.None; 
            ExportScreenShot(true);
            theChartControl.Dock = DockStyle.Fill;
        }

        private void btnExportAll_ItemClick(object sender, EventArgs e)
        {
            if (CurrentInspection == null)
            {
                ReformerWindowManager.Instance.Main.ShowMessage("A Main Inspection must be selected to export.");
                return;
            }
            ExportAll();
        }
        private void chkManualYAxis_CheckedChanged(object sender, EventArgs e)
        {
            uiYCenter.Enabled = chkManualYAxis.Checked;
            uiYSpread.Enabled = chkManualYAxis.Checked;
        }

        #endregion Event Handlers

        public override void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDataSource));
                return;
            }
            CurrentInspection = LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;
            BSReformerTube.DataSource = CurrentInspection == null ? null : CurrentInspection.Tubes.Where(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)).ToList(); //There may be tubes with eddy current data only, so check for axial data.
            if (CurrentProject.InspectionFiles.Count > 1) uiComparisonMethod.Enabled = true;
        }

        public override void UpdateDisplayUnits()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(UpdateDisplayUnits));
                return;
            }
            if (MainTube == null) return;
            //X Axis Units and Label
            TheChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator.Title.Text = string.Format("Inspected Tube Length ({0})", DefaultValues.AxialUnitSymbol);
            double xScaleFactor = Length.Convert(1.0, DisplayUnits.Instance.AxialDistanceUnits.Scale, MainTube.Inspection.Units.LengthUnitSystem.Scale);
            ((NLinearScaleConfigurator)TheChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator).LabelStyle.ValueScale = xScaleFactor;
            ((NLinearScaleConfigurator)TheChart.Axis(StandardAxis.PrimaryX).ScaleConfigurator).LabelValueFormatter.FormatSpecifier = "0";

            //Y Axis Units and Label
            string dataText = DataToShow == ODorID.ID ? "Internal" : "External";
            TheChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator.Title.Text = string.Format("Tube {0} Diameter ({1})", dataText, DefaultValues.AxialUnitSymbol);
            double yScaleFactor = Length.Convert(1.0, DisplayUnits.Instance.MeasurementUnits.Scale, MainTube.Inspection.Units.LengthUnitSystem.Scale);
            ((NLinearScaleConfigurator)TheChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).LabelStyle.ValueScale = yScaleFactor;
            ((NLinearScaleConfigurator)TheChart.Axis(StandardAxis.PrimaryY).ScaleConfigurator).LabelValueFormatter.FormatSpecifier = DefaultValues.MeasHashFormat;
            theChartControl.Refresh();
        }

        private enum InspectionComparison
        {
            None,
            Level1,
            Overall
        }

        private void uiComparisonMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReDrawTubeData();
        }

        private void uiPointSmoothing_EditValueChanged(object sender, EventArgs e)
        {
            ReDrawTubeData();
        }

        private void chkSmoothData_CheckedChanged(object sender, EventArgs e)
        {
            ReDrawTubeData();
        }
    }
}
