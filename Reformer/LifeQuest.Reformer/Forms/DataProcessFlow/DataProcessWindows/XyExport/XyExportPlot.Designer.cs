﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows.XyExport
{
    sealed partial class XyExport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XyExport));
            this.theChartControl = new Nevron.Chart.WinForm.NChartControl();
            this.LCOptions = new DevExpress.XtraLayout.LayoutControl();
            this.uiComparisonMethod = new DevExpress.XtraEditors.RadioGroup();
            this.uiPointSmoothing = new DevExpress.XtraEditors.SpinEdit();
            this.chkSmoothData = new DevExpress.XtraEditors.CheckEdit();
            this.uiTube = new DevExpress.XtraEditors.GridLookUpEdit();
            this.BSReformerTube = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTubeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowNum1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNum1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnExportAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportOne = new DevExpress.XtraEditors.SimpleButton();
            this.uiYSpread = new DevExpress.XtraEditors.CalcEdit();
            this.uiYCenter = new DevExpress.XtraEditors.CalcEdit();
            this.chkVertical = new DevExpress.XtraEditors.CheckEdit();
            this.chkManualYAxis = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgOptions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciYCenter = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciYSpread = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTube = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSmoothPoints = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciComparisonMethod = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.LCOptions)).BeginInit();
            this.LCOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiComparisonMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPointSmoothing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSmoothData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiYSpread.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiYCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVertical.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManualYAxis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciYCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciYSpread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSmoothPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciComparisonMethod)).BeginInit();
            this.SuspendLayout();
            // 
            // theChartControl
            // 
            this.theChartControl.AutoRefresh = false;
            this.theChartControl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.theChartControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.theChartControl.InputKeys = new System.Windows.Forms.Keys[0];
            this.theChartControl.Location = new System.Drawing.Point(193, 0);
            this.theChartControl.Name = "theChartControl";
            this.theChartControl.Size = new System.Drawing.Size(807, 395);
            this.theChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("theChartControl.State")));
            this.theChartControl.TabIndex = 0;
            this.theChartControl.Text = "nChartControl1";
            // 
            // LCOptions
            // 
            this.LCOptions.Controls.Add(this.uiComparisonMethod);
            this.LCOptions.Controls.Add(this.uiPointSmoothing);
            this.LCOptions.Controls.Add(this.chkSmoothData);
            this.LCOptions.Controls.Add(this.uiTube);
            this.LCOptions.Controls.Add(this.btnExportAll);
            this.LCOptions.Controls.Add(this.btnExportOne);
            this.LCOptions.Controls.Add(this.uiYSpread);
            this.LCOptions.Controls.Add(this.uiYCenter);
            this.LCOptions.Controls.Add(this.chkVertical);
            this.LCOptions.Controls.Add(this.chkManualYAxis);
            this.LCOptions.Dock = System.Windows.Forms.DockStyle.Left;
            this.LCOptions.Location = new System.Drawing.Point(0, 0);
            this.LCOptions.Name = "LCOptions";
            this.LCOptions.Root = this.layoutControlGroup1;
            this.LCOptions.Size = new System.Drawing.Size(193, 395);
            this.LCOptions.TabIndex = 5;
            this.LCOptions.Text = "The Layout Control";
            // 
            // uiComparisonMethod
            // 
            this.uiComparisonMethod.Enabled = false;
            this.uiComparisonMethod.Location = new System.Drawing.Point(2, 51);
            this.uiComparisonMethod.Name = "uiComparisonMethod";
            this.uiComparisonMethod.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Show Previous Inspection"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Show Oldest Inspection"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Show Only Current")});
            this.uiComparisonMethod.Size = new System.Drawing.Size(189, 61);
            this.uiComparisonMethod.StyleController = this.LCOptions;
            this.uiComparisonMethod.TabIndex = 17;
            this.uiComparisonMethod.SelectedIndexChanged += new System.EventHandler(this.uiComparisonMethod_SelectedIndexChanged);
            // 
            // uiPointSmoothing
            // 
            this.uiPointSmoothing.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiPointSmoothing.Location = new System.Drawing.Point(91, 27);
            this.uiPointSmoothing.Name = "uiPointSmoothing";
            this.uiPointSmoothing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiPointSmoothing.Properties.IsFloatValue = false;
            this.uiPointSmoothing.Properties.Mask.EditMask = "N00";
            this.uiPointSmoothing.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uiPointSmoothing.Size = new System.Drawing.Size(50, 20);
            this.uiPointSmoothing.StyleController = this.LCOptions;
            this.uiPointSmoothing.TabIndex = 16;
            this.uiPointSmoothing.EditValueChanged += new System.EventHandler(this.uiPointSmoothing_EditValueChanged);
            // 
            // chkSmoothData
            // 
            this.chkSmoothData.EditValue = true;
            this.chkSmoothData.Location = new System.Drawing.Point(2, 27);
            this.chkSmoothData.Name = "chkSmoothData";
            this.chkSmoothData.Properties.Caption = "Smooth Over";
            this.chkSmoothData.Size = new System.Drawing.Size(85, 19);
            this.chkSmoothData.StyleController = this.LCOptions;
            this.chkSmoothData.TabIndex = 15;
            this.chkSmoothData.CheckedChanged += new System.EventHandler(this.chkSmoothData_CheckedChanged);
            // 
            // uiTube
            // 
            this.uiTube.EditValue = "";
            this.uiTube.Location = new System.Drawing.Point(29, 2);
            this.uiTube.Name = "uiTube";
            this.uiTube.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTube.Properties.DataSource = this.BSReformerTube;
            this.uiTube.Properties.NullText = "";
            this.uiTube.Properties.View = this.gridLookUpEdit1View;
            this.uiTube.Size = new System.Drawing.Size(162, 20);
            this.uiTube.StyleController = this.LCOptions;
            this.uiTube.TabIndex = 14;
            this.uiTube.EditValueChanged += new System.EventHandler(this.TriggerRedraw);
            // 
            // BSReformerTube
            // 
            this.BSReformerTube.DataSource = typeof(Reformer.Data.Tubes.ReformerTube);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTubeName,
            this.colRowNum1,
            this.colTubeNum1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colTubeName
            // 
            this.colTubeName.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeName.Caption = "Name";
            this.colTubeName.FieldName = "Name";
            this.colTubeName.Name = "colTubeName";
            this.colTubeName.Visible = true;
            this.colTubeName.VisibleIndex = 0;
            // 
            // colRowNum1
            // 
            this.colRowNum1.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNum1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNum1.AppearanceHeader.Options.UseTextOptions = true;
            this.colRowNum1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNum1.Caption = "Row #";
            this.colRowNum1.FieldName = "ReformerAttributes.RowNumber";
            this.colRowNum1.Name = "colRowNum1";
            this.colRowNum1.Visible = true;
            this.colRowNum1.VisibleIndex = 1;
            // 
            // colTubeNum1
            // 
            this.colTubeNum1.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNum1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNum1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNum1.Caption = "Col #";
            this.colTubeNum1.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNum1.Name = "colTubeNum1";
            this.colTubeNum1.Visible = true;
            this.colTubeNum1.VisibleIndex = 2;
            // 
            // btnExportAll
            // 
            this.btnExportAll.Location = new System.Drawing.Point(98, 225);
            this.btnExportAll.Name = "btnExportAll";
            this.btnExportAll.Size = new System.Drawing.Size(93, 22);
            this.btnExportAll.StyleController = this.LCOptions;
            this.btnExportAll.TabIndex = 13;
            this.btnExportAll.Text = "Export All";
            this.btnExportAll.Click += new System.EventHandler(this.btnExportAll_ItemClick);
            // 
            // btnExportOne
            // 
            this.btnExportOne.Location = new System.Drawing.Point(2, 225);
            this.btnExportOne.Name = "btnExportOne";
            this.btnExportOne.Size = new System.Drawing.Size(92, 22);
            this.btnExportOne.StyleController = this.LCOptions;
            this.btnExportOne.TabIndex = 12;
            this.btnExportOne.Text = "Export One";
            this.btnExportOne.Click += new System.EventHandler(this.bbiExportOne_ItemClick);
            // 
            // uiYSpread
            // 
            this.uiYSpread.Enabled = false;
            this.uiYSpread.Location = new System.Drawing.Point(98, 200);
            this.uiYSpread.Name = "uiYSpread";
            this.uiYSpread.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiYSpread.Size = new System.Drawing.Size(92, 20);
            this.uiYSpread.StyleController = this.LCOptions;
            this.uiYSpread.TabIndex = 11;
            this.uiYSpread.EditValueChanged += new System.EventHandler(this.ChartExtentsChanged);
            // 
            // uiYCenter
            // 
            this.uiYCenter.Enabled = false;
            this.uiYCenter.Location = new System.Drawing.Point(3, 200);
            this.uiYCenter.Name = "uiYCenter";
            this.uiYCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiYCenter.Size = new System.Drawing.Size(91, 20);
            this.uiYCenter.StyleController = this.LCOptions;
            this.uiYCenter.TabIndex = 10;
            this.uiYCenter.EditValueChanged += new System.EventHandler(this.ChartExtentsChanged);
            // 
            // chkVertical
            // 
            this.chkVertical.Location = new System.Drawing.Point(3, 136);
            this.chkVertical.Name = "chkVertical";
            this.chkVertical.Properties.Caption = "Vertical Plot";
            this.chkVertical.Size = new System.Drawing.Size(187, 19);
            this.chkVertical.StyleController = this.LCOptions;
            this.chkVertical.TabIndex = 9;
            this.chkVertical.CheckedChanged += new System.EventHandler(this.TriggerRedraw);
            // 
            // chkManualYAxis
            // 
            this.chkManualYAxis.Location = new System.Drawing.Point(3, 159);
            this.chkManualYAxis.Name = "chkManualYAxis";
            this.chkManualYAxis.Properties.Caption = "Manual Y Axis";
            this.chkManualYAxis.Size = new System.Drawing.Size(187, 19);
            this.chkManualYAxis.StyleController = this.LCOptions;
            this.chkManualYAxis.TabIndex = 8;
            this.chkManualYAxis.CheckedChanged += new System.EventHandler(this.chkManualYAxis_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgOptions,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.lciTube,
            this.layoutControlItem1,
            this.lciSmoothPoints,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.lciComparisonMethod});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(193, 395);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lcgOptions
            // 
            this.lcgOptions.CustomizationFormText = "Options";
            this.lcgOptions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.lciYCenter,
            this.lciYSpread});
            this.lcgOptions.Location = new System.Drawing.Point(0, 114);
            this.lcgOptions.Name = "lcgOptions";
            this.lcgOptions.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgOptions.Size = new System.Drawing.Size(193, 109);
            this.lcgOptions.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgOptions.Text = "Options";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.chkManualYAxis;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(191, 23);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chkVertical;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(191, 23);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // lciYCenter
            // 
            this.lciYCenter.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciYCenter.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciYCenter.Control = this.uiYCenter;
            this.lciYCenter.CustomizationFormText = "Y Center";
            this.lciYCenter.Location = new System.Drawing.Point(0, 46);
            this.lciYCenter.Name = "lciYCenter";
            this.lciYCenter.Size = new System.Drawing.Size(95, 42);
            this.lciYCenter.Text = "Y Center";
            this.lciYCenter.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciYCenter.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciYCenter.TextSize = new System.Drawing.Size(42, 13);
            this.lciYCenter.TextToControlDistance = 5;
            // 
            // lciYSpread
            // 
            this.lciYSpread.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lciYSpread.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lciYSpread.Control = this.uiYSpread;
            this.lciYSpread.CustomizationFormText = "Y Spread";
            this.lciYSpread.Location = new System.Drawing.Point(95, 46);
            this.lciYSpread.Name = "lciYSpread";
            this.lciYSpread.Size = new System.Drawing.Size(96, 42);
            this.lciYSpread.Text = "Y Spread";
            this.lciYSpread.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciYSpread.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciYSpread.TextSize = new System.Drawing.Size(43, 13);
            this.lciYSpread.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnExportOne;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 223);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnExportAll;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(96, 223);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(97, 26);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // lciTube
            // 
            this.lciTube.Control = this.uiTube;
            this.lciTube.CustomizationFormText = "Tube";
            this.lciTube.Location = new System.Drawing.Point(0, 0);
            this.lciTube.MaxSize = new System.Drawing.Size(193, 25);
            this.lciTube.MinSize = new System.Drawing.Size(193, 25);
            this.lciTube.Name = "lciTube";
            this.lciTube.Size = new System.Drawing.Size(193, 25);
            this.lciTube.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciTube.Text = "Tube";
            this.lciTube.TextSize = new System.Drawing.Size(24, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkSmoothData;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(89, 24);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciSmoothPoints
            // 
            this.lciSmoothPoints.Control = this.uiPointSmoothing;
            this.lciSmoothPoints.CustomizationFormText = "Points";
            this.lciSmoothPoints.Location = new System.Drawing.Point(89, 25);
            this.lciSmoothPoints.MinSize = new System.Drawing.Size(88, 24);
            this.lciSmoothPoints.Name = "lciSmoothPoints";
            this.lciSmoothPoints.Size = new System.Drawing.Size(88, 24);
            this.lciSmoothPoints.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciSmoothPoints.Text = "Points";
            this.lciSmoothPoints.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciSmoothPoints.TextLocation = DevExpress.Utils.Locations.Right;
            this.lciSmoothPoints.TextSize = new System.Drawing.Size(29, 13);
            this.lciSmoothPoints.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 249);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(193, 146);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(177, 25);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(16, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciComparisonMethod
            // 
            this.lciComparisonMethod.Control = this.uiComparisonMethod;
            this.lciComparisonMethod.CustomizationFormText = "lciComparisonMethod";
            this.lciComparisonMethod.Location = new System.Drawing.Point(0, 49);
            this.lciComparisonMethod.MaxSize = new System.Drawing.Size(193, 65);
            this.lciComparisonMethod.MinSize = new System.Drawing.Size(193, 65);
            this.lciComparisonMethod.Name = "lciComparisonMethod";
            this.lciComparisonMethod.Size = new System.Drawing.Size(193, 65);
            this.lciComparisonMethod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciComparisonMethod.Text = "lciComparisonMethod";
            this.lciComparisonMethod.TextSize = new System.Drawing.Size(0, 0);
            this.lciComparisonMethod.TextVisible = false;
            // 
            // XyExport
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.Controls.Add(this.theChartControl);
            this.Controls.Add(this.LCOptions);
            this.Name = "XyExport";
            this.Size = new System.Drawing.Size(1000, 395);
            ((System.ComponentModel.ISupportInitialize)(this.LCOptions)).EndInit();
            this.LCOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiComparisonMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPointSmoothing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSmoothData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiYSpread.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiYCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVertical.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManualYAxis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciYCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciYSpread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSmoothPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciComparisonMethod)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Nevron.Chart.WinForm.NChartControl theChartControl;
        private System.Windows.Forms.BindingSource BSReformerTube;
        private DevExpress.XtraLayout.LayoutControl LCOptions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgOptions;
        private DevExpress.XtraEditors.CheckEdit chkManualYAxis;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit chkVertical;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CalcEdit uiYSpread;
        private DevExpress.XtraEditors.CalcEdit uiYCenter;
        private DevExpress.XtraLayout.LayoutControlItem lciYCenter;
        private DevExpress.XtraLayout.LayoutControlItem lciYSpread;
        private DevExpress.XtraEditors.SimpleButton btnExportAll;
        private DevExpress.XtraEditors.SimpleButton btnExportOne;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.GridLookUpEdit uiTube;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeName;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNum1;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNum1;
        private DevExpress.XtraLayout.LayoutControlItem lciTube;
        private DevExpress.XtraEditors.SpinEdit uiPointSmoothing;
        private DevExpress.XtraEditors.CheckEdit chkSmoothData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem lciSmoothPoints;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.RadioGroup uiComparisonMethod;
        private DevExpress.XtraLayout.LayoutControlItem lciComparisonMethod;
    }
}
