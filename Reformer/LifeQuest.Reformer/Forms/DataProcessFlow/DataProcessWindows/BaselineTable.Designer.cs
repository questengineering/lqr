﻿namespace Reformer.Forms.DataProcessFlow.DataProcessWindows
{
    sealed partial class BaselineTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnOpenOutputFolder = new DevExpress.XtraEditors.SimpleButton();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.TheGrid = new DevExpress.XtraGrid.GridControl();
            this.BSPipingSizeInfo = new System.Windows.Forms.BindingSource(this.components);
            this.BSReformerInspection = new System.Windows.Forms.BindingSource(this.components);
            this.TheView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRowNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxDiam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelMaxPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinDiam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeldNearestMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectionMin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeldNearestMin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.orderBy = new DevExpress.XtraEditors.LookUpEdit();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSPipingSizeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnOpenOutputFolder);
            this.lcMain.Controls.Add(this.btnExport);
            this.lcMain.Controls.Add(this.TheGrid);
            this.lcMain.Controls.Add(this.orderBy);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Margin = new System.Windows.Forms.Padding(4);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(756, 411, 451, 350);
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(1164, 640);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "layoutControl1";
            // 
            // btnOpenOutputFolder
            // 
            this.btnOpenOutputFolder.Location = new System.Drawing.Point(118, 2);
            this.btnOpenOutputFolder.Name = "btnOpenOutputFolder";
            this.btnOpenOutputFolder.Size = new System.Drawing.Size(144, 23);
            this.btnOpenOutputFolder.StyleController = this.lcMain;
            this.btnOpenOutputFolder.TabIndex = 7;
            this.btnOpenOutputFolder.Text = "Open Output Folder";
            this.btnOpenOutputFolder.Click += new System.EventHandler(this.btnOpenOutputFolder_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(2, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(112, 23);
            this.btnExport.StyleController = this.lcMain;
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // TheGrid
            // 
            this.TheGrid.DataSource = this.BSPipingSizeInfo;
            this.TheGrid.Location = new System.Drawing.Point(2, 29);
            this.TheGrid.MainView = this.TheView;
            this.TheGrid.Name = "TheGrid";
            this.TheGrid.Size = new System.Drawing.Size(1160, 564);
            this.TheGrid.TabIndex = 5;
            this.TheGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TheView});
            // 
            // BSPipingSizeInfo
            // 
            this.BSPipingSizeInfo.DataMember = "Tubes";
            this.BSPipingSizeInfo.DataSource = this.BSReformerInspection;
            // 
            // BSReformerInspection
            // 
            this.BSReformerInspection.AllowNew = true;
            this.BSReformerInspection.DataSource = typeof(Reformer.Data.InspectionFile.ReformerInspection);
            // 
            // TheView
            // 
            this.TheView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRowNumber,
            this.colTubeNumber,
            this.colMaxDiam,
            this.colSection,
            this.colRelMaxPos,
            this.colMinDiam,
            this.colWeldNearestMax,
            this.colSectionMin,
            this.colWeldNearestMin,
            this.colMinPosition,
            this.colOrderIndex});
            this.TheView.GridControl = this.TheGrid;
            this.TheView.Name = "TheView";
            this.TheView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.TheView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.TheView.OptionsBehavior.ReadOnly = true;
            this.TheView.OptionsCustomization.AllowGroup = false;
            this.TheView.OptionsDetail.AllowZoomDetail = false;
            this.TheView.OptionsDetail.EnableMasterViewMode = false;
            this.TheView.OptionsDetail.ShowDetailTabs = false;
            this.TheView.OptionsView.ShowDetailButtons = false;
            this.TheView.OptionsView.ShowGroupPanel = false;
            this.TheView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.TheView_CustomUnboundColumnData);
            // 
            // colRowNumber
            // 
            this.colRowNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colRowNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNumber.Caption = "Row";
            this.colRowNumber.FieldName = "ReformerAttributes.RowNumber";
            this.colRowNumber.Name = "colRowNumber";
            this.colRowNumber.Visible = true;
            this.colRowNumber.VisibleIndex = 0;
            this.colRowNumber.Width = 49;
            // 
            // colTubeNumber
            // 
            this.colTubeNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colTubeNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colTubeNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTubeNumber.Caption = "Tube";
            this.colTubeNumber.FieldName = "ReformerAttributes.TubeNumber";
            this.colTubeNumber.Name = "colTubeNumber";
            this.colTubeNumber.OptionsColumn.AllowEdit = false;
            this.colTubeNumber.Visible = true;
            this.colTubeNumber.VisibleIndex = 1;
            this.colTubeNumber.Width = 50;
            // 
            // colMaxDiam
            // 
            this.colMaxDiam.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxDiam.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxDiam.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxDiam.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxDiam.Caption = "Max Diam";
            this.colMaxDiam.DisplayFormat.FormatString = "N3";
            this.colMaxDiam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxDiam.FieldName = "MaxDiam";
            this.colMaxDiam.Name = "colMaxDiam";
            this.colMaxDiam.OptionsColumn.AllowEdit = false;
            this.colMaxDiam.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colMaxDiam.Visible = true;
            this.colMaxDiam.VisibleIndex = 2;
            this.colMaxDiam.Width = 85;
            // 
            // colSection
            // 
            this.colSection.AppearanceCell.Options.UseTextOptions = true;
            this.colSection.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSection.AppearanceHeader.Options.UseTextOptions = true;
            this.colSection.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSection.Caption = "Section+";
            this.colSection.FieldName = "colSectionMax";
            this.colSection.Name = "colSection";
            this.colSection.OptionsColumn.AllowEdit = false;
            this.colSection.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colSection.Visible = true;
            this.colSection.VisibleIndex = 3;
            this.colSection.Width = 85;
            // 
            // colRelMaxPos
            // 
            this.colRelMaxPos.AppearanceCell.Options.UseTextOptions = true;
            this.colRelMaxPos.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelMaxPos.AppearanceHeader.Options.UseTextOptions = true;
            this.colRelMaxPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRelMaxPos.Caption = "Position+";
            this.colRelMaxPos.DisplayFormat.FormatString = "N3";
            this.colRelMaxPos.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRelMaxPos.FieldName = "colRelMaxPos";
            this.colRelMaxPos.Name = "colRelMaxPos";
            this.colRelMaxPos.OptionsColumn.AllowEdit = false;
            this.colRelMaxPos.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colRelMaxPos.Visible = true;
            this.colRelMaxPos.VisibleIndex = 5;
            this.colRelMaxPos.Width = 85;
            // 
            // colMinDiam
            // 
            this.colMinDiam.AppearanceCell.Options.UseTextOptions = true;
            this.colMinDiam.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinDiam.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinDiam.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinDiam.Caption = "Min Diam";
            this.colMinDiam.DisplayFormat.FormatString = "N3";
            this.colMinDiam.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinDiam.FieldName = "MinDiam";
            this.colMinDiam.Name = "colMinDiam";
            this.colMinDiam.OptionsColumn.AllowEdit = false;
            this.colMinDiam.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colMinDiam.Visible = true;
            this.colMinDiam.VisibleIndex = 6;
            this.colMinDiam.Width = 85;
            // 
            // colWeldNearestMax
            // 
            this.colWeldNearestMax.AppearanceCell.Options.UseTextOptions = true;
            this.colWeldNearestMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeldNearestMax.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeldNearestMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeldNearestMax.Caption = "Weld+";
            this.colWeldNearestMax.FieldName = "colWeldNearestMax";
            this.colWeldNearestMax.Name = "colWeldNearestMax";
            this.colWeldNearestMax.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colWeldNearestMax.Visible = true;
            this.colWeldNearestMax.VisibleIndex = 4;
            // 
            // colSectionMin
            // 
            this.colSectionMin.AppearanceCell.Options.UseTextOptions = true;
            this.colSectionMin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSectionMin.AppearanceHeader.Options.UseTextOptions = true;
            this.colSectionMin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSectionMin.Caption = "Section-";
            this.colSectionMin.FieldName = "colSectionMin";
            this.colSectionMin.Name = "colSectionMin";
            this.colSectionMin.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colSectionMin.Visible = true;
            this.colSectionMin.VisibleIndex = 7;
            // 
            // colWeldNearestMin
            // 
            this.colWeldNearestMin.AppearanceCell.Options.UseTextOptions = true;
            this.colWeldNearestMin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeldNearestMin.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeldNearestMin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeldNearestMin.Caption = "Weld-";
            this.colWeldNearestMin.FieldName = "colWeldNearestMin";
            this.colWeldNearestMin.Name = "colWeldNearestMin";
            this.colWeldNearestMin.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colWeldNearestMin.Visible = true;
            this.colWeldNearestMin.VisibleIndex = 8;
            // 
            // colMinPosition
            // 
            this.colMinPosition.AppearanceCell.Options.UseTextOptions = true;
            this.colMinPosition.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinPosition.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinPosition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinPosition.Caption = "Position-";
            this.colMinPosition.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMinPosition.FieldName = "colMinPosition";
            this.colMinPosition.Name = "colMinPosition";
            this.colMinPosition.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colMinPosition.Visible = true;
            this.colMinPosition.VisibleIndex = 9;
            // 
            // colOrderIndex
            // 
            this.colOrderIndex.FieldName = "OrderIndex";
            this.colOrderIndex.Name = "colOrderIndex";
            // 
            // orderBy
            // 
            this.orderBy.Location = new System.Drawing.Point(933, 2);
            this.orderBy.Name = "orderBy";
            this.orderBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.orderBy.Properties.NullText = "";
            this.orderBy.Properties.PopupSizeable = false;
            this.orderBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.orderBy.Size = new System.Drawing.Size(229, 22);
            this.orderBy.StyleController = this.lcMain;
            this.orderBy.TabIndex = 8;
            this.orderBy.EditValueChanged += new System.EventHandler(this.orderBy_EditValueChanged);
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "layoutControlGroup1";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciGrid,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem3});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "Root";
            this.lcgMain.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMain.Size = new System.Drawing.Size(1164, 640);
            this.lcgMain.Text = "Root";
            this.lcgMain.TextVisible = false;
            // 
            // lciGrid
            // 
            this.lciGrid.Control = this.TheGrid;
            this.lciGrid.CustomizationFormText = "lciGrid";
            this.lciGrid.Location = new System.Drawing.Point(0, 27);
            this.lciGrid.Name = "lciGrid";
            this.lciGrid.Size = new System.Drawing.Size(1164, 568);
            this.lciGrid.Text = "lciGrid";
            this.lciGrid.TextSize = new System.Drawing.Size(0, 0);
            this.lciGrid.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnExport;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(116, 27);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnOpenOutputFolder;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(148, 27);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 595);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1164, 45);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(264, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(571, 27);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.orderBy;
            this.layoutControlItem3.CustomizationFormText = "Order by Text: ";
            this.layoutControlItem3.Location = new System.Drawing.Point(835, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(329, 27);
            this.layoutControlItem3.Text = "Sorted by Text: ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(93, 16);
            // 
            // BaselineTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "BaselineTable";
            this.Size = new System.Drawing.Size(1164, 640);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TheGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSPipingSizeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSReformerInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TheView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraGrid.GridControl TheGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView TheView;
        private DevExpress.XtraLayout.LayoutControlItem lciGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxDiam;
        private DevExpress.XtraGrid.Columns.GridColumn colRelMaxPos;
        private DevExpress.XtraGrid.Columns.GridColumn colMinDiam;
        private DevExpress.XtraGrid.Columns.GridColumn colSection;
        private System.Windows.Forms.BindingSource BSReformerInspection;
        private System.Windows.Forms.BindingSource BSPipingSizeInfo;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNumber;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colWeldNearestMax;
        private DevExpress.XtraGrid.Columns.GridColumn colSectionMin;
        private DevExpress.XtraGrid.Columns.GridColumn colWeldNearestMin;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderIndex;
        private DevExpress.XtraEditors.SimpleButton btnOpenOutputFolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit orderBy;
    }
}
