﻿namespace Reformer.Forms.DataProcessFlow
{
    partial class CentralDataProcessForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            DetachCurrentWindow();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CentralDataProcessForm));
            this.btnHelp = new DevExpress.XtraEditors.SimpleButton();
            this.ProgMessage = new DevExpress.XtraEditors.LabelControl();
            this.ProgBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.btnPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.pnlMainWindow = new DevExpress.XtraEditors.PanelControl();
            this.lblCurrentItemName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ProgBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMainWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.Location = new System.Drawing.Point(1120, 621);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(26, 23);
            this.btnHelp.TabIndex = 5;
            this.btnHelp.ToolTip = "Help: Shows a description of what you should be doing with the current window.";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // ProgMessage
            // 
            this.ProgMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ProgMessage.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ProgMessage.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.ProgMessage.Location = new System.Drawing.Point(87, 605);
            this.ProgMessage.Name = "ProgMessage";
            this.ProgMessage.Size = new System.Drawing.Size(42, 13);
            this.ProgMessage.TabIndex = 4;
            this.ProgMessage.Text = "Progress";
            this.ProgMessage.Visible = false;
            // 
            // ProgBar
            // 
            this.ProgBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ProgBar.Location = new System.Drawing.Point(84, 624);
            this.ProgBar.Name = "ProgBar";
            this.ProgBar.Size = new System.Drawing.Size(230, 18);
            this.ProgBar.TabIndex = 3;
            this.ProgBar.Visible = false;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Image")));
            this.btnPrevious.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnPrevious.Location = new System.Drawing.Point(3, 621);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.Location = new System.Drawing.Point(1152, 621);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.ToolTip = "Next: Moves you to the next step of data processing";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pnlMainWindow
            // 
            this.pnlMainWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMainWindow.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnlMainWindow.Location = new System.Drawing.Point(0, 0);
            this.pnlMainWindow.Name = "pnlMainWindow";
            this.pnlMainWindow.Size = new System.Drawing.Size(1230, 600);
            this.pnlMainWindow.TabIndex = 0;
            // 
            // lblCurrentItemName
            // 
            this.lblCurrentItemName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCurrentItemName.AutoSize = true;
            this.lblCurrentItemName.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentItemName.Location = new System.Drawing.Point(320, 617);
            this.lblCurrentItemName.Name = "lblCurrentItemName";
            this.lblCurrentItemName.Size = new System.Drawing.Size(0, 25);
            this.lblCurrentItemName.TabIndex = 6;
            // 
            // CentralDataProcessForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblCurrentItemName);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.ProgMessage);
            this.Controls.Add(this.ProgBar);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.pnlMainWindow);
            this.FormDockingStyle = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.Name = "CentralDataProcessForm";
            this.Size = new System.Drawing.Size(1230, 647);
            this.TabText = "Central Command";
            ((System.ComponentModel.ISupportInitialize)(this.ProgBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMainWindow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlMainWindow;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraEditors.SimpleButton btnPrevious;
        private DevExpress.XtraEditors.ProgressBarControl ProgBar;
        private DevExpress.XtraEditors.LabelControl ProgMessage;
        private DevExpress.XtraEditors.SimpleButton btnHelp;
        private System.Windows.Forms.Label lblCurrentItemName;
    }
}
