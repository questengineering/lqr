﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using Nevron.Chart;
using Nevron.Chart.WinForm;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Forms.CustomTools
{
    public partial class PartialRadialAdjustment : Form
    {
        #region variables and such

        const string MainDataSide1Name = "MainSide1";
        const string MainDataSide2Name = "MainSide2";
        private ReformerProjectInfo CurrentProject { get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; } }
        private ReformerInspection MainInspection { get { return uiMainInspection.EditValue as ReformerInspection; } }
        private ReformerTube MainTube { get { return uiTube.EditValue as ReformerTube;} }
        
        private IEnumerable<ReformerInspection> RefInspections
        {
            get
            {
                return (from CheckedListBoxItem item in chkRefInspections.CheckedItems select (ReformerInspection) item.Value);
            }
        }

        private IEnumerable<ReformerTube> RefTubes
        {
            get
            {
                return MainTube == null 
                    ? new List<ReformerTube>() 
                    : RefInspections.SelectMany(i => i.Tubes.Where(t => t.Name == MainTube.Name));
            }
        }

        private readonly Dictionary<string, NLineSeries> _tubeLines = new Dictionary<string, NLineSeries>();

        private NChart TheChart { get { return ChartControl.Charts[0]; } }
        private NAxis XAxis { get { return TheChart.Axis(StandardAxis.PrimaryX); } }
        private NAxis YAxis { get { return TheChart.Axis(StandardAxis.PrimaryY); } }
        private NAxis ZAxis { get { return TheChart.Axis(StandardAxis.Depth); } }

        private NAxisConstLine AxialCursor { get { return XAxis.ConstLines[0]; } }
        public NAxisConstLine DraggingLine = null;


        #endregion variables and such

        public PartialRadialAdjustment()
        {
            InitializeComponent();
            SetupUI();
            ChartControl.MouseUp += TheChartControl_MouseUp;
            ChartControl.MouseMove += TheChartControl_MouseMove;
            ChartControl.MouseDown += TheChartControl_MouseDown;
            
        }

        private void SetupUI()
        {
            if (CurrentProject == null || CurrentProject.InspectionFiles.Count == 0) return;
            foreach (ReformerInspection insp in CurrentProject.InspectionFiles)
            {
                string inspName = string.Format("{0} : {1}", insp.Name, insp.DateInspected.Year);
                uiMainInspection.Properties.Items.Add(new ImageComboBoxItem(inspName, insp));
                chkRefInspections.Items.Add(insp, inspName);
            }
            uiMainInspection.EditValue = CurrentProject.InspectionFiles.FirstOrDefault();
            UpdateDisplayUnits();
        }

        private void uiMainInspection_SelectedIndexChanged(object sender, EventArgs e)
        {
            //do not allow the main insepction to be a reference inspection.
            foreach (CheckedListBoxItem item in chkRefInspections.Items)
            {
                if (item.Value == MainInspection)
                {
                    item.Enabled = false;
                    item.CheckState = CheckState.Unchecked;
                }
                else
                    item.Enabled = true;
            }

            //add all the inspection's tubes to the list of tubes
            var currentTube = uiTube.EditValue as ReformerTube;
            string currentTubeName = currentTube != null ? currentTube.Name : "No Previously Selected Tube";
            foreach (var tube in MainInspection.Tubes)
            {
                uiTube.Properties.Items.Add(new ImageComboBoxItem(tube.Name, tube));
            }
            
            //Select the same tube as before if the inspection changed and the same tube exists. Otherwise select the first.
            bool foundTube = false;
            foreach (ImageComboBoxItem item in uiTube.Properties.Items)
            {
                if (item.Description == currentTubeName)
                {
                    uiTube.EditValue = item.Value;
                    foundTube = true;
                }   
            }
            if (!foundTube)
                uiTube.EditValue = uiTube.Properties.Items.FirstOrDefault();
        }

        private void SelectedTubeChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void MainLineDataChanged(object sender, EventArgs e)
        {
            var side1Line = _tubeLines.First(l => l.Key.Contains(MainDataSide1Name));
            var side2Line = _tubeLines.First(l => l.Key.Contains(MainDataSide2Name));
            side1Line.Value.Visible = chkShowSide1.Checked;
            side2Line.Value.Visible = chkShowSide2.Checked;
            ChartControl.Refresh();
        }

        private void Redraw()
        {
            //Clean out the chart and cache.
            _tubeLines.Clear();
            TheChart.Series.Clear();

            if (MainTube == null) return;

            foreach (ReformerTube tube in RefTubes)
            {
                var line = tube.GetDefaultFormattedLine(ReformerScalar.RadiusOutside, StatisticalMeasures.Mean).First();
                line.BorderStyle.Color = Color.Gray;
                _tubeLines.Add(tube.ID.ToString(), line);
            }

            //show different lines for each side of the MANTIS tool
            float[,] mainTubeData;
            MainTube.Get2DScalarData(ReformerScalar.RadiusOutside, out mainTubeData);

            List<float> side1Data = new List<float>();
            List<float> side2Data = new List<float>();
            int numSensors = mainTubeData.GetLength(1);
            for (int slice = 0; slice < mainTubeData.GetLength(0); slice++)
            {
                float side1Total = 0, side2Total = 0;
                for (int sensor = 0; sensor < numSensors; sensor++)
                {
                    float value = mainTubeData[slice, sensor];
                    if (sensor < numSensors / 2)
                        side1Total += value;
                    else side2Total += value;
                }
                side1Data.Add(side1Total / (numSensors / 2f));
                side2Data.Add(side2Total / (numSensors / 2f));
            }
            var side1Line = MainTube.GetDefaultFormattedLine(ReformerScalar.RadiusOutside, StatisticalMeasures.Mean).First();
            var side2Line = MainTube.GetDefaultFormattedLine(ReformerScalar.RadiusOutside, StatisticalMeasures.Mean).First();

            side1Line.Values.Clear();
            side2Line.Values.Clear();

            side1Line.Values.AddRange(side1Data);
            side2Line.Values.AddRange(side2Data);

            side1Line.BorderStyle.Color = Color.Olive;
            side2Line.BorderStyle.Color = Color.Blue;

            if (chkShowSide1.Checked)
                _tubeLines.Add(MainDataSide1Name, side1Line);
            if (chkShowSide2.Checked)
                _tubeLines.Add(MainDataSide2Name, side2Line);

            foreach (var line in _tubeLines.Values)
            {
                TheChart.Series.Add(line);
            }

            float meanValue = side1Data.Median();
            YAxis.View = new NRangeAxisView(new NRange1DD(meanValue - .15, meanValue + .15));
            ChartControl.Refresh();
            uiAxialPosition.Properties.MinValue = 0;
            uiAxialPosition.Properties.MaxValue = MainTube.EndPositionInDisplayUnits.ToDecimal();

            SetAxialValue(MainTube.EndPositionInDisplayUnits / 2);
        }

        private void UpdateDisplayUnits()
        {
            bool inches = DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches;
            uiAxialPosition.Properties.Mask.EditMask = DefaultValues.MeasNFormat;
            uiRadialAdjustmentRight.Properties.Mask.EditMask = DefaultValues.MeasNFormat;
            uiRadialAdjustmentRight.Properties.Increment = inches ? .002m : .2m;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ApplyChanges();
        }

        private void ApplyChanges()
        {
            float[,] rawData;
            ReformerHdf5Helper.Read2DTubeData(MainTube, ReformerScalar.RadiusOutside, out rawData);
            var side1LineValues = _tubeLines.First(l => l.Key.Contains(MainDataSide1Name)).Value.Values;
            var side2LineValues = _tubeLines.First(l => l.Key.Contains(MainDataSide2Name)).Value.Values;

            int numSlices = rawData.GetLength(0);
            int numSensors = rawData.GetLength(1);

            for (int slice = 0; slice < numSlices; slice++)
            {
                //Figure out how much the mean has changed from original data at each slice.
                double currentSide1Mean = side1LineValues[slice].ToDouble();
                double currentSide2Mean = side2LineValues[slice].ToDouble();

                double previousSide1Sum = 0, previousSide2Sum = 0;
                int halfSensors = numSensors / 2;
                for (int sensor = 0; sensor < numSensors; sensor++)
                {
                    bool side1 = sensor < halfSensors;
                    if (side1) 
                        previousSide1Sum += rawData[slice, sensor];
                    else
                        previousSide2Sum += rawData[slice, sensor];
                }

                double meanDiffSide1 = currentSide1Mean - previousSide1Sum / halfSensors;
                double meanDiffSide2 = currentSide2Mean - previousSide2Sum / halfSensors;

                //apply the desired offset at each sensor.
                for (int sensor = 0; sensor < numSensors; sensor++)
                {
                    bool side1 = sensor < halfSensors;
                    if (side1)
                        rawData[slice, sensor] += meanDiffSide1.ToSingle();
                    else
                        rawData[slice, sensor] += meanDiffSide2.ToSingle();
                }
            }
            ReformerHdf5Helper.SetTubeData(MainTube, ReformerScalar.RadiusOutside, rawData);
            Redraw();
        }

        #region mouse handling

        protected virtual void TheChartControl_MouseDown(object sender, MouseEventArgs e)
        {
            ((NChartControl)sender).Focus();
            NHitTestResult hitTestResult = ChartControl.HitTest(e.X, e.Y);

            if (hitTestResult.ChartElement == ChartElement.AxisConstLine)
            {
                NAxisConstLine pickedLine = (NAxisConstLine)hitTestResult.Object;
                if (XAxis.ConstLines.Contains(pickedLine))
                {
                    ChartControl.Cursor = Cursors.SizeWE;
                    ChartControl.Capture = true;
                    DraggingLine = pickedLine;
                }
            }
        }

        protected virtual void TheChartControl_MouseUp(object sender, MouseEventArgs e)
        {
            DraggingLine = null;
            ChartControl.Cursor = Cursors.Default;
            ChartControl.Capture = false;
        }

        protected void TheChartControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (DraggingLine == null) return;
            NVector3DF vecModelPoint = new NVector3DF();
            
            TheChart.TransformClientToModel(ChartControl.View.Context, XAxis, YAxis, ZAxis, 0, new Point(e.X, e.Y), ref vecModelPoint);
            double xValue = XAxis.TransformModelToScale(vecModelPoint.X, false);
            SetAxialValue(xValue);
        }

        #endregion mouse handling

        private void SetAxialValue(double xValue)
        {
            AxialCursor.Value = xValue;
            uiAxialPosition.Value = xValue.ToDecimal();
            ChartControl.Refresh();
        }

        private void uiRadialAdjustmentLeft_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void AdjustLineData(NLineSeries line, decimal shiftAmount, decimal posBoundary, bool leftShift)
        {
            for (int i = 0; i < line.XValues.Count; i++)
            {
                decimal xValue = line.XValues[i].ToDecimal();
                if (leftShift)
                {
                    if (xValue <= posBoundary)
                        line.Values[i] = (float)(line.Values[i].ToDecimal() + shiftAmount);
                }
                else //right shift
                {
                    if (xValue >= posBoundary)
                        line.Values[i] = (float)(line.Values[i].ToDecimal() + shiftAmount);
                }

            }
        }

        private void uiAxialPosition_EditValueChanged(object sender, EventArgs e)
        {
            SetAxialValue(uiAxialPosition.Value.ToDouble());
        }

        private void AdjustmentButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (MainTube == null || sender == null || (e.Button.Caption != "Right" && e.Button.Caption != "Left")) return;
            
            bool leftShift = e.Button.Caption == "Left";
            decimal amountToShift = leftShift ? uiRadialAdjustmentLeft.Value : uiRadialAdjustmentRight.Value;
            decimal axialPositionBoundary = uiAxialPosition.Value;

            if (chkShowSide1.Checked)
            {
                //adjust side 1
                var side1Line = _tubeLines.First(l => l.Key.Contains(MainDataSide1Name)).Value;
                AdjustLineData(side1Line, amountToShift, axialPositionBoundary, leftShift);
            }
            if (chkShowSide2.Checked)
            {
                //adjust side 2 too
                var side2Line = _tubeLines.First(l => l.Key.Contains(MainDataSide2Name)).Value;
                AdjustLineData(side2Line, amountToShift, axialPositionBoundary, leftShift);
            }
            ChartControl.Refresh();
            
        }

        private void chkRefInspections_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            Redraw();
        }

        
    }
}
