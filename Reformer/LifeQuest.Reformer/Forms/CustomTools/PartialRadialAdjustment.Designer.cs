﻿namespace Reformer.Forms.CustomTools
{
    partial class PartialRadialAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PartialRadialAdjustment));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.uiRadialAdjustmentRight = new DevExpress.XtraEditors.SpinEdit();
            this.uiAxialPosition = new DevExpress.XtraEditors.SpinEdit();
            this.chkShowSide2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkShowSide1 = new DevExpress.XtraEditors.CheckEdit();
            this.uiTube = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ChartControl = new Nevron.Chart.WinForm.NChartControl();
            this.chkRefInspections = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.uiMainInspection = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.uiRadialAdjustmentLeft = new DevExpress.XtraEditors.SpinEdit();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciMainInspection = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRefInspections = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTube = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciShowSide1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciShowSide2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciAxialPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRadialAdjustmentRight = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRadialAdjustmentLeft = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRadialAdjustmentRight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAxialPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSide2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSide1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRefInspections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMainInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRadialAdjustmentLeft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMainInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefInspections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowSide1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowSide2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAxialPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRadialAdjustmentRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRadialAdjustmentLeft)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.btnSave);
            this.lcMain.Controls.Add(this.uiRadialAdjustmentRight);
            this.lcMain.Controls.Add(this.uiAxialPosition);
            this.lcMain.Controls.Add(this.chkShowSide2);
            this.lcMain.Controls.Add(this.chkShowSide1);
            this.lcMain.Controls.Add(this.uiTube);
            this.lcMain.Controls.Add(this.ChartControl);
            this.lcMain.Controls.Add(this.chkRefInspections);
            this.lcMain.Controls.Add(this.uiMainInspection);
            this.lcMain.Controls.Add(this.uiRadialAdjustmentLeft);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.OptionsPrint.AppearanceGroupCaption.BackColor = System.Drawing.Color.LightGray;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseBackColor = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseFont = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.Options.UseTextOptions = true;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lcMain.OptionsPrint.AppearanceGroupCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lcMain.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lcMain.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lcMain.OptionsPrint.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(933, 501);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "layoutControl1";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 240);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(164, 22);
            this.btnSave.StyleController = this.lcMain;
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // uiRadialAdjustmentRight
            // 
            this.uiRadialAdjustmentRight.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiRadialAdjustmentRight.Location = new System.Drawing.Point(87, 188);
            this.uiRadialAdjustmentRight.Name = "uiRadialAdjustmentRight";
            this.uiRadialAdjustmentRight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Right", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("uiRadialAdjustmentRight.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.uiRadialAdjustmentRight.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiRadialAdjustmentRight.Properties.Mask.EditMask = "F3";
            this.uiRadialAdjustmentRight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uiRadialAdjustmentRight.Size = new System.Drawing.Size(89, 22);
            this.uiRadialAdjustmentRight.StyleController = this.lcMain;
            this.uiRadialAdjustmentRight.TabIndex = 11;
            this.uiRadialAdjustmentRight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AdjustmentButtonPressed);
            this.uiRadialAdjustmentRight.EditValueChanged += new System.EventHandler(this.uiRadialAdjustmentLeft_EditValueChanged);
            // 
            // uiAxialPosition
            // 
            this.uiAxialPosition.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiAxialPosition.Location = new System.Drawing.Point(84, 164);
            this.uiAxialPosition.Name = "uiAxialPosition";
            this.uiAxialPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiAxialPosition.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.uiAxialPosition.Properties.Mask.EditMask = "F3";
            this.uiAxialPosition.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uiAxialPosition.Size = new System.Drawing.Size(92, 20);
            this.uiAxialPosition.StyleController = this.lcMain;
            this.uiAxialPosition.TabIndex = 10;
            this.uiAxialPosition.EditValueChanged += new System.EventHandler(this.uiAxialPosition_EditValueChanged);
            // 
            // chkShowSide2
            // 
            this.chkShowSide2.EditValue = true;
            this.chkShowSide2.Location = new System.Drawing.Point(96, 60);
            this.chkShowSide2.Name = "chkShowSide2";
            this.chkShowSide2.Properties.Caption = "Show Side 2";
            this.chkShowSide2.Size = new System.Drawing.Size(80, 19);
            this.chkShowSide2.StyleController = this.lcMain;
            this.chkShowSide2.TabIndex = 9;
            this.chkShowSide2.CheckedChanged += new System.EventHandler(this.MainLineDataChanged);
            // 
            // chkShowSide1
            // 
            this.chkShowSide1.EditValue = true;
            this.chkShowSide1.Location = new System.Drawing.Point(12, 60);
            this.chkShowSide1.Name = "chkShowSide1";
            this.chkShowSide1.Properties.Caption = "Show Side 1";
            this.chkShowSide1.Size = new System.Drawing.Size(80, 19);
            this.chkShowSide1.StyleController = this.lcMain;
            this.chkShowSide1.TabIndex = 8;
            this.chkShowSide1.CheckedChanged += new System.EventHandler(this.MainLineDataChanged);
            // 
            // uiTube
            // 
            this.uiTube.Location = new System.Drawing.Point(45, 36);
            this.uiTube.Name = "uiTube";
            this.uiTube.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiTube.Size = new System.Drawing.Size(131, 20);
            this.uiTube.StyleController = this.lcMain;
            this.uiTube.TabIndex = 7;
            this.uiTube.SelectedIndexChanged += new System.EventHandler(this.SelectedTubeChanged);
            // 
            // ChartControl
            // 
            this.ChartControl.AutoRefresh = false;
            this.ChartControl.BackColor = System.Drawing.SystemColors.Control;
            this.ChartControl.InputKeys = new System.Windows.Forms.Keys[0];
            this.ChartControl.Location = new System.Drawing.Point(180, 12);
            this.ChartControl.Name = "ChartControl";
            this.ChartControl.Size = new System.Drawing.Size(741, 477);
            this.ChartControl.State = ((Nevron.Chart.WinForm.NState)(resources.GetObject("ChartControl.State")));
            this.ChartControl.TabIndex = 6;
            this.ChartControl.Text = "Chart";
            // 
            // chkRefInspections
            // 
            this.chkRefInspections.CheckOnClick = true;
            this.chkRefInspections.Location = new System.Drawing.Point(12, 99);
            this.chkRefInspections.Name = "chkRefInspections";
            this.chkRefInspections.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.chkRefInspections.Size = new System.Drawing.Size(164, 61);
            this.chkRefInspections.StyleController = this.lcMain;
            this.chkRefInspections.TabIndex = 5;
            this.chkRefInspections.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.chkRefInspections_ItemCheck);
            // 
            // uiMainInspection
            // 
            this.uiMainInspection.Location = new System.Drawing.Point(71, 12);
            this.uiMainInspection.Name = "uiMainInspection";
            this.uiMainInspection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMainInspection.Size = new System.Drawing.Size(105, 20);
            this.uiMainInspection.StyleController = this.lcMain;
            this.uiMainInspection.TabIndex = 4;
            this.uiMainInspection.SelectedIndexChanged += new System.EventHandler(this.uiMainInspection_SelectedIndexChanged);
            // 
            // uiRadialAdjustmentLeft
            // 
            this.uiRadialAdjustmentLeft.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiRadialAdjustmentLeft.Location = new System.Drawing.Point(87, 214);
            this.uiRadialAdjustmentLeft.Name = "uiRadialAdjustmentLeft";
            this.uiRadialAdjustmentLeft.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Left", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("uiRadialAdjustmentLeft.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.uiRadialAdjustmentLeft.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiRadialAdjustmentLeft.Properties.Mask.EditMask = "F3";
            this.uiRadialAdjustmentLeft.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uiRadialAdjustmentLeft.Size = new System.Drawing.Size(89, 22);
            this.uiRadialAdjustmentLeft.StyleController = this.lcMain;
            this.uiRadialAdjustmentLeft.TabIndex = 11;
            this.uiRadialAdjustmentLeft.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AdjustmentButtonPressed);
            this.uiRadialAdjustmentLeft.EditValueChanged += new System.EventHandler(this.uiRadialAdjustmentLeft_EditValueChanged);
            // 
            // lcgMain
            // 
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciMainInspection,
            this.lciRefInspections,
            this.emptySpaceItem2,
            this.lciChart,
            this.lciTube,
            this.lciShowSide1,
            this.lciShowSide2,
            this.lciAxialPosition,
            this.lciRadialAdjustmentRight,
            this.layoutControlItem1,
            this.lciRadialAdjustmentLeft});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "Root";
            this.lcgMain.Size = new System.Drawing.Size(933, 501);
            this.lcgMain.TextVisible = false;
            // 
            // lciMainInspection
            // 
            this.lciMainInspection.Control = this.uiMainInspection;
            this.lciMainInspection.Location = new System.Drawing.Point(0, 0);
            this.lciMainInspection.Name = "lciMainInspection";
            this.lciMainInspection.Size = new System.Drawing.Size(168, 24);
            this.lciMainInspection.Text = "Inspection:";
            this.lciMainInspection.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciMainInspection.TextSize = new System.Drawing.Size(54, 13);
            this.lciMainInspection.TextToControlDistance = 5;
            // 
            // lciRefInspections
            // 
            this.lciRefInspections.Control = this.chkRefInspections;
            this.lciRefInspections.Location = new System.Drawing.Point(0, 71);
            this.lciRefInspections.MaxSize = new System.Drawing.Size(0, 81);
            this.lciRefInspections.MinSize = new System.Drawing.Size(116, 81);
            this.lciRefInspections.Name = "lciRefInspections";
            this.lciRefInspections.Size = new System.Drawing.Size(168, 81);
            this.lciRefInspections.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciRefInspections.Text = "Reference Inspections:";
            this.lciRefInspections.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciRefInspections.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 254);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(168, 227);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciChart
            // 
            this.lciChart.Control = this.ChartControl;
            this.lciChart.Location = new System.Drawing.Point(168, 0);
            this.lciChart.Name = "lciChart";
            this.lciChart.Size = new System.Drawing.Size(745, 481);
            this.lciChart.TextSize = new System.Drawing.Size(0, 0);
            this.lciChart.TextVisible = false;
            // 
            // lciTube
            // 
            this.lciTube.Control = this.uiTube;
            this.lciTube.Location = new System.Drawing.Point(0, 24);
            this.lciTube.Name = "lciTube";
            this.lciTube.Size = new System.Drawing.Size(168, 24);
            this.lciTube.Text = "Tube:";
            this.lciTube.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciTube.TextSize = new System.Drawing.Size(28, 13);
            this.lciTube.TextToControlDistance = 5;
            // 
            // lciShowSide1
            // 
            this.lciShowSide1.Control = this.chkShowSide1;
            this.lciShowSide1.Location = new System.Drawing.Point(0, 48);
            this.lciShowSide1.Name = "lciShowSide1";
            this.lciShowSide1.Size = new System.Drawing.Size(84, 23);
            this.lciShowSide1.TextSize = new System.Drawing.Size(0, 0);
            this.lciShowSide1.TextVisible = false;
            // 
            // lciShowSide2
            // 
            this.lciShowSide2.Control = this.chkShowSide2;
            this.lciShowSide2.Location = new System.Drawing.Point(84, 48);
            this.lciShowSide2.Name = "lciShowSide2";
            this.lciShowSide2.Size = new System.Drawing.Size(84, 23);
            this.lciShowSide2.TextSize = new System.Drawing.Size(0, 0);
            this.lciShowSide2.TextVisible = false;
            // 
            // lciAxialPosition
            // 
            this.lciAxialPosition.Control = this.uiAxialPosition;
            this.lciAxialPosition.Location = new System.Drawing.Point(0, 152);
            this.lciAxialPosition.Name = "lciAxialPosition";
            this.lciAxialPosition.Size = new System.Drawing.Size(168, 24);
            this.lciAxialPosition.Text = "Axial Position:";
            this.lciAxialPosition.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciAxialPosition.TextSize = new System.Drawing.Size(67, 13);
            this.lciAxialPosition.TextToControlDistance = 5;
            // 
            // lciRadialAdjustmentRight
            // 
            this.lciRadialAdjustmentRight.Control = this.uiRadialAdjustmentRight;
            this.lciRadialAdjustmentRight.Location = new System.Drawing.Point(0, 176);
            this.lciRadialAdjustmentRight.Name = "lciRadialAdjustmentRight";
            this.lciRadialAdjustmentRight.Size = new System.Drawing.Size(168, 26);
            this.lciRadialAdjustmentRight.Text = "Adjustment >:";
            this.lciRadialAdjustmentRight.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciRadialAdjustmentRight.TextSize = new System.Drawing.Size(70, 13);
            this.lciRadialAdjustmentRight.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnSave;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 228);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(168, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciRadialAdjustmentLeft
            // 
            this.lciRadialAdjustmentLeft.Control = this.uiRadialAdjustmentLeft;
            this.lciRadialAdjustmentLeft.CustomizationFormText = "Adjustment >:";
            this.lciRadialAdjustmentLeft.Location = new System.Drawing.Point(0, 202);
            this.lciRadialAdjustmentLeft.Name = "lciRadialAdjustmentLeft";
            this.lciRadialAdjustmentLeft.Size = new System.Drawing.Size(168, 26);
            this.lciRadialAdjustmentLeft.Text = "Adjustment <:";
            this.lciRadialAdjustmentLeft.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciRadialAdjustmentLeft.TextSize = new System.Drawing.Size(70, 13);
            this.lciRadialAdjustmentLeft.TextToControlDistance = 5;
            // 
            // PartialRadialAdjustment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 501);
            this.Controls.Add(this.lcMain);
            this.Name = "PartialRadialAdjustment";
            this.ShowIcon = false;
            this.Text = "Partial Radial Adjustment";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRadialAdjustmentRight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiAxialPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSide2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSide1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTube.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRefInspections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMainInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiRadialAdjustmentLeft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMainInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRefInspections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTube)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowSide1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowSide2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciAxialPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRadialAdjustmentRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRadialAdjustmentLeft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraEditors.CheckedListBoxControl chkRefInspections;
        private DevExpress.XtraEditors.ImageComboBoxEdit uiMainInspection;
        private DevExpress.XtraLayout.LayoutControlItem lciMainInspection;
        private DevExpress.XtraLayout.LayoutControlItem lciRefInspections;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private Nevron.Chart.WinForm.NChartControl ChartControl;
        private DevExpress.XtraLayout.LayoutControlItem lciChart;
        private DevExpress.XtraEditors.ImageComboBoxEdit uiTube;
        private DevExpress.XtraLayout.LayoutControlItem lciTube;
        private DevExpress.XtraEditors.CheckEdit chkShowSide2;
        private DevExpress.XtraEditors.CheckEdit chkShowSide1;
        private DevExpress.XtraLayout.LayoutControlItem lciShowSide1;
        private DevExpress.XtraLayout.LayoutControlItem lciShowSide2;
        private DevExpress.XtraEditors.SpinEdit uiAxialPosition;
        private DevExpress.XtraLayout.LayoutControlItem lciAxialPosition;
        private DevExpress.XtraEditors.SpinEdit uiRadialAdjustmentRight;
        private DevExpress.XtraLayout.LayoutControlItem lciRadialAdjustmentRight;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SpinEdit uiRadialAdjustmentLeft;
        private DevExpress.XtraLayout.LayoutControlItem lciRadialAdjustmentLeft;
    }
}