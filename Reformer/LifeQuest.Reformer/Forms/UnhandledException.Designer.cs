﻿namespace Reformer.Forms
{
    partial class UnhandledException
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnhandledException));
            this.label2 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnIgnore = new DevExpress.XtraEditors.SimpleButton();
            this.lblReportHeader = new DevExpress.XtraEditors.LabelControl();
            this.lblReportText = new System.Windows.Forms.Label();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnReport = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCopyrightAndVersionInfo
            // 
            this.lblCopyrightAndVersionInfo.Location = new System.Drawing.Point(0, 234);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.labelControl4);
            this.pnlControls.Controls.Add(this.btnReport);
            this.pnlControls.Controls.Add(this.label1);
            this.pnlControls.Controls.Add(this.labelControl3);
            this.pnlControls.Controls.Add(this.labelControl2);
            this.pnlControls.Controls.Add(this.lblReportText);
            this.pnlControls.Controls.Add(this.lblReportHeader);
            this.pnlControls.Controls.Add(this.lblErrorMessage);
            this.pnlControls.Controls.Add(this.label2);
            this.pnlControls.Controls.Add(this.labelControl1);
            this.pnlControls.Controls.Add(this.btnClose);
            this.pnlControls.Controls.Add(this.btnIgnore);
            this.pnlControls.Size = new System.Drawing.Size(615, 252);
            this.pnlControls.Controls.SetChildIndex(this.btnIgnore, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnClose, 0);
            this.pnlControls.Controls.SetChildIndex(this.labelControl1, 0);
            this.pnlControls.Controls.SetChildIndex(this.label2, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblErrorMessage, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblReportHeader, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblReportText, 0);
            this.pnlControls.Controls.SetChildIndex(this.labelControl2, 0);
            this.pnlControls.Controls.SetChildIndex(this.labelControl3, 0);
            this.pnlControls.Controls.SetChildIndex(this.label1, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnReport, 0);
            this.pnlControls.Controls.SetChildIndex(this.labelControl4, 0);
            this.pnlControls.Controls.SetChildIndex(this.imgHeader, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblCopyrightAndVersionInfo, 0);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(622, 258);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(541, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ignoring the error may allow for continued work, but the error may have left the " +
    "program in an unrecoverable state.";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(9, 73);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(251, 13);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "An unexpected error has occurred. You may:";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(9, 151);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(24, 23);
            this.btnClose.TabIndex = 8;
            // 
            // btnIgnore
            // 
            this.btnIgnore.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.btnIgnore.Image = ((System.Drawing.Image)(resources.GetObject("btnIgnore.Image")));
            this.btnIgnore.Location = new System.Drawing.Point(8, 87);
            this.btnIgnore.Name = "btnIgnore";
            this.btnIgnore.Size = new System.Drawing.Size(24, 23);
            this.btnIgnore.TabIndex = 7;
            // 
            // lblReportHeader
            // 
            this.lblReportHeader.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblReportHeader.Location = new System.Drawing.Point(38, 124);
            this.lblReportHeader.Name = "lblReportHeader";
            this.lblReportHeader.Size = new System.Drawing.Size(92, 13);
            this.lblReportHeader.TabIndex = 14;
            this.lblReportHeader.Text = "Report the Error";
            // 
            // lblReportText
            // 
            this.lblReportText.AutoSize = true;
            this.lblReportText.Location = new System.Drawing.Point(52, 140);
            this.lblReportText.Name = "lblReportText";
            this.lblReportText.Size = new System.Drawing.Size(528, 13);
            this.lblReportText.TabIndex = 15;
            this.lblReportText.Text = "A log of the crash has been saved. Send this to devs with a brief description of " +
    "how to recreate it to get it fixed.";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(38, 92);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 13);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "Ignore the Error";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "If LifeQuest Reformer is in a broken state, simply exit and restart the program.";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(38, 156);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(21, 13);
            this.labelControl3.TabIndex = 17;
            this.labelControl3.Text = "Exit";
            // 
            // btnReport
            // 
            this.btnReport.Image = ((System.Drawing.Image)(resources.GetObject("btnReport.Image")));
            this.btnReport.Location = new System.Drawing.Point(9, 119);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(24, 23);
            this.btnReport.TabIndex = 19;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Location = new System.Drawing.Point(9, 189);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(84, 13);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "Error Message:";
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.AutoSize = true;
            this.lblErrorMessage.Location = new System.Drawing.Point(23, 205);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(46, 13);
            this.lblErrorMessage.TabIndex = 13;
            this.lblErrorMessage.Text = "No Error";
            // 
            // UnhandledException
            // 
            this.AcceptButton = this.btnIgnore;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(622, 258);
            this.ControlBox = false;
            this.Name = "UnhandledException";
            this.Text = "Unexpected Error";
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnIgnore;
        private System.Windows.Forms.Label lblReportText;
        private DevExpress.XtraEditors.LabelControl lblReportHeader;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnReport;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.Label lblErrorMessage;

    }
}