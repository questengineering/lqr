﻿using System;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Data.Project;
using Reformer.Interfaces;

namespace Reformer.Forms
{
    public partial class ReformerBaseForm : LifeQuestBaseForm, IHasReformerProjectDataSource
    {
        /// <summary> A flag on whether this form will react to changes in project structure (Inspection or data added/removed) </summary>
        protected bool ListeningToProjectChanges { get; set; }

        public ReformerBaseForm()
        {
            InitializeComponent();
            ListeningToProjectChanges = true;
        } 

        public virtual void UpdateDataSource()
        {
            if (ListeningToProjectChanges)
            {
                throw new NotImplementedException("The base UpdateDataSource should never be called.");
            }
        }

        public ReformerProjectInfo CurrentProject
        {
            get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; }
        }
    }
}
