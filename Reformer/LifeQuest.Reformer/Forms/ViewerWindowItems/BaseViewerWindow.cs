﻿using Reformer.Data.Tubes;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;

namespace Reformer.Forms.ViewerWindowItems
{
    public partial class BaseViewerWindow : BaseProcessWindow
    {
        public BaseViewerWindow()
        {
            InitializeComponent();
            TubeTable.TubeSelected += TubeSelected;
        }

        private void TubeSelected(object Sender, ReformerTube ReformerTube)
        {
           
        }

        public override void UpdateDataSource()
        {
            TubeTable.SetProject(CurrentProject);
        }
    }
}
