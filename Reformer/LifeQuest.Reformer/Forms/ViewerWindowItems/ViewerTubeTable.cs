﻿using System;
using System.Linq;
using System.Windows.Forms;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Tubes;

namespace Reformer.Forms.ViewerWindowItems
{
    public partial class ViewerTubeTable : UserControl
    {
        public ReformerInspection CurrentInspection { get {  return uiInspection.EditValue as ReformerInspection;} }

        public EventHandler<ReformerTube> TubeSelected;

        public ViewerTubeTable()
        {
            InitializeComponent();
        }

        public void SetProject(ReformerProjectInfo project)
        {
            if (project == null) return;
            bsProject.DataSource = project;
            if (project.InspectionFiles.Count > 0)
                uiInspection.EditValue = project.InspectionFiles.Last();
        }

        private void uiInspection_EditValueChanged(object sender, EventArgs e)
        {
            SetColumnsOptions();
            if (CurrentInspection != null)
                bsInspection.Position = bsInspection.IndexOf(CurrentInspection);
        }

        private void SetColumnsOptions()
        {
            if (CurrentInspection == null) return;
            colIDGrowth.Visible = !CurrentInspection.ReformerInfo.NewTubes;
            colODGrowth.Visible = !CurrentInspection.ReformerInfo.NewTubes && CurrentInspection.InspectionTool == Tool.MANTIS;
            colTubeRow.FieldName = CurrentInspection.ReformerInfo.UseRowLetters ? "ReformerAttributes.RowLetter" : "ReformerAttributes.RowNumber";
        }

        private void viewTubeData_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            var selectedTube = viewTubeData.GetRow(e.FocusedRowHandle) as ReformerTube;
            if (TubeSelected != null && selectedTube != null)
                TubeSelected(this, selectedTube);
        }
    }
}
