﻿namespace Reformer.Forms.ViewerWindowItems
{
    partial class ViewerTubeTable
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.gridTubeData = new DevExpress.XtraGrid.GridControl();
            this.bsTubes = new System.Windows.Forms.BindingSource(this.components);
            this.bsInspection = new System.Windows.Forms.BindingSource(this.components);
            this.bsProject = new System.Windows.Forms.BindingSource(this.components);
            this.viewTubeData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDGrowth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTubeRow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colODGrowth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.uiInspection = new DevExpress.XtraEditors.LookUpEdit();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciInspection = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTubeData = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTubeData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTubes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTubeData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeData)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.gridTubeData);
            this.lcMain.Controls.Add(this.uiInspection);
            this.lcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcMain.Location = new System.Drawing.Point(0, 0);
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.lcgMain;
            this.lcMain.Size = new System.Drawing.Size(332, 556);
            this.lcMain.TabIndex = 0;
            this.lcMain.Text = "layoutControl1";
            // 
            // gridTubeData
            // 
            this.gridTubeData.DataSource = this.bsTubes;
            this.gridTubeData.Location = new System.Drawing.Point(2, 26);
            this.gridTubeData.MainView = this.viewTubeData;
            this.gridTubeData.Name = "gridTubeData";
            this.gridTubeData.Size = new System.Drawing.Size(328, 528);
            this.gridTubeData.TabIndex = 5;
            this.gridTubeData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewTubeData});
            // 
            // bsTubes
            // 
            this.bsTubes.DataMember = "Tubes";
            this.bsTubes.DataSource = this.bsInspection;
            // 
            // bsInspection
            // 
            this.bsInspection.DataMember = "InspectionFiles";
            this.bsInspection.DataSource = this.bsProject;
            // 
            // bsProject
            // 
            this.bsProject.DataSource = typeof(Reformer.Data.Project.ReformerProjectInfo);
            // 
            // viewTubeData
            // 
            this.viewTubeData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colIDGrowth,
            this.colTubeRow,
            this.colODGrowth});
            this.viewTubeData.GridControl = this.gridTubeData;
            this.viewTubeData.GroupCount = 1;
            this.viewTubeData.Name = "viewTubeData";
            this.viewTubeData.OptionsBehavior.Editable = false;
            this.viewTubeData.OptionsBehavior.ReadOnly = true;
            this.viewTubeData.OptionsDetail.AllowZoomDetail = false;
            this.viewTubeData.OptionsDetail.EnableMasterViewMode = false;
            this.viewTubeData.OptionsDetail.ShowDetailTabs = false;
            this.viewTubeData.OptionsView.ShowDetailButtons = false;
            this.viewTubeData.OptionsView.ShowGroupPanel = false;
            this.viewTubeData.OptionsView.ShowIndicator = false;
            this.viewTubeData.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTubeRow, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.viewTubeData.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.viewTubeData_FocusedRowChanged);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Tube";
            this.colName.FieldName = "ReformerAttributes.TubeNumber";
            this.colName.MaxWidth = 79;
            this.colName.MinWidth = 79;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 100;
            // 
            // colIDGrowth
            // 
            this.colIDGrowth.AppearanceCell.Options.UseTextOptions = true;
            this.colIDGrowth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowth.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDGrowth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDGrowth.Caption = "%ID Growth";
            this.colIDGrowth.DisplayFormat.FormatString = "N1";
            this.colIDGrowth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIDGrowth.FieldName = "ReformerAttributes.OverallIDGrowth";
            this.colIDGrowth.Name = "colIDGrowth";
            this.colIDGrowth.Visible = true;
            this.colIDGrowth.VisibleIndex = 2;
            this.colIDGrowth.Width = 97;
            // 
            // colTubeRow
            // 
            this.colTubeRow.Caption = "Row";
            this.colTubeRow.FieldName = "ReformerAttributes.RowNumber";
            this.colTubeRow.Name = "colTubeRow";
            this.colTubeRow.Visible = true;
            this.colTubeRow.VisibleIndex = 4;
            // 
            // colODGrowth
            // 
            this.colODGrowth.AppearanceCell.Options.UseTextOptions = true;
            this.colODGrowth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODGrowth.AppearanceHeader.Options.UseTextOptions = true;
            this.colODGrowth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colODGrowth.Caption = "%OD Growth";
            this.colODGrowth.DisplayFormat.FormatString = "N1";
            this.colODGrowth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colODGrowth.FieldName = "ReformerAttributes.OverallODGrowth";
            this.colODGrowth.Name = "colODGrowth";
            this.colODGrowth.Visible = true;
            this.colODGrowth.VisibleIndex = 1;
            this.colODGrowth.Width = 93;
            // 
            // uiInspection
            // 
            this.uiInspection.Location = new System.Drawing.Point(59, 2);
            this.uiInspection.Name = "uiInspection";
            this.uiInspection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiInspection.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateInspected", "Date Inspected", 84, DevExpress.Utils.FormatType.DateTime, "M/d/yyyy", true, DevExpress.Utils.HorzAlignment.Center)});
            this.uiInspection.Properties.DataSource = this.bsInspection;
            this.uiInspection.Properties.DisplayMember = "Name";
            this.uiInspection.Properties.NullText = "";
            this.uiInspection.Size = new System.Drawing.Size(271, 20);
            this.uiInspection.StyleController = this.lcMain;
            this.uiInspection.TabIndex = 4;
            this.uiInspection.EditValueChanged += new System.EventHandler(this.uiInspection_EditValueChanged);
            // 
            // lcgMain
            // 
            this.lcgMain.AllowCustomizeChildren = false;
            this.lcgMain.CustomizationFormText = "lcgMain";
            this.lcgMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgMain.GroupBordersVisible = false;
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciInspection,
            this.lciTubeData});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgMain.ShowInCustomizationForm = false;
            this.lcgMain.Size = new System.Drawing.Size(332, 556);
            this.lcgMain.Text = "lcgMain";
            this.lcgMain.TextVisible = false;
            // 
            // lciInspection
            // 
            this.lciInspection.Control = this.uiInspection;
            this.lciInspection.CustomizationFormText = "Inspection:";
            this.lciInspection.Location = new System.Drawing.Point(0, 0);
            this.lciInspection.Name = "lciInspection";
            this.lciInspection.Size = new System.Drawing.Size(332, 24);
            this.lciInspection.Text = "Inspection:";
            this.lciInspection.TextSize = new System.Drawing.Size(54, 13);
            // 
            // lciTubeData
            // 
            this.lciTubeData.Control = this.gridTubeData;
            this.lciTubeData.CustomizationFormText = "lciTubeData";
            this.lciTubeData.Location = new System.Drawing.Point(0, 24);
            this.lciTubeData.Name = "lciTubeData";
            this.lciTubeData.Size = new System.Drawing.Size(332, 532);
            this.lciTubeData.Text = "lciTubeData";
            this.lciTubeData.TextSize = new System.Drawing.Size(0, 0);
            this.lciTubeData.TextToControlDistance = 0;
            this.lciTubeData.TextVisible = false;
            // 
            // ViewerTubeTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.Name = "ViewerTubeTable";
            this.Size = new System.Drawing.Size(332, 556);
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTubeData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTubes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTubeData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTubeData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraEditors.LookUpEdit uiInspection;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraLayout.LayoutControlItem lciInspection;
        private System.Windows.Forms.BindingSource bsInspection;
        private System.Windows.Forms.BindingSource bsProject;
        private DevExpress.XtraGrid.GridControl gridTubeData;
        private DevExpress.XtraGrid.Views.Grid.GridView viewTubeData;
        private DevExpress.XtraLayout.LayoutControlItem lciTubeData;
        private System.Windows.Forms.BindingSource bsTubes;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDGrowth;
        private DevExpress.XtraGrid.Columns.GridColumn colTubeRow;
        private DevExpress.XtraGrid.Columns.GridColumn colODGrowth;

    }
}
