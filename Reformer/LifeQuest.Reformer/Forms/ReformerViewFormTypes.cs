﻿using System;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Forms.DataProcessFlow;
using Reformer.Forms.SideBarControls;

namespace Reformer.Forms
{
    /// <summary> Defines the view types for LifeQuest Reformer </summary>
    public sealed class ReformerViewType: ViewType
    {
        private ReformerViewType(string value, Type theViewType): base(value, theViewType){}
    }

    /// <summary>
    /// Defines the form types for LifeQuest Reformer
    /// </summary>
    public sealed class ReformerFormType : FormType
    {
        public static readonly ReformerFormType ReformerProjectProperties = new ReformerFormType("ReformerProjectProperties", typeof(ReformerProjectProperties));
        public static readonly ReformerFormType ReformerProcessFlow = new ReformerFormType("ReformerProcessFlow", typeof(DataProcessFlowForm));
        public static readonly ReformerFormType CentralDataProcessForm = new ReformerFormType("CentralDataProcessForm,", typeof(CentralDataProcessForm));
        public static readonly ReformerFormType BlankBaseForm = new ReformerFormType("BlankBaseForm,", typeof(BlankBaseForm));
        
        private ReformerFormType(string value, Type TheFormType): base(value, TheFormType)
        {
        }
    }
}
