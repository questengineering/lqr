﻿namespace Reformer.Forms.Options
{
    partial class ColorOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.scalarRangeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chkNominal = new DevExpress.XtraEditors.CheckEdit();
            this.pnlORExample = new DevExpress.XtraEditors.PanelControl();
            this.uiDesignedOR = new DevExpress.XtraEditors.CalcEdit();
            this.uiMinIR = new DevExpress.XtraEditors.SpinEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.uiMaxIR = new DevExpress.XtraEditors.SpinEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.uiDesignedIR = new DevExpress.XtraEditors.CalcEdit();
            this.lblMaxOR = new DevExpress.XtraEditors.LabelControl();
            this.uiNominalSchedule = new DevExpress.XtraEditors.GridLookUpEdit();
            this.ViewSchedules = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNPS_Value = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextString = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInnerRadiusInDisplayUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblMinOR = new DevExpress.XtraEditors.LabelControl();
            this.uiMaxOR = new DevExpress.XtraEditors.SpinEdit();
            this.lblDesignedOR = new DevExpress.XtraEditors.LabelControl();
            this.uiMinOR = new DevExpress.XtraEditors.SpinEdit();
            this.lblMaxIR = new DevExpress.XtraEditors.LabelControl();
            this.pnlIRExample = new DevExpress.XtraEditors.PanelControl();
            this.lblMinIR = new DevExpress.XtraEditors.LabelControl();
            this.lblDesignedIR = new DevExpress.XtraEditors.LabelControl();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scalarRangeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNominal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlORExample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedOR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMinIR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMaxIR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedIR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNominalSchedule.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMaxOR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMinOR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIRExample)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.chkNominal);
            this.pnlControls.Controls.Add(this.pnlORExample);
            this.pnlControls.Controls.Add(this.uiDesignedOR);
            this.pnlControls.Controls.Add(this.uiMinIR);
            this.pnlControls.Controls.Add(this.btnOK);
            this.pnlControls.Controls.Add(this.uiMaxIR);
            this.pnlControls.Controls.Add(this.btnCancel);
            this.pnlControls.Controls.Add(this.uiDesignedIR);
            this.pnlControls.Controls.Add(this.lblMaxOR);
            this.pnlControls.Controls.Add(this.uiNominalSchedule);
            this.pnlControls.Controls.Add(this.lblMinOR);
            this.pnlControls.Controls.Add(this.uiMaxOR);
            this.pnlControls.Controls.Add(this.lblDesignedOR);
            this.pnlControls.Controls.Add(this.uiMinOR);
            this.pnlControls.Controls.Add(this.lblMaxIR);
            this.pnlControls.Controls.Add(this.pnlIRExample);
            this.pnlControls.Controls.Add(this.lblMinIR);
            this.pnlControls.Controls.Add(this.lblDesignedIR);
            this.pnlControls.Size = new System.Drawing.Size(378, 215);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(385, 221);
            // 
            // scalarRangeBindingSource
            // 
            this.scalarRangeBindingSource.DataSource = typeof(QuestIntegrity.Graphics.ColorScaleBar.ScalarRange);
            // 
            // chkNominal
            // 
            this.chkNominal.Location = new System.Drawing.Point(5, 6);
            this.chkNominal.Name = "chkNominal";
            this.chkNominal.Properties.Caption = "Base Off Nominal Size:";
            this.chkNominal.Size = new System.Drawing.Size(135, 19);
            this.chkNominal.TabIndex = 1;
            this.chkNominal.CheckedChanged += new System.EventHandler(this.chkNominal_CheckedChanged);
            // 
            // pnlORExample
            // 
            this.pnlORExample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlORExample.Location = new System.Drawing.Point(6, 131);
            this.pnlORExample.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.pnlORExample.Name = "pnlORExample";
            this.pnlORExample.Size = new System.Drawing.Size(367, 50);
            this.pnlORExample.TabIndex = 67;
            // 
            // uiDesignedOR
            // 
            this.uiDesignedOR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiDesignedOR.Location = new System.Drawing.Point(103, 108);
            this.uiDesignedOR.Name = "uiDesignedOR";
            this.uiDesignedOR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDesignedOR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedOR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedOR.Size = new System.Drawing.Size(57, 20);
            this.uiDesignedOR.TabIndex = 6;
            this.uiDesignedOR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // uiMinIR
            // 
            this.uiMinIR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiMinIR.Location = new System.Drawing.Point(211, 32);
            this.uiMinIR.Name = "uiMinIR";
            this.uiMinIR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMinIR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMinIR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMinIR.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.uiMinIR.Properties.Mask.EditMask = "p";
            this.uiMinIR.Size = new System.Drawing.Size(58, 20);
            this.uiMinIR.TabIndex = 4;
            this.uiMinIR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(249, 184);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(59, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "&OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // uiMaxIR
            // 
            this.uiMaxIR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiMaxIR.Location = new System.Drawing.Point(319, 32);
            this.uiMaxIR.Name = "uiMaxIR";
            this.uiMaxIR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMaxIR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMaxIR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMaxIR.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.uiMaxIR.Size = new System.Drawing.Size(54, 20);
            this.uiMaxIR.TabIndex = 5;
            this.uiMaxIR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(314, 184);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(59, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "&Cancel";
            // 
            // uiDesignedIR
            // 
            this.uiDesignedIR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiDesignedIR.Location = new System.Drawing.Point(103, 32);
            this.uiDesignedIR.Name = "uiDesignedIR";
            this.uiDesignedIR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiDesignedIR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedIR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiDesignedIR.Size = new System.Drawing.Size(57, 20);
            this.uiDesignedIR.TabIndex = 3;
            this.uiDesignedIR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // lblMaxOR
            // 
            this.lblMaxOR.Location = new System.Drawing.Point(275, 111);
            this.lblMaxOR.Name = "lblMaxOR";
            this.lblMaxOR.Size = new System.Drawing.Size(42, 13);
            this.lblMaxOR.TabIndex = 74;
            this.lblMaxOR.Text = "Max OR:";
            // 
            // uiNominalSchedule
            // 
            this.uiNominalSchedule.EditValue = "";
            this.uiNominalSchedule.Enabled = false;
            this.uiNominalSchedule.Location = new System.Drawing.Point(146, 5);
            this.uiNominalSchedule.Name = "uiNominalSchedule";
            this.uiNominalSchedule.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiNominalSchedule.Properties.DisplayMember = "TextString";
            this.uiNominalSchedule.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.uiNominalSchedule.Properties.View = this.ViewSchedules;
            this.uiNominalSchedule.Size = new System.Drawing.Size(106, 20);
            this.uiNominalSchedule.TabIndex = 1;
            this.uiNominalSchedule.EditValueChanged += new System.EventHandler(this.uiNominalSchedule_EditValueChanged);
            // 
            // ViewSchedules
            // 
            this.ViewSchedules.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNPS_Value,
            this.colTextString,
            this.colInnerRadiusInDisplayUnits});
            this.ViewSchedules.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.ViewSchedules.GroupCount = 1;
            this.ViewSchedules.Name = "ViewSchedules";
            this.ViewSchedules.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.ViewSchedules.OptionsView.ShowGroupPanel = false;
            this.ViewSchedules.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNPS_Value, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colNPS_Value
            // 
            this.colNPS_Value.Caption = "NPS";
            this.colNPS_Value.FieldName = "NPS_Value";
            this.colNPS_Value.Name = "colNPS_Value";
            this.colNPS_Value.Visible = true;
            this.colNPS_Value.VisibleIndex = 2;
            // 
            // colTextString
            // 
            this.colTextString.AppearanceCell.Options.UseTextOptions = true;
            this.colTextString.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTextString.AppearanceHeader.Options.UseTextOptions = true;
            this.colTextString.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTextString.Caption = "Sch";
            this.colTextString.FieldName = "TextString";
            this.colTextString.Name = "colTextString";
            this.colTextString.OptionsColumn.AllowEdit = false;
            this.colTextString.OptionsColumn.ReadOnly = true;
            this.colTextString.Visible = true;
            this.colTextString.VisibleIndex = 0;
            // 
            // colInnerRadiusInDisplayUnits
            // 
            this.colInnerRadiusInDisplayUnits.AppearanceCell.Options.UseTextOptions = true;
            this.colInnerRadiusInDisplayUnits.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInnerRadiusInDisplayUnits.AppearanceHeader.Options.UseTextOptions = true;
            this.colInnerRadiusInDisplayUnits.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInnerRadiusInDisplayUnits.Caption = "IR";
            this.colInnerRadiusInDisplayUnits.DisplayFormat.FormatString = "#.000";
            this.colInnerRadiusInDisplayUnits.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colInnerRadiusInDisplayUnits.FieldName = "InnerRadiusInDisplayUnits";
            this.colInnerRadiusInDisplayUnits.Name = "colInnerRadiusInDisplayUnits";
            this.colInnerRadiusInDisplayUnits.OptionsColumn.AllowEdit = false;
            this.colInnerRadiusInDisplayUnits.OptionsColumn.ReadOnly = true;
            this.colInnerRadiusInDisplayUnits.Visible = true;
            this.colInnerRadiusInDisplayUnits.VisibleIndex = 1;
            // 
            // lblMinOR
            // 
            this.lblMinOR.Location = new System.Drawing.Point(171, 111);
            this.lblMinOR.Name = "lblMinOR";
            this.lblMinOR.Size = new System.Drawing.Size(38, 13);
            this.lblMinOR.TabIndex = 73;
            this.lblMinOR.Text = "Min OR:";
            // 
            // uiMaxOR
            // 
            this.uiMaxOR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiMaxOR.Location = new System.Drawing.Point(319, 108);
            this.uiMaxOR.Name = "uiMaxOR";
            this.uiMaxOR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMaxOR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMaxOR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMaxOR.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.uiMaxOR.Size = new System.Drawing.Size(54, 20);
            this.uiMaxOR.TabIndex = 8;
            this.uiMaxOR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // lblDesignedOR
            // 
            this.lblDesignedOR.Location = new System.Drawing.Point(7, 111);
            this.lblDesignedOR.Name = "lblDesignedOR";
            this.lblDesignedOR.Size = new System.Drawing.Size(89, 13);
            this.lblDesignedOR.TabIndex = 72;
            this.lblDesignedOR.Text = "Designed OR (xx):";
            // 
            // uiMinOR
            // 
            this.uiMinOR.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.uiMinOR.Location = new System.Drawing.Point(211, 108);
            this.uiMinOR.Name = "uiMinOR";
            this.uiMinOR.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiMinOR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMinOR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.uiMinOR.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.uiMinOR.Size = new System.Drawing.Size(58, 20);
            this.uiMinOR.TabIndex = 7;
            this.uiMinOR.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // lblMaxIR
            // 
            this.lblMaxIR.Location = new System.Drawing.Point(275, 35);
            this.lblMaxIR.Name = "lblMaxIR";
            this.lblMaxIR.Size = new System.Drawing.Size(38, 13);
            this.lblMaxIR.TabIndex = 71;
            this.lblMaxIR.Text = "Max IR:";
            // 
            // pnlIRExample
            // 
            this.pnlIRExample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIRExample.Location = new System.Drawing.Point(7, 55);
            this.pnlIRExample.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.pnlIRExample.Name = "pnlIRExample";
            this.pnlIRExample.Size = new System.Drawing.Size(366, 50);
            this.pnlIRExample.TabIndex = 68;
            // 
            // lblMinIR
            // 
            this.lblMinIR.Location = new System.Drawing.Point(171, 35);
            this.lblMinIR.Name = "lblMinIR";
            this.lblMinIR.Size = new System.Drawing.Size(34, 13);
            this.lblMinIR.TabIndex = 70;
            this.lblMinIR.Text = "Min IR:";
            // 
            // lblDesignedIR
            // 
            this.lblDesignedIR.Location = new System.Drawing.Point(7, 35);
            this.lblDesignedIR.Name = "lblDesignedIR";
            this.lblDesignedIR.Size = new System.Drawing.Size(85, 13);
            this.lblDesignedIR.TabIndex = 69;
            this.lblDesignedIR.Text = "Designed IR (xx):";
            // 
            // ColorOptions
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(385, 221);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ColorOptions";
            this.Text = "Color Options";
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scalarRangeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNominal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlORExample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedOR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMinIR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMaxIR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiDesignedIR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNominalSchedule.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMaxOR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiMinOR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIRExample)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource scalarRangeBindingSource;
        private DevExpress.XtraEditors.CheckEdit chkNominal;
        private DevExpress.XtraEditors.PanelControl pnlORExample;
        private DevExpress.XtraEditors.CalcEdit uiDesignedOR;
        private DevExpress.XtraEditors.SpinEdit uiMinIR;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SpinEdit uiMaxIR;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.CalcEdit uiDesignedIR;
        private DevExpress.XtraEditors.LabelControl lblMaxOR;
        private DevExpress.XtraEditors.GridLookUpEdit uiNominalSchedule;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewSchedules;
        private DevExpress.XtraGrid.Columns.GridColumn colNPS_Value;
        private DevExpress.XtraGrid.Columns.GridColumn colTextString;
        private DevExpress.XtraGrid.Columns.GridColumn colInnerRadiusInDisplayUnits;
        private DevExpress.XtraEditors.LabelControl lblMinOR;
        private DevExpress.XtraEditors.SpinEdit uiMaxOR;
        private DevExpress.XtraEditors.LabelControl lblDesignedOR;
        private DevExpress.XtraEditors.SpinEdit uiMinOR;
        private DevExpress.XtraEditors.LabelControl lblMaxIR;
        private DevExpress.XtraEditors.PanelControl pnlIRExample;
        private DevExpress.XtraEditors.LabelControl lblMinIR;
        private DevExpress.XtraEditors.LabelControl lblDesignedIR;
    }
}