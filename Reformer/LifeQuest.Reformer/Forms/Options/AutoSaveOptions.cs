﻿
using System;
using System.Windows.Forms;
using QuestIntegrity.Core.Extensions;
using Reformer.WindowManagement;

namespace Reformer.Forms.Options
{
    public partial class AutoSaveOptions : UserControl
    {
        public AutoSaveOptions()
        {
            InitializeComponent();
            SetCurrentValues();
        }

        private void SetCurrentValues()
        {
            chkAutoSaveEnabled.Checked = Properties.Settings.Default.AutoSave;
            uiSaveTimer.Value = Properties.Settings.Default.AutoSaveTimer;
            chkSaveOnNext.Checked = Properties.Settings.Default.AutoSaveOnNext;
        }

        private void SaveAutoSaveOptions()
        {
            Properties.Settings.Default.AutoSave = chkAutoSaveEnabled.Checked;
            Properties.Settings.Default.AutoSaveTimer = uiSaveTimer.Value.ToInt32();
            Properties.Settings.Default.AutoSaveOnNext = chkSaveOnNext.Checked;
        }

        #region Event Handlers

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveAutoSaveOptions();
            ReformerWindowManager.Instance.Main.SetupAutoSave();
        }

        /// <summary> Prevents the autosave timer from going less than one minute. </summary>
        private void uiSaveTimer_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (uiSaveTimer.Value < 1) e.Cancel = true;
        }
        #endregion Event Handlers

        
    }
}
