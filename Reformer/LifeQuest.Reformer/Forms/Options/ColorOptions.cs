﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Units;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Project;
using QuestIntegrity.Graphics.ColorScaleBar;
using QuestIntegrity.LifeQuest.Common.Engineering;
using QuestIntegrity.LifeQuest.Common.Forms;
using QuestIntegrity.LifeQuest.Common.Interfaces;

namespace Reformer.Forms.Options
{
    public partial class ColorOptions : QIGBackColorForm, IHasDisplayUnits
    {
        private readonly ColorScaleRenderWindow _iRWindow = new ColorScaleRenderWindow { Dock = DockStyle.Fill };
        private readonly ColorScaleBar _irColorScale = new ColorScaleBar();
        private readonly ColorScaleRenderWindow _wTWindow = new ColorScaleRenderWindow { Dock = DockStyle.Fill };
        private readonly ColorScaleBar _orColorScale = new ColorScaleBar();

        public static ReformerProjectInfo CurrentProject { get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; } }
        private static double BaseToDisplayUnitFactor
        {
            get { return Length.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale, Length.LengthScale.Inches); }
        }

        private static double DisplayToBaseUnitFactor { get { return 1 / BaseToDisplayUnitFactor; } }

        public static EventHandler<ColorScaleChangedEventArgs> ColorScaleChanged;
        /// <summary> flag to prevent a designed OR/IR change from sending out multiple valuechanged events when updating IR/OR+- values. </summary>
        private bool _ignoreValueChanges;

        public ColorOptions()
        {
            InitializeComponent();
            SetupGraphics();
            SetupDataSource();
            UpdateDisplayUnits();
            SetValues();
        }

        private void SetValues()
        {
            if (CurrentProject == null) return;

            if (CurrentProject.ColorScaleBasis as NominalPipeSize != null)
            {
                //Get the matching reference from the list of items in the ui's datasource.
                NominalPipeSize nps = ((List<NominalPipeSize>)uiNominalSchedule.Properties.DataSource).FirstOrDefault(P => P.TextString == CurrentProject.ColorScaleBasis.TextString);
                chkNominal.Checked = true;
                uiNominalSchedule.EditValue = nps;
            }
            else if (CurrentProject.ColorScaleBasis != null) //set the previous values manually if it's not a nominal size
            {
                //If it isn't directly typed as NominalPipeSize, check to see if the IR and OR match up to one that can be set.
                decimal ir = CurrentProject.ColorScaleBasis.InnerRadius;
                decimal or = CurrentProject.ColorScaleBasis.OuterRadius;
                NominalPipeSize nps;
                if (PipeSchedules.Instance.GetNominalPipingSize(ir * 2 + or * 2, or - ir, out nps))
                {
                    chkNominal.Checked = false;
                    uiNominalSchedule.EditValue = nps;
                }
                else
                {
                    chkNominal.Checked = false;
                    uiDesignedIR.EditValue = CurrentProject.ColorScaleBasis.InnerRadiusInDisplayUnits;
                    uiDesignedOR.EditValue = CurrentProject.ColorScaleBasis.OuterRadiusInDisplayUnits;
                }
            }

            uiMinIR.EditValue = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusInside.ToString()).MinInDisplayUnits;
            uiMaxIR.EditValue = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusInside.ToString()).MaxInDisplayUnits;
            uiMinOR.EditValue = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusOutside.ToString()).MinInDisplayUnits;
            uiMaxOR.EditValue = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusOutside.ToString()).MaxInDisplayUnits;
        }

        private void SetupDataSource()
        {
            uiNominalSchedule.Properties.DataSource = PipeSchedules.Instance.PipeSizeTable.Where(P => P.NPS_Value >= 3 && P.NPS_Value <= 8).ToList(); //Only show pipe sizes 3-8 inches.
        }

        private void SetupGraphics()
        {
            _iRWindow.CreateControl();
            pnlIRExample.Controls.Add(_iRWindow);

            _irColorScale.CopyFrom(CurrentProject.ColorScale);
            _irColorScale.ActiveScalarName = ReformerScalar.RadiusInside.ToString();
            _irColorScale.Orientation = ColorScaleBar.ColorScaleBarOrientation.Horizontal;
            _iRWindow.SetColorScaleBar(_irColorScale);

            _wTWindow.CreateControl();
            pnlORExample.Controls.Add(_wTWindow);

            _orColorScale.CopyFrom(CurrentProject.ColorScale);
            _orColorScale.ActiveScalarName = ReformerScalar.RadiusOutside.ToString();
            _orColorScale.Orientation = ColorScaleBar.ColorScaleBarOrientation.Horizontal;
            _wTWindow.SetColorScaleBar(_orColorScale);

            UpdateExample(false);
        }

        private void UpdateExample(bool useUIValues)
        {
            if (useUIValues)
                _irColorScale.SetRange(new ScalarRange { Min = (double)uiMinIR.Value * DisplayToBaseUnitFactor, Max = (double)uiMaxIR.Value * DisplayToBaseUnitFactor, ScalarName = ReformerScalar.RadiusInside.ToString() });
            _iRWindow.SetColorScaleBar(_irColorScale);

            if (useUIValues)
                _orColorScale.SetRange(new ScalarRange { Min = (double)uiMinOR.Value * DisplayToBaseUnitFactor, Max = (double)uiMaxOR.Value * DisplayToBaseUnitFactor, ScalarName = ReformerScalar.RadiusOutside.ToString() });
            _wTWindow.SetColorScaleBar(_orColorScale);
        }

        private void Apply()
        {
            Cursor = Cursors.WaitCursor;

            if (CurrentProject == null) return;
            //Update the project values
            var oldOutRadRange = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusOutside.ToString());
            var oldInRadRange = CurrentProject.ColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusInside.ToString());
            var newOutRadRange = _orColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusOutside.ToString());
            var newInRadRange = _irColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusInside.ToString());
            CurrentProject.ColorScale.SetRange(newOutRadRange);
            CurrentProject.ColorScale.SetRange(newInRadRange);

            bool outerRadChanged = !oldOutRadRange.Min.IsWithinToleranceOf(newOutRadRange.Min, .0001) || !oldOutRadRange.Max.IsWithinToleranceOf(newOutRadRange.Max, .0001);
            bool innerRadChanged = !oldInRadRange.Min.IsWithinToleranceOf(newInRadRange.Min, .0001) || !oldInRadRange.Max.IsWithinToleranceOf(newInRadRange.Max, .0001);

            //Store the color scale basis in the project
            CurrentProject.ColorScaleBasis = chkNominal.Checked && uiNominalSchedule.EditValue as NominalPipeSize != null
                ? uiNominalSchedule.EditValue as NominalPipeSize
                : new PipeSize(uiDesignedIR.Value * 2, uiDesignedOR.Value);

            CurrentProject.ColorScaleBasis.InnerRadius = DisplayToBaseUnitFactor.ToDecimal() * uiDesignedIR.Value;
            CurrentProject.ColorScaleBasis.WallThickness = DisplayToBaseUnitFactor.ToDecimal() * (uiDesignedOR.Value - uiDesignedIR.Value);

            CurrentProject.ColorScale.Build();
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            if (ColorScaleChanged != null)
            {
                //Update the IR and then the OR in all forms if they've changed.
                if (innerRadChanged)
                    ColorScaleChanged(this, new ColorScaleChangedEventArgs(_irColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusInside.ToString())));
                if (outerRadChanged)
                    ColorScaleChanged(this, new ColorScaleChangedEventArgs(_orColorScale.MinMaxRanges.First(R => R.ScalarName == ReformerScalar.RadiusOutside.ToString())));
            }
            Cursor = Cursors.Default;
        }

        #region Event Handlers

        /// <summary> Enables/Disables selecters depending on state of chkNominal </summary>
        private void chkNominal_CheckedChanged(object sender, EventArgs e)
        {
            uiNominalSchedule.Enabled = chkNominal.Checked;
            uiDesignedIR.Enabled = !chkNominal.Checked;
            uiDesignedOR.Enabled = !chkNominal.Checked;
        }

        private void uiNominalSchedule_EditValueChanged(object sender, EventArgs e)
        {
            if (uiNominalSchedule.EditValue as NominalPipeSize == null) return;
            NominalPipeSize nps = (NominalPipeSize)uiNominalSchedule.EditValue;
            uiDesignedIR.Value = nps.InnerRadiusInDisplayUnits;
            uiDesignedOR.Value = nps.OuterRadiusInDisplayUnits;
            CurrentProject.ColorScaleBasis = nps;
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreValueChanges) return;

            _ignoreValueChanges = true;
            if (sender == uiDesignedIR)
            {
                uiMinIR.Value = uiDesignedIR.Value * DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusInside].Min.ToDecimal();
                uiMaxIR.Value = uiDesignedIR.Value * DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusInside].Max.ToDecimal();
            }
            else if (sender == uiDesignedOR)
            {
                uiMinOR.Value = uiDesignedOR.Value * DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusOutside].Min.ToDecimal();
                uiMaxOR.Value = uiDesignedOR.Value * DefaultValues.ColorScaleRangeByPercentOfNominal[ReformerScalar.RadiusOutside].Max.ToDecimal();
            }
            _ignoreValueChanges = false;
            UpdateExample(true);
        }

        #endregion Event Handlers

        public void UpdateDisplayUnits()
        {
            lblDesignedIR.Text = string.Format("Designed IR ({0})", DefaultValues.MeasurementUnitSymbol);
            lblDesignedOR.Text = string.Format("Designed OR ({0})", DefaultValues.MeasurementUnitSymbol);
            if (CurrentProject == null) return;
            const int precision = 3;
            //Set display and edit formats to the proper precision.
            uiDesignedIR.Properties.DisplayFormat.FormatString = uiMaxIR.Properties.DisplayFormat.FormatString = uiMinIR.Properties.DisplayFormat.FormatString = string.Format("N{0}", precision);
            uiDesignedIR.Properties.EditFormat.FormatString = uiMaxIR.Properties.EditFormat.FormatString = uiMinIR.Properties.EditFormat.FormatString = string.Format("N{0}", precision);
            uiDesignedIR.Properties.EditMask = uiMaxIR.Properties.EditMask = uiMinIR.Properties.EditMask = string.Format("N{0}", precision);

            uiDesignedOR.Properties.DisplayFormat.FormatString = uiMaxOR.Properties.DisplayFormat.FormatString = uiMinOR.Properties.DisplayFormat.FormatString = string.Format("N{0}", precision);
            uiDesignedOR.Properties.EditFormat.FormatString = uiMaxOR.Properties.EditFormat.FormatString = uiMinOR.Properties.EditFormat.FormatString = string.Format("N{0}", precision);
            uiDesignedOR.Properties.EditMask = uiMaxOR.Properties.EditMask = uiMinOR.Properties.EditMask = string.Format("N{0}", precision);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Apply();
            Close();
        }

    }


}
