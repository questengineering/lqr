﻿namespace Reformer.Forms.Options
{
    partial class AutoSaveOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lcOptions = new DevExpress.XtraLayout.LayoutControl();
            this.uiSaveTimer = new DevExpress.XtraEditors.SpinEdit();
            this.chkAutoSaveEnabled = new DevExpress.XtraEditors.CheckEdit();
            this.btnSave = new System.Windows.Forms.Button();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSaveTimer = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkSaveOnNext = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcOptions)).BeginInit();
            this.lcOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiSaveTimer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoSaveEnabled.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSaveTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaveOnNext.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // lcOptions
            // 
            this.lcOptions.AllowCustomizationMenu = false;
            this.lcOptions.Controls.Add(this.chkSaveOnNext);
            this.lcOptions.Controls.Add(this.uiSaveTimer);
            this.lcOptions.Controls.Add(this.chkAutoSaveEnabled);
            this.lcOptions.Controls.Add(this.btnSave);
            this.lcOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcOptions.Location = new System.Drawing.Point(0, 0);
            this.lcOptions.Name = "lcOptions";
            this.lcOptions.Root = this.layoutControlGroup1;
            this.lcOptions.Size = new System.Drawing.Size(369, 280);
            this.lcOptions.TabIndex = 0;
            this.lcOptions.Text = "The Layout Control";
            // 
            // uiSaveTimer
            // 
            this.uiSaveTimer.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.uiSaveTimer.Location = new System.Drawing.Point(97, 35);
            this.uiSaveTimer.Name = "uiSaveTimer";
            this.uiSaveTimer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.uiSaveTimer.Properties.IsFloatValue = false;
            this.uiSaveTimer.Properties.Mask.EditMask = "N00";
            this.uiSaveTimer.Size = new System.Drawing.Size(73, 20);
            this.uiSaveTimer.StyleController = this.lcOptions;
            this.uiSaveTimer.TabIndex = 8;
            this.uiSaveTimer.Validating += new System.ComponentModel.CancelEventHandler(this.uiSaveTimer_Validating);
            // 
            // chkAutoSaveEnabled
            // 
            this.chkAutoSaveEnabled.Location = new System.Drawing.Point(12, 12);
            this.chkAutoSaveEnabled.Name = "chkAutoSaveEnabled";
            this.chkAutoSaveEnabled.Properties.Caption = "Enable AutoSave";
            this.chkAutoSaveEnabled.Size = new System.Drawing.Size(158, 19);
            this.chkAutoSaveEnabled.StyleController = this.lcOptions;
            this.chkAutoSaveEnabled.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 245);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.lciSaveTimer,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(369, 280);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(162, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(187, 260);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 70);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(162, 163);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnSave;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 233);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(63, 27);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(63, 27);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(63, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(63, 233);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(99, 27);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkAutoSaveEnabled;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(162, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciSaveTimer
            // 
            this.lciSaveTimer.Control = this.uiSaveTimer;
            this.lciSaveTimer.CustomizationFormText = "Save Every (min)";
            this.lciSaveTimer.Location = new System.Drawing.Point(0, 23);
            this.lciSaveTimer.Name = "lciSaveTimer";
            this.lciSaveTimer.Size = new System.Drawing.Size(162, 24);
            this.lciSaveTimer.Text = "Save Every (min)";
            this.lciSaveTimer.TextSize = new System.Drawing.Size(82, 13);
            // 
            // chkSaveOnNext
            // 
            this.chkSaveOnNext.Location = new System.Drawing.Point(12, 59);
            this.chkSaveOnNext.Name = "chkSaveOnNext";
            this.chkSaveOnNext.Properties.Caption = "Save On \"Next\" Click";
            this.chkSaveOnNext.Size = new System.Drawing.Size(158, 19);
            this.chkSaveOnNext.StyleController = this.lcOptions;
            this.chkSaveOnNext.TabIndex = 9;
            this.chkSaveOnNext.ToolTip = "Saves progress every time the Next button in the central form is clicked.";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chkSaveOnNext;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(162, 23);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // AutoSaveOptions
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.Controls.Add(this.lcOptions);
            this.Name = "AutoSaveOptions";
            this.Size = new System.Drawing.Size(369, 280);
            ((System.ComponentModel.ISupportInitialize)(this.lcOptions)).EndInit();
            this.lcOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiSaveTimer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoSaveEnabled.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSaveTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaveOnNext.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcOptions;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.Button btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SpinEdit uiSaveTimer;
        private DevExpress.XtraEditors.CheckEdit chkAutoSaveEnabled;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem lciSaveTimer;
        private DevExpress.XtraEditors.CheckEdit chkSaveOnNext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}
