﻿
using System;
using DevExpress.XtraEditors.Controls;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Data;
using Reformer.WindowManagement;

namespace Reformer.Forms.Options
{
    public partial class UnitOptions : QIGBackColorForm
    {
        public UnitOptions()
        {
            Log.Debug(@"Showing Unit Options");
            InitializeComponent();
            SetupMenuItems();
            SetCurrentValues();
        }

        private void SetCurrentValues()
        {
            uiAxialUnits.EditValue = DisplayUnits.Instance.AxialDistanceUnits.Scale;
            uiMeasUnits.EditValue = DisplayUnits.Instance.MeasurementUnits.Scale;
        }

        private void SetupMenuItems()
        {
            //allow inches and mm for axial units
            uiAxialUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Inches.ToString(), Length.LengthScale.Inches));
            //uiAxialUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Feet.ToString(), Length.LengthScale.Feet));
            uiAxialUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Millimeters.ToString(), Length.LengthScale.Millimeters));
            //uiAxialUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Meters.ToString(), Length.LengthScale.Meters));

            //Only add mm and inches to measurement
            uiMeasUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Inches.ToString(), Length.LengthScale.Inches));
            uiMeasUnits.Properties.Items.Add(new ImageComboBoxItem(Length.LengthScale.Millimeters.ToString(), Length.LengthScale.Millimeters));
        }

        public void Save()
        {
            //By setting the display units, the project will save these in a serialized file during the save operation.
            DisplayUnits.Instance.AxialDistanceUnits = new Length((Length.LengthScale)Enum.Parse(typeof(Length.LengthScale), uiAxialUnits.EditValue.ToString()));
            DisplayUnits.Instance.MeasurementUnits = new Length((Length.LengthScale)Enum.Parse(typeof(Length.LengthScale), uiMeasUnits.EditValue.ToString()));

            //Trigger forms to update, may take a while but shouldn't hopefully.
            ReformerWindowManager.Instance.UpdateWindowDisplayUnits(null);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Save();
            Log.DebugFormat(@"Closing Unit Options. MeasUnits: {0}, AxialUnits: {1}", DefaultValues.MeasurementUnitSymbol, DefaultValues.AxialUnitSymbol);
            Close();
        }
    }
}
