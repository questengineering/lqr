﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.Forms;
using Reformer.Data;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Scalars;
using Reformer.Forms.DataProcessFlow;
using Reformer.Forms.Startup;
using Reformer.WindowManagement;
using Microsoft.Office.Interop.Outlook;
using QuestIntegrity.Core.Licensing;
using Exception = System.Exception;
using OutlookApp = Microsoft.Office.Interop.Outlook.Application;

namespace Reformer.Forms
{
    public partial class UnhandledException : BasePopupWindow
    {
        #region Private Properties

        readonly Exception _ex;
        private static ReformerProjectInfo Prj { get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; } }
        private static ReformerInspection Insp { get { return Prj != null ? Prj.CurrentInspectionFile : null; } }
        private readonly Task<IEnumerable<string>> _infoTask;

        #endregion Private Properties

        #region Constructors

        private UnhandledException()
        {
            InitializeComponent();

            //Figure out if we can actually open outlook by trying to create a temporary mail item.
            MailItem tmpMailItem = TryOpenOutlook();
            btnReport.Enabled = tmpMailItem != null;
            if (tmpMailItem != null) tmpMailItem.Close(OlInspectorClose.olDiscard);

            //Kick off a task to get the email body text in the background.
            _infoTask = new Task<IEnumerable<string>>(() => GetPertinentInformation(_ex));
            _infoTask.Start();
        }

        public UnhandledException(Exception ex) : this()
        {
            lblErrorMessage.Text = ex.Message;
            _ex = ex;
        }

        #endregion Constructors

        #region Event Handlers

        private void btnReport_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            CreateMailItem();
            Cursor = Cursors.Default;
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary> Creates the Outlook mail item with error information embedded. </summary>
        private void CreateMailItem()
        {
            //Try opening Outlook
            MailItem mailItem = TryOpenOutlook();
            if (mailItem == null) return;
            try
            {
                mailItem.Subject = @"LQR - Crash Report";
                mailItem.To = "LifeQuestSupport@QuestIntegrity.com";
                _infoTask.Wait();
                mailItem.Body = string.Join(Environment.NewLine, _infoTask.Result);
                mailItem.Display(true);
            }
            catch (Exception ex)
            {
                var result = MessageBox.Show(@"Could not open Outlook for the following reason: " + ex.Message, @"Failed To Create Email", MessageBoxButtons.RetryCancel, MessageBoxIcon.Asterisk);
                if (result == DialogResult.Retry)
                    CreateMailItem();
            }
        }

        /// <summary> Attempts to create an Outlook mail item. If it cannot create it, then it returns null. </summary>
        private static MailItem TryOpenOutlook()
        {
            try
            {
                OutlookApp outlookApp = new OutlookApp();
                return outlookApp.CreateItem(OlItemType.olMailItem);
            }
            catch (Exception ex)
            {
                Log.DebugFormat("Cannot open Outlook to send the report. \r\n\r\nMessage: {0}", ex.Message);
            }
            return null;
        }

        public static IEnumerable<string> GetPertinentInformation(Exception ex = null)
        {
            List<string> returnString = new List<string>{ @"--Input more information about triggering the behavior here--", string.Empty, "Auto-Gathered Information:", string.Empty };

            try
            {
                //Get the processing step they were on.
                returnString.Add("[Application Info]");
                returnString.Add(string.Format("    LicenseVersion: {0}", ApplicationVersion.LicenseVersion));
                returnString.Add(string.Format("    Today's Date: {0}", DateTime.Today.ToString("yyyy.MMdd")));

                returnString.Add("[ProcessWindow Info]");
                List<LifeQuestBaseForm> centralForms = ReformerWindowManager.Instance.GetFormsByType(typeof(CentralDataProcessForm), true).ToList();
                if (centralForms.Count > 0)
                {
                    CentralDataProcessForm form = (CentralDataProcessForm)centralForms[0];
                    returnString.Add(form.CurrentItem != null
                        ? string.Format("    Open Processing Window: {0}", form.CurrentItem.Name)
                        : "    No Processing Window Open.");
                }
                else returnString.Add("    No Processing Window Open.");

                //Licensing Info
                returnString.Add("[Licensing Info]");
                returnString.AddRange(LicenseManager.Instance.GetLicensesText());

                //Get the display units
                returnString.Add("[Unit Info]");
                returnString.Add(string.Format("    Measurement Units: {0}", DefaultValues.AxialUnitSymbol));
                returnString.Add(string.Format("    Axial Units: {0}", DefaultValues.AxialUnitSymbol));

                //Get the current project info
                returnString.Add("[Project Info]");
                if (Prj != null)
                {
                    returnString.Add(string.Format("    FilePath: {0}", Prj.FilePath));
                    returnString.Add(string.Format("    DateCreated: {0}", Prj.DateCreated));
                    returnString.Add(string.Format("    AppVersionWhenCreated: {0}", Prj.ApplicationVersionOnCreation));
                    returnString.Add(string.Format("    AppLicenseVersionWhenCreated: {0}", Prj.ApplicationLicenseVersionOnCreation));
                    returnString.Add(string.Format("    NumberOfInspections: {0}", Prj.InspectionFiles != null ? Prj.InspectionFiles.Count : 0));
                }
                else returnString.Add("No Project Open.");

                //Get the current Inspection info
                returnString.Add("[Inspection Info]");
                if (Insp != null)
                {
                    returnString.Add(string.Format("    Name: {0}", Insp.Name));
                    returnString.Add(string.Format("    Customer: {0}", Insp.ReformerInfo.Customer));
                    returnString.Add(string.Format("    DataFileName: {0}", Insp.DataFileFullName));
                    returnString.Add(string.Format("    GUID: {0}", Insp.GUID));
                    returnString.Add(string.Format("    ToolType: {0}", Insp.InspectionTool));
                    returnString.Add(string.Format("    ProcessFlowName: {0}", Insp.ProcessFlow.FlowName));
                    returnString.Add(string.Format("    NumberOfDataTubes: {0}", Insp.Tubes != null ? Insp.Tubes.Count(T => T.AvailableScalars.Contains(ReformerScalar.AxialPosition)) : 0));
                    returnString.Add(string.Format("    NumberOfEddyTubes: {0}", Insp.Tubes != null ? Insp.Tubes.Count(T => T.AvailableScalars.Contains(ReformerScalar.EddyData)) : 0));
                }
                else returnString.Add("     No Current Inspection.");

                //Display Info
                returnString.Add("[Display Info]");
                returnString.Add(string.Format("    DPIX: {0}", ReformerWindowManager.Instance.Main.CreateGraphics().DpiX));
                returnString.Add(string.Format("    DPIY: {0}", ReformerWindowManager.Instance.Main.CreateGraphics().DpiY));
                returnString.Add(string.Format("    MainWindowSize: {0}", ReformerWindowManager.Instance.Main != null ? ReformerWindowManager.Instance.Main.Size.ToString() : "Unknown"));

                //Get the error information, if any.
                returnString.Add("[Exception Info]");
                if (ex != null)
                {
                    returnString.Add(string.Format("    Error Message: {0}", ex.Message));
                    returnString.Add(string.Format("    Error StackTrace: {0}", ex.StackTrace));
                }
                else returnString.Add(@"    No Crash Detected");
            }
            catch
            {
                returnString.Add("--Couldn't get any more info.");
            }

            return returnString;
        }

        #endregion Private Methods

        #region Public Methods

        public static bool CreateBugReport()
        {
            try
            {
                MailItem mailItem = TryOpenOutlook();
                if (mailItem == null) return false;
                mailItem.Subject = @"LQR - Bug Report";
                mailItem.To = "LifeQuestSupport@QuestIntegrity.com";
                string stuff = string.Join(Environment.NewLine, GetPertinentInformation());
                mailItem.Body = stuff;
                mailItem.Display(true);
            }
            catch (Exception ex)
            {
                var result = MessageBox.Show(@"Could not open Outlook for the following reason: " + ex.Message, @"Failed To Create Email", MessageBoxButtons.RetryCancel, MessageBoxIcon.Asterisk);
                if (result == DialogResult.Retry)
                    CreateBugReport();
            }
            return true;
        }
        #endregion Public Methods
    }
}
