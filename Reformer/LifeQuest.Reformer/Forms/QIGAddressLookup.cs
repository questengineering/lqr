﻿using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.Core.Extensions.Strings;

namespace Reformer.Forms
{
    public static class QIGAddressLookup
    {
        /// <summary>
        /// field IDs for the target fields in the NevronDB used as template (Level1Template.ndb) for layout drawing
        /// </summary>
        public static int[] ElementIDs = new int[] {101,219,221,271};
        
        /// <summary>
        /// Addresses for the included offices current as of 1 Dec 2015
        /// Expected: 3 lines per office, final line is telephone #; lines pipe-delimited for GetAddressStringsByKey()
        /// </summary>
        private static Dictionary<string, string> Offices = new Dictionary<string, string>
        {
            { "Abu Dhabi UAE", "PO Box 130403|Abu Dhabi, United Arab Emirates|+971 2 6718961" },
            { "Best NL", "Hallenweg 1, 5683 CT Best|Netherlands|+31 499 745 300" },
            { "Calgary, AB CAN", "1339 40 Avenue NE, #27|Calgary, AB T2E 8N6 Canada|+1 403 273 0051" },
            { "Kent, WA USA", "19828 58th Pl South, Suite 100|Kent, WA 98032 USA|+1 253 893 7070" },
            { "Webster, TX USA", "17146 Feathercraft Lane, Suite 350|Webster, TX 77598 USA|+1 281 786 4700" }
        };

        public static List<string> GetOfficeList()
        {
            return Offices.Keys.ToList();
        }

        public static string[] GetAddressStringsByKey(string k)
        {
            if (k.IsNullOrEmpty())
            {
                k = Offices.Keys.ToList()[4]; //force Webster default if no entry
            }
            string linesdump;
            Offices.TryGetValue(k, out linesdump);
            string[] lines = linesdump.Split('|');
            return lines;
        }

    }


}