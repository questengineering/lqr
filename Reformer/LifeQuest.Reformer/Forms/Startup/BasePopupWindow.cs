﻿using System;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.Forms;

namespace Reformer.Forms.Startup
{
    public partial class BasePopupWindow : QIGBackColorForm
    {
        public BasePopupWindow()
        {
            InitializeComponent();
            GetUpdatedVersionInfo();
        }

        private void GetUpdatedVersionInfo()
        {
            Log.Debug("Updating process architecture and version info for BasePopupWindow.");
            string processArchitecture = Environment.Is64BitProcess ? "x64" : "x86";
            lblCopyrightAndVersionInfo.Text = string.Format("Quest Integrity Group  ©2014. Version {0} ({1})", ApplicationVersion.LicenseVersion, processArchitecture);
        }
    }
}
