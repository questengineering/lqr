﻿using System;
using QuestIntegrity.Core.Licensing;
using QuestIntegrity.Core.Licensing.Resources;
using QuestIntegrity.LifeQuest.Common;

namespace Reformer.Forms.Startup
{
    partial class LicenseError : BasePopupWindow
    {
        public LicenseError()
        {
            InitializeComponent();
            GetErrorCodeText();
        }

        private void GetErrorCodeText()
        {
            string header;
            string mainErrorMessage;
            switch (LicenseManager.Instance.CoreLicenseStatus)
            {
                case -999:
                    header = string.Format(Strings.LicenseError_Header_MissingDLL, "Reformer");
                    mainErrorMessage = string.Format(Strings.LicenseError_Message_MissingDLL, Environment.NewLine, LicenseManager.Instance.CoreLicenseStatusText, -999, LicenseManager.Instance.HostID, ApplicationVersion.LicenseVersion, ApplicationVersion.FullVersion, "Reformer");
                    break;
                case -6:
                    header = string.Format(Strings.LicenseError_Header_Version, "Reformer");
                    mainErrorMessage = string.Format(Strings.LicenseError_Message_Version, Environment.NewLine, LicenseManager.Instance.CoreLicenseStatusText, -6, LicenseManager.Instance.HostID, ApplicationVersion.LicenseVersion, ApplicationVersion.FullVersion, "Reformer");
                    break;
                case -3:
                    header = string.Format(Strings.LicenseError_Header_Expire, "Reformer");
                    mainErrorMessage = string.Format(Strings.LicenseError_Message_Expire, Environment.NewLine, LicenseManager.Instance.CoreLicenseStatusText, -3, LicenseManager.Instance.HostID, ApplicationVersion.LicenseVersion, ApplicationVersion.FullVersion, "Reformer");
                    break;
                case -1:
                    header = string.Format(Strings.LicenseError_Header, "Reformer");
                    mainErrorMessage = string.Format(Strings.LicenseError_Message_Missing, Environment.NewLine, LicenseManager.Instance.CoreLicenseStatusText, -1, LicenseManager.Instance.HostID, ApplicationVersion.LicenseVersion, ApplicationVersion.FullVersion, "Reformer");
                    break;
                default:
                    header = string.Format(Strings.LicenseError_Header, "Reformer");
                    mainErrorMessage = string.Format(Strings.LicenseError_Message_General, Environment.NewLine, LicenseManager.Instance.CoreLicenseStatusText, LicenseManager.Instance.CoreLicenseStatus, LicenseManager.Instance.HostID, ApplicationVersion.LicenseVersion, ApplicationVersion.FullVersion);
                    break;
            }
            lblHeaderText.Text = header;
            theErrorMessageBox.Text = mainErrorMessage;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
