﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraLayout.Utils;
using QuestIntegrity.Core.Licensing;
using Reformer.Data.Project;

namespace Reformer.Forms.Startup
{
    public partial class SplashScreen : BasePopupWindow
    {
        public FileInfo SelectedProject = new FileInfo("None Selected");
        public WelcomeAction Action;

        public SplashScreen()
        {
            InitializeComponent();
            if (LicenseManager.Instance.IsViewerOnly) 
                SetViewerOnly();
            InitializeMRUList();
        }

        #region Private Methods

        private void SetViewerOnly()
        {
            lciNewProject.Visibility = LayoutVisibility.Never; //no new projects in viewer mode.
            emptySpaceNextToNewProject.Visibility = LayoutVisibility.Never;//Make this invisible too so the space gets recovered.
        }

        private void InitializeMRUList()
        {
            //Add the first 5 files to the display.
            if (ProjectMRUList.Instance.Count == 0)
            {
                lciRecentProjects.Text = @"No Recent Projects to Choose From";
                return;
            }
            listRecentlyUsed.Items.Clear();
            foreach (FileInfo file in ProjectMRUList.Instance.Take(8).Select(s => new FileInfo(s)))
            {
                listRecentlyUsed.Items.Add(file.FullName);
            }
        }

        #endregion

        #region Event Handlers 

        private void linkOpenProject_OpenLink(object sender, OpenLinkEventArgs e)
        {
            SelectedProject = new FileInfo("None selected"); //clear any selected project since they clicked the generic open link.
            Action = WelcomeAction.Open;
            Close();
        }

        private void linkNewProject_OpenLink(object sender, OpenLinkEventArgs e)
        {
            Action = WelcomeAction.New;
            Close();
        }

        /// <summary> Formats the items being drawn to make ones that don't exist anymore have a strikethrough. </summary>
        private void listRecentlyUsed_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            FileInfo file = new FileInfo(e.Item.ToString());
            e.Appearance.ForeColor = Color.Blue;
            e.Appearance.Font = !file.Exists ? 
                new Font(e.Appearance.GetFont(), FontStyle.Strikeout) : 
                new Font(e.Appearance.GetFont(), FontStyle.Underline);
        }

        private void OpenSelectedItem()
        {
            string selectedValue = listRecentlyUsed.SelectedValue.ToString();
            if (string.IsNullOrEmpty(selectedValue)) return;
            FileInfo file = new FileInfo(selectedValue);
            if (!file.Exists) return;
            SelectedProject = file;
            Action = WelcomeAction.Open;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Action = WelcomeAction.Cancel;
            Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Action = WelcomeAction.ExitApplication;
            Close();
        }

        #endregion Event Handlers

        private void listRecentlyUsed_MouseClick(object sender, MouseEventArgs e)
        {
             if (listRecentlyUsed.IndexFromPoint(e.Location) != -1)
             OpenSelectedItem();
        }
        
    }

    public enum WelcomeAction
    {
        /// <summary> User just wants to leave this place. </summary>
        ExitApplication,
        /// <summary> User wants to see the main form in its full glory </summary>
        Cancel,
        /// <summary> User wants to create a new project for nefarious purposes no doubt.  Not available in Viewer Mode</summary>
        New,
        /// <summary> User wants to open a project. If Selected Project exists, that's the one user wants opened. If not, user wants to see the open dialog from Main. </summary>
        Open
    }
}

