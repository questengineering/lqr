﻿#region Copyright Quest Integrity Group, LLC 2012

// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// Filename: $Id: About.cs,v 1.514 2012/01/26 22:11:29 J.Rowe Exp $

#endregion Copyright Quest Integrity Group, LLC 2010

namespace Reformer.Forms.Startup
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkSupport = new System.Windows.Forms.LinkLabel();
            this.linkURL = new System.Windows.Forms.LinkLabel();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.aboutDetailPanel = new System.Windows.Forms.Panel();
            this.maxAllowedVersion = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.platform = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.expirationDate = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.issuedDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.customer = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.hostid = new System.Windows.Forms.Label();
            this.licenseVersion = new System.Windows.Forms.Label();
            this.buildVersion = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.optionsList = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.applicationName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.aboutDetailPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCopyrightAndVersionInfo
            // 
            this.lblCopyrightAndVersionInfo.Location = new System.Drawing.Point(0, 291);
            this.lblCopyrightAndVersionInfo.Padding = new System.Windows.Forms.Padding(10, 0, 0, 40);
            this.lblCopyrightAndVersionInfo.Size = new System.Drawing.Size(231, 52);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.pictureBox1);
            this.pnlControls.Controls.Add(this.linkSupport);
            this.pnlControls.Controls.Add(this.linkURL);
            this.pnlControls.Controls.Add(this.btnClose);
            this.pnlControls.Controls.Add(this.aboutDetailPanel);
            this.pnlControls.Size = new System.Drawing.Size(578, 343);
            this.pnlControls.Controls.SetChildIndex(this.imgHeader, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblCopyrightAndVersionInfo, 0);
            this.pnlControls.Controls.SetChildIndex(this.aboutDetailPanel, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnClose, 0);
            this.pnlControls.Controls.SetChildIndex(this.linkURL, 0);
            this.pnlControls.Controls.SetChildIndex(this.linkSupport, 0);
            this.pnlControls.Controls.SetChildIndex(this.pictureBox1, 0);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(585, 349);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(8, 156);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(315, 130);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // linkSupport
            // 
            this.linkSupport.AutoSize = true;
            this.linkSupport.Location = new System.Drawing.Point(5, 322);
            this.linkSupport.Name = "linkSupport";
            this.linkSupport.Size = new System.Drawing.Size(200, 13);
            this.linkSupport.TabIndex = 2;
            this.linkSupport.TabStop = true;
            this.linkSupport.Text = "lifequestsupport@questintegrity.com";
            this.linkSupport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSupport_LinkClicked);
            // 
            // linkURL
            // 
            this.linkURL.AutoSize = true;
            this.linkURL.Location = new System.Drawing.Point(5, 306);
            this.linkURL.Name = "linkURL";
            this.linkURL.Size = new System.Drawing.Size(135, 13);
            this.linkURL.TabIndex = 1;
            this.linkURL.TabStop = true;
            this.linkURL.Text = "www.QuestIntegrity.com";
            this.linkURL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkURL_LinkClicked);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(502, 312);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "OK";
            // 
            // aboutDetailPanel
            // 
            this.aboutDetailPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.aboutDetailPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.aboutDetailPanel.Controls.Add(this.maxAllowedVersion);
            this.aboutDetailPanel.Controls.Add(this.label11);
            this.aboutDetailPanel.Controls.Add(this.platform);
            this.aboutDetailPanel.Controls.Add(this.label1);
            this.aboutDetailPanel.Controls.Add(this.expirationDate);
            this.aboutDetailPanel.Controls.Add(this.label10);
            this.aboutDetailPanel.Controls.Add(this.label7);
            this.aboutDetailPanel.Controls.Add(this.label6);
            this.aboutDetailPanel.Controls.Add(this.issuedDate);
            this.aboutDetailPanel.Controls.Add(this.label2);
            this.aboutDetailPanel.Controls.Add(this.customer);
            this.aboutDetailPanel.Controls.Add(this.label9);
            this.aboutDetailPanel.Controls.Add(this.hostid);
            this.aboutDetailPanel.Controls.Add(this.licenseVersion);
            this.aboutDetailPanel.Controls.Add(this.buildVersion);
            this.aboutDetailPanel.Controls.Add(this.label5);
            this.aboutDetailPanel.Controls.Add(this.optionsList);
            this.aboutDetailPanel.Controls.Add(this.label4);
            this.aboutDetailPanel.Controls.Add(this.label3);
            this.aboutDetailPanel.Controls.Add(this.applicationName);
            this.aboutDetailPanel.Location = new System.Drawing.Point(339, 9);
            this.aboutDetailPanel.Name = "aboutDetailPanel";
            this.aboutDetailPanel.Padding = new System.Windows.Forms.Padding(3);
            this.aboutDetailPanel.Size = new System.Drawing.Size(233, 277);
            this.aboutDetailPanel.TabIndex = 51;
            // 
            // maxAllowedVersion
            // 
            this.maxAllowedVersion.AutoSize = true;
            this.maxAllowedVersion.Location = new System.Drawing.Point(96, 163);
            this.maxAllowedVersion.Margin = new System.Windows.Forms.Padding(3);
            this.maxAllowedVersion.Name = "maxAllowedVersion";
            this.maxAllowedVersion.Size = new System.Drawing.Size(58, 13);
            this.maxAllowedVersion.TabIndex = 20;
            this.maxAllowedVersion.Text = "2012.0401";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 163);
            this.label11.Margin = new System.Windows.Forms.Padding(3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Max Version:";
            // 
            // platform
            // 
            this.platform.Location = new System.Drawing.Point(96, 53);
            this.platform.Margin = new System.Windows.Forms.Padding(3);
            this.platform.Name = "platform";
            this.platform.Size = new System.Drawing.Size(113, 17);
            this.platform.TabIndex = 18;
            this.platform.Text = "x64";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Platform:";
            // 
            // expirationDate
            // 
            this.expirationDate.AutoSize = true;
            this.expirationDate.Location = new System.Drawing.Point(96, 146);
            this.expirationDate.Margin = new System.Windows.Forms.Padding(3);
            this.expirationDate.Name = "expirationDate";
            this.expirationDate.Size = new System.Drawing.Size(68, 13);
            this.expirationDate.TabIndex = 16;
            this.expirationDate.Text = "12-Jan-2008";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 146);
            this.label10.Margin = new System.Windows.Forms.Padding(3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Expiration Date:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 20);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Version:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 74);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 18);
            this.label6.TabIndex = 13;
            this.label6.Text = "Licensing";
            // 
            // issuedDate
            // 
            this.issuedDate.AutoSize = true;
            this.issuedDate.Location = new System.Drawing.Point(96, 128);
            this.issuedDate.Margin = new System.Windows.Forms.Padding(3);
            this.issuedDate.Name = "issuedDate";
            this.issuedDate.Size = new System.Drawing.Size(68, 13);
            this.issuedDate.TabIndex = 12;
            this.issuedDate.Text = "12-Jan-2007";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 128);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Issue Date:";
            // 
            // customer
            // 
            this.customer.AutoEllipsis = true;
            this.customer.AutoSize = true;
            this.customer.Location = new System.Drawing.Point(96, 93);
            this.customer.Margin = new System.Windows.Forms.Padding(3);
            this.customer.Name = "customer";
            this.customer.Size = new System.Drawing.Size(117, 13);
            this.customer.TabIndex = 10;
            this.customer.Text = "Rich Roberts dot com";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 93);
            this.label9.Margin = new System.Windows.Forms.Padding(3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Licensed To:";
            // 
            // hostid
            // 
            this.hostid.AutoSize = true;
            this.hostid.Location = new System.Drawing.Point(96, 111);
            this.hostid.Margin = new System.Windows.Forms.Padding(3);
            this.hostid.Name = "hostid";
            this.hostid.Size = new System.Drawing.Size(75, 13);
            this.hostid.TabIndex = 8;
            this.hostid.Text = "01ab3c04ffa2";
            // 
            // licenseVersion
            // 
            this.licenseVersion.Location = new System.Drawing.Point(96, 20);
            this.licenseVersion.Margin = new System.Windows.Forms.Padding(3);
            this.licenseVersion.Name = "licenseVersion";
            this.licenseVersion.Size = new System.Drawing.Size(113, 13);
            this.licenseVersion.TabIndex = 7;
            this.licenseVersion.Text = "2014.1231";
            // 
            // buildVersion
            // 
            this.buildVersion.Location = new System.Drawing.Point(96, 36);
            this.buildVersion.Margin = new System.Windows.Forms.Padding(3);
            this.buildVersion.Name = "buildVersion";
            this.buildVersion.Size = new System.Drawing.Size(113, 17);
            this.buildVersion.TabIndex = 6;
            this.buildVersion.Text = "1.2.2123";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Build:";
            // 
            // optionsList
            // 
            this.optionsList.FormattingEnabled = true;
            this.optionsList.Items.AddRange(new object[] {
            "Licenses"});
            this.optionsList.Location = new System.Drawing.Point(7, 208);
            this.optionsList.Name = "optionsList";
            this.optionsList.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.optionsList.Size = new System.Drawing.Size(218, 56);
            this.optionsList.TabIndex = 4;
            this.optionsList.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 193);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Options and Data Sets:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Host ID:";
            // 
            // applicationName
            // 
            this.applicationName.AutoSize = true;
            this.applicationName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.applicationName.Location = new System.Drawing.Point(3, 2);
            this.applicationName.Margin = new System.Windows.Forms.Padding(3);
            this.applicationName.Name = "applicationName";
            this.applicationName.Size = new System.Drawing.Size(138, 17);
            this.applicationName.TabIndex = 0;
            this.applicationName.Text = "LifeQuest™ Reformer";
            // 
            // About
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(585, 349);
            this.ControlBox = false;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "About";
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.aboutDetailPanel.ResumeLayout(false);
            this.aboutDetailPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkSupport;
        private System.Windows.Forms.LinkLabel linkURL;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.Panel aboutDetailPanel;
        private System.Windows.Forms.Label maxAllowedVersion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label platform;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label expirationDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label issuedDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label customer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label hostid;
        private System.Windows.Forms.Label licenseVersion;
        private System.Windows.Forms.Label buildVersion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox optionsList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label applicationName;



    }
}