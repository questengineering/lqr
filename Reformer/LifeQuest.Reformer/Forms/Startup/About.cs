﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 1/13/2012 2:08:25 PM
// Created by:   C.Madigan
//
#endregion

using QuestIntegrity.LifeQuest.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using QuestIntegrity.Core.Licensing;

namespace Reformer.Forms.Startup
{
    public partial class About : BasePopupWindow
    {
        public About()
        {
            InitializeComponent();
            SetValues();
        }

        private void SetValues()
        {
            try
            {
                licenseVersion.Text = ApplicationVersion.LicenseVersion;
                buildVersion.Text = ApplicationVersion.FullVersion;
                customer.Text = LicenseManager.Instance.Customer;
                hostid.Text = LicenseManager.Instance.HostID;
                issuedDate.Text = LicenseManager.Instance.IssueDate;
                expirationDate.Text = LicenseManager.Instance.ExpirationDate;
                maxAllowedVersion.Text = LicenseManager.Instance.Version;
                OptionsList = LicenseManager.Instance.GetLicensesText();
                platform.Text = Environment.Is64BitProcess ? "x64" : "x86";
                lblCopyrightAndVersionInfo.Text = @"Quest Integrity Group ©2014-2016";
            }
            catch (Exception ex)
            {
                throw new Exception("Error setting about form values.", ex);
            }
        }

        public List<string> OptionsList
        {
            set
            {
                optionsList.Items.Clear();
                if (value == null) return;
                value.ForEach(item => optionsList.Items.Add(item));
            }
        }

        #region Private Methods

        private void linkSupport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:lifequestsupport@Questintegrity.com");
        }

        private void linkURL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(linkURL.Text);
        }

        #endregion
    }
}