﻿namespace Reformer.Forms.Startup
{
    partial class BasePopupWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BasePopupWindow));
            this.lblCopyrightAndVersionInfo = new System.Windows.Forms.Label();
            this.imgHeader = new System.Windows.Forms.PictureBox();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.lblCopyrightAndVersionInfo);
            this.pnlControls.Controls.Add(this.imgHeader);
            this.pnlControls.Size = new System.Drawing.Size(476, 305);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(483, 311);
            // 
            // lblCopyrightAndVersionInfo
            // 
            this.lblCopyrightAndVersionInfo.AutoSize = true;
            this.lblCopyrightAndVersionInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblCopyrightAndVersionInfo.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyrightAndVersionInfo.Location = new System.Drawing.Point(0, 287);
            this.lblCopyrightAndVersionInfo.Name = "lblCopyrightAndVersionInfo";
            this.lblCopyrightAndVersionInfo.Padding = new System.Windows.Forms.Padding(6, 0, 0, 6);
            this.lblCopyrightAndVersionInfo.Size = new System.Drawing.Size(227, 18);
            this.lblCopyrightAndVersionInfo.TabIndex = 4;
            this.lblCopyrightAndVersionInfo.Text = "Quest Integrity Group  ©2014. Version 2014.0403 (x86)";
            // 
            // imgHeader
            // 
            this.imgHeader.Image = ((System.Drawing.Image)(resources.GetObject("imgHeader.Image")));
            this.imgHeader.Location = new System.Drawing.Point(8, 9);
            this.imgHeader.Name = "imgHeader";
            this.imgHeader.Size = new System.Drawing.Size(315, 60);
            this.imgHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgHeader.TabIndex = 3;
            this.imgHeader.TabStop = false;
            // 
            // BasePopupWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 311);
            this.Name = "BasePopupWindow";
            this.Text = "BasePopupWindow";
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblCopyrightAndVersionInfo;
        protected System.Windows.Forms.PictureBox imgHeader;
    }
}