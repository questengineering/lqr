﻿namespace Reformer.Forms.Startup
{
    partial class LicenseError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.theErrorMessageBox = new DevExpress.XtraEditors.MemoEdit();
            this.lblHeaderText = new DevExpress.XtraEditors.LabelControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.theErrorMessageBox.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCopyrightAndVersionInfo
            // 
            this.lblCopyrightAndVersionInfo.Location = new System.Drawing.Point(0, 336);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.btnOK);
            this.pnlControls.Controls.Add(this.lblHeaderText);
            this.pnlControls.Controls.Add(this.theErrorMessageBox);
            this.pnlControls.Size = new System.Drawing.Size(407, 354);
            this.pnlControls.Controls.SetChildIndex(this.imgHeader, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblCopyrightAndVersionInfo, 0);
            this.pnlControls.Controls.SetChildIndex(this.theErrorMessageBox, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblHeaderText, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnOK, 0);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(414, 360);
            // 
            // theErrorMessageBox
            // 
            this.theErrorMessageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.theErrorMessageBox.EditValue = "Licensing error message";
            this.theErrorMessageBox.Location = new System.Drawing.Point(8, 96);
            this.theErrorMessageBox.Name = "theErrorMessageBox";
            this.theErrorMessageBox.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.theErrorMessageBox.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.theErrorMessageBox.Properties.ReadOnly = true;
            this.theErrorMessageBox.Size = new System.Drawing.Size(390, 226);
            this.theErrorMessageBox.TabIndex = 5;
            this.theErrorMessageBox.UseOptimizedRendering = true;
            // 
            // lblHeaderText
            // 
            this.lblHeaderText.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderText.Location = new System.Drawing.Point(8, 75);
            this.lblHeaderText.Name = "lblHeaderText";
            this.lblHeaderText.Size = new System.Drawing.Size(139, 13);
            this.lblHeaderText.TabIndex = 6;
            this.lblHeaderText.Text = "Licensing Error Header Text";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(349, 328);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(49, 20);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            // 
            // LicenseError
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(414, 360);
            this.Name = "LicenseError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Licensing Error";
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.theErrorMessageBox.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblHeaderText;
        private DevExpress.XtraEditors.MemoEdit theErrorMessageBox;
        private DevExpress.XtraEditors.SimpleButton btnOK;

    }
}
