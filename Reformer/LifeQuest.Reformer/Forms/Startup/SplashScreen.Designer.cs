﻿namespace Reformer.Forms.Startup
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgQIGLogo = new System.Windows.Forms.PictureBox();
            this.pnlProjects = new DevExpress.XtraEditors.PanelControl();
            this.lcProjects = new DevExpress.XtraLayout.LayoutControl();
            this.listRecentlyUsed = new DevExpress.XtraEditors.ListBoxControl();
            this.linkOpenProject = new DevExpress.XtraEditors.HyperLinkEdit();
            this.linkNewProject = new DevExpress.XtraEditors.HyperLinkEdit();
            this.lcgProjects = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciNewProject = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciOpenProject = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceNextToNewProject = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciRecentProjects = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.borderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgQIGLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProjects)).BeginInit();
            this.pnlProjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcProjects)).BeginInit();
            this.lcProjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listRecentlyUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkOpenProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkNewProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNewProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOpenProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceNextToNewProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRecentProjects)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCopyrightAndVersionInfo
            // 
            this.lblCopyrightAndVersionInfo.Location = new System.Drawing.Point(0, 355);
            // 
            // imgHeader
            // 
            this.imgHeader.Location = new System.Drawing.Point(10, 3);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.imgQIGLogo);
            this.pnlControls.Controls.Add(this.pnlProjects);
            this.pnlControls.Controls.Add(this.btnExit);
            this.pnlControls.Controls.Add(this.btnClose);
            this.pnlControls.Size = new System.Drawing.Size(622, 373);
            this.pnlControls.Controls.SetChildIndex(this.imgHeader, 0);
            this.pnlControls.Controls.SetChildIndex(this.lblCopyrightAndVersionInfo, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnClose, 0);
            this.pnlControls.Controls.SetChildIndex(this.btnExit, 0);
            this.pnlControls.Controls.SetChildIndex(this.pnlProjects, 0);
            this.pnlControls.Controls.SetChildIndex(this.imgQIGLogo, 0);
            // 
            // borderPanel
            // 
            this.borderPanel.Size = new System.Drawing.Size(629, 379);
            // 
            // imgQIGLogo
            // 
            this.imgQIGLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.imgQIGLogo.Location = new System.Drawing.Point(8, 277);
            this.imgQIGLogo.Name = "imgQIGLogo";
            this.imgQIGLogo.Size = new System.Drawing.Size(189, 75);
            this.imgQIGLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgQIGLogo.TabIndex = 10;
            this.imgQIGLogo.TabStop = false;
            // 
            // pnlProjects
            // 
            this.pnlProjects.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pnlProjects.Controls.Add(this.lcProjects);
            this.pnlProjects.Location = new System.Drawing.Point(8, 90);
            this.pnlProjects.Margin = new System.Windows.Forms.Padding(0);
            this.pnlProjects.Name = "pnlProjects";
            this.pnlProjects.Size = new System.Drawing.Size(607, 183);
            this.pnlProjects.TabIndex = 9;
            // 
            // lcProjects
            // 
            this.lcProjects.AllowCustomization = false;
            this.lcProjects.Appearance.Control.BorderColor = System.Drawing.Color.Black;
            this.lcProjects.Appearance.Control.Options.UseBorderColor = true;
            this.lcProjects.BackColor = System.Drawing.Color.White;
            this.lcProjects.Controls.Add(this.listRecentlyUsed);
            this.lcProjects.Controls.Add(this.linkOpenProject);
            this.lcProjects.Controls.Add(this.linkNewProject);
            this.lcProjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcProjects.Location = new System.Drawing.Point(2, 2);
            this.lcProjects.Margin = new System.Windows.Forms.Padding(0);
            this.lcProjects.Name = "lcProjects";
            this.lcProjects.OptionsView.ShareLookAndFeelWithChildren = false;
            this.lcProjects.Root = this.lcgProjects;
            this.lcProjects.Size = new System.Drawing.Size(603, 179);
            this.lcProjects.TabIndex = 2;
            this.lcProjects.Text = "Layout Control";
            // 
            // listRecentlyUsed
            // 
            this.listRecentlyUsed.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.listRecentlyUsed.Location = new System.Drawing.Point(2, 64);
            this.listRecentlyUsed.Name = "listRecentlyUsed";
            this.listRecentlyUsed.Size = new System.Drawing.Size(599, 113);
            this.listRecentlyUsed.TabIndex = 3;
            this.listRecentlyUsed.ToolTip = "Double click an existing project to open. Marked projects do not exist anymore.";
            this.listRecentlyUsed.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this.listRecentlyUsed_DrawItem);
            this.listRecentlyUsed.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listRecentlyUsed_MouseClick);
            // 
            // linkOpenProject
            // 
            this.linkOpenProject.EditValue = "Open Project...";
            this.linkOpenProject.Location = new System.Drawing.Point(2, 24);
            this.linkOpenProject.Name = "linkOpenProject";
            this.linkOpenProject.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.linkOpenProject.Size = new System.Drawing.Size(91, 18);
            this.linkOpenProject.TabIndex = 2;
            this.linkOpenProject.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.linkOpenProject_OpenLink);
            // 
            // linkNewProject
            // 
            this.linkNewProject.EditValue = "New Project...";
            this.linkNewProject.Location = new System.Drawing.Point(2, 2);
            this.linkNewProject.Name = "linkNewProject";
            this.linkNewProject.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.linkNewProject.Size = new System.Drawing.Size(76, 18);
            this.linkNewProject.TabIndex = 1;
            this.linkNewProject.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.linkNewProject_OpenLink);
            // 
            // lcgProjects
            // 
            this.lcgProjects.AppearanceGroup.BorderColor = System.Drawing.Color.Black;
            this.lcgProjects.AppearanceGroup.Options.UseBorderColor = true;
            this.lcgProjects.CustomizationFormText = "lcgProjects";
            this.lcgProjects.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgProjects.GroupBordersVisible = false;
            this.lcgProjects.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciNewProject,
            this.lciOpenProject,
            this.emptySpaceNextToNewProject,
            this.emptySpaceItem2,
            this.lciRecentProjects});
            this.lcgProjects.Location = new System.Drawing.Point(0, 0);
            this.lcgProjects.Name = "lcgProjects";
            this.lcgProjects.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lcgProjects.Size = new System.Drawing.Size(603, 179);
            this.lcgProjects.Text = "lcgProjects";
            this.lcgProjects.TextVisible = false;
            // 
            // lciNewProject
            // 
            this.lciNewProject.Control = this.linkNewProject;
            this.lciNewProject.CustomizationFormText = "lciNewProject";
            this.lciNewProject.Location = new System.Drawing.Point(0, 0);
            this.lciNewProject.MaxSize = new System.Drawing.Size(80, 22);
            this.lciNewProject.MinSize = new System.Drawing.Size(80, 22);
            this.lciNewProject.Name = "lciNewProject";
            this.lciNewProject.Size = new System.Drawing.Size(80, 22);
            this.lciNewProject.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciNewProject.Text = "lciNewProject";
            this.lciNewProject.TextSize = new System.Drawing.Size(0, 0);
            this.lciNewProject.TextVisible = false;
            // 
            // lciOpenProject
            // 
            this.lciOpenProject.Control = this.linkOpenProject;
            this.lciOpenProject.CustomizationFormText = "lciOpenProject";
            this.lciOpenProject.Location = new System.Drawing.Point(0, 22);
            this.lciOpenProject.MaxSize = new System.Drawing.Size(95, 24);
            this.lciOpenProject.MinSize = new System.Drawing.Size(95, 24);
            this.lciOpenProject.Name = "lciOpenProject";
            this.lciOpenProject.Size = new System.Drawing.Size(95, 24);
            this.lciOpenProject.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOpenProject.Text = "lciOpenProject";
            this.lciOpenProject.TextSize = new System.Drawing.Size(0, 0);
            this.lciOpenProject.TextVisible = false;
            // 
            // emptySpaceNextToNewProject
            // 
            this.emptySpaceNextToNewProject.AllowHotTrack = false;
            this.emptySpaceNextToNewProject.CustomizationFormText = "emptySpaceNextToNewProject";
            this.emptySpaceNextToNewProject.Location = new System.Drawing.Point(80, 0);
            this.emptySpaceNextToNewProject.Name = "emptySpaceNextToNewProject";
            this.emptySpaceNextToNewProject.Size = new System.Drawing.Size(523, 22);
            this.emptySpaceNextToNewProject.Text = "emptySpaceNextToNewProject";
            this.emptySpaceNextToNewProject.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(95, 22);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(508, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciRecentProjects
            // 
            this.lciRecentProjects.Control = this.listRecentlyUsed;
            this.lciRecentProjects.CustomizationFormText = "Recent Projects";
            this.lciRecentProjects.Location = new System.Drawing.Point(0, 46);
            this.lciRecentProjects.Name = "lciRecentProjects";
            this.lciRecentProjects.Size = new System.Drawing.Size(603, 133);
            this.lciRecentProjects.Text = "Recent Projects";
            this.lciRecentProjects.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciRecentProjects.TextSize = new System.Drawing.Size(76, 13);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(495, 341);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "&Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(558, 341);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(57, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // SplashScreen
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(629, 379);
            this.Name = "SplashScreen";
            this.Text = "SplashScreen";
            ((System.ComponentModel.ISupportInitialize)(this.imgHeader)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.borderPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgQIGLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProjects)).EndInit();
            this.pnlProjects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcProjects)).EndInit();
            this.lcProjects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listRecentlyUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkOpenProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkNewProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNewProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOpenProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceNextToNewProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRecentProjects)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.PictureBox imgQIGLogo;
        private DevExpress.XtraEditors.PanelControl pnlProjects;
        private DevExpress.XtraLayout.LayoutControl lcProjects;
        private DevExpress.XtraEditors.ListBoxControl listRecentlyUsed;
        private DevExpress.XtraEditors.HyperLinkEdit linkOpenProject;
        private DevExpress.XtraEditors.HyperLinkEdit linkNewProject;
        private DevExpress.XtraLayout.LayoutControlGroup lcgProjects;
        private DevExpress.XtraLayout.LayoutControlItem lciNewProject;
        private DevExpress.XtraLayout.LayoutControlItem lciOpenProject;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceNextToNewProject;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lciRecentProjects;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.SimpleButton btnClose;
    }
}