﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Kitware.VTK;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Graphics.ColorScaleBar;
using Reformer.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.Forms.Options;

namespace Reformer.Graphics
{
    public partial class Reformer3DWindow : UserControl, IHasColorScaleBar
    {
        #region Private / Public Variables

        private readonly Dictionary<ReformerTube, vtkActor> _tubeActors = new Dictionary<ReformerTube, vtkActor>();
        private readonly List<ReformerTube> _tubeAddOrder = new List<ReformerTube>();
        private CartesianPoint _tubeRotation;
        protected readonly vtkAxisActor2D XAxis = new vtkAxisActor2D();
        
        #endregion

        #region Public Properties

        public CartesianPoint ViewScale { get; set; }
        protected RenderWindowControl RenderWindowControl;
        public vtkRenderWindow RenderWindow { get { return RenderWindowControl.RenderWindow; }}
        public vtkRenderer Renderer { get { return RenderWindow.GetRenderers().GetFirstRenderer(); }}
        public vtkCamera Camera { get { return Renderer.GetActiveCamera(); }}
        public ColorScaleBar ColorScaleBar { get; set; }

        public Dictionary<ReformerTube, vtkActor> TubeActors
        {
            get { return _tubeActors; }
        }

        public List<ReformerTube> TubeAddOrder
        {
            get { return _tubeAddOrder; }
        }

        public CartesianPoint CurrentFocus 
        {
            get 
            { 
                return new CartesianPoint(Camera.GetFocalPoint());
            }
            set { FocusCamera(value); }
        }

        private void FocusCamera(CartesianPoint value)
        {
            Camera.SetFocalPoint(value.X, value.Y, value.Z);
        }

        #endregion

        #region Constructors

        public Reformer3DWindow()
        {
            ColorScaleBar = new ColorScaleBar();
            ColorScaleBar.CopyFrom(LifeQuestReformer.Instance.TheDataManager.CurrentProject.ColorScale);
            InitializeComponent();
            RenderWindowControl = new RenderWindowControl();
            RenderWindowControl.CreateControl();
            RenderWindowControl.Dock = DockStyle.Fill;
            RenderWindowControl.RenderWindow.LineSmoothingOn();
            Camera.ParallelProjectionOn();
            Renderer.SetBackground(1, 1, 1);
            SetUpLighting();

            ViewScale = new CartesianPoint(1, 1, 1);
            SetupAxes();
            
            Controls.Add(RenderWindowControl);
            AttachColorBarChangedEvent();
        }

        private void SetupAxes()
        {
            XAxis.SetLabelFormat("%4.0f");
            XAxis.GetProperty().SetColor(0,0,0);
            vtkTextProperty tp = new vtkTextProperty();
            tp.SetOrientation(90);
            tp.SetColor(0, 0, 0);
            tp.ShadowOff();
            tp.SetBold(0);
            tp.SetItalic(0);
            XAxis.SetLabelTextProperty(tp);
            XAxis.SetTitleVisibility(0);
            Renderer.AddActor(XAxis);
        }

        private void SetUpLighting()
        {
            Renderer.SetAutomaticLightCreation(0);
            Renderer.GetLights().RemoveAllItems();

            // Add light kit which gives better illumination in 3D
            vtkLightKit kit = new vtkLightKit();
            kit.SetKeyLightAngle(50, 10);
            kit.SetKeyLightWarmth(0.60);
            kit.SetKeyLightIntensity(0.75);

            kit.SetFillLightAngle(-75, -10);
            kit.SetFillLightWarmth(0.40);
            kit.SetKeyToFillRatio(3.0);

            kit.SetBackLightAngle(0, 110);
            kit.SetBackLightWarmth(0.50);
            kit.SetKeyToBackRatio(3.5);

            kit.SetHeadLightWarmth(0.50);
            kit.SetKeyToHeadRatio(3.0);

            kit.AddLightsToRenderer(Renderer);
        }

        #endregion

        #region Public Methods

        public void Render()
        {
           RenderWindow.Render();
        }

        public void DrawXAxes(double[] ObjectBounds, double MinAxesValue, double MaxAxesValue, int NumberOfTicks = -1)
        {
            int[] windowSize = RenderWindow.GetSize();
            vtkCoordinate startCoordinate = new vtkCoordinate();
            startCoordinate.SetCoordinateSystemToWorld();
            startCoordinate.SetValue(ObjectBounds[0], ObjectBounds[2], ObjectBounds[4]);
            int[] startViewPortPoint = startCoordinate.GetComputedViewportValue(Renderer);
            double[] normalizedStartPoint = { (double)startViewPortPoint[0] / windowSize[0], (double)startViewPortPoint[1] / windowSize[1] };

            vtkCoordinate endCoordinate = new vtkCoordinate();
            endCoordinate.SetCoordinateSystemToWorld();
            endCoordinate.SetValue(ObjectBounds[1], ObjectBounds[2], ObjectBounds[4]);
            int[] endViewPortPoint = endCoordinate.GetComputedViewportValue(Renderer);
            double[] normalizedEndPoint = { (double)endViewPortPoint[0] / windowSize[0], (double)endViewPortPoint[1] / windowSize[1] };

            XAxis.SetPoint1(normalizedStartPoint[0], normalizedStartPoint[1]);
            XAxis.SetPoint2(normalizedEndPoint[0], normalizedEndPoint[1]); //Keep the Y value the same since it's an X Axis.

            XAxis.SetTitle(string.Format("{0}", DefaultValues.AxialUnitSymbol));
            XAxis.AdjustLabelsOff();
            XAxis.SetRange(MinAxesValue, MaxAxesValue);
            XAxis.SetNumberOfLabels(NumberOfTicks);
            
        }

        public void SetAxisVisible(bool IsVisible)
        {
            if (IsVisible)
                XAxis.VisibilityOn();
            else
                XAxis.VisibilityOff();
        }

        public void SetAbsolutRotation(double? X = null, double? Y = null, double? Z = null)
        {
            //validate incoming data.
            double nonNullX = 0, nonNullY = 0, nonNullZ = 0;
            if (X != null) {nonNullX = double.Parse(X.ToString());}
            if (Y != null) {nonNullY = double.Parse(Y.ToString());}
            if (Z != null) {nonNullZ = double.Parse(Z.ToString());}

            double xRelative = nonNullX - _tubeRotation.X;
            double yRelative = nonNullY - _tubeRotation.Y;
            double zRelative = nonNullZ - _tubeRotation.Z;
                
            //Perform a relative rotation on all non-null input axes.
            if (X != null)
            {
                _tubeActors.Keys.ToList().ForEach(tube =>
                {
                    _tubeActors[tube].RotateX(xRelative);
                    _tubeRotation.X = _tubeRotation.X + (nonNullX - _tubeRotation.X);
                });
            }
            if (Y != null)
            {
                _tubeActors.Keys.ToList().ForEach(tube =>
                {
                    _tubeActors[tube].RotateY(yRelative);
                    _tubeRotation.Y = _tubeRotation.Y + (nonNullY - _tubeRotation.Y);
                });
            }
            if (Z != null)
            {
                _tubeActors.Keys.ToList().ForEach(tube =>
                {
                    _tubeActors[tube].RotateZ(zRelative);
                    _tubeRotation.Z = _tubeRotation.Z + (nonNullZ - _tubeRotation.Z);
                });
            }
        }

        public void SetCameraRoll(float degrees)
        {
            Camera.SetRoll(degrees);
        }

        public void SetDefaultActorProperties(vtkActor Actor, ColorScaleBar CSB)
        {
            Actor.GetMapper().UseLookupTableScalarRangeOn();
            Actor.GetMapper().ScalarVisibilityOn();
            Actor.GetMapper().SetScalarModeToDefault();
            Actor.GetMapper().SetLookupTable(CSB.ColorTransferFunction);
            Actor.SetScale(ViewScale.X, ViewScale.Y, ViewScale.Z);
            Actor.GetProperty().SetInterpolationToGouraud();
         }

        public void SetCurrentXRotation(vtkActor Actor)
        {
            Actor.RotateX(_tubeRotation.X);
        }

        public void SetViewExtents(double[] VisibleBounds)
        {
            double midX = (VisibleBounds[1] + VisibleBounds[0])/2;
            double midY = (VisibleBounds[3] + VisibleBounds[2])/2;
            double midZ = (VisibleBounds[5] + VisibleBounds[4]) / 2;
            Camera.SetFocalPoint(midX, midY, midZ);
            
            Camera.SetViewUp(0, 1, 0);
            double distance = midX / (Math.Tan(Math.PI * Camera.GetViewAngle() * 2 / 180)); //2 is a fudge factor
            Camera.SetPosition(midX, midY, distance);
            
        }

        public void TakeScreenshot(string Folder, string FileName, int xPixels, int yPixels)
        {
            //int magnification = 1; // This has to be one, otherwise the axes won't show up.
            string tmpFileName = "tmp" + FileName;
            string tmpFullFileName = Folder + "\\" + tmpFileName;

            //Capture the image of this window including the cursor.
            vtkWindowToImageFilter newFilter = new vtkWindowToImageFilter();
            newFilter.SetInput(RenderWindow);
            newFilter.Update();
            vtkJPEGWriter theWriter = new vtkJPEGWriter();
            theWriter.SetFileName(tmpFullFileName);
            theWriter.SetInputConnection(newFilter.GetOutputPort());
            theWriter.Write();

            using (Bitmap newImage = new Bitmap(xPixels, yPixels))
            {
                Image srcImage = Image.FromFile(tmpFullFileName);
                using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(newImage))
                {
                    gr.SmoothingMode = SmoothingMode.HighQuality;
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gr.DrawImage(srcImage, new Rectangle(0, 0, xPixels, yPixels));
                }
                srcImage.Dispose();
                string fullFileName = Folder + "\\" + FileName;
                newImage.Save(fullFileName, ImageFormat.Jpeg);
            }

            File.Delete(tmpFullFileName);
        }

        public void SetCsbOrientation(Orientation TheOrientation)
        {
            if (TheOrientation == Orientation.Vertical)
            {
                ColorScaleBar.Orientation = ColorScaleBar.ColorScaleBarOrientation.Vertical;
                ColorScaleBar.Width = .10;
                ColorScaleBar.Height = .8;
                ColorScaleBar.Position = new[] { .85, .05 };
                ColorScaleBar.TextProp.SetFontSize(16);
            }
            else
            {
                ColorScaleBar.Orientation = ColorScaleBar.ColorScaleBarOrientation.Horizontal;
                ColorScaleBar.Width = .5;
                ColorScaleBar.Height = .2;
                ColorScaleBar.Position = new[] { .45, .05 };
                ColorScaleBar.TextProp.SetFontSize(16);
            }
            ColorScaleBar.Draw(Renderer);
        }

        internal void SetScalarOnColorScaleBar(ReformerScalar ScalarKey)
        {
            ColorScaleBar.Clear(Renderer);
            ColorScaleBar.ActiveScalarName = ScalarKey.ToString();
            ColorScaleBar.Build();
            ColorScaleBar.Draw(Renderer);
            ColorScaleBar.Render();
        }

        #endregion Public Methods

        public void SetInteractive(bool B)
        {
            if (B) Renderer.InteractiveOn();
            else Renderer.InteractiveOff();
        }
       
        #region IHasColorScaleBar Members

        public void AttachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged += HandleColorBarChangedEvent;
        }

        public void HandleColorBarChangedEvent(object Sender, ColorScaleChangedEventArgs Args)
        {
            if (ColorScaleBar.ActiveScalarName != Args.TheScalarRange.ScalarName) return;
            ColorScaleBar.SetRange(Args.TheScalarRange);
            ColorScaleBar.Build();
            ColorScaleBar.Draw(Renderer);
            Render();
        }

        public void DetachColorBarChangedEvent()
        {
            ColorOptions.ColorScaleChanged -= HandleColorBarChangedEvent;
        }
        
        #endregion IHasColorScaleBar Members

        
    }
}