﻿#region Copyright Quest Integrity Group, LLC 2013

//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/10/2013 1:40:52 PM
// Created by:   J.Foster
//

#endregion

using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Kitware.VTK;
using log4net;
using log4net.Config;
using QuestIntegrity.LifeQuest.Common.Properties;
using Reformer.Data;
using Reformer.Forms;

namespace Reformer
{
    public static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static ReformerMain _mainWindow;
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            //Set up logging.
            FileInfo logfile = new FileInfo(Path.Combine(UsefulDirectories.CommonApplicationDataPath.FullName, "LoggingConfig.xml"));
            if (logfile.Exists) XmlConfigurator.Configure(logfile);
            vtkObject.GlobalWarningDisplayOff(); //Disable VTK logging and warnings.
                
            Log.Info("Starting application and setting up.");

            // App Properties
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Set failsafe error handler, although these aren't needed for Reformer yet.
            Application.ThreadException += Application_UIThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Thread.CurrentThread.Name = "main"; // name the main thread for convenience of logging/debugging

            LifeQuestReformer lifeQuest = LifeQuestReformer.Instance;
            
            // Load application settings and logging
            if (lifeQuest.ProductSettings.UpgradeSettings)
            {
                lifeQuest.ProductSettings.Upgrade();
                lifeQuest.ProductSettings.UpgradeSettings = false;
            }

            // Set up third party licensing
            SetupThirdPartyLicenses();

            if (args.Length > 0) // Parse and load filename if specified
                lifeQuest.ProjectFileName = args[0];

            Log.Info("Creating Mutex.");
            //Create mutex to help installer recognize app is running
            IntPtr ipMutexAttr = new IntPtr(0);
            CreateMutex(ipMutexAttr, true, Properties.Resources.AppMutexName);

            if (Settings.Default.UpgradeSettings)
            {
                Settings.Default.Upgrade();
                Settings.Default.UpgradeSettings = false;
            }                        

            Log.Info("Showing the application.");
            _mainWindow = new ReformerMain();
            Application.Run(_mainWindow);
            Log.Info("Exiting application.");
        }

        /// <summary> Handler for UI thread exceptions </summary>
        private static void Application_UIThreadException(object sender, ThreadExceptionEventArgs e)
        {
            // Log it and close if requested
            DialogResult result;
            using (var form = new UnhandledException(e.Exception))
            {
                result = _mainWindow != null ? form.ShowDialog(_mainWindow) : form.ShowDialog();
                Log.Fatal(e.Exception.Message, e.Exception);
            }

            if (result == DialogResult.Abort)
            {
                if (_mainWindow != null) _mainWindow.Close(); //Close windows so the Exit call doesn't have to close them. Causes exception if panels are open, otherwise.
                Application.Exit();
            }
        }

        /// <summary>
        ///     Handler for non-UI thread errors.
        /// </summary>
        /// <param name="sender">The object raising the exception.</param>
        /// <param name="e">The unhandled exception.</param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;                
                Log.Fatal(ex.Message, ex);
                Application.Exit();
            }
            catch (Exception exc)
            {
                try
                {
                    MessageBox.Show(@"Fatal Non-UI Error", @"Fatal Non-UI Error. Could not write the error to the event log. Reason: " + exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally { Application.Exit();}
            }
        }

        [DllImport("kernel32.dll")]
        private static extern IntPtr CreateMutex(IntPtr lpMutexAttributes, bool bInitialOwner, string lpName);

        /// <summary> Setup any third party licensing that needs to be setup such as Nevron </summary>
        private static void SetupThirdPartyLicenses()
        {
            // Set the Nevron License
            Nevron.NLicenseManager.Instance.SetLicense(new Nevron.NLicense(Resources.Licenses.NevronDiagram));
            Nevron.NLicenseManager.Instance.SetLicense(new Nevron.NLicense(Resources.Licenses.NevronCharting));
            Nevron.NLicenseManager.Instance.LockLicense = true;
        }
        
    }
}