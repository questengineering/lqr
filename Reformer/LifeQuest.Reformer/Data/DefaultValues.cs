﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Data
{
    static class DefaultValues
    {
        private static ReformerProjectInfo CurrentProject
        {
            get { return LifeQuestReformer.Instance.TheDataManager.CurrentProject; }
        }

        private static ReformerInspection CurrentInspection
        {
            get { return CurrentProject == null ? null : CurrentProject.CurrentInspectionFile; }
        }

        public static class TubeTolerances
        {
            public const float ODTolUp = .079f;
            public const float ODTolDown = 0f;
            public const float IDTolDown = -.039f;
            public const float IDTolUp = 0f;
        }

        static internal Dictionary<Tool, double> MissedStartData = new Dictionary<Tool, double>
        {
            {Tool.LOTIS, 15},
            {Tool.MANTIS, 5}
        };

        static internal Dictionary<Tool, double> MissedEndData = new Dictionary<Tool, double>
        {
            {Tool.LOTIS, 10}, //Add 6 for axial encoder error
            {Tool.MANTIS, 32}
        };

        //Default oxidation rate provided by Tim Haugen, units: in/yr. Changed to 0 pending changes per Tim Haugen 3/20/14
        static internal float OxidationRate
        {
            get { return .000f; }
        }

        static internal Dictionary<ReformerScalar, Range<float>> ColorScaleRangeByPercentOfNominal = new Dictionary<ReformerScalar, Range<float>>
        {
            {ReformerScalar.RadiusInside, new Range<float>(.98f, 1.05f)},
            {ReformerScalar.RadiusOutside, new Range<float>(.98f, 1.05f)}
        };

        static internal Dictionary<Tool, Range<float>> ToolTolerances
        {
            get
            {
                return new Dictionary<Tool, Range<float>>
                    {
                        {Tool.LOTIS, DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? new Range<float>(-.005f, .005f) : new Range<float>(-.127f, 0.127f) },
                        {Tool.MANTIS, DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? new Range<float>(-.015f, .015f) : new Range<float>(-.381f, 0.381f) }
                    };
            }
        }

        static internal string AxialNFormat { get { return "F1"; } }
        static internal string MeasNFormat { get { return "F" + MeasNumDecimals; } }
        static internal int MeasNumDecimals { get { return DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? 3 : 2; } }

        static internal string AxialHashFormat { get { return "0.0"; } }
        static internal string MeasHashFormat { get { return DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? "F3" : "F2"; } }

        internal static string AxialUnitSymbol
        {
            get
            {
                return DisplayUnits.Instance.AxialDistanceUnits.Scale == Length.LengthScale.Inches
                    ? DisplayUnits.Instance.AxialDistanceUnits.Symbol + "."
                    : DisplayUnits.Instance.AxialDistanceUnits.Symbol;
            }
        }

        internal static string MeasurementUnitSymbol
        {
            get
            {
                return DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches
                    ? DisplayUnits.Instance.MeasurementUnits.Symbol + "."
                    : DisplayUnits.Instance.MeasurementUnits.Symbol;
            }
        }

        /// <summary>
        /// CAM: Tim wants Inch instead of in. on the reports. To avoid breaking MeasurementUnitSymbol I have added this to convert
        /// </summary>
        internal static string MeasurementUnitSymbolConverted => DisplayUnits.Instance.MeasurementUnits.Scale == Length.LengthScale.Inches ? "Inch" : MeasurementUnitSymbol;

        public static float MeasDisplayToBaseUnits { get { return CurrentInspection == null ? 1 : Length.Convert(1, Length.LengthScale.Inches, DisplayUnits.Instance.MeasurementUnits.Scale); } }
        public static float MeasBaseToDisplayUnits { get { return 1 / MeasDisplayToBaseUnits; } }
        public static float AxialDisplayToBaseUnits { get { return CurrentInspection == null ? 1 : Length.Convert(1, Length.LengthScale.Inches, DisplayUnits.Instance.AxialDistanceUnits.Scale); } }
        public static float AxialBaseToDisplayUnits { get { return 1 / AxialDisplayToBaseUnits; } }

        static public Dictionary<Tool, StatisticalMeasures> Meaures = new Dictionary<Tool, StatisticalMeasures>
        {
            {Tool.LOTIS, StatisticalMeasures.Median},
            {Tool.MANTIS,StatisticalMeasures.Mean}
        };

        static public Range<double> GetAxesRange(IEnumerable<ReformerTube> tubes, out int equalPartitions)
        {
            double maxLengthDisplayUnits = tubes.Max(t => t.Specs.LengthDisplayUnits);
            int unusedValue, roundedLength;
            if (DisplayUnits.Instance.AxialDistanceUnits.Scale == Length.LengthScale.Inches)
            {
                equalPartitions = (Math.DivRem((int)maxLengthDisplayUnits - 1, 50, out unusedValue) + 1);
                roundedLength = 50 * equalPartitions;
            }
            else
            {
                equalPartitions = (Math.DivRem((int)maxLengthDisplayUnits - 1, 1000, out unusedValue) + 1);
                roundedLength = 1000 * equalPartitions;
            }
            //equal partitions may have some use in the future, but for now not really.
            return new Range<double>(0, roundedLength);
        }

    }
}
