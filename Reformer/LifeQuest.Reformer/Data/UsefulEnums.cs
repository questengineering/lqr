﻿namespace Reformer.Data
{
    public enum ODorID
    {
        OD,
        ID
    }

    public enum ScaleMode
    {
        SpecificRange,
        Automatic
    }

    public enum Tool
    {
        MANTIS,
        LOTIS
    }

    public enum MapTypes
    {
        ExpansionMap,
        Level1Assessment
    }

    public enum ReformerLayout
    {
        Cylindrical,
        Box
    }

    public enum TubeNumberingDirection
    {
        Clockwise,
        CounterClockWise
    }

    public enum MapDirections
    {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }

    public enum ToleranceComparisons
    {
        Exceeds,
        Within,
        WithinToolTolerance,
        NoValue
    }

    public enum ToleranceDirection
    {
        Below,
        Above
    }

    public enum OrderBy
    {
        RowNumber,
        TubeNumber
    }

}
