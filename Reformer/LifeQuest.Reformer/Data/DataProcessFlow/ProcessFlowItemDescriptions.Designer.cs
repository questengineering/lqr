﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Reformer.Data.DataProcessFlow {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ProcessFlowItemDescriptions {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ProcessFlowItemDescriptions() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Reformer.Data.DataProcessFlow.ProcessFlowItemDescriptions", typeof(ProcessFlowItemDescriptions).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Because there are only 6/8 sensors on a MANTIS tool, the data may be skewed with sensors on opposite sides showing an offset in one direction that does not exist on the physical tube. This centers the data to find the best-fit circle at each slice..
        /// </summary>
        internal static string CircleFitData {
            get {
                return ResourceManager.GetString("CircleFitData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import tube data into the program. This copies the original data files into a special file used during the analysis process stored within the inspection folder. The original data is untouched.
        ///
        ///To begin, select a folder. This will load a list of all files that can be loaded into this inspection. Remove any files you do not want to include in the analysis, if any, and then begin the import process..
        /// </summary>
        internal static string DataImporting {
            get {
                return ResourceManager.GetString("DataImporting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set inspection parameters before loading data. This allows default values for tube dimensions and color scales be set beforehand, along with storing inspection-wide data for display on exported items..
        /// </summary>
        internal static string InspectionInput {
            get {
                return ResourceManager.GetString("InspectionInput", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Physical dimensions and limitations of hardware prevent the entire tube surface from being inspected. This allows plots and drawings to show blank space where data wasn&apos;t gathered from the tube..
        /// </summary>
        internal static string TubeAxialAdjustment {
            get {
                return ResourceManager.GetString("TubeAxialAdjustment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set the parameters used to draw each tube and place them in the proper location on exported drawings..
        /// </summary>
        internal static string TubeInput {
            get {
                return ResourceManager.GetString("TubeInput", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Slight offets may occur when reading data, so this provides the capability to adjust the cold zone to match previous readings or measured data..
        /// </summary>
        internal static string TubeRadialAdjustment {
            get {
                return ResourceManager.GetString("TubeRadialAdjustment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selecting welds allows for sections to be determined in software so identified locations can more easily be found by customers..
        /// </summary>
        internal static string WeldSelection {
            get {
                return ResourceManager.GetString("WeldSelection", resourceCulture);
            }
        }
    }
}
