﻿
using System.ComponentModel;
using System.Linq;
using NGenerics.Extensions;
using Reformer.Data.InspectionFile;
using Reformer.Forms.DataProcessFlow;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.AxialAdjustmentProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.EddyCurrentExamination;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.FullReformerViewProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.RadialAdjustmentProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.RefMaxSelectionProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.TubeLayoutMapProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSelectionProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.WeldSnapProcess;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.XyExport;
using Reformer.WindowManagement;

namespace Reformer.Data.DataProcessFlow
{
    public class ProcessFlowList
    {
        public string FlowName { get; set; }
        public BindingList<BaseProcessFlowItem> FlowList { get; set; }

        /// <summary> Checks a deserialized process flow to make sure any changes are accounted for without losing progress indicators where possible. </summary>
        public static ProcessFlowList UpdateProcesses(ProcessFlowList OldProcessFlow)
        {
            ProcessFlowList newProcess;
            switch (OldProcessFlow.FlowName)
            {
                case ProcessFlowLists.NewMANTISInspection: newProcess = ProcessListCreator.NewMANTISInspection; break;
                case ProcessFlowLists.AgedMANTISInspection: newProcess = ProcessListCreator.AgedMANTISInspection; break;
                case ProcessFlowLists.NewLOTISInspection: newProcess = ProcessListCreator.NewLOTISInspection; break;
                default: newProcess = ProcessListCreator.AgedLOTISInspection; break;
            }
            return MergeProcesses(OldProcessFlow, newProcess);
        }

        /// <summary> Merges an old and new process flow. Only keeps progress from old process if names match new process. </summary>
        private static ProcessFlowList MergeProcesses(ProcessFlowList OldProcess, ProcessFlowList NewProcess)
        {
            foreach (var step in OldProcess.FlowList.Where(step => step.IsComplete))
            {
                NewProcess.FlowList.Where(P => P.Name == step.Name).ForEach(P => P.IsComplete = true);
            }
            return NewProcess;
        }

        public static void SetProcessFlowList(ReformerInspection insp)
        {
            ProcessFlowList newProcess;
            if (insp.InspectionTool == Tool.LOTIS)
            { newProcess = insp.ReformerInfo.NewTubes ? 
                ProcessListCreator.NewLOTISInspection : 
                ProcessListCreator.AgedLOTISInspection; }
            else
            { newProcess = insp.ReformerInfo.NewTubes ? 
                ProcessListCreator.NewMANTISInspection : 
                ProcessListCreator.AgedMANTISInspection; }
            insp.ProcessFlow = newProcess;
            var processListForm = ReformerWindowManager.Instance.GetFormsByType(typeof(DataProcessFlowForm), true);
            if (processListForm.Count > 0) ((DataProcessFlowForm)processListForm[0]).RefreshProcessFlow();
        }
    }

    public static class ProcessListCreator
    {
        public static ProcessFlowList AgedMANTISInspection { get { return CreateMANTISInspectionList(false); } }
        public static ProcessFlowList AgedLOTISInspection { get { return CreateLOTISInspectionList(false); } }
        public static ProcessFlowList NewMANTISInspection { get { return CreateMANTISInspectionList(true); } }
        public static ProcessFlowList NewLOTISInspection { get { return CreateLOTISInspectionList(true); } }

        private static ProcessFlowList CreateMANTISInspectionList(bool NewTubes)
        {
            ProcessFlowList newProcess = new ProcessFlowList();
            newProcess.FlowList = new BindingList<BaseProcessFlowItem>
            {
                new BaseProcessFlowItem("Set Inspection Parameters", ProcessFlowItemDescriptions.InspectionInput, typeof (InspectionInput)),
                new BaseProcessFlowItem("Load Inspection Data", ProcessFlowItemDescriptions.DataImporting, typeof (DataImporting)),
                new BaseProcessFlowItem("AutoCenter MANTIS Data", ProcessFlowItemDescriptions.CircleFitData, typeof (CircleFitData)),
                new BaseProcessFlowItem("Set Tube Parameters", ProcessFlowItemDescriptions.TubeInput, typeof (TubeInput)),
                new BaseProcessFlowItem("Account for Missed Data", ProcessFlowItemDescriptions.TubeAxialAdjustment, typeof (TubeAxialAdjustment)),
                new BaseProcessFlowItem("Pick Welds", ProcessFlowItemDescriptions.WeldSelection, typeof (WeldSelection)),
                new BaseProcessFlowItem("Adjust Diameter Readings", ProcessFlowItemDescriptions.TubeRadialAdjustment, typeof (TubeRadialAdjustment)),
                new BaseProcessFlowItem("Pick Ref/Max Points", "Select Reference and Max Diameter points.", typeof(RefMaxSelection)),
                new BaseProcessFlowItem("Import Eddy Current Data", "If Eddy Current data is being examined, import it here.", typeof(EddyCurrentImport)),
                new BaseProcessFlowItem("Examine Eddy Current Data", "If Eddy Current data is being examined, examine it here.", typeof(EddyCurrentExamine)),
                new BaseProcessFlowItem("Convert OD to ID", "Convert Outer Diameter to Inner Diameter.", typeof(ODtoID)),
                new BaseProcessFlowItem("Export XY Plots", "Create images of the XY Plots.", typeof(XyExport)),
                new BaseProcessFlowItem("Export Single Tube 3D Visuals", "Create images of the 3D data.", typeof(Single3DExport)),
                NewTubes 
                ? new BaseProcessFlowItem("Export Baseline Table", "Create an Excel spreadsheet of the Baseline assessment table.", typeof(BaselineTable))
                : new BaseProcessFlowItem("Export Feature Table", "Create an Excel spreadsheet of the Level 1 assessment table.", typeof(FeatureTable)), 
                new BaseProcessFlowItem("Export Tube Layout Maps", "Export the AutoSketch equivalents.", typeof(TubeLayoutMap)),
                new BaseProcessFlowItem("Export Full 3D Reformer Views", "Export the Full 3D Reformer displays.", typeof(FullReformerView)),
                new BaseProcessFlowItem("*Export CSV's", "Export CSV's for use in legacy software or for customer use.", typeof(CSVExport)),
                new BaseProcessFlowItem("*Add/Delete/Import/Export Inspection", "Allows Inspection files (.Qiir) to be created, imported, or exported for use in this or other projects.", typeof(QiirImportExport)),
            };
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Count > 1)
                newProcess.FlowList.Insert(6, new BaseProcessFlowItem("Snap Welds", "Matches positions to a reference inspection.", typeof(SnapWelds)));

            newProcess.FlowName = NewTubes ? ProcessFlowLists.NewMANTISInspection : ProcessFlowLists.AgedMANTISInspection ;
            return newProcess;
        }

        private static ProcessFlowList CreateLOTISInspectionList(bool NewTubes)
        {
            ProcessFlowList newProcess = new ProcessFlowList();
            newProcess.FlowList = new BindingList<BaseProcessFlowItem>
            {
                new BaseProcessFlowItem("Set Inspection Parameters", ProcessFlowItemDescriptions.InspectionInput, typeof (InspectionInput)),
                new BaseProcessFlowItem("Load Data", ProcessFlowItemDescriptions.DataImporting, typeof (DataImporting)),
                new BaseProcessFlowItem("Set Tube Parameters", ProcessFlowItemDescriptions.TubeInput, typeof (TubeInput)),
                new BaseProcessFlowItem("Account for Missing Data", ProcessFlowItemDescriptions.TubeAxialAdjustment, typeof (TubeAxialAdjustment)),
                new BaseProcessFlowItem("Pick Welds", "Select Welds and verify their names", typeof (WeldSelection)),
                new BaseProcessFlowItem("Adjust Diameter Readings", ProcessFlowItemDescriptions.TubeRadialAdjustment, typeof (TubeRadialAdjustment)),
                new BaseProcessFlowItem("Pick Ref/Max Points", "Select Reference and Max Diameter points.", typeof(RefMaxSelection)),
                new BaseProcessFlowItem("Export XY Plots", "Create images of the XY Plots.", typeof(XyExport)),
                new BaseProcessFlowItem("Export Single Tube 3D Visuals", "Create images of the 3D data.", typeof(Single3DExport)),
                NewTubes 
                ? new BaseProcessFlowItem("Export Baseline Table", "Create an Excel spreadsheet of the Baseline assessment table.", typeof(BaselineTable))
                : new BaseProcessFlowItem("Export Feature Table", "Create an Excel spreadsheet of the Level 1 assessment table.", typeof(FeatureTable)),
                new BaseProcessFlowItem("Export Tube Layout Maps", "Export the AutoSketch equivalents.", typeof(TubeLayoutMap)),
                new BaseProcessFlowItem("Export Full 3D Reformer Views", "Export the Full 3D Reformer displays.", typeof(FullReformerView)),
                new BaseProcessFlowItem("Import Eddy Current Data", "Import the eddy current data.", typeof(EddyCurrentImport)),
                new BaseProcessFlowItem("Analyze Eddy Current Data", "Analyze the eddy current data.", typeof(EddyCurrentExamine)),
                new BaseProcessFlowItem("*Export CSV's", "Export CSV's for use in legacy software or for customer use.", typeof(CSVExport)),
                new BaseProcessFlowItem("*Add/Delete/Import/Export Inspection", "Allows Inspection files (.Qiir) to be created, imported, or exported for use in this or other projects.", typeof(QiirImportExport)),
            };
            if (LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Count > 1)
                newProcess.FlowList.Insert(5, new BaseProcessFlowItem("Snap Welds", "Matches positions to a reference inspection.", typeof(SnapWelds)));

            newProcess.FlowName = NewTubes ? ProcessFlowLists.NewLOTISInspection : ProcessFlowLists.AgedLOTISInspection;
            return newProcess;
        }
    }

    public static class ProcessFlowLists
    {
        public const string AgedMANTISInspection = "AgedMANTISInspection";
        public const string NewMANTISInspection = "NewMANTISInspection";
        public const string AgedLOTISInspection = "AgedLOTISInspection";
        public const string NewLOTISInspection = "NewLOTISInspection";
    }
}
