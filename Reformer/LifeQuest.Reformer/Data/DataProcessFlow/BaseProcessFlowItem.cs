﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using Reformer.Annotations;

namespace Reformer.Data.DataProcessFlow
{
    public class BaseProcessFlowItem: INotifyPropertyChanged
    {
        private bool _isComplete;

        /// <summary> Purposefully kept protected to keep outside things from using a parameterless constructor. </summary>
        protected BaseProcessFlowItem() { }

        public BaseProcessFlowItem(string name, string description, Type processWindowType)
        {
            Name = name;
            Description = description;
            ProcessWindowType = processWindowType;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }

        [XmlIgnore]
        public Type ProcessWindowType { get; set; }

        public bool IsComplete
        {
            get { return _isComplete; }
            set
            {
                if (value.Equals(_isComplete)) return;
                _isComplete = value;
                OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged Methods

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion INotifyPropertyChanged Methods
    }
}
