﻿using System;
using System.IO;

namespace Reformer.Data
{
    public class UsefulDirectories
    {
        /// <summary>
        /// returns the default project output directory, and creates it if it doesn't exist yet.
        /// </summary>
        public static DirectoryInfo Reporting
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(LifeQuestReformer.Instance.TheDataManager.CurrentProject.DirectoryPath, "ReportingOutput"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo Graphics
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Reporting.FullName, "Graphics"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo ThreeDImage
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Graphics.FullName, "3D Tube Image"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo DiameterGraphs
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Graphics.FullName, "Diameter Graphs"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo Tables
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Graphics.FullName, "Tables"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo OtherImages
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Graphics.FullName, "Other Images"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo FullReformer
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(Graphics.FullName, "3D Reformer Model"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo TemplateDirectory
        {
            get
            {
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(CommonApplicationDataPath.FullName, "Templates"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo Materials
        {
            get
            {
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(CommonApplicationDataPath.FullName, "Materials"));
                if (!dir.Exists) dir.Create();
                return dir;
            }
        }

        public static DirectoryInfo CommonApplicationDataPath
        {
            get
            {
                DirectoryInfo programDir = new DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "LQR"));
                if (!programDir.Exists) programDir.Create();
                return programDir;
            }
        }

        /// <summary> The default directory to load data from. Does not create it if it doesn't exist, so it may not exist. </summary>
        public static DirectoryInfo DefaultLoadDirectory
        {
            get
            {
                string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                DirectoryInfo path = new DirectoryInfo(Path.Combine(myDocs, "LifeQuest Reformer"));
                return path;
            }
        }
    }
}
