﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Data;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Features;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;

namespace Reformer.Data.Tubes
{
    public class ReformerTube : PipeSectionInfo, IComparable<ReformerTube>
    {
        private ReformerData _reformerAttributes;
        private ManufacturingSpecs _specs;
        
        [Obsolete("Do not use paramaterless constructor, it only exists for XML Serialization", true)]
        public ReformerTube() { }

        public ReformerTube(ReformerInspection ParentInspection): base(ParentInspection)
        {
            //Make a new feature list for the tube and make it default-sorted by start position.
            Features = new BindingList<ReformerFeatureInfo>();
        }

        public double StartPosition => Positions.First();

        public double StartPositionInDisplayUnits => Inspection.Units.LengthUnitSystem.Convert(StartPosition, DisplayUnits.Instance.AxialDistanceUnits.Scale);

        public double EndPosition => Positions.Last();

        public double EndPositionInDisplayUnits => Inspection.Units.LengthUnitSystem.Convert(EndPosition, DisplayUnits.Instance.AxialDistanceUnits.Scale);

        public double Length => EndPosition - StartPosition;

        public double LengthInDisplayUnits => EndPositionInDisplayUnits - StartPositionInDisplayUnits;

        /// <summary> Determines whether the data has been flipped or not. </summary>
        public bool AxiallyFlipped { get; set; }

        /// <summary>  Dynamically finds the inspection that this objects belongs to if it's null (first lookup) </summary>
        [XmlIgnore]
        public new ReformerInspection Inspection => (ReformerInspection)base.Inspection;

        public ReformerData ReformerAttributes 
        {
            //Attempting to shorten this to a ?? will cause it to fail occasionally inside of Linq ForEach loops.
            get { if (_reformerAttributes == null) _reformerAttributes = new ReformerData(this); return _reformerAttributes; }
            set { _reformerAttributes = value; }
        }

        public ManufacturingSpecs Specs
        {
            get { return _specs ?? (_specs = new ManufacturingSpecs(this)); }
            set { _specs = value; }
        }

        /// <summary> gathers a list of position data for the tube  </summary>
        [XmlIgnore]
        public List<float> Positions
        {
            get
            {
                List<float> positions;
                this.GetPositions(out positions);
                return positions;
            }
        }

        /// <summary> Gets an integer representing this tube's location in a list of tubes ordered by Row# then Tube#, breaks down after 10000 tubes in a row. </summary>
        public int OrderIndex => ReformerAttributes.RowNumber * 10000 + ReformerAttributes.TubeNumber;

        public BindingList<ReformerFeatureInfo> Features { get; set; }

        internal List<ReformerScalar> AvailableScalars => ReformerHdf5Helper.GetAvailableScalars(this);

        public int CompareTo(ReformerTube other)
        {
            if (OrderIndex > other.OrderIndex) return 1;
            if (OrderIndex == other.OrderIndex) return 0;
            return -1;
        }

        protected override QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo.InspectionFile GetParentInspection()
        {
            return LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.First(Insp => Insp.GUID == ParentInspectionGuid);
        }
        
    }
}
