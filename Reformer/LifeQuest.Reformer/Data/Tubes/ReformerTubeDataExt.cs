﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.IO;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Features;
using Reformer.Data.Scalars;

namespace Reformer.Data.Tubes
{
    public static class ReformerTubeDataExt
    {
        #region Data Retrieval

        public static float GetScalarStatisticalValue(this ReformerTube tube, ReformerScalar scalar, StatisticalMeasures stats, int sliceIndex)
        {
            return tube.Get2DScalarStatisticalData(scalar, sliceIndex, sliceIndex, stats).First().Value[0];
        }

        /// <summary> Gets all position data for the tube </summary>
        public static List<float> GetPositionsSubset(this ReformerTube theTube, int sliceIndexStart, int sliceIndexEnd)
        {
            return ReformerHdf5Helper.Read1DTubeDataSubset(theTube, ReformerScalar.AxialPosition, sliceIndexStart, sliceIndexEnd);
        }

        /// <summary> Gets all position data for the tube </summary>
        public static void GetPositions(this ReformerTube theTube, out List<float> outputPosition)
        {
            ReformerHdf5Helper.Read1DTubeData(theTube, ReformerScalar.AxialPosition, out outputPosition);
        }

        /// <summary> Gets position data for the tube averaged down by a sample factor. IE: 1,2,3 with a sample factor of 1 would be 2 </summary>
        public static void GetReducedPositions(this ReformerTube theTube, float sampleFactor, out List<float> outputPosition)
        {
            ReformerHdf5Helper.Read1DTubeData(theTube, ReformerScalar.AxialPosition, out outputPosition);
            outputPosition = ReduceList(outputPosition, sampleFactor);
        }

        /// <summary> Gets position data for the tube averaged by a sample factor. IE: 1,3 with a sample factor of 1 would be 1,2,3 </summary>
        public static void GetExpandedPositions(this ReformerTube theTube, float sampleFactor, out List<float> outputPosition)
        {
            ReformerHdf5Helper.Read1DTubeData(theTube, ReformerScalar.AxialPosition, out outputPosition);
            outputPosition = ExpandList(outputPosition, sampleFactor);
        }

        /// <summary> Gets all position data for the tube </summary>
        public static void GetAxiallyReducedSensorData(this ReformerTube theTube, ReformerScalar scalar, int sensorNum, float sampleFactor, out List<float> sampledData)
        {
            theTube.Get2DScalarForSensor(scalar, sensorNum, out sampledData);
            sampledData = ReduceList(sampledData, sampleFactor);
        }

        public static void GetAxiallyReducedTubeStatisticalData(ReformerTube tube, ReformerScalar scalar, StatisticalMeasures stats, float sampleFactor, out List<float> positions, out List<float> reducedData)
        {
            tube.GetReducedPositions(sampleFactor, out positions);
            var fullDataList = tube.Get2DScalarStatisticalData(scalar, stats).First().Value;
            reducedData = ReduceList(fullDataList, sampleFactor);
        }

        public static void GetAxiallyReducedData(ReformerTube tube, ReformerScalar scalar, float sampleFactor, out List<float> positions, out float[,] reducedData)
        {
            tube.GetReducedPositions(sampleFactor, out positions);

            reducedData = new float[positions.Count, tube.Inspection.NumRadialReadings];
            for (int sensorIdx = 0; sensorIdx < tube.Inspection.NumRadialReadings; sensorIdx++)
            {
                List<float> subSampledSensorData;
                tube.GetAxiallyReducedSensorData(scalar, sensorIdx + 1, sampleFactor, out subSampledSensorData);
                for (int slice = 0; slice < positions.Count; slice++)
                {
                    reducedData[slice, sensorIdx] = subSampledSensorData[slice];
                }
            }
        }

        /// <summary> Reduces a list by replacing multiple data points with their average. a reduction factor of 1 reduces a list to 50% or original,  </summary>
        private static List<float> ReduceList(List<float> inList, float pointsRemovedPerOriginal)
        {
            List<float> newList = new List<float>();
            
            if (pointsRemovedPerOriginal < 1)
            {
                int skippingCounter = 1;
                int wholeNumberSkips = (int)Math.Floor(1 / pointsRemovedPerOriginal);
                double decimalRem = 1 / pointsRemovedPerOriginal - Math.Floor(1 / pointsRemovedPerOriginal);
                for (int i = 0; i < inList.Count - 1; i++)
                {
                    if (i % wholeNumberSkips == 0) // then we need to do some reducing here and combine a point.
                    {
                        bool skipOneMoreDueToRounding = (int)Math.Round(decimalRem * skippingCounter - Math.Floor(decimalRem * skippingCounter)) == 1;
                        if (skipOneMoreDueToRounding)
                        {
                            newList.Add(inList[i]);
                            i++;
                        }
                        //Add the average of two points unless this is the last point.
                        newList.Add(i < inList.Count - 1 ? inList.GetRange(i, 1).Average() : inList[i]);
                        skippingCounter++;
                    }
                    else //We won't be reducing at this point, so add the original value.
                    {
                        newList.Add(inList[i]);
                    }
                }
            }
            else //Add subrange-averages to the new list over the course of the data
            {
                for (float i = 0; i < inList.Count; i += pointsRemovedPerOriginal)
                {
                    int startIdx = (int)Math.Floor(i);
                    int endIdx = (int)Math.Floor(Math.Min(inList.Count - 1, i + pointsRemovedPerOriginal));
                    newList.Add(inList.GetRange(startIdx, endIdx - startIdx + 1).Average());
                }
            }

            return newList;
        }

        /// <summary> Expands a list by adding equally spaced points between each original data point. </summary>
        private static List<float> ExpandList(List<float> inList, float pointsAddedPerOriginal)
        {
            List<float> newList = new List<float>();
            
            int additionalPointsPerOriginal = (int)Math.Floor(pointsAddedPerOriginal);
            int extraAdditionalPointEvery = (int)Math.Ceiling(1 / (pointsAddedPerOriginal % 1));
                 
            for (int i = 0; i < inList.Count - 1; i++)
            {
                newList.Add(inList[i]);
                float endSliceValue = inList[i + 1];
                float startSliceValue = inList[i];
                int extraPointsThisSlice = additionalPointsPerOriginal + (i % extraAdditionalPointEvery == 0 ? 1 : 0);
                for (int addedPoint = 1; addedPoint <= extraPointsThisSlice; addedPoint++)
                {
                    float startPercent = (float)addedPoint / (extraPointsThisSlice + 1);
                    float endPercent = 1 - startPercent;
                    float newValue = startSliceValue * startPercent + endSliceValue * endPercent;
                    inList.Add(newValue);
                }
            }
            
            return newList;
        }

        /// <summary> Sets all position data for the tube </summary>
        public static void SetPositions(this ReformerTube theTube, List<float> positions)
        {
            ReformerHdf5Helper.SetTubeData(theTube, ReformerScalar.AxialPosition, positions);
        }

        public static void Get2DScalarData(this ReformerTube theTube, ReformerScalar theScalar, out float[,] outputData)
        {
            ReformerHdf5Helper.Read2DTubeData(theTube, theScalar, out outputData);
        }

        public static void Get2DScalarData(this ReformerTube theTube, ReformerScalar theScalar, out float[] outputData)
        {
            ReformerHdf5Helper.Read2DTubeData(theTube, theScalar, out outputData);
        }

        public static Dictionary<string, List<float>> Get2DScalarStatisticalData(this ReformerTube theTube, ReformerScalar theScalar, StatisticalMeasures stats)
        {
            if (!theTube.AvailableScalars.Contains(theScalar)) return null;
            float[] rawData;
            ReformerHdf5Helper.Read2DTubeData(theTube, theScalar, out rawData);
            ScalarArray<float> theScalarArray = new ScalarArray<float>(theScalar);
            theScalarArray.AddRange(rawData);
            var rawResults = new Dictionary<string, IScalarArray>();
            theScalarArray.Calc1DStats(stats, theTube.Inspection.NumRadialReadings, rawResults);
            return rawResults.ToDictionary(result => result.Key, result => (from ValueType v in result.Value select v.ToSingle()).ToList());
        }

        public static Dictionary<string, List<float>> Get2DScalarStatisticalData(this ReformerTube theTube, ReformerScalar theScalar, int startIndex, int endIndex, StatisticalMeasures stats)
        {
            if (!theTube.AvailableScalars.Contains(theScalar)) return null;
            float[,] rawData;
            ReformerHdf5Helper.GetTubeDataSubset(theTube, startIndex, endIndex, 0, theTube.Inspection.NumRadialReadings - 1, theScalar, out rawData);
            ScalarArray<float> theScalarArray = new ScalarArray<float>(theScalar);
            theScalarArray.AddRange(HDF5Helper.Unwind2DDataTo1D(rawData));
            var rawResults = new Dictionary<string, IScalarArray>();
            theScalarArray.Calc1DStats(stats, theTube.Inspection.NumRadialReadings, rawResults);
            return rawResults.ToDictionary(result => result.Key, result => (from ValueType v in result.Value select v.ToSingle()).ToList());
        }

        /// <summary> Gets the sensor data out of 2D scalar data. Assumes 1-based sensor number. </summary>
        public static void Get2DScalarForSensor(this ReformerTube theTube, ReformerScalar theScalar, int sensorNumber, out List<float> outputData)
        {
            float[,] allData;
            ReformerHdf5Helper.Read2DTubeData(theTube, theScalar, out allData);
            outputData = new List<float>(allData.GetLength(1));
            for (int i = 0; i < allData.GetLength(0); i++) { outputData.Add(allData[i, sensorNumber - 1]); }
        }

        /// <summary> Get the dimensions of scalar data. Always rank 2, even if something like position data with only one number per axial location. </summary>
        public static long[] GetDataDimensions(this ReformerTube tube, ReformerScalar theScalar)
        {
           return ReformerHdf5Helper.GetDataDimensions(tube.Inspection.DataFileFullName, tube, theScalar);
        }

        #endregion Data Retrieval

        #region Data Writing

        public static void Set2DScalarData(this ReformerTube theTube, ReformerScalar theScalar, float[,] data) { ReformerHdf5Helper.SetTubeData(theTube, theScalar, data); }
        public static void Set2DSensorData(this ReformerTube theTube, ReformerScalar theScalar, int sensor, List<float> data) { ReformerHdf5Helper.SetTubeSensorData(theTube, theScalar, sensor, data); }

        #endregion

        #region Data Adjustments

        /// <summary> Flips the axial data around the center point and shifts it to start at the current start offset </summary>
        public static void FlipAxialData(this ReformerTube tube)
        {
            List<float> positions;
            tube.GetPositions(out positions);

            //Flip around 0 by multiplying -1
            for (int i = 0; i < positions.Count; i++) { positions[i] = positions[i] * -1; }
            
            //Reverse the order after flipping around 0
            positions.Reverse();

            //shift to start at 0 again
            float tmpMinPos = positions.Min();
            for (int i = 0; i < positions.Count; i++) { positions[i] = positions[i] - tmpMinPos; }

            //Now all the data arrays need to be reversed as well;
            foreach (ReformerScalar scalar in tube.AvailableScalars.Where(s => s != ReformerScalar.AxialPosition))
            {
                float[,] originalData;
                tube.Get2DScalarData(scalar, out originalData);
                
                int maxSliceIdx = originalData.GetLength(0) - 1;
                int maxSensorIdx = originalData.GetLength(1) - 1;

                float[,] flippedData = new float[originalData.GetLength(0), originalData.GetLength(1)];
                Parallel.For(0, maxSliceIdx + 1, slice => 
                {
                    for (int sensor = 0; sensor <= maxSensorIdx; sensor++)
                    {
                        flippedData[maxSliceIdx - slice, maxSensorIdx - sensor] = originalData[slice, sensor];
                    }
                });
                tube.Set2DScalarData(scalar, flippedData);
            }
            
            //shift the data to start at the current start offset
            float minPos = positions.Min();
            float tubeOffset = (float)tube.Specs.MissedStart;
            for (int i = 0; i < positions.Count; i++) { positions[i] = positions[i] - minPos + tubeOffset; }

            tube.SetPositions(positions);
        }

        /// <summary> Flips the axial data around the center point and shifts it to start at the current start offset </summary>
        public static void FlipEddyAxialData(this ReformerTube tube)
        {
            List<float> eddyAxialData; 
            bool lotis = tube.Inspection.InspectionTool == Tool.LOTIS;
            if (lotis)
                tube.Get2DScalarForSensor(ReformerScalar.LotisEddyData, LOTISEddySensorMap.AxialPosition, out eddyAxialData);
            else
                tube.Get2DScalarForSensor(ReformerScalar.EddyData, LOTISEddySensorMap.AxialPosition, out eddyAxialData);
            
            float centerPos = (eddyAxialData.Max() - eddyAxialData.Min()) / 2;
            int axialSensorIndex = MANTISEddySensorMap.AxialPosition - 1;

            float[,] originalData;
            if (lotis)
                tube.Get2DScalarData(ReformerScalar.LotisEddyData, out originalData);
            else
                tube.Get2DScalarData(ReformerScalar.EddyData, out originalData);
            int numSlices = originalData.GetLength(0);
            int numSensors = originalData.GetLength(1);
            float[,] flippedData = new float[numSlices, originalData.GetLength(1)];
            for (int slice = 0; slice < originalData.GetLength(0); slice++)
            {
                for (int sensor = 0; sensor < numSensors; sensor++)
                {
                    //Reverse the data
                    flippedData[slice, sensor] = originalData[numSlices - slice - 1, sensor];
                }
                //flip the axial position around the mean position.
                flippedData[slice, axialSensorIndex] = centerPos + (centerPos - flippedData[slice, axialSensorIndex]);
            }
            if (tube.Inspection.InspectionTool == Tool.LOTIS)
                tube.Set2DScalarData(ReformerScalar.LotisEddyData, flippedData);
            else
                tube.Set2DScalarData(ReformerScalar.EddyData, flippedData);
        }

        public static void ShiftAxialData(this ReformerTube theTube, double shiftAmount)
        {
            List<float> positions;
            theTube.GetPositions(out positions);
            for (int i = 0; i < positions.Count; i++) positions[i] = positions[i] + (float)shiftAmount;
            theTube.SetPositions(positions);
        }

        public static void ShiftRadialData(this ReformerTube theTube, ReformerScalar theScalar, double shiftAmount)
        {
            float[,] data;
            theTube.Get2DScalarData(theScalar, out data);

            int numSlices = data.GetLength(0);
            int numSensor = data.GetLength(1);
            for (int slice = 0; slice < numSlices; slice++)
            {
                for (int sensor = 0; sensor < numSensor; sensor++)
                {
                    data[slice, sensor] = (float)(data[slice, sensor] + shiftAmount);
                }
            }
            theTube.Set2DScalarData(theScalar, data);
        }

        public static void ShiftRadialSensorData(this ReformerTube theTube, ReformerScalar theScalar, int sensor, double shiftAmount)
        {
            List<float> data;
            theTube.Get2DScalarForSensor(theScalar, sensor, out data);

            for (int i = 0; i < data.Count; i++) data[i] = (float)(data[i] + shiftAmount);
            theTube.Set2DSensorData(theScalar, sensor, data);
        }

        public static void SnapWelds(this ReformerTube thisTube, ReformerTube refTube)
        {
            //Start from the start (left side) and go towards the end (right side)
            List<ReformerFeatureInfo> myWelds = thisTube.Features.Where(f => f.Type == ReformerFeatureType.Weld).OrderBy(f => f.SliceIndexStart).ToList();
            List<ReformerFeatureInfo> refWelds = refTube.Features.Where(f => f.Type == ReformerFeatureType.Weld).OrderBy(f => f.SliceIndexStart).ToList();
            if (myWelds.Count == 0 || refWelds.Count == 0) return;

            ReformerFeatureInfo previousMatch = null;
            foreach (ReformerFeatureInfo weld in myWelds)
            {
                var refWeld = refWelds.FirstOrDefault(f2 => f2.Label == weld.Label);
                if (refWeld == null) continue;

                //Shift the first match, and stretch the rest
                int lockStartSlice = previousMatch == null ? 0 : previousMatch.SliceIndexStart;
                thisTube.StretchAxialData(lockStartSlice, weld, refWeld.StartPosition);
                previousMatch = weld;
            }

            //Also snap the last slice of data to the known end position based off of the tube's designed length and the 'missed end' value.
            var lastWeld = myWelds.Last();
            double endPosition = thisTube.Specs.MissedStart + thisTube.Length;
            thisTube.StretchAxialData(lastWeld.SliceIndexStart, thisTube.Positions.Count - 1, endPosition);
        }

        public static void StretchAxialData(this ReformerTube thisTube, int lockedStartSlice, ReformerFeatureInfo featureToShift, double newPosition)
        {
            thisTube.StretchAxialData(lockedStartSlice, featureToShift.SliceIndexStart, newPosition);
        }

        public static void StretchAxialData(this ReformerTube thisTube, int lockedStartSlice, int targetSliceToGrabAndStretchToNewPosition, double newPosition)
        {
            List<float> positions;
            thisTube.GetPositions(out positions);

            //Get a deep copy of the data we want to mess with
            List<double> positionsToAdjust = positions.GetRange(lockedStartSlice, positions.Count - lockedStartSlice).DeepCopy().Select(p => (double)p).ToList();
            int numStretchedPoints = targetSliceToGrabAndStretchToNewPosition - lockedStartSlice + 1;
            double diff = newPosition - thisTube.GetPositionsSubset(targetSliceToGrabAndStretchToNewPosition, targetSliceToGrabAndStretchToNewPosition).First();
            double diffPerSlice = diff / (positionsToAdjust.GetRange(0, numStretchedPoints).Distinct().Count());
            double sumOfChange = 0;

            if (positionsToAdjust.Count < 2) return; //just in case.
            for (int i = 1; i < positionsToAdjust.Count; i++)
            {
                double prevPos = positionsToAdjust[i - 1];
                //only increment if non-unique value
                if (!prevPos.IsWithinToleranceOf(positionsToAdjust[i], .0001))
                    sumOfChange += diffPerSlice;
                positionsToAdjust[i] += sumOfChange;
            }

            for (int i = lockedStartSlice; i < positions.Count; i++)
            {
                positions[i] = (float)positionsToAdjust[i - lockedStartSlice];
            }
            positions.Sort(); //without this sort, sometimes the axial data will be wonky at the .0001 level due to float rounding in high-res files (LOT). No harm in sorting anyway.
            thisTube.SetPositions(positions);
        }

        #endregion Data Adjustments
    }
}
