﻿using System;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Numeric;

namespace Reformer.Data.Tubes
{
    /// <summary>
    /// A helper class to define tolerances set by the manufacturer for tubes.
    /// </summary>
    public class ManufacturingSpecs: TubeAttachment
    {
        private decimal _yearsSincePreviousInspection;
        private int _yearsUntilNextInspection;
       
        [Obsolete("Only exists for XML Serialization. Do not use parameterless constructor", true)]
        public ManufacturingSpecs() { } 

        public ManufacturingSpecs(ReformerTube Parent) : base(Parent)
        {
            SetDefaults();
        }

        private void SetDefaults()
        {
            ODTolDown = 0f; ODTolUp = .079f;
            IDTolDown = -.039f; IDTolUp = 0f;
            DownFlow = true;
        }

        #region public properties

        public double IDTolUp { get; set; }
        public double IDTolDown { get; set; }
        public double ODTolUp { get; set; }
        public double ODTolDown { get; set; }

        public double DiameterInside { get; set; }
        public double DiameterOutside { get; set; }
        public double MissedStart { get; set; }
        public double MissedEnd { get; set; }
        public double RadialShift { get; set; }

        public double ThicknessWall { get { return (DiameterOutside - DiameterInside) / 2; } }
        public double RadiusInside { get { return DiameterInside / 2; } }
        public double RadiusOutside { get { return DiameterOutside / 2; } }
        public bool DownFlow { get; set; }

        /// <summary> YearsUntilNextInspection must be between 1-6 years. Automatically constrained to these values as per procedure dealing with level 1 assessment. </summary>
        public int YearsUntilNextInspection
        {
            get { return _yearsUntilNextInspection.Constrain(1, 6); }
            set { _yearsUntilNextInspection = value.Constrain(1, 6); }
        }

        /// <summary> Age between 1-6 years. Automatically constrained to these values as per procedure dealing with level 1 assessment. </summary>
        public decimal YearsSinceLastInspection
        {
            get { return _yearsSincePreviousInspection.Constrain(1, 6); }
            set { _yearsSincePreviousInspection = value.Constrain(1, 6); }
        }

        #endregion public properties

        #region Display Units versions
        [XmlIgnore]
        public double DiameterOutsideInDisplayUnits
        { get { return Math.Round(DiameterOutside * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { DiameterOutside = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double DiameterInsideInDisplayUnits
        { get { return Math.Round(DiameterInside * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { DiameterInside = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double ThicknessWallInDisplayUnits
        { get { return Math.Round(ThicknessWall * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } }

        [XmlIgnore]
        public double ODTolUpDisplayUnits
        { get { return Math.Round(ODTolUp * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { ODTolUp = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double ODTolDownDisplayUnits
        { get { return Math.Round(ODTolDown * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { ODTolDown = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double IDTolUpDisplayUnits
        { get { return Math.Round(IDTolUp * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { IDTolUp = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double IDTolDownDisplayUnits
        { get { return Math.Round(IDTolDown * MeasDisplayFactor, DefaultValues.MeasNumDecimals); } set { IDTolDown = value / MeasDisplayFactor; } }

        [XmlIgnore]
        public double MissedStartDisplayUnits
        { get { return MissedStart * AxialDisplayFactor; } set { MissedStart = value / AxialDisplayFactor; } }

        [XmlIgnore]
        public double MissedEndDisplayUnits
        { get { return MissedEnd * AxialDisplayFactor; } set { MissedEnd = value / AxialDisplayFactor; } }

        /// <summary> Changed per Tim's request to always be the inspection's default length. Then Changed back to MissedStart+Length+MissedEnd </summary>
        [XmlIgnore]
        public double LengthDisplayUnits => Tube.Specs.MissedStartDisplayUnits + Tube.LengthInDisplayUnits + Tube.Specs.MissedEndDisplayUnits;

        /// <summary> Determines the axial encoder used for position data. MANTIS and LOTIS CSV files are 1, LOT files can be 1 or 2. </summary>
        public int AxialEncoder{ get;set;}

        /// <summary> Calculates the overall age. If no baseline tube exists, it will return 0 </summary>
        [XmlIgnore]
        public int OverallAge 
        { 
            get 
            {
                if (Tube.ReformerAttributes.OldestOrBaselineTube == null) return 0;
                return Tube.Inspection.DateInspected.Year - Tube.ReformerAttributes.OldestOrBaselineTube.Inspection.DateInspected.Year;
            }
        }

        #endregion
        
    }

    
}
