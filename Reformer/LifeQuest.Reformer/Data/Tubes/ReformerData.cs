﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using NGenerics.Extensions;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.Features;
using Reformer.Data.Materials;
using Reformer.Data.Scalars;
using Reformer.Forms.DataProcessFlow.DataProcessWindows.Level1TableProcess;

namespace Reformer.Data.Tubes
{
    [Serializable]
    public class ReformerData : TubeAttachment
    {
        private const double DefaultDouble = double.NaN;
        public const int DefaultInt = int.MinValue;
        private ReformerTube _oldestOrBaselineTube;
        private Guid _oldestOrBaselineTubeGUID;
        private ReformerTube _previousTube;
        private Guid _previousTubeGUID;

        #region Constructor

        [Obsolete("Do not use Parameterless constructor, needs TubeID to function. Only here for XML Serialization", true)]
        public ReformerData() { }

        public ReformerData(ReformerTube ParentTube) : base (ParentTube)
        {
        
        }

        #endregion Constructor

        #region Public, Calculated Properties

        [XmlIgnore]
        public ReformerTube OldestOrBaselineTube
        {
            get
            {
                //If this doesn't remember what tube it's associated with, go find it based on the GUID. This prevents circular references in XML, while also only requiring a lookup once.
                if (_oldestOrBaselineTube == null)
                { //Go through all of the inspections in the active project and search for the tube out of all the tubes in those inspections.
                    LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.ForEach(Insp =>
                    {
                        List<ReformerTube> matchingTubes = Insp.Tubes.Where(Pipe => Pipe.ID == OldestOrBaselineTubeGUID).ToList();
                        if (matchingTubes.Count > 0) _oldestOrBaselineTube = matchingTubes.First();
                    });
                }
                if (_oldestOrBaselineTube == null) return null;
                return _oldestOrBaselineTube;
            }
        }

        public Guid OldestOrBaselineTubeGUID
        {
            get { return _oldestOrBaselineTubeGUID; }
            set
            {
                _oldestOrBaselineTubeGUID = value;
                _oldestOrBaselineTube = null; //reset the baseline tube. It'll be set on first lookup.
            }
        }

        [XmlIgnore]
        public ReformerTube PreviousTube
        {
            get
            {
                //If this doesn't remember what tube it's associated with, go find it based on the GUID. This prevents circular references in XML, while also only requiring a lookup once.
                if (_previousTube == null)
                { //Go through all of the inspections in the active project and search for the tube out of all the tubes in those inspections.
                    LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.ForEach(Insp =>
                    {
                        List<ReformerTube> matchingTubes = Insp.Tubes.Where(Pipe => Pipe.ID == PreviousTubeGUID).ToList();
                        if (matchingTubes.Count > 0) _previousTube = matchingTubes.First();
                    });
                }
                if (_previousTube == null) return null;
                return _previousTube;
            }
        }

        public Guid PreviousTubeGUID
        {
            get { return _previousTubeGUID; }
            set
            {
                _previousTubeGUID = value;
                _previousTube = null; //reset the baseline tube. It'll be set on first lookup.
            }
        }

        public int RowNumber { get; set; }
        public char RowLetter { get { return (Char)(65 + RowNumber - 1); } }
        public int TubeNumber { get; set; }
        public CreepCalcMethod CalcMethod { get; set; }
        
        /// <summary> Stores the level 1 material properties needed for basic reformer analysis. </summary>
        public ReformerMaterial Material { get; set; }

        /// <summary> Stores the Level 1 engineering result.  </summary>
        public RemainingLifeStatus Level1Result { get; set; }

        /// <summary> The creep damage indicator based off of the tube material </summary>
        public ReformerMaterial.CreepDamage CreepDamage { get; set; }

        /// <summary> The feature on this tube that represents the max point for calculating overall growth </summary>
        private ReformerFeatureInfo OverallGrowthMaxFeature { get {  return Tube.Features.FirstOrDefault(F => F.Type == ReformerFeatureType.OverallGrowthMax);} }

        /// <summary> The feature on this tube that represents the max point for calculating Level 1 growth </summary>
        private ReformerFeatureInfo Level1GrowthMaxFeature { get { return Tube.Features.FirstOrDefault(F => F.Type == ReformerFeatureType.Level1GrowthMax); } }

        /// <summary> The slice on the baseline tube that represents the ref point for overall growth. </summary>
        public int PickedOverallGrowthRefSlice { get; set; } = DefaultInt;

        /// <summary> The slice on the most recent inspection (or this inspection, if only 1 exists) that represents the ref point for level 1 growth. </summary>
        public int PickedLevel1GrowthRefSlice { get; set; } = DefaultInt;

        #region Overall Max/Ref Data Points
        #region OVerall OD Max/Ref Data

        // <summary> Finds the MaxOD pick value for a given tube if a Max picks exists, otherwise NaN </summary>
        [XmlIgnore] public double PickedOverallMaxODDisplayUnits 
        { 
            get
            {
                return OverallGrowthMaxFeature == null ? DefaultDouble
                    : Math.Round(GetDataPoint(Tube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, OverallGrowthMaxFeature.SliceIndexStart).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
            }
        }

        /// <summary> Returns the index of the MaxOD pick for this tube if it exists, otherwise int.Min </summary>
        [XmlIgnore] public int OverallPickedMaxODIndex
        {
            get
            {
                return OverallGrowthMaxFeature == null ? DefaultInt : OverallGrowthMaxFeature.SliceIndexStart;
            }
        }

        /// <summary> Finds the Level1RefOD pick for a given tube if it exists based on the creepCalcMethod, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedOverallRefODDisplayUnits
        {
            get
            {
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return Math.Round(GetDataPoint(Tube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, PickedOverallGrowthRefSlice).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
                    case CreepCalcMethod.AgedToBaseline:
                    case CreepCalcMethod.AgedToAged:
                        return Math.Round(GetDataPoint(OldestOrBaselineTube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, PickedOverallGrowthRefSlice).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
                }
                return DefaultDouble; //should never get here.
            }
        }

            #endregion OVerall OD Max/Ref Data
            #region Overall ID Data

        // <summary> Finds the OverallMaxID pick value for a given tube if a Max picks exists, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedOverallMaxIDDisplayUnits
        {
            get
            {
                return OverallGrowthMaxFeature == null ? DefaultDouble
                    : Math.Round(GetDataPoint(Tube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, OverallGrowthMaxFeature.SliceIndexStart).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
            }
        }

        /// <summary>  Finds the OverallMaxID pick's position for this tube if it exists, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedOverallMaxPositionDisplayUnits
        {
            get
            {
                return OverallGrowthMaxFeature == null ? DefaultDouble
                    : GetDataPoint(Tube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, OverallGrowthMaxFeature.SliceIndexStart).PositionInDisplayUnits;
            }
        }

        /// <summary> Returns the index of the MaxID pick for this tube if it exists, otherwise int.Min </summary>
        [XmlIgnore]
        public int PickedMaxIDIndex
        {
            get
            {
                return OverallGrowthMaxFeature == null ? DefaultInt : OverallGrowthMaxFeature.SliceIndexStart;
            }
        }

        /// <summary> Finds the Level1RefID pick for a given tube if it exists based on the creepCalcMethod, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedOverallRefIDDisplayUnits
        {
            get
            {
                if (PickedOverallGrowthRefSlice == DefaultInt) return DefaultDouble;
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return Math.Round(GetDataPoint(Tube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, PickedOverallGrowthRefSlice).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
                    case CreepCalcMethod.AgedToBaseline:
                    case CreepCalcMethod.AgedToAged:
                        return Math.Round(GetDataPoint(OldestOrBaselineTube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, PickedOverallGrowthRefSlice).ValueInDisplayUnits * 2, DefaultValues.MeasNumDecimals);
                }
                return DefaultDouble; //should never get here.
            }
        }

        [XmlIgnore]
        public double PickedOverallRefPositionDisplayUnits
        {
            get
            {
                if (PickedOverallGrowthRefSlice == DefaultInt) return DefaultDouble;
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return Tube.GetPositionsSubset(PickedOverallGrowthRefSlice, PickedOverallGrowthRefSlice).First() * DefaultValues.AxialBaseToDisplayUnits;
                    case CreepCalcMethod.AgedToBaseline:
                    case CreepCalcMethod.AgedToAged:
                        {
                            if (OldestOrBaselineTube == null) return DefaultDouble;
                            return OldestOrBaselineTube.GetPositionsSubset(PickedOverallGrowthRefSlice, PickedOverallGrowthRefSlice).First() * DefaultValues.AxialBaseToDisplayUnits;
                        }
                }
                return DefaultDouble; //should never get here.
            }
        }

            #endregion Overall ID Data
        #endregion Overall Growth Max/Ref Data

        #region Level 1 Max/Ref Data

        // <summary> Finds the MaxOD pick value for a given tube if a Max picks exists, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedLevel1MaxODDisplayUnits
        {
            get
            {
                return Level1GrowthMaxFeature == null ? DefaultDouble
                    : GetDataPoint(Tube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, Level1GrowthMaxFeature.SliceIndexStart).ValueInDisplayUnits * 2;
            }
        }

        /// <summary>  Finds the MaxOD pick's position for this tube if it exists, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedLevel1MaxODPositionDisplayUnits
        {
            get
            {
                return Level1GrowthMaxFeature == null ? DefaultDouble
                    : GetDataPoint(Tube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, Level1GrowthMaxFeature.SliceIndexStart).PositionInDisplayUnits;
            }
        }

        /// <summary> Finds the Level1RefOD pick for a given tube if it exists based on the creepCalcMethod, otherwise NaN </summary>
        [XmlIgnore] public double PickedLevel1RefODDisplayUnits 
        { 
            get 
            { 
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return GetDataPoint(Tube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                    case CreepCalcMethod.AgedToBaseline:
                        return GetDataPoint(OldestOrBaselineTube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                    case CreepCalcMethod.AgedToAged:
                        return GetDataPoint(PreviousTube, ReformerScalar.RadiusOutside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                }
                return DefaultDouble; //should never get here.
            }
        }

        [XmlIgnore]
        public double PickedLevel1RefPositionDisplayUnits
        {
            get
            {
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return Tube.GetPositionsSubset(PickedLevel1GrowthRefSlice, PickedLevel1GrowthRefSlice).First();
                    case CreepCalcMethod.AgedToBaseline:
                        return OldestOrBaselineTube.GetPositionsSubset(PickedLevel1GrowthRefSlice, PickedLevel1GrowthRefSlice).First();
                    case CreepCalcMethod.AgedToAged:
                        return PreviousTube.GetPositionsSubset(PickedLevel1GrowthRefSlice, PickedLevel1GrowthRefSlice).First();
                }
                return DefaultDouble; //should never get here.
            }
        }

        // <summary> Finds the MaxOD pick value for a given tube if a Max picks exists, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedLevel1MaxIDDisplayUnits
        {
            get
            {
                return Level1GrowthMaxFeature == null ? DefaultDouble
                    : GetDataPoint(Tube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, Level1GrowthMaxFeature.SliceIndexStart).ValueInDisplayUnits * 2;
            }
        }

        /// <summary> Finds the Level1RefID pick for a given tube if it exists based on the creepCalcMethod, otherwise NaN </summary>
        [XmlIgnore]
        public double PickedLevel1RefIDDisplayUnits
        {
            get
            {
                if (PickedLevel1GrowthRefSlice == DefaultInt) return DefaultDouble;
                switch (CalcMethod)
                {
                    case CreepCalcMethod.SameInspection:
                        return GetDataPoint(Tube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                    case CreepCalcMethod.AgedToBaseline:
                        return GetDataPoint(OldestOrBaselineTube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                    case CreepCalcMethod.AgedToAged:
                        return GetDataPoint(PreviousTube, ReformerScalar.RadiusInside, Tube.Inspection.DataMeasure, PickedLevel1GrowthRefSlice).ValueInDisplayUnits * 2;
                }
                return DefaultDouble; //should never get here.
            }
        }

        #endregion Level 1 Max/Ref Data
        

        /// <summary> Returns the growth percent based off of the OverallPickedRefOD and OverallPickedMaxOD. </summary>
        [XmlIgnore]
        public double OverallODGrowth { get { return (PickedOverallMaxODDisplayUnits - PickedOverallRefODDisplayUnits) / (PickedOverallRefODDisplayUnits != 0 ? PickedOverallRefODDisplayUnits : double.NaN) * 100; } }

        /// <summary> Returns the Level 1 growth percent based off of the Level1PickedRefOD and Level1PickedMaxOD. </summary>
        [XmlIgnore]
        public double Level1ODGrowth { get { return (PickedLevel1MaxODDisplayUnits - PickedLevel1RefODDisplayUnits) / (PickedLevel1RefODDisplayUnits != 0 ? PickedLevel1RefODDisplayUnits : double.NaN) * 100; } }
        

        /// <summary> Returns the growth percent based off of the PickedRefID and PickedMaxID. </summary>
        [XmlIgnore]
        public double OverallIDGrowth { get { return (PickedOverallMaxIDDisplayUnits - PickedOverallRefIDDisplayUnits) / (PickedOverallRefIDDisplayUnits != 0 ? PickedOverallRefIDDisplayUnits : double.NaN) * 100; } }

        /// <summary> Returns the Level 1 growth percent based off of the PickedRefID and PickedMaxID. </summary>
        [XmlIgnore]
        public double Level1IDGrowth { get { return (PickedLevel1MaxIDDisplayUnits - PickedLevel1RefIDDisplayUnits) / (PickedLevel1RefIDDisplayUnits != 0 ? PickedLevel1RefIDDisplayUnits : double.NaN) * 100; } }

        /// <summary> Returns the ID growth percent per year depending on the CreepCalcMethod and the Tube's age. NaN if age is 0 </summary>
        [XmlIgnore] public double Level1GrowthRate { get { return Level1IDGrowth / (Tube.Specs.YearsSinceLastInspection != 0 ? (double)Tube.Specs.YearsSinceLastInspection : double.NaN); } }

        [XmlIgnore]
        public bool IsValidPickedOverallRefSlice
        {
            get { return OldestOrBaselineTube != null && PickedOverallGrowthRefSlice.Between(0, ReformerHdf5Helper.GetScalarDataLength(OldestOrBaselineTube, ReformerScalar.AxialPosition) - 1); }
        }

        [XmlIgnore]
        public bool IsValidPickedLevel1RefSlice
        {
            get { return PreviousTube != null && PickedLevel1GrowthRefSlice.Between(0, ReformerHdf5Helper.GetScalarDataLength(PreviousTube, ReformerScalar.AxialPosition) - 1); }
        }

        public double OxidationRateUsedBaseUnits { get; set; }

        public double OxidationRateUsedDisplayUnits { get { return OxidationRateUsedBaseUnits * DefaultValues.MeasBaseToDisplayUnits; } }

        #endregion Public, Calculated Properties

        private static PickedDataPoint GetDataPoint(ReformerTube Tube, ReformerScalar Scalar, StatisticalMeasures Measure, int sliceIndex)
        {
            if (Tube == null) return new PickedDataPoint();
            PickedDataPoint returnValue = new PickedDataPoint();
            double rawValue = DefaultDouble, rawPosition = DefaultDouble;
            int sliceOfMin = DefaultInt;
            if (Tube.AvailableScalars.Contains(Scalar))
            {
                //Constrain the slice to actual data in case reference tube changes.
                sliceOfMin = sliceIndex.Constrain(0, ReformerHdf5Helper.GetScalarDataLength(Tube, ReformerScalar.AxialPosition) - 1);
                rawValue = Tube.GetScalarStatisticalValue(Scalar, Measure, sliceOfMin);
                rawPosition = Tube.GetPositionsSubset(sliceOfMin, sliceOfMin).First();
            }
            
            returnValue.ValueInDisplayUnits = Tube.Inspection.Units.LengthUnitSystem.Convert(rawValue, DisplayUnits.Instance.MeasurementUnits.Scale);
            returnValue.PositionInDisplayUnits = Tube.Inspection.Units.LengthUnitSystem.Convert(rawPosition, DisplayUnits.Instance.AxialDistanceUnits.Scale);
            returnValue.SliceIndex = sliceOfMin;
            return returnValue;
        }
    }

    public enum CreepCalcMethod
    {
        SameInspection,
        AgedToBaseline,
        AgedToAged
    }

    internal struct PickedDataPoint
    {
        internal int SliceIndex;
        internal double PositionInDisplayUnits;
        internal double ValueInDisplayUnits;
    }
}
