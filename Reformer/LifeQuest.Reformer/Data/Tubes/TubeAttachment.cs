﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using NGenerics.Extensions;
using QuestIntegrity.Core.Units;

namespace Reformer.Data.Tubes
{
    /// <summary>
    /// Base class for helper classes attached to ReformerTube that performs attachment of a 'parent tube' object in an XML Serializably safe way.
    /// </summary>
    public class TubeAttachment
    {
        private Guid _tubeGUID;
        private ReformerTube _tube;

        protected double MeasDisplayFactor { get { return Length.Convert(1d, DisplayUnits.Instance.MeasurementUnits.Scale, Tube.Inspection.Units.LengthUnitScale); } }
        protected double AxialDisplayFactor { get { return Length.Convert(1d, DisplayUnits.Instance.AxialDistanceUnits.Scale, Tube.Inspection.Units.LengthUnitScale); } } 

        public TubeAttachment() { }  //Only kept around to please the XML Serializer god.

        public TubeAttachment(ReformerTube Parent)
        {
            _tubeGUID = Parent.ID;
            _tube = Parent;
        }

        [XmlIgnore]
        protected ReformerTube Tube
        {
            get
            {
                //If this doesn't remember what tube it's associated with, go find it based on the GUID. This prevents circular references in XML, while also only requiring a lookup once.
                if (_tube == null)
                { //Go through all of the inspections in the active project and search for the tube out of all the tubes in those inspections.
                    LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.ForEach(Insp =>
                    {
                        List<ReformerTube> matchingTubes = Insp.Tubes.Where(Pipe => Pipe.ID == TubeGUID).ToList();
                        if (matchingTubes.Count > 0) _tube = matchingTubes.First();
                    });
                }
                return _tube;
            }
        }

        /// <summary>
        /// The GUID of the parent Tube. This is the field XML serialized that will alow the tube attachment to link back to the object.
        /// </summary>
        public Guid TubeGUID
        {
            get { return _tubeGUID; }
            set
            {
                _tubeGUID = value;
                _tube = null; //Reset the tube field if the GUID changes.
            }
        }

    }
}
