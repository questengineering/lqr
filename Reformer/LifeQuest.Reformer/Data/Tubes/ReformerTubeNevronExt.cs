﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Nevron.Chart;
using Nevron.GraphicsCore;
using QuestIntegrity.Core.Maths;
using Reformer.Data.Features;
using Reformer.Data.Scalars;

namespace Reformer.Data.Tubes
{
    public static class ReformerTubeNevronExt
    {
        /// <summary> Gets default black lines representing the statistical measures held in Stats(flaggable). </summary>
        internal static List<NLineSeries> GetDefaultFormattedLine(this ReformerTube Tube, ReformerScalar Scalar, StatisticalMeasures Stats)
        {
            List<NLineSeries> returnValue = new List<NLineSeries>();

            //Get position data
            List<float> positions;
            Tube.GetPositions(out positions);

            //Get the scalar data at those positions.
            Dictionary<string, List<float>> values = Tube.Get2DScalarStatisticalData(Scalar, Stats);

            foreach (var item in values)
            {
                //Fill the X and scalar(Y) values and do some formatting
                NLineSeries newLine = new NLineSeries
                {
                    Name = string.Format("{0}: {1}", Tube.Name, item.Key),
                    UseXValues = true,
                    DataLabelStyle = {Visible = false},
                    MarkerStyle = {Visible = false}
                };
                newLine.Values.AddRange(item.Value);
                newLine.XValues.AddRange(positions);
                
                returnValue.Add(newLine);
            }
            return returnValue;
        }

        /// <summary> Gets a red line representing the tubes sensor data (1-based). </summary>
        internal static NLineSeries GetDefaultFormattedLine(this ReformerTube Tube, int SensorNumber, ReformerScalar Scalar)
        {
            //Get the position data
            List<float> positions;
            Tube.GetPositions(out positions);

            //Fill the X and scalar(Y) values and do some formatting
            NLineSeries newLine = new NLineSeries
            {
                Name = SensorNumber.ToString(),
                UseXValues = true,
                DataLabelStyle = {Visible = false},
                MarkerStyle =
                {
                    Visible = false,
                    BorderStyle = {Color = GetDefaultLineColor(SensorNumber)},
                    Width = new NLength(.5f, NGraphicsUnit.Point),
                    Height = new NLength(.5f, NGraphicsUnit.Point)
                },
                BorderStyle = {Color = GetDefaultLineColor(SensorNumber)}
            };

            List<float> values;
            Tube.Get2DScalarForSensor(Scalar, SensorNumber, out values);
            newLine.Values.AddRange(values);
            newLine.XValues.AddRange(positions);

            return newLine;
        }

        /// <summary> Gets a list of lines representing the tubes sensor data (1-based) and returned in order. </summary>
        internal static List<NLineSeries> GetAllEddyLines(this ReformerTube Tube)
        {
            List<NLineSeries> returnLines = new List<NLineSeries>();
            //Get the position data
            List<float> positions;
            if (!Tube.AvailableScalars.Contains(ReformerScalar.EddyData) & !Tube.AvailableScalars.Contains(ReformerScalar.LotisEddyData)) 
                return returnLines;

            
            bool lotis = Tube.Inspection.InspectionTool == Tool.LOTIS;
            ReformerScalar scalar = lotis ? ReformerScalar.LotisEddyData : ReformerScalar.EddyData;
            if (lotis)
                Tube.Get2DScalarForSensor(scalar, LOTISEddySensorMap.AxialPosition, out positions);
            else
                Tube.Get2DScalarForSensor(scalar, MANTISEddySensorMap.AxialPosition, out positions);
            
            int numLines = lotis ? 13 : 9;
            //Fill the X and scalar(Y) values and do some formatting
            for (int i = 2; i <= numLines; i++)
            {
                NLineSeries newLine = new NLineSeries
                {
                    Name = "EddyColumn #" + i,
                    UseXValues = true,
                    DataLabelStyle = { Visible = false },
                    MarkerStyle =
                    {
                        Visible = false,
                        BorderStyle = { Color = GetDefaultLineColor(i) },
                        Width = new NLength(.5f, NGraphicsUnit.Point),
                        Height = new NLength(.5f, NGraphicsUnit.Point)
                    },
                    BorderStyle = { Color = GetDefaultLineColor(i) }
                };

                List<float> values;

                Tube.Get2DScalarForSensor(scalar, i, out values);
                newLine.Values.AddRange(values);
                newLine.XValues.AddRange(positions);
                returnLines.Add(newLine);
            }
            return returnLines;
        }

        /// <summary> Gets a black line representing the tubes first 1D Mean scalar if one isn't specified. Returns null if unable to fulfill the request. </summary>
        internal static NPointSeries GetFeaturePointSeries(this ReformerTube Tube, ReformerScalar Scalar, IEnumerable<ReformerFeatureType> TheTypes)
        {
            List<double> xPositions = new List<double>();
            List<double> yValues = new List<double>();
            List<string> labels = new List<string>();

            foreach (ReformerFeatureInfo f in Tube.Features.Where(F => TheTypes.Contains(F.Type)))
            {
                xPositions.Add(f.StartPosition);
                yValues.Add(Tube.GetScalarStatisticalValue(Scalar, Tube.Inspection.DataMeasure, f.SliceIndexStart));
                labels.Add(f.Label);
            }

            //Create the points object that will represent this feature and add it to the chart.
            NPointSeries newFeatureSeries = new NPointSeries
            {
                UseXValues = true,
                Size = new NLength(1f, NRelativeUnit.ParentPercentage),
                DataLabelStyle = { Format = "<label>" },
                Legend = { Mode = SeriesLegendMode.None }
            };
            newFeatureSeries.XValues.AddRange(xPositions);
            newFeatureSeries.Values.AddRange(yValues);
            newFeatureSeries.Labels.AddRange(labels);

            //Set the point shape.
            NMarkerStyle featureStyle = new NMarkerStyle { PointShape = PointShape.LineDiagonalCross };
            newFeatureSeries.MarkerStyle = featureStyle;

            return newFeatureSeries;
        }

        private static Color GetDefaultLineColor(int number)
        {
            List<Color> possibleColors = new List<Color>{Color.Red, Color.Blue, Color.Green, Color.CadetBlue, Color.Indigo, Color.Violet, Color.Orange, Color.DimGray};
            return possibleColors[(int)Math.Round((double)number % possibleColors.Count)];
        }
    }
}
