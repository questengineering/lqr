﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using QuestIntegrity.Core.Extensions;
using Reformer.Data.Features;

namespace Reformer.Data.Tubes
{
    public static class ReformerTubeBusinessLogicExt
    {
        /// <summary> Looks for the weld closest to the given point in a tube. Returns null and double.NaN if not found. </summary>
        /// <param name="DistanceFromWeld">The distance from the weld, if found. If not found, double.NaN</param>
        public static ReformerFeatureInfo GetClosestWeld(this ReformerTube Tube, double PositionInDisplayUnits, out double DistanceFromWeld)
        {
            List<ReformerFeatureInfo> welds = Tube.Features.Where(F => F.Type == ReformerFeatureType.Weld).ToList();
            ReformerFeatureInfo closestWeld = null;
            DistanceFromWeld = double.NaN;
            foreach (ReformerFeatureInfo weld in welds)
            {
                double distanceFromPosition = (PositionInDisplayUnits - weld.PositionInDisplayUnits) * -1; //multiplied by -1 because section numbering is from bottom to top, and position raw data is from top to bottom.
                if (!DistanceFromWeld.IsNaN() && Math.Abs(distanceFromPosition) > Math.Abs(DistanceFromWeld)) continue;
                DistanceFromWeld = distanceFromPosition;
                closestWeld = weld;
            }
            return closestWeld;
        }


        /// <summary> Gets the tube section based on weld number name. Does not use weld number by index because weld 1 may be below captured data and not named. </summary>
        public static int GetClosestTubeSection(this ReformerTube Tube, double PositionInDisplayUnits)
        {
            if (PositionInDisplayUnits.IsNaN()) return -1;
            double distanceFromWeld;
            ReformerFeatureInfo closestWeld = Tube.GetClosestWeld(PositionInDisplayUnits, out distanceFromWeld);
            if (closestWeld == null) return -1;
            int weldNum;
            var matches = Regex.Matches(closestWeld.Label, @"\d+$");
            if (matches.Count == 0 || !int.TryParse(matches[(matches.Count - 1)].Value, out weldNum)) return -2; //couldn't find a weld that fit the naming convention.

            //Welds are left-oriented visually, meaning data below (towards the bottom of the reformer) is considered part of the section
            int sectionNum = distanceFromWeld > 0 ? weldNum + 1: weldNum;

            return sectionNum;
        }

        /// <summary> Gets the relative distance from the nearest weld. </summary>
        public static double GetClosestTubeSectionRelativePosition(this ReformerTube Tube, double PositionInDisplayUnits, string NumberFormat)
        {
            if (PositionInDisplayUnits.IsNaN()) return double.NaN;
            double distanceFromWeld;
            ReformerFeatureInfo closestWeld = Tube.GetClosestWeld(PositionInDisplayUnits, out distanceFromWeld);
            if (closestWeld == null) return double.NaN;
            return distanceFromWeld;
        }

        /// <summary> Returns an individual tube's header in the format: "Row # Tube #". Removes row indicator if row # is 0 </summary>
        public static string GetHeaderText(this ReformerTube Tube, bool UseLetters)
        {
            string headerText = "";
            if (Tube.ReformerAttributes.RowNumber != 0)
            {
                string rowIndicator = UseLetters ? Tube.ReformerAttributes.RowLetter.ToString(CultureInfo.InvariantCulture) : Tube.ReformerAttributes.RowNumber.ToString(CultureInfo.InvariantCulture);
                headerText += string.Format("Row {0} ", rowIndicator);
            }
            headerText += string.Format("Tube {0}", Tube.ReformerAttributes.TubeNumber);
            return headerText;
        }

    }
}
