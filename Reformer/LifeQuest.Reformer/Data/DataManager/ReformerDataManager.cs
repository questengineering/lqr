﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Ionic.Zlib;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Cryptography;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.Properties;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.InspectionFile;
using Reformer.Data.Project;
using Reformer.Data.Tubes;
using Reformer.Forms.DataProcessFlow.DataProcessWindows;

namespace Reformer.Data.DataManager
{
    public class ReformerDataManager : QuestIntegrity.LifeQuest.Common.DataManager
    {
        
        #region public properties

        public new ReformerProjectInfo CurrentProject
        {
            get { return (ReformerProjectInfo)base.CurrentProject; }
            set { base.CurrentProject = value; }
        }

        #endregion Public Properties

        /// <summary> Creates a new project and saves immediately after. </summary>
        public override void NewProject(string fileName)
        {
            CurrentProject = new ReformerProjectInfo
            {
                FileName = Path.GetFileName(fileName),
               DirectoryPath = Path.GetDirectoryName(fileName),
                Description = "",
            };
            string name = Path.GetFileNameWithoutExtension(fileName);
            //Create a default inspection as soon as a project is created.
            string dataFileName = GetDataFileName(CurrentProject.DirectoryPath, name);
            CreateNewInspection(name, dataFileName);
            CurrentProject.CurrentInspectionFile = CurrentProject.InspectionFiles[0];
            Log.InfoFormat("New Project: {0}.", fileName);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
        }

        /// <summary> Finds an unused FullFileName using the inputs. </summary>
        public string GetDataFileName(string projectDirectory, string inspectionName)
        {
            int attemptCount = 0;
            while (true)
            {
                string dataFileName = string.Format("{0}{1}{2}", Path.Combine(projectDirectory, inspectionName), "#" + attemptCount++, Resources.FileExtensions.HDF5Extension);
                if (new FileInfo(dataFileName).Exists) continue;
                return dataFileName;
            }
        }

        /// <summary> Creates an inspection file, hooks it up, and adds it to the current project before saving the project. </summary>
        public ReformerInspection CreateNewInspection(string newName, string fullDataFileName)
        {
            ReformerInspection newInspection = new ReformerInspection(fullDataFileName)
            {
                Name = newName,
                Parent = LifeQuestReformer.Instance.TheDataManager.CurrentProject,
                DateInspected = DateTime.Today
            };
            LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Add(newInspection);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
            return newInspection;
        }

        /// <summary> Deletes an inspection from the current project before saving. </summary>
        public void DeleteInspection(ReformerInspection insp)
        {
            LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Remove(insp);
            LifeQuestReformer.Instance.TheDataManager.SaveProject();
        }

        #region File Loading Methods
        
        /// <summary> Asyncronously adds files into the Current Inspection File utilizing the progress token to denote progress</summary>
        public Task LoadDataFilesAsync(List<RawFileInfo> filesToLoad, ProgressToken theProgressToken)
        {
            Task newTask = new Task(() => 
            {
                if (filesToLoad == null) return;
                ReformerInspection insp = CurrentProject.CurrentInspectionFile;
                List<ReformerTube> duplicateList = insp.Tubes.Where(p => filesToLoad.Select(f => f.TubeName).Contains(p.Name)).ToList();
                DeleteTubes(duplicateList);
                List<ReformerTube> newTubes = CreateTubes(filesToLoad);

                Stopwatch sw = new Stopwatch();
                filesToLoad.ForEach(f =>
                {
                    sw.Restart();
                    ReformerTube matchedTube = newTubes.Find(T => T.Name == f.TubeName);
                    LoadReformerFile(matchedTube, f.RawFile);
                    theProgressToken.CurrentStatus = string.Format("Finished Loading: {0} in {1} seconds", f.TubeName,
                        Math.Round(sw.Elapsed.TotalSeconds, 1));
                    theProgressToken.AddProgress(1);
                });
                SaveProject();
            });
            newTask.Start();
            return newTask;
        }

        private List<ReformerTube> CreateTubes(IEnumerable<RawFileInfo> filesToLoad)
        {
            List<ReformerTube> newTubes = new List<ReformerTube>();
            foreach (RawFileInfo rawFile in filesToLoad)
            {
                ReformerTube newTube = new ReformerTube(CurrentProject.CurrentInspectionFile) {Name = rawFile.TubeName};
                //Figure out the default row number, tube number, and Material
                int rowNum;
                int.TryParse(newTube.Name.Substring(1, 2), out rowNum);
                newTube.ReformerAttributes.RowNumber = rowNum;
                int tubeNum;
                int.TryParse(newTube.Name.Substring(4, 3), out tubeNum);
                newTube.ReformerAttributes.TubeNumber = tubeNum;
                newTube.ReformerAttributes.Material = CurrentProject.CurrentInspectionFile.ReformerInfo.DefaultMaterial;
                newTube.Specs.AxialEncoder = rawFile.Encoder;

                CurrentProject.CurrentInspectionFile.Tubes.Add(newTube);
                ReformerHdf5Helper.AddBlankTube(newTube);
                newTube.Specs.DiameterInsideInDisplayUnits = newTube.Inspection.ReformerInfo.DefaultIDDisplayUnits;
                newTube.Specs.DiameterOutsideInDisplayUnits = newTube.Inspection.ReformerInfo.DefaultODDisplayUnits;
                newTubes.Add(newTube);
            }
            return newTubes;
        }

        private void DeleteTubes(List<ReformerTube> tubes)
        {
            //Find all the duplicates and delete them from the data file and from the inspection.
            ReformerInspection insp = CurrentProject.CurrentInspectionFile;
            tubes.ForEach(ReformerHdf5Helper.DeleteTube);
            tubes.ForEach(dupP => insp.Tubes.Remove(dupP));            
        }

        private void LoadReformerFile(ReformerTube theTube, FileInfo fi)
        {
            if (CurrentProject == null) throw new InvalidOperationException("There is no active project to load Reformer data into.");
            if (CurrentProject.CurrentInspectionFile == null) throw new InvalidOperationException("There is no active inspection to load Reformer data into.");
            
            using (RawDataImporter dataReader = new RawDataImporter(fi.FullName))
            {
                dataReader.AddReformerFile(theTube, theTube.Specs.AxialEncoder);
                dataReader.Close();
            }
        }

        public Task LoadEddyCurrentDataFilesAsync(List<RawFileInfo> filesToLoad, ProgressToken token)
        {
            Task newTask = new Task(() =>
            {
                if (filesToLoad == null) return;
                ReformerInspection insp = CurrentProject.CurrentInspectionFile;
                //Find all tubes that have already been loaded. Only create new tube objects that don't exist already based on their names.
                List<RawFileInfo> newTubesNeeded = filesToLoad.Where(f => !insp.Tubes.Any(T => f.TubeName.Contains(T.Name))).ToList();
                CreateTubes(newTubesNeeded);

                Stopwatch sw = new Stopwatch();
                filesToLoad.ForEach(f =>
                {
                    sw.Restart();
                    ReformerTube matchedTube = insp.Tubes.ToList().Find(T => f.TubeName.Contains(T.Name));
                    LoadEddyFile(matchedTube, f.RawFile);
                    token.CurrentStatus = string.Format("Finished Loading: {0} in {1} seconds", f.TubeName,
                        Math.Round(sw.Elapsed.TotalSeconds, 1));
                    token.AddProgress(1);
                });
                SaveProject();
            });
            newTask.Start();
            return newTask;
        }

        private void LoadEddyFile(ReformerTube theTube, FileInfo fi)
        {
            if (CurrentProject == null) throw new InvalidOperationException("There is no active project to load Reformer data into.");
            if (CurrentProject.CurrentInspectionFile == null) throw new InvalidOperationException("There is no active inspection to load Reformer data into.");

            using (EddyImporter dataReader = new EddyImporter(fi.FullName))
            {
                dataReader.AddEddyFile(theTube);
            }
        }

        #endregion File Loading Methods

        
        #region Project Save/Load

        /// <summary>
        ///     Saves project file and all dirty files in the project
        /// </summary>
        public override void SaveProject()
        {
            try
            {
                Monitor.Enter(this);
                string password = Encoding.ASCII.GetString(ApplicationConstants.Key);
                string signature = Convert.ToBase64String(Guid.NewGuid().ToByteArray()); //random signature string
                string filePathPermanent = CurrentProject.FilePath;
                string filePathTemp = filePathPermanent + ".tmp";

                //serialize and write out the project
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = "\t",
                    Encoding = Encoding.UTF8,
                    OmitXmlDeclaration = false,
                    NewLineHandling = NewLineHandling.Entitize
                };

                try
                {
                    using (XmlWriter writer = XmlWriter.Create(filePathTemp, settings))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("project");
                        if (LifeQuestReformer.Instance != null)
                        {
                            writer.WriteElementString("productName", LifeQuestReformer.Product);
                        }
                        writer.WriteElementString("productVersion", ApplicationVersion.FullVersion);
                        writer.WriteElementString("fileVersion", ((int)ProjectVersions.Version1).ToString(CultureInfo.InvariantCulture));
                        writer.WriteElementString("signature", signature);
                        writer.WriteStartElement("data");
                        writer.WriteAttributeString("version", "1.0");
                        writer.Flush();

                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (SymmetricAlgorithm algorithm = DES.Create())
                            {
                                algorithm.Padding = PaddingMode.PKCS7;
                                byte[] key = CryptographyHelper.GeneratePasswordHashKey(password + signature, 8);
                                byte[] iV = CryptographyHelper.GeneratePasswordHashKey(password + signature, 8);
                                using (
                                    CryptoStream cryptoStream = new CryptoStream(ms, algorithm.CreateEncryptor(key, iV), CryptoStreamMode.Write))
                                {
                                    using ( DeflateStream compStream = new DeflateStream(
                                            cryptoStream, CompressionMode.Compress,
                                            CompressionLevel.Default, true))
                                    {
                                        compStream.BufferSize = 65536;

                                        XmlWriterSettings xmlSettings = new XmlWriterSettings
                                        {
                                            Indent = false,
                                            Encoding = Encoding.UTF8,
                                            OmitXmlDeclaration = true,
                                            NewLineHandling = NewLineHandling.Entitize
                                        };

                                        using (XmlWriter formattedXml = XmlWriter.Create(compStream, xmlSettings))
                                        {
                                            XmlSerializer serializer = new XmlSerializer(typeof(ReformerProjectInfo));
                                            serializer.Serialize(formattedXml, CurrentProject);

                                            formattedXml.Close();
                                            compStream.Close();
                                            cryptoStream.FlushFinalBlock();
                                        }
                                    }
                                    //Read the bytes out of the memory stream and write out encoded text to the outer xml document
                                    ms.Position = 0;
                                    const int bufferSize = 76 * 6 / 8;
                                    //b64 is 6 bits per character, so this equals the standard line length of 76 characters
                                    byte[] buffer = new byte[bufferSize];
                                    int readBytes;
                                    do
                                    {
                                        readBytes = ms.Read(buffer, 0, bufferSize);
                                        writer.WriteBase64(buffer, 0, readBytes);
                                        writer.WriteRaw(Environment.NewLine); //break into lines
                                    } while (bufferSize <= readBytes);

                                }
                            }
                        }

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                }
                catch (Exception e)
                {
                    Log.Error(@"Save project failed.", e);
                    MessageBox.Show(@"Save project failed.", @"Error Occurred");
                    return;
                }

                try
                {
                    //Swap the files
                    File.Copy(filePathTemp, filePathPermanent, true); //save permanent
                    File.Delete(filePathTemp);

                }
                catch (SystemException e) // catches IOException, ArgumentException, UnauthorizedAccessException
                {
                    Log.Error(string.Format("There is an issue with source({0}) or destination({1}): {2}", filePathTemp,
                        filePathPermanent, e));
                    MessageBox.Show(@"Save project failed.", @"Error Occurred");
                }

            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        /// <summary>
        ///     Saves project file and all dirty files in the project
        /// </summary>
        public void SaveProjectRaw()
        {
                //serialize and write out the project
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    IndentChars = "\t",
                    Encoding = Encoding.UTF8,
                    OmitXmlDeclaration = false,
                    NewLineHandling = NewLineHandling.Entitize
                };

                using (XmlWriter writer = XmlWriter.Create(CurrentProject.FilePath, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("project");
                    if (LifeQuestReformer.Instance != null)
                    {
                        writer.WriteElementString("productName", LifeQuestReformer.Product);
                    }
                    writer.WriteElementString("productVersion", ApplicationVersion.FullVersion);
                    writer.WriteElementString("fileVersion", ((int) ProjectVersions.Version1).ToString(CultureInfo.InvariantCulture));
                    writer.WriteElementString("signature", "NO Sig Required");
                    writer.WriteStartElement("data");
                    writer.WriteAttributeString("version", "1.0");
                    writer.Flush();

                    XmlSerializer serializer = new XmlSerializer(typeof (ReformerProjectInfo));
                    serializer.Serialize(writer, CurrentProject);
                    writer.Close();
                }
        }

        public bool LoadProject(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            if (extension == null || extension.ToLower() != Resources.FileExtensions.ProjectFile) throw new InvalidOperationException("Cannot open Project File named: " + fileName);
            Log.DebugFormat("Loading Project: {0}.", fileName);

            //if the previous project doesn't close properly or the new project doesn't open properly return false
            if (!CloseCurrentProject() || !LoadProjectFile(fileName))
                return false;
            
            return true;
        }

        protected bool LoadProjectFile(string filePath)
        {
            ReformerProjectInfo projectFile;

            string password = Encoding.ASCII.GetString(ApplicationConstants.Key);

            //Read header info
            XmlReader reader = XmlReader.Create(filePath);

            bool found = reader.ReadToFollowing("productName");
            string product = reader.ReadElementContentAsString();
            if (!found || !product.Contains(LifeQuestReformer.Product))
            {
                return false;
            }
            reader.ReadToFollowing("productVersion");
            string version = reader.ReadElementContentAsString();
            reader.ReadToFollowing("fileVersion");
            int versionNum = int.Parse(reader.ReadElementContentAsString());
            reader.ReadToFollowing("signature");
            string signature = reader.ReadElementContentAsString();
            reader.ReadToFollowing("data");
            //Decryptor info
            using (SymmetricAlgorithm algorithm = DES.Create())
            {
                algorithm.Padding = PaddingMode.PKCS7;
                byte[] key = CryptographyHelper.GeneratePasswordHashKey(password + signature, 8);
                byte[] iv = CryptographyHelper.GeneratePasswordHashKey(password + signature, 8);

                //Copy bytes to ms
                int readBytes;
                const int size = 65536;
                byte[] buffer = new byte[size];
                MemoryStream ms = new MemoryStream();
                while ((readBytes = reader.ReadElementContentAsBase64(buffer, 0, size)) > 0)
                {
                    ms.Write(buffer, 0, readBytes);
                }

                //De-serialize
                ms.Seek(0, SeekOrigin.Begin);
                XmlSerializer serializer = new XmlSerializer(typeof(ReformerProjectInfo));
                if (versionNum == (int)ProjectVersions.Version1)
                {
                    CryptoStream cryptoStream = new CryptoStream(ms, algorithm.CreateDecryptor(key, iv), CryptoStreamMode.Read);
                    DeflateStream compStream = new DeflateStream(cryptoStream, CompressionMode.Decompress, false);
                    projectFile = (ReformerProjectInfo)serializer.Deserialize(compStream);
                    cryptoStream.Close();
                    compStream.Close();
                }
                else
                {
                    throw new Exception(string.Format("The Project File version {0} is not supported.  Please load the latest version of the software.", version));
                }
                ms.Close();
            }
            reader.Close();
            CurrentProject = projectFile;

            //Update some project file data on load, in case it got copied/pasted
            if (projectFile != null)
            {
                projectFile.TrackingUpdates = true;
                projectFile.DirectoryPath =  Path.GetDirectoryName(filePath);
                projectFile.FileName = Path.GetFileName(filePath);

                //Check Inspection files
                foreach (ReformerInspection inspectionFile in projectFile.InspectionFiles)
                {
                    //Check the Files
                    inspectionFile.ProcessingOptions = new ProcessingOptions(true, Environment.ProcessorCount, Settings.Default.MaximumMemoryMB, true);
                    inspectionFile.Parent = projectFile;
                    inspectionFile.ProcessFlow = ProcessFlowList.UpdateProcesses(inspectionFile.ProcessFlow);
                }
            }
            
            return true;
        }

        #endregion Project Save/Load

        
    }
}
