﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/30/2012 11:19:13 AM
// Created by:   j.Foster
//
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Extensions.Numeric;
using QuestIntegrity.LifeQuest.Common.Conversion;
using QuestIntegrity.LifeQuest.Common.IO;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;

namespace Reformer.Data.DataManager.DataImporting
{
    public class RawDataImporter : IDisposable
    {
        #region Private Variables

        StreamReader _fileStream;
        EndianBinaryReader _binaryReader;
        Tool _currentFileType;

        readonly string _currentFileName;

        #endregion

        #region Public Properties

        /// <summary> /// returns the propery amount of header lines based on the current type of ReformerFileType. /// </summary>
        public int NumCSVHeaderLines { get { return _currentFileType == Tool.MANTIS ? 10 : 9; } }

        /// <summary> /// Gets the file size in MB. /// </summary>
        public float SizeInMB { get { return _currentFileName == null ? 0 : (new FileInfo(_currentFileName).Length / 1000000.0F); } }

        #endregion

        public RawDataImporter(string FullFileName)
        {
            _currentFileName = FullFileName;
            CreateFileStream(FullFileName);
        }

        public void Close() { _fileStream.Close(); }

        private void CreateFileStream(string FullFileName)
        {
            try { _fileStream = new StreamReader(FullFileName); }
            catch (IOException ex)
            {
                Debug.WriteLine(ex.Message);
                DialogResult result = ReformerWindowManager.Instance.Main.ShowMessage(string.Format("Cannot access the data file: {0}. If it is open, please close it so processing can continue.",
                        FullFileName), "Cannot access file.", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (result == DialogResult.Retry) CreateFileStream(FullFileName);
                else throw;
            }
        }

        /// <summary> Adds the raw file to the Current Inspection's Data file </summary>
        public void AddReformerFile(ReformerTube TheTube, int encoderToUse)
        {
            bool isBinary = !_currentFileName.ToLower().Contains(".csv");
            
            float[,] scalarData;
            List<float> positions;
            if (isBinary)
            {
                ReadAndAnalyzeLotFile(out positions, out scalarData, encoderToUse);
                SortLotisData(ref positions, ref scalarData);
                //if (smoothLOTData)
                    //SmoothLOTData(ref scalarData); //disabled for now.
             }
            else
            {
                int numSlices;
                int sensorCount;
                List<string> fileContents = ReadAndAnalyzeCSVFile(out numSlices, out sensorCount);
                CompileCSVContents(fileContents, sensorCount, out positions, out scalarData);
            }

            if (TheTube.Inspection.InspectionTool != _currentFileType) throw new DataException(string.Format("Inspection File has different tool type than raw file: {0}", _currentFileName));    
            if (scalarData.Length == 0 || positions.Count == 0) throw new InvalidDataException(string.Format("Corrupted or empty data file detected. No Position or Scalar data found for: {0}", _currentFileName));
           
            TheTube.Inspection.NumRadialReadings = scalarData.GetLength(1);
            ReformerScalar scalar = TheTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.RadiusInside : ReformerScalar.RadiusOutside;
            ReformerHdf5Helper.SetTubeData(TheTube, ReformerScalar.AxialPosition, positions);
            ReformerHdf5Helper.SetTubeData(TheTube, scalar, scalarData);
            
        }

        private void SmoothLOTData(ref float[,] scalarData)
        {
            //make a copy of the data so all median calcs are from original data. No need to worry about memory explosion, not my problem.
            //var originalData = new float[scalarData.GetLength(0), scalarData.GetLength(1)];
            //for (int axialIdx = 0; axialIdx < scalarData.GetLength(0); axialIdx++)
            //{
            //    for (int sensorIdx = 0; sensorIdx < scalarData.GetLength(1); sensorIdx++)
            //    {
            //        originalData[axialIdx, sensorIdx] = scalarData[axialIdx, sensorIdx];
            //    }
            //}

            //perform a circumferential median filter with a 5 point radius to start
            int filterRadius = 5;
            int filterAxialRadius = 3;
            for (int centerAxialIdx = 0; centerAxialIdx < scalarData.GetLength(0); centerAxialIdx++)
            {
                for (int centerSensor = 0; centerSensor < scalarData.GetLength(1); centerSensor++)
                {
                    List<float> valueBuffer = new List<float>();
                    for (int sensorIdx = -filterRadius; sensorIdx <= filterRadius; sensorIdx++)
                    {
                        int actualSensorIdx = (centerSensor + sensorIdx) % scalarData.GetLength(1);
                        if (actualSensorIdx < 0) actualSensorIdx += scalarData.GetLength(1);
                        valueBuffer.Add(scalarData[centerAxialIdx, actualSensorIdx]);
                    }
                    for (int axialIdx = -filterAxialRadius; axialIdx <= filterAxialRadius; axialIdx++)
                    {
                        int actualIdx = centerAxialIdx + axialIdx;
                        if (!actualIdx.Between(0, scalarData.GetLength(0) - 1)) continue;

                        valueBuffer.Add(scalarData[actualIdx, centerSensor]);
                    }
                    scalarData[centerAxialIdx, centerSensor] = valueBuffer.Mean(true);
                }
            }

            //perform an axial median filter with a 5 point radius to start
            //int filterAxialRadius = 1;
            //for (int sensor = 0; sensor < scalarData.GetLength(1); sensor++)
            //{
            //    for (int centerAxialIdx = 0; centerAxialIdx < scalarData.GetLength(0); centerAxialIdx++)
            //    {
            //        List<float> valueBuffer = new List<float>();
            //        for (int axialIdx = -filterAxialRadius; axialIdx <= filterAxialRadius; axialIdx++)
            //        {
            //            int actualIdx = centerAxialIdx + axialIdx;
            //            if (!actualIdx.Between(0, scalarData.GetLength(0) - 1)) continue;
                        
            //            valueBuffer.Add(scalarData[actualIdx, sensor]);
            //        }
            //        scalarData[centerAxialIdx, sensor] = valueBuffer.Mean(true);
            //    }
            //}
        }

        private void SortLotisData(ref List<float> Positions, ref float[,] ScalarData)
        {
            //Create a datatable that we can use to sort the data by position.
            DataTable theTable = CreateBlankDataTable(ScalarData.GetLength(1));
            theTable.DefaultView.Sort = "Position";

            //float midPosition = Positions.Mean();
            //float[] distancesFromMid = Positions.Select(P => midPosition - P).ToArray();
            //float[] flippedPositions = distancesFromMid.Select(D => D + midPosition).ToArray();

            //Write the data into the table.
            for (int i = 0; i < ScalarData.GetLength(0); i++)
            {
                //Gather a row's worth of data, including position.
                float[] rowScalars = new float [ScalarData.GetLength(1) + 1];
                rowScalars[0] = Positions[i];
                for (int colNum = 0; colNum < ScalarData.GetLength(1); colNum++) rowScalars[colNum + 1] = ScalarData[i, colNum];
                theTable.Rows.Add();

                //Write the 'row' of values into the table.
                for (int colNum = 0; colNum < ScalarData.GetLength(1) + 1; colNum++) theTable.Rows[i][colNum] = rowScalars[colNum];
            }

            //Get the table data that's sorted by position and write it back into the scalar array.
            DataTable newTable = theTable.DefaultView.ToTable();
            for (int row = 0; row < ScalarData.GetLength(0); row++)
            {
                Positions[row] = (float)newTable.Rows[row][0];
                for (int sensor = 0; sensor < ScalarData.GetLength(1); sensor++) ScalarData[row, sensor] = (float)newTable.Rows[row][sensor + 1];
            }

        }

        private void CompileCSVContents(List<string> FileContents, int SensorCount, out List<float> Positions, out float[,] ScalarData)
        {
            DataTable theTable = CreateBlankDataTable(SensorCount);
            FillDataTable(theTable, FileContents);
            theTable.DefaultView.Sort = "Position";
            DataTable sortedTable = theTable.DefaultView.ToTable();

            //Remove duplicate position values. It can happen if the axial encoder stutters up and down, as happened in a dataset during final testing.
            for (int i = 1; i < sortedTable.Rows.Count; i++)
            {
                float curPos = (float)sortedTable.Rows[i]["Position"];
                float prevPos = (float)sortedTable.Rows[i - 1]["Position"];
                if (curPos.IsWithinToleranceOf(prevPos, .001f))
                    sortedTable.Rows.Remove(sortedTable.Rows[i]);
            }

            ShiftPositionsToStartAtZero(sortedTable.DefaultView);

            //Get the sorted position data in the right format.
            DataTable newTable = sortedTable.DefaultView.ToTable();
            Positions = (from DataRow row in newTable.Rows select (float)row["Position"]).ToList();

            //Get the scalar data in the right format.
            int rowCount = newTable.Rows.Count;
            int colCount = newTable.Columns.Count; //Remember, col1 is position.
            float[,] tmpData = new float[rowCount,colCount - 1];
            Parallel.For(0, rowCount, row =>
            {
                for (int sensor = 1; sensor < colCount; sensor++) { tmpData[row, sensor - 1] = (float)newTable.Rows[row][sensor]; }
            });
            ScalarData = tmpData;
        }

        private void FillDataTable(DataTable BlankTable, List<string> FileContents)
        {
            int startIdx = NumCSVHeaderLines;
            float lastPosition = float.NaN;
            for (int slice = startIdx; slice < FileContents.Count; slice++)
            {
                string[] splitLine = FileContents[slice].Split(',');
                if (splitLine[0] == string.Empty) continue;

                float position = splitLine[0].ToSingle();
                //If the axial encoder stopped, do not get duplicate data. 
                if (position.IsWithinToleranceOf(lastPosition, .001f))
                    continue;
                lastPosition = position;
                object[] allValues = new object[BlankTable.Columns.Count];
                allValues[0] = position;

                if (_currentFileType == Tool.LOTIS && (splitLine[1].ToDouble() < 0 || splitLine[3].ToDouble() < 0)) continue; //Skip LOTIS lines that have a negative sensor reading. They cause bad data.

                for (int i = 0; i < BlankTable.Columns.Count - 1; i++)
                {
                    int columnShift = _currentFileType == Tool.MANTIS ? 4 : 2; //Accounts for MANTIS sensor data starting at column 5 instead of 3 like LOTIS does.
                    float divisor = _currentFileType == Tool.MANTIS ? 1 : 2; //LOTIS CSV has diameter data, so just divide it by 2 so it matches the MANTIS format.
                    string rawTextValue = splitLine[columnShift + i];
                    if (rawTextValue.ToLower().Contains("nan")) rawTextValue = "0.0"; //Some super old data has NaNs in it
                    allValues[i + 1] = float.Parse(rawTextValue) / divisor;
                }
                BlankTable.Rows.Add(allValues);
                
            }
        }

        private DataTable CreateBlankDataTable(int SensorCount)
        {
            //Create a datatable of the actual data so any automated-tweaking can be performed easily.
            DataTable theTable = new DataTable();
            theTable.Columns.Add("Position", typeof(float));
            for (int i = 0; i < SensorCount; i++)
            {
                theTable.Columns.Add("Sensor_" + i, typeof(float));
            }
            return theTable;
        }

        public void Dispose()
        {
            if (_fileStream != null)
                _fileStream.Close();
        }

       


        #region Private Methods

        private List<string> ReadAndAnalyzeCSVFile(out int numSlices, out int sensorCount)
        {
            sensorCount = 0;
            List<string> returnValue = new List<string>();
            bool firstLine = true;
            
            while (_fileStream.Peek() > 0)
            {
                string thisLine = _fileStream.ReadLine();
                //The first line will determine what type of file it is, LOTIS or MANTIS. MANTIS 600 will have a sensor count, MANTIS 400 will have something about calibration coefficients, LOTIS will have nothing.
                if (firstLine && thisLine != null)
                {
                    string thisTrimmedLine = thisLine.Replace(",", "").Trim();//Added comma removal in case someone did some fiddling in Excel and forgot to tell anyone. Excel saves to CSV add extra commas
                    if (!Int32.TryParse(thisTrimmedLine, out sensorCount) && !thisTrimmedLine.Contains("Calibration"))
                    {
                        _currentFileType = Tool.LOTIS;
                        sensorCount = 1; //We assume the average diameter reading counts as the first sensor for LOTIS data.
                    }
                    else
                    { // sensorCount got set in the try-parse if it passed. Don't need to set it again.
                        _currentFileType = Tool.MANTIS;
                        if (sensorCount == 0) //manually read the data to figure out how many sensors there are.
                        {
                            //move ahead a couple of readings and figure out if this is 6 or 8
                            for (int i = 0; i < 13; i++) _fileStream.ReadLine();
                            string testLine = _fileStream.ReadLine();
                            if (testLine != null)
                            {
                                var data = testLine.Trim().Split(',');
                                float sampleNum;
                                for (int i = 4; i < data.Length; i++)
                                {
                                    if (float.TryParse(data[i], out sampleNum))
                                        sensorCount++;
                                }
                            }
                        }
                    }
                    firstLine = false;
                }
                returnValue.Add(thisLine);
            }
            numSlices = returnValue.Count - NumCSVHeaderLines;
            return returnValue;
        }

        private void ReadAndAnalyzeLotFile(out List<float> PositionData, out float[,] SensorData, int encoderToUse)
        {
            Stream stream = new FileStream(_currentFileName, FileMode.Open, FileAccess.Read);
            _currentFileType = Tool.LOTIS;
            _binaryReader = new EndianBinaryReader(new BigEndianBitConverter(), stream);

            _binaryReader.Seek(0, SeekOrigin.Begin); //used for debugging

            //Based on LOTIS-600 File Format: 01-29-10
            //Meta Data
            int version = _binaryReader.ReadInt32();
            int dateCompiled = _binaryReader.ReadInt32();
            double secondsSinceUTC1904 = _binaryReader.ReadDouble();

            //Header Information
            int beginningByteOfCalData = _binaryReader.ReadInt32();
            int beginningByteOfSliceData = _binaryReader.ReadInt32();
            int fileNameSize = _binaryReader.ReadInt32();
            string fileName = GetBinaryString(_binaryReader, fileNameSize);
            int modelNoSize = _binaryReader.ReadInt32();
            string modelNo = GetBinaryString(_binaryReader, modelNoSize);
            int serialNoSize = _binaryReader.ReadInt32();
            string serialNo = GetBinaryString(_binaryReader, serialNoSize);

            //these header things are flattened LabView structs and are broken. Just skip to the cal data point.
            //int headSetupSize = _binaryReader.ReadInt32();
            //string headSetup = GetBinaryString(_binaryReader, headSetupSize);
            //int inspTableSize = _binaryReader.ReadInt32();
            //string inspTable = GetBinaryString(_binaryReader, inspTableSize);

            _binaryReader.Seek(beginningByteOfCalData, SeekOrigin.Begin);

            //Calibration Data
            double diamSF = _binaryReader.ReadDouble();
            double axialSF = _binaryReader.ReadDouble();
            double offset1 = _binaryReader.ReadDouble();
            double offset2 = _binaryReader.ReadDouble();
            int calSens1Size = _binaryReader.ReadInt32();
            double[] calSens1 = GetDoubleArray(_binaryReader, calSens1Size);
            int calSens2Size = _binaryReader.ReadInt32();
            double[] calSens2 = GetDoubleArray(_binaryReader, calSens2Size);

            //Slice Data
            int sliceCount = 0;
            int numReadingsPerSlice = 0;

            _binaryReader.Seek(beginningByteOfSliceData, SeekOrigin.Begin);
            var pos1 = new List<float>();
            var pos2 = new List<float>();
            List2D<float> tmpSensorData = new List2D<float>();

            while (_binaryReader.BaseStream.Position < _binaryReader.BaseStream.Length)
            {
                int sliceHeaderSize = _binaryReader.ReadInt32();
                int probeCount = _binaryReader.ReadUInt16();

                //record the position by averaging the axial positions. 0 positions are junk data.
                int axCnt1 = _binaryReader.ReadInt16();
                int axCnt2 = _binaryReader.ReadInt16();
                int sizeSliceData = _binaryReader.ReadInt32();
                numReadingsPerSlice = _binaryReader.ReadInt32(); //This is set every slice, but is the same value every time.

                //Get sensor 1 data
                List<float> sensor1Rad = new List<float>();
                for (int j = 0; j < numReadingsPerSlice; j++)
                {
                    float rawRadius = _binaryReader.ReadUInt16();
                    sensor1Rad.Add(CalibrateLOTISValue(rawRadius, diamSF, calSens1, offset1));
                }

                _binaryReader.Seek(numReadingsPerSlice * 4, SeekOrigin.Current); //Skip power and peak data.

                //Get sensor 2 data
                List<float> sensor2Rad = new List<float>();
                for (int j = 0; j < numReadingsPerSlice; j++)
                {
                    float rawRadius = _binaryReader.ReadUInt16();
                    sensor2Rad.Add(CalibrateLOTISValue(rawRadius, diamSF, calSens2, offset2));
                }

                _binaryReader.Seek(numReadingsPerSlice * 4, SeekOrigin.Current); //Skip power and peak data.

                //record the data
                //PositionData.Add((float)(Math.Max(axCnt1, axCnt2) * axialSF));
                pos1.Add((float)(axCnt1 * axialSF));
                pos2.Add((float)(axCnt2 * axialSF));
                tmpSensorData.Add(new List<float>(numReadingsPerSlice - 1));
                for (int j = 0; j < numReadingsPerSlice; j++)
                    tmpSensorData[sliceCount].Add((sensor1Rad[j] + sensor2Rad[j]) / 2);
                sliceCount++;
            }

            //Check if the encoder that's selected zerod properly by checking if the tube's length is reported to be over 3000 different.
            bool pos1DiffOkay = Math.Abs(pos1.Max() - pos1.Min()) < 3000 && pos1.Max() != pos1.Min();
            bool pos2DiffOkay = Math.Abs(pos2.Max() - pos2.Min()) < 3000 && pos2.Max() != pos2.Min();
            //If both are bad, just assume .1" distance per tick.
            if (!pos1DiffOkay && !pos2DiffOkay)
            {
                pos1 = Enumerable.Range(0, pos1.Count).Select(s => s*.5f).ToList();
                encoderToUse = 1;
                pos1DiffOkay = true;
            }
            if (encoderToUse == 1 && !pos1DiffOkay && pos2DiffOkay)
                encoderToUse = 2;
            else if (encoderToUse == 2 && !pos2DiffOkay)
                encoderToUse = 1;
            PositionData = encoderToUse == 1 ? pos1 : pos2;
            CleanupLotData(ref PositionData, ref tmpSensorData, numReadingsPerSlice);

            _binaryReader.Close();

            //Reverse the LOT data, according to Joel's bugtesting on: 2014.08.04
            PositionData.Reverse();
            SensorData = new float[tmpSensorData.Count, numReadingsPerSlice];
            for (int slice = 0; slice < tmpSensorData.Count; slice++)
            {
                for (int sensor = 0; sensor < numReadingsPerSlice; sensor++)
                {
                    SensorData[slice, sensor] = tmpSensorData[tmpSensorData.Count - 1 - slice, sensor];
                }
            }
           
            stream.Close();
        }

        private void CleanupLotData(ref List<float> positionData, ref List2D<float> sensorData, int readingsPerSlice)
        {
            //Shift the position data so the minimum value is 0 instead of whatever value the counter has it as. Check for position data in case of empty file.
            int firstValidValueIndex = int.MaxValue;

            if (positionData.Count > 0)
            {
                //According to alan there should always be a 0 point, and it will be the first actual axial point even if the tool is going up or down.
                float firstValidValue = positionData.FirstOrDefault(f => f.IsWithinToleranceOf(0, 5f)); 
                firstValidValueIndex = positionData.LastIndexOf(firstValidValue);
            }

            //Remove junk data that comes with the start of each file by checking to make sure the max is towars the end.
            positionData.RemoveRange(0, firstValidValueIndex + 1);
            sensorData.RemoveRange(0, firstValidValueIndex + 1);

            if (positionData.Count > 0)
            {
                //Data is at some large number to start, so subtract the minimum number to get actual inches.
                float minValue = positionData.Min();
                for (int i = 0; i < positionData.Count; i++) { positionData[i] = positionData[i] - minValue; }
            }

            //Get the mean value at each slice, throwing out any that are 1 inch or greater from the mean as that is known to be junk.
            List<double> means = new List<double>();
            for (int slice = 0; slice < sensorData.Count; slice++)
            {
                double mean = 0;
                for (int sensor = 0; sensor < readingsPerSlice; sensor++)
                {
                    mean += sensorData[slice, sensor] / readingsPerSlice;
                }
                means.Add(mean);
            }
            double totalMean = means.Mean();
            for (int slice = means.Count - 1; slice >= 0; slice--)  //go from end to start so indexing works even after removing items
            {
                if (Math.Abs(means[slice] - totalMean) >= 1)
                {
                    sensorData.RemoveRange(slice, 1);
                    positionData.RemoveAt(slice);
                }
            }
        }

        private static float CalibrateLOTISValue(float RawRadius, double DiameterScaleFactor, IList<double> CalibrationFactors, double Offset)
        {
            float r1 = (float)(RawRadius / DiameterScaleFactor);
            float calibratedValue = (float)
                (CalibrationFactors[4] * Math.Pow(r1, 4) +
                CalibrationFactors[3] * Math.Pow(r1, 3) +
                CalibrationFactors[2] * Math.Pow(r1, 2) +
                CalibrationFactors[1] * r1 +
                CalibrationFactors[0] +
                Offset);
            return calibratedValue;
        }

        private double[] GetDoubleArray(EndianBinaryReader BinaryReader, int Count)
        {
            List<double> doubleList = new List<double>(Count);
            for (int i = 0; i < Count; i++)
            {
                doubleList.Add(BinaryReader.ReadDouble());
            }
            return doubleList.ToArray();
        }

        public static string GetBinaryString(EndianBinaryReader BinaryReader, int CharCount)
        {
            List<byte> byteList = new List<byte>(CharCount);
            for (int i = 0; i < CharCount; i++)
            {
                byteList.Add(BinaryReader.ReadByte());
            }
            string parsedString = Encoding.ASCII.GetString(byteList.ToArray());
            string trimmedValue = parsedString.Trim();
            return trimmedValue;
        }

        public static void ShiftPositionsToStartAtZero(DataView TheDataView)
        {
            float minValue = (from DataRowView row in TheDataView select (float)row["Position"]).Concat(new[] { float.MaxValue }).Min();
            foreach (DataRow row in TheDataView.Table.Rows)
            {
                row["Position"] = (float)row["Position"] - minValue;
            }
        }

        public static void ReverseTable(DataView view)
        {
            float maxValue = view.Cast<DataRowView>().Max(row => (float) row["Position"]);
            float minValue = view.Cast<DataRowView>().Min(row => (float)row["Position"]);
            float midValue = (maxValue + minValue) / 2;
            foreach (DataRow row in view.Table.Rows)
            {
                row["Position"] = midValue - ((float)row["Position"] - midValue);
            }
        }

        #endregion

        
    }
}
