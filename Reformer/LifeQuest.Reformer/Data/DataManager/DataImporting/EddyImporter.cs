﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using QuestIntegrity.Core.Extensions.Integers;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;
using Reformer.WindowManagement;

namespace Reformer.Data.DataManager.DataImporting
{
    class EddyImporter : IDisposable
    {
        /// <summary> returns the propery amount of header lines based on the current type of ReformerFileType. /// </summary>
        const int NumCSVHeaderLines = 1;
        StreamReader _fileStream;
        private readonly string _fullFileName;

        #region Constructor and Setup

        public EddyImporter(string fullFileName)
        {
            _fullFileName = fullFileName;
            CreateFileStream();
        }

        private void CreateFileStream()
        {
            try { _fileStream = new StreamReader(_fullFileName); }
            catch (IOException)
            {
                DialogResult result = ReformerWindowManager.Instance.Main.ShowMessage(string.Format("Cannot access the data file: {0}. If it is open, please close it so processing can continue.",
                        _fullFileName), "Cannot access file.", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (result == DialogResult.Retry) CreateFileStream();
                else throw;
            }
        }

        #endregion Constructor and Setup

        public void AddEddyFile(ReformerTube TheTube)
        {
            float[,] rawNumericalData = ReadData();
            var scalar = TheTube.Inspection.InspectionTool == Tool.LOTIS ? ReformerScalar.LotisEddyData : ReformerScalar.EddyData;
            if (scalar == ReformerScalar.LotisEddyData)
                rawNumericalData = AddPolarPoints(rawNumericalData);
            else
                rawNumericalData = ConvertMANTISToPolarPoints(rawNumericalData);
            
            TheTube.Set2DScalarData(scalar, rawNumericalData);
        }

        /// <summary> Converts MANTIS' default ANear and Afar from -90 to 90 data to 0-360 data. Requested by Ed in 3/2015 </summary>
        private float[,] ConvertMANTISToPolarPoints(float[,] mantisData)
        {
            int rowCount = mantisData.GetLength(0),
                xNearCol = 1,
                xFarCol = 3,
                yNearCol = 2,
                yFarCol = 4,
                aNearCol = 6,
                aFarCol = 8;
            for (int row = 0; row < rowCount; row++)
            {
                double xNear = mantisData[row, xNearCol];
                double yNear = mantisData[row, yNearCol];
                double nearRawAngleInDegrees = (Math.Atan(yNear / xNear) * 180 / Math.PI);//tan-1(y/x) to degrees
                if (xNear < 0) nearRawAngleInDegrees += 180; //add 180 if x is negative. 0 is right flat, 180 is left flat, and atan goes 90 to -90.
                if (nearRawAngleInDegrees < 0) nearRawAngleInDegrees += 360; //if it's negative, then just add 360 to get it's positibe equivalent. We want 0-360
                mantisData[row, aNearCol] = (float)(nearRawAngleInDegrees);

                double xFar = mantisData[row, xFarCol];
                double yFar = mantisData[row, yFarCol];
                double farRawAngleInDegrees = (Math.Atan(yFar / xFar) * 180 / Math.PI);//tan-1(y/x) to degrees
                if (xFar < 0) farRawAngleInDegrees += 180; //add 180 if x is negative. 0 is right flat, 180 is left flat, and atan goes 90 to -90.
                if (farRawAngleInDegrees < 0) farRawAngleInDegrees += 360; //if it's negative, then just add 360 to get it's positibe equivalent. We want 0-360
                mantisData[row, aFarCol] = (float)(farRawAngleInDegrees);
            }
            return mantisData;
        }

        private static float[,] AddPolarPoints(float[,] input)
        {
            float[,] output = new float[input.GetLength(0), 13];
            for (int row = 0; row < input.GetLength(0); row++)
            {
                for (int col = 0; col < 13; col++)
                {
                    if (col < 7)
                        output[row, col] = input[row, col];
                    else
                    {
                        float x = input[row, col - (col.IsOdd() ? 6 : 7)];
                        float y = input[row, col - (col.IsOdd() ? 5 : 6)];
                        if (x == 0) 
                        {
                            output[row, col] = float.NaN;
                            continue;
                        }
                        
                        if (col.IsOdd())
                            output [row,col] = (float)Math.Pow((Math.Pow(x, 2) + Math.Pow(y,2)), .5); //(x^2 + y^2)^.5
                        else
                        {
                            double rawAngleInDegrees = (Math.Atan(y / x) * 180 / Math.PI);//tan-1(y/x) to degrees
                            if (x < 0) rawAngleInDegrees += 180; //add 180 if x is negative. 0 is right flat, 180 is left flat, and atan goes 90 to -90.
                            if (rawAngleInDegrees < 0) rawAngleInDegrees += 360; //if it's negative, then just add 360 to get it's positibe equivalent. We want 0-360
                            output[row, col] = (float)(rawAngleInDegrees); 
                        }
                            
                    }
                }
            }
            return output;
        }

        private float[,] ReadData()
        {
                        //Read the raw data into lines of string, IE: 5.5555,4.4444,3.3333....
            List<string> rawSlices = new List<string>();
            int sliceCount = -1;
            while (_fileStream.Peek() > 0)
            {
                string thisLine = _fileStream.ReadLine();
                sliceCount++;
                if (sliceCount < NumCSVHeaderLines) continue;
                if (!string.IsNullOrEmpty(thisLine))
                    rawSlices.Add(thisLine);                
            }
            
            //Separate and type-cast the float values.
            List<float[]> rawSeparatedValues = new List<float[]>();
            foreach (string s in rawSlices)
            {
                string[] stringValues = s.Split(',');
                float outValue;
                float[] floatvalues = stringValues.Select(S => float.TryParse(S, out outValue) ? outValue : float.NaN).ToArray();
                rawSeparatedValues.Add(floatvalues);
            }

           int numColumns = rawSeparatedValues[0].Length;
           int numRows = rawSeparatedValues.Count;

           if (numRows == 0)
                throw new Exception(string.Format("Unable to parse any valid data from {0}", _fullFileName));
            //convert the [][] array into a [,] array.
            float[,] parsedArray = new float[numRows, numColumns];
            List<float> positions = new List<float>();
            for (int i = 0; i < numRows; i++)
            {
                for (int col = 0; col < numColumns; col++)
                {
                    parsedArray[i, col] = rawSeparatedValues[i][col];
                }
                positions.Add(parsedArray[i,0]);
            }

            DataTable sortedTable = GetSortedDataTable(parsedArray);
            DataTable compressedTable = AverageReadingsAtSamePosition(sortedTable);
            int numCompressedRows = compressedTable.Rows.Count;
            
            //copy the table into a 2D Array now.
            float[,] rA = new float[numCompressedRows, numColumns];
            for (int row = 0; row < numCompressedRows; row++)
            {
                DataRow r = compressedTable.Rows[row];
                for (int col = 0; col < numColumns; col++ )
                {
                    rA[row, col] = (float)r[col]; 
                }
            }
            return rA;
        }

        private DataTable AverageReadingsAtSamePosition(DataTable sortedTable)
        {
            DataTable newTable = sortedTable.Clone();
            int numSameReads = 0;
            int numCols = sortedTable.Columns.Count;
            
            //Start off with the first row's values filling the array.
            float[] sumReads = new float[numCols - 1]; //don't average position

            for (int rowNum = 0; rowNum < sortedTable.Rows.Count; rowNum++)
            {
                DataRow curRow = sortedTable.Rows[rowNum];
                float curPos = (float)curRow["Position"];
                float nextPos = float.NaN;
                if (rowNum + 1 < sortedTable.Rows.Count)
                {
                    DataRow nextRow = sortedTable.Rows[rowNum + 1];
                    nextPos = (float)nextRow["Position"];
                }

                //Add the value of this row.
                for (int col = 0; col < numCols - 1; col++)
                {
                    sumReads[col] += (float)curRow[col + 1];
                }
                numSameReads++;

                if (nextPos != curPos) //Write out the values if the position changes next row.
                {
                    List<float> rowValues = sumReads.Select(f => f / numSameReads).ToList();
                    DataRow newRow = newTable.NewRow();
                    newRow[0] = curPos;
                    for (int col = 1; col < numCols; col++)
                    {
                        newRow[col] = rowValues[col - 1];
                    }
                    newTable.Rows.Add(newRow);
                    numSameReads = 0;
                    sumReads = new float[sortedTable.Columns.Count - 1];
                }
            }
            return newTable;
        }

        private static DataTable GetSortedDataTable(float[,] array)
        {
            //Create a datatable of the actual data so any automated-tweaking can be performed easily.
            DataTable theTable = new DataTable();
            theTable.Columns.Add("Position", typeof(float));
            int numRows = array.GetLength(0);
            int numCols = array.GetLength(1);
            for (int i = 0; i < numCols - 1; i++) { theTable.Columns.Add("Sensor_" + i, typeof(float)); }

            for (int row = 0; row < numRows; row++)
            {
                DataRow newRow = theTable.NewRow();
                for (int col = 0; col < numCols; col++ )
                {
                    newRow[col] = array[row, col];  
                }
                theTable.Rows.Add(newRow);
            }
            RawDataImporter.ShiftPositionsToStartAtZero(theTable.DefaultView);
            RawDataImporter.ReverseTable(theTable.DefaultView);
            theTable.DefaultView.Sort = "Position";
            theTable = theTable.DefaultView.ToTable();
            
            return theTable;
        }

        public void Dispose()
        {
            if (_fileStream != null) _fileStream.Close();
        }
    }
}
