﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HDF5DotNet;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;
using QuestIntegrity.LifeQuest.Common.IO;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Data.DataManager.DataImporting
{
    public class ReformerHdf5Helper : HDF5Helper
    {
        const int SlicesPerChunk = 700;   

        /// <summary> Creates a HDF5 file with the given full name. </summary>
        public static void CreateDefaultH5File(string fullFileName)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.create(fullFileName, H5F.CreateMode.ACC_TRUNC);
                H5F.close(fileId);
            }
        }
        
        /// <summary> Creates a blank space for a tube inside of the data file, unlinking existing data for that tube. </summary>
        public static void AddBlankTube(ReformerTube newTube)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(newTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDWR);

                //Delete existing tube data if it exists.
                if (H5L.Exists(fileId, newTube.Name)) 
                    H5L.Delete(fileId, newTube.Name); 

                //Create a group (folder) for the tube's data to reside in
                var defaultProps = new H5PropertyListId(H5P.Template.DEFAULT);
                var groupID = H5G.create(fileId, "/" + newTube.Name, defaultProps, defaultProps, defaultProps);

                H5P.close(defaultProps);
                H5G.close(groupID);
                H5F.close(fileId);
            }
        }

        /// <summary> Create a dataspace with extensible amount of rows, but locked column(sensor) count </summary>
        private static void CreateChunkedDataArray(H5GroupId groupID, ReformerScalar scalar, int sensorCount, H5DataTypeId dataTypeId)
        {
            lock (SyncRoot)
            {
                //Determine if there are 2 or 1 dimensions to this data, -1 means 2 dimensional but unknown currently.
                int rank = sensorCount == 1 ? 1 : 2;

                //Set up initial parameters.
                long[] dims = rank > 1 ? new long[] { 0, sensorCount } : new long[] { 0 };
                long[] maxDims = rank > 1 ? new long[] { -1, sensorCount } : new long[] { -1 }; //allow extensible readings by setting length to -1 (infinite).

                var defaultProps = new H5PropertyListId(H5P.Template.DEFAULT);
                // Setup the parameters for a chunked and compressed dataset.
                var chunkedAndCompressedPropertyList = H5P.create(H5P.PropertyListClass.DATASET_CREATE);
                long[] chunkSlices = rank > 1 ? new long[] { SlicesPerChunk, sensorCount } : new long[] { SlicesPerChunk };
                H5P.setChunk(chunkedAndCompressedPropertyList, chunkSlices);
                H5P.setDeflate(chunkedAndCompressedPropertyList, 6); //6 was arbitrarily chosen.

                H5DataSpaceId spaceId = H5S.create_simple(rank, dims, maxDims);
                H5DataSetId tubeDataSetId = H5D.create(groupID, scalar.ToString(), dataTypeId, spaceId, defaultProps, chunkedAndCompressedPropertyList, defaultProps);

                //clean up.
                H5P.close(defaultProps);
                H5S.close(spaceId);
                H5D.close(tubeDataSetId);
            }
        }

        /// <summary> Appends a 2D array of values to the end of the data array. </summary>
        public static void AddData<T>(H5DataSetId dataSetId, Array theData)
        {
            int rank = theData.Rank;
            H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId); 

            //Update the extents to be large enough to append with the given data.
            long[] existingDataExtents = H5S.getSimpleExtentDims(dataSpaceId);
            long[] newDataDims = rank > 1 
                ? new long[] { theData.GetLength(0), theData.GetLength(1) }
                : new long[] { theData.GetLength(0) };
            long[] newDataExtents = rank > 1 && existingDataExtents.Length > 1
                ? new[] { existingDataExtents[0] + newDataDims[0], existingDataExtents[1] }
                : new[] { existingDataExtents[0] + newDataDims[0] };

            H5D.setExtent(dataSetId, newDataExtents);

            //The dataSpace is not up to date anymore, it must be closed and reopened to accomodate the new size.
            H5S.close(dataSpaceId);
            dataSpaceId = H5D.getSpace(dataSetId);

            //Allocate some memory space to hold the new data in memory during I/O
            var memSpaceId = H5S.create_simple(rank, newDataDims);

            //Select the area of data that we'll want to write to in the data file using a 'hyperslab'
            long[] offset = { existingDataExtents[0], 0 };
            long[] count = newDataDims;
            H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, offset, count);

            //Define some default transfer properties needed for I/O
            H5PropertyListId transferPropsId = H5P.create(H5P.PropertyListClass.DATASET_XFER);

            //Now that we have all the pre-work figured out, we can actually write some data out.
            H5T.H5Type dataType = GetH5DataType(typeof(T));
            if (rank == 1)
                H5D.write(dataSetId, new H5DataTypeId(dataType), memSpaceId, dataSpaceId, transferPropsId, new H5Array<T>((T[])theData));
            else if (rank == 2)
                H5D.write(dataSetId, new H5DataTypeId(dataType), memSpaceId, dataSpaceId, transferPropsId, new H5Array<T>((T[,])theData));
            else throw new InvalidDataException("Unable to parse generic array with > 2 Rank. Current array rank is: " + rank);
            H5S.close(memSpaceId);
            H5S.close(dataSpaceId);
        }

        /// <summary> Removes a tube from the data file. </summary>
        public static void DeleteTube(ReformerTube deadTube)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(deadTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5GroupId groupID = H5G.open(fileId, deadTube.Name);
                H5L.Delete(fileId, deadTube.Name);
                H5G.close(groupID);
                H5F.close(fileId);
                H5.Close();
            }
        }

        /// <summary> Writes a scalar for a given tube. Deletes all scalar data if scalar had previously been written for this tube. </summary>
        internal static void SetTubeData(ReformerTube theTube, ReformerScalar theScalar, List<float> oneDScalarData)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5GroupId groupID = H5G.open(fileId, theTube.Name);
                
                //If the scalar already exists, delete links to it
                if (H5L.Exists(groupID, theScalar.ToString())) H5L.Delete(groupID, theScalar.ToString());
                
                CreateChunkedDataArray(groupID, theScalar, 1, new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT));
                H5DataSetId dataSetID = H5D.open(groupID, theScalar.ToString(), new H5PropertyListId(H5P.Template.DEFAULT));
                AddData<float>(dataSetID, oneDScalarData.ToArray());
                H5D.close(dataSetID);
                H5G.close(groupID); 
                H5F.close(fileId);
                H5.Close();
            }
        }

        /// <summary> Writes a scalar for a given tube. Deletes all scalar data if scalar had previously been written for this tube. </summary>
        internal static void SetTubeData(ReformerTube theTube, ReformerScalar theScalar, float[,] scalarData)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5GroupId groupID = H5G.open(fileId, theTube.Name);

                //If the scalar already exists, delete links to it
                if (H5L.Exists(groupID, theScalar.ToString())) H5L.Delete(groupID, theScalar.ToString());
                
                CreateChunkedDataArray(groupID, theScalar, scalarData.GetLength(1), new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT));
                H5DataSetId dataSetID = H5D.open(groupID, theScalar.ToString(), new H5PropertyListId(H5P.Template.DEFAULT));
                AddData<float>(dataSetID, scalarData);
                H5D.close(dataSetID);
                H5G.close(groupID);
                H5F.close(fileId);
                H5.Close();
            }
        }

        internal static void SetTubeSensorData(ReformerTube theTube, ReformerScalar theScalar, int sensor, List<float> data)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDWR);
                H5GroupId groupID = H5G.open(fileId, theTube.Name);
                H5DataSetId dataSetId = H5D.open(groupID, theScalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                
                //Designate a dimensional array that defines which dimensions we're pulling from. Position only has 1.
                long numPoints = data.Count;
                const long numCircIndices = 1;
                float[,] dataArray = new float[numPoints, numCircIndices];
                for (int i = 0; i < numPoints; i++) dataArray[i, 0] = data[i];
                long[] dims = { numPoints, numCircIndices };

                //Allocate 2D memory to read into.
                H5DataSpaceId memSpace = H5S.create_simple(2, dims);

                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { 0, sensor - 1 }, new[] { numPoints, numCircIndices });

                //Read into the memory space we built.
                H5D.write(dataSetId, new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<float>(dataArray));

                H5S.close(memSpace);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5G.close(groupID);
                H5F.close(fileId);
                H5.Close();
            }
        }

        /// <summary> Reads 1D Data, such as position, for the entire tube. </summary>
        internal static void Read1DTubeData<T>(ReformerTube theTube, ReformerScalar theScalar, out List<T> scalarData)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, theTube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, theScalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dims = H5S.getSimpleExtentDims(dataSpaceId);
                var array = new T[dims[0]];
                H5D.read(dataSetId, new H5DataTypeId(GetH5DataType(typeof(T))), new H5Array<T> (array));
                scalarData = array.ToList();
                H5F.close(fileId);
                H5G.close(groupId);
                H5D.close(dataSetId);
                H5S.close(dataSpaceId);
                H5.Close();
            }
        }

        /// <summary> Reads 2D Data, such as RadiusInside, for the entire tube. </summary>
        internal static void Read2DTubeData<T>(ReformerTube theTube, ReformerScalar theScalar, out T[,] scalarData)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, theTube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, theScalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dims = H5S.getSimpleExtentDims(dataSpaceId);
                long secondDim = dims.Length > 1 ? dims[1] : 1; //Makes this capable of reading 1D data such as Axial data without crashing.
                var array = new T[dims[0], secondDim];
                H5D.read(dataSetId, new H5DataTypeId(GetH5DataType(typeof(T))), new H5Array<T>(array));
                scalarData = array;
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
            }
        }

        /// <summary> Reads 2D Data, such as RadiusInside, for the entire tube and unwinds it into a 1D array. </summary>
        internal static void Read2DTubeData<T>(ReformerTube theTube, ReformerScalar theScalar, out T[] scalarData)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, theTube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, theScalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dims = H5S.getSimpleExtentDims(dataSpaceId);
                long secondDim = dims.Length > 1 ? dims[1] : 1; //Makes this capable of reading 1D data such as Axial data without crashing.
                var array = new T[dims[0], secondDim];
                H5D.read(dataSetId, new H5DataTypeId(GetH5DataType(typeof(T))), new H5Array<T>(array));
                scalarData = Unwind2DDataTo1D(array);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
            }
        }

        /// <summary> Returns all scalars that exist in the data file for a given tube </summary>
        internal static List<ReformerScalar> GetAvailableScalars(ReformerTube theTube)
        {
            lock (SyncRoot)
            {
                List<ReformerScalar> scalars = new List<ReformerScalar>();
                if (theTube == null || theTube.Inspection == null) return scalars;

                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, theTube.Name);
                scalars.AddRange(ReformerScalar.AllPossibleScalars.Where(s => H5L.Exists(groupId, s.ToString())));
                H5G.close(groupId);
                H5F.close(fileId);

                return scalars;
            }
        }
        
        /// <summary> Recreates the data file, purging all un-linked items leftover in the original file because it is impossible to delete data (it can only be unlinked). </summary>
        internal static void RePackDataFile(ReformerInspection inspection)
        {
            lock (SyncRoot)
            {
                string tempFile = Path.GetTempFileName();
                H5FileId tmpFileId = H5F.create(tempFile, H5F.CreateMode.ACC_TRUNC);
                foreach (ReformerTube tube in inspection.Tubes)
                {
                    H5GroupId tubeGroupId = H5G.create(tmpFileId, tube.Name);
                    foreach (ReformerScalar scalar in tube.AvailableScalars)
                    {
                        int scalarWidth = GetScalarDataWidth(tube, scalar);
                        H5DataTypeId dataType = GetScalarDataType(tube, scalar);
                        CreateChunkedDataArray(tubeGroupId, scalar, scalarWidth, dataType);
                        H5DataSetId dataSetId = H5D.open(tubeGroupId, scalar.ToString());
                        float[,] scalars;
                        tube.Get2DScalarData(scalar, out scalars);
                        AddData<float>(dataSetId, scalars);
                        H5D.close(dataSetId);
                    }
                    H5G.close(tubeGroupId);
                }
                H5F.close(tmpFileId);
                new FileInfo(inspection.DataFileFullName).Delete();
                new FileInfo(tempFile).MoveTo(inspection.DataFileFullName);
            }
        }

        private static H5DataTypeId GetScalarDataType(ReformerTube tube, ReformerScalar scalar)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(tube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, tube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, scalar.ToString());
                H5DataTypeId dataType = H5D.getType(dataSetId);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
                return dataType;
            }
        }

        /// <summary> Gets the width of a scalar held in a file, IE: The sensor count if 2D, or 1 if just axial data. </summary>
        internal static int GetScalarDataWidth(ReformerTube tube, ReformerScalar scalar)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(tube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, tube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, scalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] existingDataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
                return existingDataExtents.Length == 1 ? 1 : existingDataExtents[1].ToInt32();
            }
        }

        /// <summary> Gets the length of a scalar held in a Tube's data, IE: The number of slices of data. </summary>
        internal static int GetScalarDataLength(ReformerTube tube, ReformerScalar scalar)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(tube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, tube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, scalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] existingDataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
                return existingDataExtents[0].ToInt32();
            }
        }

        internal static void GetTubeDataSubset(ReformerTube tube, int startIndex, int endIndex, int startCircIndex, int endCircIndex, ReformerScalar scalarKey, out float[,] outputData)
        {
            lock (SyncRoot)
            {
                //Get some information about the scalars we're about to read.
                H5FileId fileId = H5F.open(tube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, tube.Name);
                H5T.H5Type dataType = GetH5DataType(typeof(float));
                H5DataSetId dataSetId = H5D.open(groupId, scalarKey.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                //H5DataTypeId dataTypeInFile = H5D.getType(dataSetId);

                //Designate a dimensional array that defines which dimensions we're pulling from. Position only has 1.
                long numPoints = endIndex - startIndex + 1;
                long numCircIndices = endCircIndex - startCircIndex + 1;
                outputData = new float[numPoints, numCircIndices];
                long[] dims = { numPoints, numCircIndices };

                //Allocate 2D memory to read into.
                H5DataSpaceId memSpace = H5S.create_simple(2, dims);

                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { startIndex, startCircIndex }, new[] { numPoints, numCircIndices });

                //Read into the array.
                H5D.read(dataSetId, new H5DataTypeId(dataType), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<float>(outputData));
                H5S.close(memSpace);
                H5S.close(dataSpaceId);
                H5D.close(dataSetId);
                H5F.close(fileId);
            }
        }
        internal static List<float> Read1DTubeDataSubset(ReformerTube theTube, ReformerScalar theScalar, int sliceIndexStart, int sliceIndexEnd)
        {
            List<float> returnValue;
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(theTube.Inspection.DataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, theTube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, theScalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);

                long numberOfValues = sliceIndexEnd - sliceIndexStart + 1;
                var array = new float[numberOfValues];
                //Allocate 2D memory to read into.
                H5DataSpaceId memSpace = H5S.create_simple(1, new[] { numberOfValues });

                //Select a hyperslab of 2D scalar data based off of the dataspace ID.
                H5S.selectHyperslab(dataSpaceId, H5S.SelectOperator.SET, new long[] { sliceIndexStart }, new[] { numberOfValues });

                H5D.read(dataSetId, new H5DataTypeId(H5T.H5Type.NATIVE_FLOAT), memSpace, dataSpaceId, new H5PropertyListId(H5P.Template.DEFAULT), new H5Array<float>(array));
                returnValue = array.ToList();
                H5S.close(dataSpaceId);
                H5S.close(memSpace);
                H5D.close(dataSetId);
                H5G.close(groupId);
                H5F.close(fileId);
            }
            return returnValue;
        }

        /// <summary> Gets the extents of Reformer-specific data. minimum of Rank 2 (even if 1D data, the second dimension will be 1.) </summary>
        public static long[] GetDataDimensions(string dataFileFullName, ReformerTube tube, Scalar scalar)
        {
            lock (SyncRoot)
            {
                H5FileId fileId = H5F.open(dataFileFullName, H5F.OpenMode.ACC_RDONLY);
                H5GroupId groupId = H5G.open(fileId, tube.Name);
                H5DataSetId dataSetId = H5D.open(groupId, scalar.ToString());
                H5DataSpaceId dataSpaceId = H5D.getSpace(dataSetId);
                long[] dataExtents = H5S.getSimpleExtentDims(dataSpaceId);
                long[] returnDataExtents = { dataExtents[0], dataExtents.Length > 1 ? dataExtents[1] : 1 };

                H5D.close(dataSetId);
                H5F.close(fileId);
                H5G.close(groupId);
                H5.Close();
                return returnDataExtents;
            }
        }
    }
}
