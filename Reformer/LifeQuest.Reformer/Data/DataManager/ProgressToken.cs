﻿using System;
using System.Collections.Generic;
using QuestIntegrity.Core.Extensions;

namespace Reformer.Data.DataManager
{
    /// <summary>
    /// This thread-safe class is utilized to indicate cross-thread or cross-class progress
    /// </summary>
    public class ProgressToken
    {
        

        private readonly object _calcLock = new object();
        private string _statusText;
        private readonly List<string> _allStatusUpdates = new List<string>(); 

        public int Progress { get; private set; }

        public void AddProgress(int IncrementalProgress)
        {
            lock (_calcLock)
            {
                Progress += IncrementalProgress;
                OnProgressChanged(new EventArgs<int>(Progress));
            }
        }
        
        public string CurrentStatus
        {
            get
            {
                return _statusText;
            }
            set
            {
                lock (_calcLock)
                {
                    _statusText = value;
                    _allStatusUpdates.Add(value);
                    OnStatusChanged(new EventArgs<string>(value));
                }
            }
        }

        public List<string> AllStatusUpdates
        {
            get
            {
                return _allStatusUpdates;
            }
        }

        #region Events

        public event EventHandler<EventArgs<int>> ProgressChanged;
        public event EventHandler<EventArgs<string>> StatusChanged;

        protected virtual void OnStatusChanged(EventArgs<string> E)
        {
            if (StatusChanged != null) StatusChanged(this, E);
        }

        protected virtual void OnProgressChanged(EventArgs<int> E)
        {
            if (ProgressChanged != null) ProgressChanged(this, E);
        }

        #endregion Events
    }

    
}
