#region Copyright Quest Integrity Group, LLC 2013
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/27/2013 2:45:16 PM
// Created by:   j.Foster
//
#endregion Copyright Quest Integrity Group, LLC 2013

using QuestIntegrity.LifeQuest.Common.Data.Features;

namespace Reformer.Data.Features
{
    public class ReformerFeatureType : FeatureType
    {
        //When adding feature types, make sure to add a display name to the ReformerFeatureTypDisplayNames resource.
        public static ReformerFeatureType OverallGrowthMax = new ReformerFeatureType("OverallGrowthMax");
        public static ReformerFeatureType Level1GrowthMax = new ReformerFeatureType("Level1GrowthMax");
        public static ReformerFeatureType Weld = new ReformerFeatureType("Weld");

        public ReformerFeatureType() {} //Needs a paramaterless constructor for XML Serialization.

        public ReformerFeatureType(string TheFeatureType)
        {
            Value = TheFeatureType;
        }

        public override string ToString()
        {
            return Value == null ? @"Feature Not In Resource File" : ReformerFeatureTypeDisplayNames.ResourceManager.GetString(Value);
        }

        
    }
}