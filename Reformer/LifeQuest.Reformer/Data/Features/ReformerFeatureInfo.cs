﻿using System;
using System.Linq;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Data.Features;
using Reformer.Data.InspectionFile;
using Reformer.Data.Tubes;

namespace Reformer.Data.Features
{
    public class ReformerFeatureInfo : FeatureInfo
    {
        private ReformerFeatureStats _stats;
        private ReformerTube _parentTube;
        
        [Obsolete("Do not use, only here for XML Serialization.", true)]
        public ReformerFeatureInfo() { }

        public ReformerFeatureInfo(ReformerTube ParentTube)
        {
            _parentTube = ParentTube;
            ParentTubeGUID = ParentTube.ID;
        }

        public ReformerFeatureStats Stats => _stats ?? new ReformerFeatureStats(GUID);

        //Replaces ParentInspectionFile with a properly typecast version, and finds the parent inspection on first lookup.
        [XmlIgnore]
        public ReformerInspection ParentInspectionFile
        {
            get
            {
                if (ParentInspection == null) ParentInspection = ParentTube.Inspection;
                return (ReformerInspection)ParentInspection; 
            } 
        }

        public Guid ParentTubeGUID { get; set; }

        [XmlIgnore]
        public ReformerTube ParentTube => _parentTube ?? (_parentTube = GetParentTube());

        private ReformerTube GetParentTube()
        {
            return LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.
                Find(Insp => Insp.Tubes.Any(P => P.ID == ParentTubeGUID)).Tubes.
                Find(P => P.ID == ParentTubeGUID);
        }
        
        public ReformerFeatureType Type { get; set; }

        public string TypeString { get { return Type.ToString(); } }

        protected override float GetPosition(int SliceIndex)
        {
            return ParentTube.GetPositionsSubset(SliceIndex, SliceIndex).First();
        }

        protected override float DisplayUnitsAxialFactor
        {
            get { return (float)ParentInspectionFile.Units.LengthUnitSystem.Convert(1, DisplayUnits.Instance.AxialDistanceUnits.Scale); }
        }

        #region Public Methods

        public override string ToString() { return Label; }

        #endregion
    }
}
