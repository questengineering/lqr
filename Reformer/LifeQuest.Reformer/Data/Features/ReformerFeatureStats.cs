﻿using System;
using System.Linq;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;
using Reformer.Data.Scalars;

namespace Reformer.Data.Features
{
    public class ReformerFeatureStats
    {
        #region Constructor and Overhead Items

        private ReformerFeatureInfo _parentFeature;
        
        [Obsolete("Do not use default ReformerFeatureStats constructor. Only exists for XML Serialization.", true)]
        public ReformerFeatureStats() { }

        public ReformerFeatureStats(Guid FeatureGuid)
        {
            ParentFeatureGuid = FeatureGuid;
        }

        public Guid ParentFeatureGuid { get; set; }

        public ReformerFeatureInfo ParentFeature
        {
            get
            {
                return _parentFeature ?? (_parentFeature = (from insp in LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles
                               from tube in insp.Tubes from feature in tube.Features where feature.GUID == ParentFeatureGuid select feature).FirstOrDefault());
            }
        }

        #endregion Constructor and Overhead Items

        #region Calculated Properties

        /// <summary>  2x the minimum inner radius reading  </summary>
        public float IDMinDisplayUnits
        {
            get
            {
                return (float)_parentFeature.ParentInspectionFile.Units.LengthUnitSystem.Convert(GetScalar2DData(ReformerScalar.RadiusInside).Min() * 2, DisplayUnits.Instance.MeasurementUnits.Scale);
            }
        }

        /// <summary> 2x the maximum inner radius reading </summary>
        public float IDMaxDisplayUnits
        {
            get
            {
                return (float)_parentFeature.ParentInspectionFile.Units.LengthUnitSystem.Convert(GetScalar2DData(ReformerScalar.RadiusInside).Max() * 2, DisplayUnits.Instance.MeasurementUnits.Scale);
            }
        }

        /// <summary>  2x the minimum outer radius reading </summary>
        public float ODMinDisplayUnits
        {
            get
            {
                return (float)_parentFeature.ParentInspectionFile.Units.LengthUnitSystem.Convert(GetScalar2DData(ReformerScalar.RadiusOutside).Min() * 2, DisplayUnits.Instance.MeasurementUnits.Scale);
            }
        }

        /// <summary> 2x the maximum outer radius reading </summary>
        public float ODMaxDisplayUnits
        {
            get
            {
                return (float)_parentFeature.ParentInspectionFile.Units.LengthUnitSystem.Convert(GetScalar2DData(ReformerScalar.RadiusOutside).Max() * 2, DisplayUnits.Instance.MeasurementUnits.Scale);
            }
        }

        #endregion Calculated Properties

        #region Public Methods

        public ScalarArray<float> GetScalar2DData(Scalar Scalar)
        {
            ScalarArray<float> returnArray = new ScalarArray<float>(Scalar);
            //switch (_parentFeature.ParentInspectionFile.GetScalar2DTypeCode(Scalar.ToString()))
            //{
            //    case TypeCode.Single:
            //        returnArray = _parentFeature.ParentInspectionFile.GetScalars2DAxialSubset<float>(_parentFeature.SliceIndexStart, _parentFeature.SliceIndexEnd,ReformerScalar.ToString());
            //        break;
            //    case TypeCode.Double:
            //        {
            //            ScalarArray<double> doubleArray = _parentFeature.ParentInspectionFile.GetScalars2DAxialSubset<double>(_parentFeature.SliceIndexStart, _parentFeature.SliceIndexEnd,ReformerScalar.ToString());
            //            returnArray = doubleArray.CastToFloat();
            //        }
            //        break;
            //    default: //If it's not single or double, something is wrong. Just add a single NaN to prevent catastrophic failures.
            //        returnArray.Add(float.NaN);
            //        break;
            //}
            return returnArray;
        }

        #endregion Public Methods
    }
}
