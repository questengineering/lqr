#region Copyright Quest Integrity Group, LLC 2013
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 12/11/2013 6:45:16 PM
// Created by:   j.Foster
//
#endregion Copyright Quest Integrity Group, LLC 2012

namespace Reformer.Data.Features
{
    public class ReformerFeatureTypeGroupings
    {
        #region Public Properties

        public static ReformerFeatureType[] All = 
        {
            ReformerFeatureType.OverallGrowthMax,
            ReformerFeatureType.Level1GrowthMax, 
            ReformerFeatureType.Weld 
        };

        #endregion

        #region Public Methods

        #endregion
      
    }
}