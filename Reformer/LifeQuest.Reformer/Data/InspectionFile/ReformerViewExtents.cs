﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Data.InspectionFile
{
    /// <summary> Class created to store information about views like XY plot ranges for various types of data. </summary>
    [Serializable]
    public class ReformerViewExtents
    {
        /// <summary> Returns the min/max ranges for XY first, and R second for all tubes in an inspection. </summary>
        public static List<Range<double>> GetEddyCurrentRanges(ReformerInspection insp)
        {
            ReformerScalar scalarToGet = insp.InspectionTool == Tool.LOTIS ? ReformerScalar.LotisEddyData : ReformerScalar.EddyData;

            double minXy = double.MaxValue;
            double maxXy = double.MinValue;
            double minR = double.MaxValue;
            double maxR = double.MinValue;
            
            foreach (ReformerTube tube in insp.Tubes.Where(t => t.AvailableScalars.Contains(scalarToGet)))
            {
                float[,] data;
                tube.Get2DScalarData(scalarToGet, out data);
                
                //XY Data
                List<int> columnsToCheck;
                if (scalarToGet == ReformerScalar.LotisEddyData)
                    columnsToCheck = new List<int> { LOTISEddySensorMap.X1 - 1, LOTISEddySensorMap.Y1 - 1, LOTISEddySensorMap.X2 - 1, LOTISEddySensorMap.Y2 - 1, LOTISEddySensorMap.X3 - 1, LOTISEddySensorMap.Y3 -1};
                else
                    columnsToCheck = new List<int> {MANTISEddySensorMap.XNear - 1, MANTISEddySensorMap.YNear - 1, MANTISEddySensorMap.XFar - 1, MANTISEddySensorMap.YFar - 1};
                  
                foreach (int col in columnsToCheck)
                {
                    for (int row = 0; row < data.GetLength(0); row++)
                    {
                        if (data[row, col].IsNaN()) continue;
                        minXy = Math.Min(data[row, col], minXy);
                        maxXy = Math.Max(data[row, col], maxXy);
                    }
                }

                //R Data
                if (scalarToGet == ReformerScalar.LotisEddyData)
                    columnsToCheck = new List<int> { LOTISEddySensorMap.R1 - 1, LOTISEddySensorMap.R2 - 1, LOTISEddySensorMap.R3 - 1};
                else
                    columnsToCheck = new List<int> { MANTISEddySensorMap.RNear - 1, MANTISEddySensorMap.RFar - 1};

                List<float> rCheck = new List<float>();
                foreach (int col in columnsToCheck)
                {
                    for (int row = 0; row < data.GetLength(0); row++)
                    {
                        if (data[row, col].IsNaN()) continue;
                        minR = Math.Min(data[row, col], minR);
                        maxR = Math.Max(data[row, col], maxR);
                    }
                }
                
            }
            return new List<Range<double>>{new Range<double>(minXy, maxXy), new Range<double>(minR, maxR)};
        }

        public double EddyCurrentXyMagnitude;
        public double EddyCurrentRMagnitude;
    }
}
