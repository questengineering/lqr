﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using QuestIntegrity.LifeQuest.Common.Annotations;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.Materials;
using Reformer.Forms.UserControls;

namespace Reformer.Data.InspectionFile
{
    public class ReformerSetup : INotifyPropertyChanged
    {
        public Guid ID = new Guid();
        
        ReformerInspection _inspection;
        private ReformerMaterial _defaultMaterial = ReformerMaterialsList.Instance.First();
        private float _defaultOD;
        private float _defaultID;
        private string _customer;
        private string _jobNumber;
        private string _notes;
        private string _refinery;
        private string _reformerName;
        private float _defaultLength;
        private bool _useRowLetters;
        private bool _newTubes;
        private float MeasDisplayFactor { get { return Length.Convert(1, DisplayUnits.Instance.MeasurementUnits.Scale, ParentInspection.Units.LengthUnitScale); } }
        private float AxialDisplayFactor { get { return Length.Convert(1, DisplayUnits.Instance.AxialDistanceUnits.Scale, ParentInspection.Units.LengthUnitScale); } }
        
        /// <summary> Parent Inspection file found using Dynamic first-lookup  </summary>
        public ReformerInspection ParentInspection
        {
            get
            {
                if (LifeQuestReformer.Instance.TheDataManager.CurrentProject == null) return null;
                return _inspection ?? (_inspection = (from insp in LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles where insp.ReformerInfo.ID == ID select insp).FirstOrDefault());
            }
        }

        #region Public Properties

        public string Customer
        {
            get { return _customer; }
            set
            {
                if (value == _customer) return;
                _customer = value;
                OnPropertyChanged();
            }
        }

        public string Refinery
        {
            get { return _refinery; }
            set
            {
                if (value == _refinery) return;
                _refinery = value;
                OnPropertyChanged();
            }
        }

        public string ReformerName
        {
            get { return _reformerName; }
            set
            {
                if (value == _reformerName) return;
                _reformerName = value;
                OnPropertyChanged();
            }
        }

        public string JobNumber
        {
            get { return _jobNumber; }
            set
            {
                if (value == _jobNumber) return;
                _jobNumber = value;
                OnPropertyChanged();
            }
        }

        public string Notes
        {
            get { return _notes; }
            set
            {
                if (value == _notes) return;
                _notes = value;
                OnPropertyChanged();
            }
        }

        public float DefaultOD
        {
            get { return _defaultOD; }
            set
            {
                if (value.Equals(_defaultOD)) return;
                _defaultOD = value;
                OnPropertyChanged();
                OnPropertyChanged("DefaultODDisplayUnits");
            }
        }

        public ReformerMaterial DefaultMaterial
        {
            get { return _defaultMaterial; }
            set
            {
                if (value.Equals(_defaultMaterial)) return;
                _defaultMaterial = value;
                OnPropertyChanged();
            }
        }

        public float DefaultID
        {
            get { return _defaultID; }
            set
            {
                if (value.Equals(_defaultID)) return;
                _defaultID = value;
                OnPropertyChanged();
                OnPropertyChanged("DefaultIDDisplayUnits");
            }
        }

        public bool UseRowLetters
        {
            get { return _useRowLetters; }
            set
            {
                if (value.Equals(_useRowLetters)) return;
                _useRowLetters = value;
                OnPropertyChanged();
            }
        }

        public bool NewTubes
        {
            get { return _newTubes; }
            set
            {
                if (value.Equals(_newTubes)) return;
                _newTubes = value;
                if (!ParentInspection.IsNull()) ProcessFlowList.SetProcessFlowList(ParentInspection);
                OnPropertyChanged();
            }
        }

        public Range<double> YAxisViewExtents
        {
            get { return DataXyPlot.YAxisSavedRange; }
            set { DataXyPlot.YAxisSavedRange = value; }
        }

        #region Display Unit Properties

        [XmlIgnore]
        public float DefaultIDDisplayUnits
        {
            get { return DefaultID * MeasDisplayFactor; }
            set { DefaultID = value / MeasDisplayFactor;}
        }

        [XmlIgnore]
        public float DefaultODDisplayUnits
        {
            get { return DefaultOD * MeasDisplayFactor; }
            set { DefaultOD = value / MeasDisplayFactor; }
        }

        #endregion Display Unit Properties

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
