﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.Core.Units;
using Reformer.Data.DataManager.DataImporting;
using Reformer.Data.DataProcessFlow;
using Reformer.Data.Project;
using Reformer.Data.Tubes;

namespace Reformer.Data.InspectionFile
{
    [Serializable]
    public class ReformerInspection : QuestIntegrity.LifeQuest.Common.Data.InspectionFileInfo.InspectionFile
    {
        private Tool _inspectionTool;

        [Obsolete("Do not use paramaterless constructor. Only exists for XML Serialization", true)]
        public ReformerInspection()
        {
            //This is here because it has to be created. XML Serialization cannot create this type of list on its own, but it can add the items to it.
            Tubes = new SortableSearchableList<ReformerTube>(TypeDescriptor.GetProperties(typeof(ReformerTube))["OrderIndex"]);
            
        }

        public ReformerInspection(string DataFileFullName): base(DataFileFullName)
        {
            ReformerHdf5Helper.CreateDefaultH5File(DataFileFullName);
            ReformerInfo = new ReformerSetup();
            ProcessFlowList.SetProcessFlowList(this);
            Units = new UnitSystem(UnitSystem.UnitSystems.Lbs_in_psi);
            Tubes = new SortableSearchableList<ReformerTube>(TypeDescriptor.GetProperties(typeof(ReformerTube))["OrderIndex"]);
        }

        #region Public Properties

        [XmlIgnore]
        public new ReformerProjectInfo Parent
        {
            get { return (ReformerProjectInfo)base.Parent; }
            set { base.Parent = value; }
        }

        public SortableSearchableList<ReformerTube> Tubes { get; set; }
        public ProcessFlowList ProcessFlow { get; set; }
        public string RawDataSourceDirectory { get; set; }
        public ReformerSetup ReformerInfo { get; set; }

        private ReformerViewExtents _viewExtents;
        public ReformerViewExtents ViewExtents { 
            get
            {
                return _viewExtents ?? (_viewExtents = new ReformerViewExtents());
            }
            set
            {
                _viewExtents = value;
            }
        }

        public StatisticalMeasures DataMeasure { get { return DefaultValues.Meaures.First(M => M.Key == InspectionTool).Value; } }

        /// <summary> The tool used for inspection. On property set, the process flow list will be updated (if possible). </summary>
        public Tool InspectionTool
        {
            get { return _inspectionTool; }
            set
            {
                _inspectionTool = value;
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary> Deletes the inspection file and all data files associated with it. </summary>
        public void Delete()
        {
            LifeQuestReformer.Instance.TheDataManager.DeleteInspection(this);
            try { new FileInfo(DataFileFullName).Delete(); } 
            catch (Exception) { Debug.WriteLine("Cannot delete DataFile."); } //TODO: Figure out how to delete data file if it fails.
        }

        public void RepackData()
        {
            ReformerHdf5Helper.RePackDataFile(this);
        }

        #endregion Public Methods
        
    }
    
}
