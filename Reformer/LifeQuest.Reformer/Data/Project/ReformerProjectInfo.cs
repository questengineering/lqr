﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Graphics.ColorScaleBar;
using QuestIntegrity.LifeQuest.Common.Data;
using QuestIntegrity.LifeQuest.Common.Engineering;
using Reformer.Data.InspectionFile;
using Reformer.Data.Scalars;

namespace Reformer.Data.Project
{
    [Serializable]
    public class ReformerProjectInfo : ProjectInfo
    {
        private int _currentInspectionFileIndex;
        
        public ReformerProjectInfo()
        {
            InspectionFiles = new BindingList<ReformerInspection>();
            ColorScale = new ColorScaleBar();
            ColorScale.InitializeScalars( new SerializableDictionary<string, ColorMap>
            {
                {ReformerScalar.RadiusInside.ToString(), ColorMap.JetBlueRed},
                {ReformerScalar.RadiusOutside.ToString(), ColorMap.JetBlueRed},
            });
        }

        /// <summary>     Gets or Sets a pointer to the currently selected InspectionFile </summary>
        [XmlIgnore]
        public virtual ReformerInspection CurrentInspectionFile
        {
            get
            {
                if (InspectionFiles == null || InspectionFiles.Count == 0) return null;
                if (_currentInspectionFileIndex >= InspectionFiles.Count) return InspectionFiles.FirstOrDefault();
                return InspectionFiles[_currentInspectionFileIndex];
            }
            set { _currentInspectionFileIndex = InspectionFiles.IndexOf(value); }
        }

        
        public BindingList<ReformerInspection> InspectionFiles {get; set; }

        public PipeSize ColorScaleBasis;

        public int TotalNumberOfInspectionFiles { get { return InspectionFiles.Count; } }

        public OrderBy OrderBaselineBy { get; set; }
    }
}
