﻿using QuestIntegrity.LifeQuest.Common.Resources;
using Reformer.Properties;

namespace Reformer.Data.Project
{
    /// <summary> A singleton most recently used list that stores its data inside of the settings file. </summary>
    public class ProjectMRUList : MruList
    {
        public static ProjectMRUList Instance = new ProjectMRUList();

        /// <summary> Creates the MRU list and reads all values in from the settings file </summary>
        private ProjectMRUList()
            : base(Settings.Default.MRUProjectList)
        {
        }

        /// <summary> Stores changes to the MRU List. </summary>
        public void SaveMRUList()
        {
            Settings.Default.MRUProjectList = ToArrayList();
            Settings.Default.Save();
        }
    }
}
