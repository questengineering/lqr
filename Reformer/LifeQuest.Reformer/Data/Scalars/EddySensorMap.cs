﻿namespace Reformer.Data.Scalars
{
    /// <summary> Provides a description of each column in scalar data linked to its sensor number. </summary>
    public static class MANTISEddySensorMap
    {
        public static int AxialPosition = 1;
        public static int XNear = 2;
        public static int YNear = 3;
        public static int XFar = 4;
        public static int YFar = 5;
        public static int RNear = 6;
        public static int ANear = 7;
        public static int RFar = 8;
        public static int AFar = 9;
    }

    /// <summary> Provides a description of each column in scalar data linked to its sensor number. </summary>
    public static class LOTISEddySensorMap
    {
        public static int AxialPosition = 1;
        public static int X1 = 2;
        public static int Y1 = 3;
        public static int X2 = 4;
        public static int Y2 = 5;
        public static int X3 = 6;
        public static int Y3 = 7;
        public static int R1 = 8;
        public static int A1 = 9;
        public static int R2 = 10;
        public static int A2 = 11;
        public static int R3 = 12;
        public static int A3 = 13;
    }
}
