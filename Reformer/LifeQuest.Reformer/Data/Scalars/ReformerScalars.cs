﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.LifeQuest.Common.Data.Scalars;

namespace Reformer.Data.Scalars
{
    public class ReformerScalar: Scalar
    {
        public static readonly ReformerScalar RadiusInside = new ReformerScalar("RadiusInside");
        public static readonly ReformerScalar RadiusInsideOriginal = new ReformerScalar("RadiusInsideOriginal");
        public static readonly ReformerScalar RadiusOutside = new ReformerScalar("RadiusOutside");
        public new static readonly ReformerScalar AxialPosition = new ReformerScalar("AxialPosition");
        public static readonly ReformerScalar EddyData = new ReformerScalar("EddyData");
        public static readonly ReformerScalar LotisEddyData = new ReformerScalar("LOTISEddyData");

        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public ReformerScalar() {} //Needed for XML serialization.
        public ReformerScalar(string theValue) : base(theValue) { }

        public override string ToString()
        {
            string value = ScalarDisplayNames.ResourceManager.GetString(Value) ?? base.ToString();
            return value;
        }

        public string DisplayName => ToString();

        /// <summary> Returns a list of all possible scalar types that can exist, calculated using reflection. </summary>
        public static List<ReformerScalar> AllPossibleScalars
        {
            get { return typeof(ReformerScalar).GetFields().Select(f => f.GetValue(null) as ReformerScalar).Where(f => f != null).ToList(); }
        }
    }
}
