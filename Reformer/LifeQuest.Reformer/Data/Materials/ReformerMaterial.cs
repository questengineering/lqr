﻿
using System;

namespace Reformer.Data.Materials
{
    public class ReformerMaterial
    {
        public string Name { get; set; }
        public CreepIndicatorValues CreepValues { get; set; }

        public enum CreepDamage
        {
            NotSet,
            NoIndication,
            EarlyIndication,
            ThirtyPercent,
            FortyPercent,
            FiftyPercent,
            GreaterThanFiftyPercent,
            NotEnoughData
        }

        /// <summary> Once used to calculate creep damage, but no more. Now it is user input. </summary>
        [Obsolete("Not used anymore, according to Tim Haugen.", true)]
        internal static CreepDamage CalculateCreepDamage(ReformerMaterial Material, double IDGrowth)
        {
            if (Material == null | double.IsNaN(IDGrowth)) return CreepDamage.NotEnoughData;

            //Step through each increasing value and return the proper enum
            if (IDGrowth < Material.CreepValues.EarlyIndication)
                return CreepDamage.NoIndication;
            if (IDGrowth < Material.CreepValues.ThirtyPercent)
                return CreepDamage.EarlyIndication;
            if (IDGrowth < Material.CreepValues.FortyPercent)
                return CreepDamage.ThirtyPercent;
            if (IDGrowth < Material.CreepValues.FiftyPercent)
                return CreepDamage.FortyPercent;
            if (IDGrowth < Material.CreepValues.GreaterThanFiftyPercent)
                return CreepDamage.FiftyPercent;
            return CreepDamage.GreaterThanFiftyPercent;
        }

        public override string ToString() { return Name; }
    }

    public struct CreepIndicatorValues
    {
        public float NoIndication;
        public float EarlyIndication;
        public float ThirtyPercent;
        public float FortyPercent;
        public float FiftyPercent;
        public float GreaterThanFiftyPercent;
    }
    
}
