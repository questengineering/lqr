﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Reformer.Data.Materials
{
    public class ReformerMaterialsList
    {
        public static List<ReformerMaterial> Instance;
        
        public ReformerMaterialsList()
        {
            if (Instance == null) GetMaterialsList();
        }

        private void GetMaterialsList()
        {
            FileInfo materialsFile = new FileInfo(Path.Combine(UsefulDirectories.Materials.FullName, "Materials.xml"));
            if (materialsFile.Exists)
            {
                Instance = new List<ReformerMaterial>();
                XmlSerializer deserializer = new XmlSerializer(typeof(List<ReformerMaterial>));
                Instance.AddRange((List<ReformerMaterial>)deserializer.Deserialize(new FileStream(materialsFile.FullName, FileMode.Open)));                
            }
            else CreateDefaultMaterialsList();
        }

        private void CreateDefaultMaterialsList()
        {
            Instance = new List<ReformerMaterial>();
            ReformerMaterial hK40 = new ReformerMaterial
                {
                    Name = "HK40",
                    CreepValues = new CreepIndicatorValues
                        {
                            NoIndication = 0f,
                            EarlyIndication = .5f,
                            ThirtyPercent = 1f,
                            FortyPercent = 1.5f,
                            FiftyPercent = 2f,
                            GreaterThanFiftyPercent = 3f
                        }
                };
            ReformerMaterial hPModified = new ReformerMaterial
                {
                    Name = "HP Modified",
                    CreepValues = new CreepIndicatorValues
                        {
                            NoIndication = 0f,
                            EarlyIndication = .5f,
                            ThirtyPercent = 1f,
                            FortyPercent = 2f,
                            FiftyPercent = 3f,
                            GreaterThanFiftyPercent = 5.5f
                        }
                };
            ReformerMaterial microAlloy = new ReformerMaterial
                {
                    Name = "MicroAlloy",
                    CreepValues = new CreepIndicatorValues
                        {
                            NoIndication = 0f,
                            EarlyIndication = .5f,
                            ThirtyPercent = 1f,
                            FortyPercent = 2f,
                            FiftyPercent = 3f,
                            GreaterThanFiftyPercent = 5.5f
                        }
                };
            Instance.Add(hPModified);
            Instance.Add(hK40);
            Instance.Add(microAlloy);
            //SerializeInstance(theList); //Saved for debugging, in case defaults need to change.
        }

        //private void SerializeInstance(object theList)
        //{
        //    var xmlS = new XmlSerializer(typeof(List<ReformerMaterial>));
        //    xmlS.Serialize(new FileStream(Path.Combine(@"C:\Temp\", "Materials.xml"), FileMode.Create), theList);
        //}

        
    }
}
