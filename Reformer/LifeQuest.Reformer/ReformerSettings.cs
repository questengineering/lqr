﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 10/23/2012 1:23:25 PM
// Created by:   J.Foster
//
#endregion

using QuestIntegrity.LifeQuest.Common.Interfaces;
using QuestIntegrity.LifeQuest.Common.Properties;

namespace Reformer
{
    public class ReformerSettings: ISettings
    {
        public bool UpgradeSettings
        { 
            get { return Settings.Default.UpgradeSettings; }
            set { Settings.Default.UpgradeSettings = value; }
        }

        public void Upgrade()
        {
            Settings.Default.Upgrade();
        }

    }
}
