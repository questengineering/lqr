﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestIntegrity.LifeQuest.Common;

namespace Reformer.ScreenLayout
{
    class ReformerPanelLayout : PanelLayout
    {
        public static readonly ReformerPanelLayout NewProject = new ReformerPanelLayout("NewProject");
        public static readonly ReformerPanelLayout Customer = new ReformerPanelLayout("Customer");

        [Obsolete("Parameterless Constructor only used for serialization", true)]
        public ReformerPanelLayout() { } //Needed for XML serialization.
        public ReformerPanelLayout(string TheValue) : base(TheValue) { }

        public string DisplayName { get { return ToString(); } }

        /// <summary> Returns a list of all possible PanelLayout types that can exist, calculated using reflection. </summary>
        public static List<ReformerPanelLayout> AllPossibleLayouts
        {
            get { return typeof(ReformerPanelLayout).GetFields().Select(F => F.GetValue(null) as ReformerPanelLayout).Where(F => F != null).ToList(); }
        }
    }
}
