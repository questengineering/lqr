﻿using QuestIntegrity.LifeQuest.Common;
using Reformer.Forms;
using Reformer.Forms.DataProcessFlow;
using Reformer.Forms.ViewerWindowItems;
using Reformer.WindowManagement;

namespace Reformer.ScreenLayout
{
    public class ReformerScreenLayout : QuestIntegrity.LifeQuest.Common.ScreenLayout
    {
        private ReformerScreenLayout()
        {
        }

        public static ReformerScreenLayout Instance
        {
            get
            {
                if (_instance as ReformerScreenLayout == null) _instance = new ReformerScreenLayout();
                return _instance as ReformerScreenLayout;
            }
        }

        public void SetPanelLayout(PanelLayout layout)
        {
            EnsureInitialized();
            ReformerWindowManager.Instance.Main.SuspendFormDrawing();

            try
            {
                if (layout == PanelLayout.Default)
                    Layout_NewProject();
                else if (layout == ReformerPanelLayout.Customer)
                    Layout_Customer();
            }
            finally
            {
               ReformerWindowManager.Instance.Main.ResumeFormDrawing(true);
            }
        }

        /// <summary>
        ///     Layout for a new project
        /// </summary>
        protected virtual void Layout_NewProject()
        {
            // Left side
            ReformerMain main = ReformerWindowManager.Instance.Main;
            main.CloseAllPanels();
            main.SetupForm(ReformerWindowManager.Instance.CreateForm(ReformerFormType.ReformerProjectProperties));
            main.SetupForm(ReformerWindowManager.Instance.CreateForm(ReformerFormType.ReformerProcessFlow));
            SizeRootPanels(400, 150, 300, 150);

            //Center Screen
            main.SetupForm(ReformerWindowManager.Instance.CreateForm(ReformerFormType.CentralDataProcessForm));
            main.ToggleTabClose(false);
        }

        /// <summary> Layout for a customer </summary>
        protected virtual void Layout_Customer()
        {
            var main = ReformerWindowManager.Instance.Main;
            main.SuspendLayout();
            main.CloseAllPanels();
            //Center Screen. Load up the central form, and place the TMin viewer in it.
            BlankBaseForm newForm = (BlankBaseForm)ReformerWindowManager.Instance.CreateForm(ReformerFormType.BlankBaseForm);
            newForm.SetProcessWindow(new BaseViewerWindow());
            newForm.SetCurrentInspection(LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile);
            main.SetupForm(newForm);

            //Don't allow users to close the window.
            main.ToggleTabClose(false);
            main.ResumeLayout();
        }

    }
}
