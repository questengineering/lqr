﻿using System.ComponentModel;
using System.Linq;
using NGenerics.Extensions;
using QuestIntegrity.LifeQuest.Common;
using QuestIntegrity.LifeQuest.Common.Forms;
using QuestIntegrity.LifeQuest.Common.Interfaces;
using Reformer.Forms;
using Reformer.Forms.DataProcessFlow;
using Reformer.ScreenLayout;

namespace Reformer.WindowManagement
{
    class ReformerWindowManager : WindowManager
    {
        private readonly LifeQuestReformer App;
        public ReformerMain Main; //Set in ReformerMain during construction utilized in '.Instance'.
        private static ReformerProcessFlowFormManager _processFormManagerInstance;
        
        protected ReformerWindowManager()
        {
            _instance = this;
            App = LifeQuestReformer.Instance;
        }

        public static ReformerWindowManager Instance
        {
            get
            {
                if (_instance != null) return _instance as ReformerWindowManager;
                lock (SyncRoot)
                {
                    _instance = new ReformerWindowManager();
                }
                return _instance as ReformerWindowManager;
            }
        }

        public static DataProcessFlowForm ProcessFlowForm
        {
            get
            {
                var listOfProcessFlowForms = Instance.GetFormsByType(typeof(DataProcessFlowForm), false);
                if (listOfProcessFlowForms.Count > 0) 
                    return (DataProcessFlowForm)listOfProcessFlowForms.First();
                return null;
            }
        }
        /// <summary> The controlling form for the process flow. Created if non-existant on get. </summary>
        public static ReformerProcessFlowFormManager ProcessFormManager
        {
            get
            {
                if (_processFormManagerInstance != null) return _processFormManagerInstance;
                lock (SyncRoot) { _processFormManagerInstance = new ReformerProcessFlowFormManager(); }
                return _processFormManagerInstance;
            }
        }

        public void SetPanelLayout()
        {
            App.Layout.SetPanelLayout(LifeQuestReformer.Instance.TheDataManager.CurrentProject.InspectionFiles.Count > 0
                        ? PanelLayout.Default
                        : ReformerPanelLayout.NewProject);
        }

        #region Trigger form actions

        public void UpdateWindowDisplayUnits(PropertyChangedEventArgs Args)
        {
            GetFormsByInterface(typeof(IHasDisplayUnits)).Cast<IHasDisplayUnits>().ForEach(F => F.UpdateDisplayUnits());
        }

        #endregion

        #region Protected Methods

        protected override void ShowForm(LifeQuestBaseForm TheForm)
        {
            Main.ShowForm(TheForm);
        }

        #endregion Protected Methods

        #region Public Methods

        #endregion

        
    }

    
}
