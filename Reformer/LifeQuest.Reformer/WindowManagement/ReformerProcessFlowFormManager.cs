﻿using System.Linq;
using System.Reflection;
using log4net;
using Reformer.Data.DataProcessFlow;
using Reformer.Forms;
using Reformer.Forms.DataProcessFlow;


namespace Reformer.WindowManagement
{
    class ReformerProcessFlowFormManager
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        #region Properties

        /// <summary> Dynamically looks up the central process form, and returns null if it doesn't exist. Private to slap other hands away. </summary>
        private static CentralDataProcessForm CentralForm
        {
            get
            {
                var forms = ReformerWindowManager.Instance.GetFormsByType(typeof(CentralDataProcessForm), false).Cast<CentralDataProcessForm>().ToArray();
                return forms.Any() ? forms.First() : null;
            }
        }

        #endregion

        /// <summary> Tells the central processing form which step to change to, and creates the central form if it doesn't exist for whatever reason. </summary>
        public void SetCurrentFlowItem(BaseProcessFlowItem ProcessItem)
        {
            var centralForm = CentralForm; //locally cache the CentralForm link so it doesn't have to keep looking for it in the getter.
            if (centralForm == null)
            {
                Log.InfoFormat("Central form has been destroyed, creating new one.");
                ReformerWindowManager.Instance.Main.SetupForm(ReformerWindowManager.Instance.CreateForm(ReformerFormType.CentralDataProcessForm));
                centralForm = CentralForm;
            }
            centralForm.SetCurrentProcessItem(ProcessItem);
            Log.InfoFormat("Created Central Form: {0}", ProcessItem.Name);
        }
    }
}
