﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using QuestIntegrity.Extensions;
using Reformer.Data.Features;
using Reformer.Data.InspectionFile;
using QuestIntegrity.Maths;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.OneOffScripts
{
    public static class IrvingDataExport
    {
        public static ReformerInspection Insp => LifeQuestReformer.Instance.TheDataManager.CurrentProject.CurrentInspectionFile;

        public static void ExportIrvingData()
        {
            var orderedTubes = Insp.Tubes.OrderBy(t => t.ReformerAttributes.RowNumber).ThenBy(t => t.ReformerAttributes.TubeNumber).ToList();

            Application xlApp = new Application { Visible = true };
            var wb = xlApp.Workbooks.Add();
            Worksheet ws = wb.ActiveSheet;

            //Heading stuff.
            ws.Range[ws.Cells[1, 1], ws.Cells[1, 1]].Value = @"Data for Irving Oil MANTIS Inspection 2015 Data (Job 108029) ";
            ws.Range[ws.Cells[2, 1], ws.Cells[2, 1]].Value = @"Position of Welds in Data";
            ws.Range[ws.Cells[3, 1], ws.Cells[3, 1]].Value = @"Weld 4";
            ws.Range[ws.Cells[4, 1], ws.Cells[4, 1]].Value = @"Weld 3";
            ws.Range[ws.Cells[5, 1], ws.Cells[5, 1]].Value = @"Weld 2";
            ws.Range[ws.Cells[6, 1], ws.Cells[6, 1]].Value = @"Weld 1";
            ws.Range[ws.Cells[7, 1], ws.Cells[7, 1]].Value = @"Tube ID";
            ws.Range[ws.Cells[8, 1], ws.Cells[8, 1]].Value = @"Position 0.0 = Top (of firebox or top of tube)";
            ws.Range[ws.Cells[8, 2], ws.Cells[8, 2]].Value = @"Average Diameter";
            ws.Range[ws.Cells[9, 1], ws.Cells[9, 1]].Value = @"(inches)";
            ws.Range[ws.Cells[9, 2], ws.Cells[9, 2]].Value = @"(inches)";

            int inchesPerSegment = 6;
            int numSegments = (int)Math.Ceiling(orderedTubes.Max(t => t.EndPosition)/inchesPerSegment);

            for (int slice = 0; slice < numSegments; slice++)
            {
                ws.Range[ws.Cells[10 + slice, 1], ws.Cells[10 + slice, 1]].Value = inchesPerSegment * slice;
            }


            for (int tubeNum = 0; tubeNum < orderedTubes.Count; tubeNum++)
            {
                var tube = Insp.Tubes[tubeNum];
                ws.Range[ws.Cells[7, 2 + tubeNum], ws.Cells[7, 2 + tubeNum]].Value = tube.Name;

                //Write welds
                var tubeWelds = tube.Features.Where(f => f.Type == ReformerFeatureType.Weld).OrderBy(t => t.SliceIndexStart).ToList();
                for (int weldNum = 0; weldNum < tubeWelds.Count; weldNum++)
                {
                    var weld = tubeWelds[weldNum];
                    ws.Range[ws.Cells[3 + weldNum, 2 + tubeNum], ws.Cells[3 + weldNum, 2 + tubeNum]].Value = Math.Round(weld.PositionInDisplayUnits);
                }

                var positionData = tube.Positions;
                var orData = tube.Get2DScalarStatisticalData(ReformerScalar.RadiusOutside, StatisticalMeasures.Mean);
                var odData = orData.First().Value.Select(f => f*2).ToList();
                
                //write split data segment means
                for (int segment = 0; segment < numSegments; segment++)
                {
                    string value = "";
                    int posMin = segment*inchesPerSegment;
                    int posMax = (segment + 1)*inchesPerSegment;
                    
                    if (tubeWelds.Any(w => w.StartPosition.Between(posMin, posMax)))
                        ws.Range[ws.Cells[10 + segment,2 + tubeNum], ws.Cells[10 + segment, 2 + tubeNum]].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.YellowGreen);

                    List<float> valuesinRange = new List<float>();
                    for (int slice = 0; slice < positionData.Count; slice++)
                    {
                        if (positionData[slice] > posMax) continue;
                        if (positionData[slice].Between(posMin, posMax))
                            valuesinRange.Add(odData[slice]);
                    }

                    if (valuesinRange.Count > 0)
                        value = Math.Round(valuesinRange.Mean(), 3).ToString("n3");

                    ws.Range[ws.Cells[10 + segment, 2 + tubeNum], ws.Cells[10 + segment, 2 + tubeNum]].Value = value;
                }

            }




        }

    }
}
