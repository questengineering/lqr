﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Kitware.VTK;
using QuestIntegrity.Core.Extensions;
using QuestIntegrity.Core.Extensions.Collections;
using QuestIntegrity.Core.Maths;
using QuestIntegrity.LifeQuest.Common.Data;
using Reformer.Data.Scalars;
using Reformer.Data.Tubes;

namespace Reformer.Visualization
{
    public class VTKTubeCreatorBase
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Creates an empty 3D tube based off of the start and end axial positions. Low memory consumption, and minimal number of points required to make a tube-like structure.
        /// </summary>
        public static vtkStructuredGrid CreatePreviewTubeGrid(double MinPosition, double MaxPosition, int NumberOfRadialPoints, double Diameter, double WallThickness)
        {
            double[] ptTube = new double[3]; //x,y,z triplet for tube point
            int[] dims = { 2, NumberOfRadialPoints, 2 }; //x,y,z dimensions

            vtkPoints tubePoints = new vtkPoints();
            tubePoints.SetDataTypeToDouble();
            tubePoints.SetNumberOfPoints(dims[0] * (dims[1] + 1) * dims[2]);//note: number of points is +1 to wrap back to itself.

            int id3D = 0;
            // Fill order is x->y->z, so we will loop in that order as well to make managing the array IDs easier.
            for (int z = 0; z < dims[2]; z++)
            {
                for (int y = 0; y < dims[1]; y++)
                {
                    for (int x = 0; x < dims[0]; x++)
                    {
                        ptTube[0] = x == 0 ? MinPosition : MaxPosition;
                        double theta = 360.0d * (y / (double)dims[1]);

                        //set the tube points
                        ptTube[1] = (Diameter / 2 + z * WallThickness) * Math.Cos(theta * Math.PI / 180.0d);
                        ptTube[2] = (Diameter / 2 + z * WallThickness) * Math.Sin(theta * Math.PI / 180.0d);
                        tubePoints.SetPoint(id3D, ptTube[0], ptTube[1], ptTube[2]);

                        //Add extra point for the the tube to wrap.  The point itself is the same as the first, but the index is +dims[0]*dims[1]
                        if (y == 0)
                        {
                            tubePoints.SetPoint(id3D + (dims[1] * dims[0]), ptTube[0], ptTube[1], ptTube[2]);
                        }
                        id3D++;
                    }
                }
                id3D += dims[0];  //increment for the extra y set of data
            }

            //3D Tube Grid
            vtkStructuredGrid returnGrid = new vtkStructuredGrid();
            returnGrid.SetDimensions(dims[0], dims[1] + 1, dims[2]); //Note: y-dimension is +1 to wrap
            returnGrid.SetPoints(tubePoints);
            returnGrid.Update();
            return returnGrid;
        }

        /// <summary> Creates a blank 3D VTK grid in the shape of the Tube </summary>
        public static vtkActor Get3DTube(ReformerTube Tube, ReformerScalar scalar, StatisticalMeasures stats, float axialSampleFactor = 1, int RadialPoints = 50)
        {
            var diameter = Tube.Specs.RadiusInside * 2;
            var wallThickness = Tube.Specs.ThicknessWall;
            //Force the diameter to be at least 1 inch wide to prevent a blank drawing if diameter hasn't been set yet.
            if (diameter.IsNaN() || diameter <= 1) diameter = Math.Max(diameter, 1);
            //Likewise with wall thickness, prevent the points from overlapping.
            if (wallThickness.IsNaN() || wallThickness <= .1) wallThickness = (float)Math.Max(wallThickness, .1);

            List<float> positions;
            List<float> data;

            if (axialSampleFactor > 1) //axial data needs to be reduced
                ReformerTubeDataExt.GetAxiallyReducedTubeStatisticalData(Tube, scalar, stats, axialSampleFactor, out positions, out data);
            else //no reduction or expanding needed.
            {
                Tube.GetPositions(out positions);
                data = Tube.Get2DScalarStatisticalData(scalar, stats).First().Value;
            }
           
            //Create a blank grid
            vtkStructuredGrid newGrid = GridBuilder.CreateEmptyTubeGrid(positions, RadialPoints, diameter, wallThickness, false);

            float[,] repeatedData = new float[data.Count, RadialPoints + 1];
            for (int fakeSensor = 0; fakeSensor <= RadialPoints; fakeSensor++)
            {
                for (int slice = 0; slice < data.Count; slice++)
                {
                    repeatedData[slice, fakeSensor] = data[slice];
                }
            }

                //Paint the scalars onto the grid
            GridBuilder.AddScalarsToGrid(newGrid, repeatedData, 1, scalar.ToString());
            return MapGridToActor(newGrid);
        }

        /// <summary> Creates a blank 3D VTK grid in the shape of the Tube </summary>
        public static vtkActor Get3DTube(ReformerTube Tube, ReformerScalar scalar, float axialSampleFactor = 1, int? RadialPoints = null)
        {
            if (RadialPoints == null) RadialPoints = Tube.Inspection.NumRadialReadings;
            double diameter = Tube.Specs.RadiusInside * 2;
            double wallThickness = Tube.Specs.ThicknessWall;
            //Force the diameter to be at least 1 inch wide to prevent a blank drawing if diameter hasn't been set yet.
            if (diameter.IsNaN() || diameter <= 1) diameter = Math.Max(diameter, 1);
            //Likewise with wall thickness, prevent the points from overlapping.
            if (wallThickness.IsNaN() || wallThickness <= .1) wallThickness = (float)Math.Max(wallThickness, .1);

            List<float> positions;
            float[,] sensorData;
            if (axialSampleFactor > 1) //axial data needs to be reduced
                ReformerTubeDataExt.GetAxiallyReducedData(Tube, scalar, axialSampleFactor, out positions, out sensorData);
            else //no reduction or expanding needed.
            {
                Tube.GetPositions(out positions);
                Tube.Get2DScalarData(scalar, out sensorData);
            }
            
            int radialRepeats = (int)(Math.Ceiling((double)RadialPoints / Tube.Inspection.NumRadialReadings));
            float[,] blendedData = BlendDataPointsFor3DGrid(sensorData, radialRepeats);
            //Create a blank grid
            vtkStructuredGrid newGrid = GridBuilder.CreateEmptyTubeGrid(positions, blendedData.GetLength(1) - 1, diameter, wallThickness, false);
            //Paint the scalars onto the grid
            GridBuilder.AddScalarsToGrid(newGrid, blendedData, 1, scalar.ToString());
            return MapGridToActor(newGrid);
        }

        /// <summary> Creates a 3D VTK grid in the shape of the Tube with diameter data taken from raw radius data </summary>
        public static vtkActor Get3DDiameterTube(ReformerTube Tube, ReformerScalar scalar, float axialSampleFactor = 1, int? RadialPoints = null)
        {
            if (RadialPoints == null) RadialPoints = Tube.Inspection.NumRadialReadings;
            double diameter = Tube.Specs.RadiusInside * 2;
            double wallThickness = Tube.Specs.ThicknessWall;
            //Force the diameter to be at least 1 inch wide to prevent a blank drawing if diameter hasn't been set yet.
            if (diameter.IsNaN() || diameter <= 1) diameter = Math.Max(diameter, 1);
            //Likewise with wall thickness, prevent the points from overlapping.
            if (wallThickness.IsNaN() || wallThickness <= .1) wallThickness = (float)Math.Max(wallThickness, .1);

            List<float> positions;
            float[,] sensorData;
            if (axialSampleFactor > 1) //axial data needs to be reduced
                ReformerTubeDataExt.GetAxiallyReducedData(Tube, scalar, axialSampleFactor, out positions, out sensorData);
            else //no reduction or expanding needed.
            {
                Tube.GetPositions(out positions);
                Tube.Get2DScalarData(scalar, out sensorData);
            }

            float repeats = (float)RadialPoints / Tube.Inspection.NumRadialReadings;
            float radialRepeats = repeats > 1 ? (int)(Math.Ceiling(repeats)) : repeats;
            float[,] blendedData = Get3DDiameterData(sensorData, radialRepeats);
            //Create a blank grid
            vtkStructuredGrid newGrid = GridBuilder.CreateEmptyTubeGrid(positions, blendedData.GetLength(1) - 1, diameter, wallThickness, false);
            //Paint the scalars onto the grid
            GridBuilder.AddScalarsToGrid(newGrid, blendedData, 1, scalar.ToString());
            return MapGridToActor(newGrid);
        }

        private static float[,] BlendDataPointsFor3DGrid(float[,] originalData, int RadialRepeats)
        {
            Stopwatch sw = Stopwatch.StartNew();
            int rows = originalData.GetLength(0), cols = originalData.GetLength(1);

            float[,] repeatedData = new float[rows, cols * RadialRepeats + 1]; // Add 1 to wrap around
            Parallel.For(0, rows, row =>
            //for (int row = 0; row < rows; row++) //left in for debugging, because parallel debugging is difficult.
            {
                for (int col = 0; col < cols; col++)
                {
                    int startColumn = col * RadialRepeats;
                    //linearly interpolate values of points not attached to an actual data point using values in the same slice (0,.33,.66 - 1,1.33,1.66 - 2)

                    //Get the current point and the point after it on the same slice, preventing the column value from attempting to get a column that doesn't exist.
                    float leftValue = originalData[row, col], rightValue = originalData[row, col != cols - 1 ? col + 1 : 0];
                    for (int repeat = 0; repeat < RadialRepeats; repeat++)  // start at i at 1 here, as 0 was taken care of in the line above.
                    {
                        if (repeat == 0) repeatedData[row, startColumn] = originalData[row, col]; //Set the first data point of the column as it's not interpolated.
                        else  //Blend the left and right values based on how close 'i' is to the number of radial repeats that will be performed
                            repeatedData[row, startColumn + repeat] = (leftValue * (RadialRepeats - repeat) / RadialRepeats + rightValue * repeat / RadialRepeats);
                    }

                }
                repeatedData[row, repeatedData.GetLength(1) - 1] = originalData[row, 0]; //Repeat the first column's value in the last column.
            });
            Log.Debug(string.Format("Repeating 2D Radius Scalars took: {0}ms", sw.ElapsedMilliseconds));
            return repeatedData;
        }

        /// <summary> Downsamples data averaging every few sensors samples as indicated by the parameter maintaining the number of slices. Make sure it divides evenly. </summary>
        public static float[,] DownSampleSensorData(float[,] originalData, int SampleEvery)
        {
            int slices = originalData.GetLength(0), sensors = originalData.GetLength(1);
            float[,] outputData = new float[slices, sensors / SampleEvery];

            float runningAverage = 0;
            for (int slice = 0; slice < slices; slice++)
            {
                for (int sensor = 0; sensor < sensors; sensor++)
                {
                    runningAverage += originalData[slice, sensor] / SampleEvery;
                    if (SampleEvery == 1 || (sensor + 1) % SampleEvery == 0)
                    {
                        outputData[slice, (sensor + 1) / SampleEvery - 1] = runningAverage;
                        runningAverage = 0;
                    }
                }
            }

            return outputData;
        }

        /// <summary> Gets 2D scalars for a 3D grid, and allows for up or down sampling based on how many scalar points should be shown in a grid point. Make sampling a clean number, though. </summary>
        public static float[,] Get3DDiameterData(float[,] originalData, float ScalarPointsPerGridPoint)
        {
            float[,] returnData;
            if (ScalarPointsPerGridPoint >= 1)
            {
                int slices = originalData.GetLength(0), sensors = originalData.GetLength(1);
                int numVisiblePoints = (int)Math.Round((sensors * ScalarPointsPerGridPoint) + 1);
                returnData = new float[slices, numVisiblePoints]; // Add 1 to wrap around

                Parallel.For(0, slices - 1, slice =>
                //for (int slice = 0; slice < slices; slice++) //saved for debugging while running, just in case.
                {
                    //Change requested by Mike Flennekin because actual diameter data was causing confusion. Changed to Mean diameter instead by just using radius values.
                    List<float> radii = new List<float>();
                    for (int sensor = 0; sensor < sensors; sensor++)
                        radii.Add(originalData[slice, sensor]);
                    
                    float meanRadius = radii.Mean();

                    for (int visiblePoint = 0; visiblePoint < numVisiblePoints; visiblePoint++)
                        returnData[slice, visiblePoint] = meanRadius * 2;
                });
                //}
            }
            else //Then there's less than 1 scalar per grid point, and we need to downsample before converting to diameter.
            {
                float[,] downSampledRadiusData = DownSampleSensorData(originalData, (int)Math.Round(1 / ScalarPointsPerGridPoint));
                int slices = downSampledRadiusData.GetLength(0), sensors = downSampledRadiusData.GetLength(1);

                returnData = new float[slices, sensors + 1]; // Add 1 to wrap around
                Parallel.For(0, slices - 1, slice =>
                //for (int slice = 0; slice < slices; slice++)
                {
                    List<float> sliceDiams = new List<float>();
                    for (int sensor = 0; sensor < sensors / 2; sensor++)
                    {
                        sliceDiams.Add(downSampledRadiusData[slice, sensor] + downSampledRadiusData[slice, sensor + sensors / 2]);
                    }
                    for (int i = 0; i < sliceDiams.Count; i++) returnData[slice, i] = returnData[slice, i + sensors / 2] = sliceDiams[i];
                    returnData[slice, sensors] = returnData[slice, 0]; //wrap the last point, which is the same as the first.
                    //}
                });
            }
            return returnData;
        }

        public static vtkActor MapGridToActor(vtkStructuredGrid Grid)
        {
            vtkDataSetMapper newMapper = new vtkDataSetMapper();
            newMapper.SetInput(Grid);

            vtkActor newTube = new vtkActor();
            newTube.GetProperty().SetInterpolationToFlat();
            newTube.SetMapper(newMapper);
            return newTube;
        }
    }
}
