
#define MyAppName "LQR"
#define MyAppVerName "LQR 2022.0127"
#define MyAppExeName "Reformer.exe"
#define MyAppVerNum GetFileVersion("..\..\LifeQuest.Reformer\bin\x64\Release\Reformer.exe")
#define MyAppCopyright "Copyright (C) 2022"
#define MyAppMutexName = "Global\LQR"

;concats the Company Name and Application Name to form the standard appdata path
; DO NOT CHANGE THIS
#define CommonApplicationData AddBackSlash("{commonappdata}") + AddBackSlash(MyAppName)

[Setup]
AppName={#MyAppName}
AppVerName={#MyAppVerName}
AppCopyright={#MyAppCopyright}
VersionInfoVersion={#MyAppVerNum}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AppMutex={#MyAppMutexName}
OutputBaseFilename=LQR_setup_x64
AppID={{F850C082-3C7F-4993-8C36-1219AA7BBF72}} 
ArchitecturesInstallIn64BitMode=x64
ArchitecturesAllowed=x64
UninstallDisplayName={#MyAppVerName}    

UninstallDisplayIcon={app}\Reformer.exe
Compression=lzma2/Max
SolidCompression=true

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Dirs]
Name: {#CommonApplicationData}
Name: {#CommonApplicationData}\Templates; Permissions: users-full

[InstallDelete]
;Clean up DevEx files Since DevEx puts the version number in the .dll.
Name: {app}\DevExpress.*; Type: files;
;Clean Nevron files becuase some ancient beta versions used 11.1 instead of 12.2
Name: {app}\Nevron.*; Type: files; 

[UninstallDelete]
Type: filesandordirs; Name: {#CommonApplicationData}\Updates\*;
Type: filesandordirs; Name: {#CommonApplicationData}\*.log;

[Files]
; .NET --used if install needed.  Keep at top for decompression optimization reasons
Source: "dotNetFx45_Full_setup.exe"; DestDir: "{tmp}"; Flags: dontcopy

;Get all the program and DLL files from the release directory.
Source: "..\..\LifeQuest.Reformer\bin\x64\Release\Templates\*"; DestDir: "{app}\Templates"; Flags: ignoreversion skipifsourcedoesntexist; Permissions: users-full
Source: "..\..\LifeQuest.Reformer\bin\x64\Release\Templates\*"; DestDir: "{#CommonApplicationData}\Templates"; Flags: ignoreversion skipifsourcedoesntexist
Source: "..\..\LifeQuest.Reformer\bin\x64\Release\*"; DestDir: "{app}"; Flags: ignoreversion skipifsourcedoesntexist

[Registry]
;Sets mapping of .prjR files to LifeQuest App
Root: HKCR; Subkey: .prjR; ValueType: string; ValueName: ; ValueData: LifeQuestReformer; Flags: uninsdeletevalue createvalueifdoesntexist
Root: HKCR; Subkey: LifeQuestReformer; ValueType: string; ValueName: ; ValueData: LifeQuest Reformer; Flags: uninsdeletekey createvalueifdoesntexist
Root: HKCR; Subkey: LifeQuestReformer\DefaultIcon; ValueType: string; ValueName: ; ValueData: {app}\Reformer.EXE,0
Root: HKCR; Subkey: LifeQuestReformer\shell\open\command; ValueType: string; ValueName: ; ValueData: """{app}\Reformer.EXE"" ""%1"""

[Icons]
Name: {group}\{#MyAppName} (x64); Filename: {app}\{#MyAppExeName};
Name: {commondesktop}\{#MyAppName} (x64); Filename: {app}\{#MyAppExeName}; Tasks: desktopicon

[Run]
Filename: {app}\{#MyAppExeName}; Description: {cm:LaunchProgram,{#MyAppName}}; Flags: nowait postinstall skipifsilent

[Code]
const SMTO_ABORTIFHUNG = $2;
const SPI_SETNONCLIENTMETRICS = $002A;
const WM_SETTINGCHANGE = $001A;

var dotNET45Missing: Boolean;	// Is the .NET 4.5 Framework missing entirely?
var ResultCode: Integer;
var fileVersion: String;
var DotNETVersion: Cardinal;

function SendMessageTimeout(hWnd: Integer;
							 msg: Integer;
							 wParam: Integer;
							 lParam: Integer;
							 fuflags: Integer;
							 uTimeout: Integer;                                                                                       
							 lpdwResult: DWORD ): DWORD;
external 'SendMessageTimeoutA@user32.dll stdcall';   

///Broadcasts message that the icon size has changed
procedure BroadcastChanges;
 var
   success: DWORD;
 begin
   SendMessageTimeout(HWND_BROADCAST,
					  WM_SETTINGCHANGE,
					  SPI_SETNONCLIENTMETRICS,
					  0,
					  SMTO_ABORTIFHUNG,
					  10000,
					  success);        
 end;

///Causes the Icon Cache to be rebuilt.  Does this by changing the icon size'
/// and sending a windows message to refresh
function RefreshScreenIcons : Boolean;
var
  strDataRet, strDataRet2: string;
begin
  Result := False;
  try     
	if RegQueryStringValue(HKEY_CURRENT_USER, 'Control Panel\Desktop\WindowMetrics', 'Shell Icon Size', strDataRet) then
	begin
	  // Convert sDataRet to a number and subtract 1,
	  //    convert back to a string, and write it to the registry
	  strDataRet2 := IntToStr(StrToInt(strDataRet) - 1);
	  RegWriteStringValue(HKEY_CURRENT_USER, 'Control Panel\Desktop\WindowMetrics', 'Shell Icon Size', strDataRet2);
	  // Because the registry was changed, broadcast
	  //    the fact passing SPI_SETNONCLIENTMETRICS,
	  //    with a timeout of 10000 milliseconds (10 seconds)
	  BroadcastChanges;
	  // The4 will have refreshed with the
	  //    new (shrunken) icon size. Now restore things
	  //    back to the correct settings by again writing
	  //    to the registry and posing another message.
	  RegWriteStringValue(HKEY_CURRENT_USER, 'Control Panel\Desktop\WindowMetrics', 'Shell Icon Size', strDataRet);
	  // Broadcast the change again
	  BroadcastChanges;
	  Result := True;
	end;
  finally
  end;
end;


function InitializeSetup(): Boolean;
begin
   // Test the presence of .NET 4.5, the reg key must exist and be greater than 378389 
	if (not(RegKeyExists(HKLM, 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full'))) then dotNET45Missing := True;
  if (RegQueryDWordValue(HKLM, 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full', 'Release', DotNETVersion)) then
  begin
    if (DotNETVersion < 378389) then dotNET45Missing := True;   
  end;
  Result := True;
end;


procedure CurStepChanged(CurStep: TSetupStep);
begin

	//Install .NET 4.5 if needed
	if (CurStep = ssPostInstall) AND (dotNET45Missing) then
	begin
		if MsgBox('Setup needs to install Microsoft .NET Framework 4.5, OK to proceed?', mbConfirmation, MB_YESNO) = IDYES then
		begin
			ExtractTemporaryFile('dotNetFx45_Full_setup.exe');
			Exec(ExpandConstant('{tmp}\dotNetFx45_Full_setup.exe'), '/norestart', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);

			if ResultCode <> 0 then
			begin
			  SysErrorMessage(ResultCode);
			end
		end
		else begin
			MsgBox('Setup cannot continue, please exit and install Microsoft .NET Framework 4.5', mbInformation, MB_OK);
		end;
	end;
	
	//Refresh icons after installation
	if (CurStep = ssDone) then
	begin
	  RefreshScreenIcons;
	end;
	
end;
