﻿#region Copyright Quest Integrity Group, LLC 2012
//
// www.QuestIntegrity.com
// +1-303-415-1475
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
//
// File Created: 9/23/2008 12:14:59 PM
// Created by:   j.rowe
//
#endregion

// Common corporate information

// ***************************************************************************************
// ********* DO NOT CHANGE THIS WITHOUT CONSIDERING SIDE EFFECTS: ************************
// ********* 1) Existing PathExtensions.CommonApplicationDataPath uses this to construct the path.
// ********* 2) Installer needs to match this to install in the right Path
using System.Reflection;

[assembly: AssemblyCompany("Quest Integrity Group")]  //***AGAIN, DON'T CHANGE THIS WITHOUT CONSIDERING THE ABOVE
// ***************************************************************************************

[assembly: AssemblyCopyright("Copyright © Quest Integrity Group 2008-2016")]
[assembly: AssemblyTrademark("LifeQuest is a registered trademark of Quest Integrity Group, LLC")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("2.2.*")]


