
Public Class cPipeTubeReportData
    Private mPTI_Data As PipeTubeReportData

    Public Property Allowance() As String
        Get
            Allowance = mPTI_Data.Allowance
        End Get
        Set(ByVal value As String)
            mPTI_Data.Allowance = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Description = mPTI_Data.Description
        End Get
        Set(ByVal value As String)
            mPTI_Data.Description = value
        End Set
    End Property

    Public Property ReportSummaries() As cPipeTubeSummaries
        Get
            ' instance object as needed
            If (mPTI_Data.ReportSummaries Is Nothing) Then
                mPTI_Data.ReportSummaries = New cPipeTubeSummaries
            End If
            ReportSummaries = mPTI_Data.ReportSummaries
        End Get
        Set(ByVal value As cPipeTubeSummaries)
            mPTI_Data.ReportSummaries = value
        End Set
    End Property

    Public Property InnerDims() As String
        Get
            InnerDims = mPTI_Data.InnerDims
        End Get
        Set(ByVal value As String)
            mPTI_Data.InnerDims = value
        End Set
    End Property

    Public Property Lengths() As String
        Get
            Lengths = mPTI_Data.Lengths
        End Get
        Set(ByVal value As String)
            mPTI_Data.Lengths = value
        End Set
    End Property

    Public Property Material() As String
        Get
            Material = mPTI_Data.Material
        End Get
        Set(ByVal value As String)
            mPTI_Data.Material = value
        End Set
    End Property

    Public Property OuterDims() As String
        Get
            OuterDims = mPTI_Data.OuterDims
        End Get
        Set(ByVal value As String)
            mPTI_Data.OuterDims = value
        End Set
    End Property

    Public Property SectionsInspected() As String
        Get
            SectionsInspected = mPTI_Data.SectionsInspected
        End Get
        Set(ByVal value As String)
            mPTI_Data.SectionsInspected = value
        End Set
    End Property

    Public Property PutIntoServiceDate() As String
        Get
            PutIntoServiceDate = mPTI_Data.PutIntoServiceDate
        End Get
        Set(ByVal value As String)
            mPTI_Data.PutIntoServiceDate = value
        End Set
    End Property
    Public Property TolerancesProvidedBy() As Integer
        Get
            TolerancesProvidedBy = mPTI_Data.TolerancedProvidedBy
        End Get
        Set(ByVal value As Integer)
            mPTI_Data.TolerancedProvidedBy = value
        End Set
    End Property

    Public Property Surface() As String
        Get
            Surface = mPTI_Data.Surface
        End Get
        Set(ByVal value As String)
            mPTI_Data.Surface = value
        End Set
    End Property

    Public Property InspectionThreshold() As String
        Get
            InspectionThreshold = mPTI_Data.InspectionThreshold
        End Get
        Set(ByVal value As String)
            mPTI_Data.InspectionThreshold = value
        End Set
    End Property
    Public Property Thickness() As String
        Get
            Thickness = mPTI_Data.Thickness
        End Get
        Set(ByVal value As String)
            mPTI_Data.Thickness = value
        End Set
    End Property
    Public Property Manufacturer() As String
        Get
            Manufacturer = mPTI_Data.Manufacturer
        End Get
        Set(ByVal value As String)
            mPTI_Data.Manufacturer = value
        End Set
    End Property
    Public Property OrientationOrDirection() As String
        Get
            OrientationOrDirection = mPTI_Data.OrientationOrDirection
        End Get
        Set(ByVal value As String)
            mPTI_Data.OrientationOrDirection = value
        End Set
    End Property
    Public Property Pressure() As String
        Get
            Pressure = mPTI_Data.Pressure
        End Get
        Set(ByVal value As String)
            mPTI_Data.Pressure = value
        End Set
    End Property
    Public Property TargetTemperature() As String
        Get
            TargetTemperature = mPTI_Data.TargetTemperature
        End Get
        Set(ByVal value As String)
            mPTI_Data.TargetTemperature = value
        End Set
    End Property
    Public Property DegreesMarker() As Integer
        Get
            DegreesMarker = mPTI_Data.DegreesMarker
        End Get
        Set(ByVal value As Integer)
            mPTI_Data.DegreesMarker = value
        End Set
    End Property
    Public Property InspectionLocations() As String
        Get
            InspectionLocations = mPTI_Data.InspectionLocations
        End Get
        Set(ByVal value As String)
            mPTI_Data.InspectionLocations = value
        End Set
    End Property
    Public Property AgeAtInspection() As String
        Get
            AgeAtInspection = mPTI_Data.AgeAtInspection
        End Get
        Set(ByVal value As String)
            mPTI_Data.AgeAtInspection = value
        End Set
    End Property
    Public Property NumberThermalCycles() As String
        Get
            NumberThermalCycles = mPTI_Data.NumberThermalCycles
        End Get
        Set(ByVal value As String)
            mPTI_Data.NumberThermalCycles = value
        End Set
    End Property
    Public Property OperatingHours() As String
        Get
            OperatingHours = mPTI_Data.OperatingHours
        End Get
        Set(ByVal value As String)
            mPTI_Data.OperatingHours = value
        End Set
    End Property
    Public Property NextInspectionDate() As String
        Get
            NextInspectionDate = mPTI_Data.NextInspectionDate
        End Get
        Set(ByVal value As String)
            mPTI_Data.NextInspectionDate = value
        End Set
    End Property
    Public Property NumberTubesInspected() As Integer
        Get
            NumberTubesInspected = mPTI_Data.NumberTubesInspected
        End Get
        Set(ByVal value As Integer)
            mPTI_Data.NumberTubesInspected = value
        End Set
    End Property
    Public Property NumberTubesTotal() As Integer
        Get
            NumberTubesTotal = mPTI_Data.NumberTubesTotal
        End Get
        Set(ByVal value As Integer)
            mPTI_Data.NumberTubesTotal = value
        End Set
    End Property
End Class
