Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.Data.Filtering
Imports System.Threading

Public Class Main

    Private Sub QTT_Main_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' check for any un-closed inspections:
        Dim CurrentOpenInspections As cOpenInspections = modProgram.CurrentOpenInspections()
        Dim NumOpen As Integer = CurrentOpenInspections.Count
        If NumOpen > 1 Then
            If MsgBoxResult.No = MsgBox("You still have " & CurrentOpenInspections.Count & " Inspections open.  Are you sure that you want to exit AutoReporter?", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2) Then
                e.Cancel = True
                Exit Sub
            End If
        ElseIf NumOpen = 1 Then
            If MsgBoxResult.No = MsgBox("You still have one Inspection open.  Are you sure that you want to exit AutoReporter?", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2) Then
                e.Cancel = True
                Exit Sub
            End If
        End If

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Calls from Load are perhaps causing this error because form hasn't finished loading. Moved to Shown event 7/7/13 to see if it can be prevented.
        'If this doesn't work, try Solution 3 at http://www.codeproject.com/Questions/326541/Invoke-or-BeginInvoke-Exception
        'Invoke or BeginInvoke cannot be called on a control until the window handle has been created.'

        'Call SetupUI_Main()
        'modProgram.MainForm = Me
        DBUpdater.UpdateAddresses()
        DBUpdater.UpdateInspectors()
    End Sub
    Private Sub Main_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        Call SetupUI_Main()
        modProgram.MainForm = Me

    End Sub
    Private Sub SetupUI_Main()

        ' Const DefaultInspectionType = 4

        Const DB_ERROR_MSG = "Could not access the local AutoReporter database. If you recently installed a new version of the AutoReporter software, removing your old 'Log' database may solve this problem."
        Dim fDB_FAILURE As Boolean = False

        'Application title
        If My.Application.Info.Title <> "" Then
            ApplicationTitle.Text = My.Application.Info.Title
        Else
            'If the application title is missing, use the application name, without the extension
            ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If

        'get version
        Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)
        Try
            'This is the first access to the database - if there is an issue, trap it and warn user.
            Me.TblInspectionTypesTableAdapter.FillBy_ActiveTypes(Me.QTT_InspectionsDataSets.tblInspectionTypes)

        Catch ex As Exception
            MsgBox(DB_ERROR_MSG & vbNewLine & vbNewLine & "Error Details: " & ex.Message, MsgBoxStyle.Exclamation, "QIG Database Connection Failure.")
            fDB_FAILURE = True
        End Try

        If fDB_FAILURE Then
            MsgBox("Quest Integrity AutoReporter will now close.  Try to run this program again when you have resolved the database error(s).", MsgBoxStyle.Information, "No Valid Database")
            Application.Exit()
            Return
        End If

        'This line of code loads data into the 'QTT_InspectionsDataSets.tblCustomers' table. You can move, or remove it, as needed.
        Me.TblCustomersTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblCustomers)

        AddHandler uiGridViewInspections.CustomFilterDialog, AddressOf GridView_CustomFilterDialog
        AddHandler uiGridViewInspections.ShowFilterPopupDate, AddressOf GridView_ShowFilterPopupDate

        'This line of code loads data into the 'QTT_InspectionsDataSets.tblInspections' table. You can move, or remove it, as needed.
        TblInspectionsTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblInspections)
        uiGridViewInspections.BestFitColumns()

    End Sub

    Private Sub StartNewInspection(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiCreateInspectionButton.Click   'KM 7/1/2011
        NewInspection.Show()
    End Sub

    Private Sub ExitProgram(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiExitButton.Click
        Application.Exit()
    End Sub

    Sub ShowSelectedInspection()

        If uiGridViewInspections.SelectedRowsCount = 0 Then
            MsgBox("No Inspection is currently selected.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim intInspectionID As Integer = CLng(uiGridViewInspections.GetDataRow(uiGridViewInspections.FocusedRowHandle).Item(0))
        Dim intInspectionType As Integer = CLng(uiGridViewInspections.GetDataRow(uiGridViewInspections.FocusedRowHandle).Item(1))
        OpenQTTInspection(intInspectionID, intInspectionType, False)
        Me.Cursor = Cursors.Default

    End Sub


    Public Sub ApplyFilters(Optional ByVal SelectInspectionID As Integer = 0)
        Dim intInspectionType As Integer = 0
        Dim intCustomerID As Integer = 0
        Dim datInspectionStarted As Date = "1/1/1900"

        TblInspectionsTableAdapter.FillBy_TypeCustomerDate(QTT_InspectionsDataSets.tblInspections, intCustomerID, intInspectionType, datInspectionStarted)

        If SelectInspectionID <> 0 Then
            For intRow As Integer = 0 To (uiGridViewInspections.RowCount - 1)
                If SelectInspectionID = CLng(uiGridViewInspections.GetRowCellValue(intRow, "ID")) Then
                    uiGridViewInspections.FocusedRowHandle = intRow
                    Exit For
                End If
            Next
        End If

        uiGridViewInspections.BestFitColumns()

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        'JHF 8/8/2011 Added Password requirement to modify the database
        PWEntry.ShowDialog()

    End Sub

    Private Sub uiGridControlInspections_DoubleClick(sender As Object, e As System.EventArgs) Handles uiGridControlInspections.DoubleClick

        Call ShowSelectedInspection()

    End Sub

    Private Sub uiOpenInspectionButton_Click(sender As System.Object, e As System.EventArgs) Handles uiOpenInspectionButton.Click

        Call ShowSelectedInspection()

    End Sub

    Private Sub GridView_CustomFilterDialog(ByVal sender As Object, ByVal e As CustomFilterDialogEventArgs)

        e.Handled = True
        DirectCast(sender, GridView).ShowFilterEditor(e.Column)

    End Sub

    Private Sub GridView_ShowFilterPopupDate(ByVal sender As System.Object, ByVal e As FilterPopupDateEventArgs)

        'Add filter option for dates
        Select Case e.Column.FieldName
            'Customize filter for date columns to show past week, past month, past year
            Case "InspectionDateStart"
                e.List.Clear()
                Dim Today As DateTime = DateTime.Today

                Dim PastWeek As Date = Today.Subtract(TimeSpan.FromDays(7))
                Dim crit As CriteriaOperator = New BinaryOperator(e.Column.FieldName, PastWeek, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Week", "", crit))

                Dim PastMonth As DateTime = New DateTime(DateTime.Today.Year, DateTime.Today.Month - 1, DateTime.Today.Day)
                crit = New BinaryOperator(e.Column.FieldName, PastMonth, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Month", "", crit))

                Dim PastYear As DateTime = New DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day)
                crit = New BinaryOperator(e.Column.FieldName, PastYear, BinaryOperatorType.GreaterOrEqual)
                e.List.Add(New DevExpress.XtraEditors.FilterDateElement("Past Year", "", crit))

            Case Else
                'As needed, add other custom filters for other columns

        End Select

    End Sub
    Public Sub UpdateInspections()

        Me.TblInspectionsTableAdapter.Fill(Me.QTT_InspectionsDataSets.tblInspections)

    End Sub
    Private Sub uiDeleteInspection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiDeleteInspection.Click

        ' deletes the inspection
        Dim strDelPath As String = ""
        Dim fDidNotMoveFiles As Boolean = True

        ' get ID to delete
        Dim drvTemp As DataRowView = bsInspections.Current
        Dim curInsp As QTT_InspectionsDataSets.tblInspectionsRow = drvTemp.Row
        Dim intInspectionID As Integer = curInsp.ID
        If intInspectionID = 0 Then
            MsgBox("Cannot Delete this inspection.  Error - Inspection ID not found")
            Exit Sub
        End If
        Dim strInspRootName As String = ""

        If Not curInsp.IsRootFolderNameNull Then
            strInspRootName = curInsp.RootFolderName
            fDidNotMoveFiles = False
        End If

        ' confirm with user
        If MsgBoxResult.Cancel = MsgBox("Are you sure that you want to PERMANENTLY DELETE this inspection?  (You cannot undo this action)", MsgBoxStyle.Exclamation + MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, _
                "PERMANENTLY Delete Inspection?") Then
            Exit Sub
        End If

        ' move folder structure
        If Len(strInspRootName) > 0 Then
            With My.Computer.FileSystem
                ' create deleted folder as needed
                Dim strRootPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & My.Settings.RootDataFolder
                Dim strFolderToMove = My.Computer.FileSystem.CombinePath(strRootPath, strInspRootName)
                strDelPath = .CombinePath(strRootPath, "xDeletedInspections")
                strDelPath = .CombinePath(strDelPath, strInspRootName)
                Try
                    'MsgBox("Trying to move: " & strFolderToMove & " to: " & strDelPath, MsgBoxStyle.Information, "Inspection Deleting.")
                    .MoveDirectory(strFolderToMove, strDelPath)
                Catch
                    ' MsgBox("Error occurred trying to move the root folder for the deleted project.", MsgBoxStyle.Information)
                    fDidNotMoveFiles = True
                End Try

            End With
        End If

        ' delete the item
        If TblInspectionsTableAdapter.DeleteInspection(intInspectionID).Value <> 0 Then
            MsgBox("Unexpected result returned from Delete Inspection database procedure.", MsgBoxStyle.Information)
        ElseIf Len(strDelPath) > 0 Then
            If fDidNotMoveFiles Then
                ' MsgBox("The Inspection was successfully deleted - the inspection root folder (and all files and sub-folders) was not moved - these files may not exist on this computer.", MsgBoxStyle.Information, "Inspection Deleted")
                bsInspections.RemoveCurrent()
                TblInspectionsTableAdapter.Update(QTT_InspectionsDataSets.Tables(bsInspections.DataMember))
            Else
                'MsgBox("Inspection successfully deleted - the inspection root folder (and all files and sub-folders) were moved to the folder: " & strDelPath, MsgBoxStyle.Information, "Inspection Deleted")
                bsInspections.RemoveCurrent()
                TblInspectionsTableAdapter.Update(QTT_InspectionsDataSets.Tables(bsInspections.DataMember))
            End If
        Else
            ' MsgBox("Inspection successfully deleted.", MsgBoxStyle.Information, "Inspection Deleted")
            bsInspections.RemoveCurrent()
            TblInspectionsTableAdapter.Update(QTT_InspectionsDataSets.Tables(bsInspections.DataMember))
        End If

    End Sub


End Class