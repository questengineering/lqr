﻿Imports DevExpress.XtraEditors

Public Class ucProjectSummary
    Dim MySummaryData As SummaryData

#Region "Form Load Subroutines"

    Private Sub ucProjectSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        'Fill the table adapater containing data associated with all the editboxes.
        ' Me.TblDefaultText_ListItemsTableAdapter1.Fill(QtT_LookupsData1.tblDefaultText_ListItems)
        'Set up the ComboBoxes with the items they should have. Obtained from the Database

        MySummaryData = CreateDataSummary()

    End Sub
    
#End Region
#Region "Update Text Buttons"
    Private Sub btnConPipesUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConPipesUpdate.Click
        uiSummaryConPipeTextbox.Text = WriteUpSummaryForMe(mysummarydata, "Convective", "Pipe")
    End Sub
    Private Sub btnRadPipesUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRadPipesUpdate.Click
        uiSummaryRadPipeTextbox.Text = WriteUpSummaryForMe(mysummarydata, "Radiant", "Pipe")
    End Sub
    Private Sub btnConBendsUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConBendsUpdate.Click
        uiSummaryConBendTextbox.Text = WriteUpSummaryForMe(mysummarydata, "Convective", "Bend")
    End Sub

    Private Sub btnRadBendsUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRadBendsUpdate.Click
        uiSummaryRadBendTextbox.Text = WriteUpSummaryForMe(mysummarydata, "Radiant", "Bend")
    End Sub
#End Region
#Region "General Input Changed Events"
    Private Sub uiSummaryWriteup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSummaryWriteUp.SelectedIndexChanged
        mysummarydata.GeneralOrSectionSummary = uiSummaryWriteUp.Text
    End Sub
    Private Sub uiPassNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiPassNumber.SelectedIndexChanged
        mysummarydata.PassNumber = uiPassNumber.Text
    End Sub

    Private Sub uiUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiUnits.SelectedIndexChanged
        mysummarydata.Units = uiUnits.Text
    End Sub

    Private Sub uiAppendix_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAppendix.SelectedIndexChanged
        mysummarydata.Appendix = uiAppendix.Text
    End Sub

    Private Sub uiNewUsedPipes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiNewUsedPipes.SelectedIndexChanged
        mysummarydata.NewUsedPipes = uiNewUsedPipes.Text
    End Sub
#End Region
#Region "Convective Pipe ID Texbox Changed Events"
    Private Sub txtConPipeID0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID0.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(0) = txtConPipeID0.Text
    End Sub
    Private Sub txtConPipeID1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID1.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(1) = txtConPipeID1.Text
    End Sub
    Private Sub txtConPipeID2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID2.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(2) = txtConPipeID2.Text
    End Sub
    Private Sub txtConPipeID3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID3.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(3) = txtConPipeID3.Text
    End Sub
    Private Sub txtConPipeID4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID4.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(4) = txtConPipeID4.Text
    End Sub
    Private Sub txtConPipeID5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID5.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(5) = txtConPipeID5.Text
    End Sub
    Private Sub txtConPipeID6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID6.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(6) = txtConPipeID6.Text
    End Sub
    Private Sub txtConPipeID7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeID7.EditValueChanged
        mysummarydata.MyConvectivePipeData.ID(7) = txtConPipeID7.Text
    End Sub
#End Region
#Region "Convective Pipe MinWall Changed Events"
    Private Sub txtConPipeMinWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall0.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(0) = txtConPipeMinWall0.Text
    End Sub
    Private Sub txtConPipeMinWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall1.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(1) = txtConPipeMinWall1.Text
    End Sub
    Private Sub txtConPipeMinWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall2.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(2) = txtConPipeMinWall2.Text
    End Sub
    Private Sub txtConPipeMinWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall3.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(3) = txtConPipeMinWall3.Text
    End Sub
    Private Sub txtConPipeMinWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall4.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(4) = txtConPipeMinWall4.Text
    End Sub
    Private Sub txtConPipeMinWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall5.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(5) = txtConPipeMinWall5.Text
    End Sub
    Private Sub txtConPipeMinWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall6.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(6) = txtConPipeMinWall6.Text
    End Sub
    Private Sub txtConPipeMinWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeMinWall7.EditValueChanged
        mysummarydata.MyConvectivePipeData.MinWall(7) = txtConPipeMinWall7.Text
    End Sub
#End Region
#Region "Convective Pipe Location Changed Events"
    Private Sub txtConPipeLocation0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation0.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(0) = txtConPipeLocation0.Text
    End Sub
    Private Sub txtConPipeLocation1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation1.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(1) = txtConPipeLocation1.Text
    End Sub
    Private Sub txtConPipeLocation2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation2.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(2) = txtConPipeLocation2.Text
    End Sub
    Private Sub txtConPipeLocation3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation3.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(3) = txtConPipeLocation3.Text
    End Sub
    Private Sub txtConPipeLocation4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation4.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(4) = txtConPipeLocation4.Text
    End Sub
    Private Sub txtConPipeLocation5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation5.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(5) = txtConPipeLocation5.Text
    End Sub
    Private Sub txtConPipeLocation6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation6.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(6) = txtConPipeLocation6.Text
    End Sub
    Private Sub txtConPipeLocation7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeLocation7.EditValueChanged
        mysummarydata.MyConvectivePipeData.Location(7) = txtConPipeLocation7.Text
    End Sub
#End Region
#Region "Convective Pipe Estimated Wall Loss Changed Events"
    Private Sub txtConPipeEstWallLoss0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss0.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(0) = txtConPipeEstWallLoss0.Text
    End Sub
    Private Sub txtConPipeEstWallLoss1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss1.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(1) = txtConPipeEstWallLoss1.Text
    End Sub
    Private Sub txtConPipeEstWallLoss2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss2.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(2) = txtConPipeEstWallLoss2.Text
    End Sub
    Private Sub txtConPipeEstWallLoss3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss3.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(3) = txtConPipeEstWallLoss3.Text
    End Sub
    Private Sub txtConPipeEstWallLoss4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss4.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(4) = txtConPipeEstWallLoss4.Text
    End Sub
    Private Sub txtConPipeEstWallLoss5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss5.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(5) = txtConPipeEstWallLoss5.Text
    End Sub
    Private Sub txtConPipeEstWallLoss6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss6.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(6) = txtConPipeEstWallLoss6.Text
    End Sub
    Private Sub txtConPipeEstWallLoss7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstWallLoss7.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstWallLoss(7) = txtConPipeEstWallLoss7.Text
    End Sub
#End Region
#Region "Convective Pipe Remaining Wall Thickness Changed Events"
    Private Sub txtConPipeEstRemWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall0.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(0) = txtConPipeEstRemWall0.Text
    End Sub
    Private Sub txtConPipeEstRemWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall1.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(1) = txtConPipeEstRemWall1.Text
    End Sub
    Private Sub txtConPipeEstRemWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall2.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(2) = txtConPipeEstRemWall2.Text
    End Sub
    Private Sub txtConPipeEstRemWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall3.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(3) = txtConPipeEstRemWall3.Text
    End Sub
    Private Sub txtConPipeEstRemWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall4.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(4) = txtConPipeEstRemWall4.Text
    End Sub
    Private Sub txtConPipeEstRemWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall5.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(5) = txtConPipeEstRemWall5.Text
    End Sub
    Private Sub txtConPipeEstRemWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall6.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(6) = txtConPipeEstRemWall6.Text
    End Sub
    Private Sub txtConPipeEstRemWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeEstRemWall7.EditValueChanged
        mysummarydata.MyConvectivePipeData.EstRemWall(7) = txtConPipeEstRemWall7.Text
    End Sub

#End Region
#Region "Convective Pipe Average Wall Thickness Changed Events"

    Private Sub txtConPipeAvgWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall0.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(0) = txtConPipeAvgWall0.Text
    End Sub
    Private Sub txtConPipeAvgWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall1.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(1) = txtConPipeAvgWall1.Text
    End Sub
    Private Sub txtConPipeAvgWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall2.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(2) = txtConPipeAvgWall2.Text
    End Sub
    Private Sub txtConPipeAvgWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall3.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(3) = txtConPipeAvgWall3.Text
    End Sub
    Private Sub txtConPipeAvgWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall4.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(4) = txtConPipeAvgWall4.Text
    End Sub
    Private Sub txtConPipeAvgWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall5.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(5) = txtConPipeAvgWall5.Text
    End Sub
    Private Sub txtConPipeAvgWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall6.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(6) = txtConPipeAvgWall6.Text
    End Sub
    Private Sub txtConPipeAvgWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgWall7.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgWall(7) = txtConPipeAvgWall7.Text
    End Sub
#End Region
#Region "Convective Pipe Designed Wall Thickness Changed Events"
    Private Sub txtConPipeDesWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall0.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(0) = txtConPipeDesWall0.Text
    End Sub
    Private Sub txtConPipeDesWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall1.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(1) = txtConPipeDesWall1.Text
    End Sub
    Private Sub txtConPipeDesWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall2.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(2) = txtConPipeDesWall2.Text
    End Sub
    Private Sub txtConPipeDesWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall3.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(3) = txtConPipeDesWall3.Text
    End Sub
    Private Sub txtConPipeDesWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall4.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(4) = txtConPipeDesWall4.Text
    End Sub
    Private Sub txtConPipeDesWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall5.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(5) = txtConPipeDesWall5.Text
    End Sub
    Private Sub txtConPipeDesWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall6.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(6) = txtConPipeDesWall6.Text
    End Sub
    Private Sub txtConPipeDesWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeDesWall7.EditValueChanged
        mysummarydata.MyConvectivePipeData.DesignedWall(7) = txtConPipeDesWall7.Text
    End Sub
#End Region
#Region "Convective Pipe Average Diameter Changed Events"
    Private Sub txtConPipeAvgDiameter0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter0.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(0) = txtConPipeAvgDiameter0.Text
    End Sub
    Private Sub txtConPipeAvgDiameter1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter1.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(1) = txtConPipeAvgDiameter1.Text
    End Sub
    Private Sub txtConPipeAvgDiameter2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter2.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(2) = txtConPipeAvgDiameter2.Text
    End Sub
    Private Sub txtConPipeAvgDiameter3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter3.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(3) = txtConPipeAvgDiameter3.Text
    End Sub
    Private Sub txtConPipeAvgDiameter4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter4.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(4) = txtConPipeAvgDiameter4.Text
    End Sub
    Private Sub txtConPipeAvgDiameter5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter5.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(5) = txtConPipeAvgDiameter5.Text
    End Sub
    Private Sub txtConPipeAvgDiameter6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter6.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(6) = txtConPipeAvgDiameter6.Text
    End Sub
    Private Sub txtConPipeAvgDiameter7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConPipeAvgDiameter7.EditValueChanged
        mysummarydata.MyConvectivePipeData.AvgDiameter(7) = txtConPipeAvgDiameter7.Text
    End Sub
#End Region
#Region "Convective Pipe General Input Changed Events"
    Private Sub uiConPipeGeneralOrLocal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConPipeGeneralOrLocal.SelectedIndexChanged
        mysummarydata.MyConvectivePipeData.GeneralOrLocal = uiConPipeGeneralOrLocal.Text
    End Sub

    Private Sub uiConPipeInternalOrExternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConPipeInternalOrExternal.SelectedIndexChanged
        mysummarydata.MyConvectivePipeData.InternalOrExternal = uiConPipeInternalOrExternal.Text
    End Sub

    Private Sub uiConPipePipeSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConPipePipeSize.SelectedIndexChanged
        mysummarydata.MyConvectivePipeData.PipeSize = uiConPipePipeSize.Text
    End Sub

    Private Sub uiConPipeScheduleSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConPipeScheduleSize.SelectedIndexChanged
        mysummarydata.MyConvectivePipeData.PipeSchedule = uiConPipeScheduleSize.Text
    End Sub
#End Region
#Region "Radiant Pipe ID Texbox Changed Events"
    Private Sub txtRadPipeID0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID0.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(0) = txtRadPipeID0.Text
    End Sub
    Private Sub txtRadPipeID1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID1.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(1) = txtRadPipeID1.Text
    End Sub
    Private Sub txtRadPipeID2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID2.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(2) = txtRadPipeID2.Text
    End Sub
    Private Sub txtRadPipeID3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID3.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(3) = txtRadPipeID3.Text
    End Sub
    Private Sub txtRadPipeID4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID4.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(4) = txtRadPipeID4.Text
    End Sub
    Private Sub txtRadPipeID5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID5.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(5) = txtRadPipeID5.Text
    End Sub
    Private Sub txtRadPipeID6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID6.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(6) = txtRadPipeID6.Text
    End Sub
    Private Sub txtRadPipeID7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeID7.EditValueChanged
        mysummarydata.MyRadiantPipeData.ID(7) = txtRadPipeID7.Text
    End Sub
#End Region
#Region "Radiant Pipe MinWall Changed Events"
    Private Sub txtRadPipeMinWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall0.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(0) = txtRadPipeMinWall0.Text
    End Sub
    Private Sub txtRadPipeMinWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall1.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(1) = txtRadPipeMinWall1.Text
    End Sub
    Private Sub txtRadPipeMinWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall2.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(2) = txtRadPipeMinWall2.Text
    End Sub
    Private Sub txtRadPipeMinWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall3.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(3) = txtRadPipeMinWall3.Text
    End Sub
    Private Sub txtRadPipeMinWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall4.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(4) = txtRadPipeMinWall4.Text
    End Sub
    Private Sub txtRadPipeMinWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall5.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(5) = txtRadPipeMinWall5.Text
    End Sub
    Private Sub txtRadPipeMinWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall6.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(6) = txtRadPipeMinWall6.Text
    End Sub
    Private Sub txtRadPipeMinWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeMinWall7.EditValueChanged
        mysummarydata.MyRadiantPipeData.MinWall(7) = txtRadPipeMinWall7.Text
    End Sub
#End Region
#Region "Radiant Pipe Location Changed Events"
    Private Sub txtRadPipeLocation0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation0.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(0) = txtRadPipeLocation0.Text
    End Sub
    Private Sub txtRadPipeLocation1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation1.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(1) = txtRadPipeLocation1.Text
    End Sub
    Private Sub txtRadPipeLocation2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation2.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(2) = txtRadPipeLocation2.Text
    End Sub
    Private Sub txtRadPipeLocation3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation3.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(3) = txtRadPipeLocation3.Text
    End Sub
    Private Sub txtRadPipeLocation4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation4.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(4) = txtRadPipeLocation4.Text
    End Sub
    Private Sub txtRadPipeLocation5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation5.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(5) = txtRadPipeLocation5.Text
    End Sub
    Private Sub txtRadPipeLocation6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation6.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(6) = txtRadPipeLocation6.Text
    End Sub
    Private Sub txtRadPipeLocation7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeLocation7.EditValueChanged
        mysummarydata.MyRadiantPipeData.Location(7) = txtRadPipeLocation7.Text
    End Sub
#End Region
#Region "Radiant Pipe Estimated Wall Loss Changed Events"
    Private Sub txtRadPipeEstWallLoss0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss0.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(0) = txtRadPipeEstWallLoss0.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss1.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(1) = txtRadPipeEstWallLoss1.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss2.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(2) = txtRadPipeEstWallLoss2.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss3.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(3) = txtRadPipeEstWallLoss3.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss4.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(4) = txtRadPipeEstWallLoss4.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss5.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(5) = txtRadPipeEstWallLoss5.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss6.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(6) = txtRadPipeEstWallLoss6.Text
    End Sub
    Private Sub txtRadPipeEstWallLoss7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstWallLoss7.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstWallLoss(7) = txtRadPipeEstWallLoss7.Text
    End Sub
#End Region
#Region "Radiant Pipe Remaining Wall Thickness Changed Events"
    Private Sub txtRadPipeEstRemWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall0.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(0) = txtRadPipeEstRemWall0.Text
    End Sub
    Private Sub txtRadPipeEstRemWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall1.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(1) = txtRadPipeEstRemWall1.Text
    End Sub
    Private Sub txtRadPipeEstRemWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall2.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(2) = txtRadPipeEstRemWall2.Text
    End Sub
    Private Sub txtRadPipeEstRemWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall3.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(3) = txtRadPipeEstRemWall3.Text
    End Sub
    Private Sub txtRadPipeEstRemWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall4.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(4) = txtRadPipeEstRemWall4.Text
    End Sub
    Private Sub txtRadPipeEstRemWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall5.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(5) = txtRadPipeEstRemWall5.Text
    End Sub
    Private Sub txtRadPipeEstRemWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall6.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(6) = txtRadPipeEstRemWall6.Text
    End Sub
    Private Sub txtRadPipeEstRemWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeEstRemWall7.EditValueChanged
        mysummarydata.MyRadiantPipeData.EstRemWall(7) = txtRadPipeEstRemWall7.Text
    End Sub

#End Region
#Region "Radiant Pipe Average Wall Thickness Changed Events"

    Private Sub txtRadPipeAvgWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall0.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(0) = txtRadPipeAvgWall0.Text
    End Sub
    Private Sub txtRadPipeAvgWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall1.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(1) = txtRadPipeAvgWall1.Text
    End Sub
    Private Sub txtRadPipeAvgWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall2.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(2) = txtRadPipeAvgWall2.Text
    End Sub
    Private Sub txtRadPipeAvgWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall3.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(3) = txtRadPipeAvgWall3.Text
    End Sub
    Private Sub txtRadPipeAvgWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall4.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(4) = txtRadPipeAvgWall4.Text
    End Sub
    Private Sub txtRadPipeAvgWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall5.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(5) = txtRadPipeAvgWall5.Text
    End Sub
    Private Sub txtRadPipeAvgWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall6.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(6) = txtRadPipeAvgWall6.Text
    End Sub
    Private Sub txtRadPipeAvgWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgWall7.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgWall(7) = txtRadPipeAvgWall7.Text
    End Sub
#End Region
#Region "Radiant Pipe Designed Wall Thickness Changed Events"
    Private Sub txtRadPipeDesWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall0.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(0) = txtRadPipeDesWall0.Text
    End Sub
    Private Sub txtRadPipeDesWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall1.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(1) = txtRadPipeDesWall1.Text
    End Sub
    Private Sub txtRadPipeDesWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall2.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(2) = txtRadPipeDesWall2.Text
    End Sub
    Private Sub txtRadPipeDesWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall3.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(3) = txtRadPipeDesWall3.Text
    End Sub
    Private Sub txtRadPipeDesWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall4.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(4) = txtRadPipeDesWall4.Text
    End Sub
    Private Sub txtRadPipeDesWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall5.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(5) = txtRadPipeDesWall5.Text
    End Sub
    Private Sub txtRadPipeDesWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall6.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(6) = txtRadPipeDesWall6.Text
    End Sub
    Private Sub txtRadPipeDesWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeDesWall7.EditValueChanged
        mysummarydata.MyRadiantPipeData.DesignedWall(7) = txtRadPipeDesWall7.Text
    End Sub
#End Region
#Region "Radiant Pipe Average Diameter Changed Events"
    Private Sub txtRadPipeAvgDiameter0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter0.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(0) = txtRadPipeAvgDiameter0.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter1.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(1) = txtRadPipeAvgDiameter1.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter2.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(2) = txtRadPipeAvgDiameter2.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter3.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(3) = txtRadPipeAvgDiameter3.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter4.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(4) = txtRadPipeAvgDiameter4.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter5.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(5) = txtRadPipeAvgDiameter5.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter6.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(6) = txtRadPipeAvgDiameter6.Text
    End Sub
    Private Sub txtRadPipeAvgDiameter7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadPipeAvgDiameter7.EditValueChanged
        mysummarydata.MyRadiantPipeData.AvgDiameter(7) = txtRadPipeAvgDiameter7.Text
    End Sub
#End Region
#Region "Radiant Pipe General Input Changed Events"
    Private Sub uiRadPipeGeneralOrLocal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadPipeGeneralOrLocal.SelectedIndexChanged
        mysummarydata.MyRadiantPipeData.GeneralOrLocal = uiRadPipeGeneralOrLocal.Text
    End Sub

    Private Sub uiRadPipeInternalOrExternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadPipeInternalOrExternal.SelectedIndexChanged
        mysummarydata.MyRadiantPipeData.InternalOrExternal = uiRadPipeInternalOrExternal.Text
    End Sub

    Private Sub uiRadPipePipeSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadPipePipeSize.SelectedIndexChanged
        mysummarydata.MyRadiantPipeData.PipeSize = uiRadPipePipeSize.Text
    End Sub

    Private Sub uiRadPipeScheduleSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadPipeScheduleSize.SelectedIndexChanged
        mysummarydata.MyRadiantPipeData.PipeSchedule = uiRadPipeScheduleSize.Text
    End Sub
#End Region

#Region "Convective Bend ID Texbox Changed Events"
    Private Sub txtConBendID0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID0.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(0) = txtConBendID0.Text
    End Sub
    Private Sub txtConBendID1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID1.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(1) = txtConBendID1.Text
    End Sub
    Private Sub txtConBendID2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID2.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(2) = txtConBendID2.Text
    End Sub
    Private Sub txtConBendID3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID3.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(3) = txtConBendID3.Text
    End Sub
    Private Sub txtConBendID4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID4.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(4) = txtConBendID4.Text
    End Sub
    Private Sub txtConBendID5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID5.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(5) = txtConBendID5.Text
    End Sub
    Private Sub txtConBendID6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID6.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(6) = txtConBendID6.Text
    End Sub
    Private Sub txtConBendID7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendID7.EditValueChanged
        mysummarydata.MyConvectiveBendData.ID(7) = txtConBendID7.Text
    End Sub
#End Region
#Region "Convective Bend MinWall Changed Events"
    Private Sub txtConBendMinWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall0.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(0) = txtConBendMinWall0.Text
    End Sub
    Private Sub txtConBendMinWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall1.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(1) = txtConBendMinWall1.Text
    End Sub
    Private Sub txtConBendMinWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall2.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(2) = txtConBendMinWall2.Text
    End Sub
    Private Sub txtConBendMinWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall3.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(3) = txtConBendMinWall3.Text
    End Sub
    Private Sub txtConBendMinWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall4.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(4) = txtConBendMinWall4.Text
    End Sub
    Private Sub txtConBendMinWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall5.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(5) = txtConBendMinWall5.Text
    End Sub
    Private Sub txtConBendMinWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall6.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(6) = txtConBendMinWall6.Text
    End Sub
    Private Sub txtConBendMinWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendMinWall7.EditValueChanged
        mysummarydata.MyConvectiveBendData.MinWall(7) = txtConBendMinWall7.Text
    End Sub
#End Region
#Region "Convective Bend Estimated Wall Loss Changed Events"
    Private Sub txtConBendEstWallLoss0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss0.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(0) = txtConBendEstWallLoss0.Text
    End Sub
    Private Sub txtConBendEstWallLoss1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss1.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(1) = txtConBendEstWallLoss1.Text
    End Sub
    Private Sub txtConBendEstWallLoss2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss2.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(2) = txtConBendEstWallLoss2.Text
    End Sub
    Private Sub txtConBendEstWallLoss3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss3.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(3) = txtConBendEstWallLoss3.Text
    End Sub
    Private Sub txtConBendEstWallLoss4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss4.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(4) = txtConBendEstWallLoss4.Text
    End Sub
    Private Sub txtConBendEstWallLoss5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss5.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(5) = txtConBendEstWallLoss5.Text
    End Sub
    Private Sub txtConBendEstWallLoss6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss6.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(6) = txtConBendEstWallLoss6.Text
    End Sub
    Private Sub txtConBendEstWallLoss7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstWallLoss7.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstWallLoss(7) = txtConBendEstWallLoss7.Text
    End Sub
#End Region
#Region "Convective Bend Remaining Wall Thickness Changed Events"
    Private Sub txtConBendEstRemWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall0.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(0) = txtConBendEstRemWall0.Text
    End Sub
    Private Sub txtConBendEstRemWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall1.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(1) = txtConBendEstRemWall1.Text
    End Sub
    Private Sub txtConBendEstRemWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall2.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(2) = txtConBendEstRemWall2.Text
    End Sub
    Private Sub txtConBendEstRemWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall3.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(3) = txtConBendEstRemWall3.Text
    End Sub
    Private Sub txtConBendEstRemWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall4.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(4) = txtConBendEstRemWall4.Text
    End Sub
    Private Sub txtConBendEstRemWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall5.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(5) = txtConBendEstRemWall5.Text
    End Sub
    Private Sub txtConBendEstRemWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall6.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(6) = txtConBendEstRemWall6.Text
    End Sub
    Private Sub txtConBendEstRemWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConBendEstRemWall7.EditValueChanged
        mysummarydata.MyConvectiveBendData.EstRemWall(7) = txtConBendEstRemWall7.Text
    End Sub

#End Region
#Region "Conective Bend General Input Changed Events"
    Private Sub uiConBendGeneralOrLocal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConBendGeneralOrLocal.SelectedIndexChanged
        mysummarydata.MyConvectiveBendData.GeneralOrLocal = uiConBendGeneralOrLocal.Text
    End Sub

    Private Sub uiConBendInternalOrExternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConBendInternalOrExternal.SelectedIndexChanged
        mysummarydata.MyConvectiveBendData.InternalOrExternal = uiConBendInternalOrExternal.Text
    End Sub

    Private Sub uiConBendBendSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConBendBendSize.SelectedIndexChanged
        mysummarydata.MyConvectiveBendData.PipeSize = uiConBendBendSize.Text
    End Sub

    Private Sub uiConBendScheduleSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiConBendScheduleSize.SelectedIndexChanged
        mysummarydata.MyConvectiveBendData.PipeSchedule = uiConBendScheduleSize.Text
    End Sub
#End Region
#Region "Radiant Bend ID Texbox Changed Events"
    Private Sub txtRadBendID0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID0.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(0) = txtRadBendID0.Text
    End Sub
    Private Sub txtRadBendID1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID1.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(1) = txtRadBendID1.Text
    End Sub
    Private Sub txtRadBendID2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID2.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(2) = txtRadBendID2.Text
    End Sub
    Private Sub txtRadBendID3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID3.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(3) = txtRadBendID3.Text
    End Sub
    Private Sub txtRadBendID4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID4.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(4) = txtRadBendID4.Text
    End Sub
    Private Sub txtRadBendID5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID5.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(5) = txtRadBendID5.Text
    End Sub
    Private Sub txtRadBendID6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID6.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(6) = txtRadBendID6.Text
    End Sub
    Private Sub txtRadBendID7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendID7.EditValueChanged
        mysummarydata.MyRadiantBendData.ID(7) = txtRadBendID7.Text
    End Sub
#End Region
#Region "Radiant Bend MinWall Changed Events"
    Private Sub txtRadBendMinWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall0.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(0) = txtRadBendMinWall0.Text
    End Sub
    Private Sub txtRadBendMinWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall1.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(1) = txtRadBendMinWall1.Text
    End Sub
    Private Sub txtRadBendMinWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall2.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(2) = txtRadBendMinWall2.Text
    End Sub
    Private Sub txtRadBendMinWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall3.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(3) = txtRadBendMinWall3.Text
    End Sub
    Private Sub txtRadBendMinWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall4.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(4) = txtRadBendMinWall4.Text
    End Sub
    Private Sub txtRadBendMinWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall5.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(5) = txtRadBendMinWall5.Text
    End Sub
    Private Sub txtRadBendMinWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall6.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(6) = txtRadBendMinWall6.Text
    End Sub
    Private Sub txtRadBendMinWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendMinWall7.EditValueChanged
        mysummarydata.MyRadiantBendData.MinWall(7) = txtRadBendMinWall7.Text
    End Sub
#End Region
#Region "Radiant Bend Estimated Wall Loss Changed Events"
    Private Sub txtRadBendEstWallLoss0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss0.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(0) = txtRadBendEstWallLoss0.Text
    End Sub
    Private Sub txtRadBendEstWallLoss1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss1.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(1) = txtRadBendEstWallLoss1.Text
    End Sub
    Private Sub txtRadBendEstWallLoss2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss2.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(2) = txtRadBendEstWallLoss2.Text
    End Sub
    Private Sub txtRadBendEstWallLoss3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss3.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(3) = txtRadBendEstWallLoss3.Text
    End Sub
    Private Sub txtRadBendEstWallLoss4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss4.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(4) = txtRadBendEstWallLoss4.Text
    End Sub
    Private Sub txtRadBendEstWallLoss5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss5.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(5) = txtRadBendEstWallLoss5.Text
    End Sub
    Private Sub txtRadBendEstWallLoss6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss6.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(6) = txtRadBendEstWallLoss6.Text
    End Sub
    Private Sub txtRadBendEstWallLoss7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstWallLoss7.EditValueChanged
        mysummarydata.MyRadiantBendData.EstWallLoss(7) = txtRadBendEstWallLoss7.Text
    End Sub
#End Region
#Region "Radiant Bend Remaining Wall Thickness Changed Events"
    Private Sub txtRadBendEstRemWall0_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall0.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(0) = txtRadBendEstRemWall0.Text
    End Sub
    Private Sub txtRadBendEstRemWall1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall1.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(1) = txtRadBendEstRemWall1.Text
    End Sub
    Private Sub txtRadBendEstRemWall2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall2.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(2) = txtRadBendEstRemWall2.Text
    End Sub
    Private Sub txtRadBendEstRemWall3_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall3.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(3) = txtRadBendEstRemWall3.Text
    End Sub
    Private Sub txtRadBendEstRemWall4_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall4.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(4) = txtRadBendEstRemWall4.Text
    End Sub
    Private Sub txtRadBendEstRemWall5_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall5.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(5) = txtRadBendEstRemWall5.Text
    End Sub
    Private Sub txtRadBendEstRemWall6_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall6.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(6) = txtRadBendEstRemWall6.Text
    End Sub
    Private Sub txtRadBendEstRemWall7_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRadBendEstRemWall7.EditValueChanged
        mysummarydata.MyRadiantBendData.EstRemWall(7) = txtRadBendEstRemWall7.Text
    End Sub

#End Region
#Region "Radiant Bend General Input Changed Events"
    Private Sub uiRadBendGeneralOrLocal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadBendGeneralOrLocal.SelectedIndexChanged
        mysummarydata.MyRadiantBendData.GeneralOrLocal = uiRadBendGeneralOrLocal.Text
    End Sub

    Private Sub uiRadBendInternalOrExternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadBendInternalOrExternal.SelectedIndexChanged
        mysummarydata.MyRadiantBendData.InternalOrExternal = uiRadBendInternalOrExternal.Text
    End Sub

    Private Sub uiRadBendBendSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadBendBendSize.SelectedIndexChanged
        mysummarydata.MyRadiantBendData.PipeSize = uiRadBendBendSize.Text
    End Sub

    Private Sub uiRadBendScheduleSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiRadBendScheduleSize.SelectedIndexChanged
        mysummarydata.MyRadiantBendData.PipeSchedule = uiRadBendScheduleSize.Text
    End Sub
#End Region
   
End Class
