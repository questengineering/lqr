<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InspectionDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim bsPipeTubeItemTypes_LU As System.Windows.Forms.BindingSource
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CompareAgainstControlValidationRule1 As DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule = New DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling8 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling9 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling10 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling11 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling12 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling13 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim OptionsSpelling14 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling15 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling16 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling17 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling18 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling19 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling20 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling21 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling22 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim OptionsSpelling23 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling24 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling25 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling26 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling28 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling29 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling30 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling31 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling32 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule1 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling33 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling34 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule2 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling35 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule3 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling36 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule4 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling37 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling38 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule5 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling39 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule6 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling40 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule7 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling41 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling42 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling43 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling44 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling45 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling27 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling46 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling47 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling48 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling49 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule8 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling50 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling51 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling52 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule9 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling53 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule10 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling54 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule11 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling55 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule12 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling56 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule13 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling57 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule14 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling58 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule15 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling59 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule16 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling60 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule17 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling61 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule18 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling62 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule19 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling63 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule20 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling64 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule21 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling65 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule22 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling66 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule23 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling67 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule24 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling68 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule25 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling69 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling70 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule26 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling71 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule27 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling72 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule28 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling73 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule29 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling74 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule30 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling75 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule31 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling76 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule32 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling77 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule33 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling78 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule34 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling79 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule35 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling80 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule36 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling81 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule37 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling82 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim OptionsSpelling83 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule38 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim OptionsSpelling84 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling85 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling86 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling87 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling88 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim ConditionValidationRule39 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InspectionDetails))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject6 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject7 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject8 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject9 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject10 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule40 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject11 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule41 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule42 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject12 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CompareAgainstControlValidationRule2 As DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule = New DevExpress.XtraEditors.DXErrorProvider.CompareAgainstControlValidationRule()
        Dim ConditionValidationRule43 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject13 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule44 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject14 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule45 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject15 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule46 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject16 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule47 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject17 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule48 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject18 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule49 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject19 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule50 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject20 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule51 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject21 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule52 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject22 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule53 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule54 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule55 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule56 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject23 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule57 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject24 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule58 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule59 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule60 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule61 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule62 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule63 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject25 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule64 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim ConditionValidationRule65 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject26 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule66 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject27 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule67 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject28 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule68 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject29 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim ConditionValidationRule69 As DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule = New DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule()
        Dim SerializableAppearanceObject30 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject31 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.QTT_LookupsData = New QIG.AutoReporter.QTT_LookupsData()
        Me.uiInspectionEndDate = New DevExpress.XtraEditors.DateEdit()
        Me.bsInspections = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_InspectionsDataSets = New QIG.AutoReporter.QTT_InspectionsDataSets()
        Me.bsInspections_AppendixItems = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspections_Appendixes = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspections_ReportItems = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspections_PipeTubeItems = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsPipeTubeItemsSummaries = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsDefaultPipeSchedules = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTT_Customers = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspections_OtherInspectors = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTT_Roles_Main = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionTypesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsTblInspectionTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTT_Roles_Other = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTT_LeadInspectors = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspectionTools = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspections_InspectionsAndTools = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsOtherInspectors = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTTOffices = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsQTTInspectors = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.CheckedListBox2 = New System.Windows.Forms.CheckedListBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.ComboBox11 = New System.Windows.Forms.ComboBox()
        Me.ComboBox12 = New System.Windows.Forms.ComboBox()
        Me.DateTimePicker5 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker6 = New System.Windows.Forms.DateTimePicker()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.ComboBox13 = New System.Windows.Forms.ComboBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ComboBox14 = New System.Windows.Forms.ComboBox()
        Me.ComboBox15 = New System.Windows.Forms.ComboBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.TextBox37 = New System.Windows.Forms.TextBox()
        Me.TextBox38 = New System.Windows.Forms.TextBox()
        Me.TextBox39 = New System.Windows.Forms.TextBox()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.TextBox41 = New System.Windows.Forms.TextBox()
        Me.TextBox42 = New System.Windows.Forms.TextBox()
        Me.TextBox43 = New System.Windows.Forms.TextBox()
        Me.TextBox44 = New System.Windows.Forms.TextBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.dgAppendixItems = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewButtonColumn1 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.TextBox45 = New System.Windows.Forms.TextBox()
        Me.TextBox46 = New System.Windows.Forms.TextBox()
        Me.TextBox47 = New System.Windows.Forms.TextBox()
        Me.HScrollBar2 = New System.Windows.Forms.HScrollBar()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewButtonColumn3 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.bsInspectionCustomers = New System.Windows.Forms.BindingSource(Me.components)
        Me.bsInspectionCustomer = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionstblCustomersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionstblCustomersBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.QTT_PipeTubeDetails = New QIG.AutoReporter.QTT_PipeTubeDetails()
        Me.bsPipeTubeItemTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.taTblPipeTubeItems_Dets = New QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.taTblPipeTubeItemTypes_Dets = New QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter()
        Me.taTblPipeTubeItemTypes = New QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter()
        Me.taPipeTubeItemTypes = New QIG.AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter()
        Me.bsPipeTubeTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionstblPipeTubeItemsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.taTblInspections = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter()
        Me.taTblInspectionTypes = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter()
        Me.taTblInspectionsAndTools = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsAndToolsTableAdapter()
        Me.taTblInspectionTools = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionToolsTableAdapter()
        Me.taTblPipeTubeItems = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemsTableAdapter()
        Me.taTblCustomers_Inspection = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter()
        Me.VwAppendixDetailsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwAppendixDetailsTableAdapter()
        Me.VwAppendixItem_DetailsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter()
        Me.VwReportItems_DetailsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwReportItems_DetailsTableAdapter()
        Me.taPipeTubeItemSummaries = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemSummariesTableAdapter()
        Me.taInspectionsAndInspectors = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsAndInspectorsTableAdapter()
        Me.TblQTT_RolesTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_RolesTableAdapter()
        Me.taInspections_TblPipeTubeItemTypes = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.Inspections_PipeTubeItemTypesTableAdapter()
        Me.taCustomers_LU = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblCustomersTableAdapter()
        Me.taTblQTT_Offices = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_OfficesTableAdapter()
        Me.taTblDefaultPipeSchedules = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultPipeSchedulesTableAdapter()
        Me.taTblQTT_Inspectors = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_InspectorsTableAdapter()
        Me.bsInspectionTypes_LU = New System.Windows.Forms.BindingSource(Me.components)
        Me.taTblInspectionTypes_LU = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblInspectionTypesTableAdapter()
        Me.TblPipeTubeItemTypesTableAdapter = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblPipeTubeItemTypesTableAdapter()
        Me.taDefaultText_PipeTubeMaterial = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.taDefaultText_PipeTubeSurface = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.TblQTT_RolesTableAdapter1 = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_RolesTableAdapter()
        Me.taTblPipeTubeItemTypes_1 = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblPipeTubeItemTypesTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.uiDateButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiDayDateButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiMonthYearButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.uiMonthCalendar = New System.Windows.Forms.MonthCalendar()
        Me.uiProjectDescriptionEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.SpellChecker1 = New DevExpress.XtraSpellChecker.SpellChecker()
        Me.uiProjectNumber = New DevExpress.XtraEditors.TextEdit()
        Me.uiProposalNumber = New DevExpress.XtraEditors.TextEdit()
        Me.uiCustomTubeFormatEditor = New DevExpress.XtraEditors.TextEdit()
        Me.uiInspectorPhone = New DevExpress.XtraEditors.TextEdit()
        Me.uiPostalCode = New DevExpress.XtraEditors.TextEdit()
        Me.uiPriorHistoryEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiCity = New DevExpress.XtraEditors.TextEdit()
        Me.uiAddress = New DevExpress.XtraEditors.TextEdit()
        Me.uiLocation = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeLength = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeWallThicknessDisplay = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeOuterDiameterDisplay = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeInnerDiameterDisplay = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeWallThickness = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeOuterDiameter = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeInnerDiameter = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeInspectionLocations = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiReportFilename = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeIdentificationEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiPurchaseOrder = New DevExpress.XtraEditors.TextEdit()
        Me.uiSummaryEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiCustomRowFormatEditor = New DevExpress.XtraEditors.TextEdit()
        Me.uiNumberTubesTotal = New DevExpress.XtraEditors.TextEdit()
        Me.uiPlusTubeTolerance = New DevExpress.XtraEditors.TextEdit()
        Me.uiMinusTubeTolerance = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeDesignToleranceDisplay = New DevExpress.XtraEditors.TextEdit()
        Me.uiRedTubeCount = New DevExpress.XtraEditors.TextEdit()
        Me.bsInspections_AgedSummary = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiOverallTubesInspected = New DevExpress.XtraEditors.TextEdit()
        Me.uiSwellingOver3PercentCount = New DevExpress.XtraEditors.TextEdit()
        Me.uiBulgingTubeCount = New DevExpress.XtraEditors.TextEdit()
        Me.uiWorstTubeIDRow = New DevExpress.XtraEditors.TextEdit()
        Me.uiWorstTubeIDTube = New DevExpress.XtraEditors.TextEdit()
        Me.uiBulgingWorstPercent = New DevExpress.XtraEditors.TextEdit()
        Me.uiYellowTubeCount = New DevExpress.XtraEditors.TextEdit()
        Me.uiAgedTubeCount3 = New DevExpress.XtraEditors.TextEdit()
        Me.uiAgedTubeCount2 = New DevExpress.XtraEditors.TextEdit()
        Me.uiAgedTubeCount1 = New DevExpress.XtraEditors.TextEdit()
        Me.uiAgedTubeCount4 = New DevExpress.XtraEditors.TextEdit()
        Me.uiNewTubeCount4 = New DevExpress.XtraEditors.TextEdit()
        Me.bsInspections_NewSummary = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiNewTubeCount3 = New DevExpress.XtraEditors.TextEdit()
        Me.uiNewTubeCount2 = New DevExpress.XtraEditors.TextEdit()
        Me.uiNewTubeCount1 = New DevExpress.XtraEditors.TextEdit()
        Me.uiInspectionSummaryEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiWorstFlawTubeNumber = New DevExpress.XtraEditors.TextEdit()
        Me.uiNumberTubesWithFlaws = New DevExpress.XtraEditors.TextEdit()
        Me.uiWorstFlawRowNumber = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeServiceDate = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeNextInspectionDate = New DevExpress.XtraEditors.TextEdit()
        Me.uiAgedTubeCount5 = New DevExpress.XtraEditors.TextEdit()
        Me.uiDiaGrowthMaxPercent = New DevExpress.XtraEditors.TextEdit()
        Me.uiDiaGrowthTubeID = New DevExpress.XtraEditors.TextEdit()
        Me.uiDiaGrowthRowID = New DevExpress.XtraEditors.TextEdit()
        Me.uiOperatingPeriod = New DevExpress.XtraEditors.TextEdit()
        Me.uiNumberOfTubes = New DevExpress.XtraEditors.TextEdit()
        Me.uiTubeLengthDisplay = New DevExpress.XtraEditors.TextEdit()
        Me.uiImageNameFormat = New DevExpress.XtraEditors.TextEdit()
        Me.ToolTipController = New DevExpress.Utils.ToolTipController(Me.components)
        Me.uiCracking = New DevExpress.XtraEditors.TextEdit()
        Me.uiFFSEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiDiaGrowthSummaryEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiExecSummaryDetailsEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiTubeHarvestingEditor = New DevExpress.XtraRichEdit.RichEditControl()
        Me.uiNumberTubesInspected = New DevExpress.XtraEditors.TextEdit()
        Me.uiClearRedProjectDescriptionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiInsertProjectDescriptionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.uiNumberingSystem = New DevExpress.XtraEditors.RadioGroup()
        Me.uiShowHideButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiClearRedTubeIdentificationButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiInsertTubeIdentificationButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.uiAutoSelectMainButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiResetMainButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiRemoveMainButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.uiReportItemsGridControl = New DevExpress.XtraGrid.GridControl()
        Me.uiReportItemsGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDescriptionMain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colStatusMain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLinkedFileMain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemButtonEditMain = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.colSortOrderMain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFK_ReportItemType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.uiImageNameFormatLabel = New System.Windows.Forms.Label()
        Me.uiAutoSelectAppendixButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAppendixItems = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiResetAppendixButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAddNewAppendixButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAddMissingAppendixButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAppendixItemsGridControl = New DevExpress.XtraGrid.GridControl()
        Me.uiAppendixItemsGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDescriptionAppendix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colStatusAppendix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLinkedFileAppendix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemButtonEditAppendix = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.colPipeTubeSortOrder = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSortOrderAppendix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReportItemType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemLookUpEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemSearchLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemLookUpEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.uiRemoveAppendixButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.taDefaultText_PipeTubeManufacturer = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.TblQTT_InspectorsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_InspectorsTableAdapter()
        Me.TblQTT_LeadInspectorsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_LeadInspectorsTableAdapter()
        Me.TblQTT_PrimaryRolesTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_PrimaryRolesTableAdapter()
        Me.TblQTT_OfficesTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_OfficesTableAdapter()
        Me.uiMainXtraTabControl = New DevExpress.XtraTab.XtraTabControl()
        Me.uiInspectionDetailsTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiSpareUninstalledGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.uiTubeNumberInstructionsLabel = New System.Windows.Forms.Label()
        Me.uiCanCircularOption = New DevExpress.XtraEditors.CheckEdit()
        Me.uiNumberOfRows = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiRowFormatGroupBox = New System.Windows.Forms.GroupBox()
        Me.RowFormatRadioGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.uiCustomRowFormatLabel = New System.Windows.Forms.Label()
        Me.uiRowsLabel = New System.Windows.Forms.Label()
        Me.uiInspectionStartDate = New DevExpress.XtraEditors.DateEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.uiToolsCheckedList = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.uiTubeFormatGroupBox = New System.Windows.Forms.GroupBox()
        Me.TubeFormatRadioGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.uiCustomTubeFormatLabel = New System.Windows.Forms.Label()
        Me.uiLabel = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.uiRemoveInspectorButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiAddInspectorButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiRolesList = New DevExpress.XtraEditors.ListBoxControl()
        Me.uiProjectInspectorsList = New DevExpress.XtraEditors.ListBoxControl()
        Me.uiOtherInspectorsList = New DevExpress.XtraEditors.ListBoxControl()
        Me.uiPlantNameLabel = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.uiTitleLabel = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.uiLeadInspectorLabel = New System.Windows.Forms.Label()
        Me.uiOfficeLabel = New System.Windows.Forms.Label()
        Me.uiEndDateLabel = New System.Windows.Forms.Label()
        Me.uiStartDateLabel = New System.Windows.Forms.Label()
        Me.uiTitleRole = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiLeadInspector = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiOffices = New DevExpress.XtraEditors.LookUpEdit()
        Me.uiCustomerDetailsTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiJMC = New DevExpress.XtraEditors.CheckEdit()
        Me.uiClearPriorHistoryRedButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiReformerOEM = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiPlantProcessType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiFlowDirection = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiBurnerConfiguration = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.uiManufacturerLabel = New System.Windows.Forms.Label()
        Me.uiHistoryLabel = New System.Windows.Forms.Label()
        Me.uiFirstInspectionInsertSampleTextButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiHistoryInsertSampleTextButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiGridControlContacts = New DevExpress.XtraGrid.GridControl()
        Me.bstblContactInformation = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiGridViewContacts = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrefix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTitle = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPhone = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmail = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFax = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFK_Inspection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.uiPriorHistoryLabel = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.uiState = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiCountry = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.uiCustomers = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.uiCustomerLabel = New System.Windows.Forms.Label()
        Me.uiReformerName = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiReformerID = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiProjectDescriptionTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiTubeDetailsTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiDegreesMarker = New DevExpress.XtraEditors.RadioGroup()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.uiMonthYearLabel = New System.Windows.Forms.Label()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.uiInspectionDetailsRowsTubesLabel = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.uiTolerancesGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.uiTubeDesignToleranceLabel = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.uiNumberTubesTotalLabel = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.uiTubeManufacturer = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiTubeUnitGroup = New System.Windows.Forms.GroupBox()
        Me.uiLeadingUnits = New DevExpress.XtraEditors.RadioGroup()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.uiOperatingHoursLabel = New System.Windows.Forms.Label()
        Me.uiThermalCyclesLabel = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.uiLengthLabel = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.uiTubeServiceDateLabel = New System.Windows.Forms.Label()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.uiTubeTypeLabel = New System.Windows.Forms.Label()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.uiTubeMaterials = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiTubeInspectionAge = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiPressure = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiTargetTemp = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiThermalCycles = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiOperatingHours = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiExecutiveSummaryTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiCrackingLabel = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label146 = New System.Windows.Forms.Label()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.uiMaxGrowthLocation = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.uiOverallMaxGrowthLocationLabel = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.uiExecSummaryGreen = New DevExpress.XtraEditors.CheckEdit()
        Me.uiExecSummaryYellow = New DevExpress.XtraEditors.CheckEdit()
        Me.uiExecSummaryRed = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.uiOverallMaxGrowth = New DevExpress.XtraEditors.RadioGroup()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.uiSummaryEditorShowHideButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiSummaryEditorButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.uiOverallMaxGrowthLocation = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiInspectionSummaryTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.uiRowNumberLabel = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.uiTubeToleranceLabel4 = New System.Windows.Forms.Label()
        Me.uiTubeToleranceLabel3 = New System.Windows.Forms.Label()
        Me.uiTubeToleranceLabel2 = New System.Windows.Forms.Label()
        Me.uiTubeToleranceLabel1 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.uiInspectionShowHideButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiInspectionSummaryButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiTubeFlawLocation = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.uiDetailedInspectionTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiDetailedSummaryTabControl = New DevExpress.XtraTab.XtraTabControl()
        Me.ExecSummaryTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.DiaGrowthSummaryTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.FFSSummaryTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.TubeHarvestingTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiDiaGrowthTotalLabel = New System.Windows.Forms.Label()
        Me.uiLevel1Label = New System.Windows.Forms.Label()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.uiDiaGrowthLabel5 = New System.Windows.Forms.Label()
        Me.uiGreenCreep = New DevExpress.XtraEditors.CheckEdit()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.uiDetailedSummaryGreen = New DevExpress.XtraEditors.CheckEdit()
        Me.uiDetailedSummaryYellow = New DevExpress.XtraEditors.CheckEdit()
        Me.uiDetailedSummaryRed = New DevExpress.XtraEditors.CheckEdit()
        Me.uiDiaGrowthLabel4 = New System.Windows.Forms.Label()
        Me.uiDiaGrowthLabel3 = New System.Windows.Forms.Label()
        Me.uiDiaGrowthLabel2 = New System.Windows.Forms.Label()
        Me.uiDiaGrowthLabel1 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.uiTubeFlawsGouges = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.uiTubeFlawsWelds = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.uiDetailedSummaryShowHideButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiDetailedSummaryButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiLinkedFilesTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiFieldReportTabPage = New DevExpress.XtraTab.XtraTabPage()
        Me.uiSampleLevel3 = New DevExpress.XtraEditors.CheckEdit()
        Me.ReportStatusLabel = New System.Windows.Forms.Label()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.uiViewReportButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiCreateReportButton = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.PaperSizeOptionGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.TblDefaultTextListItemsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionsvwReportItemsDetailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.uiCustomerNameLabel = New System.Windows.Forms.Label()
        Me.uiLocationNameLabel = New System.Windows.Forms.Label()
        Me.uiHeaterNameLabel = New System.Windows.Forms.Label()
        Me.uiInspectionTypeLabel = New System.Windows.Forms.Label()
        Me.uiSaveButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiSaveExitButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiDateLabel = New System.Windows.Forms.Label()
        Me.uiRootFolderName = New DevExpress.XtraEditors.ButtonEdit()
        Me.DxValidationProvider = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
        Me.FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblContactInformationTableAdapter1 = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblContactInformationTableAdapter()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tmrAutoSave = New System.Windows.Forms.Timer(Me.components)
        Me.uiAutoSaveInterval = New DevExpress.XtraEditors.SpinEdit()
        Me.uilblAutoSaveInterval = New System.Windows.Forms.Label()
        Me.uilblAutoSaveUnits = New System.Windows.Forms.Label()
        Me.uilblLastAutoSave = New System.Windows.Forms.Label()
        Me.taTblPlantProcessTypes = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.taTblFlowDirections = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.taTblBurnerConfigurations = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.taTblReformerOEMs = New QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter()
        Me.TblReformerAgedSummaryTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter()
        Me.TblReformerNewSummaryTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter()
        Me.taTblReformerAgedSummary = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter()
        Me.taTblReformerNewSummary = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter()
        Me.TblInspectionstblReformerAgedSummaryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        bsPipeTubeItemTypes_LU = New System.Windows.Forms.BindingSource(Me.components)
        CType(bsPipeTubeItemTypes_LU,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_LookupsData,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionEndDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionEndDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_InspectionsDataSets,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_AppendixItems,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_Appendixes,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_ReportItems,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_PipeTubeItems,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsPipeTubeItemsSummaries,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsDefaultPipeSchedules,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTT_Customers,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_OtherInspectors,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTT_Roles_Main,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionTypesBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsTblInspectionTypes,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTT_Roles_Other,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTT_LeadInspectors,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspectionTools,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_InspectionsAndTools,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsOtherInspectors,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTTOffices,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsQTTInspectors,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPage1.SuspendLayout
        Me.TabPage2.SuspendLayout
        Me.GroupBox2.SuspendLayout
        CType(Me.NumericUpDown3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPage3.SuspendLayout
        CType(Me.DataGridView3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPage4.SuspendLayout
        Me.TabPage5.SuspendLayout
        CType(Me.dgAppendixItems,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDown4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspectionCustomers,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspectionCustomer,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionstblCustomersBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionstblCustomersBindingSource1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.QTT_PipeTubeDetails,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsPipeTubeItemTypes,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsPipeTubeTypes,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionstblPipeTubeItemsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspectionTypes_LU,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SplitContainerControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SplitContainerControl2.SuspendLayout
        CType(Me.uiProjectNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiProposalNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCustomTubeFormatEditor.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectorPhone.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiPostalCode.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCity.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAddress.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiLocation.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeLength.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeWallThicknessDisplay.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeOuterDiameterDisplay.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeInnerDiameterDisplay.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeWallThickness.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeOuterDiameter.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeInnerDiameter.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiReportFilename.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiPurchaseOrder.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCustomRowFormatEditor.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberTubesTotal.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiPlusTubeTolerance.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiMinusTubeTolerance.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeDesignToleranceDisplay.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiRedTubeCount.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_AgedSummary,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOverallTubesInspected.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiSwellingOver3PercentCount.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiBulgingTubeCount.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiWorstTubeIDRow.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiWorstTubeIDTube.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiBulgingWorstPercent.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiYellowTubeCount.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAgedTubeCount3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAgedTubeCount2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAgedTubeCount1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAgedTubeCount4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNewTubeCount4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bsInspections_NewSummary,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNewTubeCount3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNewTubeCount2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNewTubeCount1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiWorstFlawTubeNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberTubesWithFlaws.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiWorstFlawRowNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeServiceDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeNextInspectionDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAgedTubeCount5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDiaGrowthMaxPercent.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDiaGrowthTubeID.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDiaGrowthRowID.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOperatingPeriod.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberOfTubes.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeLengthDisplay.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiImageNameFormat.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCracking.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberTubesInspected.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberingSystem.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SplitContainerControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SplitContainerControl1.SuspendLayout
        CType(Me.uiReportItemsGridControl,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiReportItemsGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEditMain,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemLookUpEdit2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAppendixItems.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAppendixItemsGridControl,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAppendixItemsGridView,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEditAppendix,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemLookUpEdit3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEdit1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemTextEdit1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemSearchLookUpEdit1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemLookUpEdit4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemTextEdit2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiMainXtraTabControl,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiMainXtraTabControl.SuspendLayout
        Me.uiInspectionDetailsTabPage.SuspendLayout
        CType(Me.uiSpareUninstalledGroup.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCanCircularOption.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiNumberOfRows.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiRowFormatGroupBox.SuspendLayout
        CType(Me.RowFormatRadioGroup.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionStartDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiInspectionStartDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiToolsCheckedList,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiTubeFormatGroupBox.SuspendLayout
        CType(Me.TubeFormatRadioGroup.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiRolesList,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiProjectInspectorsList,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOtherInspectorsList,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTitleRole.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiLeadInspector.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOffices.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiCustomerDetailsTabPage.SuspendLayout
        CType(Me.uiJMC.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiReformerOEM.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiPlantProcessType.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiFlowDirection.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiBurnerConfiguration.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiGridControlContacts,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.bstblContactInformation,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiGridViewContacts,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiState.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCountry.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiCustomers.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiReformerName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiReformerID.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiProjectDescriptionTabPage.SuspendLayout
        Me.uiTubeDetailsTabPage.SuspendLayout
        CType(Me.uiDegreesMarker.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox1.SuspendLayout
        CType(Me.uiTolerancesGroup.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeManufacturer.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiTubeUnitGroup.SuspendLayout
        CType(Me.uiLeadingUnits.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeMaterials.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeInspectionAge.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiPressure.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTargetTemp.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiThermalCycles.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOperatingHours.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiExecutiveSummaryTabPage.SuspendLayout
        CType(Me.uiMaxGrowthLocation.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiExecSummaryGreen.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiExecSummaryYellow.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiExecSummaryRed.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox3.SuspendLayout
        CType(Me.uiOverallMaxGrowth.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiOverallMaxGrowthLocation.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiInspectionSummaryTabPage.SuspendLayout
        CType(Me.uiTubeFlawLocation.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiDetailedInspectionTabPage.SuspendLayout
        CType(Me.uiDetailedSummaryTabControl,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiDetailedSummaryTabControl.SuspendLayout
        Me.ExecSummaryTabPage.SuspendLayout
        Me.DiaGrowthSummaryTabPage.SuspendLayout
        Me.FFSSummaryTabPage.SuspendLayout
        Me.TubeHarvestingTabPage.SuspendLayout
        CType(Me.uiGreenCreep.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDetailedSummaryGreen.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDetailedSummaryYellow.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiDetailedSummaryRed.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeFlawsGouges.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiTubeFlawsWelds.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.uiLinkedFilesTabPage.SuspendLayout
        Me.uiFieldReportTabPage.SuspendLayout
        CType(Me.uiSampleLevel3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox5.SuspendLayout
        CType(Me.PaperSizeOptionGroup.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblDefaultTextListItemsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionsvwReportItemsDetailsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiRootFolderName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.uiAutoSaveInterval.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblInspectionstblReformerAgedSummaryBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'bsPipeTubeItemTypes_LU
        '
        bsPipeTubeItemTypes_LU.DataMember = "tblPipeTubeItemTypes"
        bsPipeTubeItemTypes_LU.DataSource = Me.QTT_LookupsData
        '
        'QTT_LookupsData
        '
        Me.QTT_LookupsData.DataSetName = "QTT_LookupsData"
        Me.QTT_LookupsData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'uiInspectionEndDate
        '
        Me.uiInspectionEndDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "InspectionDateEnd", true))
        Me.uiInspectionEndDate.EditValue = Nothing
        Me.uiInspectionEndDate.Location = New System.Drawing.Point(340, 13)
        Me.uiInspectionEndDate.Name = "uiInspectionEndDate"
        Me.uiInspectionEndDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionEndDate.Properties.Appearance.Options.UseFont = true
        Me.uiInspectionEndDate.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionEndDate.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiInspectionEndDate.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionEndDate.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiInspectionEndDate.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionEndDate.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = true
        SerializableAppearanceObject1.Options.UseBorderColor = true
        Me.uiInspectionEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, true)})
        Me.uiInspectionEndDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionEndDate.Properties.DisplayFormat.FormatString = "MMMM d, yyyy"
        Me.uiInspectionEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.uiInspectionEndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uiInspectionEndDate.Size = New System.Drawing.Size(161, 24)
        Me.uiInspectionEndDate.TabIndex = 3
        CompareAgainstControlValidationRule1.CompareControlOperator = DevExpress.XtraEditors.DXErrorProvider.CompareControlOperator.GreaterOrEqual
        CompareAgainstControlValidationRule1.ErrorText = "End date must be equal to or greater than start date."
        Me.DxValidationProvider.SetValidationRule(Me.uiInspectionEndDate, CompareAgainstControlValidationRule1)
        '
        'bsInspections
        '
        Me.bsInspections.DataMember = "tblInspections"
        Me.bsInspections.DataSource = Me.QTT_InspectionsDataSets
        '
        'QTT_InspectionsDataSets
        '
        Me.QTT_InspectionsDataSets.DataSetName = "QTT_InspectionsDataSets"
        Me.QTT_InspectionsDataSets.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bsInspections_AppendixItems
        '
        Me.bsInspections_AppendixItems.DataMember = "vwAppendixDetails_vwAppendixItem_Details"
        Me.bsInspections_AppendixItems.DataSource = Me.bsInspections_Appendixes
        '
        'bsInspections_Appendixes
        '
        Me.bsInspections_Appendixes.DataMember = "tblInspections_vwAppendixDetails"
        Me.bsInspections_Appendixes.DataSource = Me.bsInspections
        Me.bsInspections_Appendixes.Sort = "SortOrder"
        '
        'bsInspections_ReportItems
        '
        Me.bsInspections_ReportItems.DataMember = "tblInspections_vwReportItems_Details"
        Me.bsInspections_ReportItems.DataSource = Me.bsInspections
        '
        'bsInspections_PipeTubeItems
        '
        Me.bsInspections_PipeTubeItems.DataMember = "tblInspectionstblPipeTubeItems"
        Me.bsInspections_PipeTubeItems.DataSource = Me.bsInspections
        '
        'bsPipeTubeItemsSummaries
        '
        Me.bsPipeTubeItemsSummaries.DataMember = "FK_tblPipeTubeItemSummaries_tblPipeTubeItems"
        Me.bsPipeTubeItemsSummaries.DataSource = Me.bsInspections_PipeTubeItems
        '
        'bsDefaultPipeSchedules
        '
        Me.bsDefaultPipeSchedules.DataMember = "tblDefaultPipeSchedules"
        Me.bsDefaultPipeSchedules.DataSource = Me.QTT_LookupsData
        Me.bsDefaultPipeSchedules.Sort = "OuterDiameter,Thickness"
        '
        'bsQTT_Customers
        '
        Me.bsQTT_Customers.DataMember = "tblCustomers"
        Me.bsQTT_Customers.DataSource = Me.QTT_InspectionsDataSets
        '
        'bsInspections_OtherInspectors
        '
        Me.bsInspections_OtherInspectors.DataMember = "tblInspections_tblInspectionsAndInspectors"
        Me.bsInspections_OtherInspectors.DataSource = Me.bsInspections
        '
        'bsQTT_Roles_Main
        '
        Me.bsQTT_Roles_Main.DataMember = "tblQTT_PrimaryRoles"
        Me.bsQTT_Roles_Main.DataSource = Me.QTT_InspectionsDataSets
        '
        'TblInspectionTypesBindingSource
        '
        Me.TblInspectionTypesBindingSource.DataSource = Me.bsTblInspectionTypes
        '
        'bsTblInspectionTypes
        '
        Me.bsTblInspectionTypes.DataMember = "tblInspectionTypes"
        Me.bsTblInspectionTypes.DataSource = Me.QTT_InspectionsDataSets
        '
        'bsQTT_Roles_Other
        '
        Me.bsQTT_Roles_Other.DataMember = "tblQTT_Roles"
        Me.bsQTT_Roles_Other.DataSource = Me.QTT_LookupsData
        '
        'bsQTT_LeadInspectors
        '
        Me.bsQTT_LeadInspectors.DataMember = "tblQTT_LeadInspectors"
        Me.bsQTT_LeadInspectors.DataSource = Me.QTT_InspectionsDataSets
        '
        'bsInspectionTools
        '
        Me.bsInspectionTools.DataMember = "tblInspectionTools"
        Me.bsInspectionTools.DataSource = Me.QTT_InspectionsDataSets
        '
        'bsInspections_InspectionsAndTools
        '
        Me.bsInspections_InspectionsAndTools.DataMember = "tblInspectionstblInspectionsAndTools"
        Me.bsInspections_InspectionsAndTools.DataSource = Me.bsInspections
        '
        'bsOtherInspectors
        '
        Me.bsOtherInspectors.DataMember = "tblQTT_Inspectors"
        Me.bsOtherInspectors.DataSource = Me.QTT_LookupsData
        '
        'bsQTTOffices
        '
        Me.bsQTTOffices.DataMember = "tblQTT_Offices"
        Me.bsQTTOffices.DataSource = Me.QTT_InspectionsDataSets
        '
        'bsQTTInspectors
        '
        Me.bsQTTInspectors.DataMember = "tblQTT_Inspectors"
        Me.bsQTTInspectors.DataSource = Me.QTT_LookupsData
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(693, 316)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 27)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "Add"
        Me.Button5.UseVisualStyleBackColor = true
        '
        'ComboBox7
        '
        Me.ComboBox7.FormattingEnabled = true
        Me.ComboBox7.Location = New System.Drawing.Point(334, 316)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(333, 21)
        Me.ComboBox7.TabIndex = 21
        '
        'Label27
        '
        Me.Label27.AutoSize = true
        Me.Label27.Location = New System.Drawing.Point(192, 316)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(132, 17)
        Me.Label27.TabIndex = 19
        Me.Label27.Text = "Add QIG Inspector:"
        '
        'Label28
        '
        Me.Label28.AutoSize = true
        Me.Label28.Location = New System.Drawing.Point(75, 181)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(106, 17)
        Me.Label28.TabIndex = 20
        Me.Label28.Text = "QIG Inspectors"
        '
        'Label29
        '
        Me.Label29.AutoSize = true
        Me.Label29.Location = New System.Drawing.Point(33, 93)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(138, 17)
        Me.Label29.TabIndex = 25
        Me.Label29.Text = "QIG Report Number"
        '
        'Label30
        '
        Me.Label30.AutoSize = true
        Me.Label30.Location = New System.Drawing.Point(30, 65)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(139, 17)
        Me.Label30.TabIndex = 26
        Me.Label30.Text = "QIG Project Number"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(192, 93)
        Me.TextBox15.Name = "TextBox15"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox15, false)
        Me.TextBox15.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox15, OptionsSpelling1)
        Me.TextBox15.TabIndex = 23
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(192, 60)
        Me.TextBox16.Name = "TextBox16"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox16, false)
        Me.TextBox16.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox16, OptionsSpelling2)
        Me.TextBox16.TabIndex = 24
        '
        'Label31
        '
        Me.Label31.AutoSize = true
        Me.Label31.Location = New System.Drawing.Point(464, 29)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(112, 17)
        Me.Label31.TabIndex = 18
        Me.Label31.Text = "Inspection Type:"
        '
        'Label32
        '
        Me.Label32.AutoSize = true
        Me.Label32.Location = New System.Drawing.Point(81, 25)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(78, 17)
        Me.Label32.TabIndex = 17
        Me.Label32.Text = "QIG Office"
        '
        'ComboBox8
        '
        Me.ComboBox8.FormattingEnabled = true
        Me.ComboBox8.Location = New System.Drawing.Point(584, 26)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(231, 21)
        Me.ComboBox8.TabIndex = 15
        '
        'ComboBox9
        '
        Me.ComboBox9.FormattingEnabled = true
        Me.ComboBox9.Location = New System.Drawing.Point(192, 25)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(231, 21)
        Me.ComboBox9.TabIndex = 16
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Location = New System.Drawing.Point(640, 131)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker3.TabIndex = 13
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.Location = New System.Drawing.Point(192, 131)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker4.TabIndex = 14
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(192, 181)
        Me.TextBox17.Multiline = true
        Me.TextBox17.Name = "TextBox17"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox17, false)
        Me.TextBox17.Size = New System.Drawing.Size(559, 118)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox17, OptionsSpelling3)
        Me.TextBox17.TabIndex = 12
        '
        'Label33
        '
        Me.Label33.AutoSize = true
        Me.Label33.Location = New System.Drawing.Point(478, 131)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(151, 17)
        Me.Label33.TabIndex = 10
        Me.Label33.Text = "Inspection Finish Date:"
        '
        'Label34
        '
        Me.Label34.AutoSize = true
        Me.Label34.Location = New System.Drawing.Point(30, 134)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(144, 17)
        Me.Label34.TabIndex = 11
        Me.Label34.Text = "Inspection Start Date:"
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(219, 206)
        Me.TextBox26.Name = "TextBox26"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox26, false)
        Me.TextBox26.Size = New System.Drawing.Size(376, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox26, OptionsSpelling4)
        Me.TextBox26.TabIndex = 2
        '
        'Label52
        '
        Me.Label52.AutoSize = true
        Me.Label52.Location = New System.Drawing.Point(73, 209)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(56, 13)
        Me.Label52.TabIndex = 3
        Me.Label52.Text = "Image 3D:"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label53)
        Me.TabPage1.Controls.Add(Me.TextBox27)
        Me.TabPage1.Controls.Add(Me.Label54)
        Me.TabPage1.Controls.Add(Me.Label55)
        Me.TabPage1.Controls.Add(Me.TextBox28)
        Me.TabPage1.Controls.Add(Me.Label56)
        Me.TabPage1.Controls.Add(Me.Label57)
        Me.TabPage1.Controls.Add(Me.Label58)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.TextBox29)
        Me.TabPage1.Controls.Add(Me.TextBox30)
        Me.TabPage1.Controls.Add(Me.TextBox31)
        Me.TabPage1.Controls.Add(Me.TextBox32)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(886, 577)
        Me.TabPage1.TabIndex = 1
        Me.TabPage1.Text = "Client, Location, and Unit"
        Me.TabPage1.UseVisualStyleBackColor = true
        '
        'Label53
        '
        Me.Label53.AutoSize = true
        Me.Label53.Location = New System.Drawing.Point(65, 213)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(87, 13)
        Me.Label53.TabIndex = 34
        Me.Label53.Text = "Client Contact(s):"
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(211, 210)
        Me.TextBox27.Multiline = true
        Me.TextBox27.Name = "TextBox27"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox27, false)
        Me.TextBox27.Size = New System.Drawing.Size(506, 92)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox27, OptionsSpelling5)
        Me.TextBox27.TabIndex = 33
        '
        'Label54
        '
        Me.Label54.AutoSize = true
        Me.Label54.Location = New System.Drawing.Point(255, 95)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(0, 13)
        Me.Label54.TabIndex = 32
        '
        'Label55
        '
        Me.Label55.AutoSize = true
        Me.Label55.Location = New System.Drawing.Point(49, 162)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(94, 13)
        Me.Label55.TabIndex = 30
        Me.Label55.Text = "Purchase Order #:"
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(211, 159)
        Me.TextBox28.Name = "TextBox28"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox28, false)
        Me.TextBox28.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox28, OptionsSpelling6)
        Me.TextBox28.TabIndex = 29
        '
        'Label56
        '
        Me.Label56.AutoSize = true
        Me.Label56.Location = New System.Drawing.Point(452, 70)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(69, 13)
        Me.Label56.TabIndex = 28
        Me.Label56.Text = "Unit Number:"
        '
        'Label57
        '
        Me.Label57.AutoSize = true
        Me.Label57.Location = New System.Drawing.Point(109, 71)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(60, 13)
        Me.Label57.TabIndex = 28
        Me.Label57.Text = "Unit Name:"
        '
        'Label58
        '
        Me.Label58.AutoSize = true
        Me.Label58.Location = New System.Drawing.Point(115, 24)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(51, 13)
        Me.Label58.TabIndex = 28
        Me.Label58.Text = "Location:"
        '
        'Label59
        '
        Me.Label59.AutoSize = true
        Me.Label59.Location = New System.Drawing.Point(49, 116)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(89, 13)
        Me.Label59.TabIndex = 28
        Me.Label59.Text = "Client Contract #:"
        '
        'TextBox29
        '
        Me.TextBox29.Location = New System.Drawing.Point(565, 66)
        Me.TextBox29.Name = "TextBox29"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox29, false)
        Me.TextBox29.Size = New System.Drawing.Size(103, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox29, OptionsSpelling7)
        Me.TextBox29.TabIndex = 27
        '
        'TextBox30
        '
        Me.TextBox30.Location = New System.Drawing.Point(211, 67)
        Me.TextBox30.Name = "TextBox30"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox30, false)
        Me.TextBox30.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox30, OptionsSpelling8)
        Me.TextBox30.TabIndex = 27
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(211, 22)
        Me.TextBox31.Name = "TextBox31"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox31, false)
        Me.TextBox31.Size = New System.Drawing.Size(392, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox31, OptionsSpelling9)
        Me.TextBox31.TabIndex = 27
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(211, 116)
        Me.TextBox32.Name = "TextBox32"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox32, false)
        Me.TextBox32.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox32, OptionsSpelling10)
        Me.TextBox32.TabIndex = 27
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.NumericUpDown3)
        Me.TabPage2.Controls.Add(Me.CheckedListBox2)
        Me.TabPage2.Controls.Add(Me.Button8)
        Me.TabPage2.Controls.Add(Me.ComboBox2)
        Me.TabPage2.Controls.Add(Me.Label60)
        Me.TabPage2.Controls.Add(Me.Label61)
        Me.TabPage2.Controls.Add(Me.Label62)
        Me.TabPage2.Controls.Add(Me.TextBox33)
        Me.TabPage2.Controls.Add(Me.Label63)
        Me.TabPage2.Controls.Add(Me.Label64)
        Me.TabPage2.Controls.Add(Me.TextBox34)
        Me.TabPage2.Controls.Add(Me.TextBox35)
        Me.TabPage2.Controls.Add(Me.Label65)
        Me.TabPage2.Controls.Add(Me.Label66)
        Me.TabPage2.Controls.Add(Me.Label67)
        Me.TabPage2.Controls.Add(Me.ComboBox11)
        Me.TabPage2.Controls.Add(Me.ComboBox12)
        Me.TabPage2.Controls.Add(Me.DateTimePicker5)
        Me.TabPage2.Controls.Add(Me.DateTimePicker6)
        Me.TabPage2.Controls.Add(Me.Label68)
        Me.TabPage2.Controls.Add(Me.Label69)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(886, 577)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Inspection Details"
        Me.TabPage2.UseVisualStyleBackColor = true
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton3)
        Me.GroupBox2.Controls.Add(Me.RadioButton4)
        Me.GroupBox2.Location = New System.Drawing.Point(324, 445)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(443, 56)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Passes Format"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = true
        Me.RadioButton3.Location = New System.Drawing.Point(214, 22)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(140, 17)
        Me.RadioButton3.TabIndex = 1
        Me.RadioButton3.TabStop = true
        Me.RadioButton3.Text = "Alphabetical (A, B, C, ...)"
        Me.RadioButton3.UseVisualStyleBackColor = true
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = true
        Me.RadioButton4.Location = New System.Drawing.Point(17, 22)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(118, 17)
        Me.RadioButton4.TabIndex = 0
        Me.RadioButton4.TabStop = true
        Me.RadioButton4.Text = "Numeric (1, 2, 3, ...)"
        Me.RadioButton4.UseVisualStyleBackColor = true
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Location = New System.Drawing.Point(192, 445)
        Me.NumericUpDown3.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(97, 20)
        Me.NumericUpDown3.TabIndex = 34
        Me.NumericUpDown3.Tag = "Th"
        Me.NumericUpDown3.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'CheckedListBox2
        '
        Me.CheckedListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckedListBox2.FormattingEnabled = true
        Me.CheckedListBox2.Items.AddRange(New Object() {"FTIS 4.1", "FTIS 6.1", "FTIS 8.0"})
        Me.CheckedListBox2.Location = New System.Drawing.Point(164, 161)
        Me.CheckedListBox2.Name = "CheckedListBox2"
        Me.CheckedListBox2.Size = New System.Drawing.Size(532, 52)
        Me.CheckedListBox2.TabIndex = 32
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(665, 379)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 27)
        Me.Button8.TabIndex = 31
        Me.Button8.Text = "Add"
        Me.Button8.UseVisualStyleBackColor = true
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = true
        Me.ComboBox2.Location = New System.Drawing.Point(324, 379)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(333, 21)
        Me.ComboBox2.TabIndex = 30
        '
        'Label60
        '
        Me.Label60.AutoSize = true
        Me.Label60.Location = New System.Drawing.Point(164, 379)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(98, 13)
        Me.Label60.TabIndex = 28
        Me.Label60.Text = "Add QIG Inspector:"
        '
        'Label61
        '
        Me.Label61.AutoSize = true
        Me.Label61.Location = New System.Drawing.Point(19, 445)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(96, 13)
        Me.Label61.TabIndex = 29
        Me.Label61.Text = "Number of Passes:"
        '
        'Label62
        '
        Me.Label62.AutoSize = true
        Me.Label62.Location = New System.Drawing.Point(19, 244)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(78, 13)
        Me.Label62.TabIndex = 29
        Me.Label62.Text = "QIG Inspectors"
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(164, 244)
        Me.TextBox33.Multiline = true
        Me.TextBox33.Name = "TextBox33"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox33, false)
        Me.TextBox33.Size = New System.Drawing.Size(559, 118)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox33, OptionsSpelling11)
        Me.TextBox33.TabIndex = 27
        '
        'Label63
        '
        Me.Label63.AutoSize = true
        Me.Label63.Location = New System.Drawing.Point(431, 101)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(101, 13)
        Me.Label63.TabIndex = 25
        Me.Label63.Text = "QIG Report Number"
        '
        'Label64
        '
        Me.Label64.AutoSize = true
        Me.Label64.Location = New System.Drawing.Point(431, 67)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(102, 13)
        Me.Label64.TabIndex = 26
        Me.Label64.Text = "QIG Project Number"
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(608, 101)
        Me.TextBox34.Name = "TextBox34"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox34, false)
        Me.TextBox34.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox34, OptionsSpelling12)
        Me.TextBox34.TabIndex = 23
        '
        'TextBox35
        '
        Me.TextBox35.Location = New System.Drawing.Point(608, 64)
        Me.TextBox35.Name = "TextBox35"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox35, false)
        Me.TextBox35.Size = New System.Drawing.Size(209, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox35, OptionsSpelling13)
        Me.TextBox35.TabIndex = 24
        '
        'Label65
        '
        Me.Label65.AutoSize = true
        Me.Label65.Location = New System.Drawing.Point(23, 161)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(77, 13)
        Me.Label65.TabIndex = 20
        Me.Label65.Text = "QIG Equpment"
        '
        'Label66
        '
        Me.Label66.AutoSize = true
        Me.Label66.Location = New System.Drawing.Point(20, 24)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(86, 13)
        Me.Label66.TabIndex = 18
        Me.Label66.Text = "Inspection Type:"
        '
        'Label67
        '
        Me.Label67.AutoSize = true
        Me.Label67.Location = New System.Drawing.Point(501, 20)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(57, 13)
        Me.Label67.TabIndex = 17
        Me.Label67.Text = "QIG Office"
        '
        'ComboBox11
        '
        Me.ComboBox11.FormattingEnabled = true
        Me.ComboBox11.Location = New System.Drawing.Point(164, 20)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(231, 21)
        Me.ComboBox11.TabIndex = 15
        '
        'ComboBox12
        '
        Me.ComboBox12.FormattingEnabled = true
        Me.ComboBox12.Location = New System.Drawing.Point(608, 17)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(231, 21)
        Me.ComboBox12.TabIndex = 16
        '
        'DateTimePicker5
        '
        Me.DateTimePicker5.CustomFormat = "ddd MMM d, yyyy"
        Me.DateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker5.Location = New System.Drawing.Point(164, 101)
        Me.DateTimePicker5.Name = "DateTimePicker5"
        Me.DateTimePicker5.Size = New System.Drawing.Size(237, 20)
        Me.DateTimePicker5.TabIndex = 13
        '
        'DateTimePicker6
        '
        Me.DateTimePicker6.CustomFormat = "ddd MMM d, yyyy"
        Me.DateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker6.Location = New System.Drawing.Point(164, 70)
        Me.DateTimePicker6.Name = "DateTimePicker6"
        Me.DateTimePicker6.Size = New System.Drawing.Size(237, 20)
        Me.DateTimePicker6.TabIndex = 14
        '
        'Label68
        '
        Me.Label68.AutoSize = true
        Me.Label68.Location = New System.Drawing.Point(13, 107)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(86, 13)
        Me.Label68.TabIndex = 10
        Me.Label68.Text = "Completed Date:"
        '
        'Label69
        '
        Me.Label69.Location = New System.Drawing.Point(0, 0)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(100, 23)
        Me.Label69.TabIndex = 36
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGridView3)
        Me.TabPage3.Controls.Add(Me.Button9)
        Me.TabPage3.Controls.Add(Me.Button10)
        Me.TabPage3.Controls.Add(Me.Button11)
        Me.TabPage3.Controls.Add(Me.Label70)
        Me.TabPage3.Controls.Add(Me.TextBox36)
        Me.TabPage3.Controls.Add(Me.Label71)
        Me.TabPage3.Controls.Add(Me.ComboBox13)
        Me.TabPage3.Controls.Add(Me.ListBox2)
        Me.TabPage3.Controls.Add(Me.ComboBox14)
        Me.TabPage3.Controls.Add(Me.ComboBox15)
        Me.TabPage3.Controls.Add(Me.Label72)
        Me.TabPage3.Controls.Add(Me.Label73)
        Me.TabPage3.Controls.Add(Me.Label74)
        Me.TabPage3.Controls.Add(Me.Label75)
        Me.TabPage3.Controls.Add(Me.Label76)
        Me.TabPage3.Controls.Add(Me.Label77)
        Me.TabPage3.Controls.Add(Me.Label78)
        Me.TabPage3.Controls.Add(Me.Label79)
        Me.TabPage3.Controls.Add(Me.TextBox37)
        Me.TabPage3.Controls.Add(Me.TextBox38)
        Me.TabPage3.Controls.Add(Me.TextBox39)
        Me.TabPage3.Controls.Add(Me.TextBox40)
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(886, 577)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Pipes / Tubes"
        Me.TabPage3.UseVisualStyleBackColor = true
        '
        'DataGridView3
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView3.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView3.Location = New System.Drawing.Point(200, 368)
        Me.DataGridView3.Name = "DataGridView3"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView3.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView3.RowTemplate.Height = 24
        Me.DataGridView3.Size = New System.Drawing.Size(586, 113)
        Me.DataGridView3.TabIndex = 65
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Pipe / Tube Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "# of Sections"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total Length"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(769, 190)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 38)
        Me.Button9.TabIndex = 64
        Me.Button9.Text = "Remove"
        Me.Button9.UseVisualStyleBackColor = true
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(769, 134)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 38)
        Me.Button10.TabIndex = 63
        Me.Button10.Text = "Update"
        Me.Button10.UseVisualStyleBackColor = true
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(690, 287)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(96, 33)
        Me.Button11.TabIndex = 62
        Me.Button11.Text = "Add"
        Me.Button11.UseVisualStyleBackColor = true
        '
        'Label70
        '
        Me.Label70.AutoSize = true
        Me.Label70.Location = New System.Drawing.Point(530, 112)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(158, 13)
        Me.Label70.TabIndex = 61
        Me.Label70.Text = "Selected Schedule Item Details:"
        '
        'TextBox36
        '
        Me.TextBox36.Location = New System.Drawing.Point(523, 134)
        Me.TextBox36.Name = "TextBox36"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox36, false)
        Me.TextBox36.Size = New System.Drawing.Size(230, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox36, OptionsSpelling14)
        Me.TextBox36.TabIndex = 60
        '
        'Label71
        '
        Me.Label71.AutoSize = true
        Me.Label71.Location = New System.Drawing.Point(197, 294)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(113, 13)
        Me.Label71.TabIndex = 59
        Me.Label71.Text = "Add Pipe to Schedule:"
        '
        'ComboBox13
        '
        Me.ComboBox13.FormattingEnabled = true
        Me.ComboBox13.Location = New System.Drawing.Point(376, 291)
        Me.ComboBox13.Name = "ComboBox13"
        Me.ComboBox13.Size = New System.Drawing.Size(297, 21)
        Me.ComboBox13.TabIndex = 58
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = true
        Me.ListBox2.Items.AddRange(New Object() {"Pipe Schedule list goes here"})
        Me.ListBox2.Location = New System.Drawing.Point(193, 120)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(317, 108)
        Me.ListBox2.TabIndex = 57
        '
        'ComboBox14
        '
        Me.ComboBox14.FormattingEnabled = true
        Me.ComboBox14.Location = New System.Drawing.Point(193, 72)
        Me.ComboBox14.Name = "ComboBox14"
        Me.ComboBox14.Size = New System.Drawing.Size(346, 21)
        Me.ComboBox14.TabIndex = 55
        '
        'ComboBox15
        '
        Me.ComboBox15.FormattingEnabled = true
        Me.ComboBox15.Location = New System.Drawing.Point(193, 30)
        Me.ComboBox15.Name = "ComboBox15"
        Me.ComboBox15.Size = New System.Drawing.Size(346, 21)
        Me.ComboBox15.TabIndex = 56
        '
        'Label72
        '
        Me.Label72.AutoSize = true
        Me.Label72.Location = New System.Drawing.Point(27, 37)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(108, 13)
        Me.Label72.TabIndex = 54
        Me.Label72.Text = "Materials Description:"
        '
        'Label73
        '
        Me.Label73.Location = New System.Drawing.Point(36, 368)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(147, 55)
        Me.Label73.TabIndex = 50
        Me.Label73.Text = "Pipe Segment(s) Inspected:"
        '
        'Label74
        '
        Me.Label74.AutoSize = true
        Me.Label74.Location = New System.Drawing.Point(36, 125)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(79, 13)
        Me.Label74.TabIndex = 50
        Me.Label74.Text = "Pipe Schedule:"
        '
        'Label75
        '
        Me.Label75.AutoSize = true
        Me.Label75.Location = New System.Drawing.Point(33, 79)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(103, 13)
        Me.Label75.TabIndex = 49
        Me.Label75.Text = "Surface Description:"
        '
        'Label76
        '
        Me.Label76.AutoSize = true
        Me.Label76.Location = New System.Drawing.Point(501, 250)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(100, 13)
        Me.Label76.TabIndex = 48
        Me.Label76.Text = "Min. Wall Accepted"
        '
        'Label77
        '
        Me.Label77.AutoSize = true
        Me.Label77.Location = New System.Drawing.Point(554, 222)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(56, 13)
        Me.Label77.TabIndex = 53
        Me.Label77.Text = "Thickness"
        '
        'Label78
        '
        Me.Label78.AutoSize = true
        Me.Label78.Location = New System.Drawing.Point(523, 194)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(78, 13)
        Me.Label78.TabIndex = 52
        Me.Label78.Text = "Outer Diameter"
        '
        'Label79
        '
        Me.Label79.AutoSize = true
        Me.Label79.Location = New System.Drawing.Point(524, 166)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(76, 13)
        Me.Label79.TabIndex = 51
        Me.Label79.Text = "Inner Diameter"
        '
        'TextBox37
        '
        Me.TextBox37.Location = New System.Drawing.Point(643, 246)
        Me.TextBox37.Name = "TextBox37"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox37, false)
        Me.TextBox37.Size = New System.Drawing.Size(110, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox37, OptionsSpelling15)
        Me.TextBox37.TabIndex = 45
        '
        'TextBox38
        '
        Me.TextBox38.Location = New System.Drawing.Point(643, 218)
        Me.TextBox38.Name = "TextBox38"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox38, false)
        Me.TextBox38.Size = New System.Drawing.Size(110, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox38, OptionsSpelling16)
        Me.TextBox38.TabIndex = 44
        '
        'TextBox39
        '
        Me.TextBox39.Location = New System.Drawing.Point(643, 190)
        Me.TextBox39.Name = "TextBox39"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox39, false)
        Me.TextBox39.Size = New System.Drawing.Size(110, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox39, OptionsSpelling17)
        Me.TextBox39.TabIndex = 47
        '
        'TextBox40
        '
        Me.TextBox40.Location = New System.Drawing.Point(643, 162)
        Me.TextBox40.Name = "TextBox40"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox40, false)
        Me.TextBox40.Size = New System.Drawing.Size(110, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox40, OptionsSpelling18)
        Me.TextBox40.TabIndex = 46
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label80)
        Me.TabPage4.Controls.Add(Me.Label81)
        Me.TabPage4.Controls.Add(Me.Label82)
        Me.TabPage4.Controls.Add(Me.Label83)
        Me.TabPage4.Controls.Add(Me.TextBox41)
        Me.TabPage4.Controls.Add(Me.TextBox42)
        Me.TabPage4.Controls.Add(Me.TextBox43)
        Me.TabPage4.Controls.Add(Me.TextBox44)
        Me.TabPage4.Location = New System.Drawing.Point(4, 27)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(886, 577)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Report Text"
        Me.TabPage4.UseVisualStyleBackColor = true
        '
        'Label80
        '
        Me.Label80.Location = New System.Drawing.Point(17, 417)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(94, 41)
        Me.Label80.TabIndex = 3
        Me.Label80.Text = "Project Summary"
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(17, 272)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(94, 41)
        Me.Label81.TabIndex = 3
        Me.Label81.Text = "Radiant Summary"
        '
        'Label82
        '
        Me.Label82.Location = New System.Drawing.Point(17, 141)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(94, 41)
        Me.Label82.TabIndex = 3
        Me.Label82.Text = "Convection Summary"
        '
        'Label83
        '
        Me.Label83.Location = New System.Drawing.Point(17, 18)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(94, 41)
        Me.Label83.TabIndex = 3
        Me.Label83.Text = "Project Description"
        '
        'TextBox41
        '
        Me.TextBox41.Location = New System.Drawing.Point(132, 408)
        Me.TextBox41.Multiline = true
        Me.TextBox41.Name = "TextBox41"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox41, false)
        Me.TextBox41.Size = New System.Drawing.Size(603, 111)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox41, OptionsSpelling19)
        Me.TextBox41.TabIndex = 2
        '
        'TextBox42
        '
        Me.TextBox42.Location = New System.Drawing.Point(132, 269)
        Me.TextBox42.Multiline = true
        Me.TextBox42.Name = "TextBox42"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox42, false)
        Me.TextBox42.Size = New System.Drawing.Size(603, 111)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox42, OptionsSpelling20)
        Me.TextBox42.TabIndex = 2
        '
        'TextBox43
        '
        Me.TextBox43.Location = New System.Drawing.Point(132, 138)
        Me.TextBox43.Multiline = true
        Me.TextBox43.Name = "TextBox43"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox43, false)
        Me.TextBox43.Size = New System.Drawing.Size(603, 111)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox43, OptionsSpelling21)
        Me.TextBox43.TabIndex = 1
        '
        'TextBox44
        '
        Me.TextBox44.Location = New System.Drawing.Point(132, 15)
        Me.TextBox44.Multiline = true
        Me.TextBox44.Name = "TextBox44"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox44, false)
        Me.TextBox44.Size = New System.Drawing.Size(603, 106)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox44, OptionsSpelling22)
        Me.TextBox44.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.dgAppendixItems)
        Me.TabPage5.Controls.Add(Me.NumericUpDown4)
        Me.TabPage5.Controls.Add(Me.Label84)
        Me.TabPage5.Controls.Add(Me.Label52)
        Me.TabPage5.Controls.Add(Me.Label85)
        Me.TabPage5.Controls.Add(Me.Label86)
        Me.TabPage5.Controls.Add(Me.Label87)
        Me.TabPage5.Controls.Add(Me.TextBox26)
        Me.TabPage5.Controls.Add(Me.TextBox45)
        Me.TabPage5.Controls.Add(Me.TextBox46)
        Me.TabPage5.Controls.Add(Me.TextBox47)
        Me.TabPage5.Controls.Add(Me.HScrollBar2)
        Me.TabPage5.Controls.Add(Me.Label88)
        Me.TabPage5.Controls.Add(Me.Label89)
        Me.TabPage5.Controls.Add(Me.Label90)
        Me.TabPage5.Location = New System.Drawing.Point(4, 27)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(886, 577)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Report Images - OLD"
        Me.TabPage5.UseVisualStyleBackColor = true
        '
        'dgAppendixItems
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgAppendixItems.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgAppendixItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgAppendixItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewTextBoxColumn5, Me.DataGridViewButtonColumn1})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgAppendixItems.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgAppendixItems.Location = New System.Drawing.Point(58, 394)
        Me.dgAppendixItems.Name = "dgAppendixItems"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgAppendixItems.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgAppendixItems.RowTemplate.Height = 24
        Me.dgAppendixItems.Size = New System.Drawing.Size(798, 151)
        Me.dgAppendixItems.TabIndex = 6
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Image Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Included"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.HeaderText = "Auto-Select File"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Image File"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewButtonColumn1
        '
        Me.DataGridViewButtonColumn1.HeaderText = "BrowseForFile"
        Me.DataGridViewButtonColumn1.Name = "DataGridViewButtonColumn1"
        Me.DataGridViewButtonColumn1.Text = "Browse..."
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Location = New System.Drawing.Point(294, 55)
        Me.NumericUpDown4.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(76, 20)
        Me.NumericUpDown4.TabIndex = 5
        Me.NumericUpDown4.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label84
        '
        Me.Label84.AutoSize = true
        Me.Label84.Location = New System.Drawing.Point(55, 57)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(138, 13)
        Me.Label84.TabIndex = 4
        Me.Label84.Text = "Report Images from Pass #:"
        '
        'Label85
        '
        Me.Label85.AutoSize = true
        Me.Label85.Location = New System.Drawing.Point(73, 179)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(55, 13)
        Me.Label85.TabIndex = 3
        Me.Label85.Text = "Image 3C:"
        '
        'Label86
        '
        Me.Label86.AutoSize = true
        Me.Label86.Location = New System.Drawing.Point(73, 140)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(55, 13)
        Me.Label86.TabIndex = 3
        Me.Label86.Text = "Image 3B:"
        '
        'Label87
        '
        Me.Label87.AutoSize = true
        Me.Label87.Location = New System.Drawing.Point(73, 100)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(55, 13)
        Me.Label87.TabIndex = 3
        Me.Label87.Text = "Image 3A:"
        '
        'TextBox45
        '
        Me.TextBox45.Location = New System.Drawing.Point(219, 172)
        Me.TextBox45.Name = "TextBox45"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox45, false)
        Me.TextBox45.Size = New System.Drawing.Size(376, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox45, OptionsSpelling23)
        Me.TextBox45.TabIndex = 2
        '
        'TextBox46
        '
        Me.TextBox46.Location = New System.Drawing.Point(219, 137)
        Me.TextBox46.Name = "TextBox46"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox46, false)
        Me.TextBox46.Size = New System.Drawing.Size(376, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox46, OptionsSpelling24)
        Me.TextBox46.TabIndex = 2
        '
        'TextBox47
        '
        Me.TextBox47.Location = New System.Drawing.Point(219, 97)
        Me.TextBox47.Name = "TextBox47"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.TextBox47, false)
        Me.TextBox47.Size = New System.Drawing.Size(376, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.TextBox47, OptionsSpelling25)
        Me.TextBox47.TabIndex = 2
        '
        'HScrollBar2
        '
        Me.HScrollBar2.Location = New System.Drawing.Point(241, 347)
        Me.HScrollBar2.Name = "HScrollBar2"
        Me.HScrollBar2.Size = New System.Drawing.Size(102, 28)
        Me.HScrollBar2.TabIndex = 1
        '
        'Label88
        '
        Me.Label88.AutoSize = true
        Me.Label88.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label88.Location = New System.Drawing.Point(237, 318)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(90, 17)
        Me.Label88.TabIndex = 0
        Me.Label88.Text = "Pass 1 of 1"
        '
        'Label89
        '
        Me.Label89.AutoSize = true
        Me.Label89.Location = New System.Drawing.Point(35, 313)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(88, 13)
        Me.Label89.TabIndex = 0
        Me.Label89.Text = "Appendix Images"
        '
        'Label90
        '
        Me.Label90.AutoSize = true
        Me.Label90.Location = New System.Drawing.Point(35, 25)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(102, 13)
        Me.Label90.TabIndex = 0
        Me.Label90.Text = "Main Report Images"
        '
        'TabPage6
        '
        Me.TabPage6.Location = New System.Drawing.Point(4, 27)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(886, 577)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Field Report"
        Me.TabPage6.UseVisualStyleBackColor = true
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Image Type"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 200
        '
        'FileStatus
        '
        Me.FileStatus.HeaderText = "Status"
        Me.FileStatus.Name = "FileStatus"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Image File"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 250
        '
        'DataGridViewButtonColumn3
        '
        Me.DataGridViewButtonColumn3.HeaderText = "Pick File"
        Me.DataGridViewButtonColumn3.Name = "DataGridViewButtonColumn3"
        Me.DataGridViewButtonColumn3.Text = "Browse..."
        Me.DataGridViewButtonColumn3.UseColumnTextForButtonValue = true
        '
        'bsInspectionCustomers
        '
        Me.bsInspectionCustomers.DataSource = Me.bsInspections
        '
        'bsInspectionCustomer
        '
        Me.bsInspectionCustomer.DataSource = Me.bsInspectionCustomers
        '
        'TblInspectionstblCustomersBindingSource
        '
        Me.TblInspectionstblCustomersBindingSource.DataSource = Me.bsInspectionCustomers
        '
        'TblInspectionstblCustomersBindingSource1
        '
        Me.TblInspectionstblCustomersBindingSource1.DataSource = Me.bsQTT_Customers
        '
        'QTT_PipeTubeDetails
        '
        Me.QTT_PipeTubeDetails.DataSetName = "QTT_PipeTubeDetails"
        Me.QTT_PipeTubeDetails.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bsPipeTubeItemTypes
        '
        Me.bsPipeTubeItemTypes.DataMember = "tblPipeTubeItemTypes"
        Me.bsPipeTubeItemTypes.DataSource = Me.QTT_PipeTubeDetails
        '
        'taTblPipeTubeItems_Dets
        '
        Me.taTblPipeTubeItems_Dets.ClearBeforeFill = true
        '
        'taTblPipeTubeItemTypes_Dets
        '
        Me.taTblPipeTubeItemTypes_Dets.ClearBeforeFill = true
        '
        'taTblPipeTubeItemTypes
        '
        Me.taTblPipeTubeItemTypes.ClearBeforeFill = true
        '
        'taPipeTubeItemTypes
        '
        Me.taPipeTubeItemTypes.ClearBeforeFill = true
        '
        'bsPipeTubeTypes
        '
        Me.bsPipeTubeTypes.DataMember = "tblPipeTubeItems_FK01"
        Me.bsPipeTubeTypes.DataSource = Me.bsPipeTubeItemTypes
        '
        'TblInspectionstblPipeTubeItemsBindingSource
        '
        Me.TblInspectionstblPipeTubeItemsBindingSource.DataMember = "tblInspectionstblPipeTubeItems"
        Me.TblInspectionstblPipeTubeItemsBindingSource.DataSource = Me.bsInspections
        '
        'taTblInspections
        '
        Me.taTblInspections.ClearBeforeFill = true
        '
        'taTblInspectionTypes
        '
        Me.taTblInspectionTypes.ClearBeforeFill = true
        '
        'taTblInspectionsAndTools
        '
        Me.taTblInspectionsAndTools.ClearBeforeFill = true
        '
        'taTblInspectionTools
        '
        Me.taTblInspectionTools.ClearBeforeFill = true
        '
        'taTblPipeTubeItems
        '
        Me.taTblPipeTubeItems.ClearBeforeFill = true
        '
        'taTblCustomers_Inspection
        '
        Me.taTblCustomers_Inspection.ClearBeforeFill = true
        '
        'VwAppendixDetailsTableAdapter
        '
        Me.VwAppendixDetailsTableAdapter.ClearBeforeFill = true
        '
        'VwAppendixItem_DetailsTableAdapter
        '
        Me.VwAppendixItem_DetailsTableAdapter.ClearBeforeFill = true
        '
        'VwReportItems_DetailsTableAdapter
        '
        Me.VwReportItems_DetailsTableAdapter.ClearBeforeFill = true
        '
        'taPipeTubeItemSummaries
        '
        Me.taPipeTubeItemSummaries.ClearBeforeFill = true
        '
        'taInspectionsAndInspectors
        '
        Me.taInspectionsAndInspectors.ClearBeforeFill = true
        '
        'TblQTT_RolesTableAdapter
        '
        Me.TblQTT_RolesTableAdapter.ClearBeforeFill = true
        '
        'taInspections_TblPipeTubeItemTypes
        '
        Me.taInspections_TblPipeTubeItemTypes.ClearBeforeFill = true
        '
        'taCustomers_LU
        '
        Me.taCustomers_LU.ClearBeforeFill = true
        '
        'taTblQTT_Offices
        '
        Me.taTblQTT_Offices.ClearBeforeFill = true
        '
        'taTblDefaultPipeSchedules
        '
        Me.taTblDefaultPipeSchedules.ClearBeforeFill = true
        '
        'taTblQTT_Inspectors
        '
        Me.taTblQTT_Inspectors.ClearBeforeFill = true
        '
        'bsInspectionTypes_LU
        '
        Me.bsInspectionTypes_LU.DataMember = "tblInspectionTypes"
        Me.bsInspectionTypes_LU.DataSource = Me.QTT_LookupsData
        '
        'taTblInspectionTypes_LU
        '
        Me.taTblInspectionTypes_LU.ClearBeforeFill = true
        '
        'TblPipeTubeItemTypesTableAdapter
        '
        Me.TblPipeTubeItemTypesTableAdapter.ClearBeforeFill = true
        '
        'taDefaultText_PipeTubeMaterial
        '
        Me.taDefaultText_PipeTubeMaterial.ClearBeforeFill = true
        '
        'taDefaultText_PipeTubeSurface
        '
        Me.taDefaultText_PipeTubeSurface.ClearBeforeFill = true
        '
        'TblQTT_RolesTableAdapter1
        '
        Me.TblQTT_RolesTableAdapter1.ClearBeforeFill = true
        '
        'taTblPipeTubeItemTypes_1
        '
        Me.taTblPipeTubeItemTypes_1.ClearBeforeFill = true
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(167, 282)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(28, 28)
        Me.Button1.TabIndex = 57
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = true
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(137, 282)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(28, 28)
        Me.Button2.TabIndex = 56
        Me.Button2.Text = "<"
        Me.Button2.UseVisualStyleBackColor = true
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl2.Horizontal = false
        Me.SplitContainerControl2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiDateButton)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiDayDateButton)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiMonthYearButton)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.Label16)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiMonthCalendar)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiProjectDescriptionEditor)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiClearRedProjectDescriptionButton)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.uiInsertProjectDescriptionButton)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.Label93)
        Me.SplitContainerControl2.Panel1.MinSize = 260
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.uiNumberingSystem)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.uiTubeIdentificationEditor)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.uiShowHideButton)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.uiClearRedTubeIdentificationButton)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.uiInsertTubeIdentificationButton)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.Label98)
        Me.SplitContainerControl2.Panel2.MinSize = 120
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(1162, 576)
        Me.SplitContainerControl2.SplitterPosition = 314
        Me.SplitContainerControl2.TabIndex = 36
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'uiDateButton
        '
        Me.uiDateButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiDateButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiDateButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiDateButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiDateButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDateButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiDateButton.Appearance.Options.UseBackColor = true
        Me.uiDateButton.Appearance.Options.UseBorderColor = true
        Me.uiDateButton.Appearance.Options.UseFont = true
        Me.uiDateButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDateButton.Location = New System.Drawing.Point(812, 204)
        Me.uiDateButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiDateButton.Name = "uiDateButton"
        Me.uiDateButton.Size = New System.Drawing.Size(98, 32)
        Me.uiDateButton.TabIndex = 10
        Me.uiDateButton.Text = "Date"
        '
        'uiDayDateButton
        '
        Me.uiDayDateButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiDayDateButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiDayDateButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiDayDateButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiDayDateButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDayDateButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiDayDateButton.Appearance.Options.UseBackColor = true
        Me.uiDayDateButton.Appearance.Options.UseBorderColor = true
        Me.uiDayDateButton.Appearance.Options.UseFont = true
        Me.uiDayDateButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDayDateButton.Location = New System.Drawing.Point(812, 158)
        Me.uiDayDateButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiDayDateButton.Name = "uiDayDateButton"
        Me.uiDayDateButton.Size = New System.Drawing.Size(98, 32)
        Me.uiDayDateButton.TabIndex = 10
        Me.uiDayDateButton.Text = "Day, Date"
        '
        'uiMonthYearButton
        '
        Me.uiMonthYearButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiMonthYearButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiMonthYearButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiMonthYearButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiMonthYearButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMonthYearButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiMonthYearButton.Appearance.Options.UseBackColor = true
        Me.uiMonthYearButton.Appearance.Options.UseBorderColor = true
        Me.uiMonthYearButton.Appearance.Options.UseFont = true
        Me.uiMonthYearButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiMonthYearButton.Location = New System.Drawing.Point(812, 112)
        Me.uiMonthYearButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiMonthYearButton.Name = "uiMonthYearButton"
        Me.uiMonthYearButton.Size = New System.Drawing.Size(98, 32)
        Me.uiMonthYearButton.TabIndex = 10
        Me.uiMonthYearButton.Text = "Month of Year"
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 9!)
        Me.Label16.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label16.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Label16.Location = New System.Drawing.Point(973, 44)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(183, 49)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Select date and click format to insert date at cursor or to replace selected text"& _ 
    " with date."
        '
        'uiMonthCalendar
        '
        Me.uiMonthCalendar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiMonthCalendar.Location = New System.Drawing.Point(924, 99)
        Me.uiMonthCalendar.MaxSelectionCount = 1
        Me.uiMonthCalendar.Name = "uiMonthCalendar"
        Me.uiMonthCalendar.TabIndex = 5
        Me.uiMonthCalendar.TitleBackColor = System.Drawing.Color.LightSteelBlue
        Me.uiMonthCalendar.TitleForeColor = System.Drawing.Color.DarkSlateBlue
        '
        'uiProjectDescriptionEditor
        '
        Me.uiProjectDescriptionEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiProjectDescriptionEditor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiProjectDescriptionEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiProjectDescriptionEditor.Appearance.Text.Options.UseFont = true
        Me.uiProjectDescriptionEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "ProjectDescription", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.uiProjectDescriptionEditor.Location = New System.Drawing.Point(5, 25)
        Me.uiProjectDescriptionEditor.Name = "uiProjectDescriptionEditor"
        Me.uiProjectDescriptionEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiProjectDescriptionEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiProjectDescriptionEditor, false)
        Me.uiProjectDescriptionEditor.Size = New System.Drawing.Size(800, 284)
        Me.uiProjectDescriptionEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiProjectDescriptionEditor, OptionsSpelling26)
        Me.uiProjectDescriptionEditor.TabIndex = 8
        Me.uiProjectDescriptionEditor.Tag = "Project Description"
        Me.uiProjectDescriptionEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.Culture = New System.Globalization.CultureInfo("en-US")
        Me.SpellChecker1.ParentContainer = Nothing
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.AsYouType
        '
        'uiProjectNumber
        '
        Me.uiProjectNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "ProjectNumber", true))
        Me.uiProjectNumber.EditValue = ""
        Me.uiProjectNumber.Location = New System.Drawing.Point(699, 268)
        Me.uiProjectNumber.Name = "uiProjectNumber"
        Me.uiProjectNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProjectNumber.Properties.Appearance.Options.UseFont = true
        Me.uiProjectNumber.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProjectNumber.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiProjectNumber.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProjectNumber.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiProjectNumber.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProjectNumber.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiProjectNumber.Properties.MaxLength = 25
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiProjectNumber, true)
        Me.uiProjectNumber.Size = New System.Drawing.Size(140, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiProjectNumber, OptionsSpelling28)
        Me.uiProjectNumber.TabIndex = 27
        '
        'uiProposalNumber
        '
        Me.uiProposalNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "ContractNumber", true))
        Me.uiProposalNumber.EditValue = ""
        Me.uiProposalNumber.Location = New System.Drawing.Point(432, 268)
        Me.uiProposalNumber.Name = "uiProposalNumber"
        Me.uiProposalNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProposalNumber.Properties.Appearance.Options.UseFont = true
        Me.uiProposalNumber.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProposalNumber.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiProposalNumber.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProposalNumber.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiProposalNumber.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProposalNumber.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiProposalNumber.Properties.MaxLength = 25
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiProposalNumber, true)
        Me.uiProposalNumber.Size = New System.Drawing.Size(140, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiProposalNumber, OptionsSpelling29)
        Me.uiProposalNumber.TabIndex = 25
        '
        'uiCustomTubeFormatEditor
        '
        Me.uiCustomTubeFormatEditor.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "CustomTubes", true))
        Me.uiCustomTubeFormatEditor.EditValue = ""
        Me.uiCustomTubeFormatEditor.Location = New System.Drawing.Point(549, 525)
        Me.uiCustomTubeFormatEditor.Name = "uiCustomTubeFormatEditor"
        Me.uiCustomTubeFormatEditor.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomTubeFormatEditor.Properties.Appearance.Options.UseFont = true
        Me.uiCustomTubeFormatEditor.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomTubeFormatEditor.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCustomTubeFormatEditor.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomTubeFormatEditor.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCustomTubeFormatEditor.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomTubeFormatEditor.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiCustomTubeFormatEditor.Properties.MaxLength = 255
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiCustomTubeFormatEditor, true)
        Me.uiCustomTubeFormatEditor.Size = New System.Drawing.Size(287, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiCustomTubeFormatEditor, OptionsSpelling30)
        Me.uiCustomTubeFormatEditor.TabIndex = 44
        '
        'uiInspectorPhone
        '
        Me.uiInspectorPhone.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "PrimaryInspector_Phone", true))
        Me.uiInspectorPhone.EditValue = ""
        Me.uiInspectorPhone.Location = New System.Drawing.Point(667, 63)
        Me.uiInspectorPhone.Name = "uiInspectorPhone"
        Me.uiInspectorPhone.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectorPhone.Properties.Appearance.Options.UseFont = true
        Me.uiInspectorPhone.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectorPhone.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiInspectorPhone.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectorPhone.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiInspectorPhone.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiInspectorPhone, true)
        Me.uiInspectorPhone.Size = New System.Drawing.Size(172, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiInspectorPhone, OptionsSpelling31)
        Me.uiInspectorPhone.TabIndex = 12
        Me.uiInspectorPhone.ToolTip = "If phone number and extension are incorrect or missing, fix in System Maintenance"& _ 
    "."
        '
        'uiPostalCode
        '
        Me.uiPostalCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "PostalCode", true))
        Me.uiPostalCode.Location = New System.Drawing.Point(747, 129)
        Me.uiPostalCode.Name = "uiPostalCode"
        Me.uiPostalCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPostalCode.Properties.Appearance.Options.UseFont = true
        Me.uiPostalCode.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPostalCode.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiPostalCode.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPostalCode.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiPostalCode.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPostalCode.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiPostalCode.Properties.MaxLength = 20
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiPostalCode, true)
        Me.uiPostalCode.Size = New System.Drawing.Size(138, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiPostalCode, OptionsSpelling32)
        Me.uiPostalCode.TabIndex = 14
        ConditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule1.ErrorText = "Postal Code cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiPostalCode, ConditionValidationRule1)
        '
        'uiPriorHistoryEditor
        '
        Me.uiPriorHistoryEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiPriorHistoryEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPriorHistoryEditor.Appearance.Text.Options.UseFont = true
        Me.uiPriorHistoryEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "PriorHistory", true))
        Me.uiPriorHistoryEditor.Location = New System.Drawing.Point(10, 442)
        Me.uiPriorHistoryEditor.Name = "uiPriorHistoryEditor"
        Me.uiPriorHistoryEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiPriorHistoryEditor, false)
        Me.uiPriorHistoryEditor.Size = New System.Drawing.Size(728, 99)
        Me.uiPriorHistoryEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiPriorHistoryEditor, OptionsSpelling33)
        Me.uiPriorHistoryEditor.TabIndex = 31
        Me.uiPriorHistoryEditor.Tag = "Prior History"
        Me.uiPriorHistoryEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiCity
        '
        Me.uiCity.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "City", true))
        Me.uiCity.Location = New System.Drawing.Point(138, 129)
        Me.uiCity.Name = "uiCity"
        Me.uiCity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCity.Properties.Appearance.Options.UseFont = true
        Me.uiCity.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCity.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCity.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCity.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCity.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCity.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiCity.Properties.MaxLength = 60
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiCity, true)
        Me.uiCity.Size = New System.Drawing.Size(249, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiCity, OptionsSpelling34)
        Me.uiCity.TabIndex = 10
        ConditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule2.ErrorText = "City cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiCity, ConditionValidationRule2)
        '
        'uiAddress
        '
        Me.uiAddress.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "Address", true))
        Me.uiAddress.Location = New System.Drawing.Point(138, 100)
        Me.uiAddress.Name = "uiAddress"
        Me.uiAddress.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddress.Properties.Appearance.Options.UseFont = true
        Me.uiAddress.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddress.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAddress.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddress.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAddress.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddress.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAddress.Properties.MaxLength = 255
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAddress, true)
        Me.uiAddress.Size = New System.Drawing.Size(424, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAddress, OptionsSpelling35)
        Me.uiAddress.TabIndex = 8
        ConditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule3.ErrorText = "Address cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAddress, ConditionValidationRule3)
        '
        'uiLocation
        '
        Me.uiLocation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "LocationDesc", true))
        Me.uiLocation.Location = New System.Drawing.Point(138, 71)
        Me.uiLocation.Name = "uiLocation"
        Me.uiLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocation.Properties.Appearance.Options.UseFont = true
        Me.uiLocation.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocation.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocation.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiLocation.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocation.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiLocation.Properties.MaxLength = 255
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiLocation, true)
        Me.uiLocation.Size = New System.Drawing.Size(424, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiLocation, OptionsSpelling36)
        Me.uiLocation.TabIndex = 4
        ConditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule4.ErrorText = "Plant Name cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiLocation, ConditionValidationRule4)
        '
        'uiTubeLength
        '
        Me.uiTubeLength.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "LengthInspected", true))
        Me.uiTubeLength.Location = New System.Drawing.Point(235, 334)
        Me.uiTubeLength.Name = "uiTubeLength"
        Me.uiTubeLength.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLength.Properties.Appearance.Options.UseFont = true
        Me.uiTubeLength.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeLength.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeLength.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLength.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeLength.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLength.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeLength.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLength.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeLength, true)
        Me.uiTubeLength.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeLength, OptionsSpelling37)
        Me.uiTubeLength.TabIndex = 50
        '
        'uiTubeWallThicknessDisplay
        '
        Me.uiTubeWallThicknessDisplay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "WallThicknessDisplay", true))
        Me.uiTubeWallThicknessDisplay.Location = New System.Drawing.Point(303, 303)
        Me.uiTubeWallThicknessDisplay.Name = "uiTubeWallThicknessDisplay"
        Me.uiTubeWallThicknessDisplay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThicknessDisplay.Properties.Appearance.Options.UseFont = true
        Me.uiTubeWallThicknessDisplay.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeWallThicknessDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThicknessDisplay.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeWallThicknessDisplay.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeWallThicknessDisplay, true)
        Me.uiTubeWallThicknessDisplay.Size = New System.Drawing.Size(163, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeWallThicknessDisplay, OptionsSpelling38)
        Me.uiTubeWallThicknessDisplay.TabIndex = 32
        Me.uiTubeWallThicknessDisplay.TabStop = false
        ConditionValidationRule5.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule5.ErrorText = "Minimum Sound Wall Thickness cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeWallThicknessDisplay, ConditionValidationRule5)
        '
        'uiTubeOuterDiameterDisplay
        '
        Me.uiTubeOuterDiameterDisplay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "OuterDiameterDisplay", true))
        Me.uiTubeOuterDiameterDisplay.Location = New System.Drawing.Point(303, 272)
        Me.uiTubeOuterDiameterDisplay.Name = "uiTubeOuterDiameterDisplay"
        Me.uiTubeOuterDiameterDisplay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameterDisplay.Properties.Appearance.Options.UseFont = true
        Me.uiTubeOuterDiameterDisplay.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeOuterDiameterDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameterDisplay.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeOuterDiameterDisplay.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeOuterDiameterDisplay, true)
        Me.uiTubeOuterDiameterDisplay.Size = New System.Drawing.Size(163, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeOuterDiameterDisplay, OptionsSpelling39)
        Me.uiTubeOuterDiameterDisplay.TabIndex = 29
        Me.uiTubeOuterDiameterDisplay.TabStop = false
        ConditionValidationRule6.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule6.ErrorText = "Nominal Outer Diameter cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeOuterDiameterDisplay, ConditionValidationRule6)
        '
        'uiTubeInnerDiameterDisplay
        '
        Me.uiTubeInnerDiameterDisplay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "InnerDiameterDisplay", true))
        Me.uiTubeInnerDiameterDisplay.Location = New System.Drawing.Point(303, 241)
        Me.uiTubeInnerDiameterDisplay.Name = "uiTubeInnerDiameterDisplay"
        Me.uiTubeInnerDiameterDisplay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameterDisplay.Properties.Appearance.Options.UseFont = true
        Me.uiTubeInnerDiameterDisplay.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeInnerDiameterDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameterDisplay.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeInnerDiameterDisplay.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeInnerDiameterDisplay, true)
        Me.uiTubeInnerDiameterDisplay.Size = New System.Drawing.Size(163, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeInnerDiameterDisplay, OptionsSpelling40)
        Me.uiTubeInnerDiameterDisplay.TabIndex = 26
        Me.uiTubeInnerDiameterDisplay.TabStop = false
        ConditionValidationRule7.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule7.ErrorText = "Nominal Inner Diameter cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeInnerDiameterDisplay, ConditionValidationRule7)
        '
        'uiTubeWallThickness
        '
        Me.uiTubeWallThickness.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "WallThickness", true))
        Me.uiTubeWallThickness.Location = New System.Drawing.Point(235, 303)
        Me.uiTubeWallThickness.Name = "uiTubeWallThickness"
        Me.uiTubeWallThickness.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThickness.Properties.Appearance.Options.UseFont = true
        Me.uiTubeWallThickness.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeWallThickness.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeWallThickness.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThickness.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeWallThickness.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThickness.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeWallThickness.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeWallThickness.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeWallThickness.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiTubeWallThickness.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiTubeWallThickness.Properties.Mask.ShowPlaceHolders = false
        Me.uiTubeWallThickness.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeWallThickness, true)
        Me.uiTubeWallThickness.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeWallThickness, OptionsSpelling41)
        Me.uiTubeWallThickness.TabIndex = 31
        '
        'uiTubeOuterDiameter
        '
        Me.uiTubeOuterDiameter.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "OuterDiameter", true))
        Me.uiTubeOuterDiameter.Location = New System.Drawing.Point(235, 272)
        Me.uiTubeOuterDiameter.Name = "uiTubeOuterDiameter"
        Me.uiTubeOuterDiameter.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameter.Properties.Appearance.Options.UseFont = true
        Me.uiTubeOuterDiameter.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeOuterDiameter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeOuterDiameter.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameter.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeOuterDiameter.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameter.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeOuterDiameter.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeOuterDiameter.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeOuterDiameter.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiTubeOuterDiameter.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiTubeOuterDiameter.Properties.Mask.ShowPlaceHolders = false
        Me.uiTubeOuterDiameter.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeOuterDiameter, true)
        Me.uiTubeOuterDiameter.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeOuterDiameter, OptionsSpelling42)
        Me.uiTubeOuterDiameter.TabIndex = 28
        '
        'uiTubeInnerDiameter
        '
        Me.uiTubeInnerDiameter.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "InnerDiameter", true))
        Me.uiTubeInnerDiameter.Location = New System.Drawing.Point(235, 241)
        Me.uiTubeInnerDiameter.Name = "uiTubeInnerDiameter"
        Me.uiTubeInnerDiameter.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameter.Properties.Appearance.Options.UseFont = true
        Me.uiTubeInnerDiameter.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeInnerDiameter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeInnerDiameter.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameter.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeInnerDiameter.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameter.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeInnerDiameter.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInnerDiameter.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeInnerDiameter.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiTubeInnerDiameter.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiTubeInnerDiameter.Properties.Mask.ShowPlaceHolders = false
        Me.uiTubeInnerDiameter.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeInnerDiameter, true)
        Me.uiTubeInnerDiameter.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeInnerDiameter, OptionsSpelling43)
        Me.uiTubeInnerDiameter.TabIndex = 25
        '
        'uiTubeInspectionLocations
        '
        Me.uiTubeInspectionLocations.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiTubeInspectionLocations.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiTubeInspectionLocations.Appearance.Text.Options.UseFont = true
        Me.uiTubeInspectionLocations.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections_PipeTubeItems, "InspectionLocations", true))
        Me.uiTubeInspectionLocations.Location = New System.Drawing.Point(207, 394)
        Me.uiTubeInspectionLocations.Name = "uiTubeInspectionLocations"
        Me.uiTubeInspectionLocations.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeInspectionLocations, false)
        Me.uiTubeInspectionLocations.Size = New System.Drawing.Size(413, 55)
        Me.uiTubeInspectionLocations.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeInspectionLocations, OptionsSpelling44)
        Me.uiTubeInspectionLocations.TabIndex = 42
        Me.uiTubeInspectionLocations.Tag = "Tube Inspection Coverage"
        Me.uiTubeInspectionLocations.Text = "100% of tube axial length inside firebox (unless otherwise noted)"
        Me.uiTubeInspectionLocations.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiReportFilename
        '
        Me.uiReportFilename.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "ReportFilename", true))
        Me.uiReportFilename.EditValue = ""
        Me.uiReportFilename.Location = New System.Drawing.Point(143, 213)
        Me.uiReportFilename.Name = "uiReportFilename"
        Me.uiReportFilename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReportFilename.Properties.Appearance.Options.UseFont = true
        Me.uiReportFilename.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReportFilename.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiReportFilename.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReportFilename.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiReportFilename.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiReportFilename, true)
        Me.uiReportFilename.Size = New System.Drawing.Size(590, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiReportFilename, OptionsSpelling45)
        Me.uiReportFilename.TabIndex = 80
        '
        'uiTubeIdentificationEditor
        '
        Me.uiTubeIdentificationEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiTubeIdentificationEditor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiTubeIdentificationEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiTubeIdentificationEditor.Appearance.Text.Options.UseFont = true
        Me.uiTubeIdentificationEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "InspectionLayoutDescription", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.uiTubeIdentificationEditor.Location = New System.Drawing.Point(5, 27)
        Me.uiTubeIdentificationEditor.Name = "uiTubeIdentificationEditor"
        Me.uiTubeIdentificationEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeIdentificationEditor, false)
        Me.uiTubeIdentificationEditor.Size = New System.Drawing.Size(800, 222)
        Me.uiTubeIdentificationEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeIdentificationEditor, OptionsSpelling27)
        Me.uiTubeIdentificationEditor.TabIndex = 6
        Me.uiTubeIdentificationEditor.Tag = "Reformer Tube Identification"
        Me.uiTubeIdentificationEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiPurchaseOrder
        '
        Me.uiPurchaseOrder.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "PurchaseOrderNumber", true))
        Me.uiPurchaseOrder.Location = New System.Drawing.Point(135, 268)
        Me.uiPurchaseOrder.Name = "uiPurchaseOrder"
        Me.uiPurchaseOrder.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPurchaseOrder.Properties.Appearance.Options.UseFont = true
        Me.uiPurchaseOrder.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPurchaseOrder.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiPurchaseOrder.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPurchaseOrder.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiPurchaseOrder.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPurchaseOrder.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiPurchaseOrder.Properties.MaxLength = 25
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiPurchaseOrder, true)
        Me.uiPurchaseOrder.Size = New System.Drawing.Size(139, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiPurchaseOrder, OptionsSpelling46)
        Me.uiPurchaseOrder.TabIndex = 23
        '
        'uiSummaryEditor
        '
        Me.uiSummaryEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiSummaryEditor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiSummaryEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiSummaryEditor.Appearance.Text.Options.UseFont = true
        Me.uiSummaryEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "ProjectSummary", true))
        Me.uiSummaryEditor.Location = New System.Drawing.Point(555, 8)
        Me.uiSummaryEditor.Name = "uiSummaryEditor"
        Me.uiSummaryEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiSummaryEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiSummaryEditor, false)
        Me.uiSummaryEditor.Size = New System.Drawing.Size(604, 558)
        Me.uiSummaryEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiSummaryEditor, OptionsSpelling47)
        Me.uiSummaryEditor.TabIndex = 45
        Me.uiSummaryEditor.Tag = "Executive Summary"
        Me.uiSummaryEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiCustomRowFormatEditor
        '
        Me.uiCustomRowFormatEditor.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "CustomRows", true))
        Me.uiCustomRowFormatEditor.EditValue = ""
        Me.uiCustomRowFormatEditor.Location = New System.Drawing.Point(220, 525)
        Me.uiCustomRowFormatEditor.Name = "uiCustomRowFormatEditor"
        Me.uiCustomRowFormatEditor.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomRowFormatEditor.Properties.Appearance.Options.UseFont = true
        Me.uiCustomRowFormatEditor.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomRowFormatEditor.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCustomRowFormatEditor.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomRowFormatEditor.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCustomRowFormatEditor.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomRowFormatEditor.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiCustomRowFormatEditor.Properties.MaxLength = 255
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiCustomRowFormatEditor, true)
        Me.uiCustomRowFormatEditor.Size = New System.Drawing.Size(287, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiCustomRowFormatEditor, OptionsSpelling48)
        Me.uiCustomRowFormatEditor.TabIndex = 38
        '
        'uiNumberTubesTotal
        '
        Me.uiNumberTubesTotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "NumberTubesTotal", true))
        Me.uiNumberTubesTotal.Location = New System.Drawing.Point(207, 485)
        Me.uiNumberTubesTotal.Name = "uiNumberTubesTotal"
        Me.uiNumberTubesTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesTotal.Properties.Appearance.Options.UseFont = true
        Me.uiNumberTubesTotal.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNumberTubesTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberTubesTotal.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesTotal.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberTubesTotal.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesTotal.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNumberTubesTotal.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesTotal.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNumberTubesTotal.Properties.EditFormat.FormatString = "#"
        Me.uiNumberTubesTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberTubesTotal.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNumberTubesTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNumberTubesTotal.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNumberTubesTotal, true)
        Me.uiNumberTubesTotal.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNumberTubesTotal, OptionsSpelling49)
        Me.uiNumberTubesTotal.TabIndex = 47
        ConditionValidationRule8.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule8.ErrorText = "Number of Tubes in Reformer cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNumberTubesTotal, ConditionValidationRule8)
        '
        'uiPlusTubeTolerance
        '
        Me.uiPlusTubeTolerance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "DesignTolerancePlus", true))
        Me.uiPlusTubeTolerance.Enabled = false
        Me.uiPlusTubeTolerance.Location = New System.Drawing.Point(794, 269)
        Me.uiPlusTubeTolerance.Name = "uiPlusTubeTolerance"
        Me.uiPlusTubeTolerance.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlusTubeTolerance.Properties.Appearance.Options.UseFont = true
        Me.uiPlusTubeTolerance.Properties.Appearance.Options.UseTextOptions = true
        Me.uiPlusTubeTolerance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPlusTubeTolerance.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlusTubeTolerance.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiPlusTubeTolerance.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlusTubeTolerance.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiPlusTubeTolerance.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlusTubeTolerance.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiPlusTubeTolerance.Properties.EditFormat.FormatString = "#"
        Me.uiPlusTubeTolerance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiPlusTubeTolerance.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiPlusTubeTolerance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiPlusTubeTolerance.Properties.Mask.ShowPlaceHolders = false
        Me.uiPlusTubeTolerance.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiPlusTubeTolerance, true)
        Me.uiPlusTubeTolerance.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiPlusTubeTolerance, OptionsSpelling50)
        Me.uiPlusTubeTolerance.TabIndex = 36
        '
        'uiMinusTubeTolerance
        '
        Me.uiMinusTubeTolerance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "DesignToleranceMinus", true))
        Me.uiMinusTubeTolerance.Enabled = false
        Me.uiMinusTubeTolerance.Location = New System.Drawing.Point(904, 269)
        Me.uiMinusTubeTolerance.Name = "uiMinusTubeTolerance"
        Me.uiMinusTubeTolerance.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMinusTubeTolerance.Properties.Appearance.Options.UseFont = true
        Me.uiMinusTubeTolerance.Properties.Appearance.Options.UseTextOptions = true
        Me.uiMinusTubeTolerance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiMinusTubeTolerance.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMinusTubeTolerance.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiMinusTubeTolerance.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMinusTubeTolerance.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiMinusTubeTolerance.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMinusTubeTolerance.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiMinusTubeTolerance.Properties.EditFormat.FormatString = "#"
        Me.uiMinusTubeTolerance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiMinusTubeTolerance.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiMinusTubeTolerance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiMinusTubeTolerance.Properties.Mask.ShowPlaceHolders = false
        Me.uiMinusTubeTolerance.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiMinusTubeTolerance, true)
        Me.uiMinusTubeTolerance.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiMinusTubeTolerance, OptionsSpelling51)
        Me.uiMinusTubeTolerance.TabIndex = 38
        '
        'uiTubeDesignToleranceDisplay
        '
        Me.uiTubeDesignToleranceDisplay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "InspectionThreshold", true))
        Me.uiTubeDesignToleranceDisplay.Location = New System.Drawing.Point(729, 299)
        Me.uiTubeDesignToleranceDisplay.Name = "uiTubeDesignToleranceDisplay"
        Me.uiTubeDesignToleranceDisplay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeDesignToleranceDisplay.Properties.Appearance.Options.UseFont = true
        Me.uiTubeDesignToleranceDisplay.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeDesignToleranceDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeDesignToleranceDisplay.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeDesignToleranceDisplay.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeDesignToleranceDisplay, true)
        Me.uiTubeDesignToleranceDisplay.Size = New System.Drawing.Size(282, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeDesignToleranceDisplay, OptionsSpelling52)
        Me.uiTubeDesignToleranceDisplay.TabIndex = 39
        Me.uiTubeDesignToleranceDisplay.TabStop = false
        ConditionValidationRule9.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule9.ErrorText = "Design Tolerance cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeDesignToleranceDisplay, ConditionValidationRule9)
        '
        'uiRedTubeCount
        '
        Me.uiRedTubeCount.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "RedCount", true))
        Me.uiRedTubeCount.Location = New System.Drawing.Point(161, 32)
        Me.uiRedTubeCount.Name = "uiRedTubeCount"
        Me.uiRedTubeCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRedTubeCount.Properties.Appearance.Options.UseFont = true
        Me.uiRedTubeCount.Properties.Appearance.Options.UseTextOptions = true
        Me.uiRedTubeCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiRedTubeCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRedTubeCount.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiRedTubeCount.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRedTubeCount.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiRedTubeCount.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRedTubeCount.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiRedTubeCount.Properties.EditFormat.FormatString = "#"
        Me.uiRedTubeCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiRedTubeCount.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiRedTubeCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiRedTubeCount.Properties.Mask.ShowPlaceHolders = false
        Me.uiRedTubeCount.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiRedTubeCount, true)
        Me.uiRedTubeCount.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiRedTubeCount, OptionsSpelling53)
        Me.uiRedTubeCount.TabIndex = 2
        ConditionValidationRule10.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule10.ErrorText = "Red Tube Count cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiRedTubeCount, ConditionValidationRule10)
        '
        'bsInspections_AgedSummary
        '
        Me.bsInspections_AgedSummary.DataMember = "tblInspections_tblReformerAgedSummary"
        Me.bsInspections_AgedSummary.DataSource = Me.bsInspections
        '
        'uiOverallTubesInspected
        '
        Me.uiOverallTubesInspected.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "NumberTubesInspected", true))
        Me.uiOverallTubesInspected.Location = New System.Drawing.Point(464, 32)
        Me.uiOverallTubesInspected.Name = "uiOverallTubesInspected"
        Me.uiOverallTubesInspected.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallTubesInspected.Properties.Appearance.Options.UseFont = true
        Me.uiOverallTubesInspected.Properties.Appearance.Options.UseTextOptions = true
        Me.uiOverallTubesInspected.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiOverallTubesInspected.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallTubesInspected.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOverallTubesInspected.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallTubesInspected.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiOverallTubesInspected.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallTubesInspected.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiOverallTubesInspected.Properties.EditFormat.FormatString = "#"
        Me.uiOverallTubesInspected.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiOverallTubesInspected.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiOverallTubesInspected.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiOverallTubesInspected.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiOverallTubesInspected, true)
        Me.uiOverallTubesInspected.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiOverallTubesInspected, OptionsSpelling54)
        Me.uiOverallTubesInspected.TabIndex = 6
        ConditionValidationRule11.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule11.ErrorText = "Number of Tubes Inspected cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiOverallTubesInspected, ConditionValidationRule11)
        '
        'uiSwellingOver3PercentCount
        '
        Me.uiSwellingOver3PercentCount.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "SwellingCount", true))
        Me.uiSwellingOver3PercentCount.Location = New System.Drawing.Point(187, 227)
        Me.uiSwellingOver3PercentCount.Name = "uiSwellingOver3PercentCount"
        Me.uiSwellingOver3PercentCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSwellingOver3PercentCount.Properties.Appearance.Options.UseFont = true
        Me.uiSwellingOver3PercentCount.Properties.Appearance.Options.UseTextOptions = true
        Me.uiSwellingOver3PercentCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiSwellingOver3PercentCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSwellingOver3PercentCount.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiSwellingOver3PercentCount.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSwellingOver3PercentCount.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiSwellingOver3PercentCount.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSwellingOver3PercentCount.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiSwellingOver3PercentCount.Properties.EditFormat.FormatString = "#"
        Me.uiSwellingOver3PercentCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiSwellingOver3PercentCount.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiSwellingOver3PercentCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiSwellingOver3PercentCount.Properties.Mask.ShowPlaceHolders = false
        Me.uiSwellingOver3PercentCount.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiSwellingOver3PercentCount, true)
        Me.uiSwellingOver3PercentCount.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiSwellingOver3PercentCount, OptionsSpelling55)
        Me.uiSwellingOver3PercentCount.TabIndex = 22
        ConditionValidationRule12.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule12.ErrorText = "Diametrical Growth Tube Count cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiSwellingOver3PercentCount, ConditionValidationRule12)
        '
        'uiBulgingTubeCount
        '
        Me.uiBulgingTubeCount.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "BulgingCount", true))
        Me.uiBulgingTubeCount.Location = New System.Drawing.Point(65, 155)
        Me.uiBulgingTubeCount.Name = "uiBulgingTubeCount"
        Me.uiBulgingTubeCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingTubeCount.Properties.Appearance.Options.UseFont = true
        Me.uiBulgingTubeCount.Properties.Appearance.Options.UseTextOptions = true
        Me.uiBulgingTubeCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiBulgingTubeCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingTubeCount.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiBulgingTubeCount.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingTubeCount.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiBulgingTubeCount.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingTubeCount.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiBulgingTubeCount.Properties.EditFormat.FormatString = "#"
        Me.uiBulgingTubeCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiBulgingTubeCount.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiBulgingTubeCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiBulgingTubeCount.Properties.Mask.ShowPlaceHolders = false
        Me.uiBulgingTubeCount.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiBulgingTubeCount, true)
        Me.uiBulgingTubeCount.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiBulgingTubeCount, OptionsSpelling56)
        Me.uiBulgingTubeCount.TabIndex = 11
        ConditionValidationRule13.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule13.ErrorText = "Bulging Tube Count cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiBulgingTubeCount, ConditionValidationRule13)
        '
        'uiWorstTubeIDRow
        '
        Me.uiWorstTubeIDRow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "BulgingWorstRowID", true))
        Me.uiWorstTubeIDRow.Location = New System.Drawing.Point(155, 155)
        Me.uiWorstTubeIDRow.Name = "uiWorstTubeIDRow"
        Me.uiWorstTubeIDRow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDRow.Properties.Appearance.Options.UseFont = true
        Me.uiWorstTubeIDRow.Properties.Appearance.Options.UseTextOptions = true
        Me.uiWorstTubeIDRow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiWorstTubeIDRow.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDRow.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiWorstTubeIDRow.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDRow.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiWorstTubeIDRow.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDRow.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiWorstTubeIDRow.Properties.EditFormat.FormatString = "#"
        Me.uiWorstTubeIDRow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiWorstTubeIDRow.Properties.Mask.ShowPlaceHolders = false
        Me.uiWorstTubeIDRow.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiWorstTubeIDRow, true)
        Me.uiWorstTubeIDRow.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiWorstTubeIDRow, OptionsSpelling57)
        Me.uiWorstTubeIDRow.TabIndex = 14
        ConditionValidationRule14.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule14.ErrorText = "Worst Tube Row ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiWorstTubeIDRow, ConditionValidationRule14)
        '
        'uiWorstTubeIDTube
        '
        Me.uiWorstTubeIDTube.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "BulgingWorstTubeID", true))
        Me.uiWorstTubeIDTube.Location = New System.Drawing.Point(221, 155)
        Me.uiWorstTubeIDTube.Name = "uiWorstTubeIDTube"
        Me.uiWorstTubeIDTube.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDTube.Properties.Appearance.Options.UseFont = true
        Me.uiWorstTubeIDTube.Properties.Appearance.Options.UseTextOptions = true
        Me.uiWorstTubeIDTube.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiWorstTubeIDTube.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDTube.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiWorstTubeIDTube.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDTube.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiWorstTubeIDTube.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstTubeIDTube.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiWorstTubeIDTube.Properties.EditFormat.FormatString = "#"
        Me.uiWorstTubeIDTube.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiWorstTubeIDTube.Properties.Mask.ShowPlaceHolders = false
        Me.uiWorstTubeIDTube.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiWorstTubeIDTube, true)
        Me.uiWorstTubeIDTube.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiWorstTubeIDTube, OptionsSpelling58)
        Me.uiWorstTubeIDTube.TabIndex = 16
        ConditionValidationRule15.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule15.ErrorText = "Worst Tube ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiWorstTubeIDTube, ConditionValidationRule15)
        '
        'uiBulgingWorstPercent
        '
        Me.uiBulgingWorstPercent.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "BulgingWorstPercent", true))
        Me.uiBulgingWorstPercent.Location = New System.Drawing.Point(288, 155)
        Me.uiBulgingWorstPercent.Name = "uiBulgingWorstPercent"
        Me.uiBulgingWorstPercent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingWorstPercent.Properties.Appearance.Options.UseFont = true
        Me.uiBulgingWorstPercent.Properties.Appearance.Options.UseTextOptions = true
        Me.uiBulgingWorstPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiBulgingWorstPercent.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingWorstPercent.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiBulgingWorstPercent.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingWorstPercent.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiBulgingWorstPercent.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBulgingWorstPercent.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiBulgingWorstPercent.Properties.EditFormat.FormatString = "#"
        Me.uiBulgingWorstPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiBulgingWorstPercent.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiBulgingWorstPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiBulgingWorstPercent.Properties.Mask.ShowPlaceHolders = false
        Me.uiBulgingWorstPercent.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiBulgingWorstPercent, true)
        Me.uiBulgingWorstPercent.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiBulgingWorstPercent, OptionsSpelling59)
        Me.uiBulgingWorstPercent.TabIndex = 18
        ConditionValidationRule16.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule16.ErrorText = "Worst Tube Worst % cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiBulgingWorstPercent, ConditionValidationRule16)
        '
        'uiYellowTubeCount
        '
        Me.uiYellowTubeCount.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "YellowCount", true))
        Me.uiYellowTubeCount.Location = New System.Drawing.Point(161, 61)
        Me.uiYellowTubeCount.Name = "uiYellowTubeCount"
        Me.uiYellowTubeCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiYellowTubeCount.Properties.Appearance.Options.UseFont = true
        Me.uiYellowTubeCount.Properties.Appearance.Options.UseTextOptions = true
        Me.uiYellowTubeCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiYellowTubeCount.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiYellowTubeCount.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiYellowTubeCount.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiYellowTubeCount.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiYellowTubeCount.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiYellowTubeCount.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiYellowTubeCount.Properties.EditFormat.FormatString = "#"
        Me.uiYellowTubeCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiYellowTubeCount.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiYellowTubeCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiYellowTubeCount.Properties.Mask.ShowPlaceHolders = false
        Me.uiYellowTubeCount.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiYellowTubeCount, true)
        Me.uiYellowTubeCount.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiYellowTubeCount, OptionsSpelling60)
        Me.uiYellowTubeCount.TabIndex = 4
        ConditionValidationRule17.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule17.ErrorText = "Yellow Tube Count cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiYellowTubeCount, ConditionValidationRule17)
        '
        'uiAgedTubeCount3
        '
        Me.uiAgedTubeCount3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGTubeCount3", true))
        Me.uiAgedTubeCount3.Enabled = false
        Me.uiAgedTubeCount3.Location = New System.Drawing.Point(86, 184)
        Me.uiAgedTubeCount3.Name = "uiAgedTubeCount3"
        Me.uiAgedTubeCount3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount3.Properties.Appearance.Options.UseFont = true
        Me.uiAgedTubeCount3.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAgedTubeCount3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAgedTubeCount3.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount3.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAgedTubeCount3.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount3.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAgedTubeCount3.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount3.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAgedTubeCount3.Properties.EditFormat.FormatString = "#"
        Me.uiAgedTubeCount3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiAgedTubeCount3.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiAgedTubeCount3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiAgedTubeCount3.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAgedTubeCount3, true)
        Me.uiAgedTubeCount3.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAgedTubeCount3, OptionsSpelling61)
        Me.uiAgedTubeCount3.TabIndex = 13
        ConditionValidationRule18.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule18.ErrorText = "Diametrical Growth Tube Count #3 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAgedTubeCount3, ConditionValidationRule18)
        '
        'uiAgedTubeCount2
        '
        Me.uiAgedTubeCount2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGTubeCount2", true))
        Me.uiAgedTubeCount2.Enabled = false
        Me.uiAgedTubeCount2.Location = New System.Drawing.Point(86, 155)
        Me.uiAgedTubeCount2.Name = "uiAgedTubeCount2"
        Me.uiAgedTubeCount2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount2.Properties.Appearance.Options.UseFont = true
        Me.uiAgedTubeCount2.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAgedTubeCount2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAgedTubeCount2.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount2.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAgedTubeCount2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount2.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAgedTubeCount2.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount2.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAgedTubeCount2.Properties.EditFormat.FormatString = "#"
        Me.uiAgedTubeCount2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiAgedTubeCount2.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiAgedTubeCount2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiAgedTubeCount2.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAgedTubeCount2, true)
        Me.uiAgedTubeCount2.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAgedTubeCount2, OptionsSpelling62)
        Me.uiAgedTubeCount2.TabIndex = 10
        ConditionValidationRule19.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule19.ErrorText = "Diametrical Growth Tube Count #2 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAgedTubeCount2, ConditionValidationRule19)
        '
        'uiAgedTubeCount1
        '
        Me.uiAgedTubeCount1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGTubeCount1", true))
        Me.uiAgedTubeCount1.Enabled = false
        Me.uiAgedTubeCount1.Location = New System.Drawing.Point(86, 126)
        Me.uiAgedTubeCount1.Name = "uiAgedTubeCount1"
        Me.uiAgedTubeCount1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount1.Properties.Appearance.Options.UseFont = true
        Me.uiAgedTubeCount1.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAgedTubeCount1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAgedTubeCount1.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount1.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAgedTubeCount1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount1.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAgedTubeCount1.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount1.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAgedTubeCount1.Properties.EditFormat.FormatString = "#"
        Me.uiAgedTubeCount1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiAgedTubeCount1.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiAgedTubeCount1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiAgedTubeCount1.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAgedTubeCount1, true)
        Me.uiAgedTubeCount1.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAgedTubeCount1, OptionsSpelling63)
        Me.uiAgedTubeCount1.TabIndex = 7
        ConditionValidationRule20.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule20.ErrorText = "Diametrical Growth Tube Count #1 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAgedTubeCount1, ConditionValidationRule20)
        '
        'uiAgedTubeCount4
        '
        Me.uiAgedTubeCount4.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGTubeCount4", true))
        Me.uiAgedTubeCount4.Enabled = false
        Me.uiAgedTubeCount4.Location = New System.Drawing.Point(86, 213)
        Me.uiAgedTubeCount4.Name = "uiAgedTubeCount4"
        Me.uiAgedTubeCount4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount4.Properties.Appearance.Options.UseFont = true
        Me.uiAgedTubeCount4.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAgedTubeCount4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAgedTubeCount4.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount4.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAgedTubeCount4.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount4.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAgedTubeCount4.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount4.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAgedTubeCount4.Properties.EditFormat.FormatString = "#"
        Me.uiAgedTubeCount4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiAgedTubeCount4.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiAgedTubeCount4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiAgedTubeCount4.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAgedTubeCount4, true)
        Me.uiAgedTubeCount4.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAgedTubeCount4, OptionsSpelling64)
        Me.uiAgedTubeCount4.TabIndex = 16
        ConditionValidationRule21.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule21.ErrorText = "Diametrical Growth Tube Count #4 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAgedTubeCount4, ConditionValidationRule21)
        '
        'uiNewTubeCount4
        '
        Me.uiNewTubeCount4.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "TolTubeCount4", true))
        Me.uiNewTubeCount4.Location = New System.Drawing.Point(89, 124)
        Me.uiNewTubeCount4.Name = "uiNewTubeCount4"
        Me.uiNewTubeCount4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount4.Properties.Appearance.Options.UseFont = true
        Me.uiNewTubeCount4.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNewTubeCount4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNewTubeCount4.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount4.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNewTubeCount4.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount4.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNewTubeCount4.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount4.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNewTubeCount4.Properties.EditFormat.FormatString = "#"
        Me.uiNewTubeCount4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNewTubeCount4.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNewTubeCount4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNewTubeCount4.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNewTubeCount4, true)
        Me.uiNewTubeCount4.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNewTubeCount4, OptionsSpelling65)
        Me.uiNewTubeCount4.TabIndex = 11
        ConditionValidationRule22.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule22.ErrorText = "Tube Tolerance Tube Count #4 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNewTubeCount4, ConditionValidationRule22)
        '
        'bsInspections_NewSummary
        '
        Me.bsInspections_NewSummary.DataMember = "tblInspections_tblReformerNewSummary"
        Me.bsInspections_NewSummary.DataSource = Me.bsInspections
        '
        'uiNewTubeCount3
        '
        Me.uiNewTubeCount3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "TolTubeCount3", true))
        Me.uiNewTubeCount3.Location = New System.Drawing.Point(89, 95)
        Me.uiNewTubeCount3.Name = "uiNewTubeCount3"
        Me.uiNewTubeCount3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount3.Properties.Appearance.Options.UseFont = true
        Me.uiNewTubeCount3.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNewTubeCount3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNewTubeCount3.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount3.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNewTubeCount3.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount3.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNewTubeCount3.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount3.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNewTubeCount3.Properties.EditFormat.FormatString = "#"
        Me.uiNewTubeCount3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNewTubeCount3.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNewTubeCount3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNewTubeCount3.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNewTubeCount3, true)
        Me.uiNewTubeCount3.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNewTubeCount3, OptionsSpelling66)
        Me.uiNewTubeCount3.TabIndex = 8
        ConditionValidationRule23.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule23.ErrorText = "Tube Tolerance Tube Count #3 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNewTubeCount3, ConditionValidationRule23)
        '
        'uiNewTubeCount2
        '
        Me.uiNewTubeCount2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "TolTubeCount2", true))
        Me.uiNewTubeCount2.Location = New System.Drawing.Point(89, 66)
        Me.uiNewTubeCount2.Name = "uiNewTubeCount2"
        Me.uiNewTubeCount2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount2.Properties.Appearance.Options.UseFont = true
        Me.uiNewTubeCount2.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNewTubeCount2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNewTubeCount2.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount2.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNewTubeCount2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount2.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNewTubeCount2.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount2.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNewTubeCount2.Properties.EditFormat.FormatString = "#"
        Me.uiNewTubeCount2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNewTubeCount2.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNewTubeCount2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNewTubeCount2.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNewTubeCount2, true)
        Me.uiNewTubeCount2.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNewTubeCount2, OptionsSpelling67)
        Me.uiNewTubeCount2.TabIndex = 5
        ConditionValidationRule24.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule24.ErrorText = "Tube Tolerance Tube Count #2 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNewTubeCount2, ConditionValidationRule24)
        '
        'uiNewTubeCount1
        '
        Me.uiNewTubeCount1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "TolTubeCount1", true))
        Me.uiNewTubeCount1.Location = New System.Drawing.Point(89, 37)
        Me.uiNewTubeCount1.Name = "uiNewTubeCount1"
        Me.uiNewTubeCount1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount1.Properties.Appearance.Options.UseFont = true
        Me.uiNewTubeCount1.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNewTubeCount1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNewTubeCount1.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount1.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNewTubeCount1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount1.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNewTubeCount1.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNewTubeCount1.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNewTubeCount1.Properties.EditFormat.FormatString = "#"
        Me.uiNewTubeCount1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNewTubeCount1.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNewTubeCount1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNewTubeCount1.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNewTubeCount1, true)
        Me.uiNewTubeCount1.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNewTubeCount1, OptionsSpelling68)
        Me.uiNewTubeCount1.TabIndex = 2
        ConditionValidationRule25.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule25.ErrorText = "Tube Tolerance Tube Count #1 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNewTubeCount1, ConditionValidationRule25)
        '
        'uiInspectionSummaryEditor
        '
        Me.uiInspectionSummaryEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiInspectionSummaryEditor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiInspectionSummaryEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiInspectionSummaryEditor.Appearance.Text.Options.UseFont = true
        Me.uiInspectionSummaryEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "InspectionSummary", true))
        Me.uiInspectionSummaryEditor.Location = New System.Drawing.Point(676, 7)
        Me.uiInspectionSummaryEditor.Name = "uiInspectionSummaryEditor"
        Me.uiInspectionSummaryEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiInspectionSummaryEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiInspectionSummaryEditor, false)
        Me.uiInspectionSummaryEditor.Size = New System.Drawing.Size(479, 561)
        Me.uiInspectionSummaryEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiInspectionSummaryEditor, OptionsSpelling69)
        Me.uiInspectionSummaryEditor.TabIndex = 25
        Me.uiInspectionSummaryEditor.Tag = "Inspection Summary"
        Me.uiInspectionSummaryEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiWorstFlawTubeNumber
        '
        Me.uiWorstFlawTubeNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "TubeID", true))
        Me.uiWorstFlawTubeNumber.Location = New System.Drawing.Point(187, 237)
        Me.uiWorstFlawTubeNumber.Name = "uiWorstFlawTubeNumber"
        Me.uiWorstFlawTubeNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawTubeNumber.Properties.Appearance.Options.UseFont = true
        Me.uiWorstFlawTubeNumber.Properties.Appearance.Options.UseTextOptions = true
        Me.uiWorstFlawTubeNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiWorstFlawTubeNumber.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawTubeNumber.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiWorstFlawTubeNumber.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawTubeNumber.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiWorstFlawTubeNumber.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawTubeNumber.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiWorstFlawTubeNumber.Properties.EditFormat.FormatString = "#"
        Me.uiWorstFlawTubeNumber.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiWorstFlawTubeNumber.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiWorstFlawTubeNumber, true)
        Me.uiWorstFlawTubeNumber.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiWorstFlawTubeNumber, OptionsSpelling70)
        Me.uiWorstFlawTubeNumber.TabIndex = 18
        ConditionValidationRule26.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule26.ErrorText = "Worst Flaw Tube ID cannot be blank"
        Me.DxValidationProvider.SetValidationRule(Me.uiWorstFlawTubeNumber, ConditionValidationRule26)
        '
        'uiNumberTubesWithFlaws
        '
        Me.uiNumberTubesWithFlaws.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "Flaws", true))
        Me.uiNumberTubesWithFlaws.Location = New System.Drawing.Point(187, 186)
        Me.uiNumberTubesWithFlaws.Name = "uiNumberTubesWithFlaws"
        Me.uiNumberTubesWithFlaws.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesWithFlaws.Properties.Appearance.Options.UseFont = true
        Me.uiNumberTubesWithFlaws.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNumberTubesWithFlaws.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberTubesWithFlaws.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesWithFlaws.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberTubesWithFlaws.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesWithFlaws.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNumberTubesWithFlaws.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesWithFlaws.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNumberTubesWithFlaws.Properties.EditFormat.FormatString = "#"
        Me.uiNumberTubesWithFlaws.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberTubesWithFlaws.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiNumberTubesWithFlaws.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiNumberTubesWithFlaws.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNumberTubesWithFlaws, true)
        Me.uiNumberTubesWithFlaws.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNumberTubesWithFlaws, OptionsSpelling71)
        Me.uiNumberTubesWithFlaws.TabIndex = 15
        ConditionValidationRule27.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule27.ErrorText = "Number of Tubes with Flaws cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNumberTubesWithFlaws, ConditionValidationRule27)
        '
        'uiWorstFlawRowNumber
        '
        Me.uiWorstFlawRowNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "RowID", true))
        Me.uiWorstFlawRowNumber.Location = New System.Drawing.Point(316, 237)
        Me.uiWorstFlawRowNumber.Name = "uiWorstFlawRowNumber"
        Me.uiWorstFlawRowNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawRowNumber.Properties.Appearance.Options.UseFont = true
        Me.uiWorstFlawRowNumber.Properties.Appearance.Options.UseTextOptions = true
        Me.uiWorstFlawRowNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiWorstFlawRowNumber.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawRowNumber.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiWorstFlawRowNumber.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawRowNumber.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiWorstFlawRowNumber.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiWorstFlawRowNumber.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiWorstFlawRowNumber.Properties.EditFormat.FormatString = "#"
        Me.uiWorstFlawRowNumber.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiWorstFlawRowNumber.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiWorstFlawRowNumber, true)
        Me.uiWorstFlawRowNumber.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiWorstFlawRowNumber, OptionsSpelling72)
        Me.uiWorstFlawRowNumber.TabIndex = 20
        ConditionValidationRule28.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule28.ErrorText = "Worst Flaw Row ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiWorstFlawRowNumber, ConditionValidationRule28)
        '
        'uiTubeServiceDate
        '
        Me.uiTubeServiceDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "DatePutIntoService", true))
        Me.uiTubeServiceDate.Location = New System.Drawing.Point(743, 30)
        Me.uiTubeServiceDate.Name = "uiTubeServiceDate"
        Me.uiTubeServiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeServiceDate.Properties.Appearance.Options.UseFont = true
        Me.uiTubeServiceDate.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeServiceDate.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeServiceDate.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeServiceDate.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeServiceDate.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeServiceDate.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeServiceDate, true)
        Me.uiTubeServiceDate.Size = New System.Drawing.Size(143, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeServiceDate, OptionsSpelling73)
        Me.uiTubeServiceDate.TabIndex = 15
        ConditionValidationRule29.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule29.ErrorText = "Tube Installation Date cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeServiceDate, ConditionValidationRule29)
        '
        'uiTubeNextInspectionDate
        '
        Me.uiTubeNextInspectionDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "NextTubeInspectionDate", true))
        Me.uiTubeNextInspectionDate.Location = New System.Drawing.Point(743, 89)
        Me.uiTubeNextInspectionDate.Name = "uiTubeNextInspectionDate"
        Me.uiTubeNextInspectionDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeNextInspectionDate.Properties.Appearance.Options.UseFont = true
        Me.uiTubeNextInspectionDate.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeNextInspectionDate.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeNextInspectionDate.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeNextInspectionDate.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeNextInspectionDate.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeNextInspectionDate.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeNextInspectionDate.Properties.MaxLength = 128
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeNextInspectionDate, true)
        Me.uiTubeNextInspectionDate.Size = New System.Drawing.Size(143, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeNextInspectionDate, OptionsSpelling74)
        Me.uiTubeNextInspectionDate.TabIndex = 20
        ConditionValidationRule30.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule30.ErrorText = "Next Tube Inspection Date cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeNextInspectionDate, ConditionValidationRule30)
        '
        'uiAgedTubeCount5
        '
        Me.uiAgedTubeCount5.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGTubeCount5", true))
        Me.uiAgedTubeCount5.Enabled = false
        Me.uiAgedTubeCount5.Location = New System.Drawing.Point(86, 242)
        Me.uiAgedTubeCount5.Name = "uiAgedTubeCount5"
        Me.uiAgedTubeCount5.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount5.Properties.Appearance.Options.UseFont = true
        Me.uiAgedTubeCount5.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAgedTubeCount5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAgedTubeCount5.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount5.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAgedTubeCount5.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount5.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAgedTubeCount5.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAgedTubeCount5.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAgedTubeCount5.Properties.EditFormat.FormatString = "#"
        Me.uiAgedTubeCount5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiAgedTubeCount5.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiAgedTubeCount5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiAgedTubeCount5.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiAgedTubeCount5, true)
        Me.uiAgedTubeCount5.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiAgedTubeCount5, OptionsSpelling75)
        Me.uiAgedTubeCount5.TabIndex = 19
        ConditionValidationRule31.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule31.ErrorText = "Diametrical Growth Tube Count #5 cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiAgedTubeCount5, ConditionValidationRule31)
        '
        'uiDiaGrowthMaxPercent
        '
        Me.uiDiaGrowthMaxPercent.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGrowthMaxPercent", true))
        Me.uiDiaGrowthMaxPercent.Location = New System.Drawing.Point(139, 311)
        Me.uiDiaGrowthMaxPercent.Name = "uiDiaGrowthMaxPercent"
        Me.uiDiaGrowthMaxPercent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthMaxPercent.Properties.Appearance.Options.UseFont = true
        Me.uiDiaGrowthMaxPercent.Properties.Appearance.Options.UseTextOptions = true
        Me.uiDiaGrowthMaxPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthMaxPercent.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiDiaGrowthMaxPercent.Properties.EditFormat.FormatString = "#"
        Me.uiDiaGrowthMaxPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiDiaGrowthMaxPercent.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiDiaGrowthMaxPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiDiaGrowthMaxPercent.Properties.Mask.ShowPlaceHolders = false
        Me.uiDiaGrowthMaxPercent.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiDiaGrowthMaxPercent, true)
        Me.uiDiaGrowthMaxPercent.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiDiaGrowthMaxPercent, OptionsSpelling76)
        Me.uiDiaGrowthMaxPercent.TabIndex = 31
        ConditionValidationRule32.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule32.ErrorText = "Maximum % of Growth cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiDiaGrowthMaxPercent, ConditionValidationRule32)
        '
        'uiDiaGrowthTubeID
        '
        Me.uiDiaGrowthTubeID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGrowthTubeID", true))
        Me.uiDiaGrowthTubeID.Location = New System.Drawing.Point(74, 311)
        Me.uiDiaGrowthTubeID.Name = "uiDiaGrowthTubeID"
        Me.uiDiaGrowthTubeID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthTubeID.Properties.Appearance.Options.UseFont = true
        Me.uiDiaGrowthTubeID.Properties.Appearance.Options.UseTextOptions = true
        Me.uiDiaGrowthTubeID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiDiaGrowthTubeID.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthTubeID.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiDiaGrowthTubeID.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthTubeID.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiDiaGrowthTubeID.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthTubeID.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiDiaGrowthTubeID.Properties.EditFormat.FormatString = "#"
        Me.uiDiaGrowthTubeID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiDiaGrowthTubeID.Properties.Mask.ShowPlaceHolders = false
        Me.uiDiaGrowthTubeID.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiDiaGrowthTubeID, true)
        Me.uiDiaGrowthTubeID.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiDiaGrowthTubeID, OptionsSpelling77)
        Me.uiDiaGrowthTubeID.TabIndex = 29
        ConditionValidationRule33.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule33.ErrorText = "Diametrical Growth Worst Tube ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiDiaGrowthTubeID, ConditionValidationRule33)
        '
        'uiDiaGrowthRowID
        '
        Me.uiDiaGrowthRowID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "DiaGrowthRowID", true))
        Me.uiDiaGrowthRowID.Location = New System.Drawing.Point(9, 311)
        Me.uiDiaGrowthRowID.Name = "uiDiaGrowthRowID"
        Me.uiDiaGrowthRowID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthRowID.Properties.Appearance.Options.UseFont = true
        Me.uiDiaGrowthRowID.Properties.Appearance.Options.UseTextOptions = true
        Me.uiDiaGrowthRowID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiDiaGrowthRowID.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthRowID.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiDiaGrowthRowID.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthRowID.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiDiaGrowthRowID.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthRowID.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiDiaGrowthRowID.Properties.EditFormat.FormatString = "#"
        Me.uiDiaGrowthRowID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiDiaGrowthRowID.Properties.Mask.ShowPlaceHolders = false
        Me.uiDiaGrowthRowID.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiDiaGrowthRowID, true)
        Me.uiDiaGrowthRowID.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiDiaGrowthRowID, OptionsSpelling78)
        Me.uiDiaGrowthRowID.TabIndex = 27
        ConditionValidationRule34.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule34.ErrorText = "Diametrical Growth Worst Tube Row ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiDiaGrowthRowID, ConditionValidationRule34)
        '
        'uiOperatingPeriod
        '
        Me.uiOperatingPeriod.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "OperatingPeriod", true))
        Me.uiOperatingPeriod.Location = New System.Drawing.Point(463, 61)
        Me.uiOperatingPeriod.Name = "uiOperatingPeriod"
        Me.uiOperatingPeriod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingPeriod.Properties.Appearance.Options.UseFont = true
        Me.uiOperatingPeriod.Properties.Appearance.Options.UseTextOptions = true
        Me.uiOperatingPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiOperatingPeriod.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingPeriod.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOperatingPeriod.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingPeriod.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiOperatingPeriod.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingPeriod.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiOperatingPeriod.Properties.EditFormat.FormatString = "#"
        Me.uiOperatingPeriod.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiOperatingPeriod.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiOperatingPeriod.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiOperatingPeriod.Properties.Mask.ShowPlaceHolders = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiOperatingPeriod, true)
        Me.uiOperatingPeriod.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiOperatingPeriod, OptionsSpelling79)
        Me.uiOperatingPeriod.TabIndex = 8
        ConditionValidationRule35.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule35.ErrorText = "Operating Period cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiOperatingPeriod, ConditionValidationRule35)
        '
        'uiNumberOfTubes
        '
        Me.uiNumberOfTubes.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "NumberOfTubes", true))
        Me.uiNumberOfTubes.Location = New System.Drawing.Point(629, 368)
        Me.uiNumberOfTubes.Name = "uiNumberOfTubes"
        Me.uiNumberOfTubes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfTubes.Properties.Appearance.Options.UseFont = true
        Me.uiNumberOfTubes.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNumberOfTubes.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfTubes.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfTubes.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberOfTubes.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfTubes.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNumberOfTubes.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfTubes.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNumberOfTubes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberOfTubes.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNumberOfTubes, true)
        Me.uiNumberOfTubes.Size = New System.Drawing.Size(62, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNumberOfTubes, OptionsSpelling80)
        Me.uiNumberOfTubes.TabIndex = 41
        ConditionValidationRule36.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Greater
        ConditionValidationRule36.ErrorText = "Number of tubes cannot be zero."
        ConditionValidationRule36.Value1 = 0
        Me.DxValidationProvider.SetValidationRule(Me.uiNumberOfTubes, ConditionValidationRule36)
        '
        'uiTubeLengthDisplay
        '
        Me.uiTubeLengthDisplay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "LengthInspectedDisplay", true))
        Me.uiTubeLengthDisplay.Location = New System.Drawing.Point(303, 334)
        Me.uiTubeLengthDisplay.Name = "uiTubeLengthDisplay"
        Me.uiTubeLengthDisplay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLengthDisplay.Properties.Appearance.Options.UseFont = true
        Me.uiTubeLengthDisplay.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTubeLengthDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTubeLengthDisplay.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLengthDisplay.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeLengthDisplay.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLengthDisplay.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeLengthDisplay.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeLengthDisplay.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeLengthDisplay.Properties.ReadOnly = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeLengthDisplay, true)
        Me.uiTubeLengthDisplay.Size = New System.Drawing.Size(163, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeLengthDisplay, OptionsSpelling81)
        Me.uiTubeLengthDisplay.TabIndex = 51
        Me.uiTubeLengthDisplay.TabStop = false
        ConditionValidationRule37.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule37.ErrorText = "Overall Tube Design Length cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeLengthDisplay, ConditionValidationRule37)
        '
        'uiImageNameFormat
        '
        Me.uiImageNameFormat.EditValue = ""
        Me.uiImageNameFormat.Location = New System.Drawing.Point(359, 64)
        Me.uiImageNameFormat.Name = "uiImageNameFormat"
        Me.uiImageNameFormat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiImageNameFormat.Properties.Appearance.Options.UseFont = true
        Me.uiImageNameFormat.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiImageNameFormat.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiImageNameFormat.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiImageNameFormat.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiImageNameFormat.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiImageNameFormat.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiImageNameFormat, true)
        Me.uiImageNameFormat.Size = New System.Drawing.Size(489, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiImageNameFormat, OptionsSpelling82)
        ToolTipTitleItem1.Text = "AutoSelect Instructions for Image Numbering"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text")
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.uiImageNameFormat.SuperTip = SuperToolTip1
        Me.uiImageNameFormat.TabIndex = 93
        Me.uiImageNameFormat.ToolTip = "i.e., 1,2,3,6,8,9,10"
        Me.uiImageNameFormat.ToolTipController = Me.ToolTipController
        Me.uiImageNameFormat.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'ToolTipController
        '
        Me.ToolTipController.AutoPopDelay = 500000
        Me.ToolTipController.CloseOnClick = DevExpress.Utils.DefaultBoolean.[True]
        '
        'uiCracking
        '
        Me.uiCracking.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "CrackingCount", true))
        Me.uiCracking.Location = New System.Drawing.Point(463, 227)
        Me.uiCracking.Name = "uiCracking"
        Me.uiCracking.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCracking.Properties.Appearance.Options.UseFont = true
        Me.uiCracking.Properties.Appearance.Options.UseTextOptions = true
        Me.uiCracking.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiCracking.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCracking.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCracking.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCracking.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCracking.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCracking.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiCracking.Properties.EditFormat.FormatString = "#"
        Me.uiCracking.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiCracking.Properties.Mask.EditMask = "([0-9]*\.[0-9]+|[0-9]+)"
        Me.uiCracking.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.uiCracking.Properties.Mask.ShowPlaceHolders = false
        Me.uiCracking.Properties.MaxLength = 10
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiCracking, true)
        Me.uiCracking.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiCracking, OptionsSpelling83)
        Me.uiCracking.TabIndex = 24
        ConditionValidationRule38.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule38.ErrorText = "Cracking Count cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiCracking, ConditionValidationRule38)
        Me.uiCracking.Visible = false
        '
        'uiFFSEditor
        '
        Me.uiFFSEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiFFSEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiFFSEditor.Appearance.Text.Options.UseFont = true
        Me.uiFFSEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "FFSSummary", true))
        Me.uiFFSEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uiFFSEditor.Location = New System.Drawing.Point(0, 0)
        Me.uiFFSEditor.Name = "uiFFSEditor"
        Me.uiFFSEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiFFSEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiFFSEditor, false)
        Me.uiFFSEditor.Size = New System.Drawing.Size(600, 532)
        Me.uiFFSEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiFFSEditor, OptionsSpelling84)
        Me.uiFFSEditor.TabIndex = 37
        Me.uiFFSEditor.Tag = "Fitness-for-Service Summary"
        Me.uiFFSEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiDiaGrowthSummaryEditor
        '
        Me.uiDiaGrowthSummaryEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiDiaGrowthSummaryEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiDiaGrowthSummaryEditor.Appearance.Text.Options.UseFont = true
        Me.uiDiaGrowthSummaryEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "DiaGrowthSummary", true))
        Me.uiDiaGrowthSummaryEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uiDiaGrowthSummaryEditor.Location = New System.Drawing.Point(0, 0)
        Me.uiDiaGrowthSummaryEditor.Name = "uiDiaGrowthSummaryEditor"
        Me.uiDiaGrowthSummaryEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiDiaGrowthSummaryEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiDiaGrowthSummaryEditor, false)
        Me.uiDiaGrowthSummaryEditor.Size = New System.Drawing.Size(600, 532)
        Me.uiDiaGrowthSummaryEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiDiaGrowthSummaryEditor, OptionsSpelling85)
        Me.uiDiaGrowthSummaryEditor.TabIndex = 39
        Me.uiDiaGrowthSummaryEditor.Tag = "Diametrical Growth Summary"
        Me.uiDiaGrowthSummaryEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiExecSummaryDetailsEditor
        '
        Me.uiExecSummaryDetailsEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiExecSummaryDetailsEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiExecSummaryDetailsEditor.Appearance.Text.Options.UseFont = true
        Me.uiExecSummaryDetailsEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "DetailsSummary", true))
        Me.uiExecSummaryDetailsEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uiExecSummaryDetailsEditor.Location = New System.Drawing.Point(0, 0)
        Me.uiExecSummaryDetailsEditor.Name = "uiExecSummaryDetailsEditor"
        Me.uiExecSummaryDetailsEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiExecSummaryDetailsEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiExecSummaryDetailsEditor, false)
        Me.uiExecSummaryDetailsEditor.Size = New System.Drawing.Size(600, 532)
        Me.uiExecSummaryDetailsEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiExecSummaryDetailsEditor, OptionsSpelling86)
        Me.uiExecSummaryDetailsEditor.TabIndex = 40
        Me.uiExecSummaryDetailsEditor.Tag = "Executive Summary Details"
        Me.uiExecSummaryDetailsEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiTubeHarvestingEditor
        '
        Me.uiTubeHarvestingEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.uiTubeHarvestingEditor.Appearance.Text.Font = New System.Drawing.Font("Segoe UI", 9.5!)
        Me.uiTubeHarvestingEditor.Appearance.Text.Options.UseFont = true
        Me.uiTubeHarvestingEditor.DataBindings.Add(New System.Windows.Forms.Binding("RtfText", Me.bsInspections, "TubeHarvestingSummary", true))
        Me.uiTubeHarvestingEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.uiTubeHarvestingEditor.Location = New System.Drawing.Point(0, 0)
        Me.uiTubeHarvestingEditor.Name = "uiTubeHarvestingEditor"
        Me.uiTubeHarvestingEditor.Options.DocumentCapabilities.CharacterFormatting = DevExpress.XtraRichEdit.DocumentCapability.Enabled
        Me.uiTubeHarvestingEditor.Options.MailMerge.KeepLastParagraph = false
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiTubeHarvestingEditor, false)
        Me.uiTubeHarvestingEditor.Size = New System.Drawing.Size(600, 532)
        Me.uiTubeHarvestingEditor.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiTubeHarvestingEditor, OptionsSpelling87)
        Me.uiTubeHarvestingEditor.TabIndex = 38
        Me.uiTubeHarvestingEditor.Tag = "Tube Harvesting Results"
        Me.uiTubeHarvestingEditor.Views.SimpleView.Padding = New System.Windows.Forms.Padding(4, 4, 4, 0)
        '
        'uiNumberTubesInspected
        '
        Me.uiNumberTubesInspected.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "NumberTubesInspected", true))
        Me.uiNumberTubesInspected.Location = New System.Drawing.Point(207, 455)
        Me.uiNumberTubesInspected.Name = "uiNumberTubesInspected"
        Me.uiNumberTubesInspected.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesInspected.Properties.Appearance.Options.UseFont = true
        Me.uiNumberTubesInspected.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNumberTubesInspected.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberTubesInspected.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesInspected.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberTubesInspected.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesInspected.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNumberTubesInspected.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesInspected.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiNumberTubesInspected.Properties.EditFormat.FormatString = "#"
        Me.uiNumberTubesInspected.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.uiNumberTubesInspected, true)
        Me.uiNumberTubesInspected.Size = New System.Drawing.Size(61, 24)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.uiNumberTubesInspected, OptionsSpelling88)
        Me.uiNumberTubesInspected.TabIndex = 45
        ConditionValidationRule39.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule39.ErrorText = "Number of Tubes Inspected cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiNumberTubesInspected, ConditionValidationRule39)
        '
        'uiClearRedProjectDescriptionButton
        '
        Me.uiClearRedProjectDescriptionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiClearRedProjectDescriptionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiClearRedProjectDescriptionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiClearRedProjectDescriptionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiClearRedProjectDescriptionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiClearRedProjectDescriptionButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiClearRedProjectDescriptionButton.Appearance.Options.UseBackColor = true
        Me.uiClearRedProjectDescriptionButton.Appearance.Options.UseBorderColor = true
        Me.uiClearRedProjectDescriptionButton.Appearance.Options.UseFont = true
        Me.uiClearRedProjectDescriptionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiClearRedProjectDescriptionButton.Location = New System.Drawing.Point(812, 56)
        Me.uiClearRedProjectDescriptionButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiClearRedProjectDescriptionButton.Name = "uiClearRedProjectDescriptionButton"
        Me.uiClearRedProjectDescriptionButton.Size = New System.Drawing.Size(155, 32)
        Me.uiClearRedProjectDescriptionButton.TabIndex = 4
        Me.uiClearRedProjectDescriptionButton.Text = "Clear Red"
        '
        'uiInsertProjectDescriptionButton
        '
        Me.uiInsertProjectDescriptionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiInsertProjectDescriptionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiInsertProjectDescriptionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiInsertProjectDescriptionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiInsertProjectDescriptionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInsertProjectDescriptionButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiInsertProjectDescriptionButton.Appearance.Options.UseBackColor = true
        Me.uiInsertProjectDescriptionButton.Appearance.Options.UseBorderColor = true
        Me.uiInsertProjectDescriptionButton.Appearance.Options.UseFont = true
        Me.uiInsertProjectDescriptionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInsertProjectDescriptionButton.Location = New System.Drawing.Point(812, 20)
        Me.uiInsertProjectDescriptionButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiInsertProjectDescriptionButton.Name = "uiInsertProjectDescriptionButton"
        Me.uiInsertProjectDescriptionButton.Size = New System.Drawing.Size(155, 32)
        Me.uiInsertProjectDescriptionButton.TabIndex = 1
        Me.uiInsertProjectDescriptionButton.Text = "Insert Sample Text"
        '
        'Label93
        '
        Me.Label93.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label93.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label93.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label93.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label93.Location = New System.Drawing.Point(5, 5)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(800, 15)
        Me.Label93.TabIndex = 0
        Me.Label93.Text = "PROJECT DESCRIPTION"
        Me.Label93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiNumberingSystem
        '
        Me.uiNumberingSystem.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiNumberingSystem.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "NumberingSystem", true))
        Me.uiNumberingSystem.EditValue = -1
        Me.uiNumberingSystem.Location = New System.Drawing.Point(813, 49)
        Me.uiNumberingSystem.Name = "uiNumberingSystem"
        Me.uiNumberingSystem.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiNumberingSystem.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberingSystem.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiNumberingSystem.Properties.Appearance.Options.UseBackColor = true
        Me.uiNumberingSystem.Properties.Appearance.Options.UseFont = true
        Me.uiNumberingSystem.Properties.Appearance.Options.UseForeColor = true
        Me.uiNumberingSystem.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberingSystem.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiNumberingSystem.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberingSystem.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiNumberingSystem.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiNumberingSystem.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "QIG Numbering System"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Customer Numbering System")})
        Me.uiNumberingSystem.Size = New System.Drawing.Size(227, 50)
        Me.uiNumberingSystem.TabIndex = 1
        '
        'uiShowHideButton
        '
        Me.uiShowHideButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiShowHideButton.Image = CType(resources.GetObject("uiShowHideButton.Image"),System.Drawing.Image)
        Me.uiShowHideButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiShowHideButton.Location = New System.Drawing.Point(812, 7)
        Me.uiShowHideButton.Name = "uiShowHideButton"
        Me.uiShowHideButton.Size = New System.Drawing.Size(27, 37)
        Me.uiShowHideButton.TabIndex = 6
        '
        'uiClearRedTubeIdentificationButton
        '
        Me.uiClearRedTubeIdentificationButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiClearRedTubeIdentificationButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiClearRedTubeIdentificationButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiClearRedTubeIdentificationButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiClearRedTubeIdentificationButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiClearRedTubeIdentificationButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiClearRedTubeIdentificationButton.Appearance.Options.UseBackColor = true
        Me.uiClearRedTubeIdentificationButton.Appearance.Options.UseBorderColor = true
        Me.uiClearRedTubeIdentificationButton.Appearance.Options.UseFont = true
        Me.uiClearRedTubeIdentificationButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiClearRedTubeIdentificationButton.Location = New System.Drawing.Point(812, 141)
        Me.uiClearRedTubeIdentificationButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiClearRedTubeIdentificationButton.Name = "uiClearRedTubeIdentificationButton"
        Me.uiClearRedTubeIdentificationButton.Size = New System.Drawing.Size(155, 32)
        Me.uiClearRedTubeIdentificationButton.TabIndex = 5
        Me.uiClearRedTubeIdentificationButton.Text = "Clear Red"
        '
        'uiInsertTubeIdentificationButton
        '
        Me.uiInsertTubeIdentificationButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiInsertTubeIdentificationButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiInsertTubeIdentificationButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiInsertTubeIdentificationButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiInsertTubeIdentificationButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInsertTubeIdentificationButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiInsertTubeIdentificationButton.Appearance.Options.UseBackColor = true
        Me.uiInsertTubeIdentificationButton.Appearance.Options.UseBorderColor = true
        Me.uiInsertTubeIdentificationButton.Appearance.Options.UseFont = true
        Me.uiInsertTubeIdentificationButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInsertTubeIdentificationButton.Location = New System.Drawing.Point(812, 104)
        Me.uiInsertTubeIdentificationButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiInsertTubeIdentificationButton.Name = "uiInsertTubeIdentificationButton"
        Me.uiInsertTubeIdentificationButton.Size = New System.Drawing.Size(155, 32)
        Me.uiInsertTubeIdentificationButton.TabIndex = 2
        Me.uiInsertTubeIdentificationButton.Text = "Insert Sample Text"
        '
        'Label98
        '
        Me.Label98.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label98.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label98.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label98.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label98.Location = New System.Drawing.Point(5, 6)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(800, 15)
        Me.Label98.TabIndex = 0
        Me.Label98.Text = "REFORMER TUBE IDENTIFICATION"
        Me.Label98.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = false
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.uiAutoSelectMainButton)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.uiResetMainButton)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.uiRemoveMainButton)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Label134)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.uiReportItemsGridControl)
        Me.SplitContainerControl1.Panel1.MinSize = 125
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label125)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiImageNameFormat)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiImageNameFormatLabel)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiAutoSelectAppendixButton)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiAppendixItems)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiResetAppendixButton)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiAddNewAppendixButton)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiAddMissingAppendixButton)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiAppendixItemsGridControl)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.uiRemoveAppendixButton)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label135)
        Me.SplitContainerControl1.Panel2.MinSize = 135
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1162, 576)
        Me.SplitContainerControl1.SplitterPosition = 248
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'uiAutoSelectMainButton
        '
        Me.uiAutoSelectMainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAutoSelectMainButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAutoSelectMainButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAutoSelectMainButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAutoSelectMainButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSelectMainButton.Appearance.Options.UseBackColor = true
        Me.uiAutoSelectMainButton.Appearance.Options.UseBorderColor = true
        Me.uiAutoSelectMainButton.Appearance.Options.UseFont = true
        Me.uiAutoSelectMainButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAutoSelectMainButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiAutoSelectMainButton.Location = New System.Drawing.Point(803, 25)
        Me.uiAutoSelectMainButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiAutoSelectMainButton.Name = "uiAutoSelectMainButton"
        Me.uiAutoSelectMainButton.Size = New System.Drawing.Size(111, 24)
        Me.uiAutoSelectMainButton.TabIndex = 0
        Me.uiAutoSelectMainButton.Text = "AutoSelect All"
        '
        'uiResetMainButton
        '
        Me.uiResetMainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiResetMainButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiResetMainButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiResetMainButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiResetMainButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiResetMainButton.Appearance.Options.UseBackColor = true
        Me.uiResetMainButton.Appearance.Options.UseBorderColor = true
        Me.uiResetMainButton.Appearance.Options.UseFont = true
        Me.uiResetMainButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiResetMainButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiResetMainButton.Location = New System.Drawing.Point(1037, 25)
        Me.uiResetMainButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiResetMainButton.Name = "uiResetMainButton"
        Me.uiResetMainButton.Size = New System.Drawing.Size(111, 24)
        Me.uiResetMainButton.TabIndex = 2
        Me.uiResetMainButton.Text = "Reset Sorting"
        '
        'uiRemoveMainButton
        '
        Me.uiRemoveMainButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiRemoveMainButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiRemoveMainButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiRemoveMainButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiRemoveMainButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRemoveMainButton.Appearance.Options.UseBackColor = true
        Me.uiRemoveMainButton.Appearance.Options.UseBorderColor = true
        Me.uiRemoveMainButton.Appearance.Options.UseFont = true
        Me.uiRemoveMainButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiRemoveMainButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiRemoveMainButton.Location = New System.Drawing.Point(920, 25)
        Me.uiRemoveMainButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiRemoveMainButton.Name = "uiRemoveMainButton"
        Me.uiRemoveMainButton.Size = New System.Drawing.Size(111, 24)
        Me.uiRemoveMainButton.TabIndex = 1
        Me.uiRemoveMainButton.Text = "Remove All"
        '
        'Label134
        '
        Me.Label134.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label134.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label134.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label134.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label134.Location = New System.Drawing.Point(5, 5)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(1144, 15)
        Me.Label134.TabIndex = 79
        Me.Label134.Text = "MAIN REPORT IMAGES/LINKED FILES"
        Me.Label134.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiReportItemsGridControl
        '
        Me.uiReportItemsGridControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiReportItemsGridControl.DataSource = Me.bsInspections_ReportItems
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.Append.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.First.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.Last.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.Next.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.NextPage.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.Prev.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.Buttons.PrevPage.Visible = false
        Me.uiReportItemsGridControl.EmbeddedNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None
        Me.uiReportItemsGridControl.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        GridLevelNode1.RelationName = "vwReportItems_Details_tblReportImageItems"
        Me.uiReportItemsGridControl.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.uiReportItemsGridControl.Location = New System.Drawing.Point(5, 54)
        Me.uiReportItemsGridControl.LookAndFeel.SkinName = "Blue"
        Me.uiReportItemsGridControl.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiReportItemsGridControl.MainView = Me.uiReportItemsGridView
        Me.uiReportItemsGridControl.Name = "uiReportItemsGridControl"
        Me.uiReportItemsGridControl.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit2, Me.RepositoryItemButtonEditMain})
        Me.uiReportItemsGridControl.Size = New System.Drawing.Size(1143, 187)
        Me.uiReportItemsGridControl.TabIndex = 78
        Me.uiReportItemsGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiReportItemsGridView})
        '
        'uiReportItemsGridView
        '
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.ColumnFilterButton.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.Empty.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229,Byte),Integer), CType(CType(234,Byte),Integer), CType(CType(237,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.uiReportItemsGridView.Appearance.EvenRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.EvenRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.FilterCloseButton.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FilterPanel.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FilterPanel.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FixedLine.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.uiReportItemsGridView.Appearance.FocusedCell.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FocusedCell.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.FocusedRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FocusedRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.FooterPanel.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.FooterPanel.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.FooterPanel.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupButton.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.GroupButton.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.GroupButton.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupFooter.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.GroupFooter.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.GroupFooter.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.uiReportItemsGridView.Appearance.GroupPanel.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.GroupPanel.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.GroupRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.GroupRow.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.GroupRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HeaderPanel.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.HeaderPanel.Options.UseBorderColor = true
        Me.uiReportItemsGridView.Appearance.HeaderPanel.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128,Byte),Integer), CType(CType(153,Byte),Integer), CType(CType(195,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HideSelectionRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.HideSelectionRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.HorzLine.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.uiReportItemsGridView.Appearance.OddRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.OddRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251,Byte),Integer), CType(CType(250,Byte),Integer), CType(CType(248,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.uiReportItemsGridView.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.Preview.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.Preview.Options.UseFont = true
        Me.uiReportItemsGridView.Appearance.Preview.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.uiReportItemsGridView.Appearance.Row.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.Row.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.RowSeparator.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107,Byte),Integer), CType(CType(133,Byte),Integer), CType(CType(179,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.SelectedRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.SelectedRow.Options.UseForeColor = true
        Me.uiReportItemsGridView.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.uiReportItemsGridView.Appearance.TopNewRow.Options.UseBackColor = true
        Me.uiReportItemsGridView.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiReportItemsGridView.Appearance.VertLine.Options.UseBackColor = true
        Me.uiReportItemsGridView.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDescriptionMain, Me.colStatusMain, Me.colLinkedFileMain, Me.colSortOrderMain, Me.colFK_ReportItemType})
        Me.uiReportItemsGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.uiReportItemsGridView.GridControl = Me.uiReportItemsGridControl
        Me.uiReportItemsGridView.Name = "uiReportItemsGridView"
        Me.uiReportItemsGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiReportItemsGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiReportItemsGridView.OptionsBehavior.AutoSelectAllInEditor = false
        Me.uiReportItemsGridView.OptionsBehavior.AutoUpdateTotalSummary = false
        Me.uiReportItemsGridView.OptionsCustomization.AllowColumnMoving = false
        Me.uiReportItemsGridView.OptionsCustomization.AllowFilter = false
        Me.uiReportItemsGridView.OptionsCustomization.AllowGroup = false
        Me.uiReportItemsGridView.OptionsCustomization.AllowQuickHideColumns = false
        Me.uiReportItemsGridView.OptionsDetail.AllowZoomDetail = false
        Me.uiReportItemsGridView.OptionsDetail.EnableMasterViewMode = false
        Me.uiReportItemsGridView.OptionsDetail.ShowDetailTabs = false
        Me.uiReportItemsGridView.OptionsDetail.SmartDetailExpand = false
        Me.uiReportItemsGridView.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.uiReportItemsGridView.OptionsView.EnableAppearanceEvenRow = true
        Me.uiReportItemsGridView.OptionsView.EnableAppearanceOddRow = true
        Me.uiReportItemsGridView.OptionsView.ShowGroupPanel = false
        Me.uiReportItemsGridView.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSortOrderMain, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDescriptionMain
        '
        Me.colDescriptionMain.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colDescriptionMain.AppearanceCell.Options.UseFont = true
        Me.colDescriptionMain.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colDescriptionMain.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colDescriptionMain.AppearanceHeader.Options.UseFont = true
        Me.colDescriptionMain.AppearanceHeader.Options.UseForeColor = true
        Me.colDescriptionMain.FieldName = "Description"
        Me.colDescriptionMain.Name = "colDescriptionMain"
        Me.colDescriptionMain.OptionsColumn.ReadOnly = true
        Me.colDescriptionMain.Visible = true
        Me.colDescriptionMain.VisibleIndex = 0
        Me.colDescriptionMain.Width = 313
        '
        'colStatusMain
        '
        Me.colStatusMain.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colStatusMain.AppearanceCell.Options.UseFont = true
        Me.colStatusMain.AppearanceCell.Options.UseTextOptions = true
        Me.colStatusMain.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colStatusMain.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colStatusMain.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colStatusMain.AppearanceHeader.Options.UseFont = true
        Me.colStatusMain.AppearanceHeader.Options.UseForeColor = true
        Me.colStatusMain.AppearanceHeader.Options.UseTextOptions = true
        Me.colStatusMain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colStatusMain.Caption = "Status"
        Me.colStatusMain.FieldName = "StatusMain"
        Me.colStatusMain.MinWidth = 100
        Me.colStatusMain.Name = "colStatusMain"
        Me.colStatusMain.OptionsColumn.ReadOnly = true
        Me.colStatusMain.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.colStatusMain.Visible = true
        Me.colStatusMain.VisibleIndex = 1
        Me.colStatusMain.Width = 100
        '
        'colLinkedFileMain
        '
        Me.colLinkedFileMain.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLinkedFileMain.AppearanceCell.Options.UseFont = true
        Me.colLinkedFileMain.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLinkedFileMain.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colLinkedFileMain.AppearanceHeader.Options.UseFont = true
        Me.colLinkedFileMain.AppearanceHeader.Options.UseForeColor = true
        Me.colLinkedFileMain.Caption = "Linked File"
        Me.colLinkedFileMain.ColumnEdit = Me.RepositoryItemButtonEditMain
        Me.colLinkedFileMain.FieldName = "LinkedFile"
        Me.colLinkedFileMain.Name = "colLinkedFileMain"
        Me.colLinkedFileMain.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
        Me.colLinkedFileMain.Visible = true
        Me.colLinkedFileMain.VisibleIndex = 2
        Me.colLinkedFileMain.Width = 500
        '
        'RepositoryItemButtonEditMain
        '
        Me.RepositoryItemButtonEditMain.AutoHeight = false
        Me.RepositoryItemButtonEditMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        SerializableAppearanceObject2.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject2.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject2.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject2.Options.UseBackColor = true
        SerializableAppearanceObject2.Options.UseBorderColor = true
        SerializableAppearanceObject2.Options.UseFont = true
        SerializableAppearanceObject3.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject3.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject3.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject3.Options.UseBackColor = true
        SerializableAppearanceObject3.Options.UseBorderColor = true
        SerializableAppearanceObject3.Options.UseFont = true
        SerializableAppearanceObject4.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject4.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject4.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject4.Options.UseBackColor = true
        SerializableAppearanceObject4.Options.UseBorderColor = true
        SerializableAppearanceObject4.Options.UseFont = true
        Me.RepositoryItemButtonEditMain.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Browse...", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "Browse", "BROWSE", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "AutoSelect", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject3, "Attempt to auto-select the correct file based on the criteria established by your"& _ 
                    " project.", "AUTO", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "None", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject4, "No file of this type should be included in the report.", "NONE", Nothing, true)})
        Me.RepositoryItemButtonEditMain.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.RepositoryItemButtonEditMain.Name = "RepositoryItemButtonEditMain"
        '
        'colSortOrderMain
        '
        Me.colSortOrderMain.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colSortOrderMain.AppearanceCell.Options.UseFont = true
        Me.colSortOrderMain.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colSortOrderMain.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colSortOrderMain.AppearanceHeader.Options.UseFont = true
        Me.colSortOrderMain.AppearanceHeader.Options.UseForeColor = true
        Me.colSortOrderMain.FieldName = "SortOrder"
        Me.colSortOrderMain.Name = "colSortOrderMain"
        Me.colSortOrderMain.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        '
        'colFK_ReportItemType
        '
        Me.colFK_ReportItemType.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFK_ReportItemType.AppearanceCell.Options.UseFont = true
        Me.colFK_ReportItemType.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFK_ReportItemType.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colFK_ReportItemType.AppearanceHeader.Options.UseFont = true
        Me.colFK_ReportItemType.AppearanceHeader.Options.UseForeColor = true
        Me.colFK_ReportItemType.FieldName = "FK_ReportItemType"
        Me.colFK_ReportItemType.Name = "colFK_ReportItemType"
        '
        'RepositoryItemLookUpEdit2
        '
        Me.RepositoryItemLookUpEdit2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RepositoryItemLookUpEdit2.Appearance.Options.UseFont = true
        Me.RepositoryItemLookUpEdit2.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RepositoryItemLookUpEdit2.AppearanceDropDown.Options.UseFont = true
        Me.RepositoryItemLookUpEdit2.AutoHeight = false
        Me.RepositoryItemLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit2.DataSource = bsPipeTubeItemTypes_LU
        Me.RepositoryItemLookUpEdit2.DisplayMember = "Description_Default"
        Me.RepositoryItemLookUpEdit2.Name = "RepositoryItemLookUpEdit2"
        Me.RepositoryItemLookUpEdit2.ValueMember = "ID"
        '
        'Label125
        '
        Me.Label125.AutoSize = true
        Me.Label125.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label125.ForeColor = System.Drawing.Color.Teal
        Me.Label125.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label125.Location = New System.Drawing.Point(854, 67)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(303, 17)
        Me.Label125.TabIndex = 95
        Me.Label125.Text = "Hover over the textbox for numbering instructions."
        Me.Label125.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'uiImageNameFormatLabel
        '
        Me.uiImageNameFormatLabel.AutoSize = true
        Me.uiImageNameFormatLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiImageNameFormatLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiImageNameFormatLabel.Location = New System.Drawing.Point(5, 63)
        Me.uiImageNameFormatLabel.Name = "uiImageNameFormatLabel"
        Me.uiImageNameFormatLabel.Size = New System.Drawing.Size(352, 17)
        Me.uiImageNameFormatLabel.TabIndex = 94
        Me.uiImageNameFormatLabel.Text = "Image Numbers for AutoSelect All (separate with commas):"
        Me.uiImageNameFormatLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiAutoSelectAppendixButton
        '
        Me.uiAutoSelectAppendixButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAutoSelectAppendixButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAutoSelectAppendixButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAutoSelectAppendixButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAutoSelectAppendixButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSelectAppendixButton.Appearance.Options.UseBackColor = true
        Me.uiAutoSelectAppendixButton.Appearance.Options.UseBorderColor = true
        Me.uiAutoSelectAppendixButton.Appearance.Options.UseFont = true
        Me.uiAutoSelectAppendixButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAutoSelectAppendixButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiAutoSelectAppendixButton.Location = New System.Drawing.Point(569, 29)
        Me.uiAutoSelectAppendixButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiAutoSelectAppendixButton.Name = "uiAutoSelectAppendixButton"
        Me.uiAutoSelectAppendixButton.Size = New System.Drawing.Size(111, 24)
        Me.uiAutoSelectAppendixButton.TabIndex = 1
        Me.uiAutoSelectAppendixButton.Text = "AutoSelect All"
        '
        'uiAppendixItems
        '
        Me.uiAppendixItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAppendixItems.Location = New System.Drawing.Point(6, 29)
        Me.uiAppendixItems.MaximumSize = New System.Drawing.Size(400, 24)
        Me.uiAppendixItems.Name = "uiAppendixItems"
        Me.uiAppendixItems.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.Appearance.Options.UseFont = true
        Me.uiAppendixItems.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAppendixItems.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiAppendixItems.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.AppearanceDropDownHeader.Options.UseFont = true
        Me.uiAppendixItems.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAppendixItems.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItems.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiAppendixItems.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup
        SerializableAppearanceObject5.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject5.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject5.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject5.Options.UseBackColor = true
        SerializableAppearanceObject5.Options.UseBorderColor = true
        SerializableAppearanceObject6.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject6.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject6.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject6.Options.UseBackColor = true
        SerializableAppearanceObject6.Options.UseBorderColor = true
        SerializableAppearanceObject7.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject7.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject7.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject7.Options.UseBackColor = true
        SerializableAppearanceObject7.Options.UseBorderColor = true
        Me.uiAppendixItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, "", "Down", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinLeft, "Previous Appendix", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject6, "", "PREV", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight, "Next Appendix", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject7, "", "NEXT", Nothing, true)})
        Me.uiAppendixItems.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAppendixItems.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ListingDesc", "Listing Desc", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 22, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far)})
        Me.uiAppendixItems.Properties.DataSource = Me.bsInspections_Appendixes
        Me.uiAppendixItems.Properties.DisplayMember = "ListingDesc"
        Me.uiAppendixItems.Properties.DropDownRows = 9
        Me.uiAppendixItems.Properties.NullText = ""
        Me.uiAppendixItems.Properties.ShowFooter = false
        Me.uiAppendixItems.Properties.ShowHeader = false
        Me.uiAppendixItems.Properties.ValueMember = "ID"
        Me.uiAppendixItems.Size = New System.Drawing.Size(324, 24)
        Me.uiAppendixItems.TabIndex = 0
        '
        'uiResetAppendixButton
        '
        Me.uiResetAppendixButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiResetAppendixButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiResetAppendixButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiResetAppendixButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiResetAppendixButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiResetAppendixButton.Appearance.Options.UseBackColor = true
        Me.uiResetAppendixButton.Appearance.Options.UseBorderColor = true
        Me.uiResetAppendixButton.Appearance.Options.UseFont = true
        Me.uiResetAppendixButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiResetAppendixButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiResetAppendixButton.Location = New System.Drawing.Point(1037, 29)
        Me.uiResetAppendixButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiResetAppendixButton.Name = "uiResetAppendixButton"
        Me.uiResetAppendixButton.Size = New System.Drawing.Size(111, 24)
        Me.uiResetAppendixButton.TabIndex = 5
        Me.uiResetAppendixButton.Text = "Reset Sorting"
        '
        'uiAddNewAppendixButton
        '
        Me.uiAddNewAppendixButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAddNewAppendixButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAddNewAppendixButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAddNewAppendixButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAddNewAppendixButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddNewAppendixButton.Appearance.Options.UseBackColor = true
        Me.uiAddNewAppendixButton.Appearance.Options.UseBorderColor = true
        Me.uiAddNewAppendixButton.Appearance.Options.UseFont = true
        Me.uiAddNewAppendixButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAddNewAppendixButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiAddNewAppendixButton.Location = New System.Drawing.Point(803, 29)
        Me.uiAddNewAppendixButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiAddNewAppendixButton.Name = "uiAddNewAppendixButton"
        Me.uiAddNewAppendixButton.Size = New System.Drawing.Size(111, 24)
        Me.uiAddNewAppendixButton.TabIndex = 3
        Me.uiAddNewAppendixButton.Text = "Add New"
        '
        'uiAddMissingAppendixButton
        '
        Me.uiAddMissingAppendixButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAddMissingAppendixButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAddMissingAppendixButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAddMissingAppendixButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAddMissingAppendixButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddMissingAppendixButton.Appearance.Options.UseBackColor = true
        Me.uiAddMissingAppendixButton.Appearance.Options.UseBorderColor = true
        Me.uiAddMissingAppendixButton.Appearance.Options.UseFont = true
        Me.uiAddMissingAppendixButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAddMissingAppendixButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiAddMissingAppendixButton.Location = New System.Drawing.Point(920, 29)
        Me.uiAddMissingAppendixButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiAddMissingAppendixButton.Name = "uiAddMissingAppendixButton"
        Me.uiAddMissingAppendixButton.Size = New System.Drawing.Size(111, 24)
        Me.uiAddMissingAppendixButton.TabIndex = 4
        Me.uiAddMissingAppendixButton.Text = "Add Missing"
        '
        'uiAppendixItemsGridControl
        '
        Me.uiAppendixItemsGridControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAppendixItemsGridControl.DataSource = Me.bsInspections_AppendixItems
        Me.uiAppendixItemsGridControl.EmbeddedNavigator.Buttons.Append.Visible = false
        Me.uiAppendixItemsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false
        Me.uiAppendixItemsGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false
        Me.uiAppendixItemsGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false
        Me.uiAppendixItemsGridControl.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAppendixItemsGridControl.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAppendixItemsGridControl.Location = New System.Drawing.Point(5, 100)
        Me.uiAppendixItemsGridControl.LookAndFeel.SkinName = "Blue"
        Me.uiAppendixItemsGridControl.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiAppendixItemsGridControl.MainView = Me.uiAppendixItemsGridView
        Me.uiAppendixItemsGridControl.Name = "uiAppendixItemsGridControl"
        Me.uiAppendixItemsGridControl.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit3, Me.RepositoryItemButtonEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemSearchLookUpEdit1, Me.RepositoryItemButtonEditAppendix, Me.RepositoryItemLookUpEdit4, Me.RepositoryItemTextEdit2})
        Me.uiAppendixItemsGridControl.Size = New System.Drawing.Size(1144, 220)
        Me.uiAppendixItemsGridControl.TabIndex = 9
        Me.uiAppendixItemsGridControl.UseEmbeddedNavigator = true
        Me.uiAppendixItemsGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiAppendixItemsGridView})
        '
        'uiAppendixItemsGridView
        '
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButton.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.Empty.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229,Byte),Integer), CType(CType(234,Byte),Integer), CType(CType(237,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.uiAppendixItemsGridView.Appearance.EvenRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.EvenRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.FilterCloseButton.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FilterPanel.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FilterPanel.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FixedLine.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.uiAppendixItemsGridView.Appearance.FocusedCell.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FocusedCell.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.FocusedRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FocusedRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.FooterPanel.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupButton.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupButton.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupButton.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupFooter.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.uiAppendixItemsGridView.Appearance.GroupPanel.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupPanel.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.GroupRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupRow.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.GroupRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.Options.UseBorderColor = true
        Me.uiAppendixItemsGridView.Appearance.HeaderPanel.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128,Byte),Integer), CType(CType(153,Byte),Integer), CType(CType(195,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HideSelectionRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.HideSelectionRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.HorzLine.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.uiAppendixItemsGridView.Appearance.OddRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.OddRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251,Byte),Integer), CType(CType(250,Byte),Integer), CType(CType(248,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.uiAppendixItemsGridView.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.Preview.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.Preview.Options.UseFont = true
        Me.uiAppendixItemsGridView.Appearance.Preview.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.uiAppendixItemsGridView.Appearance.Row.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.Row.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.RowSeparator.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107,Byte),Integer), CType(CType(133,Byte),Integer), CType(CType(179,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.SelectedRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.SelectedRow.Options.UseForeColor = true
        Me.uiAppendixItemsGridView.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.uiAppendixItemsGridView.Appearance.TopNewRow.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiAppendixItemsGridView.Appearance.VertLine.Options.UseBackColor = true
        Me.uiAppendixItemsGridView.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDescriptionAppendix, Me.colStatusAppendix, Me.colLinkedFileAppendix, Me.colPipeTubeSortOrder, Me.colSortOrderAppendix, Me.colReportItemType})
        Me.uiAppendixItemsGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.uiAppendixItemsGridView.GridControl = Me.uiAppendixItemsGridControl
        Me.uiAppendixItemsGridView.Name = "uiAppendixItemsGridView"
        Me.uiAppendixItemsGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.uiAppendixItemsGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.uiAppendixItemsGridView.OptionsBehavior.AutoSelectAllInEditor = false
        Me.uiAppendixItemsGridView.OptionsBehavior.AutoUpdateTotalSummary = false
        Me.uiAppendixItemsGridView.OptionsCustomization.AllowColumnMoving = false
        Me.uiAppendixItemsGridView.OptionsCustomization.AllowFilter = false
        Me.uiAppendixItemsGridView.OptionsCustomization.AllowGroup = false
        Me.uiAppendixItemsGridView.OptionsCustomization.AllowQuickHideColumns = false
        Me.uiAppendixItemsGridView.OptionsDetail.AllowZoomDetail = false
        Me.uiAppendixItemsGridView.OptionsDetail.EnableMasterViewMode = false
        Me.uiAppendixItemsGridView.OptionsDetail.ShowDetailTabs = false
        Me.uiAppendixItemsGridView.OptionsDetail.SmartDetailExpand = false
        Me.uiAppendixItemsGridView.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.uiAppendixItemsGridView.OptionsView.EnableAppearanceEvenRow = true
        Me.uiAppendixItemsGridView.OptionsView.EnableAppearanceOddRow = true
        Me.uiAppendixItemsGridView.OptionsView.ShowGroupPanel = false
        Me.uiAppendixItemsGridView.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSortOrderAppendix, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPipeTubeSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDescriptionAppendix
        '
        Me.colDescriptionAppendix.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colDescriptionAppendix.AppearanceCell.Options.UseFont = true
        Me.colDescriptionAppendix.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colDescriptionAppendix.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colDescriptionAppendix.AppearanceHeader.Options.UseFont = true
        Me.colDescriptionAppendix.AppearanceHeader.Options.UseForeColor = true
        Me.colDescriptionAppendix.Caption = "Description"
        Me.colDescriptionAppendix.FieldName = "FullDescription"
        Me.colDescriptionAppendix.Name = "colDescriptionAppendix"
        Me.colDescriptionAppendix.OptionsColumn.ReadOnly = true
        Me.colDescriptionAppendix.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
        Me.colDescriptionAppendix.Visible = true
        Me.colDescriptionAppendix.VisibleIndex = 0
        Me.colDescriptionAppendix.Width = 250
        '
        'colStatusAppendix
        '
        Me.colStatusAppendix.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colStatusAppendix.AppearanceCell.Options.UseFont = true
        Me.colStatusAppendix.AppearanceCell.Options.UseTextOptions = true
        Me.colStatusAppendix.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colStatusAppendix.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colStatusAppendix.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colStatusAppendix.AppearanceHeader.Options.UseFont = true
        Me.colStatusAppendix.AppearanceHeader.Options.UseForeColor = true
        Me.colStatusAppendix.AppearanceHeader.Options.UseTextOptions = true
        Me.colStatusAppendix.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colStatusAppendix.Caption = "Status"
        Me.colStatusAppendix.FieldName = "StatusAppendix"
        Me.colStatusAppendix.MinWidth = 100
        Me.colStatusAppendix.Name = "colStatusAppendix"
        Me.colStatusAppendix.OptionsColumn.ReadOnly = true
        Me.colStatusAppendix.ShowUnboundExpressionMenu = true
        Me.colStatusAppendix.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.colStatusAppendix.Visible = true
        Me.colStatusAppendix.VisibleIndex = 1
        Me.colStatusAppendix.Width = 100
        '
        'colLinkedFileAppendix
        '
        Me.colLinkedFileAppendix.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLinkedFileAppendix.AppearanceCell.Options.UseFont = true
        Me.colLinkedFileAppendix.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colLinkedFileAppendix.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colLinkedFileAppendix.AppearanceHeader.Options.UseFont = true
        Me.colLinkedFileAppendix.AppearanceHeader.Options.UseForeColor = true
        Me.colLinkedFileAppendix.ColumnEdit = Me.RepositoryItemButtonEditAppendix
        Me.colLinkedFileAppendix.FieldName = "LinkedFile"
        Me.colLinkedFileAppendix.Name = "colLinkedFileAppendix"
        Me.colLinkedFileAppendix.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
        Me.colLinkedFileAppendix.Visible = true
        Me.colLinkedFileAppendix.VisibleIndex = 2
        Me.colLinkedFileAppendix.Width = 564
        '
        'RepositoryItemButtonEditAppendix
        '
        Me.RepositoryItemButtonEditAppendix.AutoHeight = false
        SerializableAppearanceObject8.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject8.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject8.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject8.Options.UseBackColor = true
        SerializableAppearanceObject8.Options.UseBorderColor = true
        SerializableAppearanceObject8.Options.UseFont = true
        SerializableAppearanceObject9.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject9.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject9.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject9.Options.UseBackColor = true
        SerializableAppearanceObject9.Options.UseBorderColor = true
        SerializableAppearanceObject9.Options.UseFont = true
        SerializableAppearanceObject10.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject10.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject10.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject10.Options.UseBackColor = true
        SerializableAppearanceObject10.Options.UseBorderColor = true
        SerializableAppearanceObject10.Options.UseFont = true
        Me.RepositoryItemButtonEditAppendix.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Browse...", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject8, "Browse", "BROWSE", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "AutoSelect", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject9, "Attempt to auto-select the correct file based on the criteria established by your"& _ 
                    " project.", "AUTO", Nothing, true), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "None", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject10, "No file of this type should be included in the report.", "NONE", Nothing, true)})
        Me.RepositoryItemButtonEditAppendix.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.RepositoryItemButtonEditAppendix.Name = "RepositoryItemButtonEditAppendix"
        '
        'colPipeTubeSortOrder
        '
        Me.colPipeTubeSortOrder.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPipeTubeSortOrder.AppearanceCell.Options.UseFont = true
        Me.colPipeTubeSortOrder.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPipeTubeSortOrder.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colPipeTubeSortOrder.AppearanceHeader.Options.UseFont = true
        Me.colPipeTubeSortOrder.AppearanceHeader.Options.UseForeColor = true
        Me.colPipeTubeSortOrder.FieldName = "PipeTubeSortOrder"
        Me.colPipeTubeSortOrder.Name = "colPipeTubeSortOrder"
        Me.colPipeTubeSortOrder.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        '
        'colSortOrderAppendix
        '
        Me.colSortOrderAppendix.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colSortOrderAppendix.AppearanceCell.Options.UseFont = true
        Me.colSortOrderAppendix.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colSortOrderAppendix.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colSortOrderAppendix.AppearanceHeader.Options.UseFont = true
        Me.colSortOrderAppendix.AppearanceHeader.Options.UseForeColor = true
        Me.colSortOrderAppendix.FieldName = "SortOrder"
        Me.colSortOrderAppendix.Name = "colSortOrderAppendix"
        Me.colSortOrderAppendix.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        '
        'colReportItemType
        '
        Me.colReportItemType.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colReportItemType.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colReportItemType.AppearanceHeader.Options.UseFont = true
        Me.colReportItemType.AppearanceHeader.Options.UseForeColor = true
        Me.colReportItemType.Caption = "Type"
        Me.colReportItemType.FieldName = "FK_ReportItemType"
        Me.colReportItemType.Name = "colReportItemType"
        '
        'RepositoryItemLookUpEdit3
        '
        Me.RepositoryItemLookUpEdit3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RepositoryItemLookUpEdit3.Appearance.Options.UseFont = true
        Me.RepositoryItemLookUpEdit3.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RepositoryItemLookUpEdit3.AppearanceDropDown.Options.UseFont = true
        Me.RepositoryItemLookUpEdit3.AutoHeight = false
        Me.RepositoryItemLookUpEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit3.DataSource = bsPipeTubeItemTypes_LU
        Me.RepositoryItemLookUpEdit3.DisplayMember = "Description_Default"
        Me.RepositoryItemLookUpEdit3.Name = "RepositoryItemLookUpEdit3"
        Me.RepositoryItemLookUpEdit3.ValueMember = "ID"
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = false
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = false
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemSearchLookUpEdit1
        '
        Me.RepositoryItemSearchLookUpEdit1.AutoHeight = false
        Me.RepositoryItemSearchLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemSearchLookUpEdit1.Name = "RepositoryItemSearchLookUpEdit1"
        Me.RepositoryItemSearchLookUpEdit1.View = Me.GridView1
        '
        'GridView1
        '
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.GridView1.OptionsView.ShowGroupPanel = false
        '
        'RepositoryItemLookUpEdit4
        '
        Me.RepositoryItemLookUpEdit4.AutoHeight = false
        Me.RepositoryItemLookUpEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit4.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.RepositoryItemLookUpEdit4.DataSource = Me.bsInspections_AppendixItems
        Me.RepositoryItemLookUpEdit4.DisplayMember = "FullDescription"
        Me.RepositoryItemLookUpEdit4.Name = "RepositoryItemLookUpEdit4"
        Me.RepositoryItemLookUpEdit4.ValueMember = "ID"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = false
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'uiRemoveAppendixButton
        '
        Me.uiRemoveAppendixButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiRemoveAppendixButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiRemoveAppendixButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiRemoveAppendixButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiRemoveAppendixButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRemoveAppendixButton.Appearance.Options.UseBackColor = true
        Me.uiRemoveAppendixButton.Appearance.Options.UseBorderColor = true
        Me.uiRemoveAppendixButton.Appearance.Options.UseFont = true
        Me.uiRemoveAppendixButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiRemoveAppendixButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiRemoveAppendixButton.Location = New System.Drawing.Point(686, 29)
        Me.uiRemoveAppendixButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiRemoveAppendixButton.Name = "uiRemoveAppendixButton"
        Me.uiRemoveAppendixButton.Size = New System.Drawing.Size(111, 24)
        Me.uiRemoveAppendixButton.TabIndex = 2
        Me.uiRemoveAppendixButton.Text = "Remove All"
        '
        'Label135
        '
        Me.Label135.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label135.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label135.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label135.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label135.Location = New System.Drawing.Point(5, 5)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(1143, 15)
        Me.Label135.TabIndex = 83
        Me.Label135.Text = "APPENDIX IMAGES/LINKED FILES"
        Me.Label135.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'taDefaultText_PipeTubeManufacturer
        '
        Me.taDefaultText_PipeTubeManufacturer.ClearBeforeFill = true
        '
        'TblQTT_InspectorsTableAdapter
        '
        Me.TblQTT_InspectorsTableAdapter.ClearBeforeFill = true
        '
        'TblQTT_LeadInspectorsTableAdapter
        '
        Me.TblQTT_LeadInspectorsTableAdapter.ClearBeforeFill = true
        '
        'TblQTT_PrimaryRolesTableAdapter
        '
        Me.TblQTT_PrimaryRolesTableAdapter.ClearBeforeFill = true
        '
        'TblQTT_OfficesTableAdapter
        '
        Me.TblQTT_OfficesTableAdapter.ClearBeforeFill = true
        '
        'uiMainXtraTabControl
        '
        Me.uiMainXtraTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiMainXtraTabControl.Appearance.BackColor = System.Drawing.Color.White
        Me.uiMainXtraTabControl.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMainXtraTabControl.Appearance.Options.UseBackColor = true
        Me.uiMainXtraTabControl.Appearance.Options.UseFont = true
        Me.uiMainXtraTabControl.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMainXtraTabControl.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiMainXtraTabControl.Location = New System.Drawing.Point(9, 125)
        Me.uiMainXtraTabControl.LookAndFeel.SkinName = "Blue"
        Me.uiMainXtraTabControl.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiMainXtraTabControl.MultiLine = DevExpress.Utils.DefaultBoolean.[True]
        Me.uiMainXtraTabControl.Name = "uiMainXtraTabControl"
        Me.uiMainXtraTabControl.PaintStyleName = "Skin"
        Me.uiMainXtraTabControl.SelectedTabPage = Me.uiInspectionDetailsTabPage
        Me.uiMainXtraTabControl.Size = New System.Drawing.Size(1168, 604)
        Me.uiMainXtraTabControl.TabIndex = 16
        Me.uiMainXtraTabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.uiInspectionDetailsTabPage, Me.uiCustomerDetailsTabPage, Me.uiProjectDescriptionTabPage, Me.uiTubeDetailsTabPage, Me.uiExecutiveSummaryTabPage, Me.uiInspectionSummaryTabPage, Me.uiDetailedInspectionTabPage, Me.uiLinkedFilesTabPage, Me.uiFieldReportTabPage})
        '
        'uiInspectionDetailsTabPage
        '
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiSpareUninstalledGroup)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiTubeNumberInstructionsLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiCanCircularOption)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiCustomRowFormatEditor)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiNumberOfRows)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiRowFormatGroupBox)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiCustomRowFormatLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiRowsLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiInspectionStartDate)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiPurchaseOrder)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label4)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label49)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiProjectNumber)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label48)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiProposalNumber)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label45)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiToolsCheckedList)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label47)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiCustomTubeFormatEditor)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiTubeFormatGroupBox)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiCustomTubeFormatLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label46)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label44)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label43)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiRemoveInspectorButton)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiAddInspectorButton)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiRolesList)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiProjectInspectorsList)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiOtherInspectorsList)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiPlantNameLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label37)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiTitleLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label41)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.Label5)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiLeadInspectorLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiOfficeLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiInspectionEndDate)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiEndDateLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiStartDateLabel)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiTitleRole)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiLeadInspector)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiOffices)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiInspectorPhone)
        Me.uiInspectionDetailsTabPage.Controls.Add(Me.uiNumberOfTubes)
        Me.uiInspectionDetailsTabPage.Name = "uiInspectionDetailsTabPage"
        Me.uiInspectionDetailsTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiInspectionDetailsTabPage.Text = "Inspection Details"
        '
        'uiSpareUninstalledGroup
        '
        Me.uiSpareUninstalledGroup.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "SpareUninstalled", true))
        Me.uiSpareUninstalledGroup.EditValue = -1
        Me.uiSpareUninstalledGroup.Location = New System.Drawing.Point(401, 352)
        Me.uiSpareUninstalledGroup.Name = "uiSpareUninstalledGroup"
        Me.uiSpareUninstalledGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiSpareUninstalledGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSpareUninstalledGroup.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiSpareUninstalledGroup.Properties.Appearance.Options.UseBackColor = true
        Me.uiSpareUninstalledGroup.Properties.Appearance.Options.UseFont = true
        Me.uiSpareUninstalledGroup.Properties.Appearance.Options.UseForeColor = true
        Me.uiSpareUninstalledGroup.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSpareUninstalledGroup.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiSpareUninstalledGroup.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiSpareUninstalledGroup.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiSpareUninstalledGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiSpareUninstalledGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "New Spare"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "New Uninstalled")})
        Me.uiSpareUninstalledGroup.Size = New System.Drawing.Size(137, 50)
        Me.uiSpareUninstalledGroup.TabIndex = 35
        ConditionValidationRule40.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Between
        ConditionValidationRule40.ErrorText = "Please select Spare or Uninstalled."
        ConditionValidationRule40.Value1 = 0
        ConditionValidationRule40.Value2 = 1
        Me.DxValidationProvider.SetValidationRule(Me.uiSpareUninstalledGroup, ConditionValidationRule40)
        '
        'uiTubeNumberInstructionsLabel
        '
        Me.uiTubeNumberInstructionsLabel.AutoSize = true
        Me.uiTubeNumberInstructionsLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeNumberInstructionsLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeNumberInstructionsLabel.Location = New System.Drawing.Point(549, 330)
        Me.uiTubeNumberInstructionsLabel.Name = "uiTubeNumberInstructionsLabel"
        Me.uiTubeNumberInstructionsLabel.Size = New System.Drawing.Size(181, 17)
        Me.uiTubeNumberInstructionsLabel.TabIndex = 39
        Me.uiTubeNumberInstructionsLabel.Text = "List number of tubes per row."
        '
        'uiCanCircularOption
        '
        Me.uiCanCircularOption.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "CanCircular", true))
        Me.uiCanCircularOption.Location = New System.Drawing.Point(225, 327)
        Me.uiCanCircularOption.Name = "uiCanCircularOption"
        Me.uiCanCircularOption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCanCircularOption.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCanCircularOption.Properties.Appearance.Options.UseFont = true
        Me.uiCanCircularOption.Properties.Appearance.Options.UseForeColor = true
        Me.uiCanCircularOption.Properties.Caption = "Can/Circular Reformer"
        Me.uiCanCircularOption.Size = New System.Drawing.Size(166, 22)
        Me.uiCanCircularOption.TabIndex = 32
        '
        'uiNumberOfRows
        '
        Me.uiNumberOfRows.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "NumberOfRows", true))
        Me.uiNumberOfRows.Location = New System.Drawing.Point(295, 368)
        Me.uiNumberOfRows.Name = "uiNumberOfRows"
        Me.uiNumberOfRows.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfRows.Properties.Appearance.Options.UseFont = true
        Me.uiNumberOfRows.Properties.Appearance.Options.UseTextOptions = true
        Me.uiNumberOfRows.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfRows.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfRows.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiNumberOfRows.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfRows.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiNumberOfRows.Properties.AppearanceDropDown.Options.UseTextOptions = true
        Me.uiNumberOfRows.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfRows.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfRows.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiNumberOfRows.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberOfRows.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject11.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject11.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject11.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject11.Options.UseBackColor = true
        SerializableAppearanceObject11.Options.UseBorderColor = true
        Me.uiNumberOfRows.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject11, "", Nothing, Nothing, true)})
        Me.uiNumberOfRows.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiNumberOfRows.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberOfRows.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberOfRows.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"})
        Me.uiNumberOfRows.Properties.PopupFormMinSize = New System.Drawing.Size(50, 0)
        Me.uiNumberOfRows.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiNumberOfRows.Size = New System.Drawing.Size(62, 24)
        Me.uiNumberOfRows.TabIndex = 34
        ConditionValidationRule41.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Greater
        ConditionValidationRule41.ErrorText = "Number of rows cannot be zero."
        ConditionValidationRule41.Value1 = 0
        Me.DxValidationProvider.SetValidationRule(Me.uiNumberOfRows, ConditionValidationRule41)
        '
        'uiRowFormatGroupBox
        '
        Me.uiRowFormatGroupBox.Controls.Add(Me.RowFormatRadioGroup)
        Me.uiRowFormatGroupBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRowFormatGroupBox.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiRowFormatGroupBox.Location = New System.Drawing.Point(220, 399)
        Me.uiRowFormatGroupBox.Name = "uiRowFormatGroupBox"
        Me.uiRowFormatGroupBox.Size = New System.Drawing.Size(237, 103)
        Me.uiRowFormatGroupBox.TabIndex = 36
        Me.uiRowFormatGroupBox.TabStop = false
        Me.uiRowFormatGroupBox.Text = "Row # Format"
        '
        'RowFormatRadioGroup
        '
        Me.RowFormatRadioGroup.Location = New System.Drawing.Point(7, 17)
        Me.RowFormatRadioGroup.Name = "RowFormatRadioGroup"
        Me.RowFormatRadioGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.RowFormatRadioGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RowFormatRadioGroup.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.RowFormatRadioGroup.Properties.Appearance.Options.UseBackColor = true
        Me.RowFormatRadioGroup.Properties.Appearance.Options.UseFont = true
        Me.RowFormatRadioGroup.Properties.Appearance.Options.UseForeColor = true
        Me.RowFormatRadioGroup.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RowFormatRadioGroup.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.RowFormatRadioGroup.Properties.AppearanceDisabled.Options.UseFont = true
        Me.RowFormatRadioGroup.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.RowFormatRadioGroup.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RowFormatRadioGroup.Properties.AppearanceFocused.Options.UseFont = true
        Me.RowFormatRadioGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.RowFormatRadioGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Numeric (1, 2, 3, ...)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Alphabetical (A, B, C, ...)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Custom (specify below)")})
        Me.RowFormatRadioGroup.Size = New System.Drawing.Size(197, 84)
        Me.RowFormatRadioGroup.TabIndex = 36
        ConditionValidationRule42.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Between
        ConditionValidationRule42.ErrorText = "Please select a Row # format."
        ConditionValidationRule42.Value1 = "0"
        ConditionValidationRule42.Value2 = "2"
        Me.DxValidationProvider.SetValidationRule(Me.RowFormatRadioGroup, ConditionValidationRule42)
        '
        'uiCustomRowFormatLabel
        '
        Me.uiCustomRowFormatLabel.AutoSize = true
        Me.uiCustomRowFormatLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomRowFormatLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCustomRowFormatLabel.Location = New System.Drawing.Point(220, 505)
        Me.uiCustomRowFormatLabel.Name = "uiCustomRowFormatLabel"
        Me.uiCustomRowFormatLabel.Size = New System.Drawing.Size(285, 17)
        Me.uiCustomRowFormatLabel.TabIndex = 37
        Me.uiCustomRowFormatLabel.Text = "Custom Row Numbers (separate with commas):"
        Me.uiCustomRowFormatLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiRowsLabel
        '
        Me.uiRowsLabel.AutoSize = true
        Me.uiRowsLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRowsLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiRowsLabel.Location = New System.Drawing.Point(220, 368)
        Me.uiRowsLabel.Name = "uiRowsLabel"
        Me.uiRowsLabel.Size = New System.Drawing.Size(70, 17)
        Me.uiRowsLabel.TabIndex = 33
        Me.uiRowsLabel.Text = "# of Rows:"
        Me.uiRowsLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiInspectionStartDate
        '
        Me.uiInspectionStartDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "InspectionDateStart", true))
        Me.uiInspectionStartDate.EditValue = Nothing
        Me.uiInspectionStartDate.Location = New System.Drawing.Point(89, 13)
        Me.uiInspectionStartDate.Name = "uiInspectionStartDate"
        Me.uiInspectionStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionStartDate.Properties.Appearance.Options.UseFont = true
        Me.uiInspectionStartDate.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionStartDate.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiInspectionStartDate.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionStartDate.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiInspectionStartDate.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionStartDate.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject12.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject12.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject12.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject12.Options.UseBackColor = true
        SerializableAppearanceObject12.Options.UseBorderColor = true
        Me.uiInspectionStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject12, "", Nothing, Nothing, true)})
        Me.uiInspectionStartDate.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionStartDate.Properties.DisplayFormat.FormatString = "MMMM d, yyyy"
        Me.uiInspectionStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.uiInspectionStartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.uiInspectionStartDate.Size = New System.Drawing.Size(161, 24)
        Me.uiInspectionStartDate.TabIndex = 1
        CompareAgainstControlValidationRule2.CompareControlOperator = DevExpress.XtraEditors.DXErrorProvider.CompareControlOperator.LessOrEqual
        CompareAgainstControlValidationRule2.Control = Me.uiInspectionEndDate
        CompareAgainstControlValidationRule2.ErrorText = "Start date must be equal to or greater than start date."
        Me.DxValidationProvider.SetValidationRule(Me.uiInspectionStartDate, CompareAgainstControlValidationRule2)
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label4.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label4.Location = New System.Drawing.Point(15, 268)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 17)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Purchase Order #:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label49
        '
        Me.Label49.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label49.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label49.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label49.Location = New System.Drawing.Point(620, 268)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(75, 17)
        Me.Label49.TabIndex = 26
        Me.Label49.Text = "Project #:"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label48
        '
        Me.Label48.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label48.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label48.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label48.Location = New System.Drawing.Point(338, 268)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(90, 17)
        Me.Label48.TabIndex = 24
        Me.Label48.Text = "Proposal #:"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label45.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label45.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label45.Location = New System.Drawing.Point(14, 245)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(825, 15)
        Me.Label45.TabIndex = 21
        Me.Label45.Text = "REPORT NUMBERS"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiToolsCheckedList
        '
        Me.uiToolsCheckedList.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiToolsCheckedList.Appearance.Options.UseFont = true
        Me.uiToolsCheckedList.CheckOnClick = true
        Me.uiToolsCheckedList.DataSource = Me.bsInspectionTools
        Me.uiToolsCheckedList.DisplayMember = "Desc"
        Me.uiToolsCheckedList.Location = New System.Drawing.Point(15, 343)
        Me.uiToolsCheckedList.Name = "uiToolsCheckedList"
        Me.uiToolsCheckedList.Size = New System.Drawing.Size(171, 206)
        Me.uiToolsCheckedList.SortOrder = System.Windows.Forms.SortOrder.Ascending
        Me.uiToolsCheckedList.TabIndex = 30
        Me.uiToolsCheckedList.ValueMember = "ID"
        '
        'Label47
        '
        Me.Label47.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label47.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label47.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label47.Location = New System.Drawing.Point(219, 304)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(617, 15)
        Me.Label47.TabIndex = 31
        Me.Label47.Text = "TUBES"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiTubeFormatGroupBox
        '
        Me.uiTubeFormatGroupBox.Controls.Add(Me.TubeFormatRadioGroup)
        Me.uiTubeFormatGroupBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFormatGroupBox.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeFormatGroupBox.Location = New System.Drawing.Point(549, 399)
        Me.uiTubeFormatGroupBox.Name = "uiTubeFormatGroupBox"
        Me.uiTubeFormatGroupBox.Size = New System.Drawing.Size(237, 103)
        Me.uiTubeFormatGroupBox.TabIndex = 42
        Me.uiTubeFormatGroupBox.TabStop = false
        Me.uiTubeFormatGroupBox.Text = "Tube # Format"
        '
        'TubeFormatRadioGroup
        '
        Me.TubeFormatRadioGroup.Location = New System.Drawing.Point(7, 17)
        Me.TubeFormatRadioGroup.Name = "TubeFormatRadioGroup"
        Me.TubeFormatRadioGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.TubeFormatRadioGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TubeFormatRadioGroup.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.TubeFormatRadioGroup.Properties.Appearance.Options.UseBackColor = true
        Me.TubeFormatRadioGroup.Properties.Appearance.Options.UseFont = true
        Me.TubeFormatRadioGroup.Properties.Appearance.Options.UseForeColor = true
        Me.TubeFormatRadioGroup.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TubeFormatRadioGroup.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.TubeFormatRadioGroup.Properties.AppearanceDisabled.Options.UseFont = true
        Me.TubeFormatRadioGroup.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.TubeFormatRadioGroup.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TubeFormatRadioGroup.Properties.AppearanceFocused.Options.UseFont = true
        Me.TubeFormatRadioGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TubeFormatRadioGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Numeric (1, 2, 3, ...)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Alphabetical (A, B, C, ...)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Custom (specify below)")})
        Me.TubeFormatRadioGroup.Size = New System.Drawing.Size(197, 84)
        Me.TubeFormatRadioGroup.TabIndex = 42
        ConditionValidationRule43.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Between
        ConditionValidationRule43.ErrorText = "Please select a Tube # format."
        ConditionValidationRule43.Value1 = "0"
        ConditionValidationRule43.Value2 = "2"
        Me.DxValidationProvider.SetValidationRule(Me.TubeFormatRadioGroup, ConditionValidationRule43)
        '
        'uiCustomTubeFormatLabel
        '
        Me.uiCustomTubeFormatLabel.AutoSize = true
        Me.uiCustomTubeFormatLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomTubeFormatLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCustomTubeFormatLabel.Location = New System.Drawing.Point(549, 505)
        Me.uiCustomTubeFormatLabel.Name = "uiCustomTubeFormatLabel"
        Me.uiCustomTubeFormatLabel.Size = New System.Drawing.Size(289, 17)
        Me.uiCustomTubeFormatLabel.TabIndex = 43
        Me.uiCustomTubeFormatLabel.Text = "Custom Tube Numbers (separate with commas):"
        Me.uiCustomTubeFormatLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiLabel
        '
        Me.uiLabel.AutoSize = true
        Me.uiLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiLabel.Location = New System.Drawing.Point(549, 368)
        Me.uiLabel.Name = "uiLabel"
        Me.uiLabel.Size = New System.Drawing.Size(74, 17)
        Me.uiLabel.TabIndex = 40
        Me.uiLabel.Text = "# of Tubes:"
        Me.uiLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label46
        '
        Me.Label46.AutoSize = true
        Me.Label46.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label46.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label46.Location = New System.Drawing.Point(14, 323)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(93, 17)
        Me.Label46.TabIndex = 29
        Me.Label46.Text = "Project Tools:"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label44.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label44.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label44.Location = New System.Drawing.Point(14, 304)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(172, 15)
        Me.Label44.TabIndex = 28
        Me.Label44.Text = "TOOLS"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label43.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label43.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label43.Location = New System.Drawing.Point(12, 42)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(827, 15)
        Me.Label43.TabIndex = 6
        Me.Label43.Text = "INSPECTORS"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiRemoveInspectorButton
        '
        Me.uiRemoveInspectorButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiRemoveInspectorButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiRemoveInspectorButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiRemoveInspectorButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRemoveInspectorButton.Appearance.Options.UseBackColor = true
        Me.uiRemoveInspectorButton.Appearance.Options.UseBorderColor = true
        Me.uiRemoveInspectorButton.Appearance.Options.UseFont = true
        Me.uiRemoveInspectorButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiRemoveInspectorButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiRemoveInspectorButton.Location = New System.Drawing.Point(446, 176)
        Me.uiRemoveInspectorButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiRemoveInspectorButton.Name = "uiRemoveInspectorButton"
        Me.uiRemoveInspectorButton.Size = New System.Drawing.Size(84, 32)
        Me.uiRemoveInspectorButton.TabIndex = 18
        Me.uiRemoveInspectorButton.Text = "< Remove"
        '
        'uiAddInspectorButton
        '
        Me.uiAddInspectorButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiAddInspectorButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiAddInspectorButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiAddInspectorButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAddInspectorButton.Appearance.Options.UseBackColor = true
        Me.uiAddInspectorButton.Appearance.Options.UseBorderColor = true
        Me.uiAddInspectorButton.Appearance.Options.UseFont = true
        Me.uiAddInspectorButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAddInspectorButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiAddInspectorButton.Location = New System.Drawing.Point(446, 138)
        Me.uiAddInspectorButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiAddInspectorButton.Name = "uiAddInspectorButton"
        Me.uiAddInspectorButton.Size = New System.Drawing.Size(84, 32)
        Me.uiAddInspectorButton.TabIndex = 17
        Me.uiAddInspectorButton.Text = "Add >"
        '
        'uiRolesList
        '
        Me.uiRolesList.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRolesList.Appearance.Options.UseFont = true
        Me.uiRolesList.DataSource = Me.bsQTT_Roles_Other
        Me.uiRolesList.DisplayMember = "Description"
        Me.uiRolesList.Location = New System.Drawing.Point(251, 110)
        Me.uiRolesList.Name = "uiRolesList"
        Me.uiRolesList.Size = New System.Drawing.Size(173, 127)
        Me.uiRolesList.SortOrder = System.Windows.Forms.SortOrder.Ascending
        Me.uiRolesList.TabIndex = 16
        Me.uiRolesList.ValueMember = "ID"
        '
        'uiProjectInspectorsList
        '
        Me.uiProjectInspectorsList.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiProjectInspectorsList.Appearance.Options.UseFont = true
        Me.uiProjectInspectorsList.DataSource = Me.bsInspections_OtherInspectors
        Me.uiProjectInspectorsList.DisplayMember = "NameAndRoleDescription"
        Me.uiProjectInspectorsList.Location = New System.Drawing.Point(551, 110)
        Me.uiProjectInspectorsList.Name = "uiProjectInspectorsList"
        Me.uiProjectInspectorsList.Size = New System.Drawing.Size(288, 127)
        Me.uiProjectInspectorsList.TabIndex = 20
        Me.uiProjectInspectorsList.ValueMember = "ID"
        '
        'uiOtherInspectorsList
        '
        Me.uiOtherInspectorsList.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOtherInspectorsList.Appearance.Options.UseFont = true
        Me.uiOtherInspectorsList.DataSource = Me.bsOtherInspectors
        Me.uiOtherInspectorsList.DisplayMember = "Description"
        Me.uiOtherInspectorsList.Location = New System.Drawing.Point(14, 110)
        Me.uiOtherInspectorsList.Name = "uiOtherInspectorsList"
        Me.uiOtherInspectorsList.Size = New System.Drawing.Size(222, 127)
        Me.uiOtherInspectorsList.SortOrder = System.Windows.Forms.SortOrder.Ascending
        Me.uiOtherInspectorsList.TabIndex = 14
        Me.uiOtherInspectorsList.ValueMember = "ID"
        '
        'uiPlantNameLabel
        '
        Me.uiPlantNameLabel.AutoSize = true
        Me.uiPlantNameLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiPlantNameLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiPlantNameLabel.Location = New System.Drawing.Point(529, 63)
        Me.uiPlantNameLabel.Name = "uiPlantNameLabel"
        Me.uiPlantNameLabel.Size = New System.Drawing.Size(132, 17)
        Me.uiPlantNameLabel.TabIndex = 11
        Me.uiPlantNameLabel.Text = "Phone and Extension:"
        Me.uiPlantNameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label37
        '
        Me.Label37.AutoSize = true
        Me.Label37.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label37.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label37.Location = New System.Drawing.Point(251, 90)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(85, 17)
        Me.Label37.TabIndex = 15
        Me.Label37.Text = "Choose Role:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTitleLabel
        '
        Me.uiTitleLabel.AutoSize = true
        Me.uiTitleLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTitleLabel.Location = New System.Drawing.Point(302, 63)
        Me.uiTitleLabel.Name = "uiTitleLabel"
        Me.uiTitleLabel.Size = New System.Drawing.Size(66, 17)
        Me.uiTitleLabel.TabIndex = 9
        Me.uiTitleLabel.Text = "Title/Role:"
        Me.uiTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label41
        '
        Me.Label41.AutoSize = true
        Me.Label41.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label41.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label41.Location = New System.Drawing.Point(551, 90)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(122, 17)
        Me.Label41.TabIndex = 19
        Me.Label41.Text = "Project Inspectors:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label5.Location = New System.Drawing.Point(14, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Choose Inspector:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiLeadInspectorLabel
        '
        Me.uiLeadInspectorLabel.AutoSize = true
        Me.uiLeadInspectorLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspectorLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiLeadInspectorLabel.Location = New System.Drawing.Point(14, 63)
        Me.uiLeadInspectorLabel.Name = "uiLeadInspectorLabel"
        Me.uiLeadInspectorLabel.Size = New System.Drawing.Size(97, 17)
        Me.uiLeadInspectorLabel.TabIndex = 7
        Me.uiLeadInspectorLabel.Text = "Lead Inspector:"
        Me.uiLeadInspectorLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiOfficeLabel
        '
        Me.uiOfficeLabel.AutoSize = true
        Me.uiOfficeLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOfficeLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiOfficeLabel.Location = New System.Drawing.Point(521, 13)
        Me.uiOfficeLabel.Name = "uiOfficeLabel"
        Me.uiOfficeLabel.Size = New System.Drawing.Size(120, 17)
        Me.uiOfficeLabel.TabIndex = 4
        Me.uiOfficeLabel.Text = "Responsible Office:"
        Me.uiOfficeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiEndDateLabel
        '
        Me.uiEndDateLabel.AutoSize = true
        Me.uiEndDateLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiEndDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiEndDateLabel.Location = New System.Drawing.Point(270, 13)
        Me.uiEndDateLabel.Name = "uiEndDateLabel"
        Me.uiEndDateLabel.Size = New System.Drawing.Size(64, 17)
        Me.uiEndDateLabel.TabIndex = 2
        Me.uiEndDateLabel.Text = "End Date:"
        Me.uiEndDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiStartDateLabel
        '
        Me.uiStartDateLabel.AutoSize = true
        Me.uiStartDateLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiStartDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiStartDateLabel.Location = New System.Drawing.Point(14, 13)
        Me.uiStartDateLabel.Name = "uiStartDateLabel"
        Me.uiStartDateLabel.Size = New System.Drawing.Size(69, 17)
        Me.uiStartDateLabel.TabIndex = 0
        Me.uiStartDateLabel.Text = "Start Date:"
        Me.uiStartDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTitleRole
        '
        Me.uiTitleRole.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "FK_PrimaryRole", true))
        Me.uiTitleRole.EditValue = 4
        Me.uiTitleRole.Location = New System.Drawing.Point(374, 63)
        Me.uiTitleRole.Name = "uiTitleRole"
        Me.uiTitleRole.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleRole.Properties.Appearance.Options.UseFont = true
        Me.uiTitleRole.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleRole.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTitleRole.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleRole.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTitleRole.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleRole.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTitleRole.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTitleRole.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject13.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject13.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject13.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject13.Options.UseBackColor = true
        SerializableAppearanceObject13.Options.UseBorderColor = true
        Me.uiTitleRole.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject13, "", Nothing, Nothing, true)})
        Me.uiTitleRole.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTitleRole.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("IsNotActive", "Is Not Active", 83, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Address", 58, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("City", "City", 31, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("State", "State", 39, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PostalCode", "Postal Code", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Phone", "Phone", 46, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fax", "Fax", 29, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Email", "Email", 41, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiTitleRole.Properties.DataSource = Me.bsQTT_Roles_Main
        Me.uiTitleRole.Properties.DisplayMember = "Description"
        Me.uiTitleRole.Properties.DropDownRows = 4
        Me.uiTitleRole.Properties.NullText = ""
        Me.uiTitleRole.Properties.PopupFormMinSize = New System.Drawing.Size(100, 0)
        Me.uiTitleRole.Properties.PopupSizeable = false
        Me.uiTitleRole.Properties.PopupWidth = 100
        Me.uiTitleRole.Properties.ShowFooter = false
        Me.uiTitleRole.Properties.ShowHeader = false
        Me.uiTitleRole.Properties.SortColumnIndex = 1
        Me.uiTitleRole.Properties.ValueMember = "ID"
        Me.uiTitleRole.Size = New System.Drawing.Size(145, 24)
        Me.uiTitleRole.TabIndex = 10
        ConditionValidationRule44.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule44.ErrorText = "Title/Role cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTitleRole, ConditionValidationRule44)
        '
        'uiLeadInspector
        '
        Me.uiLeadInspector.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "FK_PrimaryInspector", true))
        Me.uiLeadInspector.EditValue = 4
        Me.uiLeadInspector.Location = New System.Drawing.Point(118, 63)
        Me.uiLeadInspector.Name = "uiLeadInspector"
        Me.uiLeadInspector.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspector.Properties.Appearance.Options.UseFont = true
        Me.uiLeadInspector.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspector.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiLeadInspector.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspector.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiLeadInspector.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspector.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiLeadInspector.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadInspector.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject14.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject14.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject14.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject14.Options.UseBackColor = true
        SerializableAppearanceObject14.Options.UseBorderColor = true
        Me.uiLeadInspector.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject14, "", Nothing, Nothing, true)})
        Me.uiLeadInspector.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiLeadInspector.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Address", 58, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("City", "City", 31, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("State", "State", 39, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PostalCode", "Postal Code", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Phone", "Phone", 46, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fax", "Fax", 29, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Email", "Email", 41, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiLeadInspector.Properties.DataSource = Me.bsQTT_LeadInspectors
        Me.uiLeadInspector.Properties.DisplayMember = "Description"
        Me.uiLeadInspector.Properties.DropDownRows = 9
        Me.uiLeadInspector.Properties.NullText = ""
        Me.uiLeadInspector.Properties.PopupFormMinSize = New System.Drawing.Size(150, 0)
        Me.uiLeadInspector.Properties.PopupSizeable = false
        Me.uiLeadInspector.Properties.ShowFooter = false
        Me.uiLeadInspector.Properties.ShowHeader = false
        Me.uiLeadInspector.Properties.SortColumnIndex = 1
        Me.uiLeadInspector.Properties.ValueMember = "ID"
        Me.uiLeadInspector.Size = New System.Drawing.Size(172, 24)
        Me.uiLeadInspector.TabIndex = 8
        ConditionValidationRule45.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule45.ErrorText = "Lead Inspector cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiLeadInspector, ConditionValidationRule45)
        '
        'uiOffices
        '
        Me.uiOffices.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "FK_QTT_Office", true))
        Me.uiOffices.EditValue = 4
        Me.uiOffices.Location = New System.Drawing.Point(646, 13)
        Me.uiOffices.Name = "uiOffices"
        Me.uiOffices.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOffices.Properties.Appearance.Options.UseFont = true
        Me.uiOffices.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOffices.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOffices.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOffices.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiOffices.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOffices.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiOffices.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOffices.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject15.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject15.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject15.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject15.Options.UseBackColor = true
        SerializableAppearanceObject15.Options.UseBorderColor = true
        Me.uiOffices.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject15, "", Nothing, Nothing, true)})
        Me.uiOffices.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiOffices.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Address", 58, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("City", "City", 31, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("State", "State", 39, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PostalCode", "Postal Code", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Phone", "Phone", 46, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fax", "Fax", 29, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Email", "Email", 41, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiOffices.Properties.DataSource = Me.bsQTTOffices
        Me.uiOffices.Properties.DisplayMember = "Description"
        Me.uiOffices.Properties.DropDownRows = 9
        Me.uiOffices.Properties.NullText = ""
        Me.uiOffices.Properties.PopupFormMinSize = New System.Drawing.Size(150, 0)
        Me.uiOffices.Properties.PopupSizeable = false
        Me.uiOffices.Properties.ShowFooter = false
        Me.uiOffices.Properties.ShowHeader = false
        Me.uiOffices.Properties.ValueMember = "ID"
        Me.uiOffices.Size = New System.Drawing.Size(193, 24)
        Me.uiOffices.TabIndex = 5
        ConditionValidationRule46.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule46.ErrorText = "Responsible Office cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiOffices, ConditionValidationRule46)
        '
        'uiCustomerDetailsTabPage
        '
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiJMC)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiClearPriorHistoryRedButton)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiReformerOEM)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiPlantProcessType)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiFlowDirection)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiBurnerConfiguration)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label10)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label9)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label124)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiManufacturerLabel)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiHistoryLabel)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiFirstInspectionInsertSampleTextButton)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiHistoryInsertSampleTextButton)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiGridControlContacts)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiPostalCode)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiPriorHistoryEditor)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiPriorHistoryLabel)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label91)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label51)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label40)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label36)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label25)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiState)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiCountry)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiCity)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label3)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label8)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiAddress)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label7)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label6)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label2)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiLocation)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiCustomers)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.Label1)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiCustomerLabel)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiReformerName)
        Me.uiCustomerDetailsTabPage.Controls.Add(Me.uiReformerID)
        Me.uiCustomerDetailsTabPage.Name = "uiCustomerDetailsTabPage"
        Me.uiCustomerDetailsTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiCustomerDetailsTabPage.Text = "Customer Details"
        '
        'uiJMC
        '
        Me.uiJMC.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "JMC", true))
        Me.uiJMC.Location = New System.Drawing.Point(427, 15)
        Me.uiJMC.Name = "uiJMC"
        Me.uiJMC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiJMC.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiJMC.Properties.Appearance.Options.UseFont = true
        Me.uiJMC.Properties.Appearance.Options.UseForeColor = true
        Me.uiJMC.Properties.Caption = "Johnson Matthey Job"
        Me.uiJMC.Size = New System.Drawing.Size(210, 22)
        Me.uiJMC.TabIndex = 2
        '
        'uiClearPriorHistoryRedButton
        '
        Me.uiClearPriorHistoryRedButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiClearPriorHistoryRedButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiClearPriorHistoryRedButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiClearPriorHistoryRedButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiClearPriorHistoryRedButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiClearPriorHistoryRedButton.Appearance.Options.UseBackColor = true
        Me.uiClearPriorHistoryRedButton.Appearance.Options.UseBorderColor = true
        Me.uiClearPriorHistoryRedButton.Appearance.Options.UseFont = true
        Me.uiClearPriorHistoryRedButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiClearPriorHistoryRedButton.Location = New System.Drawing.Point(744, 505)
        Me.uiClearPriorHistoryRedButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiClearPriorHistoryRedButton.Name = "uiClearPriorHistoryRedButton"
        Me.uiClearPriorHistoryRedButton.Size = New System.Drawing.Size(138, 24)
        Me.uiClearPriorHistoryRedButton.TabIndex = 35
        Me.uiClearPriorHistoryRedButton.Text = "Clear Red"
        '
        'uiReformerOEM
        '
        Me.uiReformerOEM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "ReformerOEM", true))
        Me.uiReformerOEM.EditValue = ""
        Me.uiReformerOEM.Location = New System.Drawing.Point(138, 244)
        Me.uiReformerOEM.Name = "uiReformerOEM"
        Me.uiReformerOEM.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiReformerOEM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerOEM.Properties.Appearance.Options.UseBackColor = true
        Me.uiReformerOEM.Properties.Appearance.Options.UseFont = true
        Me.uiReformerOEM.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerOEM.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiReformerOEM.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerOEM.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiReformerOEM.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerOEM.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiReformerOEM.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerOEM.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject16.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject16.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject16.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject16.Options.UseBackColor = true
        SerializableAppearanceObject16.Options.UseBorderColor = true
        Me.uiReformerOEM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject16, "", Nothing, Nothing, true)})
        Me.uiReformerOEM.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiReformerOEM.Properties.MaxLength = 20
        Me.uiReformerOEM.Size = New System.Drawing.Size(383, 24)
        Me.uiReformerOEM.TabIndex = 25
        ConditionValidationRule47.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule47.ErrorText = "Reformer OEM cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiReformerOEM, ConditionValidationRule47)
        '
        'uiPlantProcessType
        '
        Me.uiPlantProcessType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "PlantProcessType", true))
        Me.uiPlantProcessType.EditValue = ""
        Me.uiPlantProcessType.Location = New System.Drawing.Point(138, 214)
        Me.uiPlantProcessType.Name = "uiPlantProcessType"
        Me.uiPlantProcessType.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiPlantProcessType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantProcessType.Properties.Appearance.Options.UseBackColor = true
        Me.uiPlantProcessType.Properties.Appearance.Options.UseFont = true
        Me.uiPlantProcessType.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantProcessType.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiPlantProcessType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantProcessType.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiPlantProcessType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantProcessType.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiPlantProcessType.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPlantProcessType.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject17.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject17.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject17.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject17.Options.UseBackColor = true
        SerializableAppearanceObject17.Options.UseBorderColor = true
        Me.uiPlantProcessType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject17, "", Nothing, Nothing, true)})
        Me.uiPlantProcessType.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiPlantProcessType.Properties.MaxLength = 20
        Me.uiPlantProcessType.Properties.PopupFormMinSize = New System.Drawing.Size(100, 0)
        Me.uiPlantProcessType.Size = New System.Drawing.Size(206, 24)
        Me.uiPlantProcessType.TabIndex = 21
        ConditionValidationRule48.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule48.ErrorText = "Plant Process Type cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiPlantProcessType, ConditionValidationRule48)
        '
        'uiFlowDirection
        '
        Me.uiFlowDirection.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "ProcessFlowDirection", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.uiFlowDirection.EditValue = ""
        Me.uiFlowDirection.Location = New System.Drawing.Point(510, 214)
        Me.uiFlowDirection.Name = "uiFlowDirection"
        Me.uiFlowDirection.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiFlowDirection.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFlowDirection.Properties.Appearance.Options.UseBackColor = true
        Me.uiFlowDirection.Properties.Appearance.Options.UseFont = true
        Me.uiFlowDirection.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFlowDirection.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiFlowDirection.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFlowDirection.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiFlowDirection.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFlowDirection.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiFlowDirection.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFlowDirection.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject18.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject18.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject18.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject18.Options.UseBackColor = true
        SerializableAppearanceObject18.Options.UseBorderColor = true
        Me.uiFlowDirection.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject18, "", Nothing, Nothing, true)})
        Me.uiFlowDirection.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiFlowDirection.Properties.MaxLength = 20
        Me.uiFlowDirection.Properties.PopupFormMinSize = New System.Drawing.Size(100, 0)
        Me.uiFlowDirection.Size = New System.Drawing.Size(145, 24)
        Me.uiFlowDirection.TabIndex = 23
        ConditionValidationRule49.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule49.ErrorText = "Process Flow Direction cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiFlowDirection, ConditionValidationRule49)
        '
        'uiBurnerConfiguration
        '
        Me.uiBurnerConfiguration.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "BurnerConfiguration", true))
        Me.uiBurnerConfiguration.EditValue = ""
        Me.uiBurnerConfiguration.Location = New System.Drawing.Point(669, 244)
        Me.uiBurnerConfiguration.Name = "uiBurnerConfiguration"
        Me.uiBurnerConfiguration.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiBurnerConfiguration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBurnerConfiguration.Properties.Appearance.Options.UseBackColor = true
        Me.uiBurnerConfiguration.Properties.Appearance.Options.UseFont = true
        Me.uiBurnerConfiguration.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBurnerConfiguration.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiBurnerConfiguration.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBurnerConfiguration.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiBurnerConfiguration.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBurnerConfiguration.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiBurnerConfiguration.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiBurnerConfiguration.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject19.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject19.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject19.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject19.Options.UseBackColor = true
        SerializableAppearanceObject19.Options.UseBorderColor = true
        Me.uiBurnerConfiguration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject19, "", Nothing, Nothing, true)})
        Me.uiBurnerConfiguration.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiBurnerConfiguration.Properties.MaxLength = 20
        Me.uiBurnerConfiguration.Properties.PopupFormMinSize = New System.Drawing.Size(100, 0)
        Me.uiBurnerConfiguration.Size = New System.Drawing.Size(138, 24)
        Me.uiBurnerConfiguration.TabIndex = 27
        ConditionValidationRule50.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule50.ErrorText = "Burner Configuration cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiBurnerConfiguration, ConditionValidationRule50)
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label10.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label10.Location = New System.Drawing.Point(532, 244)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(132, 17)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Burner Configuration:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label9.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label9.Location = New System.Drawing.Point(10, 214)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Plant Process Type:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label124
        '
        Me.Label124.AutoSize = true
        Me.Label124.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label124.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label124.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label124.Location = New System.Drawing.Point(363, 214)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(142, 17)
        Me.Label124.TabIndex = 22
        Me.Label124.Text = "Process Flow Direction:"
        Me.Label124.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiManufacturerLabel
        '
        Me.uiManufacturerLabel.AutoSize = true
        Me.uiManufacturerLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiManufacturerLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiManufacturerLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiManufacturerLabel.Location = New System.Drawing.Point(33, 244)
        Me.uiManufacturerLabel.Name = "uiManufacturerLabel"
        Me.uiManufacturerLabel.Size = New System.Drawing.Size(99, 17)
        Me.uiManufacturerLabel.TabIndex = 24
        Me.uiManufacturerLabel.Text = "Reformer OEM:"
        Me.uiManufacturerLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiHistoryLabel
        '
        Me.uiHistoryLabel.Font = New System.Drawing.Font("Segoe UI", 9!)
        Me.uiHistoryLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiHistoryLabel.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.uiHistoryLabel.Location = New System.Drawing.Point(888, 444)
        Me.uiHistoryLabel.Name = "uiHistoryLabel"
        Me.uiHistoryLabel.Size = New System.Drawing.Size(138, 54)
        Me.uiHistoryLabel.TabIndex = 34
        Me.uiHistoryLabel.Text = "Double-click to select red text and then type to replace value."
        '
        'uiFirstInspectionInsertSampleTextButton
        '
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.Options.UseBackColor = true
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.Options.UseBorderColor = true
        Me.uiFirstInspectionInsertSampleTextButton.Appearance.Options.UseFont = true
        Me.uiFirstInspectionInsertSampleTextButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiFirstInspectionInsertSampleTextButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiFirstInspectionInsertSampleTextButton.Location = New System.Drawing.Point(744, 443)
        Me.uiFirstInspectionInsertSampleTextButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiFirstInspectionInsertSampleTextButton.Name = "uiFirstInspectionInsertSampleTextButton"
        Me.uiFirstInspectionInsertSampleTextButton.Size = New System.Drawing.Size(138, 24)
        Me.uiFirstInspectionInsertSampleTextButton.TabIndex = 32
        Me.uiFirstInspectionInsertSampleTextButton.Text = "First Inspection Text"
        '
        'uiHistoryInsertSampleTextButton
        '
        Me.uiHistoryInsertSampleTextButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiHistoryInsertSampleTextButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiHistoryInsertSampleTextButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiHistoryInsertSampleTextButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiHistoryInsertSampleTextButton.Appearance.Options.UseBackColor = true
        Me.uiHistoryInsertSampleTextButton.Appearance.Options.UseBorderColor = true
        Me.uiHistoryInsertSampleTextButton.Appearance.Options.UseFont = true
        Me.uiHistoryInsertSampleTextButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiHistoryInsertSampleTextButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiHistoryInsertSampleTextButton.Location = New System.Drawing.Point(744, 474)
        Me.uiHistoryInsertSampleTextButton.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003
        Me.uiHistoryInsertSampleTextButton.Name = "uiHistoryInsertSampleTextButton"
        Me.uiHistoryInsertSampleTextButton.Size = New System.Drawing.Size(138, 24)
        Me.uiHistoryInsertSampleTextButton.TabIndex = 33
        Me.uiHistoryInsertSampleTextButton.Text = "Prior History Text"
        '
        'uiGridControlContacts
        '
        Me.uiGridControlContacts.DataSource = Me.bstblContactInformation
        Me.uiGridControlContacts.EmbeddedNavigator.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiGridControlContacts.EmbeddedNavigator.Appearance.Options.UseFont = true
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.First.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.Last.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.Next.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.NextPage.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.Prev.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.Buttons.PrevPage.Visible = false
        Me.uiGridControlContacts.EmbeddedNavigator.TextStringFormat = ""
        Me.uiGridControlContacts.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiGridControlContacts.Location = New System.Drawing.Point(10, 301)
        Me.uiGridControlContacts.LookAndFeel.SkinName = "Blue"
        Me.uiGridControlContacts.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiGridControlContacts.MainView = Me.uiGridViewContacts
        Me.uiGridControlContacts.Name = "uiGridControlContacts"
        Me.uiGridControlContacts.Size = New System.Drawing.Size(872, 111)
        Me.uiGridControlContacts.TabIndex = 29
        Me.uiGridControlContacts.UseEmbeddedNavigator = true
        Me.uiGridControlContacts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiGridViewContacts})
        '
        'bstblContactInformation
        '
        Me.bstblContactInformation.DataMember = "tblContactInformation"
        Me.bstblContactInformation.DataSource = Me.QTT_InspectionsDataSets
        '
        'uiGridViewContacts
        '
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.ColumnFilterButton.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.Empty.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229,Byte),Integer), CType(CType(234,Byte),Integer), CType(CType(237,Byte),Integer))
        Me.uiGridViewContacts.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewContacts.Appearance.EvenRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.EvenRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FilterCloseButton.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FilterCloseButton.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.FilterCloseButton.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FilterPanel.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FilterPanel.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FixedLine.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewContacts.Appearance.FocusedCell.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FocusedCell.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.FocusedRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FocusedRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.FooterPanel.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.FooterPanel.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.FooterPanel.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupButton.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.GroupButton.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.GroupButton.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupFooter.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.GroupFooter.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.GroupFooter.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewContacts.Appearance.GroupPanel.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.GroupPanel.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.GroupRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.GroupRow.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.GroupRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81,Byte),Integer), CType(CType(109,Byte),Integer), CType(CType(158,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HeaderPanel.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.HeaderPanel.Options.UseBorderColor = true
        Me.uiGridViewContacts.Appearance.HeaderPanel.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128,Byte),Integer), CType(CType(153,Byte),Integer), CType(CType(195,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HideSelectionRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.HideSelectionRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewContacts.Appearance.HorzLine.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiGridViewContacts.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewContacts.Appearance.OddRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.OddRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251,Byte),Integer), CType(CType(250,Byte),Integer), CType(CType(248,Byte),Integer))
        Me.uiGridViewContacts.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.uiGridViewContacts.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57,Byte),Integer), CType(CType(87,Byte),Integer), CType(CType(138,Byte),Integer))
        Me.uiGridViewContacts.Appearance.Preview.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.Preview.Options.UseFont = true
        Me.uiGridViewContacts.Appearance.Preview.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249,Byte),Integer), CType(CType(251,Byte),Integer), CType(CType(252,Byte),Integer))
        Me.uiGridViewContacts.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewContacts.Appearance.Row.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.Row.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.RowSeparator.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107,Byte),Integer), CType(CType(133,Byte),Integer), CType(CType(179,Byte),Integer))
        Me.uiGridViewContacts.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239,Byte),Integer), CType(CType(235,Byte),Integer), CType(CType(220,Byte),Integer))
        Me.uiGridViewContacts.Appearance.SelectedRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.SelectedRow.Options.UseForeColor = true
        Me.uiGridViewContacts.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.uiGridViewContacts.Appearance.TopNewRow.Options.UseBackColor = true
        Me.uiGridViewContacts.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167,Byte),Integer), CType(CType(178,Byte),Integer), CType(CType(185,Byte),Integer))
        Me.uiGridViewContacts.Appearance.VertLine.Options.UseBackColor = true
        Me.uiGridViewContacts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colPrefix, Me.colName, Me.colTitle, Me.colPhone, Me.colEmail, Me.colFax, Me.colFK_Inspection})
        Me.uiGridViewContacts.GridControl = Me.uiGridControlContacts
        Me.uiGridViewContacts.Name = "uiGridViewContacts"
        Me.uiGridViewContacts.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.uiGridViewContacts.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.uiGridViewContacts.OptionsBehavior.AutoSelectAllInEditor = false
        Me.uiGridViewContacts.OptionsCustomization.AllowColumnMoving = false
        Me.uiGridViewContacts.OptionsCustomization.AllowGroup = false
        Me.uiGridViewContacts.OptionsCustomization.AllowQuickHideColumns = false
        Me.uiGridViewContacts.OptionsDetail.AllowZoomDetail = false
        Me.uiGridViewContacts.OptionsDetail.EnableMasterViewMode = false
        Me.uiGridViewContacts.OptionsDetail.ShowDetailTabs = false
        Me.uiGridViewContacts.OptionsDetail.SmartDetailExpand = false
        Me.uiGridViewContacts.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.uiGridViewContacts.OptionsView.EnableAppearanceEvenRow = true
        Me.uiGridViewContacts.OptionsView.EnableAppearanceOddRow = true
        Me.uiGridViewContacts.OptionsView.ShowGroupPanel = false
        '
        'colID
        '
        Me.colID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colID.AppearanceCell.Options.UseFont = true
        Me.colID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colID.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colID.AppearanceHeader.Options.UseFont = true
        Me.colID.AppearanceHeader.Options.UseForeColor = true
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        '
        'colPrefix
        '
        Me.colPrefix.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPrefix.AppearanceCell.Options.UseFont = true
        Me.colPrefix.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPrefix.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colPrefix.AppearanceHeader.Options.UseFont = true
        Me.colPrefix.AppearanceHeader.Options.UseForeColor = true
        Me.colPrefix.Caption = "Salutation"
        Me.colPrefix.FieldName = "Prefix"
        Me.colPrefix.Name = "colPrefix"
        '
        'colName
        '
        Me.colName.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colName.AppearanceCell.Options.UseFont = true
        Me.colName.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colName.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colName.AppearanceHeader.Options.UseFont = true
        Me.colName.AppearanceHeader.Options.UseForeColor = true
        Me.colName.Caption = "Name (Required)"
        Me.colName.FieldName = "Name"
        Me.colName.Name = "colName"
        Me.colName.Visible = true
        Me.colName.VisibleIndex = 0
        '
        'colTitle
        '
        Me.colTitle.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colTitle.AppearanceCell.Options.UseFont = true
        Me.colTitle.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colTitle.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colTitle.AppearanceHeader.Options.UseFont = true
        Me.colTitle.AppearanceHeader.Options.UseForeColor = true
        Me.colTitle.Caption = "Title (Required)"
        Me.colTitle.FieldName = "Title"
        Me.colTitle.Name = "colTitle"
        Me.colTitle.Visible = true
        Me.colTitle.VisibleIndex = 1
        '
        'colPhone
        '
        Me.colPhone.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPhone.AppearanceCell.Options.UseFont = true
        Me.colPhone.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colPhone.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colPhone.AppearanceHeader.Options.UseFont = true
        Me.colPhone.AppearanceHeader.Options.UseForeColor = true
        Me.colPhone.FieldName = "Phone"
        Me.colPhone.Name = "colPhone"
        Me.colPhone.Visible = true
        Me.colPhone.VisibleIndex = 2
        '
        'colEmail
        '
        Me.colEmail.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colEmail.AppearanceCell.Options.UseFont = true
        Me.colEmail.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colEmail.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colEmail.AppearanceHeader.Options.UseFont = true
        Me.colEmail.AppearanceHeader.Options.UseForeColor = true
        Me.colEmail.FieldName = "Email"
        Me.colEmail.Name = "colEmail"
        Me.colEmail.Visible = true
        Me.colEmail.VisibleIndex = 3
        '
        'colFax
        '
        Me.colFax.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFax.AppearanceCell.Options.UseFont = true
        Me.colFax.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFax.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colFax.AppearanceHeader.Options.UseFont = true
        Me.colFax.AppearanceHeader.Options.UseForeColor = true
        Me.colFax.FieldName = "Fax"
        Me.colFax.Name = "colFax"
        Me.colFax.Visible = true
        Me.colFax.VisibleIndex = 4
        '
        'colFK_Inspection
        '
        Me.colFK_Inspection.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFK_Inspection.AppearanceCell.Options.UseFont = true
        Me.colFK_Inspection.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.colFK_Inspection.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colFK_Inspection.AppearanceHeader.Options.UseFont = true
        Me.colFK_Inspection.AppearanceHeader.Options.UseForeColor = true
        Me.colFK_Inspection.FieldName = "FK_Inspection"
        Me.colFK_Inspection.Name = "colFK_Inspection"
        '
        'uiPriorHistoryLabel
        '
        Me.uiPriorHistoryLabel.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.uiPriorHistoryLabel.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPriorHistoryLabel.ForeColor = System.Drawing.Color.GhostWhite
        Me.uiPriorHistoryLabel.Location = New System.Drawing.Point(10, 420)
        Me.uiPriorHistoryLabel.Name = "uiPriorHistoryLabel"
        Me.uiPriorHistoryLabel.Size = New System.Drawing.Size(872, 15)
        Me.uiPriorHistoryLabel.TabIndex = 30
        Me.uiPriorHistoryLabel.Text = "PRIOR HISTORY"
        Me.uiPriorHistoryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label91
        '
        Me.Label91.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label91.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label91.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label91.Location = New System.Drawing.Point(10, 279)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(872, 15)
        Me.Label91.TabIndex = 28
        Me.Label91.Text = "CUSTOMER CONTACT(S)"
        Me.Label91.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label51
        '
        Me.Label51.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label51.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label51.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label51.Location = New System.Drawing.Point(10, 159)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(875, 15)
        Me.Label51.TabIndex = 15
        Me.Label51.Text = "REFORMER INFORMATION"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label40
        '
        Me.Label40.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label40.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label40.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label40.Location = New System.Drawing.Point(10, 50)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(875, 15)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "PLANT INFORMATION"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label36
        '
        Me.Label36.AutoSize = true
        Me.Label36.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label36.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label36.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label36.Location = New System.Drawing.Point(533, 183)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(23, 17)
        Me.Label36.TabIndex = 18
        Me.Label36.Text = "ID:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label25
        '
        Me.Label25.AutoSize = true
        Me.Label25.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label25.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label25.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label25.Location = New System.Drawing.Point(27, 183)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(105, 17)
        Me.Label25.TabIndex = 16
        Me.Label25.Text = "Reformer Name:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiState
        '
        Me.uiState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "State", true))
        Me.uiState.EditValue = ""
        Me.uiState.Location = New System.Drawing.Point(505, 129)
        Me.uiState.Name = "uiState"
        Me.uiState.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiState.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.Appearance.Options.UseBackColor = true
        Me.uiState.Properties.Appearance.Options.UseFont = true
        Me.uiState.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiState.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiState.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiState.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiState.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject20.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject20.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject20.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject20.Options.UseBackColor = true
        SerializableAppearanceObject20.Options.UseBorderColor = true
        Me.uiState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject20, "", Nothing, Nothing, true)})
        Me.uiState.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiState.Properties.MaxLength = 20
        Me.uiState.Properties.PopupFormMinSize = New System.Drawing.Size(70, 0)
        Me.uiState.Size = New System.Drawing.Size(138, 24)
        Me.uiState.TabIndex = 12
        ConditionValidationRule51.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule51.ErrorText = "State/Province cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiState, ConditionValidationRule51)
        '
        'uiCountry
        '
        Me.uiCountry.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "Country", true))
        Me.uiCountry.EditValue = ""
        Me.uiCountry.Location = New System.Drawing.Point(642, 71)
        Me.uiCountry.Name = "uiCountry"
        Me.uiCountry.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCountry.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.Appearance.Options.UseBackColor = true
        Me.uiCountry.Properties.Appearance.Options.UseFont = true
        Me.uiCountry.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCountry.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiCountry.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCountry.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCountry.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject21.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject21.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject21.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject21.Options.UseBackColor = true
        SerializableAppearanceObject21.Options.UseBorderColor = true
        Me.uiCountry.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject21, "", Nothing, Nothing, true)})
        Me.uiCountry.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCountry.Properties.MaxLength = 20
        Me.uiCountry.Properties.PopupFormMinSize = New System.Drawing.Size(100, 0)
        Me.uiCountry.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiCountry.Size = New System.Drawing.Size(243, 24)
        Me.uiCountry.TabIndex = 6
        ConditionValidationRule52.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule52.ErrorText = "Country cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiCountry, ConditionValidationRule52)
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label3.Location = New System.Drawing.Point(580, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Country:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label8.Location = New System.Drawing.Point(660, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(81, 17)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Postal Code:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label7.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label7.Location = New System.Drawing.Point(405, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(94, 17)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "State/Province:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label6.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label6.Location = New System.Drawing.Point(100, 129)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 17)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "City:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label2.Location = New System.Drawing.Point(73, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Address:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiCustomers
        '
        Me.uiCustomers.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "FK_Customer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.uiCustomers.Location = New System.Drawing.Point(138, 15)
        Me.uiCustomers.Name = "uiCustomers"
        Me.uiCustomers.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCustomers.Properties.Appearance.BackColor2 = System.Drawing.Color.White
        Me.uiCustomers.Properties.Appearance.BorderColor = System.Drawing.Color.White
        Me.uiCustomers.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.Appearance.Options.UseBackColor = true
        Me.uiCustomers.Properties.Appearance.Options.UseBorderColor = true
        Me.uiCustomers.Properties.Appearance.Options.UseFont = true
        Me.uiCustomers.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiCustomers.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiCustomers.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiCustomers.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomers.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject22.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject22.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject22.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject22.Options.UseBackColor = true
        SerializableAppearanceObject22.Options.UseBorderColor = true
        Me.uiCustomers.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject22, "", Nothing, Nothing, true)})
        Me.uiCustomers.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCustomers.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 35, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Address", "Address", 58, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("City", "City", 31, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("State", "State", 39, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PostalCode", "Postal Code", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Country", "Country", 55, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Contact", "Contact", 54, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Phone", "Phone", 46, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Fax", "Fax", 29, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Email", "Email", 41, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near)})
        Me.uiCustomers.Properties.DataSource = Me.bsQTT_Customers
        Me.uiCustomers.Properties.DisplayMember = "Name"
        Me.uiCustomers.Properties.DropDownRows = 9
        Me.uiCustomers.Properties.NullText = ""
        Me.uiCustomers.Properties.PopupFormMinSize = New System.Drawing.Size(120, 0)
        Me.uiCustomers.Properties.ShowFooter = false
        Me.uiCustomers.Properties.ShowHeader = false
        Me.uiCustomers.Properties.ValueMember = "ID"
        Me.uiCustomers.Size = New System.Drawing.Size(266, 24)
        Me.uiCustomers.TabIndex = 1
        ConditionValidationRule53.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule53.ErrorText = "Customer cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiCustomers, ConditionValidationRule53)
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label1.Location = New System.Drawing.Point(54, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Plant Name:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiCustomerLabel
        '
        Me.uiCustomerLabel.AutoSize = true
        Me.uiCustomerLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomerLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCustomerLabel.Location = New System.Drawing.Point(65, 15)
        Me.uiCustomerLabel.Name = "uiCustomerLabel"
        Me.uiCustomerLabel.Size = New System.Drawing.Size(67, 17)
        Me.uiCustomerLabel.TabIndex = 0
        Me.uiCustomerLabel.Text = "Customer:"
        Me.uiCustomerLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiReformerName
        '
        Me.uiReformerName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "UnitName", true))
        Me.uiReformerName.Location = New System.Drawing.Point(138, 183)
        Me.uiReformerName.Name = "uiReformerName"
        Me.uiReformerName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerName.Properties.Appearance.Options.UseFont = true
        Me.uiReformerName.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerName.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiReformerName.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerName.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiReformerName.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerName.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiReformerName.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiReformerName.Properties.MaxLength = 50
        Me.uiReformerName.Size = New System.Drawing.Size(383, 24)
        Me.uiReformerName.TabIndex = 17
        ConditionValidationRule54.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule54.ErrorText = "Reformer Name cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiReformerName, ConditionValidationRule54)
        '
        'uiReformerID
        '
        Me.uiReformerID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "UnitNumber", true))
        Me.uiReformerID.Location = New System.Drawing.Point(564, 183)
        Me.uiReformerID.Name = "uiReformerID"
        Me.uiReformerID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerID.Properties.Appearance.Options.UseFont = true
        Me.uiReformerID.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerID.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiReformerID.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerID.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiReformerID.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiReformerID.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiReformerID.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiReformerID.Properties.MaxLength = 15
        Me.uiReformerID.Size = New System.Drawing.Size(126, 24)
        Me.uiReformerID.TabIndex = 19
        ConditionValidationRule55.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule55.ErrorText = "Reformer ID cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiReformerID, ConditionValidationRule55)
        '
        'uiProjectDescriptionTabPage
        '
        Me.uiProjectDescriptionTabPage.Appearance.PageClient.BackColor = System.Drawing.Color.Red
        Me.uiProjectDescriptionTabPage.Appearance.PageClient.Options.UseBackColor = true
        Me.uiProjectDescriptionTabPage.Controls.Add(Me.SplitContainerControl2)
        Me.uiProjectDescriptionTabPage.Name = "uiProjectDescriptionTabPage"
        Me.uiProjectDescriptionTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiProjectDescriptionTabPage.Text = "Tube Description and Identification"
        '
        'uiTubeDetailsTabPage
        '
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeLengthDisplay)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiDegreesMarker)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label131)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiMonthYearLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label112)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiInspectionDetailsRowsTubesLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.GroupBox1)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label14)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label13)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeInspectionLocations)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeDesignToleranceDisplay)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiMinusTubeTolerance)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiPlusTubeTolerance)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeDesignToleranceLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label11)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label22)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiNumberTubesTotal)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiNumberTubesTotalLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label123)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeManufacturer)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeUnitGroup)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label127)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiOperatingHoursLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiThermalCyclesLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label12)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label121)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label117)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeLength)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiLengthLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label109)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeWallThicknessDisplay)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeOuterDiameterDisplay)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeInnerDiameterDisplay)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeWallThickness)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label113)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeOuterDiameter)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label114)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeInnerDiameter)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label115)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeServiceDateLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label119)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeTypeLabel)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.Label122)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeMaterials)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeInspectionAge)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiPressure)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTargetTemp)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiThermalCycles)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiOperatingHours)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeServiceDate)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiTubeNextInspectionDate)
        Me.uiTubeDetailsTabPage.Controls.Add(Me.uiNumberTubesInspected)
        Me.uiTubeDetailsTabPage.Name = "uiTubeDetailsTabPage"
        Me.uiTubeDetailsTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiTubeDetailsTabPage.Text = "Tube Details"
        '
        'uiDegreesMarker
        '
        Me.uiDegreesMarker.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "DegreesMarker", true))
        Me.uiDegreesMarker.EditValue = 0
        Me.uiDegreesMarker.Location = New System.Drawing.Point(301, 118)
        Me.uiDegreesMarker.Name = "uiDegreesMarker"
        Me.uiDegreesMarker.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiDegreesMarker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDegreesMarker.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDegreesMarker.Properties.Appearance.Options.UseBackColor = true
        Me.uiDegreesMarker.Properties.Appearance.Options.UseFont = true
        Me.uiDegreesMarker.Properties.Appearance.Options.UseForeColor = true
        Me.uiDegreesMarker.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDegreesMarker.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiDegreesMarker.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiDegreesMarker.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiDegreesMarker.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiDegreesMarker.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Farenheit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Celsius")})
        Me.uiDegreesMarker.Size = New System.Drawing.Size(170, 28)
        Me.uiDegreesMarker.TabIndex = 9
        '
        'Label131
        '
        Me.Label131.AutoSize = true
        Me.Label131.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label131.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label131.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label131.Location = New System.Drawing.Point(889, 92)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(148, 17)
        Me.Label131.TabIndex = 21
        Me.Label131.Text = "Enter as Year (i.e., 2017)"
        '
        'uiMonthYearLabel
        '
        Me.uiMonthYearLabel.AutoSize = true
        Me.uiMonthYearLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMonthYearLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiMonthYearLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiMonthYearLabel.Location = New System.Drawing.Point(889, 33)
        Me.uiMonthYearLabel.Name = "uiMonthYearLabel"
        Me.uiMonthYearLabel.Size = New System.Drawing.Size(227, 17)
        Me.uiMonthYearLabel.TabIndex = 16
        Me.uiMonthYearLabel.Text = "Enter as MonthYear (i.e., March 2010)"
        '
        'Label112
        '
        Me.Label112.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label112.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label112.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label112.Location = New System.Drawing.Point(633, 394)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(483, 55)
        Me.Label112.TabIndex = 43
        Me.Label112.Text = "If less than 100% coverage is obtained, indicate the amount inspected in inches o"& _ 
    "r millimeters (example: 505 in. (12827 mm) of 528 in. (13411 mm))."&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)
        '
        'uiInspectionDetailsRowsTubesLabel
        '
        Me.uiInspectionDetailsRowsTubesLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionDetailsRowsTubesLabel.ForeColor = System.Drawing.Color.Teal
        Me.uiInspectionDetailsRowsTubesLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiInspectionDetailsRowsTubesLabel.Location = New System.Drawing.Point(275, 488)
        Me.uiInspectionDetailsRowsTubesLabel.Name = "uiInspectionDetailsRowsTubesLabel"
        Me.uiInspectionDetailsRowsTubesLabel.Size = New System.Drawing.Size(340, 17)
        Me.uiInspectionDetailsRowsTubesLabel.TabIndex = 48
        Me.uiInspectionDetailsRowsTubesLabel.Text = "Inspection Details"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.uiTolerancesGroup)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.GroupBox1.Location = New System.Drawing.Point(514, 244)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(164, 78)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Tolerances Provided By:"
        '
        'uiTolerancesGroup
        '
        Me.uiTolerancesGroup.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "ToleranceProvidedBy", true))
        Me.uiTolerancesGroup.EditValue = -1
        Me.uiTolerancesGroup.Location = New System.Drawing.Point(27, 20)
        Me.uiTolerancesGroup.Name = "uiTolerancesGroup"
        Me.uiTolerancesGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiTolerancesGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTolerancesGroup.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTolerancesGroup.Properties.Appearance.Options.UseBackColor = true
        Me.uiTolerancesGroup.Properties.Appearance.Options.UseFont = true
        Me.uiTolerancesGroup.Properties.Appearance.Options.UseForeColor = true
        Me.uiTolerancesGroup.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTolerancesGroup.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiTolerancesGroup.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTolerancesGroup.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiTolerancesGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiTolerancesGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "QIG"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Customer")})
        Me.uiTolerancesGroup.Size = New System.Drawing.Size(113, 50)
        Me.uiTolerancesGroup.TabIndex = 0
        ConditionValidationRule56.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Between
        ConditionValidationRule56.ErrorText = "You must choose who provided the tolerances."
        ConditionValidationRule56.Value1 = "0"
        ConditionValidationRule56.Value2 = "1"
        Me.DxValidationProvider.SetValidationRule(Me.uiTolerancesGroup, ConditionValidationRule56)
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 18!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label14.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label14.Location = New System.Drawing.Point(762, 261)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 32)
        Me.Label14.TabIndex = 35
        Me.Label14.Text = "+"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 18!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label13.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label13.Location = New System.Drawing.Point(877, 261)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(27, 32)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "�"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeDesignToleranceLabel
        '
        Me.uiTubeDesignToleranceLabel.AutoSize = true
        Me.uiTubeDesignToleranceLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeDesignToleranceLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeDesignToleranceLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeDesignToleranceLabel.Location = New System.Drawing.Point(753, 244)
        Me.uiTubeDesignToleranceLabel.Name = "uiTubeDesignToleranceLabel"
        Me.uiTubeDesignToleranceLabel.Size = New System.Drawing.Size(239, 17)
        Me.uiTubeDesignToleranceLabel.TabIndex = 34
        Me.uiTubeDesignToleranceLabel.Text = "Outer Diameter Tube Design Tolerance:"
        Me.uiTubeDesignToleranceLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label11.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label11.Location = New System.Drawing.Point(3, 371)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(1113, 15)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "INSPECTION DETAILS"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.AutoSize = true
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label22.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label22.Location = New System.Drawing.Point(574, 89)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(165, 17)
        Me.Label22.TabIndex = 19
        Me.Label22.Text = "Next Tube Inspection Date:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiNumberTubesTotalLabel
        '
        Me.uiNumberTubesTotalLabel.AutoSize = true
        Me.uiNumberTubesTotalLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiNumberTubesTotalLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiNumberTubesTotalLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiNumberTubesTotalLabel.Location = New System.Drawing.Point(14, 485)
        Me.uiNumberTubesTotalLabel.Name = "uiNumberTubesTotalLabel"
        Me.uiNumberTubesTotalLabel.Size = New System.Drawing.Size(187, 17)
        Me.uiNumberTubesTotalLabel.TabIndex = 46
        Me.uiNumberTubesTotalLabel.Text = "Number of Tubes in Reformer:"
        Me.uiNumberTubesTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label123
        '
        Me.Label123.AutoSize = true
        Me.Label123.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label123.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label123.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label123.Location = New System.Drawing.Point(82, 30)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(122, 17)
        Me.Label123.TabIndex = 1
        Me.Label123.Text = "Tube Manufacturer:"
        Me.Label123.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeManufacturer
        '
        Me.uiTubeManufacturer.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "Manufacturer", true))
        Me.uiTubeManufacturer.EditValue = ""
        Me.uiTubeManufacturer.Location = New System.Drawing.Point(207, 30)
        Me.uiTubeManufacturer.Name = "uiTubeManufacturer"
        Me.uiTubeManufacturer.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiTubeManufacturer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeManufacturer.Properties.Appearance.Options.UseBackColor = true
        Me.uiTubeManufacturer.Properties.Appearance.Options.UseFont = true
        Me.uiTubeManufacturer.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeManufacturer.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeManufacturer.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeManufacturer.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTubeManufacturer.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeManufacturer.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeManufacturer.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeManufacturer.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject23.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject23.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject23.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject23.Options.UseBackColor = true
        SerializableAppearanceObject23.Options.UseBorderColor = true
        Me.uiTubeManufacturer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject23, "", Nothing, Nothing, true)})
        Me.uiTubeManufacturer.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTubeManufacturer.Properties.MaxLength = 128
        Me.uiTubeManufacturer.Size = New System.Drawing.Size(264, 24)
        Me.uiTubeManufacturer.TabIndex = 2
        ConditionValidationRule57.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule57.ErrorText = "Tube Manufacturer cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeManufacturer, ConditionValidationRule57)
        '
        'uiTubeUnitGroup
        '
        Me.uiTubeUnitGroup.Controls.Add(Me.uiLeadingUnits)
        Me.uiTubeUnitGroup.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeUnitGroup.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeUnitGroup.Location = New System.Drawing.Point(577, 144)
        Me.uiTubeUnitGroup.Name = "uiTubeUnitGroup"
        Me.uiTubeUnitGroup.Size = New System.Drawing.Size(283, 49)
        Me.uiTubeUnitGroup.TabIndex = 22
        Me.uiTubeUnitGroup.TabStop = false
        Me.uiTubeUnitGroup.Text = "Units (enter data in imperial units):"
        '
        'uiLeadingUnits
        '
        Me.uiLeadingUnits.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "LeadingUnits", true))
        Me.uiLeadingUnits.EditValue = 0
        Me.uiLeadingUnits.Location = New System.Drawing.Point(7, 17)
        Me.uiLeadingUnits.Name = "uiLeadingUnits"
        Me.uiLeadingUnits.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiLeadingUnits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadingUnits.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiLeadingUnits.Properties.Appearance.Options.UseBackColor = true
        Me.uiLeadingUnits.Properties.Appearance.Options.UseFont = true
        Me.uiLeadingUnits.Properties.Appearance.Options.UseForeColor = true
        Me.uiLeadingUnits.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLeadingUnits.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiLeadingUnits.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiLeadingUnits.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiLeadingUnits.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiLeadingUnits.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Imperial (Metric)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Metric (Imperial)")})
        Me.uiLeadingUnits.Size = New System.Drawing.Size(270, 28)
        Me.uiLeadingUnits.TabIndex = 0
        '
        'Label127
        '
        Me.Label127.AutoSize = true
        Me.Label127.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label127.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label127.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label127.Location = New System.Drawing.Point(38, 395)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(163, 17)
        Me.Label127.TabIndex = 41
        Me.Label127.Text = "Tube Inspection Coverage:"
        Me.Label127.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiOperatingHoursLabel
        '
        Me.uiOperatingHoursLabel.AutoSize = true
        Me.uiOperatingHoursLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingHoursLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiOperatingHoursLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiOperatingHoursLabel.Location = New System.Drawing.Point(31, 176)
        Me.uiOperatingHoursLabel.Name = "uiOperatingHoursLabel"
        Me.uiOperatingHoursLabel.Size = New System.Drawing.Size(170, 17)
        Me.uiOperatingHoursLabel.TabIndex = 12
        Me.uiOperatingHoursLabel.Text = "Estimated Operating Hours:"
        Me.uiOperatingHoursLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiThermalCyclesLabel
        '
        Me.uiThermalCyclesLabel.AutoSize = true
        Me.uiThermalCyclesLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiThermalCyclesLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiThermalCyclesLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiThermalCyclesLabel.Location = New System.Drawing.Point(14, 147)
        Me.uiThermalCyclesLabel.Name = "uiThermalCyclesLabel"
        Me.uiThermalCyclesLabel.Size = New System.Drawing.Size(187, 17)
        Me.uiThermalCyclesLabel.TabIndex = 10
        Me.uiThermalCyclesLabel.Text = "Estimated # of Thermal Cycles:"
        Me.uiThermalCyclesLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label12.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label12.Location = New System.Drawing.Point(13, 118)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(188, 17)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Target Tube Skin Temperature:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label121
        '
        Me.Label121.AutoSize = true
        Me.Label121.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label121.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label121.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label121.Location = New System.Drawing.Point(4, 89)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(199, 17)
        Me.Label121.TabIndex = 5
        Me.Label121.Text = "Internal Pressure at Top of Tube:"
        Me.Label121.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label117
        '
        Me.Label117.AutoSize = true
        Me.Label117.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label117.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label117.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label117.Location = New System.Drawing.Point(594, 59)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(145, 17)
        Me.Label117.TabIndex = 17
        Me.Label117.Text = "Tube Age at Inspection:"
        Me.Label117.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiLengthLabel
        '
        Me.uiLengthLabel.AutoSize = true
        Me.uiLengthLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLengthLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiLengthLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiLengthLabel.Location = New System.Drawing.Point(5, 334)
        Me.uiLengthLabel.Name = "uiLengthLabel"
        Me.uiLengthLabel.Size = New System.Drawing.Size(224, 17)
        Me.uiLengthLabel.TabIndex = 49
        Me.uiLengthLabel.Text = "Overall Tube Design Length (in./mm):"
        Me.uiLengthLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label109
        '
        Me.Label109.AutoSize = true
        Me.Label109.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label109.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label109.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label109.Location = New System.Drawing.Point(27, 455)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(174, 17)
        Me.Label109.TabIndex = 44
        Me.Label109.Text = "Number of Tubes Inspected:"
        Me.Label109.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label113
        '
        Me.Label113.AutoSize = true
        Me.Label113.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label113.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label113.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label113.Location = New System.Drawing.Point(35, 303)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(194, 17)
        Me.Label113.TabIndex = 30
        Me.Label113.Text = "Minimum Sound Wall Thickness:"
        Me.Label113.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label114
        '
        Me.Label114.AutoSize = true
        Me.Label114.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label114.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label114.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label114.Location = New System.Drawing.Point(75, 272)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(154, 17)
        Me.Label114.TabIndex = 27
        Me.Label114.Text = "Nominal Outer Diameter:"
        Me.Label114.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label115
        '
        Me.Label115.AutoSize = true
        Me.Label115.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label115.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label115.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label115.Location = New System.Drawing.Point(79, 241)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(150, 17)
        Me.Label115.TabIndex = 24
        Me.Label115.Text = "Nominal Inner Diameter:"
        Me.Label115.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeServiceDateLabel
        '
        Me.uiTubeServiceDateLabel.AutoSize = true
        Me.uiTubeServiceDateLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeServiceDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeServiceDateLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeServiceDateLabel.Location = New System.Drawing.Point(588, 30)
        Me.uiTubeServiceDateLabel.Name = "uiTubeServiceDateLabel"
        Me.uiTubeServiceDateLabel.Size = New System.Drawing.Size(151, 17)
        Me.uiTubeServiceDateLabel.TabIndex = 14
        Me.uiTubeServiceDateLabel.Text = "Tube Installation Date(s):"
        Me.uiTubeServiceDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label119
        '
        Me.Label119.AutoSize = true
        Me.Label119.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label119.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label119.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label119.Location = New System.Drawing.Point(112, 59)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(92, 17)
        Me.Label119.TabIndex = 3
        Me.Label119.Text = "Tube Material:"
        Me.Label119.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeTypeLabel
        '
        Me.uiTubeTypeLabel.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeTypeLabel.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeTypeLabel.ForeColor = System.Drawing.Color.GhostWhite
        Me.uiTubeTypeLabel.Location = New System.Drawing.Point(3, 8)
        Me.uiTubeTypeLabel.Name = "uiTubeTypeLabel"
        Me.uiTubeTypeLabel.Size = New System.Drawing.Size(1113, 14)
        Me.uiTubeTypeLabel.TabIndex = 0
        Me.uiTubeTypeLabel.Text = "REFORMER TUBE DETAILS"
        Me.uiTubeTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label122
        '
        Me.Label122.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label122.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label122.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label122.Location = New System.Drawing.Point(3, 218)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(1113, 15)
        Me.Label122.TabIndex = 23
        Me.Label122.Text = "TUBE DIMENSIONS AND TOLERANCES"
        Me.Label122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiTubeMaterials
        '
        Me.uiTubeMaterials.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "Material", true))
        Me.uiTubeMaterials.Location = New System.Drawing.Point(207, 59)
        Me.uiTubeMaterials.Name = "uiTubeMaterials"
        Me.uiTubeMaterials.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiTubeMaterials.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeMaterials.Properties.Appearance.Options.UseBackColor = true
        Me.uiTubeMaterials.Properties.Appearance.Options.UseFont = true
        Me.uiTubeMaterials.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeMaterials.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeMaterials.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeMaterials.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTubeMaterials.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeMaterials.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeMaterials.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeMaterials.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject24.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject24.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject24.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject24.Options.UseBackColor = true
        SerializableAppearanceObject24.Options.UseBorderColor = true
        Me.uiTubeMaterials.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject24, "", Nothing, Nothing, true)})
        Me.uiTubeMaterials.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTubeMaterials.Properties.MaxLength = 64
        Me.uiTubeMaterials.Size = New System.Drawing.Size(166, 24)
        Me.uiTubeMaterials.TabIndex = 4
        ConditionValidationRule58.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule58.ErrorText = "Tube Material cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeMaterials, ConditionValidationRule58)
        '
        'uiTubeInspectionAge
        '
        Me.uiTubeInspectionAge.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "AgeAtInspection", true))
        Me.uiTubeInspectionAge.Location = New System.Drawing.Point(743, 59)
        Me.uiTubeInspectionAge.Name = "uiTubeInspectionAge"
        Me.uiTubeInspectionAge.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInspectionAge.Properties.Appearance.Options.UseFont = true
        Me.uiTubeInspectionAge.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInspectionAge.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeInspectionAge.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInspectionAge.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeInspectionAge.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeInspectionAge.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTubeInspectionAge.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiTubeInspectionAge.Properties.MaxLength = 128
        Me.uiTubeInspectionAge.Size = New System.Drawing.Size(143, 24)
        Me.uiTubeInspectionAge.TabIndex = 18
        ConditionValidationRule59.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule59.ErrorText = "Tube Age at Inspection cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeInspectionAge, ConditionValidationRule59)
        '
        'uiPressure
        '
        Me.uiPressure.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "Pressure", true))
        Me.uiPressure.Location = New System.Drawing.Point(207, 89)
        Me.uiPressure.Name = "uiPressure"
        Me.uiPressure.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPressure.Properties.Appearance.Options.UseFont = true
        Me.uiPressure.Properties.Appearance.Options.UseTextOptions = true
        Me.uiPressure.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiPressure.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPressure.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiPressure.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPressure.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiPressure.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiPressure.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiPressure.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiPressure.Properties.MaxLength = 128
        Me.uiPressure.Size = New System.Drawing.Size(83, 24)
        Me.uiPressure.TabIndex = 6
        ConditionValidationRule60.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule60.ErrorText = "Internal Pressure cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiPressure, ConditionValidationRule60)
        '
        'uiTargetTemp
        '
        Me.uiTargetTemp.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "TargetTemp", true))
        Me.uiTargetTemp.Location = New System.Drawing.Point(207, 118)
        Me.uiTargetTemp.Name = "uiTargetTemp"
        Me.uiTargetTemp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTargetTemp.Properties.Appearance.Options.UseFont = true
        Me.uiTargetTemp.Properties.Appearance.Options.UseTextOptions = true
        Me.uiTargetTemp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiTargetTemp.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTargetTemp.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTargetTemp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTargetTemp.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTargetTemp.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTargetTemp.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiTargetTemp.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiTargetTemp.Properties.MaxLength = 64
        Me.uiTargetTemp.Size = New System.Drawing.Size(83, 24)
        Me.uiTargetTemp.TabIndex = 8
        ConditionValidationRule61.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule61.ErrorText = "Target Temperature cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTargetTemp, ConditionValidationRule61)
        '
        'uiThermalCycles
        '
        Me.uiThermalCycles.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "NumberThermalCycles", true))
        Me.uiThermalCycles.Location = New System.Drawing.Point(207, 147)
        Me.uiThermalCycles.Name = "uiThermalCycles"
        Me.uiThermalCycles.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiThermalCycles.Properties.Appearance.Options.UseFont = true
        Me.uiThermalCycles.Properties.Appearance.Options.UseTextOptions = true
        Me.uiThermalCycles.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiThermalCycles.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiThermalCycles.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiThermalCycles.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiThermalCycles.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiThermalCycles.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiThermalCycles.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiThermalCycles.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiThermalCycles.Properties.MaxLength = 128
        Me.uiThermalCycles.Size = New System.Drawing.Size(83, 24)
        Me.uiThermalCycles.TabIndex = 11
        ConditionValidationRule62.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule62.ErrorText = "Estimated # of Thermal Cycles cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiThermalCycles, ConditionValidationRule62)
        '
        'uiOperatingHours
        '
        Me.uiOperatingHours.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_PipeTubeItems, "OperatingHours", true))
        Me.uiOperatingHours.Location = New System.Drawing.Point(207, 176)
        Me.uiOperatingHours.Name = "uiOperatingHours"
        Me.uiOperatingHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingHours.Properties.Appearance.Options.UseFont = true
        Me.uiOperatingHours.Properties.Appearance.Options.UseTextOptions = true
        Me.uiOperatingHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiOperatingHours.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingHours.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOperatingHours.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingHours.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiOperatingHours.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOperatingHours.Properties.AppearanceReadOnly.Options.UseFont = true
        Me.uiOperatingHours.Properties.Items.AddRange(New Object() {"Not Supplied"})
        Me.uiOperatingHours.Properties.MaxLength = 128
        Me.uiOperatingHours.Size = New System.Drawing.Size(83, 24)
        Me.uiOperatingHours.TabIndex = 13
        ConditionValidationRule63.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule63.ErrorText = "Estimated Operating Hours cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiOperatingHours, ConditionValidationRule63)
        '
        'uiExecutiveSummaryTabPage
        '
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiCracking)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiCrackingLabel)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label108)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiOperatingPeriod)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label146)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label138)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiMaxGrowthLocation)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label139)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label140)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label141)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label142)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiDiaGrowthMaxPercent)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiDiaGrowthTubeID)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiDiaGrowthRowID)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiOverallMaxGrowthLocationLabel)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label92)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiYellowTubeCount)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label50)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiExecSummaryGreen)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiExecSummaryYellow)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiExecSummaryRed)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.GroupBox3)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label39)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label38)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label35)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label26)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiBulgingWorstPercent)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiWorstTubeIDTube)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiWorstTubeIDRow)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiBulgingTubeCount)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label24)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label23)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiSwellingOver3PercentCount)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label21)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiOverallTubesInspected)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label20)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiRedTubeCount)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label19)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label18)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label17)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label15)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiSummaryEditorShowHideButton)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiSummaryEditor)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiSummaryEditorButton)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.Label128)
        Me.uiExecutiveSummaryTabPage.Controls.Add(Me.uiOverallMaxGrowthLocation)
        Me.uiExecutiveSummaryTabPage.Name = "uiExecutiveSummaryTabPage"
        Me.uiExecutiveSummaryTabPage.PageVisible = false
        Me.uiExecutiveSummaryTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiExecutiveSummaryTabPage.Text = "Executive Summary"
        '
        'uiCrackingLabel
        '
        Me.uiCrackingLabel.AutoSize = true
        Me.uiCrackingLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCrackingLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCrackingLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiCrackingLabel.Location = New System.Drawing.Point(265, 230)
        Me.uiCrackingLabel.Name = "uiCrackingLabel"
        Me.uiCrackingLabel.Size = New System.Drawing.Size(195, 17)
        Me.uiCrackingLabel.TabIndex = 23
        Me.uiCrackingLabel.Text = "Number of Tubes with Cracking:"
        Me.uiCrackingLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiCrackingLabel.Visible = false
        '
        'Label108
        '
        Me.Label108.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label108.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label108.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label108.Location = New System.Drawing.Point(6, 344)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(535, 15)
        Me.Label108.TabIndex = 34
        Me.Label108.Text = "OVERALL GROWTH PATTERN"
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label146
        '
        Me.Label146.AutoSize = true
        Me.Label146.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label146.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label146.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label146.Location = New System.Drawing.Point(249, 61)
        Me.Label146.Name = "Label146"
        Me.Label146.Size = New System.Drawing.Size(213, 17)
        Me.Label146.TabIndex = 7
        Me.Label146.Text = "Duration of Next Operating Period:"
        Me.Label146.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label138
        '
        Me.Label138.AutoSize = true
        Me.Label138.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label138.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label138.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label138.Location = New System.Drawing.Point(204, 291)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(132, 17)
        Me.Label138.TabIndex = 32
        Me.Label138.Text = "Max Growth Location"
        Me.Label138.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'uiMaxGrowthLocation
        '
        Me.uiMaxGrowthLocation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "MaxGrowthLocation", true))
        Me.uiMaxGrowthLocation.Location = New System.Drawing.Point(204, 311)
        Me.uiMaxGrowthLocation.Name = "uiMaxGrowthLocation"
        Me.uiMaxGrowthLocation.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiMaxGrowthLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMaxGrowthLocation.Properties.Appearance.Options.UseBackColor = true
        Me.uiMaxGrowthLocation.Properties.Appearance.Options.UseFont = true
        Me.uiMaxGrowthLocation.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMaxGrowthLocation.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiMaxGrowthLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMaxGrowthLocation.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiMaxGrowthLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMaxGrowthLocation.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiMaxGrowthLocation.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiMaxGrowthLocation.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject25.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject25.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject25.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject25.Options.UseBackColor = true
        SerializableAppearanceObject25.Options.UseBorderColor = true
        Me.uiMaxGrowthLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject25, "", Nothing, Nothing, true)})
        Me.uiMaxGrowthLocation.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiMaxGrowthLocation.Properties.Items.AddRange(New Object() {"at the top of the tube at the roof refractory region", "in the top third of the tube", "in the middle of the tube", "in the bottom third of the tube", "at the bottom of the tube at the tunnel region"})
        Me.uiMaxGrowthLocation.Properties.MaxLength = 64
        Me.uiMaxGrowthLocation.Size = New System.Drawing.Size(336, 24)
        Me.uiMaxGrowthLocation.TabIndex = 33
        ConditionValidationRule64.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule64.ErrorText = "Max Growth Location cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiMaxGrowthLocation, ConditionValidationRule64)
        '
        'Label139
        '
        Me.Label139.AutoSize = true
        Me.Label139.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label139.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label139.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label139.Location = New System.Drawing.Point(32, 275)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(88, 15)
        Me.Label139.TabIndex = 25
        Me.Label139.Text = "Worst Tube ID"
        Me.Label139.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label140
        '
        Me.Label140.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label140.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label140.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label140.Location = New System.Drawing.Point(139, 262)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(61, 46)
        Me.Label140.TabIndex = 30
        Me.Label140.Text = "Max % of Growth in Tube ID"
        Me.Label140.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label141
        '
        Me.Label141.AutoSize = true
        Me.Label141.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label141.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label141.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label141.Location = New System.Drawing.Point(87, 293)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(34, 15)
        Me.Label141.TabIndex = 28
        Me.Label141.Text = "Tube"
        Me.Label141.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label142
        '
        Me.Label142.AutoSize = true
        Me.Label142.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label142.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label142.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label142.Location = New System.Drawing.Point(24, 293)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(30, 15)
        Me.Label142.TabIndex = 26
        Me.Label142.Text = "Row"
        Me.Label142.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'uiOverallMaxGrowthLocationLabel
        '
        Me.uiOverallMaxGrowthLocationLabel.AutoSize = true
        Me.uiOverallMaxGrowthLocationLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocationLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiOverallMaxGrowthLocationLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiOverallMaxGrowthLocationLabel.Location = New System.Drawing.Point(203, 373)
        Me.uiOverallMaxGrowthLocationLabel.Name = "uiOverallMaxGrowthLocationLabel"
        Me.uiOverallMaxGrowthLocationLabel.Size = New System.Drawing.Size(313, 17)
        Me.uiOverallMaxGrowthLocationLabel.TabIndex = 37
        Me.uiOverallMaxGrowthLocationLabel.Text = "...of the overall growth in the reformer was located..."
        Me.uiOverallMaxGrowthLocationLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label92
        '
        Me.Label92.AutoSize = true
        Me.Label92.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label92.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label92.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label92.Location = New System.Drawing.Point(60, 138)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(70, 15)
        Me.Label92.TabIndex = 10
        Me.Label92.Text = "Tube Count"
        Me.Label92.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label50
        '
        Me.Label50.AutoSize = true
        Me.Label50.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label50.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label50.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label50.Location = New System.Drawing.Point(40, 61)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(119, 17)
        Me.Label50.TabIndex = 3
        Me.Label50.Text = "Yellow Tube Count:"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiExecSummaryGreen
        '
        Me.uiExecSummaryGreen.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "L1Green", true))
        Me.uiExecSummaryGreen.Location = New System.Drawing.Point(17, 522)
        Me.uiExecSummaryGreen.Name = "uiExecSummaryGreen"
        Me.uiExecSummaryGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiExecSummaryGreen.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiExecSummaryGreen.Properties.Appearance.Options.UseFont = true
        Me.uiExecSummaryGreen.Properties.Appearance.Options.UseForeColor = true
        Me.uiExecSummaryGreen.Properties.Caption = "Green"
        Me.uiExecSummaryGreen.Size = New System.Drawing.Size(143, 22)
        Me.uiExecSummaryGreen.TabIndex = 42
        '
        'uiExecSummaryYellow
        '
        Me.uiExecSummaryYellow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "L1Yellow", true))
        Me.uiExecSummaryYellow.Location = New System.Drawing.Point(17, 496)
        Me.uiExecSummaryYellow.Name = "uiExecSummaryYellow"
        Me.uiExecSummaryYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiExecSummaryYellow.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiExecSummaryYellow.Properties.Appearance.Options.UseFont = true
        Me.uiExecSummaryYellow.Properties.Appearance.Options.UseForeColor = true
        Me.uiExecSummaryYellow.Properties.Caption = "Yellow"
        Me.uiExecSummaryYellow.Size = New System.Drawing.Size(143, 22)
        Me.uiExecSummaryYellow.TabIndex = 41
        '
        'uiExecSummaryRed
        '
        Me.uiExecSummaryRed.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "L1Red", true))
        Me.uiExecSummaryRed.Location = New System.Drawing.Point(17, 471)
        Me.uiExecSummaryRed.Name = "uiExecSummaryRed"
        Me.uiExecSummaryRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiExecSummaryRed.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiExecSummaryRed.Properties.Appearance.Options.UseFont = true
        Me.uiExecSummaryRed.Properties.Appearance.Options.UseForeColor = true
        Me.uiExecSummaryRed.Properties.Caption = "Red"
        Me.uiExecSummaryRed.Size = New System.Drawing.Size(143, 22)
        Me.uiExecSummaryRed.TabIndex = 40
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.uiOverallMaxGrowth)
        Me.GroupBox3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.GroupBox3.Location = New System.Drawing.Point(6, 359)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(192, 86)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = false
        '
        'uiOverallMaxGrowth
        '
        Me.uiOverallMaxGrowth.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "OverallMaxGrowth", true))
        Me.uiOverallMaxGrowth.EditValue = false
        Me.uiOverallMaxGrowth.Location = New System.Drawing.Point(6, 9)
        Me.uiOverallMaxGrowth.Name = "uiOverallMaxGrowth"
        Me.uiOverallMaxGrowth.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.uiOverallMaxGrowth.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowth.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiOverallMaxGrowth.Properties.Appearance.Options.UseBackColor = true
        Me.uiOverallMaxGrowth.Properties.Appearance.Options.UseFont = true
        Me.uiOverallMaxGrowth.Properties.Appearance.Options.UseForeColor = true
        Me.uiOverallMaxGrowth.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowth.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.uiOverallMaxGrowth.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOverallMaxGrowth.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.uiOverallMaxGrowth.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.uiOverallMaxGrowth.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "A majority..."), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "All..."), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "There was no pattern.")})
        Me.uiOverallMaxGrowth.Size = New System.Drawing.Size(180, 71)
        Me.uiOverallMaxGrowth.TabIndex = 36
        ConditionValidationRule65.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.Between
        ConditionValidationRule65.ErrorText = "You must select an Overall Growth Pattern."
        ConditionValidationRule65.Value1 = 0
        ConditionValidationRule65.Value2 = 2
        Me.DxValidationProvider.SetValidationRule(Me.uiOverallMaxGrowth, ConditionValidationRule65)
        '
        'Label39
        '
        Me.Label39.AutoSize = true
        Me.Label39.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label39.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label39.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label39.Location = New System.Drawing.Point(178, 120)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(88, 15)
        Me.Label39.TabIndex = 12
        Me.Label39.Text = "Worst Tube ID"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label38
        '
        Me.Label38.AutoSize = true
        Me.Label38.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label38.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label38.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label38.Location = New System.Drawing.Point(293, 138)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(51, 15)
        Me.Label38.TabIndex = 17
        Me.Label38.Text = "Worst %"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label35
        '
        Me.Label35.AutoSize = true
        Me.Label35.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label35.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label35.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label35.Location = New System.Drawing.Point(234, 138)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(34, 15)
        Me.Label35.TabIndex = 15
        Me.Label35.Text = "Tube"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label26
        '
        Me.Label26.AutoSize = true
        Me.Label26.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label26.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label26.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label26.Location = New System.Drawing.Point(170, 138)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(30, 15)
        Me.Label26.TabIndex = 13
        Me.Label26.Text = "Row"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label24
        '
        Me.Label24.AutoSize = true
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label24.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label24.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label24.Location = New System.Drawing.Point(9, 155)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(54, 17)
        Me.Label24.TabIndex = 9
        Me.Label24.Text = "Bulging:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label23
        '
        Me.Label23.AutoSize = true
        Me.Label23.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label23.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label23.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label23.Location = New System.Drawing.Point(182, 210)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(70, 15)
        Me.Label23.TabIndex = 21
        Me.Label23.Text = "Tube Count"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.AutoSize = true
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label21.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label21.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label21.Location = New System.Drawing.Point(6, 230)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(176, 17)
        Me.Label21.TabIndex = 20
        Me.Label21.Text = "Diametrical Growth >= 3.0%:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.AutoSize = true
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label20.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label20.Location = New System.Drawing.Point(311, 32)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(151, 17)
        Me.Label20.TabIndex = 5
        Me.Label20.Text = "Overall Tubes Inspected:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = true
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label19.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label19.Location = New System.Drawing.Point(54, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(105, 17)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Red Tube Count:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label18.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label18.Location = New System.Drawing.Point(6, 449)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(534, 15)
        Me.Label18.TabIndex = 39
        Me.Label18.Text = "LEVEL 1 ASSESSMENT PARAGRAPH"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label17.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label17.Location = New System.Drawing.Point(6, 190)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(535, 15)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "DIAMETRICAL GROWTH"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label15.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label15.Location = New System.Drawing.Point(6, 94)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(535, 15)
        Me.Label15.TabIndex = 9
        Me.Label15.Text = "DEFORMATIONS"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiSummaryEditorShowHideButton
        '
        Me.uiSummaryEditorShowHideButton.Image = CType(resources.GetObject("uiSummaryEditorShowHideButton.Image"),System.Drawing.Image)
        Me.uiSummaryEditorShowHideButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiSummaryEditorShowHideButton.Location = New System.Drawing.Point(512, 522)
        Me.uiSummaryEditorShowHideButton.Name = "uiSummaryEditorShowHideButton"
        Me.uiSummaryEditorShowHideButton.Size = New System.Drawing.Size(27, 27)
        Me.uiSummaryEditorShowHideButton.TabIndex = 44
        '
        'uiSummaryEditorButton
        '
        Me.uiSummaryEditorButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiSummaryEditorButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiSummaryEditorButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiSummaryEditorButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSummaryEditorButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiSummaryEditorButton.Appearance.Options.UseBackColor = true
        Me.uiSummaryEditorButton.Appearance.Options.UseBorderColor = true
        Me.uiSummaryEditorButton.Appearance.Options.UseFont = true
        Me.uiSummaryEditorButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiSummaryEditorButton.Location = New System.Drawing.Point(381, 522)
        Me.uiSummaryEditorButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiSummaryEditorButton.Name = "uiSummaryEditorButton"
        Me.uiSummaryEditorButton.Size = New System.Drawing.Size(123, 32)
        Me.uiSummaryEditorButton.TabIndex = 43
        Me.uiSummaryEditorButton.Text = "Generate Summary"
        '
        'Label128
        '
        Me.Label128.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label128.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label128.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label128.Location = New System.Drawing.Point(6, 8)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(535, 15)
        Me.Label128.TabIndex = 0
        Me.Label128.Text = "LEVEL 1 ASSESSMENT"
        Me.Label128.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiOverallMaxGrowthLocation
        '
        Me.uiOverallMaxGrowthLocation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "OverallMaxGrowthLocation", true))
        Me.uiOverallMaxGrowthLocation.Location = New System.Drawing.Point(203, 393)
        Me.uiOverallMaxGrowthLocation.Name = "uiOverallMaxGrowthLocation"
        Me.uiOverallMaxGrowthLocation.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiOverallMaxGrowthLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocation.Properties.Appearance.Options.UseBackColor = true
        Me.uiOverallMaxGrowthLocation.Properties.Appearance.Options.UseFont = true
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiOverallMaxGrowthLocation.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject26.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject26.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject26.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject26.Options.UseBackColor = true
        SerializableAppearanceObject26.Options.UseBorderColor = true
        Me.uiOverallMaxGrowthLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject26, "", Nothing, Nothing, true)})
        Me.uiOverallMaxGrowthLocation.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiOverallMaxGrowthLocation.Properties.Items.AddRange(New Object() {"at the top of the tube at the roof refractory region", "in the top third of the tube", "in the middle of the tube", "in the bottom third of the tube", "at the bottom of the tube at the tunnel region"})
        Me.uiOverallMaxGrowthLocation.Properties.MaxLength = 64
        Me.uiOverallMaxGrowthLocation.Size = New System.Drawing.Size(337, 24)
        Me.uiOverallMaxGrowthLocation.TabIndex = 38
        ConditionValidationRule66.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule66.ErrorText = "Overal Max Growth Location cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiOverallMaxGrowthLocation, ConditionValidationRule66)
        '
        'uiInspectionSummaryTabPage
        '
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label111)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiRowNumberLabel)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiWorstFlawRowNumber)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label110)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiWorstFlawTubeNumber)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiNumberTubesWithFlaws)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label104)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label105)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label106)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label107)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiNewTubeCount4)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiTubeToleranceLabel4)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiNewTubeCount3)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiTubeToleranceLabel3)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiNewTubeCount2)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiTubeToleranceLabel2)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiNewTubeCount1)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiTubeToleranceLabel1)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label118)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label120)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label132)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.Label136)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiInspectionSummaryEditor)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiInspectionShowHideButton)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiInspectionSummaryButton)
        Me.uiInspectionSummaryTabPage.Controls.Add(Me.uiTubeFlawLocation)
        Me.uiInspectionSummaryTabPage.Name = "uiInspectionSummaryTabPage"
        Me.uiInspectionSummaryTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiInspectionSummaryTabPage.Text = "Inspection Summary"
        '
        'Label111
        '
        Me.Label111.AutoSize = true
        Me.Label111.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label111.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label111.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label111.Location = New System.Drawing.Point(107, 214)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(77, 17)
        Me.Label111.TabIndex = 16
        Me.Label111.Text = "Worst Flaw"
        Me.Label111.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiRowNumberLabel
        '
        Me.uiRowNumberLabel.AutoSize = true
        Me.uiRowNumberLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRowNumberLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiRowNumberLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiRowNumberLabel.Location = New System.Drawing.Point(260, 237)
        Me.uiRowNumberLabel.Name = "uiRowNumberLabel"
        Me.uiRowNumberLabel.Size = New System.Drawing.Size(52, 17)
        Me.uiRowNumberLabel.TabIndex = 19
        Me.uiRowNumberLabel.Text = "Row ID:"
        Me.uiRowNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label110
        '
        Me.Label110.AutoSize = true
        Me.Label110.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label110.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label110.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label110.Location = New System.Drawing.Point(95, 267)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(89, 17)
        Me.Label110.TabIndex = 21
        Me.Label110.Text = "Flaw Location:"
        Me.Label110.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label104
        '
        Me.Label104.AutoSize = true
        Me.Label104.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label104.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label104.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label104.Location = New System.Drawing.Point(8, 124)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(78, 17)
        Me.Label104.TabIndex = 10
        Me.Label104.Text = "Tube Count:"
        Me.Label104.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label105
        '
        Me.Label105.AutoSize = true
        Me.Label105.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label105.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label105.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label105.Location = New System.Drawing.Point(8, 95)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(78, 17)
        Me.Label105.TabIndex = 7
        Me.Label105.Text = "Tube Count:"
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label106
        '
        Me.Label106.AutoSize = true
        Me.Label106.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label106.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label106.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label106.Location = New System.Drawing.Point(8, 66)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(78, 17)
        Me.Label106.TabIndex = 4
        Me.Label106.Text = "Tube Count:"
        Me.Label106.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label107
        '
        Me.Label107.AutoSize = true
        Me.Label107.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label107.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label107.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label107.Location = New System.Drawing.Point(8, 37)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(78, 17)
        Me.Label107.TabIndex = 1
        Me.Label107.Text = "Tube Count:"
        Me.Label107.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeToleranceLabel4
        '
        Me.uiTubeToleranceLabel4.AutoSize = true
        Me.uiTubeToleranceLabel4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeToleranceLabel4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeToleranceLabel4.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeToleranceLabel4.Location = New System.Drawing.Point(152, 128)
        Me.uiTubeToleranceLabel4.Name = "uiTubeToleranceLabel4"
        Me.uiTubeToleranceLabel4.Size = New System.Drawing.Size(427, 17)
        Me.uiTubeToleranceLabel4.TabIndex = 12
        Me.uiTubeToleranceLabel4.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
        Me.uiTubeToleranceLabel4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeToleranceLabel3
        '
        Me.uiTubeToleranceLabel3.AutoSize = true
        Me.uiTubeToleranceLabel3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeToleranceLabel3.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeToleranceLabel3.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeToleranceLabel3.Location = New System.Drawing.Point(152, 99)
        Me.uiTubeToleranceLabel3.Name = "uiTubeToleranceLabel3"
        Me.uiTubeToleranceLabel3.Size = New System.Drawing.Size(427, 17)
        Me.uiTubeToleranceLabel3.TabIndex = 9
        Me.uiTubeToleranceLabel3.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
        Me.uiTubeToleranceLabel3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeToleranceLabel2
        '
        Me.uiTubeToleranceLabel2.AutoSize = true
        Me.uiTubeToleranceLabel2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeToleranceLabel2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeToleranceLabel2.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeToleranceLabel2.Location = New System.Drawing.Point(152, 70)
        Me.uiTubeToleranceLabel2.Name = "uiTubeToleranceLabel2"
        Me.uiTubeToleranceLabel2.Size = New System.Drawing.Size(427, 17)
        Me.uiTubeToleranceLabel2.TabIndex = 6
        Me.uiTubeToleranceLabel2.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
        Me.uiTubeToleranceLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeToleranceLabel1
        '
        Me.uiTubeToleranceLabel1.AutoSize = true
        Me.uiTubeToleranceLabel1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeToleranceLabel1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiTubeToleranceLabel1.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiTubeToleranceLabel1.Location = New System.Drawing.Point(152, 41)
        Me.uiTubeToleranceLabel1.Name = "uiTubeToleranceLabel1"
        Me.uiTubeToleranceLabel1.Size = New System.Drawing.Size(427, 17)
        Me.uiTubeToleranceLabel1.TabIndex = 3
        Me.uiTubeToleranceLabel1.Text = "You must set tube dimensions and tolerances on the ""Tube Details"" tab!"
        Me.uiTubeToleranceLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label118
        '
        Me.Label118.AutoSize = true
        Me.Label118.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label118.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label118.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label118.Location = New System.Drawing.Point(128, 237)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(56, 17)
        Me.Label118.TabIndex = 17
        Me.Label118.Text = "Tube ID:"
        Me.Label118.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label120
        '
        Me.Label120.AutoSize = true
        Me.Label120.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label120.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label120.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label120.Location = New System.Drawing.Point(8, 186)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(176, 17)
        Me.Label120.TabIndex = 14
        Me.Label120.Text = "Number of Tubes with Flaws:"
        Me.Label120.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label132
        '
        Me.Label132.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label132.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label132.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label132.Location = New System.Drawing.Point(6, 10)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(664, 15)
        Me.Label132.TabIndex = 0
        Me.Label132.Text = "TUBE TOLERANCE RESULTS"
        Me.Label132.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label136
        '
        Me.Label136.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label136.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label136.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label136.Location = New System.Drawing.Point(6, 161)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(664, 15)
        Me.Label136.TabIndex = 13
        Me.Label136.Text = "TUBE FLAWS"
        Me.Label136.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiInspectionShowHideButton
        '
        Me.uiInspectionShowHideButton.Image = CType(resources.GetObject("uiInspectionShowHideButton.Image"),System.Drawing.Image)
        Me.uiInspectionShowHideButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiInspectionShowHideButton.Location = New System.Drawing.Point(637, 349)
        Me.uiInspectionShowHideButton.Name = "uiInspectionShowHideButton"
        Me.uiInspectionShowHideButton.Size = New System.Drawing.Size(27, 27)
        Me.uiInspectionShowHideButton.TabIndex = 24
        '
        'uiInspectionSummaryButton
        '
        Me.uiInspectionSummaryButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiInspectionSummaryButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiInspectionSummaryButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiInspectionSummaryButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionSummaryButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiInspectionSummaryButton.Appearance.Options.UseBackColor = true
        Me.uiInspectionSummaryButton.Appearance.Options.UseBorderColor = true
        Me.uiInspectionSummaryButton.Appearance.Options.UseFont = true
        Me.uiInspectionSummaryButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiInspectionSummaryButton.Location = New System.Drawing.Point(506, 349)
        Me.uiInspectionSummaryButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiInspectionSummaryButton.Name = "uiInspectionSummaryButton"
        Me.uiInspectionSummaryButton.Size = New System.Drawing.Size(123, 32)
        Me.uiInspectionSummaryButton.TabIndex = 23
        Me.uiInspectionSummaryButton.Text = "Generate Summary"
        '
        'uiTubeFlawLocation
        '
        Me.uiTubeFlawLocation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_NewSummary, "FlawLocation", true))
        Me.uiTubeFlawLocation.Location = New System.Drawing.Point(188, 267)
        Me.uiTubeFlawLocation.Name = "uiTubeFlawLocation"
        Me.uiTubeFlawLocation.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiTubeFlawLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawLocation.Properties.Appearance.Options.UseBackColor = true
        Me.uiTubeFlawLocation.Properties.Appearance.Options.UseFont = true
        Me.uiTubeFlawLocation.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawLocation.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeFlawLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawLocation.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTubeFlawLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawLocation.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeFlawLocation.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawLocation.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject27.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject27.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject27.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject27.Options.UseBackColor = true
        SerializableAppearanceObject27.Options.UseBorderColor = true
        Me.uiTubeFlawLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject27, "", Nothing, Nothing, true)})
        Me.uiTubeFlawLocation.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTubeFlawLocation.Properties.Items.AddRange(New Object() {"at the first circumferential weld from the bottom of the tube", "at the second circumferential weld from the bottom of the tube", "at the third circumferential weld from the bottom of the tube", "at the fourth circumferential weld from the bottom of the tube", "at the fifth circumferential weld from the bottom of the tube", "at the first circumferential weld from the bottom of the tube", "in the first tube segment from the bottom of the tube", "in the second tube segment from the bottom of the tube", "in the third tube segment from the bottom of the tube", "in the fourth tube segment from the bottom of the tube", "in the fifth tube segment from the bottom of the tube"})
        Me.uiTubeFlawLocation.Properties.MaxLength = 64
        Me.uiTubeFlawLocation.Size = New System.Drawing.Size(476, 24)
        Me.uiTubeFlawLocation.TabIndex = 22
        ConditionValidationRule67.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule67.ErrorText = "Flaw Location cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeFlawLocation, ConditionValidationRule67)
        '
        'uiDetailedInspectionTabPage
        '
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryTabControl)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthTotalLabel)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiLevel1Label)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label116)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiAgedTubeCount5)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthLabel5)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiGreenCreep)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label102)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label103)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label101)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label100)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryGreen)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryYellow)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryRed)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiAgedTubeCount4)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthLabel4)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiAgedTubeCount3)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthLabel3)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiAgedTubeCount2)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthLabel2)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiAgedTubeCount1)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDiaGrowthLabel1)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label99)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiTubeFlawsGouges)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label97)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiTubeFlawsWelds)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label94)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label95)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.Label96)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryShowHideButton)
        Me.uiDetailedInspectionTabPage.Controls.Add(Me.uiDetailedSummaryButton)
        Me.uiDetailedInspectionTabPage.Name = "uiDetailedInspectionTabPage"
        Me.uiDetailedInspectionTabPage.PageVisible = false
        Me.uiDetailedInspectionTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiDetailedInspectionTabPage.Text = "Detailed Inspection && Assessment Results"
        '
        'uiDetailedSummaryTabControl
        '
        Me.uiDetailedSummaryTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiDetailedSummaryTabControl.Location = New System.Drawing.Point(553, 8)
        Me.uiDetailedSummaryTabControl.LookAndFeel.SkinName = "Office 2010 Blue"
        Me.uiDetailedSummaryTabControl.LookAndFeel.UseDefaultLookAndFeel = false
        Me.uiDetailedSummaryTabControl.Name = "uiDetailedSummaryTabControl"
        Me.uiDetailedSummaryTabControl.SelectedTabPage = Me.ExecSummaryTabPage
        Me.uiDetailedSummaryTabControl.Size = New System.Drawing.Size(606, 560)
        Me.uiDetailedSummaryTabControl.TabIndex = 38
        Me.uiDetailedSummaryTabControl.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.ExecSummaryTabPage, Me.DiaGrowthSummaryTabPage, Me.FFSSummaryTabPage, Me.TubeHarvestingTabPage})
        Me.uiDetailedSummaryTabControl.Tag = ""
        '
        'ExecSummaryTabPage
        '
        Me.ExecSummaryTabPage.Controls.Add(Me.uiExecSummaryDetailsEditor)
        Me.ExecSummaryTabPage.Name = "ExecSummaryTabPage"
        Me.ExecSummaryTabPage.Size = New System.Drawing.Size(600, 532)
        Me.ExecSummaryTabPage.Text = "Executive Summary Details"
        '
        'DiaGrowthSummaryTabPage
        '
        Me.DiaGrowthSummaryTabPage.Controls.Add(Me.uiDiaGrowthSummaryEditor)
        Me.DiaGrowthSummaryTabPage.Name = "DiaGrowthSummaryTabPage"
        Me.DiaGrowthSummaryTabPage.Size = New System.Drawing.Size(600, 532)
        Me.DiaGrowthSummaryTabPage.Text = "Diametrical Growth Summary"
        '
        'FFSSummaryTabPage
        '
        Me.FFSSummaryTabPage.Controls.Add(Me.uiFFSEditor)
        Me.FFSSummaryTabPage.Name = "FFSSummaryTabPage"
        Me.FFSSummaryTabPage.Size = New System.Drawing.Size(600, 532)
        Me.FFSSummaryTabPage.Text = "Fitness-For-Service Summary"
        '
        'TubeHarvestingTabPage
        '
        Me.TubeHarvestingTabPage.Controls.Add(Me.uiTubeHarvestingEditor)
        Me.TubeHarvestingTabPage.Name = "TubeHarvestingTabPage"
        Me.TubeHarvestingTabPage.Size = New System.Drawing.Size(600, 532)
        Me.TubeHarvestingTabPage.Text = "Tube Harvesting Results"
        '
        'uiDiaGrowthTotalLabel
        '
        Me.uiDiaGrowthTotalLabel.AutoSize = true
        Me.uiDiaGrowthTotalLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthTotalLabel.ForeColor = System.Drawing.Color.Teal
        Me.uiDiaGrowthTotalLabel.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthTotalLabel.Location = New System.Drawing.Point(95, 273)
        Me.uiDiaGrowthTotalLabel.Name = "uiDiaGrowthTotalLabel"
        Me.uiDiaGrowthTotalLabel.Size = New System.Drawing.Size(37, 17)
        Me.uiDiaGrowthTotalLabel.TabIndex = 35
        Me.uiDiaGrowthTotalLabel.Text = "Total"
        Me.uiDiaGrowthTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'uiLevel1Label
        '
        Me.uiLevel1Label.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLevel1Label.ForeColor = System.Drawing.Color.Teal
        Me.uiLevel1Label.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiLevel1Label.Location = New System.Drawing.Point(131, 319)
        Me.uiLevel1Label.Name = "uiLevel1Label"
        Me.uiLevel1Label.Size = New System.Drawing.Size(166, 99)
        Me.uiLevel1Label.TabIndex = 34
        Me.uiLevel1Label.Text = "Level 1 Checkboxes Selected Are: "
        Me.uiLevel1Label.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label116
        '
        Me.Label116.AutoSize = true
        Me.Label116.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label116.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label116.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label116.Location = New System.Drawing.Point(5, 242)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(78, 17)
        Me.Label116.TabIndex = 18
        Me.Label116.Text = "Tube Count:"
        Me.Label116.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiDiaGrowthLabel5
        '
        Me.uiDiaGrowthLabel5.AutoSize = true
        Me.uiDiaGrowthLabel5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthLabel5.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDiaGrowthLabel5.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthLabel5.Location = New System.Drawing.Point(149, 246)
        Me.uiDiaGrowthLabel5.Name = "uiDiaGrowthLabel5"
        Me.uiDiaGrowthLabel5.Size = New System.Drawing.Size(315, 17)
        Me.uiDiaGrowthLabel5.TabIndex = 20
        Me.uiDiaGrowthLabel5.Text = "You must select a material on the ""Tube Details"" tab!"
        Me.uiDiaGrowthLabel5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiGreenCreep
        '
        Me.uiGreenCreep.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "Creep", true))
        Me.uiGreenCreep.Location = New System.Drawing.Point(13, 416)
        Me.uiGreenCreep.Name = "uiGreenCreep"
        Me.uiGreenCreep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiGreenCreep.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiGreenCreep.Properties.Appearance.Options.UseFont = true
        Me.uiGreenCreep.Properties.Appearance.Options.UseForeColor = true
        Me.uiGreenCreep.Properties.Caption = "Creep signature visible in Green Tubes"
        Me.uiGreenCreep.Size = New System.Drawing.Size(284, 22)
        Me.uiGreenCreep.TabIndex = 25
        '
        'Label102
        '
        Me.Label102.AutoSize = true
        Me.Label102.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label102.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label102.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label102.Location = New System.Drawing.Point(5, 213)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(78, 17)
        Me.Label102.TabIndex = 15
        Me.Label102.Text = "Tube Count:"
        Me.Label102.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label103
        '
        Me.Label103.AutoSize = true
        Me.Label103.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label103.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label103.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label103.Location = New System.Drawing.Point(5, 184)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(78, 17)
        Me.Label103.TabIndex = 12
        Me.Label103.Text = "Tube Count:"
        Me.Label103.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label101
        '
        Me.Label101.AutoSize = true
        Me.Label101.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label101.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label101.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label101.Location = New System.Drawing.Point(5, 155)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(78, 17)
        Me.Label101.TabIndex = 9
        Me.Label101.Text = "Tube Count:"
        Me.Label101.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label100
        '
        Me.Label100.AutoSize = true
        Me.Label100.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label100.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label100.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label100.Location = New System.Drawing.Point(5, 126)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(78, 17)
        Me.Label100.TabIndex = 6
        Me.Label100.Text = "Tube Count:"
        Me.Label100.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiDetailedSummaryGreen
        '
        Me.uiDetailedSummaryGreen.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "FFSGreen", true))
        Me.uiDetailedSummaryGreen.Location = New System.Drawing.Point(13, 384)
        Me.uiDetailedSummaryGreen.Name = "uiDetailedSummaryGreen"
        Me.uiDetailedSummaryGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailedSummaryGreen.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailedSummaryGreen.Properties.Appearance.Options.UseFont = true
        Me.uiDetailedSummaryGreen.Properties.Appearance.Options.UseForeColor = true
        Me.uiDetailedSummaryGreen.Properties.Caption = "Green"
        Me.uiDetailedSummaryGreen.Size = New System.Drawing.Size(143, 22)
        Me.uiDetailedSummaryGreen.TabIndex = 24
        '
        'uiDetailedSummaryYellow
        '
        Me.uiDetailedSummaryYellow.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "FFSYellow", true))
        Me.uiDetailedSummaryYellow.Location = New System.Drawing.Point(13, 352)
        Me.uiDetailedSummaryYellow.Name = "uiDetailedSummaryYellow"
        Me.uiDetailedSummaryYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailedSummaryYellow.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailedSummaryYellow.Properties.Appearance.Options.UseFont = true
        Me.uiDetailedSummaryYellow.Properties.Appearance.Options.UseForeColor = true
        Me.uiDetailedSummaryYellow.Properties.Caption = "Yellow"
        Me.uiDetailedSummaryYellow.Size = New System.Drawing.Size(143, 22)
        Me.uiDetailedSummaryYellow.TabIndex = 23
        '
        'uiDetailedSummaryRed
        '
        Me.uiDetailedSummaryRed.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "FFSRed", true))
        Me.uiDetailedSummaryRed.Location = New System.Drawing.Point(13, 320)
        Me.uiDetailedSummaryRed.Name = "uiDetailedSummaryRed"
        Me.uiDetailedSummaryRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailedSummaryRed.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDetailedSummaryRed.Properties.Appearance.Options.UseFont = true
        Me.uiDetailedSummaryRed.Properties.Appearance.Options.UseForeColor = true
        Me.uiDetailedSummaryRed.Properties.Caption = "Red"
        Me.uiDetailedSummaryRed.Size = New System.Drawing.Size(143, 22)
        Me.uiDetailedSummaryRed.TabIndex = 22
        '
        'uiDiaGrowthLabel4
        '
        Me.uiDiaGrowthLabel4.AutoSize = true
        Me.uiDiaGrowthLabel4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthLabel4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDiaGrowthLabel4.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthLabel4.Location = New System.Drawing.Point(149, 217)
        Me.uiDiaGrowthLabel4.Name = "uiDiaGrowthLabel4"
        Me.uiDiaGrowthLabel4.Size = New System.Drawing.Size(315, 17)
        Me.uiDiaGrowthLabel4.TabIndex = 17
        Me.uiDiaGrowthLabel4.Text = "You must select a material on the ""Tube Details"" tab!"
        Me.uiDiaGrowthLabel4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiDiaGrowthLabel3
        '
        Me.uiDiaGrowthLabel3.AutoSize = true
        Me.uiDiaGrowthLabel3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthLabel3.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDiaGrowthLabel3.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthLabel3.Location = New System.Drawing.Point(149, 188)
        Me.uiDiaGrowthLabel3.Name = "uiDiaGrowthLabel3"
        Me.uiDiaGrowthLabel3.Size = New System.Drawing.Size(315, 17)
        Me.uiDiaGrowthLabel3.TabIndex = 14
        Me.uiDiaGrowthLabel3.Text = "You must select a material on the ""Tube Details"" tab!"
        Me.uiDiaGrowthLabel3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiDiaGrowthLabel2
        '
        Me.uiDiaGrowthLabel2.AutoSize = true
        Me.uiDiaGrowthLabel2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthLabel2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDiaGrowthLabel2.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthLabel2.Location = New System.Drawing.Point(149, 159)
        Me.uiDiaGrowthLabel2.Name = "uiDiaGrowthLabel2"
        Me.uiDiaGrowthLabel2.Size = New System.Drawing.Size(315, 17)
        Me.uiDiaGrowthLabel2.TabIndex = 11
        Me.uiDiaGrowthLabel2.Text = "You must select a material on the ""Tube Details"" tab!"
        Me.uiDiaGrowthLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiDiaGrowthLabel1
        '
        Me.uiDiaGrowthLabel1.AutoSize = true
        Me.uiDiaGrowthLabel1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDiaGrowthLabel1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDiaGrowthLabel1.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.uiDiaGrowthLabel1.Location = New System.Drawing.Point(149, 130)
        Me.uiDiaGrowthLabel1.Name = "uiDiaGrowthLabel1"
        Me.uiDiaGrowthLabel1.Size = New System.Drawing.Size(315, 17)
        Me.uiDiaGrowthLabel1.TabIndex = 8
        Me.uiDiaGrowthLabel1.Text = "You must select a material on the ""Tube Details"" tab!"
        Me.uiDiaGrowthLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label99
        '
        Me.Label99.AutoSize = true
        Me.Label99.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label99.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label99.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label99.Location = New System.Drawing.Point(19, 63)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(56, 17)
        Me.Label99.TabIndex = 3
        Me.Label99.Text = "Gouges:"
        Me.Label99.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeFlawsGouges
        '
        Me.uiTubeFlawsGouges.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "Gouges", true))
        Me.uiTubeFlawsGouges.Location = New System.Drawing.Point(78, 63)
        Me.uiTubeFlawsGouges.Name = "uiTubeFlawsGouges"
        Me.uiTubeFlawsGouges.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiTubeFlawsGouges.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsGouges.Properties.Appearance.Options.UseBackColor = true
        Me.uiTubeFlawsGouges.Properties.Appearance.Options.UseFont = true
        Me.uiTubeFlawsGouges.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsGouges.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeFlawsGouges.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsGouges.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTubeFlawsGouges.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsGouges.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeFlawsGouges.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsGouges.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject28.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject28.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject28.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject28.Options.UseBackColor = true
        SerializableAppearanceObject28.Options.UseBorderColor = true
        Me.uiTubeFlawsGouges.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject28, "", Nothing, Nothing, true)})
        Me.uiTubeFlawsGouges.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTubeFlawsGouges.Properties.Items.AddRange(New Object() {"Single flaw", "Multiple flaws", "No flaws noted"})
        Me.uiTubeFlawsGouges.Properties.MaxLength = 64
        Me.uiTubeFlawsGouges.Size = New System.Drawing.Size(166, 24)
        Me.uiTubeFlawsGouges.TabIndex = 4
        ConditionValidationRule68.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule68.ErrorText = "Gouges dropdown cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeFlawsGouges, ConditionValidationRule68)
        '
        'Label97
        '
        Me.Label97.AutoSize = true
        Me.Label97.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label97.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label97.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label97.Location = New System.Drawing.Point(28, 31)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(47, 17)
        Me.Label97.TabIndex = 1
        Me.Label97.Text = "Welds:"
        Me.Label97.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiTubeFlawsWelds
        '
        Me.uiTubeFlawsWelds.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections_AgedSummary, "Welds", true))
        Me.uiTubeFlawsWelds.Location = New System.Drawing.Point(78, 31)
        Me.uiTubeFlawsWelds.Name = "uiTubeFlawsWelds"
        Me.uiTubeFlawsWelds.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.uiTubeFlawsWelds.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsWelds.Properties.Appearance.Options.UseBackColor = true
        Me.uiTubeFlawsWelds.Properties.Appearance.Options.UseFont = true
        Me.uiTubeFlawsWelds.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsWelds.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiTubeFlawsWelds.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsWelds.Properties.AppearanceDropDown.Options.UseFont = true
        Me.uiTubeFlawsWelds.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsWelds.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiTubeFlawsWelds.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiTubeFlawsWelds.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject29.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject29.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject29.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject29.Options.UseBackColor = true
        SerializableAppearanceObject29.Options.UseBorderColor = true
        Me.uiTubeFlawsWelds.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject29, "", Nothing, Nothing, true)})
        Me.uiTubeFlawsWelds.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiTubeFlawsWelds.Properties.Items.AddRange(New Object() {"Single flaw", "Multiple flaws", "No flaws noted"})
        Me.uiTubeFlawsWelds.Properties.MaxLength = 64
        Me.uiTubeFlawsWelds.Size = New System.Drawing.Size(166, 24)
        Me.uiTubeFlawsWelds.TabIndex = 2
        ConditionValidationRule69.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank
        ConditionValidationRule69.ErrorText = "Welds dropdown cannot be blank."
        Me.DxValidationProvider.SetValidationRule(Me.uiTubeFlawsWelds, ConditionValidationRule69)
        '
        'Label94
        '
        Me.Label94.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label94.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label94.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label94.Location = New System.Drawing.Point(4, 297)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(537, 15)
        Me.Label94.TabIndex = 21
        Me.Label94.Text = "FITNESS-FOR-SERVICE RESULTS PARAGRAPH"
        Me.Label94.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label95
        '
        Me.Label95.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label95.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label95.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label95.Location = New System.Drawing.Point(4, 99)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(537, 15)
        Me.Label95.TabIndex = 5
        Me.Label95.Text = "DIAMETRICAL GROWTH SUMMARY"
        Me.Label95.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label96
        '
        Me.Label96.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label96.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label96.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label96.Location = New System.Drawing.Point(4, 8)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(537, 15)
        Me.Label96.TabIndex = 0
        Me.Label96.Text = "TUBE FLAWS"
        Me.Label96.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'uiDetailedSummaryShowHideButton
        '
        Me.uiDetailedSummaryShowHideButton.Image = CType(resources.GetObject("uiDetailedSummaryShowHideButton.Image"),System.Drawing.Image)
        Me.uiDetailedSummaryShowHideButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.uiDetailedSummaryShowHideButton.Location = New System.Drawing.Point(507, 461)
        Me.uiDetailedSummaryShowHideButton.Name = "uiDetailedSummaryShowHideButton"
        Me.uiDetailedSummaryShowHideButton.Size = New System.Drawing.Size(27, 27)
        Me.uiDetailedSummaryShowHideButton.TabIndex = 32
        '
        'uiDetailedSummaryButton
        '
        Me.uiDetailedSummaryButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiDetailedSummaryButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiDetailedSummaryButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiDetailedSummaryButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDetailedSummaryButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiDetailedSummaryButton.Appearance.Options.UseBackColor = true
        Me.uiDetailedSummaryButton.Appearance.Options.UseBorderColor = true
        Me.uiDetailedSummaryButton.Appearance.Options.UseFont = true
        Me.uiDetailedSummaryButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDetailedSummaryButton.Location = New System.Drawing.Point(358, 461)
        Me.uiDetailedSummaryButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiDetailedSummaryButton.Name = "uiDetailedSummaryButton"
        Me.uiDetailedSummaryButton.Size = New System.Drawing.Size(141, 32)
        Me.uiDetailedSummaryButton.TabIndex = 31
        Me.uiDetailedSummaryButton.Text = "Generate Summaries"
        '
        'uiLinkedFilesTabPage
        '
        Me.uiLinkedFilesTabPage.Controls.Add(Me.SplitContainerControl1)
        Me.uiLinkedFilesTabPage.Name = "uiLinkedFilesTabPage"
        Me.uiLinkedFilesTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiLinkedFilesTabPage.Text = "Linked Files"
        '
        'uiFieldReportTabPage
        '
        Me.uiFieldReportTabPage.Controls.Add(Me.uiSampleLevel3)
        Me.uiFieldReportTabPage.Controls.Add(Me.ReportStatusLabel)
        Me.uiFieldReportTabPage.Controls.Add(Me.uiReportFilename)
        Me.uiFieldReportTabPage.Controls.Add(Me.Label133)
        Me.uiFieldReportTabPage.Controls.Add(Me.uiViewReportButton)
        Me.uiFieldReportTabPage.Controls.Add(Me.uiCreateReportButton)
        Me.uiFieldReportTabPage.Controls.Add(Me.GroupBox5)
        Me.uiFieldReportTabPage.Controls.Add(Me.Label130)
        Me.uiFieldReportTabPage.Controls.Add(Me.Label129)
        Me.uiFieldReportTabPage.Name = "uiFieldReportTabPage"
        Me.uiFieldReportTabPage.Size = New System.Drawing.Size(1162, 576)
        Me.uiFieldReportTabPage.Text = "Report"
        '
        'uiSampleLevel3
        '
        Me.uiSampleLevel3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.bsInspections, "SampleLevel3", true))
        Me.uiSampleLevel3.Location = New System.Drawing.Point(16, 52)
        Me.uiSampleLevel3.Name = "uiSampleLevel3"
        Me.uiSampleLevel3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSampleLevel3.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiSampleLevel3.Properties.Appearance.Options.UseFont = true
        Me.uiSampleLevel3.Properties.Appearance.Options.UseForeColor = true
        Me.uiSampleLevel3.Properties.Caption = "Include Sample Level 3 Assessment in Report"
        Me.uiSampleLevel3.Size = New System.Drawing.Size(301, 22)
        Me.uiSampleLevel3.TabIndex = 82
        Me.uiSampleLevel3.Visible = false
        '
        'ReportStatusLabel
        '
        Me.ReportStatusLabel.AutoSize = true
        Me.ReportStatusLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ReportStatusLabel.ForeColor = System.Drawing.Color.Crimson
        Me.ReportStatusLabel.Location = New System.Drawing.Point(561, 52)
        Me.ReportStatusLabel.Name = "ReportStatusLabel"
        Me.ReportStatusLabel.Size = New System.Drawing.Size(210, 17)
        Me.ReportStatusLabel.TabIndex = 81
        Me.ReportStatusLabel.Text = "Your report is being generated..."
        Me.ReportStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.ReportStatusLabel.Visible = false
        '
        'Label133
        '
        Me.Label133.AutoSize = true
        Me.Label133.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label133.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label133.Location = New System.Drawing.Point(17, 213)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(114, 17)
        Me.Label133.TabIndex = 79
        Me.Label133.Text = "Report Document:"
        Me.Label133.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiViewReportButton
        '
        Me.uiViewReportButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiViewReportButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiViewReportButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiViewReportButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiViewReportButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiViewReportButton.Appearance.Options.UseBackColor = true
        Me.uiViewReportButton.Appearance.Options.UseBorderColor = true
        Me.uiViewReportButton.Appearance.Options.UseFont = true
        Me.uiViewReportButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiViewReportButton.Location = New System.Drawing.Point(564, 259)
        Me.uiViewReportButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiViewReportButton.Name = "uiViewReportButton"
        Me.uiViewReportButton.Size = New System.Drawing.Size(169, 32)
        Me.uiViewReportButton.TabIndex = 5
        Me.uiViewReportButton.Text = "View Report"
        '
        'uiCreateReportButton
        '
        Me.uiCreateReportButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCreateReportButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCreateReportButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCreateReportButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCreateReportButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiCreateReportButton.Appearance.Options.UseBackColor = true
        Me.uiCreateReportButton.Appearance.Options.UseBorderColor = true
        Me.uiCreateReportButton.Appearance.Options.UseFont = true
        Me.uiCreateReportButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCreateReportButton.Location = New System.Drawing.Point(564, 89)
        Me.uiCreateReportButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiCreateReportButton.Name = "uiCreateReportButton"
        Me.uiCreateReportButton.Size = New System.Drawing.Size(169, 32)
        Me.uiCreateReportButton.TabIndex = 4
        Me.uiCreateReportButton.Text = "Create Report"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.PaperSizeOptionGroup)
        Me.GroupBox5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.GroupBox5.Location = New System.Drawing.Point(364, 52)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(120, 84)
        Me.GroupBox5.TabIndex = 74
        Me.GroupBox5.TabStop = false
        Me.GroupBox5.Text = "Paper Size:"
        '
        'PaperSizeOptionGroup
        '
        Me.PaperSizeOptionGroup.EditValue = false
        Me.PaperSizeOptionGroup.Location = New System.Drawing.Point(7, 17)
        Me.PaperSizeOptionGroup.Name = "PaperSizeOptionGroup"
        Me.PaperSizeOptionGroup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PaperSizeOptionGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PaperSizeOptionGroup.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.PaperSizeOptionGroup.Properties.Appearance.Options.UseBackColor = true
        Me.PaperSizeOptionGroup.Properties.Appearance.Options.UseFont = true
        Me.PaperSizeOptionGroup.Properties.Appearance.Options.UseForeColor = true
        Me.PaperSizeOptionGroup.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PaperSizeOptionGroup.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.DarkGray
        Me.PaperSizeOptionGroup.Properties.AppearanceDisabled.Options.UseFont = true
        Me.PaperSizeOptionGroup.Properties.AppearanceDisabled.Options.UseForeColor = true
        Me.PaperSizeOptionGroup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PaperSizeOptionGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Letter"), New DevExpress.XtraEditors.Controls.RadioGroupItem(false, "A4")})
        Me.PaperSizeOptionGroup.Size = New System.Drawing.Size(208, 63)
        Me.PaperSizeOptionGroup.TabIndex = 0
        '
        'Label130
        '
        Me.Label130.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label130.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label130.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label130.Location = New System.Drawing.Point(5, 174)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(766, 15)
        Me.Label130.TabIndex = 70
        Me.Label130.Text = "VIEW EXISTING REPORT"
        Me.Label130.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label129
        '
        Me.Label129.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Label129.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label129.ForeColor = System.Drawing.Color.GhostWhite
        Me.Label129.Location = New System.Drawing.Point(5, 10)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(766, 15)
        Me.Label129.TabIndex = 69
        Me.Label129.Text = "CREATE REPORT"
        Me.Label129.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TblDefaultTextListItemsBindingSource
        '
        Me.TblDefaultTextListItemsBindingSource.DataMember = "tblDefaultText_ListItems"
        Me.TblDefaultTextListItemsBindingSource.DataSource = Me.QTT_LookupsData
        '
        'TblInspectionsvwReportItemsDetailsBindingSource
        '
        Me.TblInspectionsvwReportItemsDetailsBindingSource.DataMember = "tblInspections_vwReportItems_Details"
        Me.TblInspectionsvwReportItemsDetailsBindingSource.DataSource = Me.bsInspections
        '
        'uiCustomerNameLabel
        '
        Me.uiCustomerNameLabel.AutoSize = true
        Me.uiCustomerNameLabel.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiCustomerNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiCustomerNameLabel.Location = New System.Drawing.Point(5, 3)
        Me.uiCustomerNameLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uiCustomerNameLabel.Name = "uiCustomerNameLabel"
        Me.uiCustomerNameLabel.Size = New System.Drawing.Size(240, 40)
        Me.uiCustomerNameLabel.TabIndex = 17
        Me.uiCustomerNameLabel.Text = "Customer Name"
        '
        'uiLocationNameLabel
        '
        Me.uiLocationNameLabel.AutoSize = true
        Me.uiLocationNameLabel.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiLocationNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiLocationNameLabel.Location = New System.Drawing.Point(5, 43)
        Me.uiLocationNameLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uiLocationNameLabel.Name = "uiLocationNameLabel"
        Me.uiLocationNameLabel.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.uiLocationNameLabel.Size = New System.Drawing.Size(132, 21)
        Me.uiLocationNameLabel.TabIndex = 18
        Me.uiLocationNameLabel.Text = "Location Name"
        '
        'uiHeaterNameLabel
        '
        Me.uiHeaterNameLabel.AutoSize = true
        Me.uiHeaterNameLabel.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiHeaterNameLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiHeaterNameLabel.Location = New System.Drawing.Point(5, 64)
        Me.uiHeaterNameLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uiHeaterNameLabel.Name = "uiHeaterNameLabel"
        Me.uiHeaterNameLabel.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.uiHeaterNameLabel.Size = New System.Drawing.Size(203, 21)
        Me.uiHeaterNameLabel.TabIndex = 18
        Me.uiHeaterNameLabel.Text = "Heater Name - Heater ID"
        '
        'uiInspectionTypeLabel
        '
        Me.uiInspectionTypeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiInspectionTypeLabel.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiInspectionTypeLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiInspectionTypeLabel.Location = New System.Drawing.Point(532, 3)
        Me.uiInspectionTypeLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uiInspectionTypeLabel.Name = "uiInspectionTypeLabel"
        Me.uiInspectionTypeLabel.Size = New System.Drawing.Size(645, 40)
        Me.uiInspectionTypeLabel.TabIndex = 17
        Me.uiInspectionTypeLabel.Text = "MANTIS"
        Me.uiInspectionTypeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiSaveButton
        '
        Me.uiSaveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.uiSaveButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiSaveButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiSaveButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiSaveButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSaveButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiSaveButton.Appearance.Options.UseBackColor = true
        Me.uiSaveButton.Appearance.Options.UseBorderColor = true
        Me.uiSaveButton.Appearance.Options.UseFont = true
        Me.uiSaveButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiSaveButton.Location = New System.Drawing.Point(9, 738)
        Me.uiSaveButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiSaveButton.Name = "uiSaveButton"
        Me.uiSaveButton.Size = New System.Drawing.Size(169, 32)
        Me.uiSaveButton.TabIndex = 1
        Me.uiSaveButton.Text = "&Save Changes"
        '
        'uiSaveExitButton
        '
        Me.uiSaveExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiSaveExitButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiSaveExitButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiSaveExitButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiSaveExitButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiSaveExitButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiSaveExitButton.Appearance.Options.UseBackColor = true
        Me.uiSaveExitButton.Appearance.Options.UseBorderColor = true
        Me.uiSaveExitButton.Appearance.Options.UseFont = true
        Me.uiSaveExitButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiSaveExitButton.Location = New System.Drawing.Point(1004, 738)
        Me.uiSaveExitButton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiSaveExitButton.Name = "uiSaveExitButton"
        Me.uiSaveExitButton.Size = New System.Drawing.Size(169, 32)
        Me.uiSaveExitButton.TabIndex = 3
        Me.uiSaveExitButton.Text = "Save and E&xit"
        '
        'uiDateLabel
        '
        Me.uiDateLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiDateLabel.Font = New System.Drawing.Font("Segoe UI", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiDateLabel.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiDateLabel.Location = New System.Drawing.Point(715, 43)
        Me.uiDateLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uiDateLabel.Name = "uiDateLabel"
        Me.uiDateLabel.Padding = New System.Windows.Forms.Padding(0, 0, 6, 0)
        Me.uiDateLabel.Size = New System.Drawing.Size(462, 21)
        Me.uiDateLabel.TabIndex = 65
        Me.uiDateLabel.Text = "Date"
        Me.uiDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uiRootFolderName
        '
        Me.uiRootFolderName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiRootFolderName.EditValue = "FolderName"
        Me.uiRootFolderName.Location = New System.Drawing.Point(16, 93)
        Me.uiRootFolderName.Name = "uiRootFolderName"
        Me.uiRootFolderName.Properties.Appearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.uiRootFolderName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiRootFolderName.Properties.Appearance.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uiRootFolderName.Properties.Appearance.Options.UseBorderColor = true
        Me.uiRootFolderName.Properties.Appearance.Options.UseFont = true
        Me.uiRootFolderName.Properties.Appearance.Options.UseForeColor = true
        Me.uiRootFolderName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        SerializableAppearanceObject30.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject30.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject30.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject30.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject30.Options.UseBackColor = true
        SerializableAppearanceObject30.Options.UseBorderColor = true
        SerializableAppearanceObject30.Options.UseFont = true
        Me.uiRootFolderName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "&Open Folder", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject30, "", "OPEN", Nothing, true)})
        Me.uiRootFolderName.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiRootFolderName.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiRootFolderName.Properties.UseParentBackground = true
        Me.uiRootFolderName.Size = New System.Drawing.Size(998, 22)
        Me.uiRootFolderName.TabIndex = 0
        '
        'DxValidationProvider
        '
        Me.DxValidationProvider.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual
        '
        'FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource
        '
        Me.FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource.DataMember = "FK_tblPipeTubeItemSummaries_tblPipeTubeItems"
        Me.FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource.DataSource = Me.bsInspections_PipeTubeItems
        '
        'TblContactInformationTableAdapter1
        '
        Me.TblContactInformationTableAdapter1.ClearBeforeFill = true
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = true
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GridColumn1.AppearanceHeader.Options.UseFont = true
        Me.GridColumn1.FieldName = "ID"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'tmrAutoSave
        '
        Me.tmrAutoSave.Interval = 300000
        '
        'uiAutoSaveInterval
        '
        Me.uiAutoSaveInterval.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uiAutoSaveInterval.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.uiAutoSaveInterval.Location = New System.Drawing.Point(895, 738)
        Me.uiAutoSaveInterval.Margin = New System.Windows.Forms.Padding(0)
        Me.uiAutoSaveInterval.Name = "uiAutoSaveInterval"
        Me.uiAutoSaveInterval.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSaveInterval.Properties.Appearance.Options.UseFont = true
        Me.uiAutoSaveInterval.Properties.Appearance.Options.UseTextOptions = true
        Me.uiAutoSaveInterval.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiAutoSaveInterval.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSaveInterval.Properties.AppearanceDisabled.Options.UseFont = true
        Me.uiAutoSaveInterval.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSaveInterval.Properties.AppearanceFocused.Options.UseFont = true
        Me.uiAutoSaveInterval.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uiAutoSaveInterval.Properties.AppearanceReadOnly.Options.UseFont = true
        SerializableAppearanceObject31.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject31.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject31.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject31.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        SerializableAppearanceObject31.Options.UseBackColor = true
        SerializableAppearanceObject31.Options.UseBorderColor = true
        SerializableAppearanceObject31.Options.UseFont = true
        Me.uiAutoSaveInterval.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject31, "", Nothing, Nothing, true)})
        Me.uiAutoSaveInterval.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiAutoSaveInterval.Properties.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.uiAutoSaveInterval.Properties.Mask.EditMask = "n1"
        Me.uiAutoSaveInterval.Properties.MaxValue = New Decimal(New Integer() {30, 0, 0, 0})
        Me.uiAutoSaveInterval.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.uiAutoSaveInterval.Size = New System.Drawing.Size(46, 24)
        Me.uiAutoSaveInterval.TabIndex = 2
        '
        'uilblAutoSaveInterval
        '
        Me.uilblAutoSaveInterval.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uilblAutoSaveInterval.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uilblAutoSaveInterval.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblAutoSaveInterval.Location = New System.Drawing.Point(777, 738)
        Me.uilblAutoSaveInterval.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uilblAutoSaveInterval.Name = "uilblAutoSaveInterval"
        Me.uilblAutoSaveInterval.Size = New System.Drawing.Size(114, 21)
        Me.uilblAutoSaveInterval.TabIndex = 1
        Me.uilblAutoSaveInterval.Text = "AutoSave Interval"
        Me.uilblAutoSaveInterval.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uilblAutoSaveUnits
        '
        Me.uilblAutoSaveUnits.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uilblAutoSaveUnits.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uilblAutoSaveUnits.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblAutoSaveUnits.Location = New System.Drawing.Point(944, 738)
        Me.uilblAutoSaveUnits.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uilblAutoSaveUnits.Name = "uilblAutoSaveUnits"
        Me.uilblAutoSaveUnits.Size = New System.Drawing.Size(34, 21)
        Me.uilblAutoSaveUnits.TabIndex = 3
        Me.uilblAutoSaveUnits.Text = "min."
        '
        'uilblLastAutoSave
        '
        Me.uilblLastAutoSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.uilblLastAutoSave.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.uilblLastAutoSave.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.uilblLastAutoSave.Location = New System.Drawing.Point(782, 763)
        Me.uilblLastAutoSave.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.uilblLastAutoSave.Name = "uilblLastAutoSave"
        Me.uilblLastAutoSave.Size = New System.Drawing.Size(199, 21)
        Me.uilblLastAutoSave.TabIndex = 69
        Me.uilblLastAutoSave.Text = "Last AutoSave:  Never"
        '
        'taTblPlantProcessTypes
        '
        Me.taTblPlantProcessTypes.ClearBeforeFill = true
        '
        'taTblFlowDirections
        '
        Me.taTblFlowDirections.ClearBeforeFill = true
        '
        'taTblBurnerConfigurations
        '
        Me.taTblBurnerConfigurations.ClearBeforeFill = true
        '
        'taTblReformerOEMs
        '
        Me.taTblReformerOEMs.ClearBeforeFill = true
        '
        'TblReformerAgedSummaryTableAdapter
        '
        Me.TblReformerAgedSummaryTableAdapter.ClearBeforeFill = true
        '
        'TblReformerNewSummaryTableAdapter
        '
        Me.TblReformerNewSummaryTableAdapter.ClearBeforeFill = true
        '
        'taTblReformerAgedSummary
        '
        Me.taTblReformerAgedSummary.ClearBeforeFill = true
        '
        'taTblReformerNewSummary
        '
        Me.taTblReformerNewSummary.ClearBeforeFill = true
        '
        'TblInspectionstblReformerAgedSummaryBindingSource
        '
        Me.TblInspectionstblReformerAgedSummaryBindingSource.DataMember = "tblInspections_tblReformerAgedSummary"
        Me.TblInspectionstblReformerAgedSummaryBindingSource.DataSource = Me.bsInspections
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = false
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(666, 581)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'InspectionDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96!, 96!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1184, 783)
        Me.Controls.Add(Me.uilblLastAutoSave)
        Me.Controls.Add(Me.uilblAutoSaveUnits)
        Me.Controls.Add(Me.uiDateLabel)
        Me.Controls.Add(Me.uiSaveExitButton)
        Me.Controls.Add(Me.uilblAutoSaveInterval)
        Me.Controls.Add(Me.uiAutoSaveInterval)
        Me.Controls.Add(Me.uiSaveButton)
        Me.Controls.Add(Me.uiHeaterNameLabel)
        Me.Controls.Add(Me.uiLocationNameLabel)
        Me.Controls.Add(Me.uiInspectionTypeLabel)
        Me.Controls.Add(Me.uiCustomerNameLabel)
        Me.Controls.Add(Me.uiMainXtraTabControl)
        Me.Controls.Add(Me.uiRootFolderName)
        Me.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.bsInspections, "RootFolderName", true))
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(1200, 821)
        Me.Name = "InspectionDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inspection Details"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        CType(bsPipeTubeItemTypes_LU,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_LookupsData,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionEndDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionEndDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_InspectionsDataSets,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_AppendixItems,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_Appendixes,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_ReportItems,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_PipeTubeItems,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsPipeTubeItemsSummaries,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsDefaultPipeSchedules,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTT_Customers,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_OtherInspectors,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTT_Roles_Main,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionTypesBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsTblInspectionTypes,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTT_Roles_Other,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTT_LeadInspectors,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspectionTools,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_InspectionsAndTools,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsOtherInspectors,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTTOffices,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsQTTInspectors,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPage1.ResumeLayout(false)
        Me.TabPage1.PerformLayout
        Me.TabPage2.ResumeLayout(false)
        Me.TabPage2.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        CType(Me.NumericUpDown3,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPage3.ResumeLayout(false)
        Me.TabPage3.PerformLayout
        CType(Me.DataGridView3,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPage4.ResumeLayout(false)
        Me.TabPage4.PerformLayout
        Me.TabPage5.ResumeLayout(false)
        Me.TabPage5.PerformLayout
        CType(Me.dgAppendixItems,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDown4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspectionCustomers,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspectionCustomer,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionstblCustomersBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionstblCustomersBindingSource1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.QTT_PipeTubeDetails,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsPipeTubeItemTypes,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsPipeTubeTypes,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionstblPipeTubeItemsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspectionTypes_LU,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SplitContainerControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.SplitContainerControl2.ResumeLayout(false)
        CType(Me.uiProjectNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiProposalNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCustomTubeFormatEditor.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectorPhone.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiPostalCode.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCity.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAddress.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiLocation.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeLength.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeWallThicknessDisplay.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeOuterDiameterDisplay.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeInnerDiameterDisplay.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeWallThickness.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeOuterDiameter.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeInnerDiameter.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiReportFilename.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiPurchaseOrder.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCustomRowFormatEditor.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberTubesTotal.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiPlusTubeTolerance.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiMinusTubeTolerance.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeDesignToleranceDisplay.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiRedTubeCount.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_AgedSummary,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOverallTubesInspected.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiSwellingOver3PercentCount.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiBulgingTubeCount.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiWorstTubeIDRow.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiWorstTubeIDTube.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiBulgingWorstPercent.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiYellowTubeCount.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAgedTubeCount3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAgedTubeCount2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAgedTubeCount1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAgedTubeCount4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNewTubeCount4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bsInspections_NewSummary,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNewTubeCount3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNewTubeCount2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNewTubeCount1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiWorstFlawTubeNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberTubesWithFlaws.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiWorstFlawRowNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeServiceDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeNextInspectionDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAgedTubeCount5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDiaGrowthMaxPercent.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDiaGrowthTubeID.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDiaGrowthRowID.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOperatingPeriod.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberOfTubes.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeLengthDisplay.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiImageNameFormat.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCracking.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberTubesInspected.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberingSystem.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SplitContainerControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.SplitContainerControl1.ResumeLayout(false)
        CType(Me.uiReportItemsGridControl,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiReportItemsGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEditMain,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemLookUpEdit2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAppendixItems.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAppendixItemsGridControl,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAppendixItemsGridView,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEditAppendix,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemLookUpEdit3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEdit1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemTextEdit1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemSearchLookUpEdit1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemLookUpEdit4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemTextEdit2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiMainXtraTabControl,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiMainXtraTabControl.ResumeLayout(false)
        Me.uiInspectionDetailsTabPage.ResumeLayout(false)
        Me.uiInspectionDetailsTabPage.PerformLayout
        CType(Me.uiSpareUninstalledGroup.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCanCircularOption.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiNumberOfRows.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiRowFormatGroupBox.ResumeLayout(false)
        CType(Me.RowFormatRadioGroup.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionStartDate.Properties.VistaTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiInspectionStartDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiToolsCheckedList,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiTubeFormatGroupBox.ResumeLayout(false)
        CType(Me.TubeFormatRadioGroup.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiRolesList,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiProjectInspectorsList,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOtherInspectorsList,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTitleRole.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiLeadInspector.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOffices.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiCustomerDetailsTabPage.ResumeLayout(false)
        Me.uiCustomerDetailsTabPage.PerformLayout
        CType(Me.uiJMC.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiReformerOEM.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiPlantProcessType.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiFlowDirection.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiBurnerConfiguration.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiGridControlContacts,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.bstblContactInformation,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiGridViewContacts,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiState.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCountry.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiCustomers.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiReformerName.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiReformerID.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiProjectDescriptionTabPage.ResumeLayout(false)
        Me.uiTubeDetailsTabPage.ResumeLayout(false)
        Me.uiTubeDetailsTabPage.PerformLayout
        CType(Me.uiDegreesMarker.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox1.ResumeLayout(false)
        CType(Me.uiTolerancesGroup.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeManufacturer.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiTubeUnitGroup.ResumeLayout(false)
        CType(Me.uiLeadingUnits.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeMaterials.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeInspectionAge.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiPressure.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTargetTemp.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiThermalCycles.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOperatingHours.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiExecutiveSummaryTabPage.ResumeLayout(false)
        Me.uiExecutiveSummaryTabPage.PerformLayout
        CType(Me.uiMaxGrowthLocation.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiExecSummaryGreen.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiExecSummaryYellow.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiExecSummaryRed.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox3.ResumeLayout(false)
        CType(Me.uiOverallMaxGrowth.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiOverallMaxGrowthLocation.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiInspectionSummaryTabPage.ResumeLayout(false)
        Me.uiInspectionSummaryTabPage.PerformLayout
        CType(Me.uiTubeFlawLocation.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiDetailedInspectionTabPage.ResumeLayout(false)
        Me.uiDetailedInspectionTabPage.PerformLayout
        CType(Me.uiDetailedSummaryTabControl,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiDetailedSummaryTabControl.ResumeLayout(false)
        Me.ExecSummaryTabPage.ResumeLayout(false)
        Me.DiaGrowthSummaryTabPage.ResumeLayout(false)
        Me.FFSSummaryTabPage.ResumeLayout(false)
        Me.TubeHarvestingTabPage.ResumeLayout(false)
        CType(Me.uiGreenCreep.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDetailedSummaryGreen.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDetailedSummaryYellow.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiDetailedSummaryRed.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeFlawsGouges.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiTubeFlawsWelds.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.uiLinkedFilesTabPage.ResumeLayout(false)
        Me.uiFieldReportTabPage.ResumeLayout(false)
        Me.uiFieldReportTabPage.PerformLayout
        CType(Me.uiSampleLevel3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox5.ResumeLayout(false)
        CType(Me.PaperSizeOptionGroup.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblDefaultTextListItemsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionsvwReportItemsDetailsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiRootFolderName.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DxValidationProvider,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.uiAutoSaveInterval.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblInspectionstblReformerAgedSummaryBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox31 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox32 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckedListBox2 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents TextBox33 As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents TextBox34 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents TextBox36 As System.Windows.Forms.TextBox
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents ComboBox13 As System.Windows.Forms.ComboBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ComboBox14 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox15 As System.Windows.Forms.ComboBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents TextBox37 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox38 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox39 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox40 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents TextBox41 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox42 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox43 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox44 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents dgAppendixItems As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewButtonColumn1 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents TextBox45 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox46 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox47 As System.Windows.Forms.TextBox
    Friend WithEvents HScrollBar2 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents bsInspections As System.Windows.Forms.BindingSource
    Friend WithEvents QTT_InspectionsDataSets As AutoReporter.QTT_InspectionsDataSets
    Friend WithEvents taTblInspections As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter
    Friend WithEvents taTblInspectionTypes As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter
    Friend WithEvents QTT_LookupsData As AutoReporter.QTT_LookupsData
    Friend WithEvents bsQTTOffices As System.Windows.Forms.BindingSource
    Friend WithEvents taTblQTT_Offices As AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_OfficesTableAdapter
    Friend WithEvents bsInspections_InspectionsAndTools As System.Windows.Forms.BindingSource
    Friend WithEvents taTblInspectionsAndTools As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsAndToolsTableAdapter
    Friend WithEvents bsInspectionTools As System.Windows.Forms.BindingSource
    Friend WithEvents taTblInspectionTools As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionToolsTableAdapter
    Friend WithEvents QTT_PipeTubeDetails As AutoReporter.QTT_PipeTubeDetails
    Friend WithEvents bsInspections_PipeTubeItems As System.Windows.Forms.BindingSource
    Friend WithEvents taTblPipeTubeItems_Dets As AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemsTableAdapter
    Friend WithEvents bsDefaultPipeSchedules As System.Windows.Forms.BindingSource
    Friend WithEvents taTblDefaultPipeSchedules As AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultPipeSchedulesTableAdapter
    Friend WithEvents bsQTTInspectors As System.Windows.Forms.BindingSource
    Friend WithEvents taTblQTT_Inspectors As AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_InspectorsTableAdapter
    Friend WithEvents bsPipeTubeItemTypes As System.Windows.Forms.BindingSource
    Friend WithEvents taTblPipeTubeItemTypes_Dets As AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter
    Friend WithEvents taTblPipeTubeItems As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemsTableAdapter
    Friend WithEvents bsTblInspectionTypes As System.Windows.Forms.BindingSource
    Friend WithEvents taTblCustomers_Inspection As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter
    Friend WithEvents bsInspectionTypes_LU As System.Windows.Forms.BindingSource
    Friend WithEvents taTblInspectionTypes_LU As AutoReporter.QTT_LookupsDataTableAdapters.tblInspectionTypesTableAdapter
    'jkt   Friend WithEvents taQryAppendixItems_Details As QTT_InspectionReporter.QTT_InspectionsDataSetsTableAdapters.qryAppendixItems_DetailsTableAdapter
    Friend WithEvents bsInspections_AppendixItems As System.Windows.Forms.BindingSource
    Friend WithEvents bsInspections_Appendixes As System.Windows.Forms.BindingSource
    'jkt  Friend WithEvents taQryAppendixDetails As QTT_InspectionReporter.QTT_InspectionsDataSetsTableAdapters.qryAppendixDetailsTableAdapter
    'jkt  Friend WithEvents taQryReportItems_Details As QTT_InspectionReporter.QTT_InspectionsDataSetsTableAdapters.qryReportItems_DetailsTableAdapter
    Friend WithEvents bsInspections_ReportItems As System.Windows.Forms.BindingSource
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewButtonColumn3 As System.Windows.Forms.DataGridViewButtonColumn
    'Friend WithEvents AutoSelectFile As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkedFileDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsUserSpecifiedDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents FKAppedixSectionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FKReportItemTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ItemTypeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ItemIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubFolderDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PassSubfoldersDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents PassPrefixDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents UnitPrefixDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents PipeTypeTextDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents FilenameSuffixDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TblInspectionTypesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents taTblPipeTubeItemTypes As AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter
    Friend WithEvents TblPipeTubeItemTypesTableAdapter As AutoReporter.QTT_LookupsDataTableAdapters.tblPipeTubeItemTypesTableAdapter
    Friend WithEvents VwAppendixDetailsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwAppendixDetailsTableAdapter
    Friend WithEvents VwAppendixItem_DetailsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwAppendixItem_DetailsTableAdapter
    Friend WithEvents VwReportItems_DetailsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.vwReportItems_DetailsTableAdapter
    Friend WithEvents bsInspectionCustomers As System.Windows.Forms.BindingSource
    Friend WithEvents bsInspectionCustomer As System.Windows.Forms.BindingSource
    Friend WithEvents TblInspectionstblCustomersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents bsOtherInspectors As System.Windows.Forms.BindingSource
    Friend WithEvents taDefaultText_PipeTubeMaterial As AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents taDefaultText_PipeTubeSurface As AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents taPipeTubeItemSummaries As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblPipeTubeItemSummariesTableAdapter
    Friend WithEvents bsPipeTubeItemsSummaries As System.Windows.Forms.BindingSource
    Friend WithEvents bsQTT_Customers As System.Windows.Forms.BindingSource
    Friend WithEvents TblInspectionstblCustomersBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents taCustomers_LU As AutoReporter.QTT_LookupsDataTableAdapters.tblCustomersTableAdapter
    Friend WithEvents taInspectionsAndInspectors As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsAndInspectorsTableAdapter
    Friend WithEvents bsInspections_OtherInspectors As System.Windows.Forms.BindingSource
    Friend WithEvents bsQTT_Roles_Main As System.Windows.Forms.BindingSource
    Friend WithEvents TblQTT_RolesTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_RolesTableAdapter
    Friend WithEvents TblQTT_RolesTableAdapter1 As AutoReporter.QTT_LookupsDataTableAdapters.tblQTT_RolesTableAdapter
    Friend WithEvents bsQTT_Roles_Other As System.Windows.Forms.BindingSource
    Friend WithEvents taPipeTubeItemTypes As AutoReporter.QTT_PipeTubeDetailsTableAdapters.tblPipeTubeItemTypesTableAdapter
    Friend WithEvents bsPipeTubeTypes As System.Windows.Forms.BindingSource
    Friend WithEvents TblInspectionstblPipeTubeItemsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents taTblPipeTubeItemTypes_1 As AutoReporter.QTT_LookupsDataTableAdapters.tblPipeTubeItemTypesTableAdapter
    Friend WithEvents taInspections_TblPipeTubeItemTypes As AutoReporter.QTT_InspectionsDataSetsTableAdapters.Inspections_PipeTubeItemTypesTableAdapter
    Friend WithEvents taDefaultText_PipeTubeManufacturer As AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents bsQTT_LeadInspectors As System.Windows.Forms.BindingSource
    Friend WithEvents TblQTT_InspectorsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_InspectorsTableAdapter
    Friend WithEvents TblQTT_LeadInspectorsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_LeadInspectorsTableAdapter
    Friend WithEvents TblQTT_PrimaryRolesTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_PrimaryRolesTableAdapter
    Friend WithEvents TblQTT_OfficesTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblQTT_OfficesTableAdapter
    Friend WithEvents uiMainXtraTabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents uiInspectionDetailsTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiCustomerDetailsTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiCustomerNameLabel As System.Windows.Forms.Label
    Friend WithEvents uiLocationNameLabel As System.Windows.Forms.Label
    Friend WithEvents uiHeaterNameLabel As System.Windows.Forms.Label
    Friend WithEvents uiInspectionTypeLabel As System.Windows.Forms.Label
    Friend WithEvents uiTubeDetailsTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiInspectionSummaryTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiLinkedFilesTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiFieldReportTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiSaveButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSaveExitButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiDateLabel As System.Windows.Forms.Label
    Friend WithEvents uiRootFolderName As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents uiInspectionEndDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents uiEndDateLabel As System.Windows.Forms.Label
    Friend WithEvents uiStartDateLabel As System.Windows.Forms.Label
    Friend WithEvents uiOfficeLabel As System.Windows.Forms.Label
    Friend WithEvents uiOffices As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents uiTitleLabel As System.Windows.Forms.Label
    Friend WithEvents uiLeadInspectorLabel As System.Windows.Forms.Label
    Friend WithEvents uiTitleRole As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents uiLeadInspector As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents uiPlantNameLabel As System.Windows.Forms.Label
    Friend WithEvents uiInspectorPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiRemoveInspectorButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiAddInspectorButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiRolesList As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents uiProjectInspectorsList As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents uiOtherInspectorsList As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents uiCustomTubeFormatEditor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeFormatGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents TubeFormatRadioGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiCustomTubeFormatLabel As System.Windows.Forms.Label
    Friend WithEvents uiLabel As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents uiToolsCheckedList As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents uiProposalNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents uiProjectNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DxValidationProvider As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
    Friend WithEvents uiLocation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiCustomers As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents uiCustomerLabel As System.Windows.Forms.Label
    Friend WithEvents uiCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents uiAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents uiCountry As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiState As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents uiPriorHistoryLabel As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TblInspectionsvwReportItemsDetailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents uiPostalCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiPriorHistoryEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiGridControlContacts As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiGridViewContacts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uiProjectDescriptionTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents uiDetailedInspectionTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiExecutiveSummaryTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SpellChecker1 As DevExpress.XtraSpellChecker.SpellChecker
    Friend WithEvents FKtblPipeTubeItemSummariestblPipeTubeItemsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblContactInformationTableAdapter1 As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblContactInformationTableAdapter
    Friend WithEvents bstblContactInformation As System.Windows.Forms.BindingSource
    Friend WithEvents TblDefaultTextListItemsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents uiTubeLength As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiLengthLabel As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents uiTubeWallThicknessDisplay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeOuterDiameterDisplay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeInnerDiameterDisplay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeWallThickness As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents uiTubeOuterDiameter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents uiTubeInnerDiameter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents uiTubeServiceDateLabel As System.Windows.Forms.Label
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents uiTubeTypeLabel As System.Windows.Forms.Label
    Friend WithEvents uiOperatingHoursLabel As System.Windows.Forms.Label
    Friend WithEvents uiThermalCyclesLabel As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label121 As System.Windows.Forms.Label
    Friend WithEvents Label127 As System.Windows.Forms.Label
    Friend WithEvents uiTubeInspectionLocations As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiSummaryEditorShowHideButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiSummaryEditorButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents uiReportFilename As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label133 As System.Windows.Forms.Label
    Friend WithEvents uiViewReportButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiCreateReportButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents PaperSizeOptionGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents Label129 As System.Windows.Forms.Label
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents uiAutoSelectMainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiResetMainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiRemoveMainButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label134 As System.Windows.Forms.Label
    Friend WithEvents uiReportItemsGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiReportItemsGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDescriptionMain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colStatusMain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLinkedFileMain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEditMain As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents colSortOrderMain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFK_ReportItemType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents uiAutoSelectAppendixButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiAppendixItems As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents uiResetAppendixButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiAddMissingAppendixButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiAppendixItemsGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents uiAppendixItemsGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDescriptionAppendix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemLookUpEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colStatusAppendix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLinkedFileAppendix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEditAppendix As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents colSortOrderAppendix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPipeTubeSortOrder As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemLookUpEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemSearchLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents uiRemoveAppendixButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label135 As System.Windows.Forms.Label
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents uiShowHideButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiProjectDescriptionEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiClearRedProjectDescriptionButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiInsertProjectDescriptionButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents uiTubeIdentificationEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiClearRedTubeIdentificationButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiInsertTubeIdentificationButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents uiAddNewAppendixButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiPurchaseOrder As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents uiTubeMaterials As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiTubeInspectionAge As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPressure As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiTargetTemp As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiThermalCycles As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiOperatingHours As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents uiTubeUnitGroup As System.Windows.Forms.GroupBox
    Friend WithEvents uiLeadingUnits As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents tmrAutoSave As System.Windows.Forms.Timer
    Friend WithEvents uiAutoSaveInterval As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents uilblAutoSaveInterval As System.Windows.Forms.Label
    Friend WithEvents uilblAutoSaveUnits As System.Windows.Forms.Label
    Friend WithEvents uilblLastAutoSave As System.Windows.Forms.Label
    Friend WithEvents uiHistoryInsertSampleTextButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiInspectionStartDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents uiMonthCalendar As System.Windows.Forms.MonthCalendar
    Friend WithEvents ReportStatusLabel As System.Windows.Forms.Label
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTitle As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPhone As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmail As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFax As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFK_Inspection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrefix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents uiHistoryLabel As System.Windows.Forms.Label
    Friend WithEvents uiSummaryEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiCanCircularOption As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiCustomRowFormatEditor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiNumberOfRows As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiRowFormatGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents RowFormatRadioGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiCustomRowFormatLabel As System.Windows.Forms.Label
    Friend WithEvents uiRowsLabel As System.Windows.Forms.Label
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents uiManufacturerLabel As System.Windows.Forms.Label
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents uiFirstInspectionInsertSampleTextButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiNumberingSystem As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiTubeManufacturer As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiNumberTubesTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiNumberTubesTotalLabel As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents uiTubeDesignToleranceLabel As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label122 As System.Windows.Forms.Label
    Friend WithEvents uiMinusTubeTolerance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiPlusTubeTolerance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeDesignToleranceDisplay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents uiTolerancesGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents taTblPlantProcessTypes As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents taTblFlowDirections As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents taTblBurnerConfigurations As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents taTblReformerOEMs As QIG.AutoReporter.QTT_LookupsDataTableAdapters.tblDefaultText_ListItemsTableAdapter
    Friend WithEvents uiReformerOEM As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiPlantProcessType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiFlowDirection As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiBurnerConfiguration As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiOverallTubesInspected As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents uiRedTubeCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents uiBulgingTubeCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents uiSwellingOver3PercentCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents uiBulgingWorstPercent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiWorstTubeIDTube As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiWorstTubeIDRow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents uiOverallMaxGrowth As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiOverallMaxGrowthLocationLabel As System.Windows.Forms.Label
    Friend WithEvents uiExecSummaryGreen As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiExecSummaryYellow As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiExecSummaryRed As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiYellowTubeCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents uiDetailedSummaryGreen As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiDetailedSummaryYellow As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiDetailedSummaryRed As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiAgedTubeCount4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthLabel4 As System.Windows.Forms.Label
    Friend WithEvents uiAgedTubeCount3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthLabel3 As System.Windows.Forms.Label
    Friend WithEvents uiAgedTubeCount2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthLabel2 As System.Windows.Forms.Label
    Friend WithEvents uiAgedTubeCount1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents uiTubeFlawsGouges As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents uiTubeFlawsWelds As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents uiDetailedSummaryShowHideButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiDetailedSummaryButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiGreenCreep As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiWorstFlawTubeNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiNumberTubesWithFlaws As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents uiNewTubeCount4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeToleranceLabel4 As System.Windows.Forms.Label
    Friend WithEvents uiNewTubeCount3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeToleranceLabel3 As System.Windows.Forms.Label
    Friend WithEvents uiNewTubeCount2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeToleranceLabel2 As System.Windows.Forms.Label
    Friend WithEvents uiNewTubeCount1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeToleranceLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label118 As System.Windows.Forms.Label
    Friend WithEvents Label120 As System.Windows.Forms.Label
    Friend WithEvents Label132 As System.Windows.Forms.Label
    Friend WithEvents Label136 As System.Windows.Forms.Label
    Friend WithEvents uiInspectionSummaryEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiInspectionShowHideButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiInspectionSummaryButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents uiTubeNumberInstructionsLabel As System.Windows.Forms.Label
    Friend WithEvents uiRowNumberLabel As System.Windows.Forms.Label
    Friend WithEvents uiWorstFlawRowNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents uiDateButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiDayDateButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiMonthYearButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents uiTubeServiceDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiTubeNextInspectionDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiInspectionDetailsRowsTubesLabel As System.Windows.Forms.Label
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents uiAgedTubeCount5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthLabel5 As System.Windows.Forms.Label
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents uiMonthYearLabel As System.Windows.Forms.Label
    Friend WithEvents uiTubeFlawLocation As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiOverallMaxGrowthLocation As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents bsInspections_AgedSummary As System.Windows.Forms.BindingSource
    Friend WithEvents TblReformerAgedSummaryTableAdapter As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter
    Friend WithEvents bsInspections_NewSummary As System.Windows.Forms.BindingSource
    Friend WithEvents TblReformerNewSummaryTableAdapter As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter
    Friend WithEvents taTblReformerAgedSummary As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerAgedSummaryTableAdapter
    Friend WithEvents taTblReformerNewSummary As QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblReformerNewSummaryTableAdapter
    Friend WithEvents TblInspectionstblReformerAgedSummaryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label138 As System.Windows.Forms.Label
    Friend WithEvents uiMaxGrowthLocation As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label139 As System.Windows.Forms.Label
    Friend WithEvents Label140 As System.Windows.Forms.Label
    Friend WithEvents Label141 As System.Windows.Forms.Label
    Friend WithEvents Label142 As System.Windows.Forms.Label
    Friend WithEvents uiDiaGrowthMaxPercent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthTubeID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthRowID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiOperatingPeriod As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label146 As System.Windows.Forms.Label
    Friend WithEvents uiLevel1Label As System.Windows.Forms.Label
    Friend WithEvents uiNumberOfTubes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDegreesMarker As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents uiTubeLengthDisplay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiDiaGrowthTotalLabel As System.Windows.Forms.Label
    Friend WithEvents uiSpareUninstalledGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents colReportItemType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents uiImageNameFormat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiImageNameFormatLabel As System.Windows.Forms.Label
    Friend WithEvents Label125 As System.Windows.Forms.Label
    Friend WithEvents uiSampleLevel3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents uiCracking As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiCrackingLabel As System.Windows.Forms.Label
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents uiFFSEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents uiDetailedSummaryTabControl As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents DiaGrowthSummaryTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiDiaGrowthSummaryEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents FFSSummaryTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ExecSummaryTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiExecSummaryDetailsEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents TubeHarvestingTabPage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents uiTubeHarvestingEditor As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents ToolTipController As DevExpress.Utils.ToolTipController
    Friend WithEvents uiClearPriorHistoryRedButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents uiNumberTubesInspected As DevExpress.XtraEditors.TextEdit
    Friend WithEvents uiReformerName As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiReformerID As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents uiJMC As DevExpress.XtraEditors.CheckEdit
End Class
