<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
   Inherits System.Windows.Forms.Form

   'Form overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      If disposing AndAlso components IsNot Nothing Then
         components.Dispose()
      End If
      MyBase.Dispose(disposing)
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim bsCustomers As System.Windows.Forms.BindingSource
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode3 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode4 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode5 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode6 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode7 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode8 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode9 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.QTT_InspectionsDataSets = New QIG.AutoReporter.QTT_InspectionsDataSets()
        Me.ApplicationTitle = New System.Windows.Forms.Label()
        Me.bsInspectionTypes = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblExistingInspections = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TblCustomersTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter()
        Me.bsInspections = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblInspectionsTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter()
        Me.TblInspectionTypesTableAdapter = New QIG.AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter()
        Me.Version = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblNewInspection = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.uiGridControlInspections = New DevExpress.XtraGrid.GridControl()
        Me.uiGridViewInspections = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInspectionTypeDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCustomerName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInspectionDateStart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLocationDesc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnitName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnitNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProjectNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNumberOfPasses = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.uiCreateInspectionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiOpenInspectionButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiExitButton = New DevExpress.XtraEditors.SimpleButton()
        Me.uiDeleteInspection = New DevExpress.XtraEditors.SimpleButton()
        Me.lblDeleteInspection = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        bsCustomers = New System.Windows.Forms.BindingSource(Me.components)
        CType(bsCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QTT_InspectionsDataSets, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsInspectionTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsInspections, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiGridControlInspections, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uiGridViewInspections, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bsCustomers
        '
        bsCustomers.DataMember = "tblCustomers"
        bsCustomers.DataSource = Me.QTT_InspectionsDataSets
        '
        'QTT_InspectionsDataSets
        '
        Me.QTT_InspectionsDataSets.DataSetName = "QTT_InspectionsDataSets"
        Me.QTT_InspectionsDataSets.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ApplicationTitle
        '
        Me.ApplicationTitle.AutoSize = True
        Me.ApplicationTitle.Font = New System.Drawing.Font("Segoe UI", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ApplicationTitle.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.ApplicationTitle.Location = New System.Drawing.Point(11, 11)
        Me.ApplicationTitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.ApplicationTitle.Name = "ApplicationTitle"
        Me.ApplicationTitle.Size = New System.Drawing.Size(478, 40)
        Me.ApplicationTitle.TabIndex = 0
        Me.ApplicationTitle.Text = "LOTIS/MANTIS SMR AutoReporter"
        '
        'bsInspectionTypes
        '
        Me.bsInspectionTypes.DataMember = "tblInspectionTypes"
        Me.bsInspectionTypes.DataSource = Me.QTT_InspectionsDataSets
        '
        'lblExistingInspections
        '
        Me.lblExistingInspections.AutoSize = True
        Me.lblExistingInspections.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExistingInspections.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblExistingInspections.Location = New System.Drawing.Point(11, 154)
        Me.lblExistingInspections.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblExistingInspections.Name = "lblExistingInspections"
        Me.lblExistingInspections.Size = New System.Drawing.Size(223, 21)
        Me.lblExistingInspections.TabIndex = 4
        Me.lblExistingInspections.Text = "Open an Existing Inspection"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label6.Location = New System.Drawing.Point(42, 175)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(634, 22)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Select an inspection from the list below. Limit the list by using the column head" & _
    "ers to filter or click to sort."
        '
        'TblCustomersTableAdapter
        '
        Me.TblCustomersTableAdapter.ClearBeforeFill = True
        '
        'bsInspections
        '
        Me.bsInspections.DataMember = "tblInspections"
        Me.bsInspections.DataSource = Me.QTT_InspectionsDataSets
        '
        'TblInspectionsTableAdapter
        '
        Me.TblInspectionsTableAdapter.ClearBeforeFill = True
        '
        'TblInspectionTypesTableAdapter
        '
        Me.TblInspectionTypesTableAdapter.ClearBeforeFill = True
        '
        'Version
        '
        Me.Version.AutoSize = True
        Me.Version.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Version.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Version.Location = New System.Drawing.Point(15, 60)
        Me.Version.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Version.Name = "Version"
        Me.Version.Size = New System.Drawing.Size(91, 13)
        Me.Version.TabIndex = 1
        Me.Version.Text = "Version {0}.{1:00}"
        Me.Version.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(12, 586)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(112, 13)
        Me.LinkLabel1.TabIndex = 18
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "System Maintenance"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1075, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(242, 89)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'lblNewInspection
        '
        Me.lblNewInspection.AutoSize = True
        Me.lblNewInspection.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewInspection.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblNewInspection.Location = New System.Drawing.Point(11, 107)
        Me.lblNewInspection.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblNewInspection.Name = "lblNewInspection"
        Me.lblNewInspection.Size = New System.Drawing.Size(195, 21)
        Me.lblNewInspection.TabIndex = 2
        Me.lblNewInspection.Text = "Create a New Inspection"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label4.Location = New System.Drawing.Point(42, 128)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(634, 25)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "New inspections can be created from scratch or by starting with key data imported" & _
    " from an existing inspection."
        '
        'uiGridControlInspections
        '
        Me.uiGridControlInspections.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiGridControlInspections.DataSource = Me.bsInspections
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.First.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.uiGridControlInspections.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.uiGridControlInspections.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GridLevelNode1.RelationName = "tblInspectionstblInspectionsAndTools"
        GridLevelNode3.RelationName = "vwReportItems_Details_tblReportImageItems"
        GridLevelNode2.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode3})
        GridLevelNode2.RelationName = "tblInspections_vwReportItems_Details"
        GridLevelNode5.RelationName = "vwAppendixDetails_vwAppendixItem_Details"
        GridLevelNode4.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode5})
        GridLevelNode4.RelationName = "tblInspections_vwAppendixDetails"
        GridLevelNode6.RelationName = "tblInspections_tblInspectionsAndInspectors"
        GridLevelNode8.RelationName = "FK_tblPipeTubeItemSummaries_tblPipeTubeItems"
        GridLevelNode9.RelationName = "tblPipeTubeItems_vwAppendixItem_Details"
        GridLevelNode7.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode8, GridLevelNode9})
        GridLevelNode7.RelationName = "tblInspectionstblPipeTubeItems"
        Me.uiGridControlInspections.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1, GridLevelNode2, GridLevelNode4, GridLevelNode6, GridLevelNode7})
        Me.uiGridControlInspections.Location = New System.Drawing.Point(11, 269)
        Me.uiGridControlInspections.LookAndFeel.SkinName = "Blue"
        Me.uiGridControlInspections.LookAndFeel.UseDefaultLookAndFeel = False
        Me.uiGridControlInspections.MainView = Me.uiGridViewInspections
        Me.uiGridControlInspections.Name = "uiGridControlInspections"
        Me.uiGridControlInspections.Size = New System.Drawing.Size(1292, 306)
        Me.uiGridControlInspections.TabIndex = 46
        Me.uiGridControlInspections.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.uiGridViewInspections})
        '
        'uiGridViewInspections
        '
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.Empty.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.Empty.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.uiGridViewInspections.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.EvenRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.EvenRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FilterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FilterPanel.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FilterPanel.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FixedLine.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.FocusedCell.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FocusedCell.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.FocusedRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FocusedRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.FooterPanel.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.GroupButton.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.GroupFooter.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.GroupPanel.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.GroupPanel.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.GroupRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(81, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(158, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.uiGridViewInspections.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(195, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.uiGridViewInspections.Appearance.HorzLine.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.uiGridViewInspections.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.OddRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.OddRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.uiGridViewInspections.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
        Me.uiGridViewInspections.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.uiGridViewInspections.Appearance.Preview.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.Preview.Options.UseFont = True
        Me.uiGridViewInspections.Appearance.Preview.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.uiGridViewInspections.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.uiGridViewInspections.Appearance.Row.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.Row.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.RowSeparator.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(179, Byte), Integer))
        Me.uiGridViewInspections.Appearance.SelectedRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.uiGridViewInspections.Appearance.SelectedRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.SelectedRow.Options.UseForeColor = True
        Me.uiGridViewInspections.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
        Me.uiGridViewInspections.Appearance.TopNewRow.Options.UseBackColor = True
        Me.uiGridViewInspections.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(167, Byte), Integer), CType(CType(178, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.uiGridViewInspections.Appearance.VertLine.Options.UseBackColor = True
        Me.uiGridViewInspections.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colInspectionTypeDescription, Me.colCustomerName, Me.colInspectionDateStart, Me.colLocationDesc, Me.colCity, Me.colCountry, Me.colUnitName, Me.colUnitNumber, Me.colProjectNumber, Me.colNumberOfPasses})
        Me.uiGridViewInspections.GridControl = Me.uiGridControlInspections
        Me.uiGridViewInspections.Name = "uiGridViewInspections"
        Me.uiGridViewInspections.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiGridViewInspections.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.uiGridViewInspections.OptionsBehavior.AutoSelectAllInEditor = False
        Me.uiGridViewInspections.OptionsBehavior.AutoUpdateTotalSummary = False
        Me.uiGridViewInspections.OptionsBehavior.Editable = False
        Me.uiGridViewInspections.OptionsBehavior.ReadOnly = True
        Me.uiGridViewInspections.OptionsCustomization.AllowGroup = False
        Me.uiGridViewInspections.OptionsCustomization.AllowQuickHideColumns = False
        Me.uiGridViewInspections.OptionsDetail.AllowZoomDetail = False
        Me.uiGridViewInspections.OptionsDetail.EnableMasterViewMode = False
        Me.uiGridViewInspections.OptionsDetail.ShowDetailTabs = False
        Me.uiGridViewInspections.OptionsDetail.SmartDetailExpand = False
        Me.uiGridViewInspections.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.uiGridViewInspections.OptionsView.EnableAppearanceEvenRow = True
        Me.uiGridViewInspections.OptionsView.EnableAppearanceOddRow = True
        Me.uiGridViewInspections.OptionsView.ShowGroupPanel = False
        Me.uiGridViewInspections.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCustomerName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID
        '
        Me.colID.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colID.AppearanceCell.Options.UseFont = True
        Me.colID.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colID.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colID.AppearanceHeader.Options.UseFont = True
        Me.colID.AppearanceHeader.Options.UseForeColor = True
        Me.colID.Caption = "ID"
        Me.colID.Name = "colID"
        '
        'colInspectionTypeDescription
        '
        Me.colInspectionTypeDescription.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colInspectionTypeDescription.AppearanceCell.Options.UseFont = True
        Me.colInspectionTypeDescription.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colInspectionTypeDescription.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseFont = True
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseForeColor = True
        Me.colInspectionTypeDescription.AppearanceHeader.Options.UseTextOptions = True
        Me.colInspectionTypeDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colInspectionTypeDescription.Caption = "Inspection Type"
        Me.colInspectionTypeDescription.FieldName = "InspectionTypeDesciption"
        Me.colInspectionTypeDescription.Name = "colInspectionTypeDescription"
        Me.colInspectionTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colInspectionTypeDescription.Visible = True
        Me.colInspectionTypeDescription.VisibleIndex = 0
        Me.colInspectionTypeDescription.Width = 131
        '
        'colCustomerName
        '
        Me.colCustomerName.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colCustomerName.AppearanceCell.Options.UseFont = True
        Me.colCustomerName.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colCustomerName.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCustomerName.AppearanceHeader.Options.UseFont = True
        Me.colCustomerName.AppearanceHeader.Options.UseForeColor = True
        Me.colCustomerName.AppearanceHeader.Options.UseTextOptions = True
        Me.colCustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCustomerName.Caption = "Customer"
        Me.colCustomerName.FieldName = "CustomerName"
        Me.colCustomerName.Name = "colCustomerName"
        Me.colCustomerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCustomerName.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
        Me.colCustomerName.Visible = True
        Me.colCustomerName.VisibleIndex = 1
        Me.colCustomerName.Width = 127
        '
        'colInspectionDateStart
        '
        Me.colInspectionDateStart.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colInspectionDateStart.AppearanceCell.Options.UseFont = True
        Me.colInspectionDateStart.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colInspectionDateStart.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colInspectionDateStart.AppearanceHeader.Options.UseFont = True
        Me.colInspectionDateStart.AppearanceHeader.Options.UseForeColor = True
        Me.colInspectionDateStart.AppearanceHeader.Options.UseTextOptions = True
        Me.colInspectionDateStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colInspectionDateStart.Caption = "Inspection Date"
        Me.colInspectionDateStart.FieldName = "InspectionDateStart"
        Me.colInspectionDateStart.Name = "colInspectionDateStart"
        Me.colInspectionDateStart.Visible = True
        Me.colInspectionDateStart.VisibleIndex = 2
        Me.colInspectionDateStart.Width = 101
        '
        'colLocationDesc
        '
        Me.colLocationDesc.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colLocationDesc.AppearanceCell.Options.UseFont = True
        Me.colLocationDesc.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colLocationDesc.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colLocationDesc.AppearanceHeader.Options.UseFont = True
        Me.colLocationDesc.AppearanceHeader.Options.UseForeColor = True
        Me.colLocationDesc.AppearanceHeader.Options.UseTextOptions = True
        Me.colLocationDesc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colLocationDesc.Caption = "Plant Name"
        Me.colLocationDesc.FieldName = "LocationDesc"
        Me.colLocationDesc.Name = "colLocationDesc"
        Me.colLocationDesc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colLocationDesc.Visible = True
        Me.colLocationDesc.VisibleIndex = 3
        Me.colLocationDesc.Width = 96
        '
        'colCity
        '
        Me.colCity.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colCity.AppearanceCell.Options.UseFont = True
        Me.colCity.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colCity.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCity.AppearanceHeader.Options.UseFont = True
        Me.colCity.AppearanceHeader.Options.UseForeColor = True
        Me.colCity.AppearanceHeader.Options.UseTextOptions = True
        Me.colCity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCity.Caption = "City"
        Me.colCity.FieldName = "City"
        Me.colCity.Name = "colCity"
        Me.colCity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCity.Visible = True
        Me.colCity.VisibleIndex = 4
        Me.colCity.Width = 77
        '
        'colCountry
        '
        Me.colCountry.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colCountry.AppearanceCell.Options.UseFont = True
        Me.colCountry.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colCountry.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colCountry.AppearanceHeader.Options.UseFont = True
        Me.colCountry.AppearanceHeader.Options.UseForeColor = True
        Me.colCountry.AppearanceHeader.Options.UseTextOptions = True
        Me.colCountry.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colCountry.Caption = "Country"
        Me.colCountry.FieldName = "Country"
        Me.colCountry.Name = "colCountry"
        Me.colCountry.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colCountry.Visible = True
        Me.colCountry.VisibleIndex = 5
        Me.colCountry.Width = 77
        '
        'colUnitName
        '
        Me.colUnitName.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colUnitName.AppearanceCell.Options.UseFont = True
        Me.colUnitName.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colUnitName.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colUnitName.AppearanceHeader.Options.UseFont = True
        Me.colUnitName.AppearanceHeader.Options.UseForeColor = True
        Me.colUnitName.AppearanceHeader.Options.UseTextOptions = True
        Me.colUnitName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colUnitName.Caption = "Reformer Name"
        Me.colUnitName.FieldName = "UnitName"
        Me.colUnitName.Name = "colUnitName"
        Me.colUnitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colUnitName.Visible = True
        Me.colUnitName.VisibleIndex = 6
        Me.colUnitName.Width = 77
        '
        'colUnitNumber
        '
        Me.colUnitNumber.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colUnitNumber.AppearanceCell.Options.UseFont = True
        Me.colUnitNumber.AppearanceCell.Options.UseTextOptions = True
        Me.colUnitNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colUnitNumber.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colUnitNumber.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colUnitNumber.AppearanceHeader.Options.UseFont = True
        Me.colUnitNumber.AppearanceHeader.Options.UseForeColor = True
        Me.colUnitNumber.AppearanceHeader.Options.UseTextOptions = True
        Me.colUnitNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colUnitNumber.Caption = "Reformer ID"
        Me.colUnitNumber.FieldName = "UnitNumber"
        Me.colUnitNumber.Name = "colUnitNumber"
        Me.colUnitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colUnitNumber.Visible = True
        Me.colUnitNumber.VisibleIndex = 7
        Me.colUnitNumber.Width = 77
        '
        'colProjectNumber
        '
        Me.colProjectNumber.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colProjectNumber.AppearanceCell.Options.UseFont = True
        Me.colProjectNumber.AppearanceCell.Options.UseTextOptions = True
        Me.colProjectNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colProjectNumber.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colProjectNumber.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colProjectNumber.AppearanceHeader.Options.UseFont = True
        Me.colProjectNumber.AppearanceHeader.Options.UseForeColor = True
        Me.colProjectNumber.AppearanceHeader.Options.UseTextOptions = True
        Me.colProjectNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colProjectNumber.Caption = "QIG Project #"
        Me.colProjectNumber.FieldName = "ProjectNumber"
        Me.colProjectNumber.Name = "colProjectNumber"
        Me.colProjectNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList
        Me.colProjectNumber.Visible = True
        Me.colProjectNumber.VisibleIndex = 8
        Me.colProjectNumber.Width = 94
        '
        'colNumberOfPasses
        '
        Me.colNumberOfPasses.AppearanceCell.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.colNumberOfPasses.AppearanceCell.Options.UseFont = True
        Me.colNumberOfPasses.AppearanceCell.Options.UseTextOptions = True
        Me.colNumberOfPasses.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.colNumberOfPasses.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.colNumberOfPasses.AppearanceHeader.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.colNumberOfPasses.AppearanceHeader.Options.UseFont = True
        Me.colNumberOfPasses.AppearanceHeader.Options.UseForeColor = True
        Me.colNumberOfPasses.Caption = "# of Passes"
        Me.colNumberOfPasses.FieldName = "NumberOfPasses"
        Me.colNumberOfPasses.Name = "colNumberOfPasses"
        '
        'uiCreateInspectionButton
        '
        Me.uiCreateInspectionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiCreateInspectionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiCreateInspectionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiCreateInspectionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiCreateInspectionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiCreateInspectionButton.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.uiCreateInspectionButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiCreateInspectionButton.Appearance.Options.UseBackColor = True
        Me.uiCreateInspectionButton.Appearance.Options.UseBorderColor = True
        Me.uiCreateInspectionButton.Appearance.Options.UseFont = True
        Me.uiCreateInspectionButton.Appearance.Options.UseForeColor = True
        Me.uiCreateInspectionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiCreateInspectionButton.Location = New System.Drawing.Point(1106, 150)
        Me.uiCreateInspectionButton.Name = "uiCreateInspectionButton"
        Me.uiCreateInspectionButton.Size = New System.Drawing.Size(197, 32)
        Me.uiCreateInspectionButton.TabIndex = 58
        Me.uiCreateInspectionButton.Text = "&Create New Inspection"
        '
        'uiOpenInspectionButton
        '
        Me.uiOpenInspectionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiOpenInspectionButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiOpenInspectionButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiOpenInspectionButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiOpenInspectionButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiOpenInspectionButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiOpenInspectionButton.Appearance.Options.UseBackColor = True
        Me.uiOpenInspectionButton.Appearance.Options.UseBorderColor = True
        Me.uiOpenInspectionButton.Appearance.Options.UseFont = True
        Me.uiOpenInspectionButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiOpenInspectionButton.Location = New System.Drawing.Point(1106, 188)
        Me.uiOpenInspectionButton.Name = "uiOpenInspectionButton"
        Me.uiOpenInspectionButton.Size = New System.Drawing.Size(197, 32)
        Me.uiOpenInspectionButton.TabIndex = 57
        Me.uiOpenInspectionButton.Text = "&Open Selected Inspection"
        '
        'uiExitButton
        '
        Me.uiExitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiExitButton.Appearance.BackColor = System.Drawing.Color.White
        Me.uiExitButton.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiExitButton.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiExitButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiExitButton.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiExitButton.Appearance.Options.UseBackColor = True
        Me.uiExitButton.Appearance.Options.UseBorderColor = True
        Me.uiExitButton.Appearance.Options.UseFont = True
        Me.uiExitButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiExitButton.Location = New System.Drawing.Point(1106, 581)
        Me.uiExitButton.Name = "uiExitButton"
        Me.uiExitButton.Size = New System.Drawing.Size(197, 32)
        Me.uiExitButton.TabIndex = 59
        Me.uiExitButton.Text = "E&xit"
        '
        'uiDeleteInspection
        '
        Me.uiDeleteInspection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.uiDeleteInspection.Appearance.BackColor = System.Drawing.Color.White
        Me.uiDeleteInspection.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.uiDeleteInspection.Appearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.uiDeleteInspection.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiDeleteInspection.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.uiDeleteInspection.Appearance.Options.UseBackColor = True
        Me.uiDeleteInspection.Appearance.Options.UseBorderColor = True
        Me.uiDeleteInspection.Appearance.Options.UseFont = True
        Me.uiDeleteInspection.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiDeleteInspection.Location = New System.Drawing.Point(1106, 225)
        Me.uiDeleteInspection.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.uiDeleteInspection.Name = "uiDeleteInspection"
        Me.uiDeleteInspection.Size = New System.Drawing.Size(197, 32)
        Me.uiDeleteInspection.TabIndex = 60
        Me.uiDeleteInspection.Text = "&Delete Selected Inspection"
        '
        'lblDeleteInspection
        '
        Me.lblDeleteInspection.AutoSize = True
        Me.lblDeleteInspection.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteInspection.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblDeleteInspection.Location = New System.Drawing.Point(11, 202)
        Me.lblDeleteInspection.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDeleteInspection.Name = "lblDeleteInspection"
        Me.lblDeleteInspection.Size = New System.Drawing.Size(232, 21)
        Me.lblDeleteInspection.TabIndex = 61
        Me.lblDeleteInspection.Text = "Delete an Existing Inspection"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label2.Location = New System.Drawing.Point(42, 223)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(615, 34)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Select an inspection from the list below. Clicking ""Delete"" will remove it from t" & _
    "he database and move all files to the ""xDeletedInspections"" folder."
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1324, 619)
        Me.Controls.Add(Me.lblDeleteInspection)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.uiDeleteInspection)
        Me.Controls.Add(Me.uiExitButton)
        Me.Controls.Add(Me.uiCreateInspectionButton)
        Me.Controls.Add(Me.uiOpenInspectionButton)
        Me.Controls.Add(Me.uiGridControlInspections)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblNewInspection)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.Version)
        Me.Controls.Add(Me.lblExistingInspections)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ApplicationTitle)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(912, 398)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inspection Home"
        CType(bsCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QTT_InspectionsDataSets, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsInspectionTypes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsInspections, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiGridControlInspections, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uiGridViewInspections, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
   Friend WithEvents ApplicationTitle As System.Windows.Forms.Label
   Friend WithEvents lblExistingInspections As System.Windows.Forms.Label
   Friend WithEvents Label6 As System.Windows.Forms.Label
   Friend WithEvents QTT_InspectionsDataSets As AutoReporter.QTT_InspectionsDataSets
   Friend WithEvents TblCustomersTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblCustomersTableAdapter
   Friend WithEvents bsInspections As System.Windows.Forms.BindingSource
   Friend WithEvents TblInspectionsTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionsTableAdapter
   Friend WithEvents bsInspectionTypes As System.Windows.Forms.BindingSource
   Friend WithEvents TblInspectionTypesTableAdapter As AutoReporter.QTT_InspectionsDataSetsTableAdapters.tblInspectionTypesTableAdapter
   Friend WithEvents IDDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
   Friend WithEvents Version As System.Windows.Forms.Label
   Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
   Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
   Friend WithEvents lblNewInspection As System.Windows.Forms.Label
   Friend WithEvents Label4 As System.Windows.Forms.Label
   Friend WithEvents uiGridControlInspections As DevExpress.XtraGrid.GridControl
   Friend WithEvents uiGridViewInspections As DevExpress.XtraGrid.Views.Grid.GridView
   Friend WithEvents colInspectionTypeDescription As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCustomerName As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colInspectionDateStart As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colLocationDesc As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCity As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colCountry As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colUnitName As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colUnitNumber As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colProjectNumber As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
   Friend WithEvents uiCreateInspectionButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents uiOpenInspectionButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents uiExitButton As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents uiDeleteInspection As DevExpress.XtraEditors.SimpleButton
   Friend WithEvents lblDeleteInspection As System.Windows.Forms.Label
   Friend WithEvents Label2 As System.Windows.Forms.Label
   Friend WithEvents colNumberOfPasses As DevExpress.XtraGrid.Columns.GridColumn
End Class
