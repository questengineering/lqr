<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fdlgAddAppendixItems
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.cboImageFileTypes = New System.Windows.Forms.ComboBox()
        Me.cboRelatedPipeTube = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblRelatedPTI = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.AppendixAddToOptionGroup = New System.Windows.Forms.GroupBox()
        Me.rbAddTo_All = New System.Windows.Forms.RadioButton()
        Me.rbAddTo_Selected = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.uiNumberOfItems = New DevExpress.XtraEditors.SpinEdit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.AppendixAddToOptionGroup.SuspendLayout()
        CType(Me.uiNumberOfItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(285, 155)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'cboImageFileTypes
        '
        Me.cboImageFileTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImageFileTypes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImageFileTypes.FormattingEnabled = True
        Me.cboImageFileTypes.Location = New System.Drawing.Point(19, 75)
        Me.cboImageFileTypes.Name = "cboImageFileTypes"
        Me.cboImageFileTypes.Size = New System.Drawing.Size(407, 25)
        Me.cboImageFileTypes.TabIndex = 2
        '
        'cboRelatedPipeTube
        '
        Me.cboRelatedPipeTube.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelatedPipeTube.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelatedPipeTube.FormattingEnabled = True
        Me.cboRelatedPipeTube.Location = New System.Drawing.Point(19, 270)
        Me.cboRelatedPipeTube.Name = "cboRelatedPipeTube"
        Me.cboRelatedPipeTube.Size = New System.Drawing.Size(407, 25)
        Me.cboRelatedPipeTube.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label1.Location = New System.Drawing.Point(19, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(315, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Add Image File (or other linked file) to Appendix:"
        '
        'lblRelatedPTI
        '
        Me.lblRelatedPTI.AutoSize = True
        Me.lblRelatedPTI.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelatedPTI.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblRelatedPTI.Location = New System.Drawing.Point(16, 250)
        Me.lblRelatedPTI.Name = "lblRelatedPTI"
        Me.lblRelatedPTI.Size = New System.Drawing.Size(177, 17)
        Me.lblRelatedPTI.TabIndex = 3
        Me.lblRelatedPTI.Text = "Related Piping / Tubing Item:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label3.Location = New System.Drawing.Point(19, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Image / File Type:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label4.Location = New System.Drawing.Point(19, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(216, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Number of Items/Pairs/Sets to Add:"
        '
        'AppendixAddToOptionGroup
        '
        Me.AppendixAddToOptionGroup.Controls.Add(Me.rbAddTo_All)
        Me.AppendixAddToOptionGroup.Controls.Add(Me.rbAddTo_Selected)
        Me.AppendixAddToOptionGroup.Location = New System.Drawing.Point(19, 205)
        Me.AppendixAddToOptionGroup.Name = "AppendixAddToOptionGroup"
        Me.AppendixAddToOptionGroup.Size = New System.Drawing.Size(407, 38)
        Me.AppendixAddToOptionGroup.TabIndex = 11
        Me.AppendixAddToOptionGroup.TabStop = False
        Me.AppendixAddToOptionGroup.Visible = False
        '
        'rbAddTo_All
        '
        Me.rbAddTo_All.AutoSize = True
        Me.rbAddTo_All.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAddTo_All.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.rbAddTo_All.Location = New System.Drawing.Point(184, 10)
        Me.rbAddTo_All.Name = "rbAddTo_All"
        Me.rbAddTo_All.Size = New System.Drawing.Size(217, 21)
        Me.rbAddTo_All.TabIndex = 1
        Me.rbAddTo_All.Text = "All Appendixes (of selected type)"
        Me.rbAddTo_All.UseVisualStyleBackColor = True
        '
        'rbAddTo_Selected
        '
        Me.rbAddTo_Selected.AutoSize = True
        Me.rbAddTo_Selected.Checked = True
        Me.rbAddTo_Selected.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAddTo_Selected.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.rbAddTo_Selected.Location = New System.Drawing.Point(8, 10)
        Me.rbAddTo_Selected.Name = "rbAddTo_Selected"
        Me.rbAddTo_Selected.Size = New System.Drawing.Size(164, 21)
        Me.rbAddTo_Selected.TabIndex = 0
        Me.rbAddTo_Selected.TabStop = True
        Me.rbAddTo_Selected.Text = "Selected Appendix Only"
        Me.rbAddTo_Selected.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.Label2.Location = New System.Drawing.Point(16, 192)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Add Item(s) to:"
        Me.Label2.Visible = False
        '
        'uiNumberOfItems
        '
        Me.uiNumberOfItems.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.uiNumberOfItems.Location = New System.Drawing.Point(240, 115)
        Me.uiNumberOfItems.Name = "uiNumberOfItems"
        Me.uiNumberOfItems.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNumberOfItems.Properties.Appearance.Options.UseFont = True
        Me.uiNumberOfItems.Properties.Appearance.Options.UseTextOptions = True
        Me.uiNumberOfItems.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfItems.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNumberOfItems.Properties.AppearanceDisabled.Options.UseFont = True
        Me.uiNumberOfItems.Properties.AppearanceDisabled.Options.UseTextOptions = True
        Me.uiNumberOfItems.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfItems.Properties.AppearanceFocused.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNumberOfItems.Properties.AppearanceFocused.Options.UseFont = True
        Me.uiNumberOfItems.Properties.AppearanceFocused.Options.UseTextOptions = True
        Me.uiNumberOfItems.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.uiNumberOfItems.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uiNumberOfItems.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.uiNumberOfItems.Properties.AppearanceReadOnly.Options.UseTextOptions = True
        Me.uiNumberOfItems.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        SerializableAppearanceObject1.BackColor = System.Drawing.Color.White
        SerializableAppearanceObject1.BackColor2 = System.Drawing.Color.LightSteelBlue
        SerializableAppearanceObject1.BorderColor = System.Drawing.Color.SteelBlue
        SerializableAppearanceObject1.Options.UseBackColor = True
        SerializableAppearanceObject1.Options.UseBorderColor = True
        Me.uiNumberOfItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, True)})
        Me.uiNumberOfItems.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.uiNumberOfItems.Properties.EditFormat.FormatString = "#"
        Me.uiNumberOfItems.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.uiNumberOfItems.Properties.EditValueChangedDelay = 1
        Me.uiNumberOfItems.Properties.IsFloatValue = False
        Me.uiNumberOfItems.Properties.Mask.EditMask = "N00"
        Me.uiNumberOfItems.Properties.MaxValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.uiNumberOfItems.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.uiNumberOfItems.Size = New System.Drawing.Size(55, 24)
        Me.uiNumberOfItems.TabIndex = 13
        '
        'fdlgAddAppendixItems
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(443, 196)
        Me.Controls.Add(Me.uiNumberOfItems)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblRelatedPTI)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboRelatedPipeTube)
        Me.Controls.Add(Me.cboImageFileTypes)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.AppendixAddToOptionGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fdlgAddAppendixItems"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Linked File to Appendix"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.AppendixAddToOptionGroup.ResumeLayout(False)
        Me.AppendixAddToOptionGroup.PerformLayout()
        CType(Me.uiNumberOfItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
   Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
   Friend WithEvents OK_Button As System.Windows.Forms.Button
   Friend WithEvents Cancel_Button As System.Windows.Forms.Button
   Friend WithEvents cboImageFileTypes As System.Windows.Forms.ComboBox
   Friend WithEvents cboRelatedPipeTube As System.Windows.Forms.ComboBox
   Friend WithEvents Label1 As System.Windows.Forms.Label
   Friend WithEvents lblRelatedPTI As System.Windows.Forms.Label
   Friend WithEvents Label3 As System.Windows.Forms.Label
   Friend WithEvents Label4 As System.Windows.Forms.Label
   Friend WithEvents AppendixAddToOptionGroup As System.Windows.Forms.GroupBox
   Friend WithEvents rbAddTo_All As System.Windows.Forms.RadioButton
   Friend WithEvents rbAddTo_Selected As System.Windows.Forms.RadioButton
   Friend WithEvents Label2 As System.Windows.Forms.Label
   Friend WithEvents uiNumberOfItems As DevExpress.XtraEditors.SpinEdit

End Class
