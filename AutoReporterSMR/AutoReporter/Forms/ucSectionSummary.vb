﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.Utils
Imports DevExpress.XtraRichEdit.API.Native
Imports System.Text.RegularExpressions

Public Class ucSectionSummary
   Dim MySummaryData As SummaryData
   Dim g_intSectionSummaryID As Integer 'Contains the current Section Summary ID
   Dim g_intPipeOrBendDataID As Integer 'Contains the current pipeOrBendData ID from the database.
   Dim g_InspectionNumber As Integer 'Stores the Inspection Number
   Dim g_Appendix As String 'Stores the appendix letter
    Public g_PassNumber As Integer 'Stores the current pass number
    Public g_PassName As String 'Stores the current pass name
   Dim G_bvisible4 As Boolean = False 'marks whether the paragraph formatting is visible or not
   Dim g_CurrentPipeTubeItemID As Integer 'Stores the ID of the current PipeTubeItemID from the database
   Dim g_CurrentPipeTubeSummaryID As Integer 'Stores the current PipeTubeSummaryID from the databse.
    Dim g_ConvectionOrRadiant As String
    Dim g_PipeDiameter As String 'Holds the pipe diameter, such as 4 in the string '4 in schedule 80'. 
    Dim g_PipeSchedule As String 'holds the pipe schedule, such as 80 in the above example.
    Dim g_PipeSectionNames As String() 'an array of the pipe schedules/names
    Public g_TabVisible As Boolean 'stores whether this user control is visible or not. Set by Inspection Details during a tab page change


#Region "Form Load Subroutines"

    Public Sub LoadSectionSummaryData(ByVal intInspectionNumber As Integer, ByVal intPassNumber As Integer, ByVal strPass As String)
        ToggleButtons("Enable")
        'Set the current pass name and number
        g_PassName = strPass
        g_PassNumber = intPassNumber
        g_InspectionNumber = intInspectionNumber

        'Fill the table adapters associated with this Section summary. If the Section Summary Doesn't Exist, create one. 
        g_intSectionSummaryID = GetSectionSummaryID(intInspectionNumber, intPassNumber)
        'Check if there's an section summary already in the database
        If g_intSectionSummaryID = -1 Then
            CreateEmptySectionSummaryInDB(intInspectionNumber, intPassNumber)
            g_intSectionSummaryID = GetSectionSummaryID(intInspectionNumber, intPassNumber)
        End If

        'Gather all the data we'll be using from the database
        taTblSummary.Fill(Me.QTT_SummaryData.tblSummary)
        bsSectionSummary.Filter = "ID = " & g_intSectionSummaryID
        taTblSummaryBendData.Fill(Me.QTT_SummaryData.tblSummaryBendData)
        taTblSummaryPipeData.Fill(Me.QTT_SummaryData.tblSummaryPipeData)
        taTblSummaryBendDataInfo.Fill(Me.QTT_SummaryData.tblSummaryBendDataInfo)
        taTblSummaryPipeDataInfo.Fill(Me.QTT_SummaryData.tblSummaryPipeDataInfo)
        taTblPipeTubeItems.Fill(Me.QTT_SummaryData.tblPipeTubeItems)
        taTblPipeTubeItemSummaries.Fill(Me.QTT_SummaryData.tblPipeTubeItemSummaries)

        'Load up the first PipeTubeItem associated with this inspection and set that as the current.
        g_CurrentPipeTubeItemID = Me.QTT_SummaryData.tblPipeTubeItems.Select("FK_Inspection = " & g_InspectionNumber)(0).Item("ID")
        TblPipeTubeItemsBindingSource.Filter = "FK_Inspection = " & g_InspectionNumber

        'Make sure the proper summary is shown, even if it is blank.
        g_CurrentPipeTubeSummaryID = GetCurrentPipeTubeSummary(g_CurrentPipeTubeItemID, g_PassNumber)
        bsSectionSummaries.Filter = "ID = " & g_CurrentPipeTubeSummaryID

        'Get all the Pipe Section names that are available for having summaries
        UpdateAvailablePipeSections(False) 'Just update the available pipe names for this pipe section 'IE: Convection Section'
        If IsNothing(g_PipeSectionNames) And g_TabVisible = True Then ' there are no pipe schedules to write a summary for and this form can't work.

            MessageBox.Show("No sections have pipe segments that can be modified at this time. Add pipe names/schedules in the Pipe Details tab before trying to write summaries.")
            ToggleButtons("Disable")
            Return
        ElseIf IsNothing(g_PipeSectionNames) Then
            ToggleButtons("Disable")
            Return
        End If
          'Tell the controls associated with the pipe or bend data which type is being shown, 'Pipe' by default.
        bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"

        'tell the gridview what its data source will be and set it up.
        uiPipeOrBendDataInfo.DataSource = bsSummaryPipeDataInfo
        UpdateGridViewColumns(uiPipeOrBendDataInfoView, "Pipe")
        radPipeOrBend.SelectedIndex = 0

        CreateSectionScheduleDataPieces() 'Create database items for each schedule in the section, like 4 in. Schedule 80
        UpdateAvailablePipeSections(True) 'Actually select one of the pipe names while updating the available pipe names
        'reset the current section so it flags that the data needs to be updated in the 'uiSectionSelection EditValue changed event'
        If uiSectionSelection.EditValue = g_CurrentPipeTubeItemID Then uiSectionSelection.EditValue = -1
        uiSectionSelection.EditValue = g_CurrentPipeTubeItemID
        UpdateCurrentPipeOrBendDataID()
        SetPipeBendDataInfoFilter()


    End Sub
#End Region
   Private Function GetCurrentPipeTubeSummary(ByVal PipeTubeItemID As Integer, ByVal intPassNumber As Integer)
        'This will get the current PipeTubeItem's Summary. If it doesn't have one, it will create one and return it.
        bsSectionSummaries.Filter = "FK_PipeTubeItem = " & PipeTubeItemID & " and PassNumber = " & intPassNumber
        If bsSectionSummaries.Count > 0 Then Return bsSectionSummaries.Item(0)("ID")
        taTblPipeTubeItemSummaries.Insert(PipeTubeItemID, Nothing, intPassNumber)
        taTblPipeTubeItemSummaries.Update(Me.QTT_SummaryData.tblPipeTubeItemSummaries)
        bsSectionSummaries.Filter = "FK_PipeTubeItem = " & PipeTubeItemID & " and PassNumber = " & intPassNumber
        If bsSectionSummaries.Count > 0 Then Return bsSectionSummaries.Item(0)("ID")
    End Function
    Private Sub SetPipeBendDataInfoFilter()
        'if there aren't any pipe schedules selected, then don't try to load any specific ones.
        If bsSummaryPipeOrBendData.Count = 0 Then
            bsSummaryPipeDataInfo.Filter = "FK_PipeTubeItemID = " & g_CurrentPipeTubeItemID
            ToggleButtons("Disable")
            MessageBox.Show("No Pipe Schedules have been selected for this section of piping. Select schedules on the Pipe Details screen first before attempting to write a summary.")
        Else
            bsSummaryPipeDataInfo.Filter = "FK_SummaryPipeData = " & bsSummaryPipeOrBendData.Item(0)("ID") & " and FK_PipeTubeItemID = " & g_CurrentPipeTubeItemID
            bsSummaryBendDataInfo.Filter = "FK_SummaryBendData = " & bsSummaryPipeOrBendData.Item(0)("ID") & " and FK_PipeTubeItemID = " & g_CurrentPipeTubeItemID
        End If
    End Sub
    Private Function GetCurrentPipeTubeItemID()
        Return uiSectionSelection.EditValue
    End Function
    Private Function CreateSectionScheduleDataPieces()
        'within each section there may be multiple schedules that each need their own data saved tot he database. This makes sure that there are only valid entries available.
        taTblPipeTubeItems.Fill(Me.QTT_SummaryData.tblPipeTubeItems) 'Fill this so we get the latest data
        Dim temporarySectionSummaryID As Integer = GetSectionSummaryID(g_InspectionNumber, g_PassNumber)
        'Now create the various pipe/tube items needed for this inspection. One for each different schedule in the various pipe sections
        Dim ItemRows() As QTT_SummaryData.tblPipeTubeItemsRow = Me.QTT_SummaryData.tblPipeTubeItems.Select("FK_Inspection = " & g_InspectionNumber)
        'Give each one a pipe and bend section for each schedule
        For Each PipeTubeItem As QTT_SummaryData.tblPipeTubeItemsRow In ItemRows
            If Not IsDBNull(PipeTubeItem.PipeSchedule) And Not IsNothing(PipeTubeItem.PipeSchedule) Then 'don't want to split up a null value.
                Dim strPipeNames() As String = PipeTubeItem.PipeSchedule.Split(",")
                'Remove the space at the beginning. Custom names will have a space because splitting with a comma&space doesn't work as expected
                For i = 0 To strPipeNames.Length - 1
                    If strPipeNames(i).Chars(0) = " " Then strPipeNames(i) = strPipeNames(i).Substring(1)
                Next
                'Create records for the pipe and bend Data tables
                For Each PipeSize As String In strPipeNames
                    'Check if these pieces already exist. If not, create them.
                    Dim TestRows() As QTT_SummaryData.tblSummaryBendDataRow = Me.QTT_SummaryData.tblSummaryBendData.Select("Fk_Summary = " & temporarySectionSummaryID & " AND FK_PipeTubeItemID = " & PipeTubeItem.ID & " AND PipeName = '" & PipeSize & "'")
                    If TestRows.Length = 0 Then
                        taTblSummaryBendData.CreateEmptyRecord(temporarySectionSummaryID, PipeTubeItem.ID, PipeSize)
                        taTblSummaryPipeData.CreateEmptyRecord(temporarySectionSummaryID, PipeTubeItem.ID, PipeSize)
                        'The BendDataInfo and PIpeDataInfo tables are added to by the user by initializing new rows in the datagrid on the form itself
                    End If
                Next
            End If

        Next

        'Update the database so it knows these have been created.
        taTblSummaryPipeData.Update(Me.QTT_SummaryData.tblSummaryPipeData)
        taTblSummaryBendData.Update(Me.QTT_SummaryData.tblSummaryBendData)

        g_intSectionSummaryID = temporarySectionSummaryID
    End Function
    Private Sub UpdateAvailablePipeSections(ByVal ChangeCurrentSelection As Boolean)
        uiPipeSection.Properties.Items.Clear() 'Clear out the old items
        If IsDBNull(Me.QTT_SummaryData.tblPipeTubeItems.Select("ID = " & g_CurrentPipeTubeItemID)(0).Item("PipeSchedule")) Then
            g_PipeSectionNames = Nothing
            Return 'this means there are no pipe schedules selected for this thing yet and no summaries can be written.
        End If
        'Figure out what items need to be added now from the PipeSchedules column of the PipeTubeSummaries table.
        Dim strCurrentPipeNames As String = Me.QTT_SummaryData.tblPipeTubeItems.Select("ID = " & g_CurrentPipeTubeItemID)(0).Item("PipeSchedule")
        Dim strSplitPipeNames As String() = strCurrentPipeNames.Split(",") 'because it's comma delimited, split up the values and add them to the list of selectable items
        'Remove the space at the beginning. Custom names will have a space because splitting with a comma&space doesn't work as expected
        For i = 0 To strSplitPipeNames.Length - 1
            If strSplitPipeNames(i).Chars(0) = " " Then strSplitPipeNames(i) = strSplitPipeNames(i).Substring(1)
        Next
        uiPipeSection.Properties.Items.AddRange(strSplitPipeNames)
        g_PipeSectionNames = strSplitPipeNames
        If strSplitPipeNames.Length > 0 And ChangeCurrentSelection = True Then uiPipeSection.EditValue = strSplitPipeNames(0)
    End Sub
   Private Function GetSectionSummaryID(ByVal intInspectionID As Integer, ByVal intPassNumber As Integer)
      Dim SomeDataTable() As DataRow = taTblSummary.GetData.Select("FK_Inspection = " & intInspectionID & " and ExecutiveOrSection = 'Section' and Pass = '" & intPassNumber & "'")
      If SomeDataTable.Length > 0 Then
         g_intSectionSummaryID = SomeDataTable(0).Item("ID")
      Else : g_intSectionSummaryID = -1
      End If
      Return g_intSectionSummaryID
   End Function
    Private Sub UpdateCurrentPipeOrBendDataID()

        If bsSummaryPipeOrBendData.Count > 0 Then
            g_intPipeOrBendDataID = bsSummaryPipeOrBendData.Item(0)("ID")
        Else : g_intPipeOrBendDataID = -1
        End If

    End Sub
    Private Function CreateEmptySectionSummaryInDB(ByVal intInspectionNumber As Integer, ByVal intPassNumber As Integer)
        'Create this pass's Section Summary Object itself if it doesn't already exist.
        taTblSummary.Insert(intInspectionNumber, "In-Service Pipes", "Imperial", Nothing, "Section", intPassNumber)
        taTblSummary.Update(QTT_SummaryData.tblSummary)
    End Function
   Public Sub UpdateSummaryDBTables()
      bsSummaryPipeOrBendData.EndEdit()
      bsSummaryPipeDataInfo.EndEdit()
      bsSummaryBendDataInfo.EndEdit()
      bsSectionSummary.EndEdit()
        bsSectionSummaries.EndEdit()
        taTblPipeTubeItems.Update(QTT_SummaryData.tblPipeTubeItems)
        taTblPipeTubeItemSummaries.Update(QTT_SummaryData.tblPipeTubeItemSummaries)
      taTblSummaryPipeData.Update(QTT_SummaryData.tblSummaryPipeData)
      taTblSummaryBendData.Update(QTT_SummaryData.tblSummaryBendData)
      taTblSummaryPipeDataInfo.Update(QTT_SummaryData.tblSummaryPipeDataInfo)
      taTblSummaryBendDataInfo.Update(QTT_SummaryData.tblSummaryBendDataInfo)
        taTblSummary.Update(QTT_SummaryData.tblSummary)
        taTblPipeTubeItems.Fill(QTT_SummaryData.tblPipeTubeItems)
        taTblSummaryPipeData.Fill(QTT_SummaryData.tblSummaryPipeData)
        taTblSummaryBendData.Fill(QTT_SummaryData.tblSummaryBendData)
        taTblSummaryPipeDataInfo.Fill(QTT_SummaryData.tblSummaryPipeDataInfo)
        taTblSummaryBendDataInfo.Fill(QTT_SummaryData.tblSummaryBendDataInfo)
        taTblSummary.Fill(QTT_SummaryData.tblSummary)
   End Sub

   Public Sub SetAppendix(ByVal strAppendix As String)
      g_Appendix = strAppendix
   End Sub
#Region "UI Stuff"
    Private Sub btnWriteExecutiveSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWriteSectionSummary.Click
        MySummaryData = CreateDataSummary()
        GatherRequiredInput(MySummaryData)
        Dim ConvectionOrRadiant As String
        Dim PipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        Dim TheWrittenSummary As String = WriteUpSummaryForMe(MySummaryData, g_ConvectionOrRadiant, PipeOrBend)

        'Add the section heading if this is a pipe section (don't do it for bends by default)
        If radPipeOrBend.SelectedIndex = 0 Then
            Dim strHeader As String = GetHeader()
            TheWrittenSummary = strHeader & Environment.NewLine & Environment.NewLine & TheWrittenSummary
        End If
        uiSectionSummaryTextbox.Text = TheWrittenSummary
    End Sub

    Private Function GetHeader()
        Return uiPipeSection.Text
    End Function
    Private Sub radPipeOrBend_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radPipeOrBend.SelectedIndexChanged
        UpdateSummaryDBTables()
        Dim PipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        If PipeOrBend = "Pipe" Then
            bsSummaryPipeOrBendData.DataMember = "tblSummaryPipeData"
            uiPipeOrBendDataInfo.DataSource = bsSummaryPipeDataInfo
        Else
            bsSummaryPipeOrBendData.DataMember = "tblSummaryBendData"
            uiPipeOrBendDataInfo.DataSource = bsSummaryBendDataInfo
        End If
        UpdateGridViewColumns(uiPipeOrBendDataInfoView, PipeOrBend)
    End Sub
    Private Function UpdateGridViewColumns(ByVal ThisGrid As DevExpress.XtraGrid.Views.Grid.GridView, ByVal PipeOrBend As String)
        'Grab all the columns that are in the table
        ThisGrid.PopulateColumns()
        'Now make unwanted cells invisible like the ID, Field Keys, and designators (Convection or Radiant)
        If ThisGrid.Columns.Count > 0 Then
            ThisGrid.Columns.Item("ID").Visible = False
            ThisGrid.Columns.Item("FK_PipeTubeItemID").Visible = False
            ThisGrid.Columns.Item("PipeName").Visible = False
            If PipeOrBend = "Pipe" Then
                ThisGrid.Columns.Item("FK_SummaryPipeData").Visible = False
                ThisGrid.Columns.Item("PipeID").Caption = "Pipe ID"
                ThisGrid.Columns.Item("PipeID").Width = 54
                ThisGrid.Columns.Item("AvgWall").Caption = "Average Wall"
                ThisGrid.Columns.Item("AvgWall").Width = 56
                ThisGrid.Columns.Item("DesWall").Caption = "Designed Wall"
                ThisGrid.Columns.Item("DesWall").Width = 62
                ThisGrid.Columns.Item("AvgDiam").Caption = "Average Diameter"
                ThisGrid.Columns.Item("AvgDiam").Width = 63
                ThisGrid.Columns.Item("Location").Width = 53
            End If
            If PipeOrBend = "Bend" Then
                ThisGrid.Columns.Item("FK_SummaryBendData").Visible = False
                ThisGrid.Columns.Item("BendID").Caption = "Bend ID"
                ThisGrid.Columns.Item("BendID").Width = 54
            End If

            ThisGrid.Columns.Item("MinWall").Caption = "Minimum Wall"
            ThisGrid.Columns.Item("MinWall").Width = 57
            ThisGrid.Columns.Item("EstWallLoss").Caption = "Estimated Wall Loss"
            ThisGrid.Columns.Item("EstWallLoss").Width = 64
            ThisGrid.Columns.Item("EstRemWall").Caption = "Estimated Remaining Wall"
            ThisGrid.Columns.Item("EstRemWall").Width = 89
        End If

        For i = 0 To ThisGrid.Columns.Count - 1
            With ThisGrid.Columns.Item(i).AppearanceHeader
                .TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
                .TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
                .TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
                .Font = New Font("Segoe UI", 8.25)
                .ForeColor = Color.DarkSlateBlue
            End With
            With ThisGrid.Columns.Item(i)
                .OptionsColumn.AllowMove = False
                .OptionsColumn.AllowSort = False
                .OptionsColumn.AllowSize = False
                .OptionsFilter.AllowFilter = False
                .AppearanceCell.Font = New Font("Segoe UI", 8.25)
                .AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            End With
        Next i

    End Function
    Private Sub uiPipeOrBendDataInfoView_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles uiPipeOrBendDataInfoView.InitNewRow
        'This sets default values for new rows in the gridview that cannot be null and cannot be set by the user. FK_Summar(pipeorbend)data and whether it's convection or radiant.
        Dim TheView As ColumnView = sender
        'Make sure they don't try to make more than 8 rows.
        If TheView.RowCount < 8 Then
            Dim strPipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
            Dim strFieldKeyName As String
            If strPipeOrBend = "Pipe" Then strFieldKeyName = "FK_SummaryPipeData"
            If strPipeOrBend = "Bend" Then strFieldKeyName = "FK_SummaryBendData"
            TheView.SetRowCellValue(e.RowHandle, strFieldKeyName, g_intPipeOrBendDataID)
            TheView.SetRowCellValue(e.RowHandle, "FK_PipeTubeItemID", g_CurrentPipeTubeItemID)
        Else
            TheView.DeleteRow(e.RowHandle)
        End If
    End Sub
#End Region

#Region "Data Gathering for Report Writing"
    Private Sub GatherRequiredInput(ByVal SummaryDataIAmFilling As SummaryData)
        'Fill the SummaryData object with data so it can be passed to the paragraph writing module.

        'First get the easy stuff, the stuff that can be filled right from the form.
        SummaryDataIAmFilling.Appendix = GetLetterEquivalent(g_PassNumber) 'Get the appendix item
        SummaryDataIAmFilling.GeneralOrSectionSummary = "Section" 'General because this is the executive summary, not a pass-specific summary.
        SummaryDataIAmFilling.NewUsedPipes = uiNewUsedPipes.Text
        SummaryDataIAmFilling.Units = uiUnits.Text
        SummaryDataIAmFilling.PassNumber = g_PassName

        'Now fill the more complicated items. Deformation Data First
        GetDeformationDataFromUI(SummaryDataIAmFilling.MyDeformationData)

        'Second, get the data in the gridview.
        Dim ConvectionOrRadiant As String = g_ConvectionOrRadiant
        Dim PipeOrBend As String = radPipeOrBend.Properties.Items(radPipeOrBend.SelectedIndex).Value
        If PipeOrBend = "Pipe" Then
            GetPipeDataFromUI(SummaryDataIAmFilling.MyPipeData)
        ElseIf PipeOrBend = "Bend" Then
            GetBendDataFromUI(SummaryDataIAmFilling.MyBendData)
        End If

    End Sub
    Private Function GetLetterEquivalent(ByVal SomeNumber)
        Dim LetterEquivalent As String = Chr(64 + SomeNumber)
        Return LetterEquivalent
    End Function
    Private Function GetPipeDataFromUI(ByVal PipeDataToFill As PipeData)
        'This gets all the data out of the rows visible in uiPipeOrBendDataGridView so it can be dumped into the summary data.
        For i = 0 To uiPipeOrBendDataInfoView.RowCount - 1
            Dim ThisRow As DataRow = uiPipeOrBendDataInfoView.GetDataRow(i)
            If Not ThisRow Is Nothing Then
                PipeDataToFill.AvgDiameter(i) = ThisRow("AvgDiam")
                PipeDataToFill.AvgWall(i) = ThisRow("AvgWall")
                PipeDataToFill.DesignedWall(i) = ThisRow("DesWall")
                PipeDataToFill.EstRemWall(i) = ThisRow("EstRemWall")
                PipeDataToFill.EstWallLoss(i) = ThisRow("EstWallLoss")
                PipeDataToFill.Location(i) = ThisRow("Location")
                PipeDataToFill.ID(i) = ThisRow("PipeID")
                PipeDataToFill.MinWall(i) = ThisRow("MinWall")
            End If
        Next
        PipeDataToFill.PipeSize = g_PipeDiameter
        PipeDataToFill.PipeSchedule = g_PipeSchedule
        PipeDataToFill.InternalOrExternal = uiInternalOrExternal.Text
        PipeDataToFill.GeneralOrLocal = uiGeneralOrLocal.Text
    End Function
    Private Function GetBendDataFromUI(ByVal PipeDataToFill As BendData)
        'This gets all the data out of the rows visible in uiPipeOrBendDataGridView so it can be dumped into the summary data.
        For i = 0 To uiPipeOrBendDataInfoView.RowCount - 1
            Dim ThisRow As DataRow = uiPipeOrBendDataInfoView.GetDataRow(i)
            If Not ThisRow Is Nothing Then
                PipeDataToFill.EstRemWall(i) = ThisRow("EstRemWall")
                PipeDataToFill.EstWallLoss(i) = ThisRow("EstWallLoss")
                PipeDataToFill.ID(i) = ThisRow("BendID")
                PipeDataToFill.MinWall(i) = ThisRow("MinWall")
            End If
        Next
        PipeDataToFill.PipeSize = g_PipeDiameter
        PipeDataToFill.PipeSchedule = g_PipeSchedule
        PipeDataToFill.InternalOrExternal = uiInternalOrExternal.Text
        PipeDataToFill.GeneralOrLocal = uiGeneralOrLocal.Text
    End Function

    Private Sub GetDeformationDataFromUI(ByRef TheDeformationData As DeformationData)
        If SEBulging.Text <> "" Then TheDeformationData.BulgingExceedingCount = SEBulging.Text
        If seBulgingBelow.Text <> "" Then TheDeformationData.BulgingNotExceedingCount = seBulgingBelow.Text
        If seBulgeThreshold.Text <> "" Then TheDeformationData.BulgingThreshold = seBulgeThreshold.Text
        If SEDenting.Text <> "" Then TheDeformationData.DentingExceedingCount = SEDenting.Text
        If seDentingBelow.Text <> "" Then TheDeformationData.DentingNotExceedingCount = seDentingBelow.Text
        If seDentThreshold.Text <> "" Then TheDeformationData.DentingThreshold = seDentThreshold.Text
        If SEOvality.Text <> "" Then TheDeformationData.OvalityExceedingCount = SEOvality.Text
        If seOvalityBelow.Text <> "" Then TheDeformationData.OvalityNotExceedingCount = seOvalityBelow.Text
        If seOvalityThreshold.Text <> "" Then TheDeformationData.OvalityThreshold = seOvalityThreshold.Text
        If SESwelling.Text <> "" Then TheDeformationData.SwellingExceedingCount = SESwelling.Text
        If seSwellingBelow.Text <> "" Then TheDeformationData.SwellingNotExceedingCount = seSwellingBelow.Text
        If seSwellThreshold.Text <> "" Then TheDeformationData.SwellingThreshold = seSwellThreshold.Text
        TheDeformationData.InternalFouling = chkInternalFouling.CheckState

    End Sub
#End Region

#Region "Copy Paste Routines"
    Private Property ClipboardData() As String
        Get
            Dim iData As IDataObject = Clipboard.GetDataObject()
            If iData Is Nothing Then
                Return ""
            End If

            If iData.GetDataPresent(DataFormats.Text) Then
                Return CStr(iData.GetData(DataFormats.Text))
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Clipboard.SetDataObject(value)
        End Set
    End Property


    Private Sub AddRow(ByVal data As String)
        If data = String.Empty Then
            Return
        End If
        'Split up one line of data separated by the special character.
        Dim rowData() As String = data.Split(New Char() {ControlChars.Cr, ChrW(&H9)})
        'Create a new row, unfortunately this does not return the handle to the new row.
        uiPipeOrBendDataInfoView.AddNewRow()
        'Get the new data row that was just created. This is an obnoxious looking line, I know!
        Dim TheNewRowHandle As Integer = uiPipeOrBendDataInfoView.GetDataRowHandleByGroupRowHandle(uiPipeOrBendDataInfoView.FocusedRowHandle)
        For i As Integer = 0 To rowData.Length - 1
            'if this is the first column of data, it should start with either 'Conv' or 'Rad' if it's a full row. Check to see if it has that at the start, and give a warning if not.
            If i = 0 And Not rowData(i).Contains("Rad") And Not rowData(i).Contains("Conv") Then
                Dim msgResult As MsgBoxResult = MessageBox.Show("The first item in this row to be pasted does not contain Rad or Conv. " & Environment.NewLine & Environment.NewLine & "Are you sure you have what you expect in the clipboard?", "Invalid Input", MessageBoxButtons.YesNo)
                If msgResult = MsgBoxResult.No Then
                    uiPipeOrBendDataInfoView.DeleteRow(TheNewRowHandle)
                    Return
                End If
            End If
            'Don't add data if there are no columns left to add to
            If i >= uiPipeOrBendDataInfoView.Columns.Count Then
                Exit For
            End If
            uiPipeOrBendDataInfoView.SetRowCellValue(TheNewRowHandle, uiPipeOrBendDataInfoView.VisibleColumns(i), rowData(i))
        Next i
    End Sub
    Private Sub btnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaste.Click
        Dim data() As String = ClipboardData.Split(ControlChars.Lf)
        If data.Length < 1 Then
            Return
        End If
        'Do a preliminary check for Conv or Rad in the data so they can quickly say 'no' to even starting the process of pasting if they have unexpected data.
        If Not data(0).Contains("Rad") And Not data(0).Contains("Conv") And data.Length > 1 Then
            Dim msgResult As MsgBoxResult = MessageBox.Show("The first row to be pasted does not contain Rad or Conv. Each row will be checked individually after this step if you continue." & Environment.NewLine & Environment.NewLine & "Are you sure you have what you expect in the clipboard?", "Probably Invalid Input", MessageBoxButtons.YesNo)
            If msgResult = MsgBoxResult.No Then Return
        End If
        For Each row As String In data
            'Only allow up to 8 rows to be created.
            If uiPipeOrBendDataInfoView.RowCount < 7 Then AddRow(row)
        Next row
        uiPipeOrBendDataInfoView.Focus()
    End Sub

    Private Function ProcessInsertSectionSummary(ByVal sender As System.Object)
        Dim strName As String = sender.name

        Select Case strName
            Case "uiInsertSectionSummaryButton", "uiReplaceSectionSummaryButton"
                uiSectionSummaryEditor.Text = uiSectionSummaryTextbox.Text
            Case "uiAppendSectionSummaryButton"
                Dim strSpace As String
                If uiSectionSummaryEditor.Text.Length > 0 Then strSpace = vbNewLine & vbNewLine
                uiSectionSummaryEditor.Document.AppendText(strSpace & uiSectionSummaryTextbox.Text)
        End Select

        'Highlight the headers (IE: the 5 in. Schedule 80 heading on a pipe segment)
        UnderlineHeaders()

    End Function
    Private Sub UnderlineHeaders()
        Dim docSummaryText As Document = uiSectionSummaryEditor.Document 'This is the document itself.
        Dim rngTextToUnderline() As DocumentRange 'This range is the text that will be selected
        Dim CharProps As CharacterProperties 'this is how you actually edit the properties to add underlines and such
        'the pattern is: 1 or more digits/decimals + in. Schedule + 1 or more digits/decimals
        Dim strRegExPattern As String = "(" & uiPipeSection.Text & "){1}"
        Dim myRegEx As Regex = New Regex(strRegExPattern)

        'Find all the ranges that match the expression and need underlining
        rngTextToUnderline = docSummaryText.FindAll(myRegEx)

        If rngTextToUnderline.Length > 0 Then
            For Each Header As DocumentRange In rngTextToUnderline
                CharProps = docSummaryText.BeginUpdateCharacters(Header)
                CharProps.Underline = UnderlineType.Single
                docSummaryText.EndUpdateCharacters(CharProps)
            Next
        End If


    End Sub

    Private Sub uiSecSumShowHideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSecSumShowHideButton.Click
        If G_bvisible4 Then
            With uiSectionSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Hidden
                .TabCharacter = RichEditFormattingMarkVisibility.Hidden
                .Space = RichEditFormattingMarkVisibility.Hidden
            End With
            G_bvisible4 = False
        Else
            With uiSectionSummaryEditor.Options.FormattingMarkVisibility
                .ParagraphMark = RichEditFormattingMarkVisibility.Visible
                .TabCharacter = RichEditFormattingMarkVisibility.Visible
                .Space = RichEditFormattingMarkVisibility.Visible
            End With
            G_bvisible4 = True
        End If
    End Sub

    Private Sub uiAppendSectionSummaryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiAppendSectionSummaryButton.Click
        ProcessInsertSectionSummary(sender)
    End Sub

    Private Sub uiReplaceSectionSummaryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiReplaceSectionSummaryButton.Click
        ProcessInsertSectionSummary(sender)
    End Sub

    Private Sub uiClearSectionSummaryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiClearSectionSummaryButton.Click
        uiSectionSummaryEditor.Text = ""
    End Sub
#End Region

    Public Sub RefreshPipeTubeData()
        taTblPipeTubeItems.Fill(Me.QTT_SummaryData.tblPipeTubeItems)
    End Sub


    Private Sub uiSectionSelection_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiSectionSelection.EditValueChanged
        If uiSectionSelection.EditValue = -1 Then Return 'this just means the value was flipped to force an update, so ignore the change.
        uiPipeSection.SelectedIndex = -1
        ToggleButtons("Enable") 'make sure all the buttons are enabled.
        UpdateSummaryDBTables()
        Dim strPipeType As String = Me.QTT_SummaryData.tblPipeTubeItems.Select("ID = " & uiSectionSelection.EditValue)(0).Item("Description")
        g_ConvectionOrRadiant = ""
        If strPipeType.Contains(" Piping") Then strPipeType = strPipeType.Remove(strPipeType.LastIndexOf(" Piping"))
        g_ConvectionOrRadiant = strPipeType

        g_CurrentPipeTubeItemID = uiSectionSelection.EditValue
        g_CurrentPipeTubeSummaryID = GetCurrentPipeTubeSummary(g_CurrentPipeTubeItemID, g_PassNumber)
        bsSectionSummaries.Filter = "ID = " & g_CurrentPipeTubeSummaryID
        UpdateAvailablePipeSections(True)
        Dim tmpPipeName As String = uiPipeSection.EditValue
        If IsNothing(tmpPipeName) Then tmpPipeName = "*" ' Just use the wildcard if nothing has set this value
        bsSummaryPipeOrBendData.Filter = "FK_Summary = " & g_intSectionSummaryID & " and FK_PipeTubeItemID = " & g_CurrentPipeTubeItemID & " AND PipeName = '" & tmpPipeName & "'"
        SetPipeBendDataInfoFilter()

    End Sub

    Private Sub uiPipeSection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uiPipeSection.SelectedIndexChanged
        If uiPipeSection.SelectedIndex = -1 Then Return 'Form is just resetting values for a changed section (IE: Convection to Radiant)
        UpdateSummaryDBTables()
        bsSummaryPipeOrBendData.Filter = "FK_Summary = " & g_intSectionSummaryID & " and FK_PipeTubeItemID = " & g_CurrentPipeTubeItemID & " AND PipeName = '" & uiPipeSection.Properties.Items(uiPipeSection.SelectedIndex).ToString & "'"
        g_intPipeOrBendDataID = bsSummaryPipeOrBendData.Item(0)("ID")
        SetPipeBendDataInfoFilter()
        'If the first character is a space, remove it.
        Dim PipeName As String = uiPipeSection.Properties.Items(uiPipeSection.SelectedIndex).ToString
        If PipeName(0) = " " Then PipeName = PipeName.Substring(1, PipeName.Length - 1)
        DerivePipeInfoFromName(PipeName)
    End Sub
    Private Sub DerivePipeInfoFromName(ByVal tmpPipeText As String)
        'This assumes incoming data looks like '4 in. Schedule 40'. If it's something else it may freak out, but it wil hopefully just not find what it's looking for and give default values.

        Dim strRegExPipeDiameter As String = "[0-9]+ in." 'look for numbers, a space, and then 'in.'
        Dim PipeDiamaterRegEx As New Regex(strRegExPipeDiameter)
        Dim DiameterMatch As System.Text.RegularExpressions.Match = PipeDiamaterRegEx.Match(tmpPipeText)
        If DiameterMatch.Success = True Then 'get the numbers out of it
            Dim FirstSpaceLocation As Integer = tmpPipeText.IndexOf(" ") 'look for the first space
            g_PipeDiameter = tmpPipeText.Substring(0, FirstSpaceLocation)
        Else
            g_PipeDiameter = "Other"
        End If
        Dim strRegExPipeSchedule As String = "Schedule [0-9]+" 'look for the word 'Schedule' and then numbers
        Dim PipeScheduleRegEx As New Regex(strRegExPipeSchedule)
        Dim ScheduleMatch As System.Text.RegularExpressions.Match = PipeScheduleRegEx.Match(tmpPipeText)
        If ScheduleMatch.Success = True Then 'get the numbers out of it
            Dim LastSpaceLocation As Integer = tmpPipeText.LastIndexOf(" ")
            g_PipeSchedule = tmpPipeText.Substring(LastSpaceLocation + 1, tmpPipeText.Length - LastSpaceLocation - 1)
        Else
            g_PipeSchedule = "Tube"
        End If

    End Sub
    Public Sub ToggleButtons(ByVal EnableOrDisable As String)
        'This subroutine goes through the controls specified and enables or disables them if they need to be.
        Dim blnEnabled As Boolean
        If EnableOrDisable = "Enable" Then blnEnabled = True
        If EnableOrDisable = "False" Then blnEnabled = False
        Dim ControlsToToggle As New List(Of Control)
        ControlsToToggle.Add(uiSectionSelection)
        ControlsToToggle.Add(uiPipeSection)
        ControlsToToggle.Add(uiNewUsedPipes)
        ControlsToToggle.Add(uiUnits)
        ControlsToToggle.Add(chkInternalFouling)
        ControlsToToggle.Add(uiGeneralOrLocal)
        ControlsToToggle.Add(uiInternalOrExternal)
        ControlsToToggle.Add(radPipeOrBend)
        ControlsToToggle.Add(btnPaste)
        ControlsToToggle.Add(btnWriteSectionSummary)
        ControlsToToggle.Add(uiPipeOrBendDataInfo)
        ControlsToToggle.Add(SEBulging)
        ControlsToToggle.Add(SESwelling)
        ControlsToToggle.Add(SEOvality)
        ControlsToToggle.Add(SEDenting)
        ControlsToToggle.Add(seBulgingBelow)
        ControlsToToggle.Add(seSwellingBelow)
        ControlsToToggle.Add(seOvalityBelow)
        ControlsToToggle.Add(seDentingBelow)
        ControlsToToggle.Add(seBulgeThreshold)
        ControlsToToggle.Add(seSwellThreshold)
        ControlsToToggle.Add(seOvalityThreshold)
        ControlsToToggle.Add(seDentThreshold)
        ControlsToToggle.Add(uiAppendSectionSummaryButton)
        ControlsToToggle.Add(uiReplaceSectionSummaryButton)
        ControlsToToggle.Add(uiClearSectionSummaryButton)
        ControlsToToggle.Add(uiSectionSummaryEditor)
        ControlsToToggle.Add(uiSectionSummaryTextbox)
        For Each Thing As Control In ControlsToToggle
            'enable or disable the control if it needs to be switched.
            If Thing.Enabled <> blnEnabled Then Thing.Enabled = blnEnabled
        Next

    End Sub
End Class
